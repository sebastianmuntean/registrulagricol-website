﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;

public partial class reparaAnimale : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
           RefacemAnimale();
    }
    protected void RefacemAnimale()
    {
        int vc1 = 0;
        int vc2 = 0;
        int vc = 0;

        // verificam toate unitatile
        //List<string> vUnitati = ManipuleazaBD.fRezultaListaStringuri("select SUM(col1) as sem1, SUM(col2) as sem2, unitati.unitateDenumire, capitole.unitateId from capitole inner join unitati on capitole.unitateId = unitati.unitateId  where codCapitol='7' and an = 2011 group by capitole.unitateid, unitateDenumire, an having SUM(col2) <= 0 and sum(col1) >0 order by capitole.unitateId ", "unitateId");

        List<string> vUnitati = new List<string> { "20", "19", "46", "121", "29", "22", "33", "44", "45", "16", "47", "37", "10", "36", "14", "17", "30", "49", "26", "24", "59"};
        foreach (string vUnitateIdDinLista in vUnitati)
        {
            // luam la rand toate gospodariile
            List<string> vListaGospodarii = ManipuleazaBD.fRezultaListaStringuri("SELECT * FROM gospodarii WHERE unitateid = '" + vUnitateIdDinLista + "' and an='2011'", "gospodarieId", Convert.ToInt16(Session["SESan"]));
            string vInsert = "";
            // capitolul 7
            foreach (string vGospodarie in vListaGospodarii)
            {
                if (1059 >= 1059)
                {
                    vInsert = "";
                    string vGospodarie2012 = ManipuleazaBD.fRezultaUnString("SELECT TOP(1) * FROM gospodarii WHERE an = '2012' and gospodarieidinitial='" + vGospodarie + "' and unitateid ='" + vUnitateIdDinLista + "'", "gospodarieId", Convert.ToInt16(Session["SESan"]));
                    // verificam sa nu aiba scrise deja animale in 2012; daca are nu facem nimic
                    string vNumarAnimaleIntroduseIn2012 = ManipuleazaBD.fRezultaUnString("SELECT coalesce(SUM(col1),0) as suma FROM capitole WHERE gospodarieID = '" + vGospodarie2012 + "' and an = 2012 and codcapitol='7'", "suma", Convert.ToInt16(Session["SESan"]));
                    string vNumarAnimaleIntroduseInSem2 = ManipuleazaBD.fRezultaUnString("SELECT  coalesce(SUM(col2),0) as suma FROM capitole WHERE gospodarieID = '" + vGospodarie + "' and an = 2011  and codcapitol='7'", "suma", Convert.ToInt16(Session["SESan"]));
                    string vNumarAnimaleIntroduseInSem1 = ManipuleazaBD.fRezultaUnString("SELECT  coalesce(SUM(col1),0) as suma FROM capitole WHERE gospodarieID = '" + vGospodarie + "' and an = 2011  and codcapitol='7'", "suma", Convert.ToInt16(Session["SESan"]));
                    if (Convert.ToInt32(Convert.ToDecimal(vNumarAnimaleIntroduseIn2012.Replace(',', '.'))) <= 0 && Convert.ToInt32(Convert.ToDecimal(vNumarAnimaleIntroduseInSem2.Replace(',', '.'))) <= 0 && Convert.ToInt32(Convert.ToDecimal(vNumarAnimaleIntroduseInSem1.Replace(',', '.'))) > 0)
                    {
                        // verificam pe 2011 semestrul 1 si copiem in semestrul 2 in cap 7
                        List<string> vCampuri = new List<string> { "gospodarieId", "codRand", "col1", "col2", "capitolId" };
                        List<List<string>> vRezultate = ManipuleazaBD.fRezultaListaStringuri("SELECT capitolid, unitateId, gospodarieId, an, codCapitol, codRand, col1, col2, col3, col4, col5 FROM capitole WHERE unitateid  = '" + vUnitateIdDinLista + "'AND (codCapitol = '7') AND (an = 2011) AND (gospodarieId = '" + vGospodarie + "') ORDER BY codRand ", vCampuri, Convert.ToInt16(Session["SESan"]));
                        foreach (List<string> vListaRezultat in vRezultate)
                        {
                            vInsert += "UPDATE capitole SET col2 = '" + vListaRezultat[2].Replace(',', '.') + "' WHERE capitolId ='" + vListaRezultat[4] + "' and unitateid = '" + vUnitateIdDinLista + "' and an = '2011' and codcapitol = '7' and gospodarieid = '" + vGospodarie + "'; ";
                            // scoatem gospodaria noua din 2012
                            vInsert += "UPDATE capitole SET col1 = '" + vListaRezultat[2].Replace(',', '.') + "' WHERE unitateid = '" + vUnitateIdDinLista + "' and an = '2012' and codcapitol = '7' and gospodarieid = '" + vGospodarie2012 + "' and codrand = '" + vListaRezultat[1] + "'; ";
                        }
                        List<string> vListaCodRand = new List<string> { "1", "10", "18", "27", "36", "45", "54", "63", "72" };
                        List<string> vListaCodRandVechi = new List<string> { "9", "17", "26", "35", "44", "53", "62", "71", "79" };
                        // verificam pe 2011 semestrul 1 si copiem in semestrul 2 in cap 7
                        vCampuri = new List<string> { "gospodarieId", "codRand", "col1", "col2", "capitolId" };
                        vRezultate = ManipuleazaBD.fRezultaListaStringuri("SELECT capitolid, unitateId, gospodarieId, an, codCapitol, codRand, col1, col2 FROM capitole WHERE unitateid  = '" + vUnitateIdDinLista + "'AND (codCapitol = '8') AND (an = 2011) AND (gospodarieId = '" + vGospodarie + "') ORDER BY codRand ", vCampuri, Convert.ToInt16(Session["SESan"]));
                        int vContor = 0;
                        foreach (List<string> vListaRezultat in vRezultate)
                        {
                            // cautam randul corespunzator 1-->9 etc
                            int vPozitie = vListaCodRand.IndexOf(vListaRezultat[1]);
                            if (vPozitie != -1)
                            {
                                // cautam pozitia corespondentului 1 --> 9
                                string vPozitieCorespondent = vListaCodRandVechi[vPozitie];
                                foreach (List<string> vListaRezultat1 in vRezultate)
                                {
                                    if (vListaRezultat1[1] == vPozitieCorespondent)
                                    {
                                        //vInsert += "UPDATE capitole SET col2 = '" + vListaRezultat1[2].Replace(',', '.') + "' WHERE capitolId ='" + vListaRezultat1[4] + "' and unitateid = '" + vUnitateId + "' and an = '2011' and codcapitol = '8' and gospodarieid = '" + vGospodarie + "'; ";
                                        vInsert += "UPDATE capitole SET col2 = '" + vListaRezultat1[2].Replace(',', '.') + "' WHERE codrand = '" + vListaRezultat1[1] + "' and unitateid = '" + vUnitateIdDinLista + "' and an = '2011' and codcapitol = '8' and gospodarieid = '" + vGospodarie + "'; ";
                                        vInsert += "UPDATE capitole SET col2 = '" + vListaRezultat1[2].Replace(',', '.') + "' WHERE codrand = '" + vListaRezultat[1] + "' and unitateid = '" + vUnitateIdDinLista + "' and an = '2011' and codcapitol = '8' and gospodarieid = '" + vGospodarie + "'; ";
                                        //scoatem gospodaria noua din 2012
                                        vInsert += "UPDATE capitole SET col1 = '" + vListaRezultat1[2].Replace(',', '.') + "' WHERE unitateid = '" + vUnitateIdDinLista + "' and an = '2012' and codcapitol = '8' and gospodarieid = '" + vGospodarie2012 + "' and codrand = '" + vListaRezultat1[1] + "'; ";
                                        vInsert += "UPDATE capitole SET col1 = '" + vListaRezultat1[2].Replace(',', '.') + "' WHERE unitateid = '" + vUnitateIdDinLista + "' and an = '2012' and codcapitol = '8' and gospodarieid = '" + vGospodarie2012 + "' and codrand = '" + vListaRezultat[1] + "'; ";
                                        break;
                                    }
                                }
                            }
                            vContor++;
                        }
                        // return;


                        string strConn = ConfigurationManager.ConnectionStrings["TNTRegistruAgricolConnectionString"].ConnectionString;
                        SqlConnection con = new SqlConnection(strConn);
                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = vInsert.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
                        con.Open();
                        try
                        {
                            cmd.ExecuteNonQuery();
                            vc1++;
                            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "repara animale", "reusit la gospodaria " + vGospodarie + "(2011) - " + vGospodarie2012 + "(2012);", vc1 + "/" + vListaGospodarii.Count.ToString(), Convert.ToInt64(Session["SESgospodarieId"]), 1);
                        }
                        catch (Exception ex)
                        {
                            vc2++;
                            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "repara animale", "EROARE la gospodaria " + vGospodarie + "(2011) - " + vGospodarie2012 + "(2012);" + ex.Message, +vc2 + "/" + vListaGospodarii.Count.ToString() + ": " + vInsert.Replace('\'', '"'), Convert.ToInt64(Session["SESgospodarieId"]), 1);
                            //  return;
                        }
                        con.Close();
                        lblEroare.Text = "vc1 = " + vc1 + "; vc2= " + vc2;
                        //   ManipuleazaBD.fManipuleazaBD(vInsert);
                    }
                }
                vc++;
            }
        }
    }
}
