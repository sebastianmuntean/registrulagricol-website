﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="hartaLayers.aspx.cs" Inherits="hartaLayers" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript">
        function colorChanged(sender) {
            sender.get_element().style.color =
       "#" + sender.get_selectedColor();
            sender.get_element().style.backgroundColor = "#" + sender.get_selectedColor();
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Configurare / Layere" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="pnAfiseazaMesaje" runat="server">
        <asp:ValidationSummary ID="valCapitole" runat="server" DisplayMode="SingleParagraph"
            Visible="true" CssClass="validator" ForeColor="" />
        <asp:CustomValidator ID="valCustom" runat="server" Visible="false" ErrorMessage=""></asp:CustomValidator>
    </asp:Panel>
    <asp:Panel ID="pnListaLayere" runat="server" CssClass="panel_general" Visible="true">
        <asp:Panel ID="pnGrid" runat="server" Visible="true">
            <asp:GridView ID="gvLayere" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                BorderStyle="None" CellPadding="3" GridLines="Vertical" ShowFooter="True" CssClass="tabela"
                EmptyDataText="Nu sunt adaugate inregistrari" AllowPaging="False" DataKeyNames="layerId"
                DataSourceID="SqlLayers" OnRowDataBound="gvLayere_RowDataBound" OnSelectedIndexChanged="gvLayere_SelectedIndexChanged"
                OnPageIndexChanged="gvLayere_PageIndexChanged">
                <Columns>
                    <asp:BoundField DataField="layerDenumire" HeaderText="Denumire" SortExpression="layerDenumire" />
                    <asp:TemplateField HeaderText="Culoare" SortExpression="layerCuloare">
                        <ItemTemplate>
                            <asp:TextBox ID="tbCuloareGrid" Enabled="false" Width="50px" BorderStyle="None" runat="server"
                                Text='<%# Bind("layerCuloare") %>'></asp:TextBox>
                            <asp:ColorPickerExtender ID="tbCuloareGrid_ColorPickerExtender" runat="server" Enabled="True"
                                TargetControlID="tbCuloareGrid" OnClientColorSelectionChanged="colorChanged">
                            </asp:ColorPickerExtender>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                <HeaderStyle Font-Bold="True" />
            </asp:GridView>
            <asp:SqlDataSource ID="SqlLayers" runat="server" 
                SelectCommand="SELECT layerId, unitateId, layerDenumire, COALESCE (layerCuloare, '0000CC') AS layerCuloare FROM hartaLayer WHERE (unitateId = @unitateId) ORDER BY layerDenumire">
                <SelectParameters>
                    <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" Type="Int32" />
                </SelectParameters>
            </asp:SqlDataSource>
        </asp:Panel>
        <asp:Panel ID="pnButoane" CssClass="butoane" runat="server">
            <asp:Button CssClass="buton" ID="btModifica" Visible="false" runat="server" Text="modifică layer"
                OnClick="btModifica_Click" />
            <asp:Button CssClass="buton" ID="btSterge" Visible="false" runat="server" Text="șterge layer"
                OnClientClick="return confirm (&quot;Sigur stergeti ?&quot;)" OnClick="btSterge_Click" />
            <asp:Button CssClass="buton" ID="btAdauga" runat="server" Text="adaugă layer" OnClick="btAdauga_Click" />
        </asp:Panel>
    </asp:Panel>
    <!-- adaugam unitate -->
    <asp:Panel ID="pnAdaugaLayer" CssClass="panel_general" runat="server" Visible="false">
        <asp:Panel ID="Panel1" CssClass="adauga" runat="server">
            <h2>
                ADAUGĂ LAYER</h2>
            <p>
                <asp:Label ID="lblDenumire" runat="server" Text="Denumire"></asp:Label>
                <asp:TextBox ID="tbDenumire" runat="server"></asp:TextBox>
                <asp:Label ID="lblCuloare" runat="server" Text="Culoare"></asp:Label>
                <asp:TextBox ID="tbCuloare" onmouseover="this.style.cursor='pointer'" runat="server"></asp:TextBox>
                <asp:TextBoxWatermarkExtender ID="tbCuloare_TextBoxWatermarkExtender" runat="server"
                    Enabled="True" TargetControlID="tbCuloare" WatermarkText="Alegeti o culoare">
                </asp:TextBoxWatermarkExtender>
                <asp:ColorPickerExtender ID="tbCuloare_ColorPickerExtender" runat="server" Enabled="True"
                    TargetControlID="tbCuloare" OnClientColorSelectionChanged="colorChanged">
                </asp:ColorPickerExtender>
            </p>
            <asp:Panel ID="pnButoaneAdaugare" runat="server" CssClass="butoane">
                <asp:Button ID="btSalveazaLocalitate" runat="server" CssClass="buton" Text="adauga layer"
                    ValidationGroup="GrupValidareSabloaneCapitole" OnClick="btSalveazaLocalitate_Click" />
                <asp:Button ID="btAnuleazaLocalitate" runat="server" CssClass="buton" Text="listă layere"
                    OnClick="btAnuleazaLocalitate_Click" />
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
</asp:Content>
