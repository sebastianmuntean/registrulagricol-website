﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;

public partial class printInstiitareRegistruAgricol : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "printInstiintare", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
        }
        if (Request.QueryString["ordonare"] == "0")
            ObjectDataSource2.SelectMethod = "GetData";
        else if (Request.QueryString["ordonare"] == "1")
            ObjectDataSource2.SelectMethod = "GetData1";
        else if (Request.QueryString["ordonare"] == "2")
            ObjectDataSource2.SelectMethod = "GetData2";
    }
    protected void Page_SaveStateComplete(object sender, EventArgs e)
    {
        Warning[] warnings;
        string[] streamids;
        string mimeType;
        string encoding;
        string fileNameExtension;
        byte[] bytes = ReportViewer1.LocalReport.Render("PDF", null, out mimeType, out encoding, out fileNameExtension, out streamids, out warnings);
        Response.Buffer = true;
        Response.Clear();
        Response.ContentType = mimeType;
        Response.AddHeader("content-disposition", "inline; filename=Nume" + fileNameExtension);
        Response.BinaryWrite(bytes);
        Response.Flush();
        Response.End();
        Response.Close();
    }
}
