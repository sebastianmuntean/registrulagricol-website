﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cautaMembri : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
           RefreshGridView();
        }
    }
    protected void cautaMembriGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(cautaMembriGridView, e, this);
        e.Row.Cells[0].Visible = false;
    }

    private void RefreshGridView()
    {
        MembriServices.CNP = cnpTextBox.Text;
        MembriServices.volum = volumTextBox.Text;
        MembriServices.pozitie = pozitieTextBox.Text;
        MembriServices.capDeGospodarie = capDeGospodarieTextBox.Text;
        MembriServices.numeMmebru = numeMembrutextBox.Text;
        MembriServices.dataNasterii = dataNasteriiTextBox.Text;
        cautaMembriGridView.DataSource = MembriServices.GetDetaliiMembri(Convert.ToInt16(Session["SESan"]));
        cautaMembriGridView.DataBind();
    }
    protected void cnpTextBox_TextChanged(object sender, EventArgs e)
    {
        RefreshGridView();
    }
    protected void volumTextBox_TextChanged(object sender, EventArgs e)
    {
        RefreshGridView();
    }
    protected void pozitieTextBox_TextChanged(object sender, EventArgs e)
    {
        RefreshGridView();
    }
    protected void capDeGospodarieTextBox_TextChanged(object sender, EventArgs e)
    {
        RefreshGridView();
    }
    protected void numeMembrutextBox_TextChanged(object sender, EventArgs e)
    {
        RefreshGridView();
    }

    protected void dataNasterii_CalendarExtender_Load(object sender, EventArgs e)
    {
        RefreshGridView();
    }
    protected void dataNasterii_CalendarExtender_Init(object sender, EventArgs e)
    {
        RefreshGridView();
    }
    protected void dataNasterii_CalendarExtender_PreRender(object sender, EventArgs e)
    {
        RefreshGridView();
    }

    protected void dataNasteriiTextBox_TextChanged(object sender, EventArgs e)
    {
        RefreshGridView();
    }
    protected void cautaMembriGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        cautaMembriGridView.PageIndex = e.NewPageIndex;
        RefreshGridView();
    }

}