﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="reparaLocalitati.aspx.cs" Inherits="reparaLocalitati" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    repara localitati
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="upLocalitati" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnListaLocalitati" runat="server" CssClass="panel_general" Visible="true">
                <asp:Panel ID="pnCautare" runat="server" CssClass="cauta">
                    <asp:Label ID="lblUnitati" runat="server" Text="Unitatea"></asp:Label>
                    <asp:DropDownList ID="ddlUnitati" runat="server" DataSourceID="SqlUnitati" DataTextField="unitateDenumire"
                        DataValueField="unitateId" AutoPostBack="true">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlUnitati" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString %>"
                        SelectCommand="SELECT [unitateId], [unitateDenumire] FROM [unitati] ORDER BY [unitateDenumire]">
                    </asp:SqlDataSource>
                </asp:Panel>
                <asp:Panel ID="pnGrid" runat="server" Visible="true">
                    <h1>
                        Localitati gospodarii
                    </h1>
                    <asp:GridView ID="gvLocalitatiGospodarii" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        BorderStyle="None" CellPadding="3" GridLines="Vertical" ShowFooter="True" CssClass="tabela"
                        EmptyDataText="Nu sunt adaugate inregistrari" AllowPaging="True" DataSourceID="SqlLocalitatiGospodarii">
                        <Columns>
                            <asp:TemplateField HeaderText="Denumire localitate" SortExpression="localitate">
                                <ItemTemplate>
                                    <asp:Label ID="lblDenumireVeche" runat="server" Text='<%# Bind("localitate") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Noua denumire">
                                <ItemTemplate>
                                    <asp:TextBox ID="tbDenumireNoua" runat="server"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbSalveaza" runat="server" OnClick="lbSalveaza_Click">Salveaza</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                        <HeaderStyle Font-Bold="True" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlLocalitatiGospodarii" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString %>"
                        SelectCommand="SELECT DISTINCT localitate FROM gospodarii WHERE (unitateId = @unitateId) ORDER BY localitate">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="ddlUnitati" Name="unitateId" PropertyName="SelectedValue" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <h1>
                        Localitati gospodarii
                    </h1>
                    <asp:GridView ID="gvLocalitatiParcele" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        BorderStyle="None" CellPadding="3" GridLines="Vertical" ShowFooter="True" CssClass="tabela"
                        EmptyDataText="Nu sunt adaugate inregistrari" AllowPaging="True" DataSourceID="SqlLocalitatiParcele">
                        <Columns>
                            <asp:TemplateField HeaderText="Denumire localitate" SortExpression="localitate">
                                <ItemTemplate>
                                    <asp:Label ID="lblDenumireVeche" runat="server" Text='<%# Bind("localitate") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Noua denumire">
                                <ItemTemplate>
                                    <asp:TextBox ID="tbDenumireNoua" runat="server"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbSalveaza" runat="server" onclick="lbSalveaza_Click1">Salveaza</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                        <HeaderStyle Font-Bold="True" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlLocalitatiParcele" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString %>"
                        
                        SelectCommand="SELECT DISTINCT parcelaLocalitate AS localitate FROM parcele WHERE (unitateaId = @unitateId) ORDER BY localitate">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="ddlUnitati" Name="unitateId" PropertyName="SelectedValue" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </asp:Panel>
                <asp:Panel ID="pnButoane" CssClass="butoane" runat="server">
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
