﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
/// <summary>
/// autor       ?
/// data        ?
/// ultima modificare       20.01.2012  
///         autor           alex    
///         modificari      -sa nu se poata adaug o localitate fara denumire
///                         -adaugare camp  cod si ruta
///                         -daca modifici o localitate , modifica si in gospodarii cod si ruta si denumire localitate
/// </summary>

public partial class localitati : System.Web.UI.Page
{
    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        DBConnections connection = new DBConnections(Convert.ToInt16(Session["SESan"]));

        SqlLocalitati.ConnectionString = connection.Create();

        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "localitatiComuna", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
        }
    }
    protected void gvLocalitati_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(gvLocalitati, e, this);
    }
    protected void gvLocalitati_SelectedIndexChanged(object sender, EventArgs e)
    {
        btModifica.Visible = true;
        btSterge.Visible = true;
    }
    protected void btAdauga_Click(object sender, EventArgs e)
    {
        ViewState["tip"] = "a";
        tbDenumire.Text = "";
        tbCodSiRuta.Text = "";
        pnCautare.Visible = false;
        pnGrid.Visible = false;
        pnButoane.Visible = false;
        pnAdaugaLocalitate.Visible = true;
    }
    protected void btSterge_Click(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        // nu se pot sterge localitati daca sunt adaugate in gospodarii
        vCmd.CommandText = "select top(1) localitateDenumire from localitatiComuna where localitateId='" + gvLocalitati.SelectedValue + "'";
        String vDenumire = vCmd.ExecuteScalar().ToString();
        // nu se pot sterge localitati daca sunt adaugate in parcele
        vCmd.CommandText = "select count(*) from gospodarii where localitate='" + vDenumire + "' and unitateId='" + Session["SESunitateId"].ToString() + "'";
        Int32 vCount = Convert.ToInt32(vCmd.ExecuteScalar());
        vCmd.CommandText = "select count(*) from parcele where parcelaLocalitate='" + vDenumire + "' and unitateaId='" + Session["SESunitateId"].ToString() + "'";
        vCount += Convert.ToInt32(vCmd.ExecuteScalar());
        if (vCount == 0)
        {
            vCmd.CommandText = "delete from localitatiComuna where localitateId='" + gvLocalitati.SelectedValue.ToString() + "'";
            vCmd.ExecuteNonQuery();
        }
        else
        {
            valCapitol.IsValid = false;
            valCapitol.ErrorMessage = "Nu pot fi sterse localitati care sunt adaugate pe pospodarii sau pe parcele";
        }
        ManipuleazaBD.InchideConexiune(vCon);
        gvLocalitati.DataBind();
    }
    protected void btModifica_Click(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select * from localitatiComuna where localitateId='" + gvLocalitati.SelectedValue.ToString() + "'";
        SqlDataReader vTabel = vCmd.ExecuteReader();
        if (vTabel.Read())
        {
            ViewState["tip"] = "m";
            ViewState["denumire"] = tbDenumire.Text = vTabel["localitateDenumire"].ToString();
            tbCodSiRuta.Text = vTabel["localitateCodSiRuta"].ToString();
            pnCautare.Visible = false;
            pnGrid.Visible = false;
            pnButoane.Visible = false;
            pnAdaugaLocalitate.Visible = true;
        }
        ManipuleazaBD.InchideConexiune(vCon);
    }
    protected void btSalveazaLocalitate_Click(object sender, EventArgs e)
    {
        if (tbDenumire.Text.Length != 0)
        {
            SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
            SqlCommand vCmd = new SqlCommand();
            vCmd.Connection = vCon;
            SqlCommand vCmd1 = new SqlCommand();
            vCmd1.Connection = vCon;
            SqlTransaction vTrans = ManipuleazaBD.DeschideTranzactie(vCon);
            vCmd.Transaction = vTrans;
            vCmd1.Transaction = vTrans;

            if (ViewState["tip"].ToString() == "a")
            {
                try
                {  // adauga localitate
                    vCmd.CommandText = "insert into localitatiComuna (unitateId, localitateDenumire,localitateCodSiRuta) values ('" + Session["SESunitateId"].ToString() + "', N'" + tbDenumire.Text + "',N'" + tbCodSiRuta.Text + "')";
                    vCmd.CommandText = vCmd.CommandText.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
                    vCmd.ExecuteNonQuery();
                    vTrans.Commit();
                }
                catch { vTrans.Rollback(); }
            }
            else if (ViewState["tip"].ToString() == "m")
            {
                try
                {
                    if (ViewState["denumire"] != null)
                    {
                        //modificare localitate in gospodari
                        vCmd.CommandText = "SELECT        gospodarieId FROM            gospodarii WHERE        (unitateId = " + Session["SESunitateId"].ToString() + ") AND (localitate = '" + ViewState["denumire"].ToString() + "') AND (an = " + Session["SESan"].ToString() + ")";
                        SqlDataReader vSdR = vCmd.ExecuteReader();
                        while (vSdR.Read())
                        {
                            vCmd1.CommandText = "UPDATE       gospodarii SET                codSiruta ='" + tbCodSiRuta.Text + "', localitate ='" + tbDenumire.Text + "', dataModificare=convert(datetime,'" + DateTime.Now.Date + "',104),oraModificare='" + DateTime.Now.Hour + "', minutModificare='" + DateTime.Now.Minute + "' WHERE        (unitateId = " + Session["SESunitateId"].ToString() + ") AND (localitate = '" + ViewState["denumire"].ToString() + "') AND (an = " + Session["SESan"].ToString() + ") AND (gospodarieId = " + vSdR["gospodarieId"].ToString() + ")";
                            vCmd1.ExecuteNonQuery();
                        }
                        vSdR.Close();
                    }
                    // modifica localitate
                    vCmd.CommandText = "update localitatiComuna set localitateCodSiRuta='" + tbCodSiRuta.Text + "', localitateDenumire=N'" + tbDenumire.Text + "' where localitateId='" + gvLocalitati.SelectedValue.ToString() + "'";
                    vCmd.CommandText = vCmd.CommandText.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
                    vCmd.ExecuteNonQuery();
                    vTrans.Commit();
                }
                catch { vTrans.Rollback(); }
            }
            gvLocalitati.DataBind();
            pnCautare.Visible = true;
            pnGrid.Visible = true;
            pnButoane.Visible = true;
            pnAdaugaLocalitate.Visible = false;
            ManipuleazaBD.InchideConexiune(vCon);
        }
        else
        {
            valCapitol.IsValid = false;
            valCapitol.ErrorMessage = "Atentie campuri goale ! Denumire !!!";
        }
    }
    protected void btAnuleazaLocalitate_Click(object sender, EventArgs e)
    {
        pnCautare.Visible = true;
        pnGrid.Visible = true;
        pnButoane.Visible = true;
        pnAdaugaLocalitate.Visible = false;
    }
}
