﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="reparaDiacriticeInchidereAn.aspx.cs" Inherits="reparaDiacriticeInchidereAn" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="pnAfiseazaMesaje" runat="server">
        <asp:ValidationSummary ID="valSumUtilizatori" runat="server" DisplayMode="SingleParagraph"
            Visible="true" ValidationGroup="GrupValidareExport" CssClass="validator" ForeColor="" />
        <asp:CustomValidator ID="valCustom" runat="server" ErrorMessage="" ValidationGroup="GrupValidareExport"></asp:CustomValidator>
    </asp:Panel>
    <asp:Panel ID="pnListaLocalitati" runat="server" CssClass="panel_general" Visible="true">
        <asp:Panel ID="pnGrid" runat="server" Visible="true">
            <h1>
                Adauga diacritice la gospodarii, memebri si parcele
            </h1>
            <p>
                Verifica in anul precedent daca datele sunt la fel cu cele din anul curent si face
                update pe an curent
            </p>
            <p>
                <asp:Label ID="lblAn" runat="server" Text="An"></asp:Label>
                <asp:TextBox ID="tbAn" runat="server"></asp:TextBox>
            </p>
            <p>
                <asp:Label ID="lblUnitatea" runat="server" Text="Unitatea"></asp:Label>
                <asp:DropDownList ID="ddlUnitate" runat="server" DataSourceID="SqlUnitati" DataTextField="unitate"
                    DataValueField="unitateId" AppendDataBoundItems="True">
                    <asp:ListItem Text="-toate-" Value="%"></asp:ListItem>
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlUnitati" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016 %>"
                    SelectCommand="SELECT judete.judetDenumire + ' - ' + unitati.unitateDenumire AS unitate, unitati.unitateId FROM unitati INNER JOIN judete ON unitati.judetId = judete.judetId ORDER BY unitate">
                </asp:SqlDataSource>
            </p>
            <asp:Panel ID="pnUnitatiReparate" runat="server">
                <h1>
                    Unitati deja reparate
                </h1>
                <asp:GridView ID="gvUnitatiFacute" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                    BorderStyle="None" CellPadding="3" GridLines="Vertical" ShowFooter="True" CssClass="tabela"
                    EmptyDataText="Nu sunt adaugate inregistrari" AllowPaging="True" DataKeyNames="id"
                    DataSourceID="SqlDataSource1">
                    <Columns>
                        <asp:CommandField ShowDeleteButton="True" />
                        <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True"
                            SortExpression="id" />
                        <asp:BoundField DataField="unitateId" HeaderText="unitateId" SortExpression="unitateId" />
                        <asp:BoundField DataField="unitateDenumire" HeaderText="unitateDenumire" SortExpression="unitateDenumire" />
                        <asp:BoundField DataField="tip" HeaderText="tip" SortExpression="tip" />
                        <asp:BoundField DataField="data" HeaderText="data" SortExpression="data" />
                    </Columns>
                    <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                    <HeaderStyle Font-Bold="True" />
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString %>"
                    DeleteCommand="DELETE FROM [tempMihaiUnitati] WHERE [id] = @id" InsertCommand="INSERT INTO [tempMihaiUnitati] ([unitateId], [unitateDenumire], [tip], [data]) VALUES (@unitateId, @unitateDenumire, @tip, @data)"
                    SelectCommand="SELECT * FROM [tempMihaiUnitati] ORDER BY [data]" UpdateCommand="UPDATE [tempMihaiUnitati] SET [unitateId] = @unitateId, [unitateDenumire] = @unitateDenumire, [tip] = @tip, [data] = @data WHERE [id] = @id">
                    <DeleteParameters>
                        <asp:Parameter Name="id" Type="Int64" />
                    </DeleteParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="unitateId" Type="Int32" />
                        <asp:Parameter Name="unitateDenumire" Type="String" />
                        <asp:Parameter Name="tip" Type="String" />
                        <asp:Parameter Name="data" Type="DateTime" />
                        <asp:Parameter Name="id" Type="Int64" />
                    </UpdateParameters>
                    <InsertParameters>
                        <asp:Parameter Name="unitateId" Type="Int32" />
                        <asp:Parameter Name="unitateDenumire" Type="String" />
                        <asp:Parameter Name="tip" Type="String" />
                        <asp:Parameter Name="data" Type="DateTime" />
                    </InsertParameters>
                </asp:SqlDataSource>
            </asp:Panel>
            <asp:Panel ID="pnButoane" CssClass="butoane" runat="server">
                <asp:Button CssClass="buton" ID="btnnAdauga" runat="server" Text="salveaza" OnClick="btAdauga_Click" />
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
</asp:Content>
