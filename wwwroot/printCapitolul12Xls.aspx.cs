﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class printCapitolul12Xls : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            int gospodarieId = Convert.ToInt32(Session["SESGospodarieId"]);
            int unitateId = Convert.ToInt32(Session["SESUnitateId"]);
            int an = Convert.ToInt32(Session["SESAn"]);
            DataTable reportDataTable = CertificateProducator.GetCertificate(unitateId, gospodarieId, an); ;
            DataTable unitatiDataTable = GetUnitateDenumire();
            DataTable gospodariiDataTable = GetGospodarie();
           
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource ds = new ReportDataSource("dsRapoarte_Cap12DataTable", reportDataTable);
            ReportDataSource ds2 = new ReportDataSource("dsRapoarte_dtUnitati", unitatiDataTable);
            ReportDataSource ds3 = new ReportDataSource("dsRapoarte_dtGospodarii", gospodariiDataTable);
            ReportViewer1.LocalReport.DataSources.Add(ds);
            ReportViewer1.LocalReport.DataSources.Add(ds2);
            ReportViewer1.LocalReport.DataSources.Add(ds3);
            ReportViewer1.LocalReport.Refresh();
            ReportViewer1.Visible = true;
        }
    }
    protected void Page_SaveStateComplete(object sender, EventArgs e)
    {
        Warning[] warnings;
        string[] streamids;
        string mimeType;
        string encoding;
        string fileNameExtension;
        byte[] bytes = ReportViewer1.LocalReport.Render("Excel", null, out mimeType, out encoding, out fileNameExtension, out streamids, out warnings);
        Response.Buffer = true;
        Response.Clear();
        Response.ContentType = mimeType;
        Response.AddHeader("content-disposition", "inline; filename=Capitol12." + fileNameExtension);
        Response.BinaryWrite(bytes);
        Response.Flush();
        Response.End();
        Response.Close();
    }
    private DataTable GetCapitole()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        string interogare = @"SELECT     capitole.capitolId, capitole.unitateId, capitole.gospodarieId, capitole.an AS capitoleAn, capitole.codCapitol, capitole.codRand AS capitoleCodRand, 
                      CONVERT(decimal(18, 2), capitole.col1) AS col1, CONVERT(decimal(18, 2), capitole.col2) AS col2, CONVERT(decimal(18, 2), capitole.col3) AS col3, 
                      CONVERT(decimal(18, 2), capitole.col4) AS col4, CONVERT(decimal(18, 2), capitole.col5) AS col5, CONVERT(decimal(18, 2), capitole.col6) AS col6, 
                      CONVERT(decimal(18, 2), capitole.col7) AS col7, CONVERT(decimal(18, 2), capitole.col8) AS col8, sabloaneCapitole.sablonCapitoleId, 
                      sabloaneCapitole.an, sabloaneCapitole.capitol, sabloaneCapitole.codRand, sabloaneCapitole.formula, sabloaneCapitole.denumire1, 
                      sabloaneCapitole.denumire2, sabloaneCapitole.denumire3, sabloaneCapitole.denumire4, sabloaneCapitole.denumire5
FROM         capitole INNER JOIN
                      sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand
WHERE     (capitole.unitateId = " + Session["SESUnitateId"] + ") AND (capitole.gospodarieId = " + Session["SESGospodarieId"] + ") AND (capitole.an = " + Session["SESAn"] + ")  AND (sabloaneCapitole.an =  " + Session["SESAn"] + @") AND (capitole.codCapitol = '4a1')
ORDER BY capitoleCodRand";
        SqlDataAdapter adapter = new SqlDataAdapter(interogare, connection);
        DataTable table = new DataTable();
        adapter.Fill(table);
        ManipuleazaBD.InchideConexiune(connection);
        return table;
    }
    private DataTable GetGospodarie()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        string interogare = @"SELECT        gospodarieId, unitateId, volum, nrPozitie, codSiruta, tip, COALESCE (strada, '') AS strada, COALESCE (nr, '') AS nr, nrInt, COALESCE (bl, '') AS bl, 
                         COALESCE (sc, '') AS sc, COALESCE (et, '') AS et, COALESCE (ap, '') AS ap, codExploatatie, codUnic, judet, localitate, persJuridica, jUnitate, jSubunitate, 
                         jCodFiscal, jNumeReprez, strainas, sStrada, sNr, sBl, sSc, sEtj, sAp, sJudet, sLocalitate,
                             (SELECT        TOP (1) nume
                               FROM            membri
                               WHERE        (gospodarieId = gospodarii.gospodarieId)
                               ORDER BY codRudenie) AS Membru,
                             (SELECT        TOP (1) cnp
                               FROM            membri AS membri_1
                               WHERE        (gospodarieId = gospodarii.gospodarieId)
                               ORDER BY codRudenie) AS cnpMembru
FROM            gospodarii
where an = " + Session["SESAn"] + " and gospodarieId = " + Session["SESGospodarieid"] + "";
        SqlDataAdapter adapter = new SqlDataAdapter(interogare, connection);
        DataTable table = new DataTable();
        adapter.Fill(table);
        ManipuleazaBD.InchideConexiune(connection);
        return table;
    }

    private DataTable GetUnitateDenumire()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        string interogare = @"SELECT        unitati.unitateId, unitati.unitateDenumire, unitati.unitateCodSiruta, unitati.judetId, unitati.localitateId, unitati.unitateStrada, unitati.unitateNr, unitati.unitateAp, 
                         unitati.unitateCodPostal, unitati.localitateComponentaId, unitati.unitateCodFiscal, unitati.unitateActiva, unitati.unitatePrincipala, judete.judetDenumire, 
                         localitati.localitateDenumire
FROM            unitati INNER JOIN
                         judete ON unitati.judetId = judete.judetId INNER JOIN
                         localitati ON unitati.localitateId = localitati.localitateId
WHERE        (unitati.unitateId = " + Session["SESunitateId"].ToString() + ")";
        SqlDataAdapter adapter = new SqlDataAdapter(interogare, connection);
        DataTable table = new DataTable();
        adapter.Fill(table);
        ManipuleazaBD.InchideConexiune(connection);
        return table;
    }

}