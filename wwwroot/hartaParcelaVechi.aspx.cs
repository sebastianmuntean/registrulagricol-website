﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SharpMap.Geometries;
using System.Data.SqlClient;
using System.Drawing;

public partial class hartaParcelaVechi : System.Web.UI.Page
{
    private SharpMap.Map myMap;
    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "harta", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        List<SharpMap.Geometries.Point> vPuncte = new List<SharpMap.Geometries.Point>();
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select coordonataX, coordonataY from coordonate where parcelaId='" + Request.QueryString["parcelaId"].ToString() + "' order by coordonataId";
        SqlDataReader vTabel = vCmd.ExecuteReader();
        List<double> vListaCoordonate = new List<double>();
        while (vTabel.Read())
        {
            double vX = Convert.ToDouble(vTabel["coordonataX"].ToString());
            double vY = Convert.ToDouble(vTabel["coordonataY"].ToString());
            vPuncte.Add(new SharpMap.Geometries.Point(vX, vY));
        }
        vTabel.Close();
        // aici voi face zoom pe harta (daca sunt 4 colturi stiu cum, daca nu fac o formula sa ajung la coturile extreme pentru a scoate maximul de margini)
        vCmd.CommandText = "select count(*) from coordonate where parcelaId='" + Request.QueryString["parcelaId"].ToString() + "'";
        Int32 vNrColturi = Convert.ToInt32(vCmd.ExecuteScalar());
        ManipuleazaBD.InchideConexiune(vCon);
        // Creeaza harta
        myMap = InitializeMap(new System.Drawing.Size((int)imgMap.Width.Value, (int)imgMap.Height.Value), vPuncte);
        // vizualizarea initiala a hartii
        if (vNrColturi == 4)
        {
            BoundingBox vBx = new BoundingBox(vPuncte.ElementAt(0), vPuncte.ElementAt(2));
            myMap.ZoomToBox(vBx);
        }
        else
            myMap.ZoomToExtents();
        // mememorez in viewstate pozitia curenta
        ViewState.Add("mapCenter", myMap.Center);
        ViewState.Add("mapZoom", myMap.Zoom);
        // creaza harta
        CreareHarta();
        //}
    }
    /// <summary>
    /// genereaza harta si aseaza layer-ele
    /// </summary>
    /// <param name="outputsize">se initializeaza cu dimensiunea imaginii</param>
    /// <returns>obiecte de tipul harta</returns>
    private SharpMap.Map InitializeMap(System.Drawing.Size outputsize, List<SharpMap.Geometries.Point> pPuncte)
    {
        // initializeaza o harta cu dimensiunile de la parametru
        SharpMap.Map map = new SharpMap.Map(outputsize);
        // iau codul fiscal la unitatea curenta
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select top(1) unitateCodFiscal from unitati where unitateId='" + Session["SESunitateId"].ToString() + "'";
        string vCodFiscal = vCmd.ExecuteScalar().ToString();
        ManipuleazaBD.InchideConexiune(vCon);
        // layer principal
        SharpMap.Layers.GdalRasterLayer layerPrincipal = new SharpMap.Layers.GdalRasterLayer("Principal", Server.MapPath(@"~\Harti\" + vCodFiscal + "\\harta.sid"));
        // adaug cate 100 puncte la initial pentru a face harta mai mica
        //SharpMap.Layers.GdalRasterLayer layerPrincipal1 = new SharpMap.Layers.GdalRasterLayer("Principal1", Server.MapPath(@"~\App_data\L_34_96_A_d_1_I.tif"));
        //SharpMap.Geometries.Point vPt = new SharpMap.Geometries.Point(409972.14, 478406);
        //SharpMap.Geometries.Point vPt1 = new SharpMap.Geometries.Point(420979.31, 482043.7);
        LinearRing vRingLiniar = new LinearRing(pPuncte);
        SharpMap.Geometries.Polygon vPoligon = new SharpMap.Geometries.Polygon(vRingLiniar);
        //???????
        SharpMap.Data.Providers.GeometryProvider _GeometryProvider = new SharpMap.Data.Providers.GeometryProvider(vPoligon);

        //Set GeometryProvider to VectorLayer's datasource
        SharpMap.Layers.VectorLayer _VectorLayer = new SharpMap.Layers.VectorLayer("myLayer", _GeometryProvider);

        //Set colors and style
        _VectorLayer.Style.Fill = new SolidBrush(Color.Transparent);
        _VectorLayer.Style.EnableOutline = true;
        _VectorLayer.Style.Outline = new Pen(Color.Red);

        // adauga layer-ele pe harta
        // ordinea in care sunt adaugate este ordinea in care au fost create
        map.Layers.Add(layerPrincipal);
        //map.Layers.Add(layerPrincipal1);
        map.Layers.Add(_VectorLayer);
        return map;
    }
    /// <summary>
    /// creaza harta, o adauga in cache si genereaza un url pentru imagine
    /// </summary>
    private void CreareHarta()
    {
        //try
        //{
            System.Drawing.Image img = myMap.GetMap();
            string imgID = SharpMap.Web.Caching.InsertIntoCache(1, img);
            imgMap.ImageUrl = "getmap.aspx?ID=" + HttpUtility.UrlEncode(imgID);
        //}
        //catch { }
    }
}
