﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="mesajeAdmin.aspx.cs" Inherits="mesajeAdmin" EnableEventValidation="false"
    Culture="ro-RO" UICulture="ro-RO" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Administrare / Mesaje" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="pnAfiseazaMesaje" runat="server">
        <asp:ValidationSummary ID="valCapitole" runat="server" DisplayMode="SingleParagraph"
            Visible="true" CssClass="validator" ForeColor="" />
    </asp:Panel>
    <asp:Panel ID="Panel5" CssClass="ascunsa" runat="server">
        <!-- //////////// VALIDATOARE  \\\\\\\\\\\\\\\\\\ -->
        <asp:CustomValidator ID="valCapitol" runat="server" ErrorMessage=""></asp:CustomValidator>
    </asp:Panel>
    <asp:Panel ID="pnListaMesaje" runat="server" CssClass="panel_general" Visible="true">
        <asp:Panel ID="pnCautare" runat="server" CssClass="cauta">
            <asp:Label ID="lblFDeLaUnitatea" runat="server" Text="De la unitatea"></asp:Label>
            <asp:DropDownList ID="ddlFDeLaUnitatea" runat="server" AutoPostBack="true" Width="150"
                OnInit="ddlFDeLaUnitatea_Init" OnSelectedIndexChanged="ddlFDeLaUnitatea_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:Label ID="lblFDeLaJudet" runat="server" Text="Din judeţul"></asp:Label>
            <asp:DropDownList ID="ddlFDeLaJudet" AutoPostBack="true" OnPreRender="ddlFDeLaJudet_PreRender"
                OnSelectedIndexChanged="ddlFDeLaJudet_SelectedIndexChanged" runat="server">
            </asp:DropDownList>
            <asp:Label ID="lblFLaUnitatea" runat="server" Text="La unitatea"></asp:Label>
            <asp:DropDownList ID="ddlFLaUnitatea" runat="server" AutoPostBack="true" OnInit="ddlFLaUnitatea_Init"
                OnSelectedIndexChanged="ddlFLaUnitatea_SelectedIndexChanged" Width="150">
            </asp:DropDownList>
            <asp:Label ID="lblFLaJudet" runat="server" Text="Din judeţul"></asp:Label>
            <asp:DropDownList ID="ddlFLaJudet" AutoPostBack="true" runat="server" OnPreRender="ddlFLaJudet_PreRender"
                OnSelectedIndexChanged="ddlFLaJudet_SelectedIndexChanged">
            </asp:DropDownList>
        </asp:Panel>
        <asp:Panel ID="pnGrid" runat="server" Visible="true">
            <asp:GridView ID="gvMesaje" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                BorderStyle="None" CellPadding="3" GridLines="Vertical" ShowFooter="True" CssClass="tabela"
                EmptyDataText="Nu sunt adaugate inregistrari" AllowPaging="True" DataKeyNames="mesajId"
                DataSourceID="SqlMesaje" OnRowDataBound="gvMesaje_RowDataBound" OnSelectedIndexChanged="gvMesaje_SelectedIndexChanged">
                <Columns>
                    <asp:BoundField DataField="unitateDenumireDeLa" HeaderText="De la unitatea" SortExpression="unitateDenumireDeLa" />
                    <asp:BoundField DataField="utilizatorNumePrenumeDeLa" HeaderText="De la utilizatorul"
                        ReadOnly="True" SortExpression="utilizatorNumePrenumeDeLa" />
                    <asp:BoundField DataField="unitateDenumireLa" HeaderText="La unitatea" SortExpression="unitateDenumireLa" />
                    <asp:BoundField DataField="utilizatorNumePrenumeLa" HeaderText="La utilizatorul"
                        SortExpression="utilizatorNumePrenumeLa" />
                    <asp:BoundField DataField="subiect" HeaderText="Subiect" SortExpression="subiect" />
                    <asp:BoundField DataField="dataAdaugarii" HeaderText="Data adăugării" SortExpression="dataAdaugarii" />
                    <asp:BoundField DataField="dataCitirii" HeaderText="Data citirii" SortExpression="dataCitirii" />
                </Columns>
                <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                <HeaderStyle Font-Bold="True" />
            </asp:GridView>
            <asp:SqlDataSource ID="SqlMesaje" runat="server" 
                SelectCommand="SELECT mesaje.mesajId, mesaje.dataAdaugarii, mesaje.dataCitirii, mesaje.subiect, utilizatori.utilizatorNume + ' ' + utilizatori.utilizatorPrenume AS utilizatorNumePrenumeDeLa, unitati.unitateDenumire AS unitateDenumireDeLa, unitati_1.unitateDenumire AS unitateDenumireLa, utilizatori_1.utilizatorNume + ' ' + utilizatori_1.utilizatorPrenume AS utilizatorNumePrenumeLa FROM mesaje INNER JOIN utilizatori ON mesaje.utilizatorIdDeLa = utilizatori.utilizatorId INNER JOIN unitati ON utilizatori.unitateId = unitati.unitateId INNER JOIN utilizatori AS utilizatori_1 ON mesaje.utilizatorId = utilizatori_1.utilizatorId INNER JOIN unitati AS unitati_1 ON utilizatori_1.unitateId = unitati_1.unitateId WHERE (CONVERT (nvarchar, unitati.unitateId) LIKE @unitateIdDeLa) AND (CONVERT (nvarchar, unitati.judetId) LIKE @judetIdDeLa) AND (CONVERT (nvarchar, unitati_1.unitateId) LIKE @unitateIdLa) AND (CONVERT (nvarchar, unitati_1.judetId) LIKE @judetIdLa) ORDER BY mesaje.dataAdaugarii DESC">
                <SelectParameters>
                    <asp:ControlParameter ControlID="ddlFDeLaUnitatea" Name="unitateIdDeLa" PropertyName="SelectedValue" />
                    <asp:ControlParameter ControlID="ddlFDeLaJudet" Name="judetIdDeLa" PropertyName="SelectedValue" />
                    <asp:ControlParameter ControlID="ddlFLaUnitatea" Name="unitateIdLa" PropertyName="SelectedValue" />
                    <asp:ControlParameter ControlID="ddlFLaJudet" Name="judetIdLa" PropertyName="SelectedValue" />
                </SelectParameters>
            </asp:SqlDataSource>
        </asp:Panel>
        <asp:Panel ID="pnButoane" CssClass="butoane" runat="server">
            <asp:Button CssClass="buton" ID="btModifica" Visible="false" runat="server" Text="modifică mesaj"
                OnClick="btModifica_Click" />
            <asp:Button CssClass="buton" ID="btSterge" Visible="false" runat="server" Text="șterge mesaj"
                OnClientClick="return confirm (&quot;Sigur ștergeți ?&quot;)" OnClick="btSterge_Click" />
            <asp:Button CssClass="buton" ID="btAdauga" runat="server" Text="adaugă mesaj" OnClick="btAdauga_Click" />
        </asp:Panel>
        <!-- adaugam unitate -->
        <asp:Panel ID="pnAdaugaMesaj" CssClass="panel_general" runat="server" Visible="false">
            <asp:Panel ID="Panel1" CssClass="adauga" runat="server">
                <asp:Panel ID="pnEroare" runat="server">
                    <h2>
                        <asp:Label ID="lblAdaugaMesaj" runat="server" Text="ADAUGĂ MESAJ"></asp:Label></h2>
                    <asp:Label ID="lblEroare" runat="server" CssClass="validator" Text="Eroare" Visible="false" />
                    <asp:ValidationSummary ID="valSumUnitati" runat="server" DisplayMode="SingleParagraph"
                        Visible="true" ValidationGroup="GrupValidareSabloaneCapitole" CssClass="validator"
                        ForeColor="" />
                </asp:Panel>
                <asp:Panel ID="Panel3" runat="server">
                    <p>
                        <asp:Label ID="lblTrimiteLa" runat="server" Text="Trimite la"></asp:Label>
                    </p>
                    <asp:UpdatePanel ID="upUtilizatori" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnUtilizatori" CssClass="lista_categorii" ScrollBars="Vertical" Height="200px"
                                Width="900px" runat="server">
                                <p style="width: 100%; border-bottom: solid 1px #5A540B; padding-bottom: 2px;">
                                    <asp:LinkButton ID="lbSelectatiTot" runat="server" OnClick="lbSelectatiTot_Click">Selectați tot</asp:LinkButton>
                                </p>
                                <p style="width: 100%; border-bottom: solid 1px #5A540B; padding-bottom: 2px;">
                                    <asp:Label ID="lblFJudet" runat="server" Text="Judeţ"></asp:Label>
                                    <asp:DropDownList ID="ddlFJudet" AutoPostBack="true" runat="server" OnPreRender="ddlFJudet_PreRender"
                                        OnSelectedIndexChanged="ddlFJudet_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <asp:Label ID="lblUnitate" CssClass="paddingLeft" runat="server" Text="Alege unitatea"></asp:Label>
                                    <asp:DropDownList ID="ddlFUnitate" runat="server" OnInit="ddlFUnitate_Init" AutoPostBack="true"
                                        OnSelectedIndexChanged="ddlFUnitate_SelectedIndexChanged" Width="150">
                                    </asp:DropDownList>
                                    <asp:Label ID="lblFUUtilizatori" runat="server" Text="Nume utilizator"></asp:Label>
                                    <asp:TextBox ID="tbFUUtilizator" Width="200px" AutoPostBack="true" runat="server"></asp:TextBox>
                                </p>
                                <asp:CheckBoxList ID="cblUtilizatori" runat="server" DataSourceID="SqlUtilizatori"
                                    DataTextField="denumire" DataValueField="utilizatorId" RepeatColumns="3" RepeatDirection="Horizontal">
                                </asp:CheckBoxList>
                                <asp:SqlDataSource ID="SqlUtilizatori" runat="server" 
                                    SelectCommand="SELECT unitati.unitateDenumire + ' - ' + utilizatori.utilizatorNume + ' ' + utilizatori.utilizatorPrenume AS denumire, utilizatori.utilizatorId FROM unitati INNER JOIN utilizatori ON unitati.unitateId = utilizatori.unitateId WHERE (unitati.unitateDenumire + ' - ' + utilizatori.utilizatorNume + ' ' + utilizatori.utilizatorPrenume LIKE '%' + @nume + '%') AND (CONVERT (nvarchar, unitati.judetId) LIKE @judetId) AND (CONVERT (nvarchar, unitati.unitateId) LIKE @unitateId) ORDER BY unitati.unitateDenumire, utilizatori.utilizatorNume, utilizatori.utilizatorPrenume">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="tbFUUtilizator" DefaultValue="%" Name="nume" PropertyName="Text" />
                                        <asp:ControlParameter ControlID="ddlFJudet" DefaultValue="" Name="judetId" PropertyName="SelectedValue" />
                                        <asp:ControlParameter ControlID="ddlFUnitate" Name="unitateId" PropertyName="SelectedValue" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <p>
                        <asp:Label ID="lblSubiect" runat="server" Text="Subiect"></asp:Label>
                        <asp:TextBox ID="tbSubiect" Width="500px" runat="server"></asp:TextBox>
                    </p>
                    <p>
                        <asp:Label ID="lblMesaj" runat="server" Text="Mesaj"></asp:Label>
                    </p>
                    <p>
                        <asp:TextBox ID="tbMesaj" runat="server" TextMode="MultiLine" Rows="20" Width="800px"></asp:TextBox>

                        <script type="text/javascript">
                        //<![CDATA[

                        CKEDITOR.replace('ctl00_ContentPlaceHolder1_tbMesaj',
					{
					    contentsCss: ['http://www.legex.ro/styles/mo.css', CKEDITOR.basePath + 'mo.css'],
					    fullPage: true,
					    	height : 300,
					    	
					    
					});

                        //]]>
                        </script>

                    </p>
                </asp:Panel>
                <asp:Panel ID="PanelValidatoareAscuns" CssClass="ascunsa" runat="server">
                    <!-- //////////// VALIDATOARE  \\\\\\\\\\\\\\\\\\ -->
                    <!-- //////////// VALIDATOARE  \\\\\\\\\\\\\\\\\\ -->
                </asp:Panel>
                <asp:Panel ID="pnButoaneAdaugare" runat="server" CssClass="butoane">
                    <asp:Button ID="btSalveazaMesaj" runat="server" CssClass="buton" Text="adauga un mesaj"
                        ValidationGroup="GrupValidareSabloaneCapitole" OnClick="btSalveazaMesaj_Click" />
                    <asp:Button ID="btAnuleazaMesaj" runat="server" CssClass="buton" Text="listă mesaje"
                        OnClick="btAnuleazaMesaj_Click" />
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
        <asp:Panel ID="pnDetaliiMesaj" CssClass="panel_general" runat="server" Visible="false">
            <asp:Panel ID="Panel2" CssClass="adauga" runat="server">
                <asp:Panel ID="Panel4" runat="server">
                    <h2>
                        DETALII MESAJ</h2>
                    <p>
                        <asp:Label ID="lblMesajText" runat="server" Text=""></asp:Label></p>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
</asp:Content>
