﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
/// <summary>
/// UNITATI
/// Creata la:                  26.01.2011
/// Autor:                      Sebastian Muntean
///     ultima actualizare          26.01.2012
///     utilizator                  SM
///     modificare                  introducere si anDeschis la creare unitate, din SESan
/// </summary>
public partial class Unitati : System.Web.UI.Page
{
    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion
    string vUnitateIdPrincipala = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        DBConnections connection = new DBConnections(Convert.ToInt16(Session["SESan"]));

        SqlUnitati.ConnectionString = connection.Create();

        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "unitati", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
            // butoanele din lista
            btAdaugaUnitate.Visible = true;
            btModificaUnitate.Visible = false;
            btStergeUnitate.Visible = false;
        }
        // daca nu e utilizator admin nu poate accesa pagina
        /*if (Redirect.RedirectUtilizator(Session["SESutilizatorId"].ToString()) == 1)
        {
            Response.Redirect("~/NuAvetiDrepturi.aspx");
        }*/

        // paneluri
        /*pnListaUnitati.Visible = true;
        pnAdaugaUnitate.Visible = false;
        // butoanele din lista
        btAdaugaUnitate.Visible = true;
        btModificaUnitate.Visible = false;
        btStergeUnitate.Visible = false;
        // butoanele din adauga
        btAdaugaModificaUnitate.Visible = false;
        btAdaugaArataListaUnitati.Visible = true;
        btModificaArataListaUnitati.Visible = false;
        btAdaugaUnitate.Visible = true;*/

    }
    protected void AdaugaModificaUnitate(object sender, EventArgs e)
    {
        // verificam sa nu fie campul de Denumire gol sau sa existe deja
        // la fel pt CIF
        string vInterogare = ""; int vVarianta = 0;
        int safa = pnAdaugaUnitate.Controls.Count;
        if (((Button)sender).ID.ToString() == "btAdaugaUnitate")
        {
            vInterogare = "INSERT INTO unitati (unitateDenumire, unitateCodSiruta,  localitateId, unitateCodFiscal, unitateStrada, unitateNr, unitateAp, judetId, unitateCodPostal, unitateActiva, unitatePrincipala, unitateLocalitateRang, unitateAdaugaDinC2B, unitateAdaugaDinC2BC4abc, unitateAdaugaDinC2BC3, unitateFocusLaEnter, unitateAdaugaAnimale, unitateAnDeschis, unitateTipAfisareStrainas,valideazaVolumSiPozitie) VALUES (N'" + tbDenumireUnitate.Text + "', '" + tbCodSiruta.Text + "', N'" + ddLocalitate.SelectedValue + "', '" + tbCIF.Text + "', N'" + tbStrada.Text + "', N'" + tbNumar.Text + "', N'" + tbAp.Text + "', '" + ddJudet.SelectedValue + "', '" + tbCodPostal.Text + "', '" + ddUnitateActiva.SelectedValue + "', '100', N'" + ddLocalitateRang.SelectedValue + "', '" + cbAdaugaDinC2B.Checked.ToString() + "', '" + cbAdaugaDinC2BxC4abc.Checked.ToString() + "', '" + cbAdaugaDinC2BxC3.Checked.ToString() + "', '" + cbFocusLaEnter.Checked.ToString() + "','" + cbAdaugaAnimale.Checked.ToString() + "', '" + Session["SESan"].ToString() + "', '" + ddlTipAfisareStrainas.SelectedValue + "','" + valideazaVolumSiPozitieCheckBox.Checked + "' )";
            vVarianta = 1;

        }
        else
        {
            vInterogare = "UPDATE unitati SET unitateDenumire =N'" + tbDenumireUnitate.Text + "' , unitateCodSiruta = '" + tbCodSiruta.Text + "',  localitateId='" + ddLocalitate.SelectedValue + "', unitateCodFiscal='" + tbCIF.Text + "', unitateStrada=N'" + tbStrada.Text + "', unitateNr=N'" + tbNumar.Text + "', unitateAp=N'" + tbAp.Text + "', judetId='" + ddJudet.SelectedValue + "', unitateCodPostal='" + tbCodPostal.Text + "', unitateActiva='" + ddUnitateActiva.SelectedValue + "', unitateLocalitateRang=N'" + ddLocalitateRang.SelectedValue + "', unitateAdaugaDinC2B = '" + cbAdaugaDinC2B.Checked.ToString() + "', unitateAdaugaDinC2BC4abc = '" + cbAdaugaDinC2BxC4abc.Checked.ToString() + "', unitateAdaugaDinC2BC3 = '" + cbAdaugaDinC2BxC3.Checked.ToString() + "', unitateFocusLaEnter = '" + cbFocusLaEnter.Checked.ToString() + "',  unitateAdaugaAnimale ='" + cbAdaugaAnimale.Checked.ToString() + "', unitateTipAfisareStrainas = '" + ddlTipAfisareStrainas.SelectedValue + "',valideazaVolumSiPozitie='" + valideazaVolumSiPozitieCheckBox.Checked + "' WHERE unitateId='" + lblUnitateId.Text + "'";
            vVarianta = 2;
        }
        //string[] vCampuri = new string[]{"unitateDenumire", "unitateCodFiscal"}; astea sunt pentru functia cu array... mai incolo
        //string[] vValoriCampuri = new string[]{tbDenumireUnitate.Text, tbCIF.Text};

        // facem verificari in baza de date:

        string vMesaj = "";
        int vTrece = 0;
        Valideaza.DimensiuneString(lblCIF.Text, tbCIF.Text, 7, 8, out vMesaj, out vTrece);
        if (vTrece == 0)
        {
            valCustDenumireUnitate.ErrorMessage = vMesaj;
            valCustDenumireUnitate.IsValid = false;
            pnListaUnitati.Visible = false;
            pnAdaugaUnitate.Visible = true;
            return;

        }
        int vVerificaUnitateDenumire = ManipuleazaBD.fVerificaExistenta("unitati", "unitateDenumire", tbDenumireUnitate.Text, Convert.ToInt16(Session["SESan"]));
        int vVerificaCIF = ManipuleazaBD.fVerificaExistenta("unitati", "unitateCodFiscal", tbCIF.Text, Convert.ToInt16(Session["SESan"]));

        if (tbDenumireUnitate.Text.Length > 1 && (vVerificaCIF != 1 || lblCifOriginal.Text == tbCIF.Text) && (lblDenumireUnitateOriginala.Text == tbDenumireUnitate.Text || vVerificaUnitateDenumire != 1))
        {
            int vUnitateId = 0;
            if (vVarianta == 1)
            {
                ManipuleazaBD.fManipuleazaBDCuId(vInterogare, out vUnitateId, Convert.ToInt16(Session["SESan"]));
                clsUnitati.AdaugaInCapitoleCentralizate(vUnitateId.ToString(), Session["SESunitateId"].ToString());
            }
            else
                ManipuleazaBD.fManipuleazaBD(vInterogare, Convert.ToInt16(Session["SESan"]));
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "unitati", "| adaugare/modificare unitate: " + tbCIF.Text, "", Convert.ToInt64(Session["SESgospodarieId"]), 2);
            gvUnitati.DataBind();
            if (vVarianta == 1)
            { gvUnitati.SelectedIndex = -1; }
            else
            {
                btModificaUnitate.Visible = true;
                btStergeUnitate.Visible = true;
            }
            pnListaUnitati.Visible = true;
            pnAdaugaUnitate.Visible = false;
        }
        else if (vVerificaUnitateDenumire == 1 && lblDenumireUnitateOriginala.Text != tbDenumireUnitate.Text)
        {
            valCustDenumireUnitate.ErrorMessage = "Există deja o unitate cu acest nume. Dacă doriți să modificați datele acesteia, căutați-o în lista de unități, o selectați și apăsați Modifică.";
            valCustDenumireUnitate.IsValid = false;
            pnListaUnitati.Visible = false;
            pnAdaugaUnitate.Visible = true;

            if (vVarianta == 1)
            { }
            else
            {
                btAdaugaModificaUnitate.Visible = true;
                btAdaugaUnitate.Visible = false;
            }
        }
        else if (vVerificaCIF == 1)
        {
            valCustDenumireUnitate.ErrorMessage = "Acest cod fiscal aparține unei alte unități deja înscrise.";
            valCustDenumireUnitate.IsValid = false;
            pnListaUnitati.Visible = false;
            pnAdaugaUnitate.Visible = true;
        }
        else
        {
            valCustDenumireUnitate.ErrorMessage = "Denumirea unității trebuie sa aibă cel puțin 2 litere! ";
            valCustDenumireUnitate.IsValid = false;
            pnListaUnitati.Visible = false;
            pnAdaugaUnitate.Visible = true;
            if (vVarianta == 1)
            { }
            else
            {
                btAdaugaModificaUnitate.Visible = true;
                btAdaugaUnitate.Visible = false;
            }
        }
    }
    protected void InitializeazaCampuri()
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select top(1) tipUtilizatorId from utilizatori where utilizatorId='" + Session["SESutilizatorId"].ToString() + "'";
        int vTipUtilizator = Convert.ToInt32(vCmd.ExecuteScalar());
        if (vTipUtilizator == 2 || vTipUtilizator == 3 || vTipUtilizator == 5)
            ddJudet.Enabled = false;
        else ddJudet.Enabled = true;
        ddJudet.DataBind();
        vCmd.CommandText = "SELECT TOP (1) unitati.judetId FROM utilizatori INNER JOIN unitati ON utilizatori.unitateId = unitati.unitateId WHERE (utilizatori.utilizatorId = '" + Session["SESutilizatorId"].ToString() + "')";
        ddJudet.SelectedValue = vCmd.ExecuteScalar().ToString();
        ManipuleazaBD.InchideConexiune(vCon);
        lblUnitateId.Text = "";
        tbDenumireUnitate.Text = "";
        tbCIF.Text = "";
        tbCodSiruta.Text = "";
        // ddJudet.SelectedValue = "0";
        // ddLocalitate.SelectedValue = "";
        ddUnitateActiva.SelectedValue = "1";
        ddlTipAfisareStrainas.SelectedIndex = -1;
        tbStrada.Text = "";
        tbNumar.Text = "";
        tbAp.Text = "";
        tbCodPostal.Text = "";
        cbFocusLaEnter.Checked = false;
        pnAdaugaUnitate.Visible = true;
        cbAdaugaDinC2B.Checked = false;
        cbAdaugaDinC2BxC3.Checked = false;
        cbAdaugaDinC2BxC4abc.Checked = false;
        cbAdaugaAnimale.Checked = false;
    }
    protected void gvUnitati_DataBound(object sender, EventArgs e)
    {
        for (int i = 0; i < gvUnitati.Rows.Count; i++)
        {
            if (gvUnitati.Rows[i].RowType == DataControlRowType.DataRow)
            {
                try
                {
                    int vJudetId = Convert.ToInt32(gvUnitati.Rows[i].Cells[3].Text);
                    int vLocalitateId = Convert.ToInt32(gvUnitati.Rows[i].Cells[4].Text);
                    string vInterogare = "SELECT judetDenumire FROM judete WHERE judetId='" + vJudetId.ToString() + "'";
                    gvUnitati.Rows[i].Cells[3].Text = SelectLocalitatiJudete(vInterogare, "judetDenumire", Convert.ToInt16(Session["SESan"]));
                    vInterogare = "SELECT localitateDenumire FROM localitati WHERE localitateId='" + vLocalitateId.ToString() + "'";
                    gvUnitati.Rows[i].Cells[4].Text = SelectLocalitatiJudete(vInterogare, "localitateDenumire", Convert.ToInt16(Session["SESan"]));
                    int vActiv = Convert.ToInt32(gvUnitati.Rows[i].Cells[10].Text);
                    if (vActiv == 1) { gvUnitati.Rows[i].Cells[10].Text = "da"; } else { gvUnitati.Rows[i].Cells[10].Text = "nu"; }
                }
                catch { }
            }
        }
    }
    public static string SelectLocalitatiJudete(string pInterogare, string pCamp, short an)
    {
        SqlConnection con = ManipuleazaBD.CreareConexiune(an);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = pInterogare;
        con.Open();
        string vDenumire = "---";
        try
        {
            SqlDataReader vLista = cmd.ExecuteReader();
            if (vLista.Read()) { vDenumire = vLista[pCamp].ToString(); }
            vLista.Close();
        }
        catch
        {
        }
        con.Close();
        return vDenumire;
    }
    protected void ModificaArataListaUnitati(object sender, EventArgs e)
    {
        // avem doua butoane de lista - tratam click pe "lista" fara sa adauge
        // paneluri
        pnListaUnitati.Visible = true;
        btModificaUnitate.Visible = true;
        btStergeUnitate.Visible = true;
        pnAdaugaUnitate.Visible = false;
    }
    protected void AdaugaArataListaUnitati(object sender, EventArgs e)
    {

        // paneluri
        pnListaUnitati.Visible = true;
        btModificaUnitate.Visible = false;
        btStergeUnitate.Visible = false;
        pnAdaugaUnitate.Visible = false;
        gvUnitati.SelectedIndex = -1;

    }
    protected void ArataAdaugaUnitati(object sender, EventArgs e)
    {
        // paneluri
        InitializeazaCampuri();
        pnAdaugaUnitate.Visible = true;
        pnListaUnitati.Visible = false;
        upUnitati.DataBind();
    }
    protected void StergeUnitate(object sender, EventArgs e)
    {
        string vUnitateId = gvUnitati.SelectedValue.ToString();
        if (ManipuleazaBD.fVerificaExistenta("LogOperatii", "unitateId", vUnitateId, Convert.ToInt16(Session["SESan"])) == 0)
        {
            ManipuleazaBD.fManipuleazaBD("DELETE FROM [unitati] WHERE [unitateId] = '" + gvUnitati.SelectedValue.ToString() + "'", Convert.ToInt16(Session["SESan"]));
            ManipuleazaBD.fManipuleazaBD("DELETE FROM capitoleCentralizate WHERE [unitateId] = '" + gvUnitati.SelectedValue.ToString() + "'", Convert.ToInt16(Session["SESan"]));
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "unitati", "| sterge unitate/id: " + vUnitateId, "", Convert.ToInt64(Session["SESgospodarieId"]), 4);
            gvUnitati.DataBind();
            gvUnitati.SelectedIndex = -1;

        }
        else
        {
            string vExpresie = "Unitatea are activitate desfăşurată, prin urmare nu poate fi ştearsă!";
            valCustDenumireUnitate.IsValid = false;
            valCustDenumireUnitate.ErrorMessage = vExpresie;
            btModificaUnitate.Visible = true;
            btStergeUnitate.Visible = true;
        }

    }
    protected void gvUnitati_SelectedIndexChanged(object sender, EventArgs e)
    {
        btAdaugaUnitate.Visible = true;
        btModificaUnitate.Visible = true;
        btStergeUnitate.Visible = true;
        gvUnitati.DataBind();

    }
    protected void gvUnitati_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(gvUnitati, e, this);
    }
    protected void ArataPanelModificaUnitate(object sender, EventArgs e)
    {
        // venim din lista unitati si populam panelul de dauga modifica cu datele existente
        // paneluri
        pnListaUnitati.Visible = false;
        pnAdaugaUnitate.Visible = true;
        // butoanele din adauga/modifica
        btAdaugaModificaUnitate.Visible = true; // butonul de modifica
        btAdaugaUnitate.Visible = false; // butonul de adauga - ascundem
        btModificaArataListaUnitati.Visible = true;
        btAdaugaArataListaUnitati.Visible = false;
        // luam id-ul liniei selectate in GV:
        int vUnitateId = Convert.ToInt32(gvUnitati.SelectedValue);
        // populam cu datele selectate paneul de adaugare
        // facem un select pentru extragerea tuturor datelor  => id
        string vInterogareSelect = "SELECT * FROM unitati WHERE unitateId = " + vUnitateId;
        string strConn = ConfigurationManager.ConnectionStrings["TNTRegistruAgricolConnectionString"].ConnectionString;
        SqlConnection vCon = new SqlConnection(strConn);
        SqlCommand vCmd1 = new SqlCommand();
        SqlCommand vCmd2 = new SqlCommand();
        vCmd1.Connection = vCon;
        vCmd2.Connection = vCon;
        vCmd2.CommandType = System.Data.CommandType.Text;
        vCmd2.CommandText = vInterogareSelect;
        vCon.Open();
        try
        {
            SqlDataReader vLinieDeModificat = vCmd2.ExecuteReader();
            if (vLinieDeModificat.Read())
            {
                // populam fiecare camp
                lblUnitateId.Text = vUnitateId.ToString();

                tbDenumireUnitate.Text = vLinieDeModificat["unitateDenumire"].ToString();
                lblDenumireUnitateOriginala.Text = tbDenumireUnitate.Text; // salvam denumirea originala
                tbCIF.Text = vLinieDeModificat["unitateCodFiscal"].ToString();
                lblCifOriginal.Text = tbCIF.Text; // salvam CIF original
                tbCodSiruta.Text = vLinieDeModificat["unitateCodSiruta"].ToString();
                // daca utilizatorul este supervizor judetean sau functionar cu drepturi pe pagina il las numai pe judetul lui
                vCmd1.CommandText = "select top(1) tipUtilizatorId from utilizatori where utilizatorId='" + Session["SESutilizatorId"].ToString() + "'";
                int vTipUtilizator = Convert.ToInt32(vCmd1.ExecuteScalar());
                if (vTipUtilizator == 2 || vTipUtilizator == 3 || vTipUtilizator == 5)
                    ddJudet.Enabled = false;
                else ddJudet.Enabled = true;
                ddJudet.DataBind();
                ddJudet.SelectedValue = vLinieDeModificat["judetId"].ToString();
                ddLocalitate.DataBind();
                ddLocalitate.SelectedValue = vLinieDeModificat["localitateId"].ToString();
                ddUnitateActiva.SelectedValue = vLinieDeModificat["unitateActiva"].ToString();
                tbStrada.Text = vLinieDeModificat["unitateStrada"].ToString();
                tbNumar.Text = vLinieDeModificat["unitateNr"].ToString();
                tbAp.Text = vLinieDeModificat["unitateAp"].ToString();
                tbCodPostal.Text = vLinieDeModificat["unitateCodPostal"].ToString();
                cbAdaugaDinC2B.Checked = Convert.ToBoolean(vLinieDeModificat["unitateAdaugaDinC2B"].ToString());
                cbAdaugaDinC2BxC3.Checked = Convert.ToBoolean(vLinieDeModificat["unitateAdaugaDinC2BC3"].ToString());
                cbAdaugaDinC2BxC4abc.Checked = Convert.ToBoolean(vLinieDeModificat["unitateAdaugaDinC2BC4abc"].ToString());
                try
                {
                    cbAdaugaAnimale.Checked = Convert.ToBoolean(vLinieDeModificat["unitateAdaugaAnimale"].ToString());
                }
                catch { cbAdaugaAnimale.Checked = false; }
                cbFocusLaEnter.Checked = Convert.ToBoolean(vLinieDeModificat["unitateFocusLaEnter"].ToString());
                ddLocalitateRang.SelectedValue = vLinieDeModificat["unitateLocalitateRang"].ToString();
                ddlTipAfisareStrainas.SelectedValue = vLinieDeModificat["unitateTipAfisareStrainas"].ToString();
                valideazaVolumSiPozitieCheckBox.Checked = Convert.ToBoolean(vLinieDeModificat["valideazaVolumSiPozitie"].ToString());
            }
        }
        catch { }
        vCon.Close();
    }
    protected void valCustDenumireUnitate_ServerValidate(object source, ServerValidateEventArgs args)
    {
        try
        {
            if (ManipuleazaBD.fVerificaExistenta("unitati", "unitateDenumire", tbDenumireUnitate.Text, Convert.ToInt16(Session["SESan"])) == 1)
            { args.IsValid = false; }
        }
        catch
        {
            args.IsValid = false;
        }
    }
    protected void SqlUnitati_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        // daca utilizatorul este supervizor judetean sau functionar cu drepturi pe pagina are dreptul sa vizualizeze doar unitatile din judet
        vCmd.CommandText = "SELECT TOP (1) unitati.judetId FROM utilizatori INNER JOIN unitati ON utilizatori.unitateId = unitati.unitateId WHERE (utilizatori.utilizatorId = '" + Session["SESutilizatorId"].ToString() + "')";
        string vJudetId = vCmd.ExecuteScalar().ToString();
        vCmd.CommandText = "select top(1) tipUtilizatorId from utilizatori where utilizatorId='" + Session["SESutilizatorId"].ToString() + "'";
        int vTipUtilizator = Convert.ToInt32(vCmd.ExecuteScalar());
        ManipuleazaBD.InchideConexiune(vCon);
        if (vTipUtilizator == 1 || vTipUtilizator == 4)
            vJudetId = "%";
        e.Command.CommandText = "SELECT unitateId, unitateDenumire, unitateCodSiruta, judetId, localitateId, unitateStrada, unitateNr, unitateAp, unitateCodPostal, localitateComponentaId, unitateCodFiscal, unitateActiva, ROW_NUMBER() OVER(ORDER BY unitateDenumire ASC) AS RowNumber FROM unitati WHERE unitateDenumire LIKE (N'%" + ((tbCUnitateDenumire.Text == "") ? "%" : tbCUnitateDenumire.Text) + "%') AND (CONVERT(nvarchar,judetId) like '" + vJudetId + "') AND (CONVERT (nvarchar, judetId) LIKE '" + ddlCJudet.SelectedValue + "') order by unitateDenumire";
    }
    protected void ddlCJudet_PreRender(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select judetDenumire,judetId from judete order by judetDenumire";
        ddlCJudet.Items.Clear();
        ListItem vItem = new ListItem("-toate-", "%");
        ddlCJudet.Items.Add(vItem);
        SqlDataReader vTabel = vCmd.ExecuteReader();
        while (vTabel.Read())
        {
            vItem = new ListItem(vTabel["judetDenumire"].ToString(), vTabel["judetId"].ToString());
            ddlCJudet.Items.Add(vItem);
        }
        vTabel.Close();
        ManipuleazaBD.InchideConexiune(vCon);
        if (ViewState["judetId"] != null)
            ddlCJudet.SelectedValue = ViewState["judetId"].ToString();
        //gvUnitati.DataBind();
    }
    protected void ddlCJudet_SelectedIndexChanged(object sender, EventArgs e)
    {
        ViewState["judetId"] = ddlCJudet.SelectedValue;
        //gvUnitati.DataBind();
    }
}
