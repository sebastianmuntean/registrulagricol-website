﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Capitol2b.aspx.cs" Inherits="Capitol2b" UICulture="ro-Ro" Culture="ro-Ro"
    EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <%--<asp:Label ID="url" runat="server" Text="Capitole / Capitolul II: b) Identificarea pe parcele a terenurilor aflate în proprietatea gospodăriei/exploataţiei agricole fără personalitate juridică şi a unităţilor cu personalitate juridică" />--%>
    <asp:Label ID="url" runat="server" Text="Capitole / Capitolul II: b) Identificarea pe parcele a terenurilor aflate în proprietate" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="upParcele" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnAfiseazaMesaje" runat="server">
                <asp:ValidationSummary ID="valSumParcele" runat="server" DisplayMode="SingleParagraph" Visible="true" ValidationGroup="GrupValidareParcele" CssClass="validator" ForeColor="" />
            </asp:Panel>
            <!-- lista Parcele -->
            <asp:Panel ID="pnListaParcele" runat="server" CssClass="panel_general" Visible="true">
                <asp:Panel ID="pnCautare" runat="server" CssClass="cauta">
                    <asp:Label ID="lblCDenumire" runat="server" Text="Denumire" />
                    <asp:TextBox ID="tbCDenumire" runat="server" autocomplete="off" AutoPostBack="True" Width="100px" />
                    <asp:Label ID="lblCTopo" runat="server" Text="Topo" />
                    <asp:TextBox ID="tbCTopo" runat="server" autocomplete="off" AutoPostBack="True" Width="50px" />
                    <asp:Label ID="lblCCF" runat="server" Text="C.F." />
                    <asp:TextBox ID="tbCCF" runat="server" autocomplete="off" AutoPostBack="True" Width="80px" />
                    <asp:Label ID="lblCBloc" runat="server" Text="Bloc fizic" />
                    <asp:TextBox ID="tbCBloc" runat="server" autocomplete="off" AutoPostBack="True" Width="50px" />
                    <asp:Label ID="lblCNrCadastral" runat="server" Text="Cadastru" />
                    <asp:TextBox ID="tbCNrCadastral" runat="server" autocomplete="off" AutoPostBack="True" Width="50px" />
                    <asp:Label ID="lblCNrCadastralProvizoriu" runat="server" Text="Cadastru provizoriu" />
                    <asp:TextBox ID="tbCNrCadastralProvizoriu" runat="server" AutoPostBack="True" Width="50px" />
                </asp:Panel>
                <asp:Panel ID="Panel12" runat="server" CssClass="cauta">
                    <asp:Label ID="lblCCategorie" runat="server" Text="Categoria de folosinţă" />
                    <asp:DropDownList ID="ddlCCategoria" runat="server" DataSourceID="sdsCategoria" DataTextField="denumire4" DataValueField="codRand" Width="180" AutoPostBack="true" AppendDataBoundItems="True">
                        <asp:ListItem Value="%">toate categoriile</asp:ListItem>
                    </asp:DropDownList>
                    <asp:Label ID="lblNrPagina" runat="server" Text="Nr. parcele afişate pe pagină" />
                    <asp:DropDownList ID="ddlNrPagina" runat="server" Width="80" AutoPostBack="true" AppendDataBoundItems="True">
                        <asp:ListItem Value="10">10</asp:ListItem>
                        <asp:ListItem Value="20">20</asp:ListItem>
                        <asp:ListItem Value="50">50</asp:ListItem>
                        <asp:ListItem Value="%">toate</asp:ListItem>
                    </asp:DropDownList>
                </asp:Panel>
                <asp:Panel ID="pnParcele" runat="server">
                     <asp:Label runat="server" ID="Label5" Text="Parcele in proprietate" Font-Bold="true" Font-Size="Medium"/>
                    <asp:GridView ID="gvParcele" AllowPaging="True" DataKeyNames="parcelaId" AllowSorting="True" EmptyDataText ="Nu exista parcele in proprietate"
                        CssClass="tabela" runat="server" AutoGenerateColumns="False" EnableViewState="true"
                        DataSourceID="SqlParcele" OnRowDataBound="GvParceleRowDataBound" OnSelectedIndexChanged="GvParceleSelectedIndexChanged"
                        OnPreRender="GvParcelePreRender" ShowFooter="True">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Label ID="lblParcelaIdGv" Visible="False" runat="server" Text='<%# Eval("parcelaId") %>'></asp:Label>
                                    <asp:CheckBox ID="cbAfisare" runat="server" OnCheckedChanged="CbAlegeCheckedChanged" />
                                </ItemTemplate>
                                <HeaderTemplate>
                                    <asp:LinkButton ID="lbAlegeTot" runat="server" OnClick="LbAlegeTotClick">Alege tot</asp:LinkButton>
                                </HeaderTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="parcelaId" HeaderText="Nr." SortExpression="parcelaId" Visible="false" />
                            <asp:BoundField DataField="parcelaDenumire" HeaderText="Denumirea parcelei / tarlalei / solei" SortExpression="parcelaDenumire" />
                            <asp:BoundField DataField="parcelaCodRand" HeaderText="Cod rând" SortExpression="parcelaCodRand" Visible="false" />
                            <asp:BoundField DataField="parcelaSuprafataIntravilanHa" HeaderText="Intravilan Ha" SortExpression="parcelaSuprafataIntravilanHa"  DataFormatString="{0:F0}"/>
                            <asp:BoundField DataField="parcelaSuprafataIntravilanAri" HeaderText="Ari" SortExpression="parcelaSuprafataIntravilanAri"   DataFormatString="{0:F2}"/>
                            <asp:BoundField DataField="parcelaSuprafataExtravilanHa" HeaderText="Extravilan Ha" SortExpression="parcelaSuprafataExtravilanHa"   DataFormatString="{0:F0}"/>
                            <asp:BoundField DataField="parcelaSuprafataExtravilanAri" HeaderText="Ari" SortExpression="parcelaSuprafataExtravilanAri"  DataFormatString="{0:F2}" />
                            <asp:BoundField DataField="parcelaNrTopo" HeaderText="Nr.Topo" SortExpression="parcelaNrTopo" />
                            <asp:BoundField DataField="parcelaCF" HeaderText="C.F." SortExpression="parcelaCF" />
                            <asp:BoundField DataField="parcelaNrCadastral" HeaderText="Nr.Cadastral" SortExpression="parcelaNrCadastral" />
                            <asp:BoundField DataField="parcelaNrCadastralProvizoriu" HeaderText="Nr.Cad.Provizoriu" SortExpression="parcelaNrCadastralProvizoriu" />
                            <asp:BoundField DataField="denumire4" HeaderText="Categorie" SortExpression="denumire4" />
                            <asp:BoundField DataField="parcelaNrBloc" HeaderText="Bloc fizic" SortExpression="parcelaNrBloc" />
                            <asp:TemplateField HeaderText="Adresa" SortExpression="parcelaAdresa">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("parcelaLocalitate") + ", " + Eval("parcelaAdresa") %>'></asp:Label>
                                    <asp:Label ID="lblParcelaId" runat="server" Text='<%# Eval("parcelaId") %>' Visible="false"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="an" HeaderText="an" SortExpression="anul" Visible="false" />
                            <asp:BoundField DataField="parcelaTarla" HeaderText="Tarla" SortExpression="parcelaTarla" />
                            <asp:BoundField DataField="parcelaTitluProprietate" HeaderText="Titlu proprietate" SortExpression="parcelaTitluProprietate" />
                            <asp:BoundField DataField="parcelaClasaBonitate" HeaderText="Cls. bonitate" SortExpression="parcelaClasaBonitate" />
                            <asp:BoundField DataField="parcelaPunctaj" HeaderText="Punctaj" SortExpression="parcelaPunctaj" />
                            <asp:BoundField DataField="parcelaCodRand" HeaderText="Ordinea introducerii" SortExpression="parcelaCodRand" />
                            <asp:BoundField DataField="Proprietar" HeaderText="Titular drept proprietate" SortExpression="Proprietar" HtmlEncode="false"/>
                            <asp:TemplateField HeaderText="Situaţia cea mai recentă">
                                <ItemTemplate>
                                    <asp:Label ID="situatieLabel" Visible="True" runat="server" Text='<%# Eval("situatie") %>' ToolTip="Situaţie (suprafaţa dată către gospodărie (volum/poziţie); număr contract/ dată contract)"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                        <FooterStyle CssClass="footer" />
                        <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlParcele" runat="server" 
                        SelectCommand="SELECT       parcelaCodRand, 
                                                    parcele.parcelaId, 
                                                    COALESCE(parcele.parcelaDenumire,'') as parcelaDenumire, 
                                                    COALESCE(parcele.parcelaCodRand,'') as parcelaCodRand, 
                                                    COALESCE(parcele.parcelaSuprafataIntravilanHa,'0') as parcelaSuprafataIntravilanHa, 
                                                    COALESCE(parcele.parcelaSuprafataIntravilanAri,'0') as parcelaSuprafataIntravilanAri, 
                                                    COALESCE(parcele.parcelaSuprafataExtravilanHa,'0') as parcelaSuprafataExtravilanHa, 
                                                    COALESCE(parcele.parcelaSuprafataExtravilanAri,'0') as parcelaSuprafataExtravilanAri, 
                                                    COALESCE(parcele.parcelaNrTopo,'') as parcelaNrTopo, 
                                                    COALESCE(parcele.parcelaCategorie,'') as parcelaCategorie, 
                                                    COALESCE(parcele.parcelaCF,'') as parcelaCF, 
                                                    COALESCE (sabloaneCapitole.denumire4, N' --- ') AS denumire4, 
                                                    COALESCE(parcele.parcelaNrBloc,'') as parcelaNrBloc, 
                                                    COALESCE(parcele.parcelaMentiuni,'') as parcelaMentiuni, 
                                                    COALESCE(parcele.an,'') as an, 
                                                    COALESCE(parcele.parcelaLocalitate,'') as parcelaLocalitate,
                                                    COALESCE(membri.nume,'&lt;small&gt;&lt;i&gt;ne-precizat&lt;/i&gt;&lt;/small&gt;') as Proprietar, 
                                                    --COALESCE(membri.nume,gospodarii.membruNume) as Proprietar, 
                                                    COALESCE(parcele.parcelaAdresa,'') as parcelaAdresa, 
                                                    COALESCE(parcele.parcelaNrCadastralProvizoriu,'') as parcelaNrCadastralProvizoriu, 
                                                    COALESCE(parcele.parcelaNrCadastral,'') as parcelaNrCadastral, 
                                                    COALESCE(parcele.parcelaTarla,'') as parcelaTarla, 
                                                    COALESCE(parcele.parcelaTitluProprietate,'') as parcelaTitluProprietate, 
                                                    COALESCE(parcele.parcelaClasaBonitate,'0') as parcelaClasaBonitate, 
                                                    COALESCE(parcele.parcelaPunctaj,'0') as parcelaPunctaj,
                                                    '' as Situatie
                                        FROM        parcele 
                                        INNER JOIN  sabloaneCapitole ON parcele.parcelaCategorie = sabloaneCapitole.codRand 
                                        LEFT JOIN   membri on membri.capitolId = parcele.proprietarId 
                                        INNER JOIN  gospodarii on gospodarii.gospodarieId = parcele.gospodarieId
                                        WHERE       (sabloaneCapitole.capitol = '2a') 
                        AND (parcele.gospodarieId = @gospodarieId) 
                        AND (parcele.an = @an) AND (sabloaneCapitole.an = @an) 
                        AND (parcele.parcelaCategorie LIKE @parcelaCategorie )  
                        AND (parcele.parcelaCF LIKE '%' + @parcelaCF + '%') 
                        AND (parcele.parcelaDenumire LIKE '%'+@parcelaDenumire+'%') 
                        AND (parcele.parcelaNrBloc LIKE '%' + @parcelaNrBloc + '%') 
                        AND (parcele.parcelaNrTopo LIKE '%' + @parcelaNrTopo + '%') 
                        AND (parcele.parcelaNrCadastral LIKE '%' + @parcelaNrCadastral + '%') 
                        AND (parcele.parcelaNrCadastralProvizoriu LIKE '%' + @parcelaNrCadastralProvizoriu + '%') 
                        
                        ORDER BY parcele.parcelaId DESC">
                        <SelectParameters>
                            <asp:SessionParameter SessionField="SESgospodarieId" DefaultValue="0" Name="gospodarieId" Type="Int32" />
                            <asp:SessionParameter SessionField="SESan" DefaultValue="2015" Name="an" Type="Int32" />
                            <asp:ControlParameter ControlID="ddlCCategoria" Name="parcelaCategorie" PropertyName="SelectedValue" />
                            <asp:ControlParameter ControlID="tbCCF" DefaultValue="%" Name="parcelaCF" PropertyName="Text" Type="String" />
                            <asp:ControlParameter ControlID="tbCDenumire" DefaultValue="%" Name="parcelaDenumire" PropertyName="Text" Type="String" />
                            <asp:ControlParameter ControlID="tbCBloc" DefaultValue="%" Name="parcelaNrBloc" PropertyName="Text" Type="String" />
                            <asp:ControlParameter ControlID="tbCTopo" DefaultValue="%" Name="parcelaNrTopo" PropertyName="Text" Type="String" />
                            <asp:ControlParameter ControlID="tbCNrCadastral" DefaultValue="%" Name="parcelaNrCadastral" PropertyName="Text" Type="String" />
                            <asp:ControlParameter ControlID="tbCNrCadastralProvizoriu" DefaultValue="%" Name="parcelaNrCadastralProvizoriu" PropertyName="Text" Type="String" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </asp:Panel>
                <asp:Panel ID="parcelePrimitePanel" runat="server">
                <asp:Label runat="server" ID="LabelPrimite" Text="Parcele primite" Font-Bold="true" Font-Size="Medium"/>
                    <asp:GridView ID="gvParcelePrimite" AllowPaging="True" DataKeyNames="parcelaId" AllowSorting="True"
                        CssClass="tabela" runat="server" AutoGenerateColumns="False" EnableViewState="true" EmptyDataText="Nu exita parcele primite"
                        DataSourceID="SqlParcelePrimite" OnRowDataBound="GvParcelePrimiteRowDataBound" OnSelectedIndexChanged="GvParcelePrimiteSelectedIndexChanged"
                        ShowFooter="True">
                        <Columns>
                            <asp:BoundField DataField="parcelaId" HeaderText="Nr." SortExpression="parcelaId" Visible="false" />
                            <asp:BoundField DataField="parcelaDenumire" HeaderText="Denumirea parcelei / tarlalei / solei" SortExpression="parcelaDenumire" />
                            <asp:BoundField DataField="ha" HeaderText="Hectare" SortExpression="ha" />
                            <asp:BoundField DataField="ari" HeaderText="Ari" SortExpression="ari" />
                            <asp:BoundField DataField="categorie" HeaderText="Categorie" SortExpression="categorie" Visible="true" />
                            <asp:BoundField DataField="contract" HeaderText="Date contract" SortExpression="contract" />
                            <asp:BoundField DataField="membruNume" HeaderText="Proprietar" SortExpression="membruNume" />
                            <asp:BoundField DataField="dinData" HeaderText="Incepand cu data" SortExpression="dinData" />
                            <asp:BoundField DataField="panaLaData" HeaderText="Pana la data" SortExpression="panaLaData" />
                        </Columns>
                        <FooterStyle CssClass="footer" />
                        <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="sqlParcelePrimite" runat="server" 
                        SelectCommand="SELECT	parcele.parcelaId,
                                                parcele.parcelaDenumire,
		                                        denumire1 as categorie,
		                                        convert(nvarchar,parceleAtribuiriContracte.contractNr) + '/' + 
		                                        convert(nvarchar,parceleAtribuiriContracte.contractDataSemnarii,104) as Contract, 
		                                        gospodarii_1.membruNume as 'membruNume', 
		                                        convert(nvarchar,parceleAtribuiriContracte.contractDataInceput,104) as 'dinData', 
		                                        convert(nvarchar,parceleAtribuiriContracte.contractDataFinal,104) as 'panaLaData', 
		                                        SUM(parceleModUtilizare.c3Ha) + ROUND(SUM(parceleModUtilizare.c3Ari) / 100, 0, 1) AS ha,
		                                        SUM(parceleModUtilizare.c3Ari) - ROUND(SUM(parceleModUtilizare.c3Ari) / 100, 0, 1) * 100 AS ari
                                        FROM	parceleAtribuiriContracte 
		                                        INNER JOIN gospodarii ON gospodarii.gospodarieId = parceleAtribuiriContracte.contractGospodarieIdPrimeste 
		                                        INNER JOIN parceleModUtilizare ON parceleModUtilizare.contractId = parceleAtribuiriContracte.contractId 
		                                        INNER JOIN gospodarii AS gospodarii_1 ON parceleAtribuiriContracte.contractGospodarieIdDa = gospodarii_1.gospodarieId 
		                                        INNER JOIN parcele on parcele.parcelaid = parceleModUtilizare.parcelaId
                                                INNER JOIN sabloaneCapitole on sabloaneCapitole.codRand = parcele.parcelaCategorie
                                        WHERE	 (gospodarii.gospodarieId = @gospodarieId) AND  contractactiv = 1  and  (sabloaneCapitole.capitol = '2a') and sabloaneCapitole.an = @an
                                        GROUP BY	gospodarii_1.gospodarieId, 
			                                        gospodarii.gospodarieId, 
			                                        contractactiv,
			                                        parceleAtribuiriContracte.contractId, 
			                                        parceleAtribuiriContracte.contractNr, 
			                                        parceleAtribuiriContracte.contractDataSemnarii, 
			                                        parceleAtribuiriContracte.contractGospodarieIdDa, 
			                                        parceleAtribuiriContracte.contractGospodarieIdPrimeste, 
			                                        parceleAtribuiriContracte.contractDataInceput, 
			                                        parceleAtribuiriContracte.contractDataFinal, 
			                                        parceleAtribuiriContracte.contractDetalii, 
			                                        ' (' + gospodarii_1.strada + ', ' + gospodarii_1.nr + ')', 
			                                        ' (' + gospodarii.strada + ', ' + gospodarii.nr + ')',
			                                        gospodarii_1.membruNume, 
			                                        parcelaDenumire, 
			                                        parcelaCategorie,
                                                    parcele.parcelaId,
                                                    denumire1
                                        ORDER BY	contractnr">
                        <SelectParameters>
                            <asp:SessionParameter SessionField="SESgospodarieId" DefaultValue="0" Name="gospodarieId" Type="Int32" />
                            <asp:SessionParameter SessionField="SESan" DefaultValue="2015" Name="an" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </asp:Panel>

                <asp:Panel ID="pnListaButoanePrincipale" CssClass="butoane" runat="server">
                    <asp:Button ID="btModificaParcela" runat="server" Text="modificare parcelă" OnClick="BtModificaParcelaClick" Visible="false" CssClass="buton" />
                    <asp:Button ID="btStergeParcela" runat="server" Text="şterge parcela" OnClick="BtStergeParcelaClick" ValidationGroup="GrupValidareParcele" OnClientClick="return confirm(&quot;Sunteți sigur că doriți să ștergeți parcela?&quot;)" Visible="false" CssClass="buton" />
                    <asp:Button ID="btListaAdaugaParcela" runat="server" Text="adaugă o parcelă" OnClick="BtListaAdaugaParcelaClick" CssClass="buton" />
                    <asp:Button ID="btIstoricParcela" runat="server" Text="istoric transfer parcelă" OnClick="BtIstoricParcelaClick" CssClass="buton" />
                    <asp:Button ID="btListaCapitole" runat="server" Text="listă gospodării" Visible="true" PostBackUrl="~/Gospodarii.aspx" CssClass="buton" />
                    <asp:Button ID="btTiparesteCapitol" runat="server" Text="tipărire capitol" Visible="false" OnClick="BtTiparesteCapitolClick" CssClass="buton" />
                    <asp:Button ID="tiparireCapitolAnCurentButton" runat="server" Text="tipărire capitol" Visible="true" OnClick="TiparireCapitolAnCurentButtonClick" CssClass="buton" />
                    <asp:Button ID="btModificaTransfer" runat="server" Text="transfer parcelă/parcele" Visible="false" OnClick="BtModificaTransferClick" CssClass="buton" />
                    <asp:Button ID="btModificaTransferaToateParcelele" runat="server" Text="transferă toate parcelele" Visible="true" PostBackUrl="~/transferToateParcelele.aspx" CssClass="buton" />
                    <asp:Button ID="btTiparesteHarta" runat="server" Text="vizualizare hartă" OnClick="BtTiparesteHartaClick" CssClass="buton" Visible="false"/>
                    <asp:Button ID="btAtribuireCulturi" runat="server" Text="culturi" Visible="true" OnClick="BtAtribuireCulturiClick" CssClass="buton" />
                    <asp:Button ID="btContracteUtilizare" runat="server" Text="contracte utilizare teren" Visible="true" PostBackUrl="~/contracte.aspx" CssClass="buton" />
                    <asp:Button ID="proprietariButton" runat="server" Text="proprietari" Visible="false" PostBackUrl="~/proprietariParcele.aspx" CssClass="buton" />
                </asp:Panel>
            </asp:Panel>

            <asp:Panel ID="pnAdaugaParcela" CssClass="panel_general" runat="server" Visible="false">
                <asp:Panel ID="pnAd" CssClass="adauga" runat="server">
                    <asp:Panel ID="pnAdTitlu" runat="server">
                        <h2>ADAUGĂ / MODIFICĂ O PARCELĂ</h2>
                    </asp:Panel>
                    <asp:Panel ID="pnAd1" runat="server">
                        <p>
                            <asp:Label ID="lblCodRand" runat="server" Visible="false" />
                            <asp:Label ID="lblParcelaId" runat="server" Visible="false" />
                            <asp:Label ID="lblDenumire" runat="server" Text="Denumirea parcelei" />
                            <asp:TextBox ID="tbDenumire" runat="server" Width="250px"></asp:TextBox>
                        </p>
                        <p>
                            <asp:Label ID="lblCategoria" runat="server" Text="Categoria de folosinţă" />
                            <asp:DropDownList ID="ddlCategoria" runat="server" DataSourceID="sdsCategoria" DataTextField="denumire4" DataValueField="codRand" AutoPostBack="true">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="sdsCategoria" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016 %>"
                                SelectCommand="SELECT codRand, denumire4 FROM sabloaneCapitole WHERE (an = @an) AND (capitol = '2a') AND (denumire4<>'') AND (CHARINDEX('+', formula) = 0) AND (CHARINDEX('-', formula) = 0)">
                                <SelectParameters>
                                    <asp:SessionParameter Name="an" SessionField="SESan" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                            <p>
                                <asp:Label ID="Label2" runat="server" Text="Titular drept proprietate" />
                                <asp:DropDownList ID="proprietarDropDownList" DataTextField="nume" DataValueField="capitolId" runat="server" Width="250" AutoPostBack="true">
                                </asp:DropDownList>
                            </p>
                            <p>
                                <asp:Label ID="lblSuprafataIntravilan" runat="server" Text="Suprafaţa: Intravilan " />
                                <asp:Label ID="lblIntravilanHa" runat="server" Text=" ha" />
                                <asp:TextBox ID="tbIntravilanHa" runat="server" Width="50px"></asp:TextBox>
                                <asp:Label ID="lblIntravilanAri" runat="server" Text=" ari" />
                                <asp:TextBox ID="tbIntravilanAri" runat="server" Width="50px"></asp:TextBox>
                                <asp:Label ID="lblSuprafataExtravilan" runat="server" Text="Extravilan " />
                                <asp:Label ID="lblExtravilanHa" runat="server" Text=" ha" />
                                <asp:TextBox ID="tbExtravilanHa" runat="server" Width="50px"></asp:TextBox>
                                <asp:Label ID="lblExtravilanAri" runat="server" Text=" ari" />
                                <asp:TextBox ID="tbExtravilanAri" runat="server" Width="50px"></asp:TextBox>
                            </p>
                            <p>
                                <asp:Label ID="lblNrTopo" runat="server" Text="Nr. topografic sau denumirea locului" />
                                <asp:TextBox ID="tbNrTopo" runat="server" Width="90px"></asp:TextBox>
                                <asp:Label ID="lblTarla" runat="server" Text="Tarla" />
                                <asp:TextBox ID="tbTarla" runat="server" Width="150px"></asp:TextBox>
                                <asp:Label ID="lblTitluProprietate" runat="server" Text="Titlu de proprietate" />
                                <asp:TextBox ID="tbTitluProprietate" runat="server" Width="200px"></asp:TextBox>
                            </p>
                            <p>
                                <asp:Label ID="lblCF" runat="server" Text="C.F." />
                                <asp:TextBox ID="tbCF" runat="server" Width="90px"></asp:TextBox>
                                <asp:Label ID="lblNrCadastral" runat="server" Text="nr. cadastral" />
                                <asp:TextBox ID="tbNrCadastral" runat="server" Width="90px"></asp:TextBox>
                                <asp:Label ID="lblNrCadastralProvizoriu" runat="server" Text="nr.cad. provizoriu" />
                                <asp:TextBox ID="tbNrCadastralProvizoriu" runat="server" Width="90px"></asp:TextBox>
                                <asp:Label ID="lblNrBloc" runat="server" Text="Bloc fizic" />
                                <asp:TextBox ID="tbNrBloc" runat="server" Width="70px"></asp:TextBox>
                            </p>
                            <p>
                                <asp:Label ID="lblClasaBonitate" runat="server" Text="Clasa de bonitate" />
                                <asp:DropDownList ID="ddlClasaBonitate" runat="server">
                                    <asp:ListItem Value="0">-</asp:ListItem>
                                    <asp:ListItem Value="1">1</asp:ListItem>
                                    <asp:ListItem Value="2">2</asp:ListItem>
                                    <asp:ListItem Value="3">3</asp:ListItem>
                                    <asp:ListItem Value="4">4</asp:ListItem>
                                    <asp:ListItem Value="5">5</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="lblPunctaj" runat="server" Text="Punctaj" />
                                <asp:DropDownList ID="ddlPunctaj" runat="server">
                                </asp:DropDownList>
                            </p>
                            <p>
                                <asp:Label ID="lblAn" runat="server" Text="Anul" Visible="false" />
                                <asp:TextBox ID="tbAn" runat="server" Visible="false"></asp:TextBox>
                                <asp:Label ID="lblLocalitate" runat="server" Text="Localitatea" />
                                <asp:DropDownList ID="ddlLocalitate" runat="server" DataSourceID="SqlLocalitati" DataTextField="localitateDenumire" DataValueField="localitateDenumire">
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="SqlLocalitati" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016 %>"
                                    SelectCommand="SELECT [localitateDenumire] FROM [localitatiComuna] WHERE ([unitateId] = @unitateId)">
                                    <SelectParameters>
                                        <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" Type="Int32" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                                <asp:Label ID="lblAdresa" runat="server" Text="Adresa (str., nr. etc.)" />
                                <asp:TextBox ID="tbAdresa" runat="server" Width="300px"></asp:TextBox>
                                <br />
                                <asp:CheckBoxList ID="proprietariCheckBoxList" runat="server" Visible="true" CssClass="checkboxuri" AutoPostBack="true"></asp:CheckBoxList>
                                <br />
                                <asp:Label ID="lblMentiuni" runat="server" Text="Menţiuni" />
                                <br />
                                <asp:TextBox ID="tbMentiuni" runat="server" Rows="2" TextMode="MultiLine" Width="600px"></asp:TextBox>
                            </p>
                    </asp:Panel>

                    <asp:Panel ID="pnAdaugaCoordonate" CssClass="panel_general" runat="server" Visible="false">
                        <asp:Panel ID="Panel1" CssClass="adauga" runat="server">
                            <asp:Panel ID="Panel2" runat="server">
                                <h2>COORDONATE</h2>
                            </asp:Panel>
                            <asp:Panel ID="adaugare" runat="server">
                                <p>
                                    <asp:Label ID="lblX" runat="server" Text="X"></asp:Label>
                                    <asp:TextBox ID="tbX" runat="server" Width="100px"></asp:TextBox>
                                    <asp:Label ID="lblY" runat="server" Text="Y"></asp:Label>
                                    <asp:TextBox ID="tbY" runat="server" Width="100px"></asp:TextBox>
                                    <asp:Label ID="lblZ" runat="server" Text="Z" Visible="false"></asp:Label>
                                    <asp:TextBox ID="tbZ" runat="server" Width="100px" Visible="false"></asp:TextBox>
                                    <asp:Label ID="lblExplicatie" runat="server" Text="(6 cifre intregi + 4 zecimale)"></asp:Label>
                                </p>
                            </asp:Panel>
                            <asp:Panel ID="pnButoane" runat="server" CssClass="butoane">
                                <asp:Button ID="btAdaugaCoordonata" runat="server" CssClass="buton" Text="adaugă o coordonată " OnClick="BtAdaugaCoordonataClick" />
                            </asp:Panel>
                        </asp:Panel>
                    </asp:Panel>

                    <asp:Panel ID="pnListaCoordonate" runat="server" CssClass="panel_general"  Visible="false">
                        <asp:GridView ID="gvCoordonate" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            BorderStyle="None" CellPadding="3" DataKeyNames="coordonataId" DataSourceID="SqlCoordonate"
                            GridLines="Vertical" ShowFooter="True" CssClass="tabela">
                            <Columns>
                                <asp:CommandField ShowEditButton="True" ShowDeleteButton="True" DeleteText="şterge" EditText="modifică" UpdateText="salvează" CancelText="renunţă" />
                                <asp:BoundField DataField="coordonataId" HeaderText="Nr." SortExpression="coordonataId" InsertVisible="False" ReadOnly="True" Visible="false" />
                                <asp:BoundField DataField="coordonataX" HeaderText="X" SortExpression="coordonataX" />
                                <asp:BoundField DataField="coordonataY" HeaderText="Y" SortExpression="coordonataY" />
                            </Columns>
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlCoordonate" runat="server"
                            DeleteCommand="DELETE FROM [coordonate] WHERE [coordonataId] = @coordonataId"
                            InsertCommand="INSERT INTO [coordonate] ([coordonataX], [coordonataY]) VALUES (@coordonataX, @coordonataY)"
                            SelectCommand="SELECT [coordonataId], [coordonataX], [coordonataY] FROM [coordonate] WHERE (([an] = @an) AND ([unitateId] = @unitateId)  AND ([parcelaId] = @parcelaId)  AND ([gospodarieId] = @gospodarieId))"
                            UpdateCommand="UPDATE [coordonate] SET [coordonataX] = @coordonataX, [coordonataY] = @coordonataY  WHERE [coordonataId] = @coordonataId">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="lblParcelaId" DefaultValue="0" Name="parcelaId" PropertyName="Text" Type="Int32" />
                                <asp:SessionParameter Name="an" SessionField="SESan" Type="Int32" />
                                <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" Type="Int32" />
                                <asp:SessionParameter Name="gospodarieId" SessionField="SESgospodarieId" Type="Int32" />
                            </SelectParameters>
                            <DeleteParameters>
                                <asp:Parameter Name="coordonataId" Type="Int32" />
                            </DeleteParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="coordonataX" Type="Decimal" />
                                <asp:Parameter Name="coordonataY" Type="Decimal" />
                                <asp:Parameter Name="coordonataId" Type="Int32" />
                            </UpdateParameters>
                            <InsertParameters>
                                <asp:Parameter Name="coordonataX" Type="Decimal" />
                                <asp:Parameter Name="coordonataY" Type="Decimal" />
                            </InsertParameters>
                        </asp:SqlDataSource>
                    </asp:Panel>

                    <asp:Panel ID="pnButoanePrincipale" runat="server" CssClass="butoane">
                        <asp:Button ID="btAdaugaParcela" runat="server" CssClass="buton" Text="adaugă o parcelă" OnClick="AdaugaModificaParcela" ValidationGroup="GrupValidareParcele" Visible="false" />
                        <asp:Button ID="btAdaugaModificaParcela" runat="server" CssClass="buton" OnClick="AdaugaModificaParcela" Text="modificare parcelă" ValidationGroup="GrupValidareParcele" Visible="false" />
                        <asp:Button ID="btModificaArataListaParcele" runat="server" CssClass="buton" OnClick="ModificaArataListaParcele" Text="lista parcelelor" Visible="false" />
                        <asp:Button ID="btAdaugaArataListaParcele" runat="server" CssClass="buton" OnClick="AdaugaArataListaParcele" Text="lista parcelelor" Visible="false" />
                    </asp:Panel>
                    <asp:Panel ID="Panel5" CssClass="ascunsa" runat="server">
                        <asp:RequiredFieldValidator ID="valDenumire" ControlToValidate="tbDenumire" ValidationGroup="GrupValidareParcele" runat="server" ErrorMessage=" Denumirea este obligatorie! "></asp:RequiredFieldValidator>
                        <asp:CustomValidator ID="valCustom" runat="server" ErrorMessage="x" ValidationGroup="GrupValidareParcele"></asp:CustomValidator>
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>

            <asp:Panel ID="pnAtribuireCulturi" CssClass="panel_general" runat="server" Visible="false">
                <asp:Panel ID="Panel4" CssClass="adauga" runat="server">
                    <asp:Panel ID="Panel6" runat="server">
                        <h2>ATRIBUIRE CULTURI</h2>
                    </asp:Panel>
                    <asp:Panel ID="Panel7" runat="server">
                        <p>
                            <asp:Label ID="lblDenumireaParcelei1" runat="server" Text="Denumirea parcelei: " />
                            <asp:Label ID="lblDenumireaParcelei" runat="server" Text="" />
                        </p>
                        <p>
                            <asp:Label ID="chapterLabel" runat="server" Text="Capitolul:" />
                            <br />
                            <asp:DropDownList ID="capitoleDropDownList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="CapitoleDropDownListSelectedIndexChanged">
                                <asp:ListItem Text="Suprafaţa arabilă situată pe raza localităţii - culturi în câmp (4a)" Value="4a" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Culturi succesive în câmp, culturi intercalate, culturi modificate genetic pe raza localității (4a1)" Value="4a1"></asp:ListItem>
                                <asp:ListItem Text="Suprafața cultivată în sere pe raza localității (4b1)" Value="4b"></asp:ListItem>
                                <asp:ListItem Text="Suprafața cultivată în solarii şi alte spații protejate pe raza localității (4b2)" Value="4b2"></asp:ListItem>
                                <asp:ListItem Text="Suprafaţa cultivată cu legume si cartofi în grădinile familiale pe raza localităţii (4c)" Value="4c"></asp:ListItem>
                            </asp:DropDownList>
                            <br />
                            <br />
                            <asp:Label ID="lblCultura4a" runat="server" Text="Cultura:" />
                            <br />
                            <asp:DropDownList ID="culturiDropDownList" runat="server" AutoPostBack="true" DataSourceID="SqlAtribuire4a" DataTextField="denumire" DataValueField="cod" OnSelectedIndexChanged="CulturiDropDownListSelectedIndexChanged">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="SqlAtribuire4a" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016 %>"
                                SelectCommand="SELECT CONVERT(nvarchar, codRand) + COALESCE(legatura,'') AS cod, CONVERT(nvarchar, codRand) + '. ' + denumire5 AS denumire FROM sabloaneCapitole  WHERE (an = @an) AND (capitol = @capitol) AND (denumire5<>'') AND (CHARINDEX('+', formula) = 0) AND (CHARINDEX('-', formula) = 0)">
                                <SelectParameters>
                                    <asp:SessionParameter Name="an" SessionField="SESan" />
                                    <asp:ControlParameter ControlID="capitoleDropDownList" PropertyName="SelectedValue" DefaultValue="4a" Type="String" Name="capitol" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                            <br />
                            <br />
                            <asp:Label ID="lblTotalParcelaInFolosinta" runat="server" Text="Total parcelă în folosinţă: " ></asp:Label>
                            <asp:Label ID="lblSuprafataInFolosinta" runat="server" Text=""></asp:Label>
                            <br />
                            <asp:Label ID="lblTotalParcelaCultivata" runat="server" Text="Total parcelă cultivată: " ></asp:Label> &nbsp;&nbsp;&nbsp;
                            <asp:Label ID="lblSuprafataCultivata" runat="server" Text=""></asp:Label>
                            <br />
                            <asp:Label ID="Label4" runat="server" Text="Total parcelă rămasă de cultivat: " ></asp:Label>
                            <asp:Label ID="lblSuprafataRamasaDeCultivat" runat="server" Text=""></asp:Label>
                            <br />
                            <asp:Label runat="server" Text="Suprafaţa de cultivat în capitolul şi cu cultura selectate:" id="statusLabel"/>
                            <asp:TextBox ID="tbAtribuireHa" Width="50" runat="server" />
                            ha
                            <asp:TextBox ID="tbAtribuireAri" Width="90" runat="server" />
                            ari
                            <asp:TextBox ID="tbAtribuireMp" Width="90" runat="server" />
                            mp
                            <br />
                            <br />
                            <asp:Label ID="lblMesajPlin" runat="server" Text="Suprafaţa parcelei este complet atribuită. Dacă mai doriţi introducerea de alte culturi, ştergeţi sau modificaţi una din atribuirile din tabelul de mai jos." CssClass="mesaj_2" Visible="false" />
                        </p>
                        <asp:Panel ID="pnButoaneAtribuiri" runat="server" CssClass="butoane">
                            <asp:Button CssClass="buton" ID="btModificaAtribuiri" runat="server" Text="salvează" Visible="true" OnClick="BtModificaAtribuiriClick" />
                               <asp:Button ID="btStergeAtribuire" runat="server" CssClass="buton" Text="şterge" OnClientClick="return confirm('Sigur doriți să ștergeți rândul?')" OnClick="BtStergeAtribuireClick" Visible="false" />
                            <asp:Button CssClass="buton" ID="resetButton" runat="server" Text="resetează" Visible="true" OnClick="ResetButton_Click" />
                        </asp:Panel>
                        <p>
                            <asp:GridView ID="gvAtribuire4a" runat="server" AutoGenerateColumns="False"
                                AllowPaging="True" AllowSorting="True" CssClass="tabela" OnPreRender="GvAtribuire4aPreRender"
                                OnRowDataBound="GvAtribuire4aRowDataBound" DataKeyNames="Id" OnSelectedIndexChanged="GvAtribuire4aSelectedIndexChanged"
                                ShowFooter="True" EmptyDataText="nu s-a atribuit nicio cultură în capitolul 4a">
                                <Columns>
                                    <asp:TemplateField HeaderText="Suprafaţa arabilă situată pe raza localităţii - culturi în câmp (4a)">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCodRand" runat="server" Text='<%# ((CulturiIn4a)Container.DataItem).CodRand %>'></asp:Label>.
                                            <asp:Label ID="lblDenumire" runat="server" Text='<%# ((CulturiIn4a)Container.DataItem).Denumire %>'></asp:Label>:
                                            <asp:Label ID="lblSuprafata" runat="server" Text='<%# ((CulturiIn4a)Container.DataItem).SuprafataCultivata %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="Label3" runat="server" Text="Suprafaţă totală cultivată:" Font-Bold="true"></asp:Label>
                                            <asp:Label ID="lblGv4aFooterNumarHa" runat="server" Text="" Font-Bold="true"></asp:Label>
                                            <asp:Label ID="lblGv4aFooterNumarAri" runat="server" Text="" Font-Bold="true"></asp:Label>
                                           
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#FFFF99" ForeColor="#990000" />
                                <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                            </asp:GridView>
                        </p>
                        <p>
                            <asp:GridView ID="gvAtribuire4a1" runat="server" AutoGenerateColumns="False"
                                AllowPaging="True" AllowSorting="True" CssClass="tabela" OnPreRender="GvAtribuire4a1PreRender"
                                OnRowDataBound="GvAtribuire4a1RowDataBound" DataKeyNames="Id" OnSelectedIndexChanged="GvAtribuire4a1SelectedIndexChanged"
                                ShowFooter="True" EmptyDataText="nu s-a atribuit nicio cultură în capitolul 4a1">
                                <Columns>
                                    <asp:TemplateField HeaderText="Culturi succesive în câmp, culturi intercalate, culturi modificate genetic pe raza localității (4a1)">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCodRand" runat="server" Text='<%# ((CulturiIn4a1)Container.DataItem).CodRand %>'></asp:Label>.
                                            <asp:Label ID="lblDenumire" runat="server" Text='<%# ((CulturiIn4a1)Container.DataItem).Denumire %>'></asp:Label>:
                                            <asp:Label ID="lblSuprafata" runat="server" Text='<%# ((CulturiIn4a1)Container.DataItem).SuprafataCultivata %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="Label3" runat="server" Text="Suprafaţă totală cultivată:" Font-Bold="true"></asp:Label>
                                            <asp:Label ID="lblGv4a1FooterNumarHa" runat="server" Text="" Font-Bold="true"></asp:Label>
                                            <asp:Label ID="lblGv4a1FooterNumarAri" runat="server" Text="" Font-Bold="true"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#FFFF99" ForeColor="#990000" />
                                <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                            </asp:GridView>
                        </p>
                        <p>
                            <asp:GridView ID="gvAtribuire4b1" runat="server" AutoGenerateColumns="False"
                                AllowPaging="True" AllowSorting="True" CssClass="tabela" OnPreRender="GvAtribuire4b1PreRender"
                                OnRowDataBound="GvAtribuire4b1RowDataBound" DataKeyNames="Id" OnSelectedIndexChanged="GvAtribuire4b1SelectedIndexChanged"
                                ShowFooter="True" EmptyDataText="nu s-a atribuit nicio cultură în capitolul 4b1">
                                <Columns>
                                    <asp:TemplateField HeaderText="Suprafața cultivată în sere pe raza localității (4b1)">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCodRand" runat="server" Text='<%# ((CulturiIn4b1)Container.DataItem).CodRand %>'></asp:Label>.
                                            <asp:Label ID="lblDenumire" runat="server" Text='<%# ((CulturiIn4b1)Container.DataItem).Denumire %>'></asp:Label>:
                                            <asp:Label ID="lblSuprafata" runat="server" Text='<%# ((CulturiIn4b1)Container.DataItem).SuprafataCultivata %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="Label3" runat="server" Text="Suprafaţă totală cultivată:" Font-Bold="true"></asp:Label>
                                            <asp:Label ID="lblGv4b1FooterNumarAri" runat="server" Text="" Font-Bold="true"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#FFFF99" ForeColor="#990000" />
                                <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                            </asp:GridView>
                        </p>
                        <p>
                            <asp:GridView ID="gvAtribuire4b2" runat="server" AutoGenerateColumns="False"
                                AllowPaging="True" AllowSorting="True" CssClass="tabela" OnPreRender="GvAtribuire4b2PreRender"
                                OnRowDataBound="GvAtribuire4b2RowDataBound" DataKeyNames="Id" OnSelectedIndexChanged="GvAtribuire4b2SelectedIndexChanged"
                                ShowFooter="True" EmptyDataText="nu s-a atribuit nicio cultură în capitolul 4b2">
                                <Columns>
                                    <asp:TemplateField HeaderText="Suprafața cultivată în solarii şi alte spații protejate pe raza localității (4b2)">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCodRand" runat="server" Text='<%# ((CulturiIn4b2)Container.DataItem).CodRand %>'></asp:Label>.
                                            <asp:Label ID="lblDenumire" runat="server" Text='<%# ((CulturiIn4b2)Container.DataItem).Denumire %>'></asp:Label>:
                                            <asp:Label ID="lblSuprafata" runat="server" Text='<%# ((CulturiIn4b2)Container.DataItem).SuprafataCultivata %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="Label3" runat="server" Text="Suprafaţă totală cultivată:" Font-Bold="true"></asp:Label>
                                            <asp:Label ID="lblGv4b2FooterNumarAri" runat="server" Text="" Font-Bold="true"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#FFFF99" ForeColor="#990000" />
                                <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                            </asp:GridView>
                        </p>
                        <p>
                            <asp:GridView ID="gvAtribuire4c" runat="server" AutoGenerateColumns="False"
                                AllowPaging="True" AllowSorting="True" CssClass="tabela" OnPreRender="GvAtribuire4cPreRender"
                                OnRowDataBound="GvAtribuire4cRowDataBound" DataKeyNames="Id" OnSelectedIndexChanged="GvAtribuire4cSelectedIndexChanged"
                                ShowFooter="True" EmptyDataText="nu s-a atribuit nicio cultură în capitolul 4c">
                                <Columns>
                                    <asp:TemplateField HeaderText="Suprafaţa cultivată cu legume şi cartofi în grădinile familiale pe raza localităţii (4c)">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCodRand" runat="server" Text='<%# ((CulturiIn4c)Container.DataItem).CodRand %>'></asp:Label>.
                                            <asp:Label ID="lblDenumire" runat="server" Text='<%# ((CulturiIn4c)Container.DataItem).Denumire %>'></asp:Label>:
                                            <asp:Label ID="lblSuprafata" runat="server" Text='<%# ((CulturiIn4c)Container.DataItem).SuprafataCultivata %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="Label3" runat="server" Text="Suprafaţă totală cultivată:" Font-Bold="true"></asp:Label>
                                            <asp:Label ID="lblGv4cFooterNumarAri" runat="server" Text="" Font-Bold="true"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#FFFF99" ForeColor="#990000" />
                                <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                            </asp:GridView>
                        </p>
                    </asp:Panel>
                    <asp:Panel ID="pnButoaneStergeAtribuire" runat="server" CssClass="butoane">
                        <asp:Label ID="lblASters" runat="server" Text="" Visible="false" />
                        <asp:Button ID="btArataListaParcele" runat="server" CssClass="buton" Text="lista parcelelor" Visible="true" OnClick="BtArataListaParceleClick" />
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>

            <asp:Panel ID="pnIstoricParcela" runat="server" CssClass="panel_general" Visible="false">

                <h2>ISTORIC TRANSFER PARCELĂ</h2>

                <asp:GridView ID="gvIstoricParcela" AllowPaging="True" AllowSorting="True" CssClass="tabela"
                    runat="server" AutoGenerateColumns="False" DataSourceID="SqlIstoricParcela">
                    <Columns>
                        <asp:BoundField DataField="parcelaDenumire" HeaderText="Denumire parcela" SortExpression="parcelaDenumire" />
                        <asp:BoundField DataField="data" DataFormatString="{0:d}" HeaderText="Data transfer"
                            HtmlEncode="False" SortExpression="data" />
                        <asp:BoundField DataField="denumire" HeaderText="Tip transfer" SortExpression="denumire" />
                        <asp:BoundField DataField="documentAtesta" HeaderText="Document care atesta transferul"
                            SortExpression="documentAtesta" />
                        <asp:BoundField DataField="observatii" HeaderText="Observatii transfer" SortExpression="observatii" />
                        <asp:BoundField DataField="deLa" HeaderText="De la" SortExpression="deLa" />
                        <asp:BoundField DataField="la" HeaderText="La" SortExpression="la" />
                    </Columns>
                    <EmptyDataTemplate>
                        Nu exista date despre istoricul acestei parcele
                    </EmptyDataTemplate>
                    <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                </asp:GridView>

                <asp:SqlDataSource ID="SqlIstoricParcela" runat="server" 
                    SelectCommand=" SELECT parcele.parcelaDenumire, 
                                    istoricParcela.data, 
                                    tipTransferParcela.denumire, 
                                    istoricParcela.documentAtesta, 
                                    istoricParcela.observatii, 
                                    gospodarii.volum AS volumDeLa, 
                                    gospodarii.nrPozitie AS nrPozitieDeLa, 
                                    'Volum ' + COALESCE (gospodarii.volum, '') + ', nr. pozitie ' + gospodarii.nrPozitie + ', nume ' + 
                                        CASE (gospodarii.persjuridica) 
                                            WHEN 0 
                                            THEN membri.nume 
                                            ELSE gospodarii.junitate 
                                        END 
                                    AS deLa, 
                                    'Volum ' + COALESCE (gospodarii_1.volum, '') + ', nr. pozitie ' + COALESCE (gospodarii_1.nrPozitie, '') + ', nume ' + 
                                        CASE gospodarii_1.persjuridica 
                                            WHEN 0 
                                            THEN membri_1.nume 
                                            ELSE gospodarii_1.junitate 
                                        END 
                                    AS la 
                                    FROM istoricParcela 
                                         INNER JOIN parcele ON istoricParcela.parcelaId = parcele.parcelaId 
                                         INNER JOIN tipTransferParcela ON istoricParcela.motiv = tipTransferParcela.tipTransferId 
                                         INNER JOIN gospodarii ON istoricParcela.deLa = gospodarii.gospodarieId 
                                         INNER JOIN gospodarii AS gospodarii_1 ON istoricParcela.la = gospodarii_1.gospodarieId 
                                         INNER JOIN membri AS membri_1 ON gospodarii_1.gospodarieId = membri_1.gospodarieId 
                                         LEFT OUTER JOIN membri ON gospodarii.gospodarieId = membri.gospodarieId 
                                    WHERE (membri.an = @an) AND (membri_1.an = @an) AND (membri_1.codRudenie = 1) AND (membri_1.codRudenie = 1) AND (istoricParcela.parcelaId = @parcelaId)">
                    <SelectParameters>
                        <asp:SessionParameter Name="an" SessionField="SESan" />
                        <asp:ControlParameter ControlID="gvParcele" Name="parcelaId" PropertyName="SelectedDataKey.Value" />
                    </SelectParameters>
                </asp:SqlDataSource>

                <asp:Panel ID="pnButoaneIstoricParcela" runat="server" Style="text-align: left;">
                    <asp:Button ID="btnInapoiListaParcele" runat="server" CssClass="buton" Text="lista parcelelor"
                        Visible="true" OnClick="BtArataListaParceleClick" />
                </asp:Panel>

            </asp:Panel>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
