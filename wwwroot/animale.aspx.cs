﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
/// <summary>
/// Animale
/// Creata la:                  10.2011
/// Autor:                      SM
/// Ultima                      actualizare: 16.11.2011
/// Autor:                      SM - animale 
/// </summary> 
public partial class Animale : System.Web.UI.Page
{
    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        DBConnections connection = new DBConnections(Convert.ToInt16(Session["SESan"]));
        SqlGv.ConnectionString = connection.Create();
        SqlVolum.ConnectionString = connection.Create();
        SqlNrPozitie.ConnectionString = connection.Create();

        if ( !IsPostBack )
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "Animale", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["SESgospodarieId"] == null)
        {
            HttpCookie vCookie = Request.Cookies["COOKTNT"];
            vCookie = CriptareCookie.DecodeCookie(vCookie);
            if (vCookie != null)
            {
                if (vCookie["COOKgospodarieId"] != null)
                {
                    Session["SESgospodarieId"] = vCookie["COOKgospodarieId"].ToString();
                }
                else if (Request.Url.AbsolutePath.IndexOf("Gospodarii.aspx") == -1 && Session["SESgospodarieId"] == null)
                    Response.Redirect("~/Gospodarii.aspx");
            }
        }
    }

    protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(gv, e, this);
    }

    protected void ArataAdauga(object sender, EventArgs e)
    {

    }

    protected void btModifica_Click(object sender, EventArgs e)
    {
        ViewState["tip"] = "m"; 
        pnAdauga.Visible = true;
        pnLista.Visible = false;
        pnAd1.Visible = true;
        pnSterge.Visible = false;
        Initializare(sender, e);
    }
    protected void Initializare(object sender, EventArgs e)
    {
        if (ViewState["tip"].ToString() == "a")
        {
            // daca suntem in semestrul 2 initializam dataFatarii cu 01.07.anul, daca nu, 01.01.anul
            if (Convert.ToInt32(DateTime.Now.Month) > 6)
            {
                tbDataFatarii.Text = "01.07." + DateTime.Now.Year.ToString();
                tbDataOperare.Text = "01.07." + DateTime.Now.Year.ToString();
            }
            else
            {
                tbDataFatarii.Text = "01.01." + DateTime.Now.Year.ToString();
                tbDataOperare.Text = "01.01." + DateTime.Now.Year.ToString();
            }
            tbNrBucati.Text = "1";
            dlCodIdentificareExemple.DataBind();
            dlTaraDenumire.DataBind();
            ddlCategoria.Enabled = true;
            ddlSex.Enabled = true;
            ddlRasa.Enabled = true;
            tbNrBucati.Enabled = true;
            tbNrBucati_Load(sender, e);
            tbDataFatarii.Enabled = true;
            x.Visible = false;
            cbCatreNecunoscuta.Enabled = true;
            ddlNrPozitie.Enabled = true;
            ddlVolum.Enabled = true;
        }
        else if (ViewState["tip"].ToString() == "m")
        { 
            // luam datele din grid si umplem campurile - animaleevolutie
            string[] vCampuri = {"animaleCategorieId","animaleRasaId","animalSex","codCapitol","animaleOperatieId","animaleOperatieData","animaleEvolutieCapete","gospodarieIdCatre","animalDataFatarii","animalCodIdentificare",  "animaleTaraId", "animalId" };
            List<string> vValori = ManipuleazaBD.fRezultaUnString("SELECT animale.animalSex, animale.codCapitol, animale.animaleTaraId, animaleEvolutie.animaleRasaId, animaleEvolutie.animalCodIdentificare, animaleEvolutie.animalDataFatarii, animale.animaleCategorieId, animaleEvolutie.gospodarieIdCatre, animaleEvolutie.animaleEvolutieCapete, animaleEvolutie.animaleOperatieData, animaleEvolutie.animaleOperatieId, animale.animalId FROM animaleEvolutie INNER JOIN animale ON animaleEvolutie.animalId = animale.animalId WHERE (animaleEvolutie.animaleEvolutieId = '" + ((Label)gv.SelectedRow.FindControl("lblAnimaleEvolutieId")).Text + "')", vCampuri, Convert.ToInt16(Session["SESan"]));
            ddlCategoria.DataBind();
            ddlCategoria.SelectedValue = vValori[0].Trim();
            // nu lasam sa modifice categoria
            ddlCategoria.Enabled = false;
            ddlRasa.Enabled = true;
            ddlRasa.DataBind();
            ddlCategoriaSchimba();
            ddlRasa.SelectedValue = vValori[1].Trim();
            CautaSexul(vValori[2]);
            // nu lasam sa modifice sexul
            ddlSex.Enabled = false;
            x.Visible = false;
            x.Text = vValori[3];
            string[] vRanduri = ScoateRandurile(vValori[3]);
            foreach (string vRand in vRanduri)
            {
                // facem sau vizibile panelurile si dropdown-urile in functie de randurile unde s-a scris, gen 12#14#15#
                InterpreteazaRandurile(vRand);
            }
            // nr bucati -  nu lasam sa modifice  - daca vrea sa mai adauge, foloseste butonul adauga
            tbNrBucati.Enabled = false;
            tbNrBucati.Text = vValori[6];
            // data fatarii - daca nu e 01.01.1900 nu lasam sa modifice; daca e 01.01.1900 atunci nu afisam nimic si lasam sa schimbe
            if (vValori[8].IndexOf("01.01.1900") != -1 )
            {
                tbDataFatarii.Text = "";
                tbDataFatarii.Enabled = true;
            }
            else
            {
                tbDataFatarii.Text = vValori[8].Substring(0,10);
            //    tbDataFatarii.Enabled = false;
            }
            // data operare
            if (vValori[5].IndexOf("01.01.1900") != -1)
            {
                tbDataOperare.Text = "";
                tbDataOperare.Enabled = true;
            }
            else
            {
                tbDataOperare.Text = vValori[5].Substring(0, 10);
                tbDataOperare.Enabled = true;
            }

            // daca nu e categorie pt care trebuie cod si daca se introduce un singur animal
            if (tbNrBucati.Text == "1")
            {
                pnlCodIdentificare.Visible = true;
            }
            else
            { pnlCodIdentificare.Visible = false; lblNrBucatiMax.Visible = false; }
            SqlTaraSelectata_Init(sender, e);
            dlTaraDenumire.DataBind();
            if (tbNrBucati.Text == "1")
            {
                ((TextBox)dlTaraDenumire.Items[0].FindControl("tbCodIdentificare")).Text = vValori[9];
            }
            // daca animaleOperatieId este 1 nu aratam gospodariacatre
            if (vValori[4] == "1")
            {
                pnlCatre.Visible = false;
            }
            else
            {
               /* cbCatreNecunoscuta.Enabled = false;
                ddlNrPozitie.Enabled = false;
                ddlVolum.Enabled = false;
               */ // gospodarieIdCatre daca nu e zero bifam
                pnlCatre.Visible = true;
                if (vValori[7] == "0")
                {
                    cbCatreNecunoscuta.Checked = true;
                    cbCatreNecunoscuta_Load(sender, e);
                }
                else
                {
                    cbCatreNecunoscuta.Checked = false;
                    // aflam vol si nr pozitie
                    string[] vCampuriCatre = {"volumInt","nrPozitieInt"};
                    List<string> vDateCatre = ManipuleazaBD.fRezultaUnString("SELECT TOP(1) volumInt,  nrPozitieInt FROM gospodarii WHERE gospodarieID = '" + vValori[7] + "'", vCampuriCatre, Convert.ToInt16(Session["SESan"]));
                    ddlVolum.SelectedValue = vDateCatre[0];
                    ddlNrPozitie.DataBind();
                    ddlNrPozitie.SelectedValue = vValori[7];
                    ckbCatreScadeti.Visible = false;
                    lblCatreScadeti.Visible = false;
                }
            
            }
            
        }
    }
    protected void InterpreteazaRandurile(string pRand)
    {
        List<Panel> vListaPaneluri = new List<Panel>();
        switch (pRand)
        {
            case "1":
                // BOVINE
                AscundePaneluriCategorii(pnlDetaliiCategoria1);
                vListaPaneluri.Add(pnlSex);
                ckbVitei.Visible = false;
                AscundePaneluriDetalii(vListaPaneluri);
                break;
            case "3":
                // femele sub 1 an 
                vListaPaneluri.Add(pnlDetaliiBovineFemele);
                vListaPaneluri.Add(pnlVitei);
                AscundePaneluriDetalii(vListaPaneluri);
                ddlDetaliiBovineFemeleVarsta1.DataBind();
                ddlDetaliiBovineFemeleVarsta1.Visible = true; 
                ddlDetaliiBovineFemeleVarsta2.Visible = false;
                ddlDetaliiBovineFemeleVarsta1.SelectedValue = "3";
                break;
            case "4":
                // femele sub 6 luni 
                vListaPaneluri.Add(pnlDetaliiBovineFemele);
                vListaPaneluri.Add(pnlVitei);
                AscundePaneluriDetalii(vListaPaneluri);
                ddlDetaliiBovineFemeleVarsta1.DataBind();
                ckbVitei.Checked = false;
                ddlDetaliiBovineFemeleVarsta1.Visible = true;
                ddlDetaliiBovineFemeleVarsta2.Visible = false;
                ddlDetaliiBovineFemeleVarsta1.SelectedValue = "4";
                break;
            case "5":
                // masculi sub 1 an 
                vListaPaneluri.Add(pnlDetaliiBovineMasculi);
                vListaPaneluri.Add(pnlVitei);
                AscundePaneluriDetalii(vListaPaneluri);
                ddlDetaliiBovineMasculiVarsta1.DataBind();
                ddlDetaliiBovineMasculiVarsta1.Visible = true;
                ddlDetaliiBovineMasculiVarsta2.Visible = false;
                ddlDetaliiBovineMasculiVarsta1.SelectedValue = "5";
                break;
            case "6":
                // masculi sub 6 luni 
                vListaPaneluri.Add(pnlDetaliiBovineMasculi);
                vListaPaneluri.Add(pnlVitei);
                AscundePaneluriDetalii(vListaPaneluri);
                ckbVitei.Checked = false;
                ddlDetaliiBovineMasculiVarsta1.DataBind();
                ddlDetaliiBovineMasculiVarsta1.Visible = true;
                ddlDetaliiBovineMasculiVarsta2.Visible = false;
                ddlDetaliiBovineMasculiVarsta1.SelectedValue = "6";
                break;
            case "7":
                // vitei sacrificare
                ckbVitei.Checked = true;
                ckbVitei.Visible = true;
                break;
            case "9":
                // femele intre 1 si 2 ani
                vListaPaneluri.Add(pnlDetaliiBovineFemele);
                AscundePaneluriDetalii(vListaPaneluri);
                ddlDetaliiBovineFemeleVarsta1.DataBind();
                ddlDetaliiBovineFemeleVarsta1.Visible = true;
                ddlDetaliiBovineFemeleVarsta1.SelectedValue = "9";
                break;
            case "10":
                // masculi intre 1 si 2 ani
                vListaPaneluri.Add(pnlDetaliiBovineMasculi);
                AscundePaneluriDetalii(vListaPaneluri);
                ddlDetaliiBovineMasculiVarsta1.DataBind();
                ddlDetaliiBovineMasculiVarsta1.Visible = true;
                ddlDetaliiBovineMasculiVarsta1.SelectedValue = "10";
                break;
            case "12":
                // bovine mascului 2 ani si peste
                vListaPaneluri.Add(pnlDetaliiBovineMasculi);
                AscundePaneluriDetalii(vListaPaneluri);
                ddlDetaliiBovineMasculiVarsta1.DataBind();
                ddlDetaliiBovineMasculiVarsta1.Visible = true;
                ddlDetaliiBovineMasculiVarsta1.SelectedValue = "0";
                break;
            case "13":
                // tauri reproducere altii
                ddlDetaliiBovineMasculiVarsta2.DataBind();
                ddlDetaliiBovineMasculiVarsta2.Visible = true;
                ddlDetaliiBovineMasculiVarsta2.SelectedValue = "131";
                break;
            case "14":
                // tauri reproducere autorizati
                ddlDetaliiBovineMasculiVarsta2.DataBind();
                ddlDetaliiBovineMasculiVarsta2.Visible = true;
                ddlDetaliiBovineMasculiVarsta2.SelectedValue = "130";
                break;
            case "15":
                // tauri reformati
                ddlDetaliiBovineMasculiVarsta2.DataBind();
                ddlDetaliiBovineMasculiVarsta2.Visible = true;
                ddlDetaliiBovineMasculiVarsta2.SelectedValue = "15";
                break;
            case "16":
                // tauri munca
                ddlDetaliiBovineMasculiVarsta2.DataBind();
                ddlDetaliiBovineMasculiVarsta2.Visible = true;
                ddlDetaliiBovineMasculiVarsta2.SelectedValue = "16";
                break;
            case "17":
                // femele peste 2 ani
                vListaPaneluri.Add(pnlDetaliiBovineFemele);
                AscundePaneluriDetalii(vListaPaneluri);
                ddlDetaliiBovineFemeleVarsta1.DataBind();
                ddlDetaliiBovineFemeleVarsta1.Visible = true;
                ddlDetaliiBovineFemeleVarsta1.SelectedValue = "0";
                break;
            case "18":
                // femele peste 2 ani juninci
                ddlDetaliiBovineFemeleVarsta2.DataBind();
                ddlDetaliiBovineFemeleVarsta2.Visible = true;
                ddlDetaliiBovineFemeleVarsta2.SelectedValue = "18";
                break;
            case "19":
                // femele peste 2 ani vaci
                ddlDetaliiBovineFemeleVarsta2.DataBind();
                ddlDetaliiBovineFemeleVarsta2.Visible = true;
                ddlDetaliiBovineFemeleVarsta2.SelectedValue = "19";
                break;
            case "20":
                // femele peste 2 ani vaci lapte
                ddlDetaliiBovineFemeleVarsta3.DataBind();
                ddlDetaliiBovineFemeleVarsta3.Visible = true;
                ddlDetaliiBovineFemeleVarsta3.SelectedValue = "20";
                break;
            case "21":
                // femele peste 2 ani vaci reformate
                ddlDetaliiBovineFemeleVarsta3.DataBind();
                ddlDetaliiBovineFemeleVarsta3.Visible = true;
                ddlDetaliiBovineFemeleVarsta3.SelectedValue = "21";
                break;
            case "22":
                // BUBALINE
                AscundePaneluriCategorii(pnlDetaliiCategoria15);
                ddlBubaline.DataBind();
                ddlBubaline.Visible = true;
                break;
            case "23":
                //Bivoliţe pentru reproducţie
                ddlBubaline.SelectedValue = "23";
                break;
            case "24":
                //Bivoli pentru reproducţie
                ddlBubaline.SelectedValue = "24";
                break;
            case "25":
                //alte bube
                ddlBubaline.SelectedValue = "25";
                break;
            case "26":
                // OVINE
                AscundePaneluriCategorii(pnlDetaliiCategoria2);
                ddlDetaliiOvine.DataBind();
                ddlDetaliiOvine.Visible = true;
                break;
            case "27":
                ddlDetaliiOvine.SelectedValue = "27";
                break;
            case "28":
                ddlDetaliiOvine.SelectedValue = "28";
                break;
            case "29":
                ddlDetaliiOvine.SelectedValue = "29";
                break;
            case "30":
                ddlDetaliiOvine.SelectedValue = "30";
                break;
            case "31":
                ddlDetaliiOvine.SelectedValue = "31";
                break;
            case "32":
                // CAPRINE
                AscundePaneluriCategorii(pnlDetaliiCategoria3);
                ddlDetaliiCaprine.DataBind();
                ddlDetaliiCaprine.Visible = true;
                break;
            case "33":
                ddlDetaliiCaprine.SelectedValue = "33";
                break;
            case "34":
                ddlDetaliiCaprine.SelectedValue = "34";
                break;
            case "35":
                ddlDetaliiCaprine.SelectedValue = "35";
                break;
            case "36":
                ddlDetaliiCaprine.SelectedValue = "36";
                break;
            case "37":
                // PORCINE
                AscundePaneluriCategorii(pnlDetaliiCategoria4);
                ckbScrofite6_9luni.Visible = false;
                ddlDetaliiPorcine.DataBind();
                ddlDetaliiPorcine.Visible = true;
                break;
            case "38":
                ddlDetaliiPorcine.SelectedValue = "38";
                break;
            case "39":
                ddlDetaliiPorcine.SelectedValue = "39";
                break;
            case "40":
                ddlDetaliiPorcine.SelectedValue = "40";
                break;
            case "41":
                vListaPaneluri.Add(pnlDetaliiPorcine);
                vListaPaneluri.Add(pnlDetaliiPorcineDeIngrasat);
                AscundePaneluriDetalii(vListaPaneluri);
                ddlDetaliiPorcineDeIngrasat.DataBind();
                ddlDetaliiPorcineDeIngrasat.Visible = true;
                ddlDetaliiPorcine.SelectedValue = "41";
                break;
            case "42":
                ddlDetaliiPorcineDeIngrasat.SelectedValue = "42";
                break;
            case "43":
                ddlDetaliiPorcineDeIngrasat.SelectedValue = "43";
                break;
            case "44":
                ddlDetaliiPorcineDeIngrasat.SelectedValue = "44";
                break;
            case "45":
                vListaPaneluri.Add(pnlDetaliiPorcine);
                vListaPaneluri.Add(pnlDetaliiPorcineDeReproductie);
                AscundePaneluriDetalii(vListaPaneluri);
                ddlDetaliiPorcineDeReproductie.DataBind();
                ddlDetaliiPorcineDeReproductie.Visible = true;
                ddlDetaliiPorcine.SelectedValue = "45";
                break;
            case "46":
                ddlDetaliiPorcineDeReproductie.Visible = true;
                ddlDetaliiPorcineDeReproductie.SelectedValue = "46";
                break;
            case "47":
                ddlDetaliiPorcineDeReproductie.Visible = true;
                ddlDetaliiPorcineDeReproductie.SelectedValue = "47";
                break;
            case "48":
                vListaPaneluri.Add(pnlDetaliiPorcine);
                vListaPaneluri.Add(pnlDetaliiPorcineDeReproductie);
                vListaPaneluri.Add(pnlDetaliiScrofite);
                AscundePaneluriDetalii(vListaPaneluri);
                ddlDetaliiPorcineDeReproductie.Visible = true;
                ddlDetaliiPorcineDeReproductie.SelectedValue = "48";
                ckbScrofite6_9luni.Visible = true;
                ckbScrofite6_9luni.Checked = false;
                break;
            case "49":
                ckbScrofite6_9luni.Visible = true;
                ckbScrofite6_9luni.Checked = true;
                break;
            case "50":
                // CABALINE
                AscundePaneluriCategorii(pnlDetaliiCategoria5);
                ddlDetaliiCabaline.DataBind();
                ddlDetaliiCabaline.Visible = true;
                ddlDetaliiCabaline.SelectedValue = "50";
                break;
            case "51":
                ddlDetaliiCabaline.SelectedValue = "51";
                break;
            case "52":
                ddlDetaliiCabaline.SelectedValue = "52";
                break;
            case "53":
                // MAGARI
                AscundePaneluriCategorii(pnlDetaliiCategoria0);
                break;
            case "54":
                // CATARI
                AscundePaneluriCategorii(pnlDetaliiCategoria0);
                break;
            case "55":
                // IEPURI
                AscundePaneluriCategorii(pnlDetaliiCategoria8);
                ddlDetaliiIepuri.DataBind();
                ddlDetaliiIepuri.Visible = true;
                ddlDetaliiIepuri.SelectedValue = "55";
                break;
            case "56":
                ddlDetaliiIepuri.SelectedValue = "56";
                break;
            case "57":
                // ANIMALE DE BLANA
                ddlRasa.Enabled = false;
                break;
            case "58":
                ddlRasa.Enabled = false;
                break;
            case "59":
                ddlRasa.Enabled = false;
                break;
            case "60":
                ddlRasa.Enabled = false;
                break;
            case "61":
                ddlRasa.Enabled = false;
                break;
            case "62":
                // PASARI
                ddlRasa.Enabled = false;
                break;
            case "63":
                ddlRasa.Enabled = false;
                break;
            case "64":
                ddlRasa.Enabled = false;
                break;
            case "65":
                ddlRasa.Enabled = false;
                break;
            case "66":
                ddlRasa.Enabled = false;
                break;
            case "67":
                ddlRasa.Enabled = false;
                break;
            case "68":
                ddlRasa.Enabled = false;
                break;
            case "69":
                // ouatoare
                ddlRasa.Enabled = false;
                ckbPasariOuatoare.Checked = true;
                break;
            case "70":
                // gaini oautoare
                ddlRasa.Enabled = false;
                ckbGainiOuatoare.Checked = true;
                ckbPasariOuatoare.Checked = true;
                break;
            case "71":
                // alte animale
                ddlRasa.Enabled = false;
                break;
            case "72":
                // ALBINE
                ddlRasa.Enabled = false;
                break;

        }
    }
    protected string[] ScoateRandurile(string pRanduri)
    {
        string[] vRanduri = pRanduri.Split('#');
        return vRanduri;
    }
    protected void CautaSexul(string pSex)
    {
        switch (pSex)
        {
            case "1":
                pnlDetaliiSex.Visible = true;
                ddlSex.SelectedValue = "femele";
                break;
            case "2":
                pnlDetaliiSex.Visible = true;
                ddlSex.SelectedValue = "masculi";
                break; 
            case "0":
                pnlDetaliiSex.Visible = false;
                break; 
        }
    }
    protected void Adauga(object sender, EventArgs e)
    {
        ViewState["tip"] = "a";
        Initializare(sender,e);
        pnAdauga.Visible = true;
        pnLista.Visible = false;
        pnAd1.Visible = true;
        pnSterge.Visible = false;
    }
    protected void btListaGospodarii_Click(object sender, EventArgs e)
    {
        pnAdauga.Visible = false;
        pnLista.Visible = true;

    }

    #region salveaza
    protected void Salveaza(object sender, EventArgs e)
    {
        string vJudet = ManipuleazaBD.fRezultaUnString("SELECT judetId FROM unitati WHERE unitateId = '" + Session["SESunitateId"].ToString() + "'", "judetId", Convert.ToInt16(Session["SESan"]));
        int vSex = 0;
        if (ddlSex.Visible)
        {
            switch (ddlSex.SelectedValue)
            {
                case "femele":     vSex = 1;      break;
                case "masculi":    vSex = 2;      break;
            }
        }
        else  {       vSex = 0;  }
        // gospodarie  de la / catre care s-a facut operarea
        string vGospodarieCatre = "0";
        if (!cbCatreNecunoscuta.Checked)   {       vGospodarieCatre = ddlNrPozitie.SelectedValue;    }
        // lista cu animale inserate, pt cazul in care sunt mai multe deodata si cu coduri
        List<int> vListaAnimaleInserate = new List<int> { };
        // salvam in animale
        // in functie de categorie vedem daca salvam cu sau fara coduri
        int vAnimalId = 0;
        // daca nu sunt animale ce pot avea cod
        if (lblAscunsTextExemplu.Text == "--")
        { 
            // nu luam codul
            // scriem in animale si animale evolutie
            vAnimalId = 0;
            // daca e adaugare introducem si in animale
            if (ViewState["tip"].ToString() == "a")
            {
                string vInsertAnimale = "INSERT INTO animale (animaleCategorieId, animaleRasaId, animalDataFatarii, animaleTaraId, animalCodIdentificare, animalSex) VALUES ('" + ddlCategoria.SelectedValue + "', '0', CONVERT(datetime,'" + tbDataFatarii.Text + "', 104), '', '', '" + vSex.ToString() + "')";
                ManipuleazaBD.fManipuleazaBDCuId(vInsertAnimale, out vAnimalId, Convert.ToInt16(Session["SESan"])); // animale
            }
            // daca e modificare facem update in animale si scriem false la ultimul camp pentru toate celelalte legate de animalId
            else
            {
                try
                {
                    vAnimalId = Convert.ToInt32(((Label)gv.SelectedRow.FindControl("lblAnimalId")).Text);
                }
                catch
                { }
                ManipuleazaBD.fManipuleazaBD("UPDATE animaleEvolutie SET animaleEvolutieUltima = 'False' WHERE animalId = '" + vAnimalId.ToString() + "'", Convert.ToInt16(Session["SESan"]));
                ManipuleazaBD.fManipuleazaBD("UPDATE animale SET animaleDataFatarii = '" + tbDataFatarii.Text + "'  WHERE animalId = '" + vAnimalId.ToString() + "'", Convert.ToInt16(Session["SESan"]));
            }
            string vInsertEvolutie = "INSERT INTO animaleEvolutie ( animalCodIdentificare, animaleRasaId,animalDataFatarii, animalId, unitateId, gospodarieId, an, judetId, animaleEvolutieData, animaleOperatieId, animaleOperatieData, animaleEvolutieCapete, gospodarieIdCatre, animaleEvolutieUltima) VALUES ('0','" + ddlRasa.SelectedValue + "', CONVERT(datetime,'" + tbDataFatarii.Text + "', 104),'" + vAnimalId + "', '" + Session["SESunitateId"].ToString() + "','" + Session["SESgospodarieId"].ToString() + "','" + Session["SESan"].ToString() + "','" + vJudet + "',  CONVERT(datetime,'" + DateTime.Now.ToString() + "', 104), '" + ddlOperatieDenumire.SelectedValue + "', CONVERT(datetime,'" + tbDataOperare.Text + "', 104), '" + Convert.ToInt32(Convert.ToDecimal(tbNrBucati.Text)).ToString().Replace(',', '.') + "', '" + vGospodarieCatre + "', 'True')";
            ManipuleazaBD.fManipuleazaBD(vInsertEvolutie, Convert.ToInt16(Session["SESan"])); // animale evolutie
            vListaAnimaleInserate.Add(vAnimalId);
        }
        // daca sunt cu cod
        else
        {
            vAnimalId = 0;
            string vInsertEvolutie = "";
            // luam din datalistul cu coduri
            for (int i = 0; i < Convert.ToInt32(tbNrBucati.Text); i++)
            {
                string vCod = ((TextBox)(dlTaraDenumire.Items[i].FindControl("tbCodIdentificare"))).Text;
                string vTara = ((DropDownList)(dlTaraDenumire.Items[i].FindControl("ddlTara"))).SelectedValue;
                if (ViewState["tip"].ToString() == "a")
                {
                    string vInsertAnimale = "INSERT INTO animale (animaleCategorieId, animaleRasaId, animalDataFatarii, animaleTaraId, animalCodIdentificare, animalSex) VALUES ('" + ddlCategoria.SelectedValue + "', '" + ddlRasa.SelectedValue + "', CONVERT(datetime,'" + tbDataFatarii.Text + "', 104), '" + vTara + "', '" + vCod + "', '" + vSex.ToString() + "')";
                    ManipuleazaBD.fManipuleazaBDCuId(vInsertAnimale, out vAnimalId, Convert.ToInt16(Session["SESan"])); // animale
                }
                else
                {
                    try
                    {
                        vAnimalId = Convert.ToInt32(((Label)gv.SelectedRow.FindControl("lblAnimalId")).Text);
                    }
                    catch
                    { }
                    ManipuleazaBD.fManipuleazaBD("UPDATE animaleEvolutie SET animaleEvolutieUltima = 'False' WHERE animalId = '" + vAnimalId.ToString() + "'", Convert.ToInt16(Session["SESan"]));
                    ManipuleazaBD.fManipuleazaBD("UPDATE animale SET animaleDataFatarii = '" + tbDataFatarii.Text + "'  WHERE animalId = '" + vAnimalId.ToString() + "'", Convert.ToInt16(Session["SESan"]));
                }
                
                // cream insertul pentru evolutie; il concatenam si scriem la final cu o singura comanda
                vInsertEvolutie += "INSERT INTO animaleEvolutie ( animalCodIdentificare, animaleRasaId,animalDataFatarii,animalId, unitateId, gospodarieId, an, judetId, animaleEvolutieData, animaleOperatieId, animaleOperatieData, animaleEvolutieCapete, gospodarieIdCatre, animaleEvolutieUltima) VALUES ('" + vCod + "','" + ddlRasa.SelectedValue + "', CONVERT(datetime,'" + tbDataFatarii.Text + "', 104),'" + vAnimalId + "', '" + Session["SESunitateId"].ToString() + "','" + Session["SESgospodarieId"].ToString() + "','" + Session["SESan"].ToString() + "','" + vJudet + "',  CONVERT(datetime,'" + DateTime.Now.ToString() + "', 104), '" + ddlOperatieDenumire.SelectedValue + "', CONVERT(datetime,'" + tbDataOperare.Text + "', 104), '1','" + vGospodarieCatre + "', 'True');";
                vListaAnimaleInserate.Add(vAnimalId);
            }
            ManipuleazaBD.fManipuleazaBD(vInsertEvolutie, Convert.ToInt16(Session["SESan"])); // animale evolutie
        }

        // daca e modificare si nu apare vreo iesire, intrerupem
        if (ViewState["tip"].ToString() == "m")
        {
            pnAdauga.Visible = false;
            pnLista.Visible = true;
            gv.DataBind();
            gv.SelectedIndex = -1;
            btModifica.Visible = false;
            return;
        }   
        // in functie de categorie alegem ce operatie facem
        // scriem inserturile / update-urile pt capitole
        int k = 0;
        // verificam sa nu fie gol capitolul 7
        CalculCapitole.PopuleazaTabelaCapitole(Convert.ToInt32(Session["SESan"]), Convert.ToInt64(Session["SESgospodarieId"]), Convert.ToInt64(Session["SESunitateId"]), "7");
        // verificam sa nu fie gol capitolul 8
        CalculCapitole.PopuleazaTabelaCapitole(Convert.ToInt32(Session["SESan"]), Convert.ToInt64(Session["SESgospodarieId"]), Convert.ToInt64(Session["SESunitateId"]), "8");
        // facem un string cu campurile modificate in c7
        string vRanduriC7 = "";

        foreach (string vCeScriem in AnimaleInC7())
        {
           // gasim linia de capitol pentru randul care se va updata 
           List<string> vCampuri = new List<string> { "capitolId", "col1", "col2" };
           List<string> vValori = ManipuleazaBD.fRezultaUnString("SELECT capitole.capitolId, capitole.col1, capitole.col2 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + Session["SESgospodarieId"].ToString() + "') AND (sabloaneCapitole.capitol = '7') AND (capitole.codRand = '" + vCeScriem + "')", vCampuri, Convert.ToInt16(Session["SESan"]));
           // daca data operarii e mai devreme de semestrul II trecem aceeasi valoare in col1 si col2, daca e dupa, operam doar in col2
           // daca suntem in semestrul 2 initializam dataFatarii cu 01.07.anul, daca nu, 01.01.anul
           int vSemI = 0;
           int vSemII = 0;
            // daca e data fatarii inainte iulie anul sesiunii
           if (DateTime.Now < Convert.ToDateTime("01.07." + Session["SESan"].ToString()))
           { 
                // trecem doar in sem I
               vSemI = Convert.ToInt32(Convert.ToDecimal(vValori[1].Replace('.', ','))) + Convert.ToInt32(Convert.ToDecimal(tbNrBucati.Text.Replace('.', ',')));
               vSemII = Convert.ToInt32(Convert.ToDecimal(vValori[2].Replace('.', ',')));
           }
           else if (Convert.ToDateTime(tbDataOperare.Text) < Convert.ToDateTime("01.07." + Session["SESan"].ToString()))
           {
               // trecem in sem I si II
               vSemI = Convert.ToInt32(Convert.ToDecimal(vValori[1].Replace('.', ','))) + Convert.ToInt32(Convert.ToDecimal(tbNrBucati.Text.Replace('.', ',')));
               vSemII = Convert.ToInt32(Convert.ToDecimal(vValori[2].Replace('.', ','))) + Convert.ToInt32(Convert.ToDecimal(tbNrBucati.Text.Replace('.', ',')));
           }
           else if (Convert.ToDateTime(tbDataOperare.Text) >= Convert.ToDateTime("01.07." + Session["SESan"].ToString()))
           {
               // trecem doar in sem II
               vSemI = Convert.ToInt32(Convert.ToDecimal(vValori[1].Replace('.', ',')));
               vSemII = Convert.ToInt32(Convert.ToDecimal(vValori[2].Replace('.', ','))) + Convert.ToInt32(Convert.ToDecimal(tbNrBucati.Text.Replace('.', ',')));
           }
            
            // scriem in 7; adunam la ce exista
            // la adaugare ---
           ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vSemI.ToString().Replace(',', '.') + "', col2 = '" + vSemII.ToString().Replace(',', '.') + "' WHERE (capitolId = '" + vValori[0] + "')", Convert.ToInt16(Session["SESan"]));
           // stringul pt codurile de randuri
            vRanduriC7 += vCeScriem + "#";
        }
        // facem update in animale pt codCapitol
        foreach (int vAnimal in vListaAnimaleInserate)
        {
            ManipuleazaBD.fManipuleazaBD("UPDATE animale SET codCapitol = '" + vRanduriC7 + "' WHERE animalId = '" + vAnimal.ToString() + "' ", Convert.ToInt16(Session["SESan"]));
        }


        foreach (string vCeScriem in AnimaleInC8())
        {
            // gasim linia de capitol pentru randul care se va updata 
            List<string> vCampuri = new List<string> { "capitolId", "col1", "col2" };
            List<string> vValori = ManipuleazaBD.fRezultaUnString("SELECT capitole.capitolId, capitole.col1, capitole.col2 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + Session["SESgospodarieId"].ToString() + "') AND (sabloaneCapitole.capitol = '8') AND (capitole.codRand = '" + vCeScriem + "')", vCampuri, Convert.ToInt16(Session["SESan"]));
            // daca data operarii e mai devreme de semestrul II trecem aceeasi valoare in col1 si col2, daca e dupa, operam doar in col2
            // daca suntem in semestrul 2 initializam dataFatarii cu 01.07.anul, daca nu, 01.01.anul
            int vSemI = 0;
            int vSemII = 0;
            // daca e data fatarii inainte iulie anul sesiunii
            if (DateTime.Now < Convert.ToDateTime("01.07." + Session["SESan"].ToString()))
            {
                // trecem doar in sem I
                vSemI = Convert.ToInt32(Convert.ToDecimal(vValori[1].Replace('.', ','))) + Convert.ToInt32(Convert.ToDecimal(tbNrBucati.Text.Replace('.', ',')));
                vSemII = Convert.ToInt32(Convert.ToDecimal(vValori[2].Replace('.', ',')));
            }
            else if (Convert.ToDateTime(tbDataOperare.Text) < Convert.ToDateTime("01.07." + Session["SESan"].ToString()))
            {
                // trecem in sem I si II
                vSemI = Convert.ToInt32(Convert.ToDecimal(vValori[1].Replace('.', ','))) + Convert.ToInt32(Convert.ToDecimal(tbNrBucati.Text.Replace('.', ',')));
                vSemII = Convert.ToInt32(Convert.ToDecimal(vValori[2].Replace('.', ','))) + Convert.ToInt32(Convert.ToDecimal(tbNrBucati.Text.Replace('.', ',')));
            }
            else if (Convert.ToDateTime(tbDataOperare.Text) >= Convert.ToDateTime("01.07." + Session["SESan"].ToString()))
            {
                // trecem doar in sem II
                vSemI = Convert.ToInt32(Convert.ToDecimal(vValori[1].Replace('.', ',')));
                vSemII = Convert.ToInt32(Convert.ToDecimal(vValori[2].Replace('.', ','))) + Convert.ToInt32(Convert.ToDecimal(tbNrBucati.Text.Replace('.', ',')));
            }

            // scriem in 7; adunam la ce exista
            // la adaugare ---
            ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vSemI.ToString().Replace(',', '.') + "', col2 = '" + vSemII.ToString().Replace(',', '.') + "' WHERE (capitolId = '" + vValori[0] + "')", Convert.ToInt16(Session["SESan"]));
            // stringul pt codurile de randuri
            vRanduriC7 += vCeScriem + "#";
        }


        pnAdauga.Visible = false;
        pnLista.Visible = true;
        gv.DataBind();
        gv.SelectedIndex = -1;
        btModifica.Visible = false;
    }
    public void ScriemInCap7(string pRand, int pTipOperatie, int pCapete, int pCapeteInainte, int pSemestru)
    { 
        //adaugam la cap7 
        // 1 adaugare, 2 modificare, 3 stergere
        // semestru 1 semI, 2 semII
        // calculam in functie de semestru
        switch (pTipOperatie)
        {
            case 1:
                // luam din Cap7 existentul la care adaugam suma noua
                int vExistent = Convert.ToInt32(ManipuleazaBD.fRezultaUnString("SELECT TOP(1) col" + pSemestru.ToString() + " FROM capitole WHERE gospodarieId = '" + Session["SESgospodarieId"].ToString() + "' AND unitateId='" + Session["SESunitateId"].ToString() + "' AND codCapitol='7' AND codRand = '" + pRand + "'", "col" + pSemestru.ToString(), Convert.ToInt16(Session["SESan"])));
                // scriem in Cap7 suma existent  + pCapete
                vExistent += pCapete;

                break;
            case 2:

                break;
            case 3:

                break;
        }
    }
    protected List<string> AnimaleInC7()
    {
        // luam datele din paneluri si completam o lista
        // 0 - nr. randului, 1 capete
        string vAnimale = "";
        List<string> vListaRanduri = new List<string>();
        // in functie de categoria aleasa
      //  vListaRanduri.Add(tbNrBucati.Text);
        switch (ddlCategoria.SelectedValue)
        { 
            case "1":
                // bovine
                // trecem la 1 toate
                vAnimale = "1";
                vListaRanduri.Add(vAnimale);
                if (ddlSex.SelectedValue == "femele")
                {
                    // bovine femele
                    // varsta 1
                    switch (ddlDetaliiBovineFemeleVarsta1.SelectedValue)
                    {
                        case "4":
                            // sub 6 luni / se adauga la randurile 3 si 4
                            // daca e bifat sacrificare se adauga si la 7
                            vAnimale = "3";
                            vListaRanduri.Add(vAnimale);
                            vAnimale = "4";
                            vListaRanduri.Add(vAnimale);
                            if (ckbVitei.Checked)
                            {
                                vAnimale = "7";
                                vListaRanduri.Add(vAnimale);
                            }
                            break;
                        case "3":
                            // intre 6 luni si 1 an / se adauga la randul 3 
                            // daca e bifat sacrificare se adauga si la 7
                            vAnimale = "3";
                            vListaRanduri.Add(vAnimale);
                            if (ckbVitei.Checked)
                            {
                                vAnimale = "7";
                                vListaRanduri.Add(vAnimale);
                            }
                            break;
                        case "9":
                            // intre 1 si 2 ani / se adauga la randul 9 si 8 
                            vAnimale = "8";
                            vListaRanduri.Add(vAnimale);
                            vAnimale = "9";
                            vListaRanduri.Add(vAnimale);
                            break;
                        case "0":
                            // peste 2 ani / se adauga la randul 11, 17
                            vAnimale = "11";
                            vListaRanduri.Add(vAnimale);
                            vAnimale = "17";
                            vListaRanduri.Add(vAnimale);
                            // daca juninci la 18
                            if (ddlDetaliiBovineFemeleVarsta2.SelectedValue == "18")
                            {
                                vAnimale = "18";
                                vListaRanduri.Add(vAnimale);
                            }
                            else
                            {
                                vAnimale = "19";
                                vListaRanduri.Add(vAnimale);
                                // daca sunt vaci se srie si in 20 (lapte) sau 21 (reformate)
                                if (ddlDetaliiBovineFemeleVarsta3.SelectedValue == "20") vAnimale = "20";
                                else vAnimale = "21";
                                vListaRanduri.Add(vAnimale);
                            }
                            break;
                    }
                }
                else
                {
                    // bovine masculi
                    // varsta 1
                    switch (ddlDetaliiBovineMasculiVarsta1.SelectedValue)
                    {
                        case "6":
                            // sub 6 luni / se adauga la randurile 6 si 5
                            // daca e bifat sacrificare se adauga si la 7
                            vAnimale = "5";
                            vListaRanduri.Add(vAnimale);
                            vAnimale = "6";
                            vListaRanduri.Add(vAnimale);
                            if (ckbVitei.Checked)
                            {
                                vAnimale = "7";
                                vListaRanduri.Add(vAnimale);
                            }
                            break;
                        case "5":
                            // intre 6 luni si 1 an / se adauga la randul 5 
                            // daca e bifat sacrificare se adauga si la 7
                            vAnimale = "5";
                            vListaRanduri.Add(vAnimale);
                            if (ckbVitei.Checked)
                            {
                                vAnimale = "7";
                                vListaRanduri.Add(vAnimale);
                            }
                            break;
                        case "10":
                            // intre 1 si 2 ani / se adauga la randul 10 si 8 
                            vAnimale = "8";
                            vListaRanduri.Add(vAnimale);
                            vAnimale = "10";
                            vListaRanduri.Add(vAnimale);
                            break;
                        case "0":
                            // peste 2 ani / se adauga la randul 11, 12
                            vAnimale = "11";
                            vListaRanduri.Add(vAnimale);
                            vAnimale = "12";
                            vListaRanduri.Add(vAnimale);
                            switch (ddlDetaliiBovineMasculiVarsta2.SelectedValue)
                            {
                                // reproducere autorizati
                                case "130":
                                    vAnimale = "13";
                                    vListaRanduri.Add(vAnimale);
                                    vAnimale = "14";
                                    vListaRanduri.Add(vAnimale);
                                    break;
                                    // reproducere altii
                                case "131":
                                    vAnimale = "13";
                                    vListaRanduri.Add(vAnimale);
                                    break;
                                    // reformati
                                case "15":
                                    vAnimale = "15";
                                    vListaRanduri.Add(vAnimale);
                                    break;
                                    // munca
                                case "16":
                                    vAnimale = "16";
                                    vListaRanduri.Add(vAnimale);
                                    break;
                            }
                            break;
                    }
                }
                break;
            case "2":
                // ovine
                // trecem la 26 toate
                vAnimale = "26";
                vListaRanduri.Add(vAnimale);
                // luam valoarea randului din ddl
                vAnimale = ddlDetaliiOvine.SelectedValue;
                vListaRanduri.Add(vAnimale);
                break;
            case "3":
                // caprine
                // trecem la 32 toate
                vAnimale = "32";
                vListaRanduri.Add(vAnimale);
                // luam valoarea randului din ddl
                vAnimale = ddlDetaliiCaprine.SelectedValue;
                vListaRanduri.Add(vAnimale);
                break;
            case "4":
                // porcine
                // trecem la 37 toate
                vAnimale = "37";
                vListaRanduri.Add(vAnimale);
                // luam valoarea randului din ddl pt primul nivel de ddl (38, 39, 41 si 45)
                if (ddlDetaliiPorcine.SelectedValue != "40")
                {
                    vAnimale = ddlDetaliiPorcine.SelectedValue;
                    vListaRanduri.Add(vAnimale);
                }
                else
                {
                    vAnimale = "39";
                    vListaRanduri.Add(vAnimale);
                    vAnimale = "40";
                    vListaRanduri.Add(vAnimale);
                }
                
                // daca sunt ultimele doua trecem la urmatorul nivel
                switch (ddlDetaliiPorcine.SelectedValue)
                {
                    case "41":
                        vAnimale = ddlDetaliiPorcineDeIngrasat.SelectedValue;
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "45":
                        vAnimale = ddlDetaliiPorcineDeReproductie.SelectedValue;
                        vListaRanduri.Add(vAnimale);
                        if (ckbScrofite6_9luni.Checked)
                        {
                            // adaugam si la scrofite 6-9luni
                            vAnimale = "49";
                            vListaRanduri.Add(vAnimale);
                        }
                        break;
                }
                break;
            case "5":
                // cabaline
                // trecem la 50 toate
                vAnimale = "50";
                vListaRanduri.Add(vAnimale);
                // luam valoarea randului din ddl
                if (ddlDetaliiCabaline.SelectedValue != "50")
                {
                    vAnimale = ddlDetaliiCabaline.SelectedValue;
                    vListaRanduri.Add(vAnimale);
                }
                break;
            case "6":
                // catari
                vAnimale = "54";
                vListaRanduri.Add(vAnimale);
                break;
            case "7":
                // magari
                vAnimale = "53";
                vListaRanduri.Add(vAnimale);
                break;
            case "8":
                // iepuri
                // trecem la 55 toate
                vAnimale = "55";
                vListaRanduri.Add(vAnimale);
                // luam valoarea randului din ddl
                if (ddlDetaliiIepuri.SelectedValue == "56")
                {
                    vAnimale = "56";
                    vListaRanduri.Add(vAnimale);
                }
                break;
            case "9":
                // animale de blana
                vAnimale = "57";
                vListaRanduri.Add(vAnimale);
                switch (ddlRasa.SelectedValue)
                {
                    // in ddlrasa sunt id-urile din animale nu randurile reale din capitole
                    case "75":
                        vAnimale = "59"; // nurca 
                        break;
                    case "76":
                        vAnimale = "58"; // vulpe polara
                        break;
                    case "77":
                        vAnimale = "58"; // vulpe argintie
                        break;
                    case "78":
                        vAnimale = "61"; // dihor
                        break;
                    case "92":
                        vAnimale = "61"; // altele
                        break;
                    case "99":
                        vAnimale = "58"; // alte vulpi
                        break;
                    case "100":
                        vAnimale = "60"; // nutrii
                        break;
                }
                vListaRanduri.Add(vAnimale);
                break;
            case "10":
                // pasari
                // trecem la 62 toate
                vAnimale = "62";
                vListaRanduri.Add(vAnimale);
                // luam valoarea randului din ddlRasa
                switch (ddlRasa.SelectedValue)
                {
                        // in ddlrasa sunt id-urile din animale nu randurile reale din capitole
                    case "79":
                        vAnimale = "63"; // gaini
                        break;
                    case "80":
                        vAnimale = "64";
                        break;
                    case "81":
                        vAnimale = "65";
                        break;
                    case "82":
                        vAnimale = "66";
                        break;
                    case "83":
                        vAnimale = "67";
                        break;
                    case "93":
                        vAnimale = "68"; // alte pasari
                        break;
                }
                vListaRanduri.Add(vAnimale);
                // ouatoare - gaini oautoare
                if (ckbGainiOuatoare.Checked)
                {
                    vListaRanduri.Add("69");
                    vListaRanduri.Add("70");
                }
                else if (ckbPasariOuatoare.Checked)
                    vListaRanduri.Add("69");
                break;
            case "11":
                // albine
                vAnimale = "72";
                vListaRanduri.Add(vAnimale);
                break;
            case "12":
                // alte animale
                vAnimale = "71";
                vListaRanduri.Add(vAnimale);
                break;
            case "15":
                // bubaline
                // trecem la 22 toate
                vAnimale = "22";
                vListaRanduri.Add(vAnimale);
                // luam valoarea randului din ddl
                vAnimale = ddlBubaline.SelectedValue;
                vListaRanduri.Add(vAnimale);
                break;
        }
        return vListaRanduri;
    }

    protected List<string> AnimaleInC8()
    {
        // luam datele din paneluri si completam o lista
        // 0 - nr. randului, 1 capete
      
        List<string> vListaRanduri = new List<string>();
        string vAnimale = "";
        
        switch (ddlOperatieDenumire.SelectedValue)
        {
            case "1":
                //obtinuti
                switch (ddlCategoria.SelectedValue)
                {
                    case "1":
                    // bovine / randurile 9, 2
                        vAnimale = "2";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "9";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "15":
                        // bubaline / randurile 9, 2
                        vAnimale = "2";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "9";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "2":
                        // ovine / 19.26
                        vAnimale = "19";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "26";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "3":
                        // caprine / 28.35
                        vAnimale = "28";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "35";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "4":
                        // porcine / 37.44
                        vAnimale = "37";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "44";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "5":
                        // cabaline / 46.53
                        vAnimale = "46";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "53";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "10":
                        // pasari / 55.62
                        vAnimale = "55";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "62";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "12":
                        // alte animale / 64.71
                        vAnimale = "64";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "71";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "11":
                        // albine / 73.79
                        vAnimale = "73";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "79";
                        vListaRanduri.Add(vAnimale);
                        break;
                }
                break;
            case "2":
                // cumparat
                switch (ddlCategoria.SelectedValue)
                {
                    case "1":
                        // bovine / randurile 9, 3
                        vAnimale = "3";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "9";
                        vListaRanduri.Add(vAnimale);
                        // daca sunt vaci
                        if (ddlSex.SelectedValue == "femele" && ddlDetaliiBovineFemeleVarsta1.SelectedValue == "0" && ddlDetaliiBovineFemeleVarsta2.SelectedValue == "19")
                        {
                            vAnimale = "11";
                            vListaRanduri.Add(vAnimale); 
                            vAnimale = "17";
                            vListaRanduri.Add(vAnimale);
                        }
                        break;
                    case "15":
                        // bubaline / randurile 9, 3
                        vAnimale = "3";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "9";
                        vListaRanduri.Add(vAnimale);
                        // bivolite
                        if (ddlBubaline.SelectedValue == "23")
                        {
                            vAnimale = "11";
                            vListaRanduri.Add(vAnimale);
                            vAnimale = "17";
                            vListaRanduri.Add(vAnimale);
                        }
                        break;
                    case "2":
                        // ovine / 20.26
                        vAnimale = "20";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "26";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "3":
                        // caprine / 29.35
                        vAnimale = "29";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "35";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "4":
                        // porcine / 38.44
                        vAnimale = "38";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "44";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "5":
                        // cabaline / 47.53
                        vAnimale = "47";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "53";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "10":
                        // pasari / 56.62
                        vAnimale = "56";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "62";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "12":
                        // alte animale / 65.71
                        vAnimale = "65";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "71";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "11":
                        // albine / 74.79
                        vAnimale = "74";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "79";
                        vListaRanduri.Add(vAnimale);
                        break;
                } 
                break;
            case "3":
                // alta intrare
                switch (ddlCategoria.SelectedValue)
                {
                    case "1":
                        // bovine / randurile 9, 4
                        vAnimale = "4";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "9";
                        vListaRanduri.Add(vAnimale);
                        // daca sunt vaci
                        if (ddlSex.SelectedValue == "femele" && ddlDetaliiBovineFemeleVarsta1.SelectedValue == "0" && ddlDetaliiBovineFemeleVarsta2.SelectedValue == "19")
                        {
                            vAnimale = "12";
                            vListaRanduri.Add(vAnimale);
                            vAnimale = "17";
                            vListaRanduri.Add(vAnimale);
                        }
                        break;
                    case "15":
                        // bubaline / randurile 9, 4
                        vAnimale = "4";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "9";
                        vListaRanduri.Add(vAnimale);
                        // bivolite
                        if (ddlBubaline.SelectedValue == "23")
                        {
                            vAnimale = "12";
                            vListaRanduri.Add(vAnimale);
                            vAnimale = "17";
                            vListaRanduri.Add(vAnimale);
                        }
                        break;
                    case "2":
                        // ovine / 21.26
                        vAnimale = "21";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "26";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "3":
                        // caprine / 30.35
                        vAnimale = "30";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "35";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "4":
                        // porcine / 39.44
                        vAnimale = "39";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "44";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "5":
                        // cabaline / 48.53
                        vAnimale = "48";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "53";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "10":
                        // pasari / 57.62
                        vAnimale = "57";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "62";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "12":
                        // alte animale / 66.71
                        vAnimale = "66";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "71";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "11":
                        // albine / 75.79
                        vAnimale = "75";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "79";
                        vListaRanduri.Add(vAnimale);
                        break;
                } 
                break;
            case "8":
                // existent la inceput
                switch (ddlCategoria.SelectedValue)
                {
                    case "1":
                        // bovine / randurile 9, 1
                        vAnimale = "1";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "9";
                        vListaRanduri.Add(vAnimale);
                        // daca sunt vaci
                        if (ddlSex.SelectedValue == "femele" && ddlDetaliiBovineFemeleVarsta1.SelectedValue == "0" && ddlDetaliiBovineFemeleVarsta2.SelectedValue == "19")
                        {
                            vAnimale = "10";
                            vListaRanduri.Add(vAnimale);
                            vAnimale = "17";
                            vListaRanduri.Add(vAnimale);
                        }
                        break;
                    case "15":
                        // bubaline / randurile 9, 1
                        vAnimale = "1";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "9";
                        vListaRanduri.Add(vAnimale);
                        if (ddlBubaline.SelectedValue == "23")
                        {
                            vAnimale = "10";
                            vListaRanduri.Add(vAnimale);
                            vAnimale = "17";
                            vListaRanduri.Add(vAnimale);
                        }
                        break;
                    case "2":
                        // ovine / 18.26
                        vAnimale = "18";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "26";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "3":
                        // caprine / 27.35
                        vAnimale = "27";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "35";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "4":
                        // porcine / 36.44
                        vAnimale = "36";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "44";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "5":
                        // cabaline / 45.53
                        vAnimale = "45";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "53";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "10":
                        // pasari / 54.62
                        vAnimale = "54";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "62";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "12":
                        // alte animale / 63.71
                        vAnimale = "63";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "71";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "11":
                        // albine / 72.79
                        vAnimale = "72";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "79";
                        vListaRanduri.Add(vAnimale);
                        break;
                } break;
        }

        return vListaRanduri;
    }

    
    public static void PreluamCap8(string pGospodarieId)
    { 
        // daca este prima inregistrare in animale stergem tot din cap7 si 8

    }
    #endregion
    protected void ArataLista(object sender, EventArgs e)
    {
        pnAdauga.Visible = false;
        pnLista.Visible = true;
        btModifica.Visible = false;
        btStergeArata.Visible = false;
        gv.DataBind();
        gv.SelectedIndex = -1;
    }
    protected void ddlCategoria_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlCategoriaSchimba();
    }
    protected void ddlCategoriaSchimba()
    {
        lblAscunsTextExemplu.Text = ManipuleazaBD.fRezultaUnString("SELECT animaleCategorieExemplu + animaleCategorieExempluElectronic as text FROM animaleCategorii WHERE animaleCategorieId = '" + ddlCategoria.SelectedValue + "'", "text", Convert.ToInt16(Session["SESan"]));
        if (lblAscunsTextExemplu.Text != "--")
        {
            pnlCodIdentificare.Visible = true;
            lblNrBucatiMax.Visible = true;
            pnlDetaliiSex.Visible = true;
            AscundePaneluriCategorii(PanelDetaliiCategoria(ddlCategoria.SelectedValue));
        }
        else
        { pnlCodIdentificare.Visible = false; lblNrBucatiMax.Visible = false; pnlDetaliiSex.Visible = false; AscundePaneluriCategorii(PanelDetaliiCategoria(ddlCategoria.SelectedValue)); }
        // aflam daca trebuie diferentiate pe sexe
        string[] vRezultate = { "animaleCategorieSexe", "animaleCategorieSubCategorii" };
        List<string> vSexe = ManipuleazaBD.fRezultaUnString("SELECT animaleCategorieSexe, animaleCategorieSubCategorii FROM animaleCategorii WHERE animaleCategorieId = '" + ddlCategoria.SelectedValue + "'", vRezultate, Convert.ToInt16(Session["SESan"]));
        if (vSexe[0] == "1") { pnlDetaliiSex.Visible = true; }
        else pnlDetaliiSex.Visible = false;
    }
    protected void ddlTara_Init(object sender, EventArgs e)
    {
        ((DropDownList)sender).SelectedValue = "1";
    }
    protected void ddlTara_DataBound(object sender, EventArgs e)
    {

    }
    protected void ddlCategoria_Init(object sender, EventArgs e)
    {
    }

    protected void ddlCategoria_DataBound(object sender, EventArgs e)
    {
        lblAscunsTextExemplu.Text = ManipuleazaBD.fRezultaUnString("SELECT animaleCategorieExemplu + animaleCategorieExempluElectronic as text FROM animaleCategorii WHERE animaleCategorieId = '" + ddlCategoria.SelectedValue + "'", "text", Convert.ToInt16(Session["SESan"]));
        // daca nu e categorie pt care trebuie cod si daca se introduce un singur animal
      if (lblAscunsTextExemplu.Text != "--")
      { 
          pnlCodIdentificare.Visible = true; 
          lblNrBucatiMax.Visible = true; 
         // pnlDetaliiSex.Visible = true;
          AscundePaneluriCategorii(PanelDetaliiCategoria(ddlCategoria.SelectedValue));
      }
      else
      { pnlCodIdentificare.Visible = false; lblNrBucatiMax.Visible = false; pnlDetaliiSex.Visible = false; }
        // aflam daca trebuie diferentiate pe sexe
      string[] vRezultate = { "animaleCategorieSexe", "animaleCategorieSubCategorii" };
      List<string> vSexe = ManipuleazaBD.fRezultaUnString("SELECT animaleCategorieSexe, animaleCategorieSubCategorii FROM animaleCategorii WHERE animaleCategorieId = '" + ddlCategoria.SelectedValue + "'", vRezultate, Convert.ToInt16(Session["SESan"]));
      if (vSexe[0] == "1") { pnlDetaliiSex.Visible = true; }
      else pnlDetaliiSex.Visible = false;

       
    }
    protected Panel PanelDetaliiCategoria( string pCategoria)
    {
        // alegem ce panel sa aratam
        Panel pPanel = new Panel();
        switch (pCategoria)
        {
            case "1":
                pPanel = pnlDetaliiCategoria1;
                break;
            case "2":
                pPanel = pnlDetaliiCategoria2;
                break;
            case "3":
                pPanel = pnlDetaliiCategoria3;
                break;
            case "4":
                pPanel = pnlDetaliiCategoria4;
                break;
            case "5":
                pPanel = pnlDetaliiCategoria5;
                break;
            case "8":
                pPanel = pnlDetaliiCategoria8;
                break;
            case "10":
                pPanel = pnlDetaliiCategoria10;
                break;
            case "15":
                pPanel = pnlDetaliiCategoria15;
                break;
           
        }
        return pPanel;
    }

    protected void cbCatreNecunoscuta_Load(object sender, EventArgs e)
    {
        if (cbCatreNecunoscuta.Checked == true)
        {
            ddlNrPozitie.Enabled = false;
            ddlVolum.Enabled = false;
            lblTextNecunoscuta.CssClass = lblTextNecunoscuta.CssClass.Replace("taiata", "rosie");
            lblCatreScadeti.Visible = false;
            ckbCatreScadeti.Visible = false;
        }
        else
        {
          /*  lblCatreScadeti.Visible = true;
            ckbCatreScadeti.Visible = true;
           */ ddlVolum.Enabled = true;
            ddlNrPozitie.Enabled = true;
            lblTextNecunoscuta.CssClass = lblTextNecunoscuta.CssClass.Replace("rosie", "taiata");
        }

    }

    protected void ddlOperatieTip_DataBound(object sender, EventArgs e)
    {
    }
    protected void ddlOperatieTip_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlOperatieDenumire.SelectedIndex = 0;
        if (ddlOperatieTip.SelectedValue == "intrare")
        {
            lblTextCatre.Text = "de la gospodăria";
        }
        else
        {
            lblTextCatre.Text = "către gospodăria";
        }

    }
    protected void ddlOperatieTip_Init(object sender, EventArgs e)
    {
    }
    protected void lblTextCatre_Load(object sender, EventArgs e)
    {
        if (ddlOperatieTip.SelectedValue == "intrare")
        {
          //  lblTextCatre.Text = "de la gospodăria";
        }
        else
        {
           // lblTextCatre.Text = "către gospodăria";
        }
    }
    protected void ddlOperatieDenumire_Load(object sender, EventArgs e)
    {

    }
    protected void ddlOperatieDenumire_PreRender(object sender, EventArgs e)
    {
        if (ddlOperatieDenumire.SelectedValue == "1" || ddlOperatieDenumire.SelectedValue == "5" || ddlOperatieDenumire.SelectedValue == "6" || ddlOperatieDenumire.SelectedValue == "8")
        {
            lblTextCatre.Visible = false;
            ddlVolum.Visible = false;
            ddlNrPozitie.Visible = false;
            lblTextNrPozitie.Visible = false;
            lblTextVolum.Visible = false;
            lblTextNecunoscuta.Visible = false;
            cbCatreNecunoscuta.Visible = false;
        }
        else
        {            
            ddlVolum.Visible = true;
            ddlNrPozitie.Visible = true;
            lblTextCatre.Visible = true;
            lblTextNrPozitie.Visible = true;
            lblTextVolum.Visible = true;
            lblTextNecunoscuta.Visible = true;
            cbCatreNecunoscuta.Visible = true;
        }
        if (ddlOperatieTip.SelectedValue == "intrare")
        {
            lblTextCatre.Text = "de la gospodăria";
        }
        else
        {
            lblTextCatre.Text = "către gospodăria";
        }
    }
    protected void tbNrBucati_TextChanged(object sender, EventArgs e)
    {
      
        // daca nu e categorie pt care trebuie cod si daca se introduce un singur animal
        if (lblAscunsTextExemplu.Text != "--")
        {
            lblNrBucatiMax.Visible = true;
            pnlCodIdentificare.Visible = true;   
            try
            {
                if (Convert.ToInt16(tbNrBucati.Text) > 20) tbNrBucati.Text = "20";
            }
            catch
            { tbNrBucati.Text = "1"; }
        }
        else
        { pnlCodIdentificare.Visible = false; lblNrBucatiMax.Visible = false; }
        SqlTaraSelectata_Init(sender, e);
        dlTaraDenumire.DataBind();
        
        //((DropDownList)dlTaraDenumire.FindControl("ddlTara")).SelectedValue = "1";
     //  
    }
    protected void tbNrBucati_Load(object sender, EventArgs e)
    {
        // daca nu e categorie pt care trebuie cod si daca se introduce un singur animal
        if (lblAscunsTextExemplu.Text != "--")
        {
            lblNrBucatiMax.Visible = true;
            pnlCodIdentificare.Visible = true;
            try
            {
                if (Convert.ToInt16(tbNrBucati.Text) > 20) tbNrBucati.Text = "20";
            }
            catch
            { tbNrBucati.Text = "1"; }
        }
        else
        { pnlCodIdentificare.Visible = false; lblNrBucatiMax.Visible = false; }
        SqlTaraSelectata_Init(sender, e);
    //    dlTaraDenumire.DataBind();

        //((DropDownList)dlTaraDenumire.FindControl("ddlTara")).SelectedValue = "1";
    }
    protected void SqlTaraSelectata_Init(object sender, EventArgs e)
    {
        string vTara = "1";
        try
        {
           // vTara = ddlTara.SelectedValue;
        }
        catch
        { }
        if (vTara == "") vTara = "1";
        SqlTaraSelectata.SelectCommand = "SELECT  TOP(" + tbNrBucati.Text + ")   animaleTari.animaleTaraDenumire, animaleTari.animaleTaraId, animaleTari.animaleTaraCodA3, animaleTari.animaleTaraNumar, animaleTari.animaleTaraCodA2, grid.id FROM  animaleTari CROSS JOIN grid WHERE     (animaleTari.animaleTaraId = '" + vTara + "')ORDER BY animaleTari.animaleTaraCodA2";
    }
    protected void dlTaraDenumire_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void ddlTara_SelectedIndexChanged(object sender, EventArgs e)
    {
        ((Label)((DropDownList)sender).Parent.FindControl("animaleTaraNumarLabel")).Text = ManipuleazaBD.fRezultaUnString("SELECT animaleTaraNumar FROM animaleTari WHERE animaleTaraId = '" + ((DropDownList)sender).SelectedValue + "'", "animaleTaraNumar", Convert.ToInt16(Session["SESan"])); 

        
    }
    protected void ddlSex_Load(object sender, EventArgs e)
    {
        List<Panel> vListaPaneluri = new List<Panel>();
        if (ddlSex.SelectedValue == "femele")
        {
            switch (ddlCategoria.SelectedValue)
            {
                case "1":
                    vListaPaneluri.Add(pnlDetaliiBovineFemele);
                    AscundePaneluriDetalii(vListaPaneluri);
                    break;
                case "15":
                  //  AscundePaneluriDetalii(pnlDetaliiBubaline);
                    break;
            }
        }
        else
        {
            switch (ddlCategoria.SelectedValue)
            {
                case "1":
                    vListaPaneluri.Add(pnlDetaliiBovineMasculi);
                    AscundePaneluriDetalii(vListaPaneluri);
                    break;
                case "15":
                  //  AscundePaneluriDetalii(pnlDetaliiBubaline);
                    break;
            }
        }
    }
    protected void ddlSex_SelectedIndexChanged(object sender, EventArgs e)
    {
        // aratam panelurile de femele sau masculi, in functie de categorie
        List<Panel> vListaPaneluri = new List<Panel>();
        if (ddlSex.SelectedValue == "femele")
        {
            switch (ddlCategoria.SelectedValue)
            {
                case "1":
                    vListaPaneluri.Add(pnlDetaliiBovineFemele);
                    AscundePaneluriDetalii(vListaPaneluri);
                    break;
            }
        }
        else
        {
            switch (ddlCategoria.SelectedValue)
            {
                case "1":
                    vListaPaneluri.Add(pnlDetaliiBovineMasculi);
                    AscundePaneluriDetalii(vListaPaneluri);
                    break;
            }
        }
    }
    protected void AscundePaneluriDetalii(List<Panel> pPanelLasa)
    {
        pnlDetaliiBovineFemele.Visible = false;
        pnlDetaliiBovineMasculi.Visible = false;
        pnlDetaliiBubaline.Visible = false;
        pnlDetaliiOvine.Visible = false;
        pnlDetaliiPorcine.Visible = false;
        pnlDetaliiScrofite.Visible = false;
        pnlDetaliiPorcineDeIngrasat.Visible = false;
        pnlDetaliiPorcineDeReproductie.Visible = false;
        pnlDetaliiCabaline.Visible = false;
        pnlDetaliiCaprine.Visible = false;
        pnlDetaliiIepuri.Visible = false;
        pnlDetaliiPasari.Visible = false;
        // le ascund pe toate si pe urma il fac vizibil de cel precizat
        foreach (Panel vPanel in pPanelLasa)
        {
            vPanel.Visible = true;
        }
    }
    protected void AscundePaneluriCategorii(Panel pPanelLasa)
    {
        pnlDetaliiCategoria1.Visible = false; // bovine
        pnlDetaliiCategoria2.Visible = false; // ovine
        pnlDetaliiCategoria3.Visible = false; // caprine
        pnlDetaliiCategoria4.Visible = false; // porcine
        pnlDetaliiCategoria5.Visible = false; // cabaline
        pnlDetaliiCategoria8.Visible = false; // iepuri
        pnlDetaliiCategoria10.Visible = false; // pasari
        pnlDetaliiCategoria15.Visible = false; // bubaline
        // le ascund pe toate si pe urma il fac vizibil de cel precizat
        pPanelLasa.Visible = true;
        List<Panel> vListaPaneluri = new List<Panel>();
        // si arat si ce e in cele ce le las
        if (pPanelLasa == pnlDetaliiCategoria1)
        { 
            
        }
        else if (pPanelLasa == pnlDetaliiCategoria2)
        {
            vListaPaneluri.Add(pnlDetaliiOvine);
            AscundePaneluriDetalii(vListaPaneluri);
        }
        else if (pPanelLasa == pnlDetaliiCategoria3)
        {
            vListaPaneluri.Add(pnlDetaliiCaprine);
            AscundePaneluriDetalii(vListaPaneluri);
        }
        else if (pPanelLasa == pnlDetaliiCategoria4)
        {
            vListaPaneluri.Add(pnlDetaliiPorcine);
            AscundePaneluriDetalii(vListaPaneluri);
        }
        else if (pPanelLasa == pnlDetaliiCategoria5)
        {
            vListaPaneluri.Add(pnlDetaliiCabaline);
            AscundePaneluriDetalii(vListaPaneluri);
        }
        else if (pPanelLasa == pnlDetaliiCategoria8)
        {
            vListaPaneluri.Add(pnlDetaliiIepuri);
            AscundePaneluriDetalii(vListaPaneluri);
        }
        else if (pPanelLasa == pnlDetaliiCategoria10)
        {
            vListaPaneluri.Add(pnlDetaliiPasari);
            AscundePaneluriDetalii(vListaPaneluri);
        }
        else if (pPanelLasa == pnlDetaliiCategoria15)
        {
            vListaPaneluri.Add(pnlDetaliiBubaline);
            AscundePaneluriDetalii(vListaPaneluri);
        }
        else 
        { }

    }
    #region bovine
    protected void ddlDetaliiBovineFemeleVarsta1_SelectedIndexChanged(object sender, EventArgs e)
    {
        // daca sunt peste 2 ani aratam mai departe
        if (ddlDetaliiBovineFemeleVarsta1.SelectedValue == "0")
        {
            ddlDetaliiBovineFemeleVarsta2.Visible = true;
            if (ddlDetaliiBovineFemeleVarsta2.SelectedValue == "19") ddlDetaliiBovineFemeleVarsta3.Visible = true;
            else ddlDetaliiBovineFemeleVarsta3.Visible = false;
        }
        else
        {
            ddlDetaliiBovineFemeleVarsta2.Visible = false;
            ddlDetaliiBovineFemeleVarsta3.Visible = false;
        }
        if (ddlDetaliiBovineFemeleVarsta1.SelectedValue == "4" || ddlDetaliiBovineFemeleVarsta1.SelectedValue == "3")
        {
            ckbVitei.Visible = true;
            ckbVitei.Checked = false;
        }
        else
        {
            ckbVitei.Visible = false;
            ckbVitei.Checked = false;
        }
    }    
    protected void ddlDetaliiBovineFemeleVarsta2_SelectedIndexChanged(object sender, EventArgs e)
    {
        // daca sunt vaci aratam mai departe
        if (ddlDetaliiBovineFemeleVarsta2.SelectedValue == "19") ddlDetaliiBovineFemeleVarsta3.Visible = true;
        else ddlDetaliiBovineFemeleVarsta3.Visible = false;
    }
    protected void ddlDetaliiBovineMasculiVarsta1_SelectedIndexChanged(object sender, EventArgs e)
    {
        // daca sunt peste 2 ani aratam mai departe
        if (ddlDetaliiBovineMasculiVarsta1.SelectedValue == "0")
        {
            ddlDetaliiBovineMasculiVarsta2.Visible = true;
        }
        else
        {
            ddlDetaliiBovineMasculiVarsta2.Visible = false;
        }

        if (ddlDetaliiBovineMasculiVarsta1.SelectedValue == "5" || ddlDetaliiBovineMasculiVarsta1.SelectedValue == "6")
        {
            ckbVitei.Visible = true;
            ckbVitei.Checked = false;
        }
        else
        {
            ckbVitei.Visible = false;
            ckbVitei.Checked = false;
        }
    }
    #endregion 
    #region porcine
    protected void ddlDetaliiPorcine_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlDetaliiPorcine.SelectedValue == "41")
        {
            pnlDetaliiPorcineDeIngrasat.Visible = true;
            pnlDetaliiPorcineDeReproductie.Visible = false;
            pnlDetaliiScrofite.Visible = false;
        }
        else if (ddlDetaliiPorcine.SelectedValue == "45")
        {
            pnlDetaliiPorcineDeIngrasat.Visible = false;
            pnlDetaliiPorcineDeReproductie.Visible = true;
            if (ddlDetaliiPorcineDeReproductie.SelectedValue == "48")
                {
                    pnlDetaliiScrofite.Visible = true;
                    ckbScrofite6_9luni.Visible = true;
                }
                else
                {
                    pnlDetaliiScrofite.Visible = false;

                
                }
        }
        else
        {
            pnlDetaliiPorcineDeIngrasat.Visible = false;
            pnlDetaliiPorcineDeReproductie.Visible = false;
            pnlDetaliiScrofite.Visible = false;
            
        }
    }
    protected void ddlDetaliiPorcine_Load(object sender, EventArgs e)
    {
       if (ddlDetaliiPorcine.SelectedValue == "41")
        {
            pnlDetaliiPorcineDeIngrasat.Visible = true;
            pnlDetaliiPorcineDeReproductie.Visible = false;
            pnlDetaliiScrofite.Visible = false;
        }
        else if (ddlDetaliiPorcine.SelectedValue == "45")
        {

            pnlDetaliiPorcineDeIngrasat.Visible = false;
            pnlDetaliiPorcineDeReproductie.Visible = true;
            if (ddlDetaliiPorcineDeReproductie.SelectedValue == "48")
                {
                    pnlDetaliiScrofite.Visible = true;
                }
                else
                {
                    pnlDetaliiScrofite.Visible = false;
                }
        }
        else
        {
            pnlDetaliiPorcineDeIngrasat.Visible = false;
            pnlDetaliiPorcineDeReproductie.Visible = false;
            pnlDetaliiScrofite.Visible = false;
            
        }
    }
    protected void ddlDetaliiPorcineDeReproductie_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlDetaliiPorcineDeReproductie.SelectedValue == "48")
        {
            pnlDetaliiScrofite.Visible = true;
            ckbScrofite6_9luni.Visible = true;
        }
        else 
        {
            pnlDetaliiScrofite.Visible = false;
            ckbScrofite6_9luni.Visible = false;
        }
    }
    protected void ddlDetaliiPorcineDeReproductie_Load(object sender, EventArgs e)
    {
        if (ddlDetaliiPorcineDeReproductie.SelectedValue == "48")
        {
            pnlDetaliiScrofite.Visible = true;
            ckbScrofite6_9luni.Visible = true;
        }
        else
        {
            pnlDetaliiScrofite.Visible = false;
            ckbScrofite6_9luni.Visible = false;
        }
    }

    #endregion

    protected void gv_RowDataBound1(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(gv, e, this);
    }
    protected void gv_DataBound(object sender, EventArgs e)
    {
        // gasim numarul de capete pe toata 
        for (int i = 0; i < gv.Rows.Count; i++)
        {
            if (gv.Rows[i].RowType == DataControlRowType.DataRow)
            {
                try
                {
                    if (gv.Rows[i].Cells[4].Text.IndexOf("01.01.1900") != -1)
                        gv.Rows[i].Cells[4].Text = "necunoscută" ;
                    if (gv.Rows[i].Cells[2].Text.Trim().Length == 0 || gv.Rows[i].Cells[2].Text == "&nbsp;")
                        gv.Rows[i].Cells[2].Text = "necunoscut";
                }
                catch { }
            }
        }
    }
    protected void ddlCCategoria_PreRender(object sender, EventArgs e)
    {
        
    }
    protected void ddlCCategoria_Init(object sender, EventArgs e)
    {
        // populam ddlul manual
        ListItem vListItem = new ListItem();
        vListItem.Value = "%";
        vListItem.Text = "toate";
        ddlCCategoria.Items.Add(vListItem);
        // luam din categorii
        List<string> vCampuri = new List<string> {"animaleCategorieId","animaleCategorieDenumire"};
        List<List<string>> vListaValori = ManipuleazaBD.fRezultaListaStringuri("SELECT animaleCategorieId, animaleCategorieDenumire FROM animaleCategorii ORDER BY animaleCategorieDenumire", vCampuri, Convert.ToInt16(Session["SESan"]));
        foreach (List<string> vValoare in vListaValori)
        {
            vListItem = new ListItem();
            vListItem.Value = vValoare[0];
            vListItem.Text = vValoare[1];
            ddlCCategoria.Items.Add(vListItem);
        }
    }

    protected void gv_SelectedIndexChanged(object sender, EventArgs e)
    {
        btModifica.Visible = true;
        btStergeArata.Visible = true;
    }
    protected void SqlGv_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {

    }
    #region sterge
    protected void btStergeArata_Click(object sender, EventArgs e)
    {
        ViewState["tip"] = "s";
        pnAdauga.Visible = true;
        pnLista.Visible = false;
        pnSterge.Visible = true;
        pnAd1.Visible = false;
        Initializare(sender, e);
        x.Visible = true;
        tbDataIesiriiSterge.Text = DateTime.Now.ToString().Substring(0, 10);
        y.Visible = false;
        tbStergeDetalii.Text = "";
        pnlStergeGospodarie.Visible = true;
        tbStergeNrBucati.Text = gv.SelectedRow.Cells[3].Text;
        lblStergeNrBucatiMaxim.Text = gv.SelectedRow.Cells[3].Text;
        ckbStergeCatre.Checked = true;
        cbStergeCatreNecunoscuta_Load(sender, e);
        
    }
    protected void Sterge(object sender, EventArgs e)
    {
        int vAnimalId = -1;
        try
        {
            vAnimalId = Convert.ToInt32(((Label)gv.SelectedRow.Cells[1].FindControl("lblAnimalId")).Text);
        }
        catch
        { return; }
        string vEvolutieId = "";
        try
        {
            vEvolutieId = ((Label)gv.SelectedRow.Cells[1].FindControl("lblAnimaleEvolutieId")).Text;
        }
        catch
        { return; }
        
        // scoatem din gv numarul de bucati
        int vNrCapete = 0;
        try
        {
            vNrCapete = Convert.ToInt32(gv.SelectedRow.Cells[3].Text);
        }
        catch { }
        string vSpecie = "";
        try
        {
            // luam din gv specia
            vSpecie = ((Label)gv.SelectedRow.Cells[2].FindControl("lblRasaId")).Text;
            if (vSpecie.IndexOf("--- nu se cunoaşte ---") != -1)
                vSpecie = "0";
        }
        catch { }
        string vJudet = ManipuleazaBD.fRezultaUnString("SELECT judetId FROM unitati WHERE unitateId = '" + Session["SESunitateId"].ToString() + "'", "judetId", Convert.ToInt16(Session["SESan"]));

        string vGospodarieCatre = "0";
        if (!ckbStergeCatre.Checked)  vGospodarieCatre = ddlStergeNrPozitie.SelectedValue;
        else vGospodarieCatre = "0";

        

        string[] vCampuri1 = { "animaleCategorieId", "animaleRasaId", "animalSex", "codCapitol", "animaleOperatieId", "animaleOperatieData", "animaleEvolutieCapete", "gospodarieIdCatre", "animalDataFatarii", "animalCodIdentificare", "animaleTaraId", "animalId" };
        List<string> vValori1 = ManipuleazaBD.fRezultaUnString("SELECT animale.animalSex, animale.codCapitol, animale.animaleTaraId, animaleEvolutie.animaleRasaId, animale.animalCodIdentificare, animaleEvolutie.animalDataFatarii, animale.animaleCategorieId, animaleEvolutie.gospodarieIdCatre, animaleEvolutie.animaleEvolutieCapete, animaleEvolutie.animaleOperatieData, animaleEvolutie.animaleOperatieId, animale.animalId FROM animaleEvolutie INNER JOIN animale ON animaleEvolutie.animalId = animale.animalId WHERE (animaleEvolutie.animaleEvolutieId = '" + vEvolutieId + "')", vCampuri1, Convert.ToInt16(Session["SESan"]));

        y.Text = "sds";
        if (vAnimalId != -1)
        {
            ManipuleazaBD.fManipuleazaBD("UPDATE animaleEvolutie SET animaleEvolutieUltima = 'False' WHERE animalId = '" + vAnimalId.ToString() + "'", Convert.ToInt16(Session["SESan"]));
         //   ManipuleazaBD.fManipuleazaBD("UPDATE animale SET animaleDataFatarii = '" + tbDataFatarii.Text + "',  WHERE animalId = '" + vAnimalId.ToString() + "'");
          //  string vInsertEvolutie = "INSERT INTO animaleEvolutie ( animalId, unitateId, gospodarieId, an, animaleEvolutieData, animaleOperatieId, animaleOperatieData,animaleEvolutieUltima, animaleEvolutieDetalii) VALUES ('" + vAnimalId + "', '" + Session["SESunitateId"].ToString() + "','" + Session["SESgospodarieId"].ToString() + "','" + Session["SESan"].ToString() + "',  CONVERT(datetime,'" + DateTime.Now.ToString() + "', 104), '" + ddlOperatieDenumire.SelectedValue + "', CONVERT(datetime,'" + tbDataIesiriiSterge.Text + "', 104), 'False', '" + tbStergeDetalii.Text + "')";
            string vInsertEvolutie = "INSERT INTO animaleEvolutie ( animalCodIdentificare, animaleRasaId,animalDataFatarii, animalId, unitateId, gospodarieId, an, judetId, animaleEvolutieData, animaleOperatieId, animaleOperatieData, animaleEvolutieCapete, gospodarieIdCatre, animaleEvolutieUltima, animaleEvolutieDetalii) VALUES ('" + gv.SelectedRow.Cells[2].Text + "','" + vSpecie + "', CONVERT(datetime,'" + gv.SelectedRow.Cells[4].Text + "', 104),'" + vAnimalId + "', '" + Session["SESunitateId"].ToString() + "','" + Session["SESgospodarieId"].ToString() + "','" + Session["SESan"].ToString() + "','" + vJudet + "',  CONVERT(datetime,'" + DateTime.Now.ToString() + "', 104), '" + ddlTipOperatieSterge.SelectedValue + "', CONVERT(datetime,'" + tbDataIesiriiSterge.Text + "', 104), '" + Convert.ToInt32(Convert.ToDecimal(tbStergeNrBucati.Text)).ToString().Replace(',', '.') + "', '" + vGospodarieCatre + "', 'False', N'IESIRE: " + tbStergeDetalii.Text + "')";
            ManipuleazaBD.fManipuleazaBD(vInsertEvolutie, Convert.ToInt16(Session["SESan"])); // animale evolutie

            // daca e bifat transferul catre alta gospodarie mai scriem o data cu gospodariile inversate
            if (!ckbStergeCatre.Checked)
            {
                vInsertEvolutie = "INSERT INTO animaleEvolutie ( animalCodIdentificare, animaleRasaId,animalDataFatarii, animalId, unitateId, gospodarieId, an, judetId, animaleEvolutieData, animaleOperatieId, animaleOperatieData, animaleEvolutieCapete, gospodarieIdCatre, animaleEvolutieUltima, animaleEvolutieDetalii) VALUES ('" + gv.SelectedRow.Cells[2].Text + "','" + vSpecie + "', CONVERT(datetime,'" + gv.SelectedRow.Cells[4].Text + "', 104),'" + vAnimalId + "', '" + Session["SESunitateId"].ToString() + "','" + vGospodarieCatre + "','" + Session["SESan"].ToString() + "','" + vJudet + "',  CONVERT(datetime,'" + DateTime.Now.ToString() + "', 104), '" + ddlTipOperatieSterge.SelectedValue + "', CONVERT(datetime,'" + tbDataIesiriiSterge.Text + "', 104), '" + Convert.ToInt32(Convert.ToDecimal(tbStergeNrBucati.Text)).ToString().Replace(',', '.') + "', '" + Session["SESgospodarieId"].ToString() + "', 'True', N'INTRARE: " + tbStergeDetalii.Text + "')";
                ManipuleazaBD.fManipuleazaBD(vInsertEvolutie, Convert.ToInt16(Session["SESan"]));
            }

            // scadem din 7
            y.Text = vValori1[3];
            // stergem in 7 pentru cel scoate
            foreach (string vCeScriem in vValori1[3].Split('#'))
            {
                Int64[] vSemestre = ValoriSemestre(vCeScriem, -vNrCapete, "7", Session["SESgospodarieId"].ToString());
                // scriem in 7; 
                ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vSemestre[1].ToString().Replace(',', '.') + "', col2 = '" + vSemestre[2].ToString().Replace(',', '.') + "' WHERE (capitolId = '" + vSemestre[0].ToString().Replace(',', '.') + "')", Convert.ToInt16(Session["SESan"]));
            }
            // adaugam in 7 si 8 pentru cel ce primeste, daca este cazul
            if (!ckbStergeCatre.Checked && ddlTipOperatieSterge.SelectedValue == "4")
            {
                foreach (string vCeScriem in vValori1[3].Split('#'))
                {
                    if (vCeScriem != "")
                    {

                        Int64[] vSemestre = ValoriSemestre(vCeScriem, vNrCapete, "7", ddlStergeNrPozitie.SelectedValue);
                        ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vSemestre[1].ToString().Replace(',', '.') + "', col2 = '" + vSemestre[2].ToString().Replace(',', '.') + "' WHERE (capitolId = '" + vSemestre[0].ToString().Replace(',', '.') + "')", Convert.ToInt16(Session["SESan"]));
                    }
                }
                // in 8
                foreach (string vCeScriem in AnimaleInC8Sterge())
                {
                    if (vCeScriem != "")
                    {

                        // scadem 2 din vcescrie
                        string vCeScriemX = "0";
                        try
                        {
                            int vCeScriemNumeric = Convert.ToInt32(vCeScriem);
                            // daca e vanzare respectiv randurile 5 13 22 31 40 49 58 67 76, restul -4 si impartit la 9 sa fie 0
                            if ((vCeScriemNumeric - 4) % 9 == 0 || vCeScriemNumeric == 5)
                            {
                                vCeScriemX = (vCeScriemNumeric - 2).ToString();
                            }
                            else
                            {
                                break;
                            }
                        }
                        catch { }
                        Int64[] vSemestre = ValoriSemestre(vCeScriemX, vNrCapete, "8", ddlStergeNrPozitie.SelectedValue);
                        // scriem in 8; 
                        ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vSemestre[1].ToString().Replace(',', '.') + "', col2 = '" + vSemestre[2].ToString().Replace(',', '.') + "' WHERE (capitolId = '" + vSemestre[0].ToString().Replace(',', '.') + "')", Convert.ToInt16(Session["SESan"]));
                    }
                }
            }
            // stergem in 8
            foreach (string vCeScriem in AnimaleInC8Sterge())
            {
                //
                if (vCeScriem != "")
                {
                    Int64[] vSemestre = ValoriSemestre(vCeScriem, vNrCapete, "8", Session["SESgospodarieId"].ToString());
                    // scriem in 8; 
                    ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vSemestre[1].ToString().Replace(',', '.') + "', col2 = '" + vSemestre[2].ToString().Replace(',', '.') + "' WHERE (capitolId = '" + vSemestre[0].ToString().Replace(',', '.') + "')", Convert.ToInt16(Session["SESan"]));
                }
            }

            pnAdauga.Visible = false;
            pnLista.Visible = true;
            gv.DataBind();
            gv.SelectedIndex = -1;
            btModifica.Visible = false;
            btStergeArata.Visible = false;
        }
    }
    protected Int64[] ValoriSemestre(string vCeScriem, int vNrCapete, string vCapitol, string vGospodarieId)
    {
        Int64[] vSemestre = { 0, 0, 0 };
        int vIntCeScriem = 0;
        try
        {
            vIntCeScriem = Convert.ToInt32(vCeScriem);
        }
        catch
        { return vSemestre; }
        // gasim linia de capitol pentru randul care se va updata 
        List<string> vCampuri = new List<string> { "capitolId", "col1", "col2" };
        List<string> vValori = ManipuleazaBD.fRezultaUnString("SELECT capitole.capitolId, capitole.col1, capitole.col2 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + vGospodarieId + "') AND (sabloaneCapitole.capitol = '" + vCapitol + "') AND (capitole.codRand = '" + vCeScriem + "')", vCampuri, Convert.ToInt16(Session["SESan"]));
        // daca data operarii e mai devreme de semestrul II scadem aceeasi valoare in col1 si col2
        // daca suntem in semestrul 2 initializam dataFatarii cu 01.07.anul, daca nu, 01.01.anul
        int vSemI = 0;
        int vSemII = 0;

        DateTime vDataIesirii = DateTime.Now;
        if (Convert.ToDateTime(tbDataIesiriiSterge.Text) > DateTime.Now)
            vDataIesirii = DateTime.Now;
        else
            vDataIesirii = Convert.ToDateTime(tbDataIesiriiSterge.Text);
        // daca suntem in sem 1 
        if (DateTime.Now < Convert.ToDateTime("01.07." + Session["SESan"].ToString()))
        {
            // trecem doar in sem I
            vSemI = Convert.ToInt32(Convert.ToDecimal(vValori[1].Replace('.', ','))) + vNrCapete;
            if (vSemI < 0) vSemI = 0; // evitam valori negative
            vSemII = Convert.ToInt32(Convert.ToDecimal(vValori[2].Replace('.', ',')));
        }
        else if (Convert.ToDateTime(tbDataIesiriiSterge.Text) < Convert.ToDateTime("01.07." + Session["SESan"].ToString()))
        {
            // trecem in sem I si II
            vSemI = Convert.ToInt32(Convert.ToDecimal(vValori[1].Replace('.', ','))) + vNrCapete;
            if (vSemI < 0) vSemI = 0; // evitam valori negative
            vSemII = Convert.ToInt32(Convert.ToDecimal(vValori[2].Replace('.', ','))) + vNrCapete;
            if (vSemII < 0) vSemII = 0; // evitam valori negative
        }
        else if (Convert.ToDateTime(tbDataIesiriiSterge.Text) >= Convert.ToDateTime("01.07." + Session["SESan"].ToString()))
        {
            // trecem doar in sem II
            vSemI = Convert.ToInt32(Convert.ToDecimal(vValori[1].Replace('.', ',')));
            vSemII = Convert.ToInt32(Convert.ToDecimal(vValori[2].Replace('.', ','))) + vNrCapete;
            if (vSemII < 0) vSemII = 0; // evitam valori negative
        }
        vSemestre = new Int64[] { Convert.ToInt64(vValori[0]), vSemI, vSemII };
        return vSemestre;
    }
    protected List<string> AnimaleInC8Sterge()
    {
        // luam datele din paneluri si completam o lista
        // 0 - nr. randului, 1 capete

        List<string> vListaRanduri = new List<string>();
        string vAnimale = "";

        switch (ddlTipOperatieSterge.SelectedValue)
        {
            case "4":
                //vandut
               // switch (ddlCategoria.SelectedValue)
                switch (((Label)gv.SelectedRow.Cells[0].FindControl("lblCategorieId")).Text)
                {
                    case "1":
                        // bovine / randurile 9, 5
                        vAnimale = "5";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "9";
                        vListaRanduri.Add(vAnimale);
                        // vaci
                        if (y.Text.IndexOf("#19") != -1)
                        {
                            vAnimale = "13";
                            vListaRanduri.Add(vAnimale);
                            vAnimale = "17";
                            vListaRanduri.Add(vAnimale);
                        }
                        break;
                    case "15":
                        // bubaline / randurile 9, 5
                        vAnimale = "5";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "9";
                        vListaRanduri.Add(vAnimale);
                        if (y.Text.IndexOf("#23") != -1)
                        {
                            vAnimale = "13";
                            vListaRanduri.Add(vAnimale);
                            vAnimale = "17";
                            vListaRanduri.Add(vAnimale);
                        }
                        break;
                    case "2":
                        // ovine / 22.26
                        vAnimale = "22";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "26";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "3":
                        // caprine / 31.35
                        vAnimale = "31";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "35";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "4":
                        // porcine / 40.44
                        vAnimale = "40";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "44";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "5":
                        // cabaline / 49.53
                        vAnimale = "49";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "53";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "10":
                        // pasari / 58.62
                        vAnimale = "58";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "62";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "12":
                        // alte animale / 67.71
                        vAnimale = "67";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "71";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "11":
                        // albine / 76.79
                        vAnimale = "76";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "79";
                        vListaRanduri.Add(vAnimale);
                        break;
                }
                break;
            case "5":
                // taiat
                switch (((Label)gv.SelectedRow.Cells[0].FindControl("lblCategorieId")).Text)
                {
                    case "1":
                        // bovine / randurile 9, 6
                        vAnimale = "3";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "9";
                        vListaRanduri.Add(vAnimale);
                        // daca sunt vaci
                         if (y.Text.IndexOf("#19") != -1)
                        {
                            vAnimale = "14";
                            vListaRanduri.Add(vAnimale);
                            vAnimale = "17";
                            vListaRanduri.Add(vAnimale);
                        }
                        break;
                    case "15":
                        // bovine / randurile 9, 6
                        vAnimale = "3";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "9";
                        vListaRanduri.Add(vAnimale);
                        // bivolite
                        if (y.Text.IndexOf("#23") != -1)
                        {
                            vAnimale = "14";
                            vListaRanduri.Add(vAnimale);
                            vAnimale = "17";
                            vListaRanduri.Add(vAnimale);
                        }
                        break;
                    case "2":
                        // ovine / 23.26
                        vAnimale = "23";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "26";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "3":
                        // caprine / 32.35
                        vAnimale = "32";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "35";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "4":
                        // porcine / 41.44
                        vAnimale = "41";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "44";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "5":
                        // cabaline / 50.53
                        vAnimale = "50";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "53";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "10":
                        // pasari / 59.62
                        vAnimale = "59";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "62";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "12":
                        // alte animale / 68.71
                        vAnimale = "68";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "71";
                        vListaRanduri.Add(vAnimale);
                        break;
                }
                break;
            case "6":
                // mort
                switch (((Label)gv.SelectedRow.Cells[0].FindControl("lblCategorieId")).Text)
                {
                    case "1":
                        // bovine / randurile 9, 7
                        vAnimale = "7";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "9";
                        vListaRanduri.Add(vAnimale);
                        // daca sunt vaci
                        if (y.Text.IndexOf("#19") != -1)
                        {
                            vAnimale = "15";
                            vListaRanduri.Add(vAnimale);
                            vAnimale = "17";
                            vListaRanduri.Add(vAnimale);
                        }
                        break;
                    case "15":
                        // bubaline / randurile 9, 7
                        vAnimale = "7";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "9";
                        vListaRanduri.Add(vAnimale);
                        // bivolite
                        if (y.Text.IndexOf("#23") != -1)
                        {
                            vAnimale = "15";
                            vListaRanduri.Add(vAnimale);
                            vAnimale = "17";
                            vListaRanduri.Add(vAnimale);
                        }
                        break;
                    case "2":
                        // ovine / 24.26
                        vAnimale = "24";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "26";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "3":
                        // caprine / 33.35
                        vAnimale = "33";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "35";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "4":
                        // porcine / 41.44
                        vAnimale = "41";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "44";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "5":
                        // cabaline / 51.53
                        vAnimale = "51";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "53";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "10":
                        // pasari / 60.62
                        vAnimale = "60";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "62";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "12":
                        // alte animale / 69.71
                        vAnimale = "69";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "71";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "11":
                        // albine / 77.79
                        vAnimale = "77";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "79";
                        vListaRanduri.Add(vAnimale);
                        break;
                }
                break;
            case "8":
                // alta iesire
                switch (((Label)gv.SelectedRow.Cells[0].FindControl("lblCategorieId")).Text)
                {
                    case "1":
                        // bovine / randurile 9, 8
                        vAnimale = "8";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "9";
                        vListaRanduri.Add(vAnimale);
                        // daca sunt vaci
                        if (y.Text.IndexOf("#19") != -1)
                        {
                            vAnimale = "16";
                            vListaRanduri.Add(vAnimale);
                            vAnimale = "17";
                            vListaRanduri.Add(vAnimale);
                        }
                        break;
                    case "15":
                        // bubaline / randurile 9, 1
                        vAnimale = "1";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "9";
                        vListaRanduri.Add(vAnimale);
                        if (y.Text.IndexOf("#23") != -1)
                        {
                            vAnimale = "16";
                            vListaRanduri.Add(vAnimale);
                            vAnimale = "17";
                            vListaRanduri.Add(vAnimale);
                        }
                        break;
                    case "2":
                        // ovine / 25.26
                        vAnimale = "25";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "26";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "3":
                        // caprine / 34.35
                        vAnimale = "34";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "35";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "4":
                        // porcine / 43.44
                        vAnimale = "43";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "44";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "5":
                        // cabaline / 52.53
                        vAnimale = "52";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "53";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "10":
                        // pasari / 61.62
                        vAnimale = "61";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "62";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "12":
                        // alte animale / 70.71
                        vAnimale = "70";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "71";
                        vListaRanduri.Add(vAnimale);
                        break;
                    case "11":
                        // albine / 78.79
                        vAnimale = "78";
                        vListaRanduri.Add(vAnimale);
                        vAnimale = "79";
                        vListaRanduri.Add(vAnimale);
                        break;
                } break;
        }

        return vListaRanduri;
    }
    protected void cbStergeCatreNecunoscuta_Load(object sender, EventArgs e)
    {
        if (ckbStergeCatre.Checked == true)
        {
            ddlStergeNrPozitie.Enabled = false;
            ddlStergeCatreVolum.Enabled = false;
            lblTextStergeCatre.CssClass = lblTextStergeCatre.CssClass.Replace("taiata", "rosie");
        }
        else
        {
            ddlStergeCatreVolum.Enabled = true;
            ddlStergeNrPozitie.Enabled = true;
            lblTextStergeCatre.CssClass = lblTextStergeCatre.CssClass.Replace("rosie", "taiata");
        }

    }    
    protected void ddlTipOperatieSterge_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlTipOperatieSterge.SelectedValue == "4")
        {
            pnlStergeGospodarie.Visible = true;
        }
        else
        {
            pnlStergeGospodarie.Visible = false;
        }
    }    
    protected void tbStergeNrBucati_TextChanged(object sender, EventArgs e)
    {
        try
        {
            if (Convert.ToInt32(tbStergeNrBucati.Text) > Convert.ToInt32(lblStergeNrBucatiMaxim.Text))
                tbStergeNrBucati.Text = lblStergeNrBucatiMaxim.Text;
        }
        catch
        {
            tbStergeNrBucati.Text = lblStergeNrBucatiMaxim.Text;
        }
    }
    #endregion


}
