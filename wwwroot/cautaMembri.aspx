﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="cautaMembri.aspx.cs" Inherits="cautaMembri" UICulture="ro-Ro" Culture="ro-Ro" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Cauta membri" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="dosareUpdatePanel" runat="server">
        <ContentTemplate>
            <asp:Panel ID="cautaMmembri" runat="server" CssClass="panel_general" Visible="true">
                <asp:Panel ID="Panel2" CssClass="adauga" runat="server">
                    <asp:Panel ID="Panel3" runat="server">
                        <h2>Cauta membri</h2>
                    </asp:Panel>
                </asp:Panel>

                <asp:Panel ID="cautaOfertePanel" runat="server" CssClass="cauta">
                    <asp:Label ID="Label1" runat="server" Text="Volum" />
                    <asp:TextBox ID="volumTextBox" OnTextChanged="volumTextBox_TextChanged" runat="server" autocomplete="off" AutoPostBack="True"
                        Width="50px" />
                    <asp:Label ID="Label2" runat="server" Text="Pozitie" />
                      <asp:TextBox ID="pozitieTextBox" OnTextChanged="pozitieTextBox_TextChanged" runat="server" autocomplete="off" AutoPostBack="True"
                        Width="50px" />
                    <asp:Label ID="Label3" runat="server" Text="Cap gospodarie" />
                      <asp:TextBox ID="capDeGospodarieTextBox" OnTextChanged="capDeGospodarieTextBox_TextChanged" runat="server" autocomplete="off" AutoPostBack="True"
                        Width="150px" />
                     <asp:Label ID="Label4" runat="server" Text="Nume membru" />
                      <asp:TextBox ID="numeMembrutextBox" OnTextChanged="numeMembrutextBox_TextChanged" runat="server" autocomplete="off" AutoPostBack="True"
                        Width="150px" />
                    <asp:Label ID="Label7" runat="server" Text="CNP" />
                    <asp:TextBox ID="cnpTextBox" OnTextChanged="cnpTextBox_TextChanged" runat="server" autocomplete="off" AutoPostBack="True"
                        Width="150px" />
                    <asp:Label ID="Label16" Text="Data nasterii" runat="server"></asp:Label>&nbsp<asp:TextBox ID="dataNasteriiTextBox" runat="server" Width="120" Format="dd.MM.yyyy" OnTextChanged="dataNasteriiTextBox_TextChanged" AutoPostBack="true" onchange="this.value=ValidareData(this.value, 'dd.MM.yyyy')"></asp:TextBox>
                        <asp:CalendarExtender ID="dataNasterii_CalendarExtender" runat="server" Enabled="True"
                            Format="dd.MM.yyyy" PopupButtonID="dataNasteriiTextBox" TargetControlID="dataNasteriiTextBox">
                        </asp:CalendarExtender>
                </asp:Panel>

                  <asp:Panel ID="cautaMembriPanel" runat="server">
                    <asp:GridView ID="cautaMembriGridView" AllowPaging="true" PageSize="10" CellPadding="3" ShowFooter="True" OnPageIndexChanging="cautaMembriGridView_PageIndexChanging" EmptyDataText="Nu sunt adaugate inregistrari"  DataKeyNames="capitolId" AllowSorting="True" CssClass="tabela"
                        runat="server" AutoGenerateColumns="true"
                       OnRowDataBound="cautaMembriGridView_RowDataBound">
                        <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                        <FooterStyle CssClass="footer" />
                    </asp:GridView>
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

