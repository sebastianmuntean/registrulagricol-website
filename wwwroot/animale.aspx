﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="animale.aspx.cs" Inherits="Animale" UICulture="ro-Ro" Culture="ro-Ro"
    EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Capitole VII + VIII: efective de animale" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnAfiseazaMesaje" runat="server">
                <asp:ValidationSummary ID="valSum" runat="server" DisplayMode="SingleParagraph" Visible="true"
                    ValidationGroup="GrupValidare" CssClass="validator" ForeColor="" />
            </asp:Panel>
            <!-- lista  -->
            <asp:Panel ID="pnLista" runat="server" CssClass="panel_general" Visible="true">
                <asp:Panel ID="pnCautare" runat="server" CssClass="cauta">
                    <asp:Label ID="lblCauta" runat="server" Text="Caută după:" />
                    <asp:Label ID="lblCCrotal" runat="server" Text="Cod Identificare" />
                    <asp:TextBox ID="tbCCrotal" runat="server" AutoPostBack="True" Width="100px" />
                    <asp:Label ID="lblCCategoria" runat="server" Text="Categoria" />
                    <asp:DropDownList ID="ddlCCategoria" runat="server" AutoPostBack="True" OnInit="ddlCCategoria_Init"
                        Width="76px">
                    </asp:DropDownList>
                </asp:Panel>
                <asp:Panel ID="pnGv" runat="server">
                    <asp:GridView ID="gv" runat="server" DataSourceID="SqlGv" AutoGenerateColumns="False"
                        CssClass="tabela" AllowPaging="True" AllowSorting="True" OnDataBound="gv_DataBound"
                        OnRowDataBound="gv_RowDataBound1" OnSelectedIndexChanged="gv_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField HeaderText="categoria" 
                                SortExpression="animaleCategorieDenumire">
                                <ItemTemplate>
                                    <asp:Label ID="lblCategorieDenumire" runat="server" 
                                        Text='<%# Bind("animaleCategorieDenumire") %>'></asp:Label>
                                <asp:Label ID="lblCategorieId" runat="server" 
                                        Text='<%# Bind("animaleCategorieId") %>' Visible="false"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="rasa / specia" 
                                SortExpression="animaleRasaDenumire">
                                <ItemTemplate>
                                    <asp:Label ID="lblRasaDenumire" runat="server" Text='<%# Bind("animaleRasaDenumire") %>'></asp:Label>
                                    <asp:Label ID="lblRasaId" runat="server" Text='<%# Bind("animaleRasaId") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblAnimaleEvolutieId" runat="server" Text='<%# Bind("animaleEvolutieId") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblAnimalId" runat="server" Text='<%# Bind("animalId") %>' Visible="false"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="animalCodIdentificare" HeaderText="cod identificare" SortExpression="animalCodIdentificare" />
                            <asp:BoundField DataField="animaleEvolutieCapete" HeaderText="capete" SortExpression="animaleEvolutieCapete" />
                            <asp:BoundField DataField="animalDataFatarii" HeaderText="data fătării" SortExpression="animalDataFatarii"
                                DataFormatString="{0:d}" />
                            <asp:BoundField DataField="animaleEvolutieId" SortExpression="animaleEvolutieId"
                                HeaderText="id ev" Visible="false" />
                            <asp:BoundField DataField="animalId" SortExpression="animalId" HeaderText="animal id"
                                Visible="false" />
                            <asp:BoundField DataField="animaleOperatieData" HeaderText="data intrării" SortExpression="animaleOperatieData"
                                DataFormatString="{0:d}" />
                            <asp:BoundField DataField="animaleEvolutieData" SortExpression="animaleEvolutieData" HeaderText="ultima modificare" />
                        </Columns>
                        <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlGv" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString %>"
                        SelectCommand="SELECT animale.animaleCategorieId, animaleEvolutie.animaleOperatieData, animaleEvolutie.animaleEvolutieData, animaleEvolutie.animaleEvolutieId, animaleRase.animaleRasaDenumire, animaleEvolutie.animaleRasaId, animaleCategorii.animaleCategorieDenumire, animaleEvolutie.animaleEvolutieCapete, animaleEvolutie.animalCodIdentificare, animale.animalDataFatarii, animale.animalId FROM animaleEvolutie INNER JOIN animale ON animaleEvolutie.animalId = animale.animalId INNER JOIN animaleCategorii ON animale.animaleCategorieId = animaleCategorii.animaleCategorieId LEFT OUTER JOIN animaleRase ON animaleEvolutie.animaleRasaId = animaleRase.animaleRasaId WHERE (animaleEvolutie.unitateId = @unitateId) AND (animaleEvolutie.gospodarieId = @gospodarieId) AND (animaleEvolutie.animaleEvolutieUltima = 'True') AND (animaleEvolutie.an = @an) AND (animale.animalCodIdentificare LIKE N'%' + @cod + N'%') AND (CONVERT (nvarchar, animale.animaleCategorieId) LIKE @categorie) ORDER BY animaleEvolutie.animaleEvolutieData DESC">
                        <SelectParameters>
                            <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" />
                            <asp:SessionParameter Name="gospodarieId" SessionField="SESgospodarieId" />
                            <asp:SessionParameter Name="an" SessionField="SESan" />
                            <asp:ControlParameter ControlID="tbCCrotal" Name="cod" PropertyName="Text" DefaultValue="%" />
                            <asp:ControlParameter ControlID="ddlCCategoria" Name="categorie" PropertyName="SelectedValue" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:GridView ID="gvTotaluriCategorii" runat="server" DataSourceID="SqlTotaluriCategorii"
                        Visible="false">
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlTotaluriCategorii" runat="server"></asp:SqlDataSource>
                </asp:Panel>
                <asp:Panel ID="pnListaButoanePrincipale" CssClass="butoane" runat="server">
                    <asp:Button CssClass="buton" ID="btModifica" runat="server" Text="modifică" Visible="false"
                        OnClick="btModifica_Click" />
                    <asp:Button CssClass="buton" ID="btSterge" runat="server" Text="şterge" OnClick="Sterge"
                        ValidationGroup="GrupValidare" OnClientClick="return confirm(&quot;Sunteți sigur că doriți să ștergeți?&quot;)"
                        Visible="false" />
                    <asp:Button CssClass="buton" ID="btListaAdauga" runat="server" Text="adaugă" OnClick="Adauga" />
                    <asp:Button CssClass="buton" ID="btStergeArata" runat="server" Text="şterge / ieşiri animale"
                        OnClick="btStergeArata_Click" Visible="false" />
                    <asp:Button ID="btListaGospodarii" runat="server" CssClass="buton" Text="listă gospodării"
                        Visible="true" PostBackUrl="~/Gospodarii.aspx" OnClick="btListaGospodarii_Click" />
                </asp:Panel>
            </asp:Panel>
            <!-- adaugam  -->
            <asp:Panel ID="pnAdauga" CssClass="panel_general" runat="server" Visible="false">
                <asp:Panel ID="pnAd" CssClass="adauga" runat="server">
                    <asp:Panel ID="pnAdTitlu" runat="server">
                        <h2>
                            ADAUGĂ / MODIFICĂ EFECTIVE ANIMALE</h2>
                    </asp:Panel>
                    <asp:Panel ID="pnAd1" runat="server">
                        <asp:Panel ID="pnlCategoria" runat="server" CssClass="bloc">
                            <asp:Panel ID="pnlCat" runat="server" CssClass="in_linie">
                                <asp:Label ID="lblTextCategoria" runat="server" Text="Categoria:" CssClass="indent">
                                </asp:Label>
                                <asp:Label ID="x" runat="server" Text="X:" CssClass="indent" Visible="false">
                                </asp:Label>
                                <asp:Label ID="lblAscunsTextExemplu" runat="server" Text="" Visible="false">
                                </asp:Label>
                                <asp:DropDownList ID="ddlCategoria" runat="server" DataSourceID="SqlAnimaleCategorii"
                                    DataTextField="animaleCategorieDenumire" DataValueField="animaleCategorieId"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlCategoria_SelectedIndexChanged"
                                    OnInit="ddlCategoria_Init" OnDataBound="ddlCategoria_DataBound">
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="SqlAnimaleCategorii" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016 %>"
                                    SelectCommand="SELECT [animaleCategorieDenumire], [animaleCategorieId] FROM [animaleCategorii] ORDER BY animaleCategorieDenumire">
                                </asp:SqlDataSource>
                            </asp:Panel>
                            <asp:Panel ID="pnlRasa" runat="server" CssClass="in_linie" Visible="true">
                                <asp:Label ID="lblTextRasa" runat="server" Text="Rasa sau specia:" CssClass="indent">
                                </asp:Label>
                                <asp:DropDownList ID="ddlRasa" runat="server" DataSourceID="SqlAnimaleRase" DataTextField="animaleRasaDenumire"
                                    DataValueField="animaleRasaId">
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="SqlAnimaleRase" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016%>"
                                    SelectCommand="SELECT [animaleRasaDenumire], [animaleRasaId] FROM [animaleRase] WHERE ([animaleCategorieId] = @animaleCategorieId) ORDER BY  animaleRasaDenumire">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="ddlCategoria" Name="animaleCategorieId" PropertyName="SelectedValue"
                                            Type="Int32" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                                <asp:HyperLink CssClass="sugereaza" Target="_blank" ID="hlkSugereaza" runat="server"
                                    NavigateUrl="~/semnalareEroare.aspx"> sugerează o altă rasă sau specie </asp:HyperLink>
                            </asp:Panel>
                        </asp:Panel>
                        <asp:Panel ID="pnlDetaliiSex" runat="server" CssClass="bloc" Visible="false">
                            <asp:Panel ID="pnlSex" runat="server" CssClass="in_linie">
                                <asp:Label ID="lblSex" runat="server" Text="Sex:" CssClass="indent"></asp:Label>
                                <asp:DropDownList ID="ddlSex" runat="server" AutoPostBack="True" OnLoad="ddlSex_Load"
                                    OnSelectedIndexChanged="ddlSex_SelectedIndexChanged">
                                    <asp:ListItem>femele</asp:ListItem>
                                    <asp:ListItem>masculi</asp:ListItem>
                                </asp:DropDownList>
                            </asp:Panel>
                        </asp:Panel>
                        <asp:Panel ID="pnlDetaliiCategoria1" runat="server" CssClass="bloc" Visible="false">
                            <asp:Panel ID="pnlDetaliiBovineFemele" runat="server" CssClass="in_linie" Visible="false">
                                <asp:Label ID="lblDetaliiBovineFemeleVarsta" runat="server" Text="Varsta:" CssClass="indent"></asp:Label>
                                <asp:DropDownList ID="ddlDetaliiBovineFemeleVarsta1" runat="server" AutoPostBack="True"
                                    OnSelectedIndexChanged="ddlDetaliiBovineFemeleVarsta1_SelectedIndexChanged">
                                    <asp:ListItem Value="4">sub 6 luni</asp:ListItem>
                                    <asp:ListItem Value="3">între 6 luni şi 1 an</asp:ListItem>
                                    <asp:ListItem Value="9">1-2 ani</asp:ListItem>
                                    <asp:ListItem Value="0">mai mari de 2 ani</asp:ListItem>
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddlDetaliiBovineFemeleVarsta2" runat="server" AutoPostBack="True"
                                    Visible="false" OnSelectedIndexChanged="ddlDetaliiBovineFemeleVarsta2_SelectedIndexChanged">
                                    <asp:ListItem Value="18">juninci</asp:ListItem>
                                    <asp:ListItem Value="19">vaci</asp:ListItem>
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddlDetaliiBovineFemeleVarsta3" runat="server" Visible="false">
                                    <asp:ListItem Value="20">vaci pentru lapte</asp:ListItem>
                                    <asp:ListItem Value="21">vaci reformate (pentru sacrificare)</asp:ListItem>
                                </asp:DropDownList>
                            </asp:Panel>
                            <asp:Panel ID="pnlDetaliiBovineMasculi" runat="server" CssClass="in_linie" Visible="false">
                                <asp:Label ID="lblDetaliiBovineMasculiVarsta" runat="server" Text="Varsta:" CssClass="indent"></asp:Label>
                                <asp:DropDownList ID="ddlDetaliiBovineMasculiVarsta1" runat="server" AutoPostBack="True"
                                    OnSelectedIndexChanged="ddlDetaliiBovineMasculiVarsta1_SelectedIndexChanged">
                                    <asp:ListItem Value="6">sub 6 luni</asp:ListItem>
                                    <asp:ListItem Value="5">între 6 luni şi 1 an</asp:ListItem>
                                    <asp:ListItem Value="10">1-2 ani</asp:ListItem>
                                    <asp:ListItem Value="0">mai mari de 2 ani</asp:ListItem>
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddlDetaliiBovineMasculiVarsta2" runat="server" Visible="false">
                                    <asp:ListItem Value="130">pentru reproducţie - tauri autorizaţi</asp:ListItem>
                                    <asp:ListItem Value="131">pentru reproducţie - alţi tauri</asp:ListItem>
                                    <asp:ListItem Value="15">reformaţi (pentru sacrificare)</asp:ListItem>
                                    <asp:ListItem Value="16">pentru muncă</asp:ListItem>
                                </asp:DropDownList>
                            </asp:Panel>
                            <asp:Panel ID="pnlVitei" runat="server" CssClass="in_linie" Visible="true">
                                <asp:CheckBox ID="ckbVitei" runat="server" Checked="false" Text="viţei pentru sacrificare?"
                                    CssClass="checkbox_linie" />
                            </asp:Panel>
                        </asp:Panel>
                        <asp:Panel ID="pnlDetaliiCategoria2" runat="server" CssClass="bloc" Visible="false">
                            <asp:Panel ID="pnlDetaliiOvine" runat="server" CssClass="in_linie" Visible="true">
                                <asp:Label ID="lblDetaliiOv" runat="server" Text="Subcategorie:" CssClass="indent"></asp:Label>
                                <asp:DropDownList ID="ddlDetaliiOvine" runat="server">
                                    <asp:ListItem Value="27">oi - femele pentru reproducţie</asp:ListItem>
                                    <asp:ListItem Value="28">mioare montatee</asp:ListItem>
                                    <asp:ListItem Value="29">tineret sub 1 an</asp:ListItem>
                                    <asp:ListItem Value="30">oi - alte categorii</asp:ListItem>
                                    <asp:ListItem Value="31">berbeci de reproducţie autorizaţi</asp:ListItem>
                                </asp:DropDownList>
                            </asp:Panel>
                        </asp:Panel>
                        <asp:Panel ID="pnlDetaliiCategoria3" runat="server" CssClass="bloc" Visible="false">
                            <asp:Panel ID="pnlDetaliiCaprine" runat="server" CssClass="in_linie" Visible="true">
                                <asp:Label ID="lblDetaliiCaprine" runat="server" Text="Subcategorie:" CssClass="indent"></asp:Label>
                                <asp:DropDownList ID="ddlDetaliiCaprine" runat="server">
                                    <asp:ListItem Value="33">capre</asp:ListItem>
                                    <asp:ListItem Value="34">ţapi</asp:ListItem>
                                    <asp:ListItem Value="35">tineret montat</asp:ListItem>
                                    <asp:ListItem Value="36">alte caprine</asp:ListItem>
                                </asp:DropDownList>
                            </asp:Panel>
                        </asp:Panel>
                        <asp:Panel ID="pnlDetaliiCategoria4" runat="server" CssClass="bloc" Visible="false">
                            <asp:Panel ID="pnlDetaliiPorcine" runat="server" CssClass="in_linie" Visible="true">
                                <asp:Label ID="lblDetaliiPorcine" runat="server" Text="Subcategorie:" CssClass="indent"></asp:Label>
                                <asp:DropDownList ID="ddlDetaliiPorcine" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlDetaliiPorcine_SelectedIndexChanged">
                                    <asp:ListItem Value="38">Purcei sub 20 kg</asp:ListItem>
                                    <asp:ListItem Value="39">Porcine între 20-50 kg</asp:ListItem>
                                    <asp:ListItem Value="40">Porcine între 20-50 kg (tineret de crescătorii (2-4 luni))</asp:ListItem>
                                    <asp:ListItem Value="41">Porcine pentru îngrăşat</asp:ListItem>
                                    <asp:ListItem Value="45">Porcine pentru reproducţie de peste 50 kg</asp:ListItem>
                                </asp:DropDownList>
                            </asp:Panel>
                            <asp:Panel ID="pnlDetaliiPorcineDeIngrasat" runat="server" CssClass="in_linie" Visible="false">
                                <asp:DropDownList ID="ddlDetaliiPorcineDeIngrasat" runat="server">
                                    <asp:ListItem Value="42">cu greutate de 50-80 kg</asp:ListItem>
                                    <asp:ListItem Value="43">cu greutate de 81-110 kg</asp:ListItem>
                                    <asp:ListItem Value="44">peste 110 kg</asp:ListItem>
                                </asp:DropDownList>
                            </asp:Panel>
                            <asp:Panel ID="pnlDetaliiPorcineDeReproductie" runat="server" CssClass="in_linie"
                                Visible="false">
                                <asp:DropDownList ID="ddlDetaliiPorcineDeReproductie" runat="server" AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlDetaliiPorcineDeReproductie_SelectedIndexChanged">
                                    <asp:ListItem Value="46">vieri de reproducţie autorizaţi</asp:ListItem>
                                    <asp:ListItem Value="47">scroafe</asp:ListItem>
                                    <asp:ListItem Value="48">scrofiţe</asp:ListItem>
                                </asp:DropDownList>
                            </asp:Panel>
                            <asp:Panel ID="pnlDetaliiScrofite" runat="server" CssClass="in_linie" Visible="false">
                                <asp:CheckBox ID="ckbScrofite6_9luni" runat="server" Checked="false" Text="tineret femel de prăsilă (6-9 luni) ?"
                                    CssClass="checkbox_linie" />
                            </asp:Panel>
                        </asp:Panel>
                        <asp:Panel ID="pnlDetaliiCategoria5" runat="server" CssClass="bloc" Visible="false">
                            <asp:Panel ID="pnlDetaliiCabaline" runat="server" CssClass="in_linie" Visible="true">
                                <asp:Label ID="lblDetaliiCabaline" runat="server" Text="Subcategorie:" CssClass="indent"></asp:Label>
                                <asp:DropDownList ID="ddlDetaliiCabaline" runat="server">
                                    <asp:ListItem Value="50">altele</asp:ListItem>
                                    <asp:ListItem Value="51">cabaline de muncă peste 3 ani</asp:ListItem>
                                    <asp:ListItem Value="52">armăsari de reproducţie autorizaţi</asp:ListItem>
                                </asp:DropDownList>
                            </asp:Panel>
                        </asp:Panel>
                        <asp:Panel ID="pnlDetaliiCategoria8" runat="server" CssClass="bloc" Visible="false">
                            <asp:Panel ID="pnlDetaliiIepuri" runat="server" CssClass="in_linie" Visible="true">
                                <asp:Label ID="lblDetaliiIepuri" runat="server" Text="Subcategorie:" CssClass="indent"></asp:Label>
                                <asp:DropDownList ID="ddlDetaliiIepuri" runat="server">
                                    <asp:ListItem Value="55">altele</asp:ListItem>
                                    <asp:ListItem Value="56">iepuri femele pentru reproducţie</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="Label1" runat="server" Text="" CssClass="indent"></asp:Label>
                            </asp:Panel>
                        </asp:Panel>
                        <asp:Panel ID="pnlDetaliiCategoria10" runat="server" CssClass="bloc" Visible="false">
                            <asp:Panel ID="pnlDetaliiPasari" runat="server" CssClass="in_linie" Visible="true">
                                <asp:CheckBox ID="ckbPasariOuatoare" runat="server" Checked="false" Text="pasări ouătoare"
                                    CssClass="checkbox_linie" />
                                <asp:CheckBox ID="ckbGainiOuatoare" runat="server" Checked="false" Text="găini ouătoare"
                                    CssClass="checkbox_linie" />
                            </asp:Panel>
                        </asp:Panel>
                        <asp:Panel ID="pnlDetaliiCategoria15" runat="server" CssClass="bloc" Visible="false">
                            <asp:Panel ID="pnlDetaliiBubaline" runat="server" CssClass="in_linie" Visible="true">
                                <asp:Label ID="lblDetaliiBubaline" runat="server" Text="Subcategorie:" CssClass="indent"></asp:Label>
                                <asp:DropDownList ID="ddlBubaline" runat="server">
                                    <asp:ListItem Value="23">Bivoliţe pentru reproducţie</asp:ListItem>
                                    <asp:ListItem Value="24">Bivoli autorizaţi pentru reproducţie</asp:ListItem>
                                    <asp:ListItem Value="25">Alte bubaline</asp:ListItem>
                                </asp:DropDownList>
                            </asp:Panel>
                        </asp:Panel>
                        <asp:Panel ID="pnlDetaliiCategoria0" runat="server" Visible="false">
                        </asp:Panel>
                        <asp:Panel ID="pnlOperatia" runat="server" CssClass="bloc" Visible="true">
                            <asp:Panel ID="pnlOp" runat="server" CssClass="in_linie">
                                <asp:Label ID="lblTextOperatia" runat="server" CssClass="indent" Text="Tip operaţiune:">
                                </asp:Label>
                                <asp:DropDownList ID="ddlOperatieTip" runat="server" AutoPostBack="True" OnDataBound="ddlOperatieTip_DataBound"
                                    OnInit="ddlOperatieTip_Init" OnSelectedIndexChanged="ddlOperatieTip_SelectedIndexChanged">
                                    <asp:ListItem Selected="True">intrare</asp:ListItem>
                                </asp:DropDownList>
                            </asp:Panel>
                            <asp:Panel ID="pnlOperatie" runat="server" CssClass="in_linie">
                                <asp:DropDownList ID="ddlOperatieDenumire" runat="server" DataSourceID="SqlOperatii"
                                    DataTextField="animaleOperatieDenumire" AutoPostBack="true" DataValueField="animaleOperatieId"
                                    OnLoad="ddlOperatieDenumire_Load" OnPreRender="ddlOperatieDenumire_PreRender">
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="SqlOperatii" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016 %>"
                                    SelectCommand="SELECT [animaleOperatieDenumire], [animaleOperatieId] FROM [animaleOperatii] WHERE ([animaleOperatieTip] = @animaleOperatieTip)">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="ddlOperatieTip" Name="animaleOperatieTip" PropertyName="SelectedValue"
                                            Type="String" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </asp:Panel>
                            <asp:Panel ID="pnlCatre" runat="server" CssClass="in_linie">
                                <asp:Label ID="lblTextCatre" runat="server" CssClass="indent" Text="către gospodăria: "
                                    OnLoad="lblTextCatre_Load" />
                                <asp:CheckBox ID="cbCatreNecunoscuta" runat="server" Checked="true" OnLoad="cbCatreNecunoscuta_Load"
                                    AutoPostBack="true" /><asp:Label ID="lblTextNecunoscuta" runat="server" CssClass="rosie indent"
                                        Text="necunoscută" />
                                <asp:Label ID="lblTextVolum" runat="server" CssClass="indent" Text="volum" Width="50" />
                                <asp:DropDownList ID="ddlVolum" runat="server" AutoPostBack="True" DataSourceID="SqlVolum"
                                    DataTextField="volum" DataValueField="volum">
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="SqlVolum" runat="server"
                                    SelectCommand="SELECT DISTINCT volum, volumInt FROM gospodarii WHERE (unitateId = @unitateId) AND (gospodarieId &lt;&gt; @gospodarieid) ORDER BY volumInt">
                                    <SelectParameters>
                                        <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" Type="Int32" />
                                        <asp:SessionParameter Name="gospodarieid" SessionField="SESgospodarieId" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                                <asp:Label ID="lblTextNrPozitie" runat="server" CssClass="indent" Text="nr.poziţie"
                                    Width="50" /><asp:DropDownList ID="ddlNrPozitie" runat="server" DataSourceID="SqlNrPozitie"
                                        DataTextField="nrPozitie" DataValueField="gospodarieId">
                                    </asp:DropDownList>
                                <asp:SqlDataSource ID="SqlNrPozitie" runat="server" 
                                    SelectCommand="SELECT nrPozitie, gospodarieId, nrPozitieInt FROM gospodarii WHERE (volum = @volum) AND (unitateId = @unitateId) AND (gospodarieId &lt;&gt; @gospodarieId) ORDER BY nrPozitieInt">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="ddlVolum" Name="volum" PropertyName="SelectedValue"
                                            Type="String" />
                                        <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" Type="Int32" />
                                        <asp:SessionParameter Name="gospodarieId" SessionField="SESgospodarieId" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                                <asp:Label ID="lblCatreScadeti" runat="server" CssClass="rosie" Text="Doriţi să scădeţi animalele din gospodăria de unde s-a cumpărat? "
                                    Visible="false" />
                                <asp:CheckBox ID="ckbCatreScadeti" runat="server" Checked="false" Visible="false" />
                            </asp:Panel>
                        </asp:Panel>
                        <asp:Panel ID="pnlDataFatarii" runat="server" CssClass="bloc" Visible="true">
                            <asp:Panel ID="pnlDataFat" runat="server" CssClass="in_linie">
                                <asp:Label ID="lblTextDataFatarii" runat="server" CssClass="indent" Text="Data fătării:" />
                                <asp:TextBox ID="tbDataFatarii" runat="server" Width="80" onchange="this.value=ValidareData(this.value, 'dd.MM.yyyy')"></asp:TextBox>
                                <asp:CalendarExtender ID="tbDataFatarii_CalendarExtender" runat="server" Enabled="True"
                                    DefaultView="Days" Format="dd.MM.yyyy" TargetControlID="tbDataFatarii">
                                </asp:CalendarExtender>
                            </asp:Panel>
                        </asp:Panel>
                        <asp:Panel ID="pnlDataOperarii" runat="server" CssClass="bloc" Visible="true">
                            <asp:Panel ID="pnlDataOp" runat="server" CssClass="in_linie">
                                <asp:Label ID="lblDataOperarii" runat="server" CssClass="indent" Text="Data intrării / ieşirii:" />
                                <asp:TextBox ID="tbDataOperare" runat="server" Width="80" Format="dd.MM.yyyy" onchange="this.value=ValidareData(this.value, 'dd.MM.yyyy')"></asp:TextBox>
                                <asp:CalendarExtender ID="tbDataOperare_CalendarExtender" runat="server" Enabled="True"
                                    Format="dd.MM.yyyy" PopupButtonID="tbDataOperare" TargetControlID="tbDataOperare">
                                </asp:CalendarExtender>
                            </asp:Panel>
                        </asp:Panel>
                        <asp:Panel ID="pnlNrBucati" runat="server" CssClass="bloc">
                            <asp:Panel ID="pnlNr" runat="server" CssClass="in_linie">
                                <asp:Label ID="lblTextNrBucati" runat="server" Text='Nr. bucăţi / capete: ' CssClass="indent" />
                                <asp:TextBox ID="tbNrBucati" runat="server" Text="1" Width="30" AutoPostBack="true"
                                    OnTextChanged="tbNrBucati_TextChanged" OnLoad="tbNrBucati_Load"></asp:TextBox>
                                <asp:Label ID="lblNrBucatiMax" runat="server" Text='(max. 20)' />
                            </asp:Panel>
                        </asp:Panel>
                        <asp:Panel ID="pnlCodIdentificare" runat="server" CssClass="">
                            <asp:Panel ID="pnCod" runat="server" CssClass="in_linie">
                                <asp:Label ID="lblTextCodIdentificare" runat="server" Text='Codul de indentificare al animalului, de ex. ' /><asp:Label
                                    ID="lblCodIdentificareExemplu" runat="server" Text='' />
                                <asp:DataList ID="dlCodIdentificareExemple" runat="server" DataKeyField="animaleCategorieId"
                                    DataSourceID="SqlDlCodIdentificare" RepeatLayout="Flow">
                                    <ItemTemplate>
                                        <asp:Label ID="animaleCategorieExempluLabel" runat="server" Text='<%# Eval("animaleCategorieExemplu") %>' />
                                        |
                                        <asp:Label ID="animaleCategorieExempluElectronicLabel" runat="server" Text='<%# Eval("animaleCategorieExempluElectronic") %>' />
                                    </ItemTemplate>
                                </asp:DataList>
                                <asp:SqlDataSource ID="SqlDlCodIdentificare" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016 %>"
                                    SelectCommand="SELECT  top(1)   animaleCategorii.animaleCategorieExemplu, animaleCategorii.animaleCategorieExempluElectronic, animaleCategorii.animaleCategorieId, animaleTari.animaleTaraDenumire, animaleTari.animaleTaraNumar, animaleTari.animaleTaraCodA2 FROM animaleCategorii CROSS JOIN animaleTari WHERE (animaleCategorii.animaleCategorieId = @animaleCategorieId)">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="ddlCategoria" Name="animaleCategorieId" PropertyName="SelectedValue"
                                            Type="Int32" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                                <br />
                                <asp:DataList ID="dlTaraDenumire" runat="server" DataKeyField="animaleTaraId" DataSourceID="SqlTaraSelectata"
                                    RepeatLayout="Flow" OnSelectedIndexChanged="dlTaraDenumire_SelectedIndexChanged">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlTara" runat="server" DataSourceID="SqlTari" DataTextField="animaleTaraCodA2"
                                            DataValueField="animaleTaraId" AutoPostBack="True" OnDataBound="ddlTara_DataBound"
                                            OnInit="ddlTara_Init" OnSelectedIndexChanged="ddlTara_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:SqlDataSource ID="SqlTari" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016 %>"
                                            SelectCommand="SELECT [animaleTaraDenumire], [animaleTaraId], [animaleTaraCodA3], [animaleTaraNumar], [animaleTaraCodA2] FROM [animaleTari] ORDER BY [animaleTaraCodA2]">
                                        </asp:SqlDataSource>
                                        <asp:Label ID="animaleTaraNumarLabel" runat="server" Text='<%# Eval("animaleTaraNumar") %>' />
                                        <asp:TextBox ID="tbCodIdentificare" runat="server"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:DataList>
                                <asp:SqlDataSource ID="SqlTaraSelectata" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016 %>"
                                    SelectCommand="" OnInit="SqlTaraSelectata_Init"></asp:SqlDataSource>
                            </asp:Panel>
                        </asp:Panel>
                        <asp:Panel ID="pnButoanePrincipale" runat="server" CssClass="butoane">
                            <asp:Button ID="btSalveaza" runat="server" CssClass="buton" OnClick="Salveaza" Text="salvează"
                                ValidationGroup="GrupValidare" Visible="true" />
                            <asp:Button ID="btArataLista" runat="server" CssClass="buton" OnClick="ArataLista"
                                Text="listă" Visible="true" />
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel ID="pnSterge" runat="server" Visible="true">
                        <asp:Panel ID="pnlSterge1" runat="server" CssClass="bloc">
                            <asp:Panel ID="pnlTipOperatieSterge" runat="server" CssClass="in_linie">
                                <asp:Label ID="lblTextTipSterge" runat="server" Text="Tipul ieşirii:" />
                                <asp:DropDownList ID="ddlTipOperatieSterge" runat="server" DataSourceID="SqlOperatiiSterge"
                                    DataTextField="animaleOperatieDenumire" DataValueField="animaleOperatieId" 
                                    AutoPostBack="true" 
                                    onselectedindexchanged="ddlTipOperatieSterge_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="SqlOperatiiSterge" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016 %>"
                                    SelectCommand="SELECT [animaleOperatieDenumire], [animaleOperatieId] FROM [animaleOperatii] WHERE ([animaleOperatieTip] LIKE 'iesire') ">
                                </asp:SqlDataSource>
                                <asp:Label ID="lblTextStergeNrBucati" runat="server" Text="Nr. capete: " />
                                <asp:Label ID="Label4" runat="server" Text="(maxim " />
                                <asp:Label ID="lblStergeNrBucatiMaxim" runat="server" Text="1" />
                                <asp:Label ID="Label3" runat="server" Text=" buc.)" />
                                <asp:TextBox ID="tbStergeNrBucati" runat="server" Width="40" 
                                    AutoPostBack="True" ontextchanged="tbStergeNrBucati_TextChanged"></asp:TextBox>
                                
                                <asp:Label ID="Label2" runat="server" Text="Data ieşirii:" />
                                
                                <asp:Label ID="y" runat="server" Text="" />
                                <asp:TextBox ID="tbDataIesiriiSterge" runat="server" Width="80" Format="dd.MM.yyyy"
                                    onchange="this.value=ValidareData(this.value, 'dd.MM.yyyy')"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd.MM.yyyy"
                                    PopupButtonID="tbDataIesiriiSterge" TargetControlID="tbDataIesiriiSterge">
                                </asp:CalendarExtender>
                            </asp:Panel>
                        </asp:Panel>
                        <asp:Panel ID="pnlStergeGospodarie" runat="server" CssClass="bloc">
                            <asp:Panel ID="pnlStergeCatre" runat="server" CssClass="in_linie">
                                <asp:Label ID="lblTextStergeCatre" runat="server" CssClass="indent" Text="către gospodăria: "
                                    OnLoad="lblTextCatre_Load" />
                                <asp:CheckBox ID="ckbStergeCatre" runat="server" Checked="true" OnLoad="cbStergeCatreNecunoscuta_Load"
                                    AutoPostBack="true" /><asp:Label ID="lblStergeNecunoscuta" runat="server" CssClass="rosie indent"
                                        Text="necunoscută / în afara unităţii" />
                                <asp:Label ID="lblStergeCatreVolum" runat="server" CssClass="indent" Text="volum"
                                    Width="50" />
                                <asp:DropDownList ID="ddlStergeCatreVolum" runat="server" AutoPostBack="True" DataSourceID="SqlStergeVolum"
                                    DataTextField="volum" DataValueField="volum">
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="SqlStergeVolum" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016 %>"
                                    SelectCommand="SELECT DISTINCT volum, volumInt FROM gospodarii WHERE (unitateId = @unitateId) AND (gospodarieId &lt;&gt; @gospodarieid) ORDER BY volumInt">
                                    <SelectParameters>
                                        <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" Type="Int32" />
                                        <asp:SessionParameter Name="gospodarieid" SessionField="SESgospodarieId" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                                <asp:Label ID="lblStergeNrPozitie" runat="server" CssClass="indent" Text="nr.poziţie"
                                    Width="50" /><asp:DropDownList ID="ddlStergeNrPozitie" runat="server" DataSourceID="SqlStergeNrPozitie"
                                        DataTextField="nrPozitie" DataValueField="gospodarieId">
                                    </asp:DropDownList>
                                <asp:SqlDataSource ID="SqlStergeNrPozitie" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016 %>"
                                    SelectCommand="SELECT nrPozitie, gospodarieId, nrPozitieInt FROM gospodarii WHERE (volum = @volum) AND (unitateId = @unitateId) AND (gospodarieId &lt;&gt; @gospodarieId) ORDER BY nrPozitieInt">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="ddlStergeCatreVolum" Name="volum" PropertyName="SelectedValue"
                                            Type="String" />
                                        <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" Type="Int32" />
                                        <asp:SessionParameter Name="gospodarieId" SessionField="SESgospodarieId" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                                <asp:Label ID="lblSterge" runat="server" CssClass="rosie" Text="Atenţie! Animalele vor fi adăugate automat în gospodăria cumpărătoare!"
                                    Visible="true" />
                                <asp:CheckBox ID="CheckBox2" runat="server" Checked="false" Visible="false" />
                            </asp:Panel>
                        </asp:Panel>
                        <div class="clear">
                        </div>
                        <asp:Panel ID="pnlStergeDetalii" runat="server" CssClass="bloc">
                            <asp:Panel ID="Panel1" runat="server" CssClass="in_linie">
                                <div class="clear" />
                                </div>
                                <asp:Label ID="lblStergeDetalii" runat="server" Text="Detalii ieşire (nume cumpărător, detalii contract etc.):" />
                                <div class="clear" />
                                </div>
                                <asp:TextBox ID="tbStergeDetalii" runat="server" Width="600" TextMode="MultiLine"
                                    Rows="4"></asp:TextBox>
                                <div class="clear" />
                                </div>
                            </asp:Panel>
                        </asp:Panel>
                        <asp:Panel ID="pnButoaneSterge" runat="server" CssClass="butoane">
                            <asp:Button ID="btStergeAnimal" runat="server" CssClass="buton" OnClick="Sterge"
                                Text="şterge - salvează ieşirea" ValidationGroup="GrupValidare" Visible="true" />
                            <asp:Button ID="btListaDinSterge" runat="server" CssClass="buton" OnClick="ArataLista"
                                Text="listă" Visible="true" />
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
