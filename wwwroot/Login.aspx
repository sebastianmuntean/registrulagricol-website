﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="_Default"
    UICulture="ro-RO" Culture="ro-RO" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>REGISTRUL AGRICOL - TNT COMPUTERS</title>
    <link href="css/master.css" rel="stylesheet" type="text/css" />
    <link href="css/login.css" rel="stylesheet" type="text/css" />
    <!--[if lte IE 7]><link rel="stylesheet" href="css/netcontaIE.css" media="screen, projection"><![endif]-->

    <script type="text/javascript" src="js/CompletareData.js"></script>

    <script src="js/jquery-1.6.1.min.js" type="text/javascript"></script>

    <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen"
        title="prettyPhoto main stylesheet" charset="utf-8" />

    <script src="js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>

</head>
<body>
    <div class="versiuneBeta">
        <a href="http://2016.tntregistruagricol.ro"><asp:Image ID="imgBeta" runat="server" ImageUrl="~/wwwroot/Imagini/versiuneabeta.png" ImageAlign="Right" Height="62px" Width="132px" /></a>
    </div>
    <form id="form1" runat="server">
        <div id="container">
            <div id="logo_login">
            </div>
            <div class="background_login">
                <asp:Panel ID="pnAfiseazaMesaje" runat="server">
                    <asp:ValidationSummary ID="valSumUtilizatori" runat="server" DisplayMode="SingleParagraph"
                        Visible="true" ValidationGroup="GrupValidareUtilizatori" CssClass="validator"
                        ForeColor="" />
                </asp:Panel>
                <div class="campuri_login_container">
                    <div class="campuri_login">
                        <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resursa, raUtilizator%>" />
                        <asp:TextBox ID="tbUtilizatorNume" runat="server"></asp:TextBox>
                    </div>
                    <div class="campuri_login">
                        <asp:Label ID="lblParola" runat="server" Text="<%$ Resources:Resursa, raParola%>" />
                        <asp:TextBox ID="tbParola" runat="server" TextMode="Password"></asp:TextBox>
                    </div>
                    <div class="campuri_login">
                        <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resursa, raCodulUnitatii%>" />
                        <asp:TextBox ID="tbUnitateId" runat="server"></asp:TextBox>
                    </div>
                    <div class="autentifica_login">
                        <div class="buton_login">
                            <asp:Button ID="btAutentificare" runat="server" Text="<%$ Resources:Resursa, raAutentificare%>"
                                OnClick="btAutentificare_Click" />
                        </div>
                    </div>
                </div>
                <div class="spatiu_orizontal">
                </div>
            </div>
            <asp:Panel ID="Panel1" CssClass="ascunsa" runat="server">
                <!-- //////////// VALIDATOARE  \\\\\\\\\\\\\\\\\\ -->
                <asp:RequiredFieldValidator ID="valLogin" ControlToValidate="tbUtilizatorNume" ValidationGroup="GrupValidareUtilizatori"
                    runat="server" ErrorMessage="Numele de utilizator este obligatoriu! (minim 8 caractere)"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="valParola" ControlToValidate="tbParola" ValidationGroup="GrupValidareUtilizatori"
                    runat="server" ErrorMessage="Parola este obligatorie! (minim 8 caractere)"></asp:RequiredFieldValidator>
                <asp:CustomValidator ID="valCustom" runat="server" ErrorMessage="bdbhdf" ValidationGroup="GrupValidareUtilizatori"></asp:CustomValidator>
            </asp:Panel>
            <asp:Panel ID="pnlSteaguri" runat="server" CssClass="steaguri">
                <asp:LinkButton ID="lkbRo" runat="server" ToolTip="Română" CssClass="lkbRo" OnClick="lkbRo_Click"
                    PostBackUrl="~/login.aspx" OnClientClick="window.document.forms[0].target='_self';"><span></span></asp:LinkButton>
                <asp:LinkButton ID="lbkHu" runat="server" ToolTip="Magyarul" CssClass="lkbHu" OnClick="lbkHu_Click"
                    PostBackUrl="~/login.aspx" OnClientClick="window.document.forms[0].target='_self';"><span></span></asp:LinkButton>
                <asp:LinkButton ID="lblEn" runat="server" ToolTip="English - under construction"
                    CssClass="lkbEn-0" OnClientClick="window.document.forms[0].target='_self';"><span></span></asp:LinkButton>
                <asp:LinkButton ID="lbkFr" runat="server" ToolTip="Français - en cours de construction"
                    CssClass="lkbFr-0" OnClientClick="window.document.forms[0].target='_self';"><span></span></asp:LinkButton>
                <asp:LinkButton ID="lbkIt" runat="server" ToolTip="Italiano - in costruzione" CssClass="lkbIt-0"
                    OnClientClick="window.document.forms[0].target='_self';"><span></span></asp:LinkButton>
            </asp:Panel>
        </div>
        <div id="banner" class="banner_patrat" style="text-align: center;">
            <div style="margin: 0 auto; padding: 0; border: 0; width: 729px; height: 312px; display: inline-block;">
                <iframe width="729px" height="312px" frameborder="0" scrolling="no" src="http://tntmag.ro/magazin-iframe-729-308.aspx"></iframe>
            </div>
        </div>
        <br />
        <div class="stiri">
            <h2>Ştiri despre Registrul Agricol Integrat</h2>
            <p>
                01.04.2012 - <a href="http://youtu.be/gt2TB6SMbp0" rel="prettyPhoto" title="Emisiunea Viaţa Satului (TVR1) despre Registrul Agricol Integrat">Emisiunea Viaţa Satului (TVR1) despre Registrul Agricol Integrat</a>
            </p>
            <a href="http://youtu.be/gt2TB6SMbp0" rel="prettyPhoto" title="Emisiunea Viaţa Satului (TVR1) despre Registrul Agricol Integrat">
                <img src="imagini/stiri/viata-satului-20120401.png" width="150" alt="01.04.2012 - Emisiunea Viaţa Satului (TVR1) despre Registrul Agricol Integrat" /></a>
        </div>
        <div class="stiri">
            <span><a href="http://tntcomputers.ro/suport.html" target="_blank">suport clienţi</a> | telefon fix: 0369 101 101 | fax: 0369 101 100</span><br />
            <span>
                <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="http://tntcomputers.ro/contact.aspx">Contact</asp:HyperLink>
                | 
            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="http://www.tntcomputers.ro">www.tntcomputers.ro</asp:HyperLink>
                <asp:HyperLink ID="HyperLink44" runat="server" NavigateUrl="http://www.tntmag.ro">www.tntmag.ro</asp:HyperLink>
            </span>
            <br />
            <span>&copy; 2011-2013 TNT COMPUTERS s.r.l.</span>

        </div>
        <!-- end container -->
    </form>

    <script type="text/javascript" charset="utf-8">
        $("a[rel^='prettyPhoto']").prettyPhoto({
            custom_markup: '<div id="map_canvas" style="width:490px; height:360px"></div>'
        });
    </script>
</body>
</html>
