﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class adminHelp : System.Web.UI.Page
{
    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "adminHelp", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
        }
    
    }
    protected void gvHelp_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(gvHelp, e, this);
    }
    protected void btAdauga_Click(object sender, EventArgs e)
    {
        ViewState["tip"] = "a";
        btSalveazaHelp.Text = "adaugă help";
        tbTextHelp.Text = "";
        pnGrid.Visible = false;
        pnButoanePrincipale.Visible = false;
        pnAdaugaHelp.Visible = true;
    }
    protected void btSterge_Click(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "delete from help where helpId='" + gvHelp.SelectedValue.ToString() + "'";
        vCmd.ExecuteNonQuery();
        ManipuleazaBD.InchideConexiune(vCon);
        gvHelp.DataBind();
    }
    protected void btModifica_Click(object sender, EventArgs e)
    {
        ViewState["tip"] = "m";
        btSalveazaHelp.Text = "modifică help";
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select * from help where helpId='" + gvHelp.SelectedValue.ToString() + "'";
        SqlDataReader vTabel = vCmd.ExecuteReader();
        if (vTabel.Read())
        {
            ddlPagina.DataBind();
            ddlPagina.SelectedValue = vTabel["paginaId"].ToString();
            tbTextHelp.Text = vTabel["helpText"].ToString();
        }
        vTabel.Close();
        ManipuleazaBD.InchideConexiune(vCon);
        pnGrid.Visible = false;
        pnButoanePrincipale.Visible = false;
        pnAdaugaHelp.Visible = true;
    }
    protected void btAnuleazaSalvarea_Click(object sender, EventArgs e)
    {
        pnGrid.Visible = true;
        pnButoanePrincipale.Visible = true;
        pnAdaugaHelp.Visible = false;
    }
    protected void btSalveazaHelp_Click(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        if (ViewState["tip"].ToString() == "a")
        {
            vCmd.CommandText = "insert into help (paginaId,helpText) values ('" + ddlPagina.SelectedValue.ToString() + "', N'" + tbTextHelp.Text + "')";
            vCmd.CommandText = vCmd.CommandText.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
            vCmd.ExecuteNonQuery();
        }
        else if (ViewState["tip"].ToString() == "m")
        {
            vCmd.CommandText = "update help set paginaId='" + ddlPagina.SelectedValue.ToString() + "', helpText=N'" + tbTextHelp.Text + "' where helpId='" + gvHelp.SelectedValue.ToString() + "'";
            vCmd.CommandText = vCmd.CommandText.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
            vCmd.ExecuteNonQuery();
        }
        ManipuleazaBD.InchideConexiune(vCon);
        pnGrid.Visible = true;
        pnButoanePrincipale.Visible = true;
        pnAdaugaHelp.Visible = false;
        gvHelp.DataBind();
    }
    protected void gvHelp_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (gvHelp.SelectedValue != null)
        {
            btModifica.Visible = true;
            btSterge.Visible = true;
        }
        else
        {
            btModifica.Visible = false;
            btSterge.Visible = false;
        }
    }
}
