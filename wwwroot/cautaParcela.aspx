﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="cautaParcela.aspx.cs" Inherits="cautaParcela" EnableEventValidation="false"
    Culture="ro-RO" UICulture="ro-RO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
     <asp:Label ID="url" runat="server" Text="Rapoarte / Căutare parcelă" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
 
            <asp:Panel ID="pnAfiseazaMesaje" runat="server">
                <asp:ValidationSummary ID="valSumParcele" runat="server" DisplayMode="SingleParagraph"
                    Visible="true" ValidationGroup="GrupValidareParcele" CssClass="validator" ForeColor="" />
            </asp:Panel>
            <!-- lista Parcele -->
            <asp:Panel ID="pnListaParcele" runat="server" CssClass="panel_general" Visible="true">
                <asp:Panel ID="pnCautare" runat="server" CssClass="cauta">
                    <asp:Label ID="lblCDenumire" runat="server" Text="Denumire" />
                    <asp:TextBox ID="tbCDenumire" runat="server" autocomplete="off" AutoPostBack="True"
                        Width="100px" />
                    <asp:Label ID="lblCTopo" runat="server" Text="TOPO" />
                    <asp:TextBox ID="tbCTopo" runat="server" autocomplete="off" AutoPostBack="True" Width="50px" />
                    <asp:Label ID="lblCCF" runat="server" Text="C.F." />
                    <asp:TextBox ID="tbCCF" runat="server" autocomplete="off" AutoPostBack="True" Width="80px" />
                    <asp:Label ID="lblCCategorie" runat="server" Text="Categoria" />
                    <asp:DropDownList ID="ddlCCategoria" runat="server" DataSourceID="sdsCategoria" DataTextField="denumire4"
                        DataValueField="codRand" Width="80" AutoPostBack="true" AppendDataBoundItems="True">
                        <asp:ListItem Value="%">toate categoriile</asp:ListItem>
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="sdsCategoria" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016 %>"
                        SelectCommand="SELECT codRand, denumire4 FROM sabloaneCapitole WHERE (an = @an) AND (capitol = '2a') AND (denumire4<>'') AND (CHARINDEX('+', formula) = 0) AND (CHARINDEX('-', formula) = 0)">
                        <SelectParameters>
                            <asp:SessionParameter Name="an" SessionField="SESan" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:Label ID="lblCBloc" runat="server" Text="Bloc fizic" />
                    <asp:TextBox ID="tbCBloc" runat="server" autocomplete="off" AutoPostBack="True" Width="50px" />
                    <asp:Label ID="lblCNrCadastral" runat="server" Text="Nr. cadastral" />
                    <asp:TextBox ID="tbCNrCadastral" runat="server" autocomplete="off" AutoPostBack="True"
                        Width="50px" />
                    <asp:Label ID="lblCNrCadastralProvizoriu" Visible="true" runat="server" Text="Nr. cadastral provizoriu" />
                    <asp:TextBox ID="tbCNrCadastralProvizoriu" Visible="true" runat="server" AutoPostBack="True" Width="50px" />
                </asp:Panel>
                <asp:Panel ID="pnCautare1" runat="server" CssClass="cauta">
                    <asp:Label ID="lblFJudet" runat="server" Text="Judeţ"></asp:Label>
                    <asp:DropDownList ID="ddlFJudet" AutoPostBack="true" runat="server" OnPreRender="ddlFJudet_PreRender"
                        OnSelectedIndexChanged="ddlFJudet_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:Label ID="lblUnitate" runat="server" Text="unitatea"></asp:Label>
                    <asp:DropDownList ID="ddlUnitate" AutoPostBack="true" runat="server" OnInit="ddlUnitate_Init"
                        OnSelectedIndexChanged="ddlUnitate_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:Label ID="localitateLabel" runat="server" Text="parcela localitate:"></asp:Label>
                    <asp:DropDownList ID="localitateDropDown" AutoPostBack="true" runat="server" OnInit="localitateDropDown_Init"
                        OnSelectedIndexChanged="localitateDropDown_SelectedIndexChanged" AppendDataBoundItems="True">
                    </asp:DropDownList>
                    <asp:Label ID="lblTitluProprietate" runat="server" Text="Titlu de proprietate" />
                    <asp:TextBox ID="tbTitluProprietate" runat="server" AutoPostBack="True" Width="150px" />
                </asp:Panel>
                <asp:Panel ID="pnCautare2" runat="server" CssClass="cauta">
                    <asp:Label ID="lblFOrdonare" runat="server" Text="Ordonare după"></asp:Label>
                    <asp:DropDownList ID="ddlFOrdonare" AutoPostBack="true" runat="server" 
                        onselectedindexchanged="ddlFOrdonare_SelectedIndexChanged">
                        <asp:ListItem Text="Volum, pozitie" Value="0"></asp:ListItem>
                        <asp:ListItem Text="Localitate, strada, nr." Value="1"></asp:ListItem>
                        <asp:ListItem Text="Nume" Value="2"></asp:ListItem>
                        <asp:ListItem Text="Tarla" Value="3"></asp:ListItem>
                        <asp:ListItem Text="Nr. cadastral" Value="4"></asp:ListItem>
                        <asp:ListItem Text="Nr. cadastral provizoriu" Value="5"></asp:ListItem>
                    </asp:DropDownList>
                </asp:Panel>
                <asp:Panel ID="pnParcele" runat="server">
                    <asp:GridView ID="gvParcele" AllowPaging="True" DataKeyNames="parcelaId" AllowSorting="True"
                        CssClass="tabela" runat="server" AutoGenerateColumns="False" DataSourceID="SqlParcele"
                        OnSelectedIndexChanged="gvParcele_SelectedIndexChanged" OnRowDataBound="gvParcele_RowDataBound">
                        <Columns>
                            <asp:BoundField DataField="parcelaId" HeaderText="Nr." SortExpression="parcelaId"
                                Visible="false" />
                            <asp:BoundField DataField="volum" HeaderText="Volum" SortExpression="volum" />
                            <asp:BoundField DataField="nrPozitie" HeaderText="Nr pozitie" SortExpression="nrPozitie" />
                            <asp:BoundField DataField="nume" HeaderText="Nume" SortExpression="nume" />
                            <asp:BoundField DataField="parcelaDenumire" HeaderText="Denumirea parcelei / tarlalei / solei"
                                SortExpression="parcelaDenumire" />
                            <asp:BoundField DataField="parcelaCodRand" HeaderText="Cod rând" SortExpression="parcelaCodRand"
                                Visible="false" />
                            <asp:BoundField DataField="parcelaSuprafataIntravilanHa" HeaderText="Intravilan Ha"
                                SortExpression="parcelaSuprafataIntravilanHa" />
                            <asp:BoundField DataField="parcelaSuprafataIntravilanAri" HeaderText="Ari" SortExpression="parcelaSuprafataIntravilanAri" />
                            <asp:BoundField DataField="parcelaSuprafataExtravilanHa" HeaderText="Extravilan Ha"
                                SortExpression="parcelaSuprafataExtravilanHa" />
                            <asp:BoundField DataField="parcelaSuprafataExtravilanAri" HeaderText="Ari" SortExpression="parcelaSuprafataExtravilanAri" />
                            <asp:BoundField DataField="parcelaNrTopo" HeaderText="Nr.Topo" SortExpression="parcelaNrTopo" />
                            <asp:BoundField DataField="parcelaCF" HeaderText="C.F." SortExpression="parcelaCF" />
                            <asp:BoundField DataField="parcelaNrCadastral" HeaderText="Nr.Cadastral" SortExpression="parcelaNrCadastral" />
                            <asp:BoundField DataField="parcelaNrCadastralProvizoriu" HeaderText="Nr.Cad.Provizoriu"
                                SortExpression="parcelaNrCadastralProvizoriu" />
                            <asp:BoundField DataField="denumire4" HeaderText="Categorie" SortExpression="denumire4" />
                            <asp:BoundField DataField="parcelaNrBloc" HeaderText="Bloc fizic" SortExpression="parcelaNrBloc" />
                            <asp:BoundField DataField="an" HeaderText="an" SortExpression="anul" Visible="false" />
                            <asp:BoundField DataField="parcelaTitluProprietate" HeaderText="Tit. propr." SortExpression="parcelaTitluProprietate"
                                Visible="true" />
                        </Columns>
                        <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlParcele" runat="server" 
                        SelectCommand="SELECT TOP(100) parcele.parcelaId, parcele.parcelaDenumire, parcele.parcelaCodRand, parcele.parcelaSuprafataIntravilanHa, parcele.parcelaSuprafataIntravilanAri, parcele.parcelaSuprafataExtravilanHa, parcele.parcelaSuprafataExtravilanAri, parcele.parcelaNrTopo, parcele.parcelaCategorie, parcele.parcelaCF, sabloaneCapitole.denumire4, parcele.parcelaNrBloc, parcele.parcelaMentiuni, parcele.an, parcele.parcelaLocalitate, parcele.parcelaAdresa, parcele.parcelaNrCadastralProvizoriu, parcele.parcelaNrCadastral, gospodarii.volum, gospodarii.nrPozitie, CASE persjuridica WHEN 0 THEN membri.nume ELSE junitate END AS nume, parcele.parcelaTitluProprietate FROM parcele INNER JOIN sabloaneCapitole ON parcele.parcelaCategorie = sabloaneCapitole.codRand INNER JOIN gospodarii ON parcele.gospodarieId = gospodarii.gospodarieId INNER JOIN unitati ON gospodarii.unitateId = unitati.unitateId LEFT OUTER JOIN membri ON gospodarii.gospodarieId = membri.gospodarieId AND membri.codRudenie = 1 AND membri.an = @an WHERE (sabloaneCapitole.capitol = '2a') AND (parcele.an = @an) AND (sabloaneCapitole.an = @an) AND (parcele.parcelaCategorie LIKE @parcelaCategorie ) AND (parcele.parcelaCF LIKE '%' + @parcelaCF + '%') AND (parcele.parcelaDenumire LIKE '%' + @parcelaDenumire + '%') AND (parcele.parcelaNrBloc LIKE '%' + @parcelaNrBloc + '%') AND (parcele.parcelaNrTopo LIKE '%' + @parcelaNrTopo + '%') AND (parcele.parcelaNrCadastral LIKE '%' + @parcelaNrCadastral + '%') AND (parcele.parcelaNrCadastralProvizoriu LIKE '%' + @parcelaNrCadastralProvizoriu + '%') AND (parcele.parcelaLocalitate Like '%' +@parcelaLocalitate +'%')  AND (parcele.parcelaTitluProprietate LIKE '%' + @parcelaTitluProprietate + '%')  AND (CONVERT (nvarchar, gospodarii.unitateId) LIKE @unitateId) AND (CONVERT (nvarchar, unitati.judetId) LIKE @judetId)"
                        OnSelecting="SqlParcele_Selecting">
                        <SelectParameters>
                            <asp:SessionParameter DefaultValue="2016" Name="an" SessionField="SESan" Type="Int32" />
                            <asp:ControlParameter ControlID="ddlCCategoria" Name="parcelaCategorie" PropertyName="SelectedValue" />
                            <asp:ControlParameter ControlID="tbCCF" DefaultValue="%" Name="parcelaCF" PropertyName="Text"
                                Type="String" />
                            <asp:ControlParameter ControlID="localitateDropDown" Name="parcelaLocalitate" PropertyName="SelectedValue" />
                            <asp:ControlParameter ControlID="tbCDenumire" DefaultValue="%" Name="parcelaDenumire"
                                PropertyName="Text" Type="String" />
                            <asp:ControlParameter ControlID="tbCBloc" DefaultValue="%" Name="parcelaNrBloc" PropertyName="Text"
                                Type="String" />
                            <asp:ControlParameter ControlID="tbCTopo" DefaultValue="%" Name="parcelaNrTopo" PropertyName="Text"
                                Type="String" />
                            <asp:ControlParameter ControlID="tbCNrCadastral" DefaultValue="%" Name="parcelaNrCadastral"
                                PropertyName="Text" Type="String" />
                            <asp:ControlParameter ControlID="tbCNrCadastralProvizoriu" DefaultValue="%" Name="parcelaNrCadastralProvizoriu"
                                PropertyName="Text" Type="String" />
                            <asp:ControlParameter ControlID="tbTitluProprietate" DefaultValue="%" Name="parcelaTitluProprietate"
                                PropertyName="Text" Type="String" />
                            <asp:ControlParameter ControlID="ddlUnitate" DefaultValue="" Name="unitateId" PropertyName="SelectedValue" />
                            <asp:ControlParameter ControlID="ddlFJudet" Name="judetId" PropertyName="SelectedValue" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </asp:Panel>
                <asp:Panel ID="pnListaButoanePrincipale" CssClass="butoane" runat="server">
                    <asp:Button CssClass="buton" ID="btTiparireIstoricParcela" runat="server" Text="istoric parcelă"
                        Visible="false" OnClick="btTiparireIstoricParcela_Click" />
                    <asp:Button CssClass="buton" ID="btTiparireParceleCautare" runat="server" Text="tipărire parcele"
                        OnClick="btTiparireParceleCautare_Click" />
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="pnIstoricParcela" CssClass="panel_general" runat="server" Visible="false">
                <asp:Panel ID="Panel1" CssClass="adauga" runat="server">
                    <asp:Panel ID="pnEroare" runat="server">
                        <h2>
                            ISTORIC PARCELĂ</h2>
                    </asp:Panel>
                    <asp:Panel ID="Panel3" runat="server">
                        <asp:GridView ID="gvIstoricParcela" AllowPaging="True" AllowSorting="True" CssClass="tabela"
                            runat="server" AutoGenerateColumns="False" DataSourceID="SqlIstoricParcela">
                            <Columns>
                                <asp:BoundField DataField="parcelaDenumire" HeaderText="Denumire parcela" SortExpression="parcelaDenumire" />
                                <asp:BoundField DataField="data" DataFormatString="{0:d}" HeaderText="Data transfer"
                                    HtmlEncode="False" SortExpression="data" />
                                <asp:BoundField DataField="denumire" HeaderText="Tip transfer" SortExpression="denumire" />
                                <asp:BoundField DataField="documentAtesta" HeaderText="Document care atesta transferul"
                                    SortExpression="documentAtesta" />
                                <asp:BoundField DataField="observatii" HeaderText="Observatii transfer" SortExpression="observatii" />
                                <asp:BoundField DataField="deLa" HeaderText="De la" SortExpression="deLa" />
                                <asp:BoundField DataField="la" HeaderText="La" SortExpression="la" />
                            </Columns>
                            <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlIstoricParcela" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString %>"
                            SelectCommand="SELECT parcele.parcelaDenumire, istoricParcela.data, tipTransferParcela.denumire, istoricParcela.documentAtesta, istoricParcela.observatii, gospodarii.volum AS volumDeLa, gospodarii.nrPozitie AS nrPozitieDeLa, 'Volum ' + COALESCE (gospodarii.volum, '') + ', nr. pozitie ' + gospodarii.nrPozitie + ', nume ' + CASE gospodarii.persjuridica WHEN 0 THEN membri.nume ELSE gospodarii.junitate END AS deLa, 'Volum ' + COALESCE (gospodarii_1.volum, '') + ', nr. pozitie ' + COALESCE (gospodarii_1.nrPozitie, '') + ', nume ' + CASE gospodarii_1.persjuridica WHEN 0 THEN membri_1.nume ELSE gospodarii_1.junitate END AS la FROM istoricParcela INNER JOIN parcele ON istoricParcela.parcelaId = parcele.parcelaId INNER JOIN tipTransferParcela ON istoricParcela.motiv = tipTransferParcela.tipTransferId INNER JOIN gospodarii ON istoricParcela.deLa = gospodarii.gospodarieId INNER JOIN gospodarii AS gospodarii_1 ON istoricParcela.la = gospodarii_1.gospodarieId INNER JOIN membri AS membri_1 ON gospodarii_1.gospodarieId = membri_1.gospodarieId LEFT OUTER JOIN membri ON gospodarii.gospodarieId = membri.gospodarieId WHERE (membri.an = @an) AND (membri_1.an = @an) AND (membri_1.codRudenie = 1) AND (membri_1.codRudenie = 1) AND (istoricParcela.parcelaId = @parcelaId)">
                            <SelectParameters>
                                <asp:SessionParameter Name="an" SessionField="SESan" />
                                <asp:ControlParameter ControlID="gvParcele" Name="parcelaId" PropertyName="SelectedValue" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <asp:Panel ID="pnButoaneAdaugare" runat="server" CssClass="butoane">
                            <asp:Button ID="btAnuleazaSalvarea" runat="server" CssClass="buton" Text="lista parcele"
                                OnClick="btAnuleazaSalvarea_Click" />
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
 
</asp:Content>
