﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class mesajeAdmin : System.Web.UI.Page
{
    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        DBConnections connection = new DBConnections(Convert.ToInt16(Session["SESan"]));

        SqlMesaje.ConnectionString = connection.Create();
        SqlUtilizatori.ConnectionString = connection.Create();

        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "mesajeAdmin", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
        }
    }
    protected void ddlFDeLaUnitatea_Init(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select top(1) tipUtilizatorId from utilizatori where utilizatorId='" + Session["SESutilizatorId"].ToString() + "'";
        int vTipUtilizator = Convert.ToInt32(vCmd.ExecuteScalar());
        if (vTipUtilizator == 2 || vTipUtilizator == 3 || vTipUtilizator == 5)
        {
            vCmd.CommandText = "SELECT TOP (1) unitati.judetId FROM utilizatori INNER JOIN unitati ON utilizatori.unitateId = unitati.unitateId WHERE (utilizatori.utilizatorId = '" + Session["SESutilizatorId"].ToString() + "')";
            string vJudetId = vCmd.ExecuteScalar().ToString();
            ddlFDeLaJudet.Items.Clear();
            ddlFDeLaJudet.Items.Add(new ListItem("", vJudetId));
            vCmd.CommandText = "select unitateDenumire,unitateId from unitati where judetId='" + vJudetId + "' order by unitateDenumire";
            ddlFDeLaUnitatea.Items.Clear();
            ListItem vItem = new ListItem("-toate-", "%");
            ddlFDeLaUnitatea.Items.Add(vItem);
            SqlDataReader vTabel = vCmd.ExecuteReader();
            while (vTabel.Read())
            {
                vItem = new ListItem(vTabel["unitateDenumire"].ToString(), vTabel["unitateId"].ToString());
                ddlFDeLaUnitatea.Items.Add(vItem);
            }
            vTabel.Close();
            if (ViewState["fDeLaUnitatea"] == null)
                ViewState["fDeLaUnitatea"] = "%";
            ddlFDeLaUnitatea.SelectedValue = ViewState["fDeLaUnitatea"].ToString();
            ((MasterPage)this.Page.Master).SchimbaGospodaria();
        }
        ManipuleazaBD.InchideConexiune(vCon);
    }
    protected void ddlFDeLaUnitatea_SelectedIndexChanged(object sender, EventArgs e)
    {
        ViewState["fDeLaUnitatea"] = ddlFDeLaUnitatea.SelectedValue;
    }
    protected void ddlFLaUnitatea_Init(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select top(1) tipUtilizatorId from utilizatori where utilizatorId='" + Session["SESutilizatorId"].ToString() + "'";
        int vTipUtilizator = Convert.ToInt32(vCmd.ExecuteScalar());
        if (vTipUtilizator == 2 || vTipUtilizator == 3 || vTipUtilizator == 5)
        {
            vCmd.CommandText = "SELECT TOP (1) unitati.judetId FROM utilizatori INNER JOIN unitati ON utilizatori.unitateId = unitati.unitateId WHERE (utilizatori.utilizatorId = '" + Session["SESutilizatorId"].ToString() + "')";
            string vJudetId = vCmd.ExecuteScalar().ToString();
            ddlFLaJudet.Items.Clear();
            ddlFLaJudet.Items.Add(new ListItem("", vJudetId));
            vCmd.CommandText = "select unitateDenumire,unitateId from unitati where judetId='" + vJudetId + "' order by unitateDenumire";
            ddlFLaUnitatea.Items.Clear();
            ListItem vItem = new ListItem("-toate-", "%");
            ddlFLaUnitatea.Items.Add(vItem);
            SqlDataReader vTabel = vCmd.ExecuteReader();
            while (vTabel.Read())
            {
                vItem = new ListItem(vTabel["unitateDenumire"].ToString(), vTabel["unitateId"].ToString());
                ddlFLaUnitatea.Items.Add(vItem);
            }
            vTabel.Close();
            if (ViewState["fLaUnitatea"] == null)
                ViewState["fLaUnitatea"] = "%";
            ddlFLaUnitatea.SelectedValue = ViewState["fLaUnitatea"].ToString();
            ((MasterPage)this.Page.Master).SchimbaGospodaria();
        }
        ManipuleazaBD.InchideConexiune(vCon);
    }
    protected void ddlFLaUnitatea_SelectedIndexChanged(object sender, EventArgs e)
    {
        ViewState["fLaUnitatea"] = ddlFLaUnitatea.SelectedValue;
    }
    protected void ddlFDeLaJudet_PreRender(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select judetDenumire,judetId from judete order by judetDenumire";
        ddlFDeLaJudet.Items.Clear();
        ListItem vItem = new ListItem("-toate-", "%");
        ddlFDeLaJudet.Items.Add(vItem);
        SqlDataReader vTabel = vCmd.ExecuteReader();
        while (vTabel.Read())
        {
            vItem = new ListItem(vTabel["judetDenumire"].ToString(), vTabel["judetId"].ToString());
            ddlFDeLaJudet.Items.Add(vItem);
        }
        vTabel.Close();
        if (ViewState["fDeLaUnitatea"] == null)
            ViewState["fDeLaUnitatea"] = "%";
        vCmd.CommandText = "select top(1) coalesce(judetId,0) from unitati where convert(nvarchar,unitateId)='" + ViewState["fDeLaUnitatea"].ToString() + "'";
        if (ViewState["fDeLaJudet"] == null)
            ViewState["fDeLaJudet"] = "%";
            //try { ViewState["fDeLaJudet"] = vCmd.ExecuteScalar().ToString(); }
            //catch { ViewState["fDeLaJudet"] = "%"; }
        vCmd.CommandText = "select unitateDenumire,unitateId from unitati where convert(nvarchar,judetId) like '" + ViewState["fDeLaJudet"].ToString() + "' order by unitateDenumire";
        ddlFDeLaUnitatea.Items.Clear();
        vItem = new ListItem("-toate-", "%");
        ddlFDeLaUnitatea.Items.Add(vItem);
        vTabel = vCmd.ExecuteReader();
        while (vTabel.Read())
        {
            vItem = new ListItem(vTabel["unitateDenumire"].ToString(), vTabel["unitateId"].ToString());
            ddlFDeLaUnitatea.Items.Add(vItem);
        }
        vTabel.Close();
        ManipuleazaBD.InchideConexiune(vCon);
        try
        {
            ddlFDeLaUnitatea.SelectedValue = ViewState["fDeLaUnitatea"].ToString();
        }
        catch
        {
            ddlFDeLaUnitatea.SelectedValue = "%";
        }
        ddlFDeLaJudet.SelectedValue = ViewState["fDeLaJudet"].ToString();
    }
    protected void ddlFDeLaJudet_SelectedIndexChanged(object sender, EventArgs e)
    {
        ViewState["fDeLaJudet"] = ddlFDeLaJudet.SelectedValue;
    }
    protected void ddlFLaJudet_PreRender(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select judetDenumire,judetId from judete order by judetDenumire";
        ddlFLaJudet.Items.Clear();
        ListItem vItem = new ListItem("-toate-", "%");
        ddlFLaJudet.Items.Add(vItem);
        SqlDataReader vTabel = vCmd.ExecuteReader();
        while (vTabel.Read())
        {
            vItem = new ListItem(vTabel["judetDenumire"].ToString(), vTabel["judetId"].ToString());
            ddlFLaJudet.Items.Add(vItem);
        }
        vTabel.Close();
        if (ViewState["fLaUnitatea"] == null)
            ViewState["fLaUnitatea"] = "%";
        vCmd.CommandText = "select top(1) coalesce(judetId,0) from unitati where convert(nvarchar,unitateId) like '" + ViewState["fLaUnitatea"].ToString() + "'";
        if (ViewState["fLajudet"] == null)
            ViewState["fLaJudet"] = "%";
            //try { ViewState["fLaJudet"] = vCmd.ExecuteScalar().ToString(); }
            //catch { ViewState["fLaJudet"] = "%"; }
        vCmd.CommandText = "select unitateDenumire,unitateId from unitati where convert(nvarchar,judetId) like '" + ViewState["fLaJudet"].ToString() + "' order by unitateDenumire";
        ddlFLaUnitatea.Items.Clear();
        vItem = new ListItem("-toate-", "%");
        ddlFLaUnitatea.Items.Add(vItem);
        vTabel = vCmd.ExecuteReader();
        while (vTabel.Read())
        {
            vItem = new ListItem(vTabel["unitateDenumire"].ToString(), vTabel["unitateId"].ToString());
            ddlFLaUnitatea.Items.Add(vItem);
        }
        vTabel.Close();
        ManipuleazaBD.InchideConexiune(vCon);
        try
        {
            ddlFLaUnitatea.SelectedValue = ViewState["fLaUnitatea"].ToString();
        }
        catch
        {
            ddlFLaUnitatea.SelectedValue = "%";
        }
        ddlFLaJudet.SelectedValue = ViewState["fLaJudet"].ToString();
    }
    protected void ddlFLaJudet_SelectedIndexChanged(object sender, EventArgs e)
    {
        ViewState["fLaJudet"] = ddlFLaJudet.SelectedValue;
    }
    protected void ddlFUnitate_Init(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select top(1) tipUtilizatorId from utilizatori where utilizatorId='" + Session["SESutilizatorId"].ToString() + "'";
        int vTipUtilizator = Convert.ToInt32(vCmd.ExecuteScalar());
        if (vTipUtilizator == 2 || vTipUtilizator == 3 || vTipUtilizator == 5)
        {
            vCmd.CommandText = "SELECT TOP (1) unitati.judetId FROM utilizatori INNER JOIN unitati ON utilizatori.unitateId = unitati.unitateId WHERE (utilizatori.utilizatorId = '" + Session["SESutilizatorId"].ToString() + "')";
            string vJudetId = vCmd.ExecuteScalar().ToString();
            ddlFJudet.Items.Clear();
            ddlFJudet.Items.Add(new ListItem("", vJudetId));
            vCmd.CommandText = "select unitateDenumire,unitateId from unitati where judetId='" + vJudetId + "' order by unitateDenumire";
            ddlFUnitate.Items.Clear();
            ListItem vItem = new ListItem("-toate-", "%");
            ddlFUnitate.Items.Add(vItem);
            SqlDataReader vTabel = vCmd.ExecuteReader();
            while (vTabel.Read())
            {
                vItem = new ListItem(vTabel["unitateDenumire"].ToString(), vTabel["unitateId"].ToString());
                ddlFUnitate.Items.Add(vItem);
            }
            vTabel.Close();
            if (ViewState["fUnitate"] == null)
                ViewState["fUnitate"] = "%";
            ddlFUnitate.SelectedValue = ViewState["fUnitate"].ToString();
            ((MasterPage)this.Page.Master).SchimbaGospodaria();
        }
        ManipuleazaBD.InchideConexiune(vCon);
    }
    protected void ddlFUnitate_SelectedIndexChanged(object sender, EventArgs e)
    {
        ViewState["fUnitate"] = ddlFUnitate.SelectedValue;
    }
    protected void ddlFJudet_PreRender(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select judetDenumire,judetId from judete order by judetDenumire";
        ddlFJudet.Items.Clear();
        ListItem vItem = new ListItem("-toate-", "%");
        ddlFJudet.Items.Add(vItem);
        SqlDataReader vTabel = vCmd.ExecuteReader();
        while (vTabel.Read())
        {
            vItem = new ListItem(vTabel["judetDenumire"].ToString(), vTabel["judetId"].ToString());
            ddlFJudet.Items.Add(vItem);
        }
        vTabel.Close();
        if (ViewState["fUnitate"] == null)
            ViewState["fUnitate"] = "%";
        vCmd.CommandText = "select top(1) coalesce(judetId,0) from unitati where convert(nvarchar,unitateId) like '" + ViewState["fUnitate"].ToString() + "'";
        if (ViewState["fJudet"] == null)
            ViewState["fJudet"] = "%";
            //try { ViewState["fJudet"] = vCmd.ExecuteScalar().ToString(); }
            //catch { ViewState["fJudet"] = "%"; }
        vCmd.CommandText = "select unitateDenumire,unitateId from unitati where convert(nvarchar,judetId) like '" + ViewState["fJudet"].ToString() + "' order by unitateDenumire";
        ddlFUnitate.Items.Clear();
        vItem = new ListItem("-toate-", "%");
        ddlFUnitate.Items.Add(vItem);
        vTabel = vCmd.ExecuteReader();
        while (vTabel.Read())
        {
            vItem = new ListItem(vTabel["unitateDenumire"].ToString(), vTabel["unitateId"].ToString());
            ddlFUnitate.Items.Add(vItem);
        }
        vTabel.Close();
        ManipuleazaBD.InchideConexiune(vCon);
        try
        {
            ddlFUnitate.SelectedValue = ViewState["fUnitate"].ToString();
        }
        catch
        {
            ddlFUnitate.SelectedValue = "%";
        }
        ddlFJudet.SelectedValue = ViewState["fJudet"].ToString();
        if (cblUtilizatori.Items.Count == 0)
            cblUtilizatori.DataBind();
    }
    protected void ddlFJudet_SelectedIndexChanged(object sender, EventArgs e)
    {
        ViewState["fJudet"] = ddlFJudet.SelectedValue;
    }
    protected void gvMesaje_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(gvMesaje, e, this);
    }
    protected void gvMesaje_SelectedIndexChanged(object sender, EventArgs e)
    {
        btModifica.Visible = true;
        btSterge.Visible = true;
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select mesaj from mesaje where mesajId='" + gvMesaje.SelectedValue + "'";
        lblMesajText.Text = vCmd.ExecuteScalar().ToString();
        pnDetaliiMesaj.Visible = true;
        ManipuleazaBD.InchideConexiune(vCon);
    }
    protected void btAdauga_Click(object sender, EventArgs e)
    {
        ViewState["tip"] = "a";
        lblAdaugaMesaj.Text = "ADAUGĂ MESAJ";
        cblUtilizatori.DataBind();
        for (int a = 0; a < cblUtilizatori.Items.Count; a++)
            cblUtilizatori.Items[a].Selected = false;
        cblUtilizatori.Enabled = true;
        tbSubiect.Text = "";
        tbMesaj.Text = "";
        pnCautare.Visible = false;
        pnGrid.Visible = false;
        pnButoane.Visible = false;
        pnAdaugaMesaj.Visible = true;
        pnDetaliiMesaj.Visible = false;
        btSalveazaMesaj.Text = "adaugă un mesaj";
    }
    protected bool trimiteEmail()
    {
        bool vTrece = true;
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd1 = new SqlCommand();
        SqlCommand vCmd2 = new SqlCommand();
        vCmd1.Connection = vCon;
        vCmd2.Connection = vCon;
        if (tbMesaj.Text != "")
        {
            string vInterogare = "SELECT * FROM utilizatori WHERE utilizatorId IN (";
            // luam toti utilizatorii selectati si le cautam adresele de email si numele
            for (int a = 0; a < cblUtilizatori.Items.Count; a++)
            {
                if (cblUtilizatori.Items[a].Selected)
                {
                    vInterogare += cblUtilizatori.Items[a].Value + ", ";
                }
            }
            vInterogare = vInterogare.Remove(vInterogare.Length - 2) + ")";
            List<string> vCampuri = new List<string> { "utilizatorEmail", "utilizatorNume", "utilizatorPrenume" };
            List<List<string>> vListaDestinatari = ManipuleazaBD.fRezultaListaStringuri(vInterogare, vCampuri, Convert.ToInt16(Session["SESan"]));
            List<List<string>> vListaDestinatariCorectata = new List<List<string>> { };
            List<List<string>> vListaDestinatariTampon = vListaDestinatariCorectata;
            // daca sunt adrese la fel nu trimitem decat o singura data; la fel si daca sunt adrese invalide
            int vExista = 0;
            foreach (List<string> vDestinar in vListaDestinatari)
            {
                foreach (List<string> vDestinatarCorectat in vListaDestinatariCorectata)
                {
                    if (vDestinar[0] == vDestinatarCorectat[0] || vDestinar[0].IndexOf('@') == -1 || vDestinar[0].IndexOf('.') == -1 || vDestinar[0].Length < 6 || vDestinar[0] == "adresaemail@x.com" || vDestinar[0] == "")
                    {
                        vExista = 1;
                        break;
                    }
                    else
                        vExista = 0;
                }
                if (vExista == 0  )
                {
                    if (vDestinar[0].IndexOf('@') == -1 || vDestinar[0].IndexOf('.') == -1 || vDestinar[0].Length < 6 || vDestinar[0] == "adresaemail@x.com" || vDestinar[0] == "")
                    { }
                    else 
                    vListaDestinatariCorectata.Add(vDestinar);
                }
            }
            vListaDestinatariTampon = vListaDestinatariCorectata;

            System.Net.Mail.MailAddress vDeLa = new System.Net.Mail.MailAddress("office@tntcomputers.ro", "TNT COMPUTERS Registrul Agricol");
            System.Net.Mail.MailAddress vPentru = new System.Net.Mail.MailAddress("office@tntcomputers.ro");
            System.Net.Mail.MailMessage vMsg = new System.Net.Mail.MailMessage(vDeLa, vPentru);
            vMsg.Body = @"" + tbMesaj.Text + "<br/><hr />Acest mesaj a fost trimis de TNT COMPUTERS SRL prin intermediul programului Registrul Agricol www.tntregistrulagricol.ro. Pentru a răspunde acestui email vă rugăm să folosiţi doar pagina Semnalează Eroare din cadrul programului, sau să ne contactaţi la telefon fix: 0369 101 101  sau fax: 0369 101 100. ";
            vMsg.Subject = tbSubiect.Text;
            vMsg.IsBodyHtml = true;
         //   System.Net.NetworkCredential vAutentificare = new System.Net.NetworkCredential("sebastian.muntean@tntcomputers.ro", "sebastianmuntean1970");
            System.Net.NetworkCredential vAutentificare = new System.Net.NetworkCredential("auto@tntcomputers.ro", "traxdata");
            System.Net.Mail.SmtpClient vClient = new System.Net.Mail.SmtpClient("smtp.gmail.com", 587);
            vClient.EnableSsl = true;
            vClient.UseDefaultCredentials = false;
            vClient.Credentials = vAutentificare;
            // adaugam la adresele destinatare
            int vContor = 0; // formez doar cate 30 de adrese la un email
            vListaDestinatariTampon = vListaDestinatariCorectata;
            while (vListaDestinatariTampon.Count > 0)
            {
                vContor = 0;
                vListaDestinatariTampon = new List<List<string>> { };
                vMsg.To.Clear();
                for (int i = 0; i < 30; i++)
                    if (vListaDestinatariCorectata.Count > i)
                    {
                        vPentru = new System.Net.Mail.MailAddress(vListaDestinatariCorectata[i][0], vListaDestinatariCorectata[i][1] + " " + vListaDestinatariCorectata[i][2]);
                        vMsg.To.Add(vPentru);
                        vListaDestinatariTampon.Add(vListaDestinatariCorectata[i]);
                        vContor++;
                        if (vContor >= 30 || vContor == vListaDestinatariCorectata.Count)
                        {
                            try
                            {
                                vClient.Send(vMsg);
                                lblEroare.Visible = true;
                                lblEroare.Text = "Mesajul dumneavoastră a fost trimis cu succes";
                                vTrece = true;
                            }
                            catch (Exception ex)
                            {
                                lblEroare.Visible = true;
                                lblEroare.Text = "Mesajul dumneavoastră nu a putut fi trimis. Vă rugăm să mai încercaţi o dată! Eroarea este: " + ex.Message;
                                vTrece = false;
                            }
                            break;
                        }
                    }
                    else
                    {
                        break;
                    }
                foreach (List<string> vDestinatar in vListaDestinatariTampon)
                {
                    vListaDestinatariCorectata.Remove(vDestinatar);
                }
            }
            
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "semnalareEroare", "trimis mesaj admin", "", Convert.ToInt64(Session["SESgospodarieId"]), 2);
        }
        else
        {
            lblEroare.Visible = true;
            lblEroare.Text = "Vă rugăm sa completaţi câmpul de mai jos";
            vTrece = false;
        }
        ManipuleazaBD.InchideConexiune(vCon);
        return vTrece;
    }
    protected void btModifica_Click(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select utilizatorId, subiect, mesaj from mesaje where mesajId='" + gvMesaje.SelectedValue + "'";
        SqlDataReader vTabel = vCmd.ExecuteReader();
        if (vTabel.Read())
        {
            ViewState["tip"] = "m";
            lblAdaugaMesaj.Text = "MODIFICĂ MESAJ";
            cblUtilizatori.DataBind();
            for (int a = 0; a < cblUtilizatori.Items.Count; a++)
                if (vTabel["utilizatorId"].ToString() == cblUtilizatori.Items[a].Value.ToString())
                    cblUtilizatori.Items[a].Selected = true;
                else cblUtilizatori.Items[a].Selected = false;
            cblUtilizatori.Enabled = false;
            tbSubiect.Text = vTabel["subiect"].ToString();
            tbMesaj.Text = vTabel["mesaj"].ToString();
            pnCautare.Visible = false;
            pnGrid.Visible = false;
            pnButoane.Visible = false;
            pnDetaliiMesaj.Visible = false;
            pnAdaugaMesaj.Visible = true;
            btSalveazaMesaj.Text = "modifică mesaj";
        }
        ManipuleazaBD.InchideConexiune(vCon);
    }
    protected void btSterge_Click(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "delete from mesaje where mesajId='" + gvMesaje.SelectedValue + "'";
        vCmd.ExecuteNonQuery();
        ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "mesajeAdmin", "stergere mesaj admin", "", Convert.ToInt64(Session["SESgospodarieId"]), 4);
        ManipuleazaBD.InchideConexiune(vCon);
        gvMesaje.DataBind();
        pnDetaliiMesaj.Visible = false;
    }
    protected void btAnuleazaMesaj_Click(object sender, EventArgs e)
    {
        pnCautare.Visible = true;
        pnGrid.Visible = true;
        pnButoane.Visible = true;
        pnAdaugaMesaj.Visible = false;
    }
    protected void btSalveazaMesaj_Click(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        // salvez mesajul pentru fiecare utilizator selectat
        if (ViewState["tip"].ToString() == "a")
        {
            if (!trimiteEmail()) return;
            for (int a = 0; a < cblUtilizatori.Items.Count; a++)
            {
                if (cblUtilizatori.Items[a].Selected)
                {
                    vCmd.CommandText = "insert into mesaje (unitateId, utilizatorId, utilizatorIdDeLa, dataAdaugarii, subiect, mesaj) values ('" + Session["SESunitateId"].ToString() + "', '" + cblUtilizatori.Items[a].Value + "', '" + Session["SESutilizatorId"].ToString() + "', convert(datetime,'" + DateTime.Now + "',104), N'" + tbSubiect.Text + "', N'" + tbMesaj.Text + "')";
                    vCmd.CommandText = vCmd.CommandText.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
                    vCmd.ExecuteNonQuery();
                    ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "mesajeAdmin", "adaugare mesaj admin", "", Convert.ToInt64(Session["SESgospodarieId"]), 2);
                }
            }
        }
        else if (ViewState["tip"].ToString() == "m")
        {
            vCmd.CommandText = "update mesaje set dataAdaugarii=convert(datetime,'" + DateTime.Now + "',104), subiect=N'" + tbSubiect.Text + "', mesaj=N'" + tbMesaj.Text + "' where mesajId='" + gvMesaje.SelectedValue + "'";
            vCmd.CommandText = vCmd.CommandText.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
            vCmd.ExecuteNonQuery();
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "mesajeAdmin", "modificare mesaj admin", "", Convert.ToInt64(Session["SESgospodarieId"]), 3);
        }
        pnCautare.Visible = true;
        pnGrid.Visible = true;
        pnButoane.Visible = true;
        pnAdaugaMesaj.Visible = false;
        gvMesaje.DataBind();
        ManipuleazaBD.InchideConexiune(vCon);
    }
    protected void lbSelectatiTot_Click(object sender, EventArgs e)
    {
        if (ViewState["selectat"] == null)
            ViewState["selectat"] = "0";
        for (int a = 0; a < cblUtilizatori.Items.Count; a++)
            if (ViewState["selectat"].ToString() == "0")
                cblUtilizatori.Items[a].Selected = true;
            else if (ViewState["selectat"].ToString() == "1")
                cblUtilizatori.Items[a].Selected = false;
        if (ViewState["selectat"].ToString() == "0")
            ViewState["selectat"] = "1";
        else if (ViewState["selectat"].ToString() == "1")
            ViewState["selectat"] = "0";
    }
}
