﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using Gospodarie;
using System.Data;

public partial class reparaVolumPozitie2016 : System.Web.UI.Page
{

    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion

    public static List<GospodariiPentruReNumerotare> listaGospodariiGenerala
    {
        get;
        set;
    }
    public static List<GospodariiPentruReNumerotare> listaGospodariiGeneralaconflict = new List<GospodariiPentruReNumerotare>();
    public static List<GospodariiPentruReNumerotare> listaGospodariiModal = new List<GospodariiPentruReNumerotare>();
    public static List<GospodariiPentruReNumerotare> listaGospodariiGridGeneral = new List<GospodariiPentruReNumerotare>();

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            //Curata gv modal
            CurataModal();

            //Curata gv principal
            gvRVP.DataSource = null;
            gvRVP.DataBind();

            //Curata liste
            listaGospodariiModal.Clear();
            listaGospodariiGeneralaconflict.Clear();

            //Populeaza lista
            listaGospodariiGenerala = GospodariiServicesPentruReNumerotare.GetGospodariiByUnitate(Convert.ToInt32(Session["SESunitateId"]), Convert.ToInt32(Session["SESan"]));
            verificaFiltre = 1;
            IncarcaListaGridView();
            verificaFiltre = 0;
            //gvRVP.DataSource = listaGospodariiGenerala;
            //gvRVP.DataBind();
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        ////Verifica daca dupa modificarile aduse sunt alte conflicte
        //AnalizeazaToataLista();

        if (IsPostBack)
        {
            if (listaGospodariiModal.Count > 1)
            {
                bt_OKModal.Visible = false;
                bt_ValidezMaiTarziuModal.Visible = true;
                IncarcaListaGridViewModal();
                //     AnalizeazaToataLista();
                IncarcaListaGridView();
            }
        }

    }

    protected void AnalizeazaToataLista()
    {
        // reinitializam lista cu neregularitate zero 
        List<GospodariiPentruReNumerotare> listaconflict = new List<GospodariiPentruReNumerotare>();
        List<GospodariiPentruReNumerotare> listaVolumPozIdentice = (from row in listaGospodariiGenerala.AsEnumerable()
                                                                    let variabilaNeregularitate = row.Neregularitate = 0
                                                                    select row).ToList();
        // verificam toata lista pentru valori egale
        foreach (GospodariiPentruReNumerotare gospodarie in listaGospodariiGenerala)
        {
            listaVolumPozIdentice = (from row in listaGospodariiGenerala.AsEnumerable()
                                     where gospodarie.Volum == row.Volum
                                         && gospodarie.NrPozitie == row.NrPozitie
                                         && gospodarie.GospodarieId != row.GospodarieId
                                     let variabilaNeregularitate = gospodarie.Neregularitate = row.Neregularitate = 1
                                     select row).ToList();
            if (listaVolumPozIdentice.ToList().Count > 1)
            {
                listaVolumPozIdentice.ToList().ForEach(rand => rand.Neregularitate = 1);
                listaconflict.AddRange(listaVolumPozIdentice.ToList());
            }
            else continue;
        }
        IncarcaListaGridView();
    }
    protected void tbChangeVerifica(object sender, EventArgs e)
    {
        List<GospodariiPentruReNumerotare> listaconflict = new List<GospodariiPentruReNumerotare>();
        // cautam randul
        TextBox tb = (TextBox)sender;
        GridViewRow randCareAGeneratEvenimentul = (GridViewRow)tb.Parent.Parent;
        // luam valori vol, nrpoz si gospid din rand
        string volum = ((TextBox)randCareAGeneratEvenimentul.FindControl("tbVolum")).Text;
        string nrPozitie = ((TextBox)randCareAGeneratEvenimentul.FindControl("tbNrPozitie")).Text;
        long gospodarieId = Convert.ToInt64(((Label)randCareAGeneratEvenimentul.FindControl("lblGospodarieId")).Text);

        // vechea gospodarie modificata
        GospodariiPentruReNumerotare gospodarieVeche = new GospodariiPentruReNumerotare();
        gospodarieVeche = (from row in listaGospodariiGridGeneral.AsEnumerable()
                           where gospodarieId == row.GospodarieId
                           select row).ToList()[0];
        // actualizam neregularitate pentru gospodariile in conflict cu gospodaria veche
        List<GospodariiPentruReNumerotare> listaConflictVeche = new List<GospodariiPentruReNumerotare>();
        if (gospodarieVeche.Neregularitate == 1)
        {
            listaConflictVeche = (from row in listaGospodariiGenerala.AsEnumerable()
                                  where gospodarieVeche.Volum == row.Volum && gospodarieVeche.NrPozitie == row.NrPozitie
                                  select row).ToList();
            if (listaConflictVeche.Count == 2)
                foreach (GospodariiPentruReNumerotare gospodarie in listaConflictVeche)
                {
                    listaGospodariiGenerala.Remove(gospodarie);
                    try { listaGospodariiGridGeneral.Remove(gospodarie); } catch { }
                    gospodarie.Neregularitate = 0;
                    listaGospodariiGenerala.Add(gospodarie);
                    try { listaGospodariiGridGeneral.Add(gospodarie); } catch { }
                }
            else
            {
                listaGospodariiGenerala.Remove(gospodarieVeche);
                try { listaGospodariiGridGeneral.Remove(gospodarieVeche); } catch { }
                gospodarieVeche.Neregularitate = 0;
                listaGospodariiGenerala.Add(gospodarieVeche);
                try { listaGospodariiGridGeneral.Add(gospodarieVeche); } catch { }
            }
        }
        // noua gospodarie dupa modificare
        GospodariiPentruReNumerotare gospodarieNoua = new GospodariiPentruReNumerotare();
        gospodarieNoua = (from row in listaGospodariiGridGeneral.AsEnumerable()
                          where gospodarieId == row.GospodarieId
                          let varVolum = row.Volum = volum
                          let varNrPozitie = row.NrPozitie = nrPozitie
                          select row).ToList()[0];

        listaconflict.Add(gospodarieNoua);

        //verificam daca sunt conflicte
        var listaVolumPozIdentice = from row in listaGospodariiGenerala.AsEnumerable()
                                    where volum == row.Volum && nrPozitie == row.NrPozitie
                                    select row;
        listaconflict = listaVolumPozIdentice.ToList();
        if (listaVolumPozIdentice.ToList().Count > 1)
        {
            listaVolumPozIdentice = from row in listaGospodariiGenerala.AsEnumerable()
                                    where volum == row.Volum && nrPozitie == row.NrPozitie
                                    let variabila = row.Neregularitate = 1
                                    select row;
            listaconflict = listaVolumPozIdentice.ToList();
            // reincarcam gv principal
            //IncarcaListaGridView();
            //umplem gridul din modal si il deschidem
            listaGospodariiModal = OrdoneazaLista(listaconflict); ;

            IncarcaListaGridViewModal();

            bt_OKModal.Visible = false;
            bt_ValidezMaiTarziuModal.Visible = true;

            ArataModal();
        }
        else
        {
        }
        listaGospodariiGridGeneral = OrdoneazaLista(listaGospodariiGridGeneral);
        IncarcaListaGridView();

    }
    protected void tbChangeModalVerifica(object sender, EventArgs e)
    {
        List<GospodariiPentruReNumerotare> listaconflict = new List<GospodariiPentruReNumerotare>();
        // cautam randul
        TextBox tb = (TextBox)sender;
        GridViewRow randCareAGeneratEvenimentul = (GridViewRow)tb.Parent.Parent;
        // luam valori vol, nrpoz si gospid din rand
        string volum = ((TextBox)randCareAGeneratEvenimentul.FindControl("tbVolum")).Text;
        string nrPozitie = ((TextBox)randCareAGeneratEvenimentul.FindControl("tbNrPozitie")).Text;
        long gospodarieId = Convert.ToInt64(((Label)randCareAGeneratEvenimentul.FindControl("lblGospodarieId")).Text);

        // vechea gospodarie modificata
        //GospodariiPentruReNumerotare gospodarieVeche = (from row in listaGospodariiModal.AsEnumerable()
        //                                                where gospodarieId == row.GospodarieId
        //                                                select row).ToList()[0];
        // noua gospodarie dupa modificare
        GospodariiPentruReNumerotare gospodarieNoua = (from row in listaGospodariiModal.AsEnumerable()
                                                       where gospodarieId == row.GospodarieId
                                                       let varVolum = row.Volum = volum
                                                       let varNrPozitie = row.NrPozitie = nrPozitie
                                                       select row).ToList()[0];
        // resetam neregularitate pentru listamodal
        listaGospodariiModal = (from row in listaGospodariiModal.AsEnumerable()
                                let variabilaNeregularitate = row.Neregularitate = 0
                                select row).ToList();
        //verificam daca sunt conflicte
        var listaVolumPozIdentice = new List<GospodariiPentruReNumerotare>();
        foreach (GospodariiPentruReNumerotare gospodarie in listaGospodariiModal)
        {
            if (gospodarie.Neregularitate == 1) continue;
            // pentru fiecare  gospodarie din lista din modal verificam daca nu au aparut alte conflicte
            List<GospodariiPentruReNumerotare> listaVolumPozIdenticePentruFiecareGospodarie = (from row in listaGospodariiGenerala.AsEnumerable()
                                                                                               where gospodarie.Volum == row.Volum
                                                                                               && gospodarie.NrPozitie == row.NrPozitie
                                                                                               && gospodarie.Neregularitate == 0
                                                                                               select row).ToList();
            // listaVolumPozIdenticePentruFiecareGospodarie = listaVolumPozIdenticePentruFiecareGospodarie.ToList();
            if (listaVolumPozIdenticePentruFiecareGospodarie.Count > 1)
            {
                // le facem neregularitate = 1 pentru toate cele 
                List<GospodariiPentruReNumerotare> listaVolumPozIdenticePentruFiecareGospodarieDinModal = (from row in listaVolumPozIdenticePentruFiecareGospodarie.AsEnumerable()
                                                                                                               // where volum == row.Volum && nrPozitie == row.NrPozitie
                                                                                                           let variabila = row.Neregularitate = 1
                                                                                                           select row).ToList();
                listaconflict.AddRange(listaVolumPozIdenticePentruFiecareGospodarieDinModal.ToList());
            }
            else
            {
                listaconflict.Add(gospodarie);
            }
        }
        // daca mai sunt conflicte eliminam gospodaria din lista de modal si apoi umplem - adaugam - toata lista noua
        listaGospodariiModal.Clear();
        listaGospodariiModal = OrdoneazaLista(listaconflict);
        listaGospodariiGridGeneral = OrdoneazaLista(listaGospodariiGridGeneral);
        if (listaGospodariiModal.Count > 1)
        {
            //re-umplem gridul din modal
            IncarcaListaGridViewModal();

            bt_OKModal.Visible = false;
            bt_ValidezMaiTarziuModal.Visible = true;
        }
        else
        {

        }
        ArataModal();
    }

    protected void bt_ValidezMaiTarziuModal_Click(object sender, EventArgs e)
    {
        //id din lista modal
        int index = -1;
        //foreach (GospodariiPentruReNumerotare gospodariiP in listaGospodariiGenerala)
        //{
        //    index++;
        //    foreach (GospodariiPentruReNumerotare gospodariiS in listaGospodariiModal)
        //    {
        //        Int64 id = gospodariiS.GospodarieId;
        //        if (id == gospodariiP.GospodarieId)
        //        {
        //            listaGospodariiGenerala[index].Volum = gospodariiS.Volum;
        //            listaGospodariiGenerala[index].NrPozitie = gospodariiS.NrPozitie;
        //        }
        //    }
        //}
        //AnalizeazaToataLista();
        IncarcaListaGridView();

        AscundeModal();
    }

    protected void gvRVP_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (((Label)(e.Row.FindControl("lblNeregularitate"))).Text == "1")

                ((GridViewRow)e.Row).BackColor = System.Drawing.Color.Tomato;
            else
            {
                ((GridViewRow)e.Row).BackColor = System.Drawing.Color.White;
            }
        }
    }
    protected void gvModal_DataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (((Label)(e.Row.FindControl("lblNeregularitate"))).Text == "1")
            {
                ((GridViewRow)e.Row).BackColor = System.Drawing.Color.Tomato;
                //bt_OKModal.Visible = false;
                //bt_ValidezMaiTarziuModal.Visible = true;
                //ArataModal();
            }
            else
            {
                ((GridViewRow)e.Row).BackColor = System.Drawing.Color.LightGreen;
                bt_OKModal.Visible = true;
                bt_ValidezMaiTarziuModal.Visible = false;
                // ArataModal();
            }
        }
    }

    protected int verificaFiltre = 0;
    protected void VerificaFiltre()
    {
        gvRVP.PageSize = Convert.ToInt32(ddNrLinii.SelectedValue);

        string filtruVolum = tbfVolum.Text.ToUpper().Trim();
        string filtruPozitie = tbfNrPoz.Text.Length > 0 ? tbfNrPoz.Text.ToUpper().Trim() : "%";
        string filtruNume = tbfNume.Text.Length > 0 ? tbfNume.Text.ToUpper().Trim() : "%";
        string filtruLoc = tbfLoc.Text.Length > 0 ? tbfLoc.Text.ToUpper().Trim() : "%";
        string filtruStrada = tbfStrada.Text.Length > 0 ? tbfStrada.Text.ToUpper().Trim() : "%";
        string filtruNr = tbfNr.Text.ToUpper().Trim();

        int filtruTip = ddlFTip.SelectedValue != "%" ? Convert.ToInt32(ddlFTip.SelectedValue) : 0;

        listaGospodariiGridGeneral = listaGospodariiGenerala
            .Where(c =>
                        (filtruVolum.Length == 0
                            ? 1 == 1
                            : ddlModSelectVol.SelectedValue == "%X%"
                                ? c.Volum.ToUpper().Trim().Contains(filtruVolum)
                                :
                                    ddlModSelectVol.SelectedValue == "X"
                                    ? c.Volum.ToUpper().Trim() == filtruVolum
                                    :
                                        // incepe cu
                                        ddlModSelectVol.SelectedValue == "X%"
                                        ? c.Volum.ToUpper().Trim().IndexOf(filtruVolum) == 0
                                        :
                                            // se termina cu
                                            ddlModSelectVol.SelectedValue == "%X"
                                            ? c.Volum.ToUpper().Trim().IndexOf(filtruVolum) == c.Volum.ToUpper().Trim().Length - c.Volum.ToUpper().Trim().Length
                                            : 1 == 1
                            )
                        && (filtruPozitie == "%" ? 1 == 1 : c.NrPozitie.ToUpper().Trim().Contains(filtruPozitie))
                        && (filtruNume == "%" ? 1 == 1 : c.MembruNume.ToUpper().Trim().Contains(filtruNume))
                        && (filtruLoc == "%" ? 1 == 1 : c.Localitate.ToUpper().Trim().Contains(filtruLoc))
                        && (filtruStrada == "%" ? 1 == 1 : c.Strada.ToUpper().Trim().Contains(filtruStrada))
                        && (filtruNr.Length == 0
                            ? 1 == 1
                            : ddlModSelectNr.SelectedValue == "%X%"
                                ? c.Nr.ToUpper().Trim().Contains(filtruNr)
                                :
                                    ddlModSelectNr.SelectedValue == "X"
                                    ? c.Nr.ToUpper().Trim() == filtruNr
                                    :
                                        // incepe cu
                                        ddlModSelectNr.SelectedValue == "X%"
                                        ? c.Nr.ToUpper().Trim().IndexOf(filtruNr) == 0
                                        :
                                            // se termina cu
                                            ddlModSelectNr.SelectedValue == "%X"
                                            ? c.Nr.ToUpper().Trim().IndexOf(filtruNr) == c.Nr.ToUpper().Trim().Length - c.Nr.ToUpper().Trim().Length
                                            : 1 == 1




                        )

                        && (filtruTip == 0 ? 1 == 1 : c.IdTip == filtruTip)
                        ).ToList();

        string filtruOrdonareDupa = ddlFOrdonareDupa.SelectedValue;
        if (filtruOrdonareDupa == "0") //volum si nr pozitie
            listaGospodariiGridGeneral = listaGospodariiGridGeneral.OrderBy(c => c.Volum).ThenBy(c => c.NrPozitie).ToList();
        if (filtruOrdonareDupa == "1") //Localitate, strada,nr
            listaGospodariiGridGeneral = listaGospodariiGridGeneral.OrderBy(c => c.Localitate).ThenBy(c => c.Strada).ThenBy(c => c.Nr).ToList();
        if (filtruOrdonareDupa == "2") //Nume
            listaGospodariiGridGeneral = listaGospodariiGridGeneral.OrderBy(c => c.MembruNume).ToList();
        if (filtruOrdonareDupa == "3") //ID crescator
            listaGospodariiGridGeneral = listaGospodariiGridGeneral.OrderBy(o => o.GospodarieId).ToList();
        if (filtruOrdonareDupa == "4") //ID descrescator
            listaGospodariiGridGeneral = listaGospodariiGridGeneral.OrderByDescending(o => o.GospodarieId).ToList();

        //if (filtruTip != "%")
        //    listaGospodariiGenerala = listaGospodariiGenerala.Where(c => c.Tip == filtruTip).ToList();
    }

    protected List<GospodariiPentruReNumerotare> OrdoneazaLista(List<GospodariiPentruReNumerotare> lista)
    {
        string filtruOrdonareDupa = ddlFOrdonareDupa.SelectedValue;
        switch (filtruOrdonareDupa)
        {
            case "0":
                return lista.OrderBy(c => c.Volum.PadLeft(5, '0')).ThenBy(c => c.NrPozitie.PadLeft(5, '0')).ToList();
            case "1":
                return lista.OrderBy(c => c.Localitate).ThenBy(c => c.Strada).ThenBy(c => c.Nr).ToList();
            case "2":
                return lista.OrderBy(c => c.MembruNume).ToList();
            case "3":
                return lista.OrderBy(o => o.GospodarieId).ToList();
            case "4":
                return lista.OrderByDescending(o => o.GospodarieId).ToList();
        }
        return lista;
    }

    protected void ReIncarcaGrid(object sender, EventArgs e)
    {
        // sa verifice filtrele
        verificaFiltre = 1;
        IncarcaListaGridView();
        // nu mai incarca la un alt eventual eveniment
        verificaFiltre = 0;
    }

    protected void IncarcaListaGridView()
    {
        if (verificaFiltre == 1) VerificaFiltre();
        gvRVP.DataSource = listaGospodariiGridGeneral;
        gvRVP.DataBind();

        foreach (GridViewRow row in gvRVP.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                if (((Label)(row.FindControl("lblNeregularitate"))).Text == "1")
                    row.BackColor = System.Drawing.Color.Tomato;
            }
        }
        //Verifica daca dupa modificarile aduse sunt alte conflicte
        //  if (verificaFiltre == 0) AnalizeazaToataLista();
    }
    protected void IncarcaListaGridViewModal()
    {
        gvModal.DataSource = listaGospodariiModal;
        gvModal.DataBind();
    }
    protected void gvGospodariiPaginare(object sender, GridViewPageEventArgs e)
    {
        gvRVP.PageIndex = e.NewPageIndex;
        gvRVP.DataSource = listaGospodariiGridGeneral;
        gvRVP.DataBind();
    }
    protected void bt_ModalOK(object sender, EventArgs e)
    {
        //id din lista modal
        //int index = -1;
        //foreach (GospodariiPentruReNumerotare gospodariiP in listaGospodariiGenerala)
        //{
        //    index++;
        //    foreach (GospodariiPentruReNumerotare gospodariiS in listaGospodariiModal)
        //    {
        //        Int64 id = gospodariiS.GospodarieId;
        //        if (id == gospodariiP.GospodarieId)
        //        {
        //            listaGospodariiGenerala[index].Volum = gospodariiS.Volum;
        //            listaGospodariiGenerala[index].NrPozitie = gospodariiS.NrPozitie;
        //        }
        //    }
        //}
        listaGospodariiModal.Clear();
        IncarcaListaGridView();
        //

        //adauaga la id din lista principala volum si pozitie din lista luata din modal
        CurataModal();
        AscundeModal();


        //Verifica daca dupa modificarile aduse sunt alte conflicte


        //foreach (GospodariiPentruReNumerotare gospodarie in listaGospodariiGenerala)
        //{
        //    List<GospodariiPentruReNumerotare> listaconflict = new List<GospodariiPentruReNumerotare>();
        //    var listaVolumPozIdentice = from row in listaGospodariiGenerala.AsEnumerable()
        //                                where gospodarie.Volum == row.Volum && gospodarie.NrPozitie == row.NrPozitie
        //                                select row;
        //    listaconflict = listaVolumPozIdentice.ToList();

        //    //Daca mai am inca gospodarii in conflict
        //    int contor = 0;
        //    if (listaconflict.Count > 1)
        //    {
        //        int indexL = -1;
        //        foreach (GospodariiPentruReNumerotare gospodariiIntermed in listaGospodariiGenerala)
        //        {
        //            indexL++;
        //            Int64 id = gospodariiIntermed.GospodarieId;
        //            foreach (GospodariiPentruReNumerotare listaGospodariiGeneralaConflict in listaconflict)
        //            {
        //                if (listaGospodariiGeneralaConflict.GospodarieId == id)
        //                {
        //                    contor++;
        //                    listaGospodariiGenerala[indexL].Neregularitate = 1;
        //                }
        //            }
        //            if (contor >= listaconflict.Count)
        //            {
        //                IncarcaListaGridView();
        //                break;
        //            }
        //        }
        //    }
        //}

    }
    protected void bt_ShowModal_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModal();", true);
    }
    protected void bt_SalveazaModificari_Click(object sender, EventArgs e)
    {
        // verificam data sunt gospodarii cu neregularitate = 1
        listaGospodariiModal.Clear();
        listaGospodariiModal = (from row in listaGospodariiGenerala.AsEnumerable()
                                where row.Neregularitate == 1
                                select row).ToList();
        ;
        if (listaGospodariiModal.Count > 1)
        {
            IncarcaListaGridViewModal();
            ArataModal();
            return;
        }

        // umplem lista cu gospodariile modificate
        List<GospodariiPentruReNumerotare> listaGospodariiModificate = (from row in listaGospodariiGenerala.AsEnumerable()
                                                                        where row.Volum != row.VolumLaIncarcareGrid ||
                                                                              row.NrPozitie != row.NrPozitieLaIncarcareGrid
                                                                        select row).ToList();


        // facem update doar pentru cele modificate
        GospodariiServicesPentruReNumerotare.SetUpdateList(listaGospodariiModificate);
        return;
    }
    protected void bt_RenuntaModificari_Click(object sender, EventArgs e)
    {
        //renunta modificari
        CurataModal();
        Response.Redirect("/Gospodarii.aspx"); //Redirectare catre pagina de gospodarii
    }
    public void ArataModal()
    {
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Pop", "openModal();", true);
    }
    public void AscundeModal()
    {
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "hideModal();", true);
    }
    protected void CurataModal()
    {
        DataTable ds = new DataTable();
        ds = null;
        gvModal.DataSource = ds;
        gvModal.DataBind();
    }
    protected void TerminaLucruCuModal(object sender, EventArgs e)
    {
        CurataModal();
        AscundeModal();
    }

    protected string ScoateNumere(string pScoateNumar)
    {
        switch (pScoateNumar)
        {
            default:
                break;
            case "I":
                pScoateNumar = "1";
                break;
            case "II":
                pScoateNumar = "2";
                break;
            case "III":
                pScoateNumar = "3";
                break;
            case "IV":
                pScoateNumar = "4";
                break;
            case "V":
                pScoateNumar = "5";
                break;
            case "VI":
                pScoateNumar = "6";
                break;
            case "VII":
                pScoateNumar = "7";
                break;
            case "VIII":
                pScoateNumar = "8";
                break;
            case "IX":
                pScoateNumar = "9";
                break;
            case "X":
                pScoateNumar = "10";
                break;
            case "XI":
                pScoateNumar = "11";
                break;
            case "XII":
                pScoateNumar = "12";
                break;
            case "XIII":
                pScoateNumar = "13";
                break;
            case "XIV":
                pScoateNumar = "14";
                break;
            case "XV":
                pScoateNumar = "15";
                break;
            case "XVI":
                pScoateNumar = "16";
                break;
            case "XVII":
                pScoateNumar = "17";
                break;
            case "XVIII":
                pScoateNumar = "18";
                break;
            case "XIX":
                pScoateNumar = "19";
                break;
            case "XX":
                pScoateNumar = "20";
                break;
            case "XXI":
                pScoateNumar = "21";
                break;
            case "XXII":
                pScoateNumar = "22";
                break;
            case "XXIII":
                pScoateNumar = "23";
                break;
            case "XXIV":
                pScoateNumar = "24";
                break;
            case "XXV":
                pScoateNumar = "25";
                break;
            case "XXVI":
                pScoateNumar = "26";
                break;
            case "XXVII":
                pScoateNumar = "27";
                break;
            case "XXVIII":
                pScoateNumar = "28";
                break;
            case "XXIX":
                pScoateNumar = "29";
                break;
            case "XXX":
                pScoateNumar = "30";
                break;
            case "XXXI":
                pScoateNumar = "31";
                break;
            case "XXXII":
                pScoateNumar = "32";
                break;
            case "XXXIII":
                pScoateNumar = "33";
                break;
            case "XXXIV":
                pScoateNumar = "34";
                break;
            case "XXXV":
                pScoateNumar = "35";
                break;
            case "XXXVI":
                pScoateNumar = "36";
                break;
            case "XXXVII":
                pScoateNumar = "37";
                break;
            case "XXXVIII":
                pScoateNumar = "38";
                break;
            case "XXXIX":
                pScoateNumar = "39";
                break;
            case "XL":
                pScoateNumar = "40";
                break;
            case "XLI":
                pScoateNumar = "41";
                break;
            case "XLII":
                pScoateNumar = "42";
                break;
            case "XLIII":
                pScoateNumar = "43";
                break;
            case "XLIV":
                pScoateNumar = "44";
                break;
            case "XLV":
                pScoateNumar = "45";
                break;
            case "XLVI":
                pScoateNumar = "46";
                break;
            case "XLVII":
                pScoateNumar = "47";
                break;
            case "XLVIII":
                pScoateNumar = "48";
                break;
            case "XLIX":
                pScoateNumar = "49";
                break;
            case "L":
                pScoateNumar = "50";
                break;
            case "LI":
                pScoateNumar = "51";
                break;
            case "LII":
                pScoateNumar = "52";
                break;
            case "LIII":
                pScoateNumar = "53";
                break;
            case "LIV":
                pScoateNumar = "54";
                break;
            case "LV":
                pScoateNumar = "55";
                break;
            case "LVI":
                pScoateNumar = "56";
                break;
            case "LVII":
                pScoateNumar = "57";
                break;
            case "LVIII":
                pScoateNumar = "58";
                break;
            case "LIX":
                pScoateNumar = "59";
                break;
            case "LX":
                pScoateNumar = "60";
                break;
            case "LXI":
                pScoateNumar = "61";
                break;
            case "LXII":
                pScoateNumar = "62";
                break;
            case "LXIII":
                pScoateNumar = "63";
                break;
            case "LXIV":
                pScoateNumar = "64";
                break;
            case "LXV":
                pScoateNumar = "65";
                break;
            case "LXVI":
                pScoateNumar = "66";
                break;
            case "LXVII":
                pScoateNumar = "67";
                break;
            case "LXVIII":
                pScoateNumar = "68";
                break;
            case "LXIX":
                pScoateNumar = "69";
                break;
            case "LXX":
                pScoateNumar = "70";
                break;
            case "LXXI":
                pScoateNumar = "71";
                break;
            case "LXXII":
                pScoateNumar = "72";
                break;
            case "LXXIII":
                pScoateNumar = "73";
                break;
            case "LXXIV":
                pScoateNumar = "74";
                break;
            case "LXXV":
                pScoateNumar = "75";
                break;
            case "LXXVI":
                pScoateNumar = "76";
                break;
            case "LXXVII":
                pScoateNumar = "77";
                break;
            case "LXXVIII":
                pScoateNumar = "78";
                break;
            case "LXXIX":
                pScoateNumar = "79";
                break;
            case "LXXX":
                pScoateNumar = "80";
                break;
            case "LXXXI":
                pScoateNumar = "81";
                break;
            case "LXXXII":
                pScoateNumar = "82";
                break;
            case "LXXXIII":
                pScoateNumar = "83";
                break;
            case "LXXXIV":
                pScoateNumar = "84";
                break;
            case "LXXXV":
                pScoateNumar = "85";
                break;
            case "LXXXVI":
                pScoateNumar = "86";
                break;
            case "LXXXVII":
                pScoateNumar = "87";
                break;
            case "LXXXVIII":
                pScoateNumar = "88";
                break;
            case "LXXXIX":
                pScoateNumar = "89";
                break;
            case "XC":
                pScoateNumar = "90";
                break;
            case "XCI":
                pScoateNumar = "91";
                break;
            case "XCII":
                pScoateNumar = "92";
                break;
            case "XCIII":
                pScoateNumar = "93";
                break;
            case "XCIV":
                pScoateNumar = "94";
                break;
            case "XCV":
                pScoateNumar = "95";
                break;
            case "XCVI":
                pScoateNumar = "96";
                break;
            case "XCVII":
                pScoateNumar = "97";
                break;
            case "XCVIII":
                pScoateNumar = "98";
                break;
            case "XCIX":
                pScoateNumar = "99";
                break;
            case "C":
                pScoateNumar = "100";
                break;
        }
        int vContor = 0;
        int vLungime = pScoateNumar.Length;
        string vNumar = "";
        while (vContor < vLungime)
        {
            string vCaracter = pScoateNumar[vContor].ToString();
            try
            {
                vNumar += Convert.ToInt16(vCaracter).ToString();
            }
            catch { }
            vContor++;
        }
        try
        {
            vNumar = Convert.ToInt16(vNumar).ToString();
        }
        catch { vNumar = "0"; }
        return vNumar; ;
    }

    //public int index = 0;
    //protected string volumintrodus = "";
    //protected string VolumIntrodus
    //{
    //    get { return volumintrodus; }
    //    set { volumintrodus = value; }
    //}
    //protected string pozitieintrodusa = "";
    //protected string PozitieIntrodusa
    //{
    //    get { return pozitieintrodusa; }
    //    set { pozitieintrodusa = value; }
    //}

    //protected static bool ValidezMaiTarziuGlobal = false;
    //protected void AnalizeazaListaGeneralaPentruListaDinModal(List<GospodariiPentruReNumerotare> listaGenerala, List<GospodariiPentruReNumerotare> listaModal)
    //{
    //    // reinitializam lista cu neregularitate zero 
    //    List<GospodariiPentruReNumerotare> listaconflict = new List<GospodariiPentruReNumerotare>();
    //    List<GospodariiPentruReNumerotare> listaVolumPozIdentice = (from row in lista.AsEnumerable()
    //                                              let variabilaNeregularitate = row.Neregularitate = 0
    //                                              select row).ToList();
    //    // verificam toata lista pentru valori egale
    //    foreach (GospodariiPentruReNumerotare gospodarie in lista)
    //    {
    //        listaVolumPozIdentice = (from row in lista.AsEnumerable()
    //                                 where gospodarie.Volum == row.Volum
    //                                     && gospodarie.NrPozitie == row.NrPozitie
    //                                     && gospodarie.GospodarieId != row.GospodarieId
    //                                 let variabilaNeregularitate = gospodarie.Neregularitate = row.Neregularitate = 1
    //                                 select row).ToList();
    //        if (listaVolumPozIdentice.ToList().Count > 1)
    //        {
    //            listaVolumPozIdentice.ToList().ForEach(rand => rand.Neregularitate = 1);
    //            listaconflict.AddRange(listaVolumPozIdentice.ToList());
    //        }
    //        else continue;
    //    }
    //    IncarcaListaGridView(lista);
    //}
    //protected void tbVolum_TextChanged(object sender, EventArgs e)
    //{
    //    List<GospodariiPentruReNumerotare> listaconflict = new List<GospodariiPentruReNumerotare>();
    //    //TextBox Volum
    //    TextBox tb = (TextBox)sender;
    //    GridViewRow gvr = (GridViewRow)tb.Parent.Parent;
    //    //Index-ul randului din gridview
    //    int rowindex = gvr.RowIndex;
    //    testvar = rowindex;
    //    //TextBox NrPozitie aferent volumului modificat
    //    TextBox pozitie = (TextBox)gvRVP.Rows[rowindex].Cells[2].FindControl("tbNrPozitie");
    //    //Adauga la lista principala volumul modificat
    //    listaGospodariiGenerala[rowindex].Volum = tb.Text;
    //    //Adauga la lista principala nr pozitie modificata
    //    listaGospodariiGenerala[rowindex].NrPozitie = pozitie.Text;

    //    //Lista cu gospodariile identice
    //    var listaVolumPozIdentice = from row in listaGospodariiGenerala.AsEnumerable()
    //                                where tb.Text == row.Volum && pozitie.Text == row.NrPozitie
    //                                select row;
    //    listaconflict = listaVolumPozIdentice.ToList();
    //    if (listaconflict.Count > 1)
    //        foreach (GospodariiPentruReNumerotare gospodarii in listaconflict)
    //        {
    //            listaGospodariiModal.Add(gospodarii);
    //        }
    //    else
    //    {
    //        GospodariiServicesPentruReNumerotare.SetUpdateList(listaconflict);
    //        //   Response.Redirect("/reparaVolumPozitie2016.aspx");
    //    }
    //    //Daca avem gospodarii cu volum si pozitie identice
    //    int contor = 0;
    //    if (listaGospodariiModal.Count > 1)
    //    {

    //        int index = -1;
    //        foreach (GospodariiPentruReNumerotare gospodariInitial in listaGospodariiGenerala)
    //        {
    //            index++;
    //            foreach (GospodariiPentruReNumerotare gospodari in listaGospodariiModal)
    //            {
    //                Int64 id = gospodari.GospodarieId;
    //                //Cand avem match de id in ambele liste
    //                if (gospodariInitial.GospodarieId == id)
    //                {
    //                    contor++;
    //                    //listaGospodariiGenerala[index].Volum = gospodari.Volum;
    //                    listaGospodariiGenerala[index].Neregularitate = 1;
    //                    gvRVP.Rows[rowindex].BackColor = System.Drawing.Color.Tomato;

    //                }
    //                else
    //                {
    //                    // listaGospodariiGenerala[index].Neregularitate = 0;
    //                    // gvRVP.Rows[rowindex].BackColor = System.Drawing.Color.White;
    //                }
    //            }
    //            if (contor >= listaGospodariiModal.Count)
    //            {
    //                IncarcaListaGridView();
    //                bt_OKModal.Visible = false;
    //                bt_ValidezMaiTarziuModal.Visible = true;

    //                ArataModal();
    //                break;
    //            }
    //        }
    //    }
    //}
    //protected void tbNrPozitie_TextChanged(object sender, EventArgs e)
    //{
    //    List<GospodariiPentruReNumerotare> listaconflict = new List<GospodariiPentruReNumerotare>();
    //    //TextBox Volum
    //    TextBox tb = (TextBox)sender;
    //    GridViewRow gvr = (GridViewRow)tb.Parent.Parent;
    //    //Index-ul randului din gridview
    //    int rowindex = gvr.RowIndex;
    //    //TextBox NrPozitie aferent volumului modificat
    //    TextBox volum = (TextBox)gvRVP.Rows[rowindex].Cells[2].FindControl("tbVolum");
    //    //Adauga la lista principala volumul modificat
    //    listaGospodariiGenerala[rowindex].Volum = volum.Text;
    //    //Adauga la lista principala nr pozitie modificata
    //    listaGospodariiGenerala[rowindex].NrPozitie = tb.Text;

    //    //Lista cu gospodariile identice
    //    var listaVolumPozIdentice = from row in listaGospodariiGenerala.AsEnumerable()
    //                                where volum.Text == row.Volum && tb.Text == row.NrPozitie
    //                                select row;
    //    listaconflict = listaVolumPozIdentice.ToList();
    //    if (listaconflict.Count > 1)
    //        foreach (GospodariiPentruReNumerotare gospodarii in listaconflict)
    //        {
    //            listaGospodariiModal.Add(gospodarii);
    //        }
    //    else
    //    {
    //        GospodariiServicesPentruReNumerotare.SetUpdateList(listaconflict);
    //        // Response.Redirect("/reparaVolumPozitie2016.aspx");
    //    }
    //    //Daca avem gospodarii cu volum si pozitie identice
    //    int contor = 0;
    //    if (listaGospodariiModal.Count > 1)
    //    {

    //        int index = -1;
    //        foreach (GospodariiPentruReNumerotare gospodariInitial in listaGospodariiGenerala)
    //        {
    //            index++;
    //            foreach (GospodariiPentruReNumerotare gospodari in listaGospodariiModal)
    //            {
    //                Int64 id = gospodari.GospodarieId;
    //                //Cand avem match de id in ambele liste
    //                if (gospodariInitial.GospodarieId == id)
    //                {
    //                    contor++;
    //                    //listaGospodariiGenerala[index].Volum = gospodari.Volum;
    //                    listaGospodariiGenerala[index].Neregularitate = 1;
    //                    gvRVP.Rows[rowindex].BackColor = System.Drawing.Color.Tomato;

    //                }
    //                else
    //                {
    //                    // listaGospodariiGenerala[index].Neregularitate = 0;
    //                    // gvRVP.Rows[rowindex].BackColor = System.Drawing.Color.White;
    //                }
    //            }
    //            if (contor >= listaGospodariiModal.Count)
    //            {
    //                IncarcaListaGridView();
    //                bt_OKModal.Visible = false;
    //                bt_ValidezMaiTarziuModal.Visible = true;
    //                ArataModal();
    //                break;
    //            }
    //        }
    //    }
    //}
    //protected void AdaugaLaListaRandulCeVaFiModificat(int index)
    //{
    //    Label gospodarieID = (Label)gvRVP.Rows[index].Cells[0].FindControl("lblGospodarieId"); ;
    //    GospodariiPentruReNumerotare gospodariiTemp = new GospodariiPentruReNumerotare();
    //    gospodariiTemp = GospodariiServicesPentruReNumerotare.GetGospodariiById(Convert.ToInt32(gospodarieID.Text));
    //    listaGospodariiGeneralaconflict.Add(gospodariiTemp);
    //}

    //public static string NumarRand(string stringParse)
    //{
    //    int index = stringParse.IndexOf("$gvRVP$ctl");
    //    index = index + 10;
    //    if (index > 10)
    //    {
    //        string numarRand = stringParse.Substring(index);
    //        if (numarRand.Contains("$tbVolum"))
    //            return numarRand.Replace("$tbVolum", "");
    //        else
    //        {
    //            return "";
    //        }
    //    }
    //    return "";
    //}


    //public int testvar = 0;


    //public List<string> listaValori;
    //protected void tbVolum_TextChanged(object sender, EventArgs e)
    //{
    //    Label vLblGospodarieId = (Label)((TextBox)sender).Parent.FindControl("lblGospodarieId");
    //    TextBox vTbVolum = (TextBox)sender;
    //    TextBox vTbPozitie = (TextBox)((TextBox)sender).Parent.FindControl("tbPozitie");
    //    SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
    //    SqlCommand vCmd = new SqlCommand();
    //    vCmd.Connection = vCon;
    //    // verific daca exista o gospodarie la volumul si numarul introdus


    //    //



    //}


    //protected void gvGospodariiReparaVolumPozitietbVolum_TextChanged1(object sender, EventArgs e)
    //{

    //}
    //protected void ddlFOrdonareDupa_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    HttpCookie vCook = Request.Cookies["COOKTNT"];

    //    if (vCook != null)
    //    {
    //        Session["SESordonareGospodarii"] = ddlFOrdonareDupa.SelectedValue;
    //        vCook = CriptareCookie.DecodeCookie(vCook);
    //        vCook["COOKordonareGospodarii"] = ddlFOrdonareDupa.SelectedValue;
    //        vCook = CriptareCookie.EncodeCookie(vCook);
    //        Response.Cookies.Add(vCook);
    //    }

    //    //if (ddlFOrdonareDupa.SelectedValue == "0")
    //    //    gvGospodarii.DataSourceID = "SqlGospodarii";
    //    //else if (ddlFOrdonareDupa.SelectedValue == "1")
    //    //    gvGospodarii.DataSourceID = "SqlGospodariiLocStrNr";
    //    //else if (ddlFOrdonareDupa.SelectedValue == "2")
    //    //    gvGospodarii.DataSourceID = "SqlGospodariiNume";
    //    //else if (ddlFOrdonareDupa.SelectedValue == "3")
    //    //    gvGospodarii.DataSourceID = "SqlGospodariiIdCresc";
    //    //else if (ddlFOrdonareDupa.SelectedValue == "4")
    //    //    gvGospodarii.DataSourceID = "SqlGospodariiIdDesc";

    //    // gvGospodarii.DataBind();
    //}

    //protected void ddlFOrdonareDupa_Init(object sender, EventArgs e)
    //{
    //    // verific daca au picat sesiunile
    //    VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
    //    vVerificaSesiuni.VerificaSesiuniCookie();
    //    ddlFOrdonareDupa.SelectedValue = Session["SESordonareGospodarii"].ToString();
    //}

    //protected void tbVolum_TextChanged(object sender, EventArgs e)
    //{
    //    Label vLblGospodarieId = (Label)((TextBox)sender).Parent.FindControl("lblGospodarieId");
    //    TextBox vTbVolum = (TextBox)sender;
    //    TextBox vTbPozitie = (TextBox)((TextBox)sender).Parent.FindControl("tbPozitie");
    //    SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
    //    SqlCommand vCmd = new SqlCommand();
    //    vCmd.Connection = vCon;
    //    // verific daca exista o gospodarie la volumul si numarul introdus
    //    if (clsUnitati.ValideazaVolumSiPozitie(Convert.ToInt32(Session["SESunitateId"]), Convert.ToInt16(Session["SESan"])))
    //    {
    //        vCmd.CommandText = "select count(*) from gospodarii where unitateId='" + Session["SESunitateId"].ToString() + "' and volum='" + vTbVolum.Text + "' and nrPozitie='" + vTbPozitie.Text + "' AND an='" + Session["SESan"].ToString() + "'";
    //        int vValidareVolum = Convert.ToInt32(vCmd.ExecuteScalar());
    //        if (vValidareVolum > 0)
    //        {
    //            lblEroare1.Visible = true; lblEroare1.Text = "La volumul " + vTbVolum.Text + ", poziţia " + vTbPozitie.Text + " există deja o gospodărie inregistrată";
    //            // gvGospodarii.DataBind();
    //            return;
    //        }
    //        else
    //        {
    //            lblEroare1.Visible = false;
    //            string pozitieVeche = clsUnitati.GetPozitieVeche(Convert.ToInt64(vLblGospodarieId.Text), Convert.ToInt16(Session["SESan"]));
    //            string volumVechi = clsUnitati.GetVolumVechi(Convert.ToInt64(vLblGospodarieId.Text), Convert.ToInt16(Session["SESan"]));
    //            vCmd.CommandText = "update gospodarii set volum='" + vTbVolum.Text + "', volumInt ='" + ScoateNumere(vTbVolum.Text) + "',volumVechi = '" + volumVechi + "',pozitieVeche = '" + pozitieVeche + "' where gospodarieId='" + vLblGospodarieId.Text + "'";
    //            vCmd.ExecuteNonQuery();
    //            ManipuleazaBD.InchideConexiune(vCon);
    //            // gvGospodarii.DataBind();
    //        }
    //    }
    //    else
    //    {
    //        string pozitieVeche = clsUnitati.GetPozitieVeche(Convert.ToInt64(vLblGospodarieId.Text), Convert.ToInt16(Session["SESan"]));
    //        string volumVechi = clsUnitati.GetVolumVechi(Convert.ToInt64(vLblGospodarieId.Text), Convert.ToInt16(Session["SESan"]));
    //        vCmd.CommandText = "update gospodarii set volum='" + vTbVolum.Text + "', volumInt ='" + ScoateNumere(vTbVolum.Text) + "',volumVechi = '" + volumVechi + "',pozitieVeche = '" + pozitieVeche + "' where gospodarieId='" + vLblGospodarieId.Text + "'";
    //        vCmd.ExecuteNonQuery();
    //        ManipuleazaBD.InchideConexiune(vCon);
    //        //gvGospodarii.DataBind();
    //    }
    //}
    //protected void tbPozitie_TextChanged(object sender, EventArgs e)
    //{
    //    Label vLblGospodarieId = (Label)((TextBox)sender).Parent.FindControl("lblGospodarieId");
    //    TextBox vTbPozitie = (TextBox)sender;
    //    TextBox vTbVolum = (TextBox)((TextBox)sender).Parent.FindControl("tbVolum");
    //    SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
    //    SqlCommand vCmd = new SqlCommand();
    //    vCmd.Connection = vCon;
    //    // verific daca exista o gospodarie la volumul si numarul introdus
    //    if (clsUnitati.ValideazaVolumSiPozitie(Convert.ToInt32(Session["SESunitateId"]), Convert.ToInt16(Session["SESan"])))
    //    {
    //        vCmd.CommandText = "select count(*) from gospodarii where unitateId='" + Session["SESunitateId"].ToString() + "' and volum='" + vTbVolum.Text + "' and nrPozitie='" + vTbPozitie.Text + "' AND an='" + Session["SESan"].ToString() + "'";
    //        int vValidareVolum = Convert.ToInt32(vCmd.ExecuteScalar());
    //        if (vValidareVolum > 0)
    //        {
    //            lblEroare1.Visible = true; lblEroare1.Text = "La volumul " + vTbVolum.Text + ", poziţia " + vTbPozitie.Text + " există deja o gospodărie inregistrată"; //gvGospodarii.DataBind();
    //            return;
    //        }
    //        else
    //        {
    //            lblEroare1.Visible = false;
    //            string pozitieVeche = clsUnitati.GetPozitieVeche(Convert.ToInt64(vLblGospodarieId.Text), Convert.ToInt16(Session["SESan"]));
    //            string volumVechi = clsUnitati.GetVolumVechi(Convert.ToInt64(vLblGospodarieId.Text), Convert.ToInt16(Session["SESan"]));
    //            vCmd.CommandText = "update gospodarii set nrPozitie='" + vTbPozitie.Text + "', nrPozitieInt = '" + ScoateNumere(vTbPozitie.Text) + "',pozitieVeche='" + pozitieVeche + "',volumVechi = '" + volumVechi + "' where  gospodarieId='" + vLblGospodarieId.Text + "'";
    //            vCmd.ExecuteNonQuery();
    //            ManipuleazaBD.InchideConexiune(vCon);
    //            //   gvGospodarii.DataBind();
    //        }
    //    }
    //    else
    //    {
    //        string pozitieVeche = clsUnitati.GetPozitieVeche(Convert.ToInt64(vLblGospodarieId.Text), Convert.ToInt16(Session["SESan"]));
    //        string volumVechi = clsUnitati.GetVolumVechi(Convert.ToInt64(vLblGospodarieId.Text), Convert.ToInt16(Session["SESan"]));
    //        vCmd.CommandText = "update gospodarii set nrPozitie='" + vTbPozitie.Text + "', nrPozitieInt = '" + ScoateNumere(vTbPozitie.Text) + "',pozitieVeche='" + pozitieVeche + "',volumVechi = '" + volumVechi + "' where  gospodarieId='" + vLblGospodarieId.Text + "'";
    //        vCmd.ExecuteNonQuery();
    //        ManipuleazaBD.InchideConexiune(vCon);
    //        // gvGospodarii.DataBind();
    //    }
    //}
    //protected void tbfVolum_TextChanged(object sender, EventArgs e)
    //{
    //    listaValori = new List<string>(new string[] { tbfVolum.Text, "", "", "", "", "", "%", "0", "10" });
    //    //  VerificaFiltre(listaGospodariiGenerala, listaValori);
    //}

    //protected void tbfNrPoz_TextChanged(object sender, EventArgs e)
    //{
    //    listaValori = new List<string>(new string[] { "", tbfNrPoz.Text, "", "", "", "", "%", "0", "10" });
    //    //  VerificaFiltre(listaGospodariiGenerala, listaValori);
    //}

    //protected void tbfNume_TextChanged(object sender, EventArgs e)
    //{
    //    listaValori = new List<string>(new string[] { "", "", tbfNume.Text, "", "", "", "%", "0", "10" });
    //    //  VerificaFiltre(listaGospodariiGenerala, listaValori);
    //}

    //protected void tbfLoc_TextChanged(object sender, EventArgs e)
    //{
    //    listaValori = new List<string>(new string[] { "", "", "", tbfLoc.Text, "", "", "%", "0", "10" });
    //    //  VerificaFiltre(listaGospodariiGenerala, listaValori);
    //}

    //protected void tbfStrada_TextChanged(object sender, EventArgs e)
    //{
    //    listaValori = new List<string>(new string[] { "", "", "", "", tbfStrada.Text, "", "%", "0", "10" });
    //    //  VerificaFiltre(listaGospodariiGenerala, listaValori);
    //}

    //protected void tbfNr_TextChanged(object sender, EventArgs e)
    //{
    //    listaValori = new List<string>(new string[] { "", "", "", "", "", tbfNr.Text, "%", "0", "10" });
    //    //  VerificaFiltre(listaGospodariiGenerala, listaValori);
    //}

    //protected void ddlFTip_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    listaValori = new List<string>(new string[] { "", "", "", "", "", "", ddlFTip.Text, "0", "10" });
    //    // VerificaFiltre(listaGospodariiGenerala, listaValori);
    //}

    //protected void ddNrLinii_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    listaValori = new List<string>(new string[] { "", "", "", "", "", "", "%", "0", ddNrLinii.Text });
    //    ///  VerificaFiltre(listaGospodariiGenerala, listaValori);
    //}


    //protected void ddlUnitate_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    HttpCookie vCook = Request.Cookies["COOKTNT"];
    //    string v1 = "";
    //    string v2 = "";
    //    string v3 = "";

    //    if (vCook != null)
    //    {
    //        vCook = CriptareCookie.DecodeCookie(vCook);
    //        v1 = vCook["COOKan"];
    //        v2 = vCook["COOKutilizatorId"];
    //        v3 = vCook["COOKunitateId"];
    //        vCook = CriptareCookie.EncodeCookie(vCook);
    //        Response.Cookies.Add(vCook);
    //        Response.Cookies.Remove("COOKTNT");
    //    }
    //    HttpCookie vCook1 = new HttpCookie("COOKTNT");
    //    vCook1["COOKan"] = v1;
    //    vCook1["COOKutilizatorId"] = v2;
    //    vCook1["COOKunitateId"] = v3;
    //    vCook1 = CriptareCookie.EncodeCookie(vCook1);
    //    Response.Cookies.Add(vCook1);

    //    Session["SESunitateId"] = ddlUnitate.SelectedValue;
    //    //   Session["SESgospodarieId"] = "NULA";
    //    Session["SESgospodarieId"] = null;

    //    ((MasterPage)this.Page.Master).SchimbaGospodaria();
    //    // gvGospodarii.SelectedIndex = -1;
    //}
    //protected List<GospodariiPentruReNumerotare> VerificaAutenticitateRand(string volum, string pozitie)
    //{
    //    listaGospodariiGeneralaconflict.Clear();
    //    List<GospodariiPentruReNumerotare> listaGospodariiGeneralaTemp = new List<GospodariiPentruReNumerotare>();
    //    listaGospodariiGeneralaTemp = listaGospodariiGenerala;

    //    // lista va contine gospodariile cu volum si pozitie identice
    //    var listaVolumPozIdentice = from row in listaGospodariiGeneralaTemp.AsEnumerable()
    //                                where volum == row.Volum && pozitie == row.NrPozitie
    //                                select row;
    //    listaGospodariiGeneralaTemp = listaVolumPozIdentice.ToList();

    //    foreach (GospodariiPentruReNumerotare gospodarieInConflict in listaGospodariiGeneralaTemp)
    //    {
    //        listaGospodariiGeneralaconflict.Add(gospodarieInConflict);
    //    }

    //    if (listaGospodariiGeneralaconflict.Count != 0)
    //    {
    //        AdaugaLaListaRandulCeVaFiModificat(index);
    //        IncarcaListaGridViewModal(listaGospodariiGeneralaconflict);
    //        return listaGospodariiGeneralaconflict;
    //    }
    //    else
    //    {
    //        return listaGospodariiGeneralaconflict;
    //    }
    //}

    //protected bool VerificaAutenticitateListaModal(List<GospodariiPentruReNumerotare> listaGospodariiGeneralaconflict, string volum, string pozitie)
    //{
    //    var listaVolumPozIdentice = from row in listaGospodariiGeneralaconflict.AsEnumerable()
    //                                where volum == row.Volum && pozitie == row.NrPozitie
    //                                select row;
    //    listaGospodariiModal = listaVolumPozIdentice.ToList();
    //    if (listaGospodariiModal.Count == 0)
    //    {
    //        return true;
    //    }
    //    else
    //    {
    //        return false;
    //    }
    //}
    //protected void tbVolumModal_TextChanged(object sender, EventArgs e)
    //{
    //    List<GospodariiPentruReNumerotare> listaconflict = new List<GospodariiPentruReNumerotare>();
    //    //TextBox Volum
    //    TextBox tb = (TextBox)sender;
    //    GridViewRow gvr = (GridViewRow)tb.Parent.Parent;
    //    //Index-ul randului din gridview
    //    int rowindex = gvr.RowIndex;
    //    //TextBox NrPozitie aferent volumului modificat
    //    TextBox pozitie = (TextBox)gvModal.Rows[rowindex].Cells[2].FindControl("tbPozitieModal");
    //    //Adauga la lista principala volumul modificat
    //    listaGospodariiModal[rowindex].Volum = tb.Text;
    //    //Adauga la lista principala nr pozitie modificata
    //    listaGospodariiModal[rowindex].NrPozitie = pozitie.Text;


    //    //Lista cu gospodariile identice
    //    var listaVolumPozIdentice = from row in listaGospodariiModal.AsEnumerable()
    //                                where tb.Text == row.Volum && pozitie.Text == row.NrPozitie
    //                                select row;
    //    listaconflict = listaVolumPozIdentice.ToList();

    //    //Daca mai am inca gospodarii in conflict
    //    int contor = 0;
    //    if (listaconflict.Count > 1)
    //    {
    //        int indexL = -1;
    //        foreach (GospodariiPentruReNumerotare gospodariiIntermed in listaGospodariiModal)
    //        {
    //            indexL++;
    //            Int64 id = gospodariiIntermed.GospodarieId;
    //            foreach (GospodariiPentruReNumerotare listaGospodariiGeneralaConflict in listaconflict)
    //            {
    //                if (listaGospodariiGeneralaConflict.GospodarieId == id)
    //                {
    //                    contor++;
    //                    listaGospodariiModal[indexL].Neregularitate = 1;
    //                }
    //            }
    //            if (contor >= listaconflict.Count)
    //            {
    //                IncarcaListaGridViewModal(listaGospodariiModal);
    //                break;
    //            }
    //        }
    //    }
    //    else
    //    {

    //        int indexL = -1;
    //        foreach (GospodariiPentruReNumerotare gospodariiIntermed in listaGospodariiModal)
    //        {
    //            indexL++;
    //            listaGospodariiModal[indexL].Neregularitate = 0;
    //            bt_OKModal.Visible = true;
    //            bt_ValidezMaiTarziuModal.Visible = false;
    //        }
    //    }
    //}
    //protected void tbPozitieModal_TextChanged(object sender, EventArgs e)
    //{
    //    return;
    //    List<GospodariiPentruReNumerotare> listaconflict = new List<GospodariiPentruReNumerotare>();
    //    //TextBox Volum
    //    TextBox tb = (TextBox)sender;
    //    GridViewRow gvr = (GridViewRow)tb.Parent.Parent;
    //    //Index-ul randului din gridview
    //    int rowindex = gvr.RowIndex;
    //    //TextBox NrPozitie aferent volumului modificat
    //    TextBox volum = (TextBox)gvModal.Rows[rowindex].Cells[2].FindControl("tbVolumModal");
    //    //Adauga la lista principala volumul modificat
    //    listaGospodariiModal[rowindex].Volum = volum.Text;
    //    //Adauga la lista principala nr pozitie modificata
    //    listaGospodariiModal[rowindex].NrPozitie = tb.Text;


    //    //Lista cu gospodariile identice
    //    var listaVolumPozIdentice = from row in listaGospodariiModal.AsEnumerable()
    //                                where volum.Text == row.Volum && tb.Text == row.NrPozitie
    //                                select row;
    //    listaconflict = listaVolumPozIdentice.ToList();

    //    //Daca mai am inca gospodarii in conflict
    //    int contor = 0;
    //    if (listaconflict.Count > 1)
    //    {
    //        int indexL = -1;
    //        foreach (GospodariiPentruReNumerotare gospodariiIntermed in listaGospodariiModal)
    //        {
    //            indexL++;
    //            Int64 id = gospodariiIntermed.GospodarieId;
    //            foreach (GospodariiPentruReNumerotare listaGospodariiGeneralaConflict in listaconflict)
    //            {
    //                if (listaGospodariiGeneralaConflict.GospodarieId == id)
    //                {
    //                    contor++;
    //                    listaGospodariiModal[indexL].Neregularitate = 1;
    //                }
    //            }
    //            if (contor >= listaconflict.Count)
    //            {
    //                IncarcaListaGridViewModal(listaGospodariiModal);
    //                break;
    //            }
    //        }
    //    }
    //    else
    //    {

    //        int indexL = -1;
    //        foreach (GospodariiPentruReNumerotare gospodariiIntermed in listaGospodariiModal)
    //        {
    //            indexL++;
    //            listaGospodariiModal[indexL].Neregularitate = 0;
    //            bt_OKModal.Visible = true;
    //            bt_ValidezMaiTarziuModal.Visible = false;
    //        }
    //    }
    //}
}
