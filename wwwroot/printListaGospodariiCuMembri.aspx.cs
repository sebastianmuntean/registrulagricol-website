﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;
using System.Data.SqlClient;

public partial class printListaGospodariiCuMembri : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataTable reportDataTable = GetDataTAbleForReport();
            DataTable unitatiDataTable = GetUnitateDenumire();
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource ds = new ReportDataSource("dsRapoarte_dtListaGospCuMembri", reportDataTable);
            ReportDataSource ds2 = new ReportDataSource("dsRapoarte_dtUnitati", unitatiDataTable);
            ReportViewer1.LocalReport.DataSources.Add(ds);
            ReportViewer1.LocalReport.DataSources.Add(ds2);
            ReportViewer1.LocalReport.Refresh();
            ReportViewer1.Visible = true;
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "printListaGosp", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
        }
    }
    protected void Page_SaveStateComplete(object sender, EventArgs e)
    {
        Warning[] warnings;
        string[] streamids;
        string mimeType;
        string encoding;
        string fileNameExtension;
        byte[] bytes = ReportViewer1.LocalReport.Render("PDF", null, out mimeType, out encoding, out fileNameExtension, out streamids, out warnings);
        Response.Buffer = true;
        Response.Clear();
        Response.ContentType = mimeType;
        Response.AddHeader("content-disposition", "inline; filename=Nume" + fileNameExtension);
        Response.BinaryWrite(bytes);
        Response.Flush();
        Response.End();
        Response.Close();
    }
    private DataTable GetDataTAbleForReport()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        string interogare = @"SELECT        gospodarii.sLocalitate, gospodarii.sJudet, gospodarii.sAp, gospodarii.sEtj, gospodarii.sSc, gospodarii.sBl, gospodarii.sNr, gospodarii.sStrada, gospodarii.strainas, gospodarii.jUnitate, gospodarii.persJuridica, 
                         gospodarii.localitate, gospodarii.judet, gospodarii.codUnic, gospodarii.codExploatatie, gospodarii.ap, gospodarii.et, gospodarii.sc, gospodarii.bl, gospodarii.nrInt, gospodarii.nr, gospodarii.strada, gospodarii.tip, 
                         gospodarii.jSubunitate, gospodarii.jCodFiscal, gospodarii.jNumeReprez, gospodarii.codSiruta, gospodarii.nrPozitie, gospodarii.volum, membri.nume, membri.cnp, membri.codRudenie, 
                         CASE membri.dataNasterii WHEN '01.01.1900' THEN NULL ELSE membri.dataNasterii END AS dataNasterii, membri.denumireRudenie, gospodarii.gospodarieId, COALESCE (membri.decedat, 'False') AS decedat, 
                         membri.decedatData, membri.decedatSerie, membri.decedatNumar
FROM            gospodarii INNER JOIN
                         membri ON gospodarii.gospodarieId = membri.gospodarieId INNER JOIN
                         unitati ON gospodarii.unitateId = unitati.unitateId
WHERE        (gospodarii.persJuridica = 0) AND (gospodarii.volum LIKE '"+Request.QueryString["vol"]+"') AND (gospodarii.nrInt >= '"+Request.QueryString["nr1"]+"') AND (gospodarii.nrInt <= '"+Request.QueryString["nr2"]+@"') 
                            AND (CONVERT(nvarchar, gospodarii.strainas) LIKE  '"+Request.QueryString["strainas"]+@"') AND 
                         (gospodarii.strada LIKE  '"+Request.QueryString["str"]+"') AND (membri.an = "+Session["SESan"]+@") 
                          AND (CONVERT(nvarchar, gospodarii.unitateId) = '" + Request.QueryString["unit"] + @"') AND (gospodarii.localitate LIKE  '" + Request.QueryString["loc"] + @"') 
                        AND  (gospodarii.nrPozitie LIKE '" +Request.QueryString["nrPoz"]+"') AND (CONVERT(nvarchar, unitati.judetId) =  '"+Request.QueryString["judet"]+@"') 
                        AND (CONVERT(nvarchar, COALESCE (membri.decedat, N'False')) LIKE '"+Request.QueryString["decedat"]+@"')
                        ORDER BY '" + Request.QueryString["conditieOrdonare"] + @"'";
        SqlDataAdapter adapter = new SqlDataAdapter(interogare, connection);
        DataTable table = new DataTable();
        adapter.Fill(table);
        ManipuleazaBD.InchideConexiune(connection);
        return table;
    }
     private DataTable GetUnitateDenumire()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        string interogare = @"SELECT        unitati.unitateId, unitati.unitateDenumire, unitati.unitateCodSiruta, unitati.judetId, unitati.localitateId, unitati.unitateStrada, unitati.unitateNr, unitati.unitateAp, 
                         unitati.unitateCodPostal, unitati.localitateComponentaId, unitati.unitateCodFiscal, unitati.unitateActiva, unitati.unitatePrincipala, judete.judetDenumire, 
                         localitati.localitateDenumire
FROM            unitati INNER JOIN
                         judete ON unitati.judetId = judete.judetId INNER JOIN
                         localitati ON unitati.localitateId = localitati.localitateId
WHERE        (unitati.unitateId = " + Session["SESunitateId"].ToString() + ")";
        SqlDataAdapter adapter = new SqlDataAdapter(interogare, connection);
        DataTable table = new DataTable();
        adapter.Fill(table);
        ManipuleazaBD.InchideConexiune(connection);
        return table;
    }
}
