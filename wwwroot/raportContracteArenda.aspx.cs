﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class raportContracteArenda : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            tbDeLaNr.Text = "0";
            tbLaNr.Text = "99999999";
            tbCFinal.Text = tbCInceput.Text = DateTime.Now.ToString().Remove(11);
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "raportContracteArenda", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);

            //ddlUnitate.DataBind();
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {


    }

    protected void ddlUnitate_Init(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select top(1) tipUtilizatorId from utilizatori where utilizatorId='" + Session["SESutilizatorId"].ToString() + "'";
        int vTipUtilizator = Convert.ToInt32(vCmd.ExecuteScalar());

        Session["rcaUtilizatorTip"] = vTipUtilizator.ToString();

        if (vTipUtilizator == 2 || vTipUtilizator == 3 || vTipUtilizator == 5)
        {
            vCmd.CommandText = "SELECT TOP (1) unitati.judetId FROM utilizatori INNER JOIN unitati ON utilizatori.unitateId = unitati.unitateId WHERE (utilizatori.utilizatorId = '" + Session["SESutilizatorId"].ToString() + "')";
            string vJudetId = vCmd.ExecuteScalar().ToString();
            ddlFJudet.Items.Clear();
            ddlFJudet.Items.Add(new ListItem("", vJudetId));
            vCmd.CommandText = "select unitateDenumire,unitateId from unitati where judetId='" + vJudetId + "' order by unitateDenumire";
            ddlUnitate.Items.Clear();
            ListItem vItem = new ListItem("-toate-", "%");
            ddlUnitate.Items.Add(vItem);
            SqlDataReader vTabel = vCmd.ExecuteReader();
            while (vTabel.Read())
            {
                vItem = new ListItem(vTabel["unitateDenumire"].ToString(), vTabel["unitateId"].ToString());
                ddlUnitate.Items.Add(vItem);
            }
            vTabel.Close();


            ((MasterPage)this.Page.Master).SchimbaGospodaria();
        }

        ManipuleazaBD.InchideConexiune(vCon);


    }
    protected void ddlFJudet_PreRender(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select judetDenumire,judetId from judete order by judetDenumire";
        ddlFJudet.Items.Clear();
        ListItem vItem = new ListItem("-toate-", "%");
        ddlFJudet.Items.Add(vItem);
        SqlDataReader vTabel = vCmd.ExecuteReader();
        while (vTabel.Read())
        {
            vItem = new ListItem(vTabel["judetDenumire"].ToString(), vTabel["judetId"].ToString());
            ddlFJudet.Items.Add(vItem);
        }
        vTabel.Close();
        vCmd.CommandText = "select top(1) coalesce(judetId,0) from unitati where convert(nvarchar,unitateId)='" + Session["SESunitateId"].ToString() + "'";
        if (ViewState["judetId"] == null)
            try { ViewState["judetId"] = vCmd.ExecuteScalar().ToString(); }
            catch { ViewState["judetId"] = "%"; }
        vCmd.CommandText = "select unitateDenumire,unitateId from unitati where convert(nvarchar,judetId) like '" + ViewState["judetId"].ToString() + "' order by unitateDenumire";
        ddlUnitate.Items.Clear();
        vItem = new ListItem("-toate-", "%");
        ddlUnitate.Items.Add(vItem);
        vTabel = vCmd.ExecuteReader();
        while (vTabel.Read())
        {
            vItem = new ListItem(vTabel["unitateDenumire"].ToString(), vTabel["unitateId"].ToString());
            ddlUnitate.Items.Add(vItem);
        }
        vTabel.Close();

        ManipuleazaBD.InchideConexiune(vCon);
        ((MasterPage)this.Page.Master).SchimbaGospodaria();
        //try
        //{
        //    ddlUnitate.SelectedValue = Session["SESunitateId"].ToString();
        //}
        //catch
        //{
        //    ddlUnitate.SelectedValue = "%";
        //}
        string unitateSelectata = string.Empty;
        if (Session["rcaUnitateId"] != null)
            unitateSelectata = Session["rcaUnitateId"].ToString();
        ddlUnitate.SelectedValue = unitateSelectata.Length > 0 ? unitateSelectata : Session["SESunitateId"].ToString();

        ddlFJudet.SelectedValue = ViewState["judetId"].ToString();
    }
    protected void ddlFJudet_SelectedIndexChanged(object sender, EventArgs e)
    {
        ViewState["judetId"] = ddlFJudet.SelectedValue;
    }
    protected void btTiparire_Click(object sender, EventArgs e)
    {
        // daca e functionar luam unitatea din sesiune
        if (Session["rcaUtilizatorTip"].ToString() == "3")
            Session["rcaUnitateId"] = Session["SESunitateId"];
        else
            Session["rcaUnitateId"] = ddlUnitate.SelectedValue;
        Session["rcaJudetId"] = ddlFJudet.SelectedValue;
        Session["rcaVolum"] = (tbVolum.Text == "") ? "%" : tbVolum.Text;
        Session["rcaPozitie"] = (tbNrPozitie.Text == "") ? "%" : tbNrPozitie.Text;
        Session["rcaNrDeLa"] = (tbDeLaNr.Text == "") ? "%" : tbDeLaNr.Text;
        Session["rcaNrLa"] = (tbLaNr.Text == "") ? "%" : tbLaNr.Text;
        Session["rcaStrainas"] = ddlStrainas.SelectedValue;
        Session["rcaStrada"] = (tbStrada.Text == "") ? "%" : tbStrada.Text;
        Session["rcaLocalitate"] = (tbLocalitate.Text == "") ? "%" : tbLocalitate.Text;
        Session["rcaData1"] = Convert.ToDateTime(tbCInceput.Text);
        Session["rcaData2"] = Convert.ToDateTime(tbCFinal.Text);
        Session["rcaGospodarieId"] = "%";
        Session["rcaTipContract"] = ddlTipContract.SelectedValue;
        Session["rcaOrdonare"] = ddlOrdonare.SelectedValue;
        ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "tiparire", "tiparire contracte arenda", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
        ResponseHelper.Redirect("~/printContracteArenda.aspx", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
    }
    protected void btTiparireXls_Click(object sender, EventArgs e)
    {
        // daca e functionar luam unitatea din sesiune
        if (Session["rcaUtilizatorTip"].ToString() == "3")
            Session["rcaUnitateId"] = Session["SESunitateId"];
        else
            Session["rcaUnitateId"] = ddlUnitate.SelectedValue;
        Session["rcaJudetId"] = ddlFJudet.SelectedValue;
        Session["rcaVolum"] = (tbVolum.Text == "") ? "%" : tbVolum.Text;
        Session["rcaPozitie"] = (tbNrPozitie.Text == "") ? "%" : tbNrPozitie.Text;
        Session["rcaNrDeLa"] = (tbDeLaNr.Text == "") ? "%" : tbDeLaNr.Text;
        Session["rcaNrLa"] = (tbLaNr.Text == "") ? "%" : tbLaNr.Text;
        Session["rcaStrainas"] = ddlStrainas.SelectedValue;
        Session["rcaStrada"] = (tbStrada.Text == "") ? "%" : tbStrada.Text;
        Session["rcaLocalitate"] = (tbLocalitate.Text == "") ? "%" : tbLocalitate.Text;
        Session["rcaData1"] = Convert.ToDateTime(tbCInceput.Text);
        Session["rcaData2"] = Convert.ToDateTime(tbCFinal.Text);
        Session["rcaGospodarieId"] = "%";
        Session["rcaTipContract"] = ddlTipContract.SelectedValue;
        Session["rcaOrdonare"] = ddlOrdonare.SelectedValue;
        ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "tiparire", "tiparire contracte arenda", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
        ResponseHelper.Redirect("~/printContracteArendaXls.aspx", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
    }
}
