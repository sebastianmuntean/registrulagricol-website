﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="printCentralizatorCapitolul10a.aspx.cs" Inherits="printCentralizatorCapitolul10a"  Culture="ro-RO" UICulture="ro-RO"  %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" Runat="Server">
    <asp:Label ID="url" runat="server" Text="Rapoarte / Centralizator CAPITOLUL  X: a) Aplicarea îngrăşămintelor, amendamentelor şi pesticidelor pe suprafeţe situate pe raza localităţii" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Panel ID="pnLista" runat="server" CssClass="panel_general" Visible="true">
    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" 
        Font-Size="8pt" Height="600px" Width="100%">
        <LocalReport ReportPath="rapoarte\raportCentralizatorCapitolul10a.rdlc">
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" 
                    Name="dsRapoarte_dtUnitati" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource2" 
                    Name="dsRapoarte_dtCentralizatorCapitole" />
            </DataSources>
        </LocalReport>
    </rsweb:ReportViewer>
    <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" 
        InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" 
        SelectMethod="GetData" 
        TypeName="dsRapoarteTableAdapters.dtCentralizatorCapitoleTableAdapter">
        <SelectParameters>
            <asp:SessionParameter Name="an" SessionField="SESan" Type="Int32" />
            <asp:SessionParameter Name="utilizatorId" SessionField="SESutilizatorId" 
                Type="Int32" />
            <asp:QueryStringParameter Name="capitol" QueryStringField="codCapitol" 
                Type="String" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="utilizatorId" Type="Int32" />
            <asp:Parameter Name="an" Type="Int32" />
            <asp:Parameter Name="capitol" Type="String" />
            <asp:Parameter Name="codRand" Type="Int32" />
            <asp:Parameter Name="denumire1" Type="String" />
            <asp:Parameter Name="denumire2" Type="String" />
            <asp:Parameter Name="denumire3" Type="String" />
            <asp:Parameter Name="denumire4" Type="String" />
            <asp:Parameter Name="denumire5" Type="String" />
            <asp:Parameter Name="col1" Type="Decimal" />
            <asp:Parameter Name="col2" Type="Decimal" />
            <asp:Parameter Name="col3" Type="Decimal" />
            <asp:Parameter Name="col4" Type="Decimal" />
            <asp:Parameter Name="col5" Type="Decimal" />
            <asp:Parameter Name="col6" Type="Decimal" />
            <asp:Parameter Name="col7" Type="Decimal" />
            <asp:Parameter Name="col8" Type="Decimal" />
        </InsertParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="dsRapoarteTableAdapters.dtUnitatiTableAdapter">
        <SelectParameters>
            <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" 
                Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    </asp:Panel>
</asp:Content>

