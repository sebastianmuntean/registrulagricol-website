﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

public partial class cautaParcela : System.Web.UI.Page
{
    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        DBConnections connection = new DBConnections(Convert.ToInt16(Session["SESan"]));

        SqlParcele.ConnectionString = connection.Create();
        SqlIstoricParcela.ConnectionString = connection.Create();

        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "cauta parcela", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
        }
    }
    protected void gvParcele_SelectedIndexChanged(object sender, EventArgs e)
    {
        btTiparireIstoricParcela.Visible = true;
        gvIstoricParcela.DataBind();
    }
    protected void gvParcele_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(gvParcele, e, this);
    }
    protected void btAnuleazaSalvarea_Click(object sender, EventArgs e)
    {
        pnListaParcele.Visible = true;
        pnIstoricParcela.Visible = false;
    }
    protected void btTiparireIstoricParcela_Click(object sender, EventArgs e)
    {
        pnListaParcele.Visible = false;
        pnIstoricParcela.Visible = true;
    }
    protected void ddlUnitate_Init(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select top(1) tipUtilizatorId from utilizatori where utilizatorId='" + Session["SESutilizatorId"].ToString() + "'";
        int vTipUtilizator = Convert.ToInt32(vCmd.ExecuteScalar());
        if (vTipUtilizator == 2 || vTipUtilizator == 3 || vTipUtilizator == 5)
        {
            vCmd.CommandText = "SELECT TOP (1) unitati.judetId FROM utilizatori INNER JOIN unitati ON utilizatori.unitateId = unitati.unitateId WHERE (utilizatori.utilizatorId = '" + Session["SESutilizatorId"].ToString() + "')";
            string vJudetId = vCmd.ExecuteScalar().ToString();
            ddlFJudet.Items.Clear();
            ddlFJudet.Items.Add(new ListItem("", vJudetId));
            vCmd.CommandText = "select unitateDenumire,unitateId from unitati where judetId='" + vJudetId + "' order by unitateDenumire";
            ddlUnitate.Items.Clear();
            ListItem vItem = new ListItem("-toate-", "%");
            ddlUnitate.Items.Add(vItem);
            SqlDataReader vTabel = vCmd.ExecuteReader();
            while (vTabel.Read())
            {
                vItem = new ListItem(vTabel["unitateDenumire"].ToString(), vTabel["unitateId"].ToString());
                ddlUnitate.Items.Add(vItem);
            }
            vTabel.Close();
            ddlUnitate.SelectedValue = Session["SESunitateId"].ToString();
            ((MasterPage)this.Page.Master).SchimbaGospodaria();
        }
        ManipuleazaBD.InchideConexiune(vCon);
    }
    protected void ddlUnitate_PreRender(object sender, EventArgs e)
    {

    }
    protected void ddlUnitate_SelectedIndexChanged(object sender, EventArgs e)
    {
        HttpCookie vCook = Request.Cookies["COOKTNT"];
        string v1 = "";
        string v2 = "";
        string v3 = "";

        if (vCook != null)
        {
            vCook = CriptareCookie.DecodeCookie(vCook);
            v1 = vCook["COOKan"];
            v2 = vCook["COOKutilizatorId"];
            v3 = vCook["COOKunitateId"];
            vCook = CriptareCookie.EncodeCookie(vCook);
            Response.Cookies.Add(vCook);
            Response.Cookies.Remove("COOKTNT");
        }
        HttpCookie vCook1 = new HttpCookie("COOKTNT");
        vCook1["COOKan"] = v1;
        vCook1["COOKutilizatorId"] = v2;
        vCook1["COOKunitateId"] = v3;
        vCook1 = CriptareCookie.EncodeCookie(vCook1);
        Response.Cookies.Add(vCook1);
        if (ddlUnitate.SelectedValue == "%")
            Session["SESunitateId"] = 0;
        else Session["SESunitateId"] = ddlUnitate.SelectedValue;
        //   Session["SESgospodarieId"] = "NULA";
        Session["SESgospodarieId"] = null;

        ((MasterPage)this.Page.Master).SchimbaGospodaria();
    }
    protected void ddlFJudet_PreRender(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select judetDenumire,judetId from judete order by judetDenumire";
        ddlFJudet.Items.Clear();
        ListItem vItem = new ListItem("-toate-", "%");
        ddlFJudet.Items.Add(vItem);
        SqlDataReader vTabel = vCmd.ExecuteReader();
        while (vTabel.Read())
        {
            vItem = new ListItem(vTabel["judetDenumire"].ToString(), vTabel["judetId"].ToString());
            ddlFJudet.Items.Add(vItem);
        }
        vTabel.Close();
        vCmd.CommandText = "select top(1) coalesce(judetId,0) from unitati where convert(nvarchar,unitateId)='" + Session["SESunitateId"].ToString() + "'";
        if (ViewState["judetId"] == null)
            try { ViewState["judetId"] = vCmd.ExecuteScalar().ToString(); }
            catch { ViewState["judetId"] = "%"; }
        vCmd.CommandText = "select unitateDenumire,unitateId from unitati where convert(nvarchar,judetId) like '" + ViewState["judetId"].ToString() + "' order by unitateDenumire";
        ddlUnitate.Items.Clear();
        vItem = new ListItem("-toate-", "%");
        ddlUnitate.Items.Add(vItem);
        vTabel = vCmd.ExecuteReader();
        while (vTabel.Read())
        {
            vItem = new ListItem(vTabel["unitateDenumire"].ToString(), vTabel["unitateId"].ToString());
            ddlUnitate.Items.Add(vItem);
        }
        vTabel.Close();
        ManipuleazaBD.InchideConexiune(vCon);
        try
        {
            ddlUnitate.SelectedValue = Session["SESunitateId"].ToString();
        }
        catch { ddlUnitate.SelectedValue = "%"; }
        ddlFJudet.SelectedValue = ViewState["judetId"].ToString();
        ((MasterPage)this.Page.Master).SchimbaGospodaria();
    }
    protected void ddlFJudet_SelectedIndexChanged(object sender, EventArgs e)
    {
        ViewState["judetId"] = ddlFJudet.SelectedValue;
    }
    protected void btTiparireParceleCautare_Click(object sender, EventArgs e)
    {
        Session["cpCategoria"] = ddlCCategoria.SelectedValue;
        Session["parcelaLocalitate"] = localitateDropDown.SelectedValue;
        Session["cpCF"] = (tbCCF.Text == "") ? "%" : tbCCF.Text;
        Session["cpDenumire"] = (tbCDenumire.Text == "") ? "%" : tbCDenumire.Text;
        Session["cpBloc"] = (tbCBloc.Text == "") ? "%" : tbCBloc.Text;
        Session["cpTopo"] = (tbCTopo.Text == "") ? "%" : tbCTopo.Text;
        Session["cpNrCadastral"] = (tbCNrCadastral.Text == "") ? "%" : tbCNrCadastral.Text;
        Session["cpNrCadastralProvizoriu"] = (tbCNrCadastralProvizoriu.Text == "") ? "%" : tbCNrCadastralProvizoriu.Text;
        Session["cpTitluProprietate"] = (tbTitluProprietate.Text == "") ? "%" : tbTitluProprietate.Text;
        Session["cpUnitate"] = ddlUnitate.SelectedValue;
        Session["cpJudet"] = ddlFJudet.SelectedValue;
        Session["cpOrdonare"] = ddlFOrdonare.SelectedValue;
        Response.Redirect("~/printParceleCautare.aspx");
    }
    protected void SqlParcele_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.CommandTimeout = 0;
        switch (ddlFOrdonare.SelectedValue)
        {
            case "0": e.Command.CommandText += " ORDER BY gospodarii.volumInt, gospodarii.nrPozitieInt"; break;
            case "1": e.Command.CommandText += " ORDER BY gospodarii.localitate, gospodarii.strada, gospodarii.nr"; break;
            case "2": e.Command.CommandText += " ORDER BY nume"; break;
            case "3": e.Command.CommandText += " ORDER BY parcele.parcelaTarla"; break;
            case "4": e.Command.CommandText += " ORDER BY parcele.parcelaNrCadastral"; break;
            case "5": e.Command.CommandText += " ORDER BY parcele.parcelaNrCadastralProvizoriu"; break;
        }
    }
    protected void ddlFOrdonare_SelectedIndexChanged(object sender, EventArgs e)
    {
        gvParcele.DataBind();
    }

    private DataTable GetLocalitati()
    {
        SqlConnection connecion = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        DataTable table = new DataTable();
        string interogare = @"select  distinct  parcele.parcelalocalitate
                                    from parcele
                            join unitati on unitati.unitateId = parcele.unitateaId
                            WHERE unitati.unitateid = '" + Session["SESunitateId"] + "' and parcele.parcelaLocalitate <> '-'";
        SqlCommand command = new SqlCommand(interogare, connecion);
        SqlDataAdapter adapter = new SqlDataAdapter(interogare, connecion);
        DataRow row = table.NewRow();
        adapter.Fill(table);
        ManipuleazaBD.InchideConexiune(connecion);
        return table;
    }
    protected void localitateDropDown_Init(object sender, EventArgs e)
    {
        localitateDropDown.DataSource = GetLocalitati();
        localitateDropDown.DataTextField = "parcelalocalitate";
        ListItem vItem = new ListItem("-Toate-", "%");
        localitateDropDown.Items.Add(vItem);
        localitateDropDown.DataBind();
    }
    protected void localitateDropDown_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
