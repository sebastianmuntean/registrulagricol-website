﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="adevSubsoluri.aspx.cs" Inherits="adevSubsoluri" UICulture="ro-Ro" Culture="ro-Ro"
    EnableEventValidation="false" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Adeverinţe / Subsoluri"></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="pnListaAdevAnteturi" runat="server" Visible="true" CssClass="panel_general">
        <asp:GridView ID="gvListaAdevAnteturi" runat="server" AllowPaging="True" AutoGenerateColumns="False"
            DataKeyNames="adevSubsolId" DataSourceID="SqlListaAdevAnteturi" CssClass="tabela"
            OnRowDataBound="gvListaAdevAnteturi_RowDataBound" OnSelectedIndexChanged="gvListaAdevAnteturi_SelectedIndexChanged"
            OnDataBound="gvListaAdevAnteturi_DataBound" AllowSorting="True" EmptyDataText="Nu există anteturi proprii create. Apăsați butonul 'adaugă un subsol de adeverință'! "
            OnPreRender="gvListaAdevAnteturi_PreRender">
            <Columns>
                <asp:BoundField DataField="adevSubsolId" HeaderText="Nr." InsertVisible="False" ReadOnly="True"
                    SortExpression="adevSubsolId" Visible="false" />
                <asp:BoundField DataField="unitateDenumire" HeaderText="Creat de" SortExpression="unitateDenumire" />
                <asp:BoundField DataField="adevSubsolDenumire" HeaderText="Denumire subsol" SortExpression="adevSubsolDenumire" />
                <asp:BoundField DataField="unitateTipAdev" HeaderText="Visibil pentru" SortExpression="unitateTipAdev" />
                <asp:TemplateField HeaderText="">
                    <ItemTemplate>
                        <asp:Label ID="lblText" runat="server" Text='<%# Bind("adevSubsolText") %>' Visible="true" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlListaAdevAnteturi" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016 %>"
            SelectCommand="SELECT * FROM [adeverinteSubsoluri] LEFT OUTER JOIN unitati ON unitati.unitateId = adeverinteSubsoluri.unitateID WHERE adevSubsolTip = '1'"
            OnInit="SqlListaAdevAnteturi_Init">
            <SelectParameters>
                <asp:SessionParameter DefaultValue="" Name="unitateId" SessionField="SESunitateId"
                    Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:Panel ID="pnListaButoanePrincipale" CssClass="butoane" runat="server">
            <asp:Button CssClass="buton" ID="btModificaAdevAntet" runat="server" Text="modificare subsol de adeverință"
                OnClick="ArataAdaugaAdevAntet" Visible="false" />
            <asp:Button CssClass="buton" ID="btStergeAdevAntet" runat="server" Text="şterge subsolul"
                OnClick="StergeAntetul" ValidationGroup="GrupValidareAdevAntet" OnClientClick="return confirm(&quot;Sunteți sigur că doriți să ştergeţi?&quot;)"
                Visible="false" />
            <asp:Button CssClass="buton" ID="btListaAdaugaAdevAntet" runat="server" Text="adaugă un subsol de adeverinţă"
                OnClick="ArataAdaugaAdevAntet" />
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnAdaugaAdevAntet" CssClass="panel_general" runat="server" Visible="false">
        <asp:Panel ID="Panel1" CssClass="adauga" runat="server">
            <asp:Panel ID="Panel2" runat="server">
                <h2>
                    ADAUGĂ / MODIFICĂ UN SUBSOL DE ADEVERINŢĂ</h2>
            </asp:Panel>
            <asp:Panel ID="pn9Coloane" runat="server">
                <p>
                    <asp:Button ID="bt1" runat="server" Text="1col" OnClick="bt1_Click" Visible="false" />
                    <asp:Button ID="bt2" runat="server" Text="2col" OnClick="bt2_Click" Visible="false" />
                    <asp:Label ID="lblDenumireAntet" runat="server" Text="Denumire antet "></asp:Label><asp:TextBox
                        ID="tbDenumireAntet" runat="server" CssClass="rand1"></asp:TextBox><asp:Label ID="lblAdevTip"
                            runat="server" Text="Tip șablon adeverință" Visible="false"></asp:Label><asp:DropDownList
                                Width="270px" ID="ddAdevTip" runat="server" DataSourceID="SqlUnitati" DataTextField="unitateDenumire"
                                DataValueField="unitateId" OnDataBound="ddAdevTip_DataBound" Visible="false">
                                <asp:ListItem Value="0">
                                        vizibilă pentru toate unitățile</asp:ListItem>
                            </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlUnitati" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016 %>"
                        SelectCommand="SELECT judete.judetDenumire + ' - ' + unitati.unitateDenumire AS unitateDenumire, unitati.unitateId FROM unitati INNER JOIN judete ON unitati.judetId = judete.judetId ORDER BY unitateDenumire"></asp:SqlDataSource>
                </p>
                <p>
                    <asp:Label ID="lblNumarColoane" runat="server" Text="Numar coloane: "></asp:Label><asp:DropDownList
                        Width="270px" ID="ddlNumarColoane" runat="server" Visible="true" AutoPostBack="True"
                        OnSelectedIndexChanged="ddlNumarColoane_SelectedIndexChanged">
                        <asp:ListItem Value="0">alege numărul de coloane</asp:ListItem>
                        <asp:ListItem Value="2">
                                        două coloane</asp:ListItem>
                        <asp:ListItem Value="1">
                                        o coloană (pe mijloc)</asp:ListItem>
                        <asp:ListItem Value="3">
                                        trei coloane</asp:ListItem>
                    </asp:DropDownList>
                </p>
                <asp:Panel ID="Panel9" runat="server">
                    <asp:TextBox ID="tbTextAvansat" runat="server" TextMode="MultiLine" Rows="20" />

                    <script type="text/javascript">
                        //<![CDATA[
                        CKEDITOR.replace('ctl00_ContentPlaceHolder1_tbTextAvansat',
					{
					    fullPage: true,
					    width: 750,
					    height: 200
					});
                        //]]>
                    </script>

                    <asp:Panel ID="pnlButRefresh" runat="server" CssClass="butoane">
                        <asp:Button runat="server" ID="btImagini" CssClass="buton" Text="actualizaţi imaginile de mai sus"
                            OnClick="btImagini_Click" />
                    </asp:Panel>
                    <asp:Panel ID="pnlDlEmbleme" runat="server" CssClass="pozitie">
                        <asp:DataList ID="dlEmbleme" runat="server" DataKeyField="fisierId" DataSourceID="SqlEmbleme"
                            RepeatDirection="Horizontal" OnPreRender="dlEmbleme_PreRender" CssClass="pozitie">
                            <ItemTemplate>
                                <asp:Label ID="lblDlNumeFisier" runat="server" Text='<%# Eval("fisierNume") %>' />
                                <asp:Label ID="lblDlDescriere" runat="server" Visible="false" Text='<%# Eval("fisierDescriere") %>' />
                                <asp:Image ID="imgDlEmblema" runat="server" BorderStyle="Solid" BorderWidth="1px"
                                    BorderColor="Beige" Width="100px" CssClass="imagine" />
                            </ItemTemplate>
                        </asp:DataList>
                        <asp:SqlDataSource ID="SqlEmbleme" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016 %>"
                            SelectCommand="SELECT [fisierId], [fisierDescriere], [fisierNume], [fisierDataIncarcare] FROM [fisiere] WHERE (([unitateCodFiscal] = @unitateCodFiscal OR [unitateCodFiscal] = '14146589') AND ([fisierStatus] = @fisierStatus)) ORDER BY [fisierNume]"
                            DeleteCommand="UPDATE fisiere SET fisierStatus='sters' WHERE (fisierId = @fisierId)"
                            OnInit="SqlEmbleme_Init">
                            <SelectParameters>
                                <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" Type="Int32" />
                                <asp:Parameter DefaultValue="activ" Name="fisierStatus" Type="String" />
                                <asp:Parameter DefaultValue="activ" Name="unitateCodFiscal" Type="String" />
                            </SelectParameters>
                            <DeleteParameters>
                                <asp:ControlParameter ControlID="gvEmbleme" Name="fisierId" PropertyName="SelectedValue" />
                            </DeleteParameters>
                        </asp:SqlDataSource>
                    </asp:Panel>
                    <asp:TextBox TextMode="MultiLine" BackColor="White" Rows="10" ID="lblLegenda" BorderStyle="None"
                        runat="server" CssClass="legenda" ReadOnly="True"></asp:TextBox></asp:Panel>
            </asp:Panel>
            <asp:Panel ID="pnButoanePrincipale" runat="server" CssClass="butoane">
                <asp:Button CssClass="buton" ID="btAdaugaAdevAntet" runat="server" Text="adaugă un subsol de adeverință"
                    OnClick="AdaugaModificaAdevAntet" Visible="true" />
                <asp:Button ID="btAdaugaModificaAdevAntet" runat="server" CssClass="buton" OnClick="AdaugaModificaAdevAntet"
                    Text="modificare subsol de adeverință" Visible="true" />
                <asp:Button ID="btModificaArataListaAdevAntet" runat="server" CssClass="buton" OnClick="ModificaArataListaAdevAntet"
                    Text="lista subsolurilor" Visible="true" />
                <asp:Button ID="btAdaugaArataListaAdevAntet" runat="server" CssClass="buton" OnClick="AdaugaArataListaAdevAntet"
                    Text="lista subsolurilor" Visible="true" />
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
</asp:Content>
