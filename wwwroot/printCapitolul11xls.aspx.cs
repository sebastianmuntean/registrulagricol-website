﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Microsoft.Reporting.WebForms;

public partial class printCapitolul11xls: System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataTable reportDataTable = GetCapitol11();
            DataTable unitatiDataTable = GetUnitateDenumire();
            DataTable gospodariiDataTable = GetGospodarie();
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource ds = new ReportDataSource("dsRapoarte_sabloaneCapitole", reportDataTable);
            ReportDataSource ds2 = new ReportDataSource("dsRapoarte_dtUnitati", unitatiDataTable);
            ReportDataSource ds3 = new ReportDataSource("dsRapoarte_dtGospodarii", gospodariiDataTable);
            ReportViewer1.LocalReport.DataSources.Add(ds);
            ReportViewer1.LocalReport.DataSources.Add(ds2);
            ReportViewer1.LocalReport.DataSources.Add(ds3);
            ReportViewer1.LocalReport.Refresh();
            ReportViewer1.Visible = true;
        }
    }
    protected void Page_SaveStateComplete(object sender, EventArgs e)
    {
        Warning[] warnings;
        string[] streamids;
        string mimeType;
        string encoding;
        string fileNameExtension;
        byte[] bytes = ReportViewer1.LocalReport.Render("Excel", null, out mimeType, out encoding, out fileNameExtension, out streamids, out warnings);
        Response.Buffer = true;
        Response.Clear();
        Response.ContentType = mimeType;
        Response.AddHeader("content-disposition", "inline; filename=Capitolul11." + fileNameExtension);
        Response.BinaryWrite(bytes);
        Response.Flush();
        Response.End();
        Response.Close();
    }
    public DataTable GetCapitol11()
    {
//        SqlConnection connection = ManipuleazaBD.CreareConexiune();
//        string interogare = @"SELECT     utilizatorId, capitol, codRand, denumire1, denumire2, denumire3, denumire4, denumire5, an1, col1_1, col1_2, col1_3, col1_4, col1_5, col1_6, col1_7, col1_8,col1_9,col1_10, an2, 
//                      col2_1, col2_2, col2_3, col2_4, col2_5, col2_6, col2_7, col2_8, col2_9,col2_10,an3, col3_1, col3_2, col3_3, col3_4, col3_5, col3_6, col3_7, col3_8, col3_9,col3_10,an4, col4_1, col4_2, col4_3, col4_4, 
//                      col4_5, col4_6, col4_7, col4_8,col4_9,col4_10, an5, col5_1, col5_2, col5_3, col5_4, col5_5, col5_6, col5_7, col5_8, col5_9,col5_10,camp1, camp2, camp3, camp4, camp5, camp6, camp7, camp8, 
//                      camp9, camp10   ,[col1_11]      ,[col2_11]      ,[col3_11]      ,[col4_11]      ,[col5_11]
//FROM         rapCapitole
//WHERE     (utilizatorId = " + Session["SESutilizatorId"]+@") AND (capitol = '11')
//ORDER BY codRand";
        //SqlDataAdapter adapter = new SqlDataAdapter(interogare, connection);
        DataTable table = new DataTable();
        //adapter.Fill(table);
        //ManipuleazaBD.InchideConexiune(connection);

        table = (DataTable)Session["printCapitol11"];


        return table;
    }
    private DataTable GetGospodarie()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        string interogare = @"SELECT        gospodarieId, unitateId, volum, nrPozitie, codSiruta, tip, COALESCE (strada, '') AS strada, COALESCE (nr, '') AS nr, nrInt, COALESCE (bl, '') AS bl, 
                         COALESCE (sc, '') AS sc, COALESCE (et, '') AS et, COALESCE (ap, '') AS ap, codExploatatie, codUnic, judet, localitate, persJuridica, jUnitate, jSubunitate, 
                         jCodFiscal, jNumeReprez, strainas, sStrada, sNr, sBl, sSc, sEtj, sAp, sJudet, sLocalitate,
                             (SELECT        TOP (1) nume
                               FROM            membri
                               WHERE        (gospodarieId = gospodarii.gospodarieId)
                               ORDER BY codRudenie) AS Membru,
                             (SELECT        TOP (1) cnp
                               FROM            membri AS membri_1
                               WHERE        (gospodarieId = gospodarii.gospodarieId)
                               ORDER BY codRudenie) AS cnpMembru
FROM            gospodarii
where an = " + Session["SESAn"] + " and gospodarieId = " + Session["SESGospodarieid"] + "";
        SqlDataAdapter adapter = new SqlDataAdapter(interogare, connection);
        DataTable table = new DataTable();
        adapter.Fill(table);
        ManipuleazaBD.InchideConexiune(connection);
        return table;
    }

    private DataTable GetUnitateDenumire()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        string interogare = @"SELECT        unitati.unitateId, unitati.unitateDenumire, unitati.unitateCodSiruta, unitati.judetId, unitati.localitateId, unitati.unitateStrada, unitati.unitateNr, unitati.unitateAp, 
                         unitati.unitateCodPostal, unitati.localitateComponentaId, unitati.unitateCodFiscal, unitati.unitateActiva, unitati.unitatePrincipala, judete.judetDenumire, 
                         localitati.localitateDenumire
FROM            unitati INNER JOIN
                         judete ON unitati.judetId = judete.judetId INNER JOIN
                         localitati ON unitati.localitateId = localitati.localitateId
WHERE        (unitati.unitateId = " + Session["SESunitateId"].ToString() + ")";
        SqlDataAdapter adapter = new SqlDataAdapter(interogare, connection);
        DataTable table = new DataTable();
        adapter.Fill(table);
        ManipuleazaBD.InchideConexiune(connection);
        return table;
    }
}