﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="raportTerenuriInProprietate.aspx.cs" Inherits="raportTerenuriInProprietate"
    Culture="ro-RO" UICulture="ro-RO" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Rapoarte / Terenuri în proprietate" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="pnGeneral" runat="server" CssClass="panel_general">
        <asp:Panel ID="pnFiltrareUnitati" runat="server" CssClass="cauta">
            <asp:Label ID="lblFJudet" runat="server" Text="Judeţ"></asp:Label>
            <asp:DropDownList ID="ddlFJudet" runat="server" OnPreRender="ddlFJudet_PreRender"
                OnSelectedIndexChanged="ddlFJudet_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:Label ID="lblUnitate" runat="server" Text="unitatea:"></asp:Label>
            <asp:DropDownList ID="ddlUnitate" runat="server" OnInit="ddlUnitate_Init">
            </asp:DropDownList>
        </asp:Panel>
        <asp:Panel ID="pnFiltrare" runat="server" CssClass="cauta">
            <asp:Label ID="lblFiltrareDupa" runat="server" Text="Caută după:"></asp:Label>
            <asp:Label ID="lblVolum" runat="server" Text="vol."></asp:Label>
            <asp:TextBox ID="tbVolum" runat="server" Width="30px"></asp:TextBox>
            <asp:Label ID="lblPozitie" runat="server" Text="poz."></asp:Label>
            <asp:TextBox ID="tbPozitie" Width="30px" runat="server"></asp:TextBox>
            <asp:Label ID="lblDeLaNr" runat="server" Text="de la nr."></asp:Label>
            <asp:TextBox ID="tbDeLaNr" runat="server" Width="40px"></asp:TextBox>
            <asp:Label ID="lblLaNr" runat="server" Text="la"></asp:Label>
            <asp:TextBox ID="tbLaNr" runat="server" Width="40px"></asp:TextBox>
            <asp:Label ID="LabelStrainas" runat="server" Text="strainaș"></asp:Label>
            <asp:DropDownList ID="ddlStrainas" runat="server">
                <asp:ListItem Value="%">toți</asp:ListItem>
                <asp:ListItem Value="1">da</asp:ListItem>
                <asp:ListItem Value="0">nu</asp:ListItem>
            </asp:DropDownList>
            <asp:Label ID="lblStrada" runat="server" Text="str."></asp:Label>
            <asp:TextBox ID="tbStrada" runat="server" Width="100px"></asp:TextBox>
            <asp:Label ID="lblLocalitate" runat="server" Text="loc."></asp:Label>
            <asp:TextBox ID="tbLocalitate" runat="server" Width="100px"></asp:TextBox>
            <asp:Label ID="lblTipPers" runat="server" Text="pers.jur."></asp:Label>
            <asp:DropDownList ID="ddlPersJuridica" runat="server">
                <asp:ListItem Value="%">toți</asp:ListItem>
                <asp:ListItem Value="1">da</asp:ListItem>
                <asp:ListItem Value="0">nu</asp:ListItem>
            </asp:DropDownList>
        </asp:Panel>
        <asp:Panel ID="pnOrdonare" runat="server" CssClass="cauta">
            <asp:Label ID="lblTipTeren" runat="server" Text="Tip teren"></asp:Label>
            <asp:DropDownList ID="ddlTipTeren" runat="server">
                <asp:ListItem Text="-toate-" Value="0"></asp:ListItem>
                <asp:ListItem Text="intravilan" Value="1"></asp:ListItem>
                <asp:ListItem Text="extravilan" Value="2"></asp:ListItem>
            </asp:DropDownList>
            <asp:Label ID="lblOrdonareDupa" runat="server" Text="Ordonează după:"></asp:Label>
            <asp:DropDownList ID="ddlOrdonareDupa" runat="server">
                <asp:ListItem Value="0">nume</asp:ListItem>
                <asp:ListItem Value="1">județ + localitate + stradă + nr.</asp:ListItem>
                <asp:ListItem Value="2">volum + nr. poziție</asp:ListItem>
            </asp:DropDownList>
        </asp:Panel>
        <asp:Panel ID="pnButoane" runat="server" CssClass="butoane">
            <asp:Button ID="btTiparire" runat="server" Text="tipărește" CssClass="buton" OnClick="btTiparire_Click" />
        </asp:Panel>
        <asp:Panel ID="pnRaport" runat="server" CssClass="panel_general">
        </asp:Panel>
    </asp:Panel>
</asp:Content>
