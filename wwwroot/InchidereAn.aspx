﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="InchidereAn.aspx.cs" Inherits="InchidereAn" Culture="ro-RO" UICulture="ro-RO" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Administrare / Închidere an" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel ID="upParcele" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel1" runat="server" CssClass="panel_general">
                <asp:Panel ID="Panel2" CssClass="adauga" runat="server">
                    <asp:Panel ID="pnEroare" runat="server" HorizontalAlign="Center">
                        <asp:Label ID="lblEroare" runat="server" CssClass="validator" Text="Eroare" Visible="false" />
                    </asp:Panel>
                    <p style="text-align:center; margin:10px auto;">
                        <asp:Label ID= "lblAn" runat="server" Text="An"></asp:Label>
                        <asp:TextBox ID="tbAn" runat="server" onprerender="tbAn_PreRender"></asp:TextBox>
                        <div  style="width:auto; margin-right:300px; margin-left:300px">
                          <div style="text-align:center;margin:0 auto; font-size:large"> 
                            <label style="color:RED; font-size:larger; font-weight:bolder">IMPORTANT</label> 
                          </div>
                            <label style="color:black; font-size:larger">    În cazul în care doriţi <b>preluarea / transferul de animale </b> din Semestrul I, 2015, şi nu aţi efectuat până acum transferul de pe Semestrul I pe Semestrul II, 2015, <b> vă invităm să lansaţi acest transfer înaintea închiderii prin pagina </b>: </label>
                            <div style="text-align:center;margin:0 auto; font-size:large"> 
                                <a href="InchidereSemestruAnimale.aspx" style="color:darkslategray; font-weight:600"> Administrare / Închidere Semestru Animale</a>
                            </div>
                            <label style="color:black; font-size:larger">     Vă rugăm ca, <b>după lansarea în execuţie a închiderii să nu se mai lucreze de pe alt calculator în Registrul Agricol </b>, până la finalizarea operaţiunii <b>(30-60 minute)</b>. </label>
                            <div style="text-align:center;margin:0 auto; font-size:large"> 
                                <label style="color:#ff3434; font-size:large; font-weight:bold;"> După ce un an s-a închis, nu se mai pot modifica sau adăuga date pentru acel an! </label>
                                <br/>
                                <label style="color:#ff3434; font-size:large; font-weight:bold;"> Închiderea va putea fi iniţiată doar după ora 15:00. </label>
                            </div>
                        </div>
                    </p>
                    <asp:Panel ID="pnButoaneAdaugare" runat="server" CssClass="butoane" HorizontalAlign="Center">
                        <asp:Button ID="inchideButton" runat="server" CssClass="buton" Text="Porneşte închiderea" OnClick="InchideButtonClick" OnClientClick="return confirm('Doriti sa incepeti inchiderea ?')" />
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>   
     
</asp:Content>
