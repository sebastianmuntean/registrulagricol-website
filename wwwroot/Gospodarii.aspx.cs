﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

/// <summary>
/// Pagina gospodarii
/// Creata la:                  15.02.2011
/// Autor:                      Laza Tudor Mihai
/// Ultima                      actualizare: 15.02.2011
/// Autor:                      Laza Tudor Mihai
/// ultima modificare           20.01.2012 , alex -> gvGospodari doar un singur sqldataSource si adaugare eveniment de selecting pentru a face filtrarea , si filtrare dupa cnp si cod APIA
/// </summary> 
public partial class Gospodarii : System.Web.UI.Page
{
    #region evenimente pagina
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        DBConnections connection = new DBConnections(Convert.ToInt16(Session["SESan"]));

        SqlGospodarii.ConnectionString = connection.Create();
        if (!IsPostBack)
        {
            VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
            vVerificaSesiuni.VerificaSesiuniCookie();
            // extrag tip afisare strainas
            SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
            SqlCommand vCmd = new SqlCommand();
            vCmd.Connection = vCon;
            vCmd.CommandText = "select top(1) unitateTipAfisareStrainas from unitati where unitateId='" + Session["SESunitateId"].ToString() + "'";
            ViewState["tipAfisareStrainas"] = vCmd.ExecuteScalar().ToString();
            ManipuleazaBD.InchideConexiune(vCon);
            try
            {
                ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "gospodarii", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
            }
            catch { }
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        if (!IsPostBack)
        {
            VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
            vVerificaSesiuni.VerificaSesiuniCookie();
        }
        if (Session["SESfVol"] != null)
            tbfVolum.Text = Session["SESfVol"].ToString();
        if (Session["SESfNrPoz"] != null)
            tbfNrPoz.Text = Session["SESfNrPoz"].ToString();
        if (Session["SESfNume"] != null)
            tbfNume.Text = Session["SESfNume"].ToString();
        if (Session["SESfLoc"] != null)
            tbfLoc.Text = Session["SESfLoc"].ToString();
        if (Session["SESfStrada"] != null)
            tbfStrada.Text = Session["SESfStrada"].ToString();
        if (Session["SESfNr"] != null)
            tbfNr.Text = Session["SESfNr"].ToString();
        if (Session["SESfTip"] != null)
            ddlFTip.SelectedValue = Session["SESfTip"].ToString();
        if (Session["SESfBl"] != null)
            tbfBl.Text = Session["SESfBl"].ToString();
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Session["SESgospodarieId"] == null || Session["SESgospodarieId"].ToString() == "")
        {
            ((MasterPage)this.Page.Master).SchimbaGospodaria();
            gvGospodarii.SelectedIndex = -1;
        }
        else
        {
            btModifica.Visible = true;
            btSterge.Visible = true;
            btMasura112.Visible = true;
            btMasura141.Visible = true;
            dosareVanzareButton.Visible = true;
        }
        btSterge.Visible = btModifica.Visible = gvGospodarii.SelectedIndex == -1 ? false : true;

    }

    #endregion

    #region filtrari

    protected void ddlfUnitati_Init(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select top(1) tipUtilizatorId from utilizatori where utilizatorId='" + Session["SESutilizatorId"].ToString() + "'";
        int vTipUtilizator = Convert.ToInt32(vCmd.ExecuteScalar());
        if (vTipUtilizator == 2 || vTipUtilizator == 3 || vTipUtilizator == 5)
        {
            vCmd.CommandText = "SELECT TOP (1) unitati.judetId FROM utilizatori INNER JOIN unitati ON utilizatori.unitateId = unitati.unitateId WHERE (utilizatori.utilizatorId = '" + Session["SESutilizatorId"].ToString() + "')";
            string vJudetId = vCmd.ExecuteScalar().ToString();
            ddlFJudet.Items.Clear();
            ddlFJudet.Items.Add(new ListItem("", vJudetId));
            vCmd.CommandText = "select unitateDenumire,unitateId from unitati where judetId='" + vJudetId + "' order by unitateDenumire";
            ddlUnitate.Items.Clear();
            ListItem vItem = new ListItem("-toate-", "%");
            ddlUnitate.Items.Add(vItem);
            SqlDataReader vTabel = vCmd.ExecuteReader();
            while (vTabel.Read())
            {
                vItem = new ListItem(vTabel["unitateDenumire"].ToString(), vTabel["unitateId"].ToString());
                ddlUnitate.Items.Add(vItem);
            }
            vTabel.Close();
            ddlUnitate.SelectedValue = Session["SESunitateId"].ToString();
            // ((MasterPage)this.Page.Master).SchimbaGospodaria();
        }
        ManipuleazaBD.InchideConexiune(vCon);
    }
    protected void ddlUnitate_PreRender(object sender, EventArgs e)
    {

    }
    protected void ddlUnitate_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlUnitate.SelectedValue == "%")
        {
            Session["SESunitateId"] = 0;
            // Session["SESan"] = ManipuleazaBD.fRezultaUnString("SELECT unitateAnDeschis FROM unitati WHERE unitateID = '" + Session["SESunitateID"].ToString() + "'", "unitateAnDeschis");
        }
        else
        {
            Session["SESunitateId"] = ddlUnitate.SelectedValue;
            Session["SESjudetId"] = ddlFJudet.SelectedValue;
            Session["SESan"] = ManipuleazaBD.fRezultaUnString("SELECT unitateAnDeschis FROM unitati WHERE unitateID = '" + Session["SESunitateID"].ToString() + "'", "unitateAnDeschis", Convert.ToInt16(Session["SESan"]));

        }
        //   Session["SESgospodarieId"] = "NULA";
        Session["SESgospodarieId"] = null;


        HttpCookie vCook = Request.Cookies["COOKTNT"];
        string v1 = "";
        string v2 = "";
        string v3 = "";
        string v4 = "";

        if (vCook != null)
        {
            vCook = CriptareCookie.DecodeCookie(vCook);
            v1 = vCook["COOKan"];
            v2 = vCook["COOKutilizatorId"];
            v3 = vCook["COOKunitateId"];
            v4 = vCook["COOKjudetId"];
            vCook = CriptareCookie.EncodeCookie(vCook);
            Response.Cookies.Add(vCook);
            Response.Cookies.Remove("COOKTNT");
        }
        HttpCookie vCook1 = new HttpCookie("COOKTNT");
        vCook1["COOKan"] = v1;
        vCook1["COOKutilizatorId"] = v2;
        vCook1["COOKunitateId"] = ddlUnitate.SelectedValue;
        vCook1["COOKjudetId"] = ddlFJudet.SelectedValue;
        // vCook1["COOKan"] = ManipuleazaBD.fRezultaUnString("SELECT unitateAnDeschis FROM unitati WHERE unitateID = '" + Session["SESunitateID"].ToString() + "'", "unitateAnDeschis");
        vCook1["COOKan"] = Session["SESan"].ToString();

        vCook1 = CriptareCookie.EncodeCookie(vCook1);
        Response.Cookies.Add(vCook1);


        ((MasterPage)this.Page.Master).SchimbaGospodaria();
        gvGospodarii.SelectedIndex = -1;
        Response.Redirect("~/gospodarii.aspx");
    }
    protected void ddlFOrdonareDupa_Init(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        ddlFOrdonareDupa.SelectedValue = Session["SESordonareGospodarii"].ToString();
    }
    protected void ddlFJudet_PreRender(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select judetDenumire,judetId from judete order by judetDenumire";
        ddlFJudet.Items.Clear();
        ListItem vItem = new ListItem("-toate-", "%");
        ddlFJudet.Items.Add(vItem);
        SqlDataReader vTabel = vCmd.ExecuteReader();
        while (vTabel.Read())
        {
            vItem = new ListItem(vTabel["judetDenumire"].ToString(), vTabel["judetId"].ToString());
            ddlFJudet.Items.Add(vItem);
        }
        vTabel.Close();
        vCmd.CommandText = "select top(1) coalesce(judetId,0) from unitati where convert(nvarchar,unitateId)='" + Session["SESunitateId"].ToString() + "'";
        if (ViewState["judetId"] == null)
            try { ViewState["judetId"] = vCmd.ExecuteScalar().ToString(); }
            catch { ViewState["judetId"] = "%"; }
        vCmd.CommandText = "select unitateDenumire,unitateId from unitati where convert(nvarchar,judetId) like '" + ViewState["judetId"].ToString() + "' order by unitateDenumire";
        ddlUnitate.Items.Clear();
        vItem = new ListItem("-toate-", "%");
        ddlUnitate.Items.Add(vItem);
        vTabel = vCmd.ExecuteReader();
        while (vTabel.Read())
        {
            vItem = new ListItem(vTabel["unitateDenumire"].ToString(), vTabel["unitateId"].ToString());
            ddlUnitate.Items.Add(vItem);
        }
        vTabel.Close();
        ManipuleazaBD.InchideConexiune(vCon);
        try
        {
            ddlUnitate.SelectedValue = Session["SESunitateId"].ToString();
        }
        catch { ddlUnitate.SelectedValue = "%"; }
        ddlFJudet.SelectedValue = ViewState["judetId"].ToString();
        ((MasterPage)this.Page.Master).SchimbaGospodaria();

        gvGospodarii.DataBind();
    }
    protected void ddlFJudet_SelectedIndexChanged(object sender, EventArgs e)
    {
        ViewState["judetId"] = ddlFJudet.SelectedValue;
        HttpCookie vCook = Request.Cookies["COOKTNT"];
        if (vCook != null)
        {
            Session["SESjudetId"] = ddlFJudet.SelectedValue; ;
            vCook = CriptareCookie.DecodeCookie(vCook);
            vCook["COOKjudetId"] = ddlFJudet.SelectedValue;
            vCook = CriptareCookie.EncodeCookie(vCook);
            Response.Cookies.Add(vCook);
        }
    }
    protected void tbfVolum_TextChanged(object sender, EventArgs e)
    {
        Session["SESfVol"] = tbfVolum.Text;
        HttpCookie vCook = Request.Cookies["COOKTNT"];
        if (vCook != null)
        {
            vCook = CriptareCookie.DecodeCookie(vCook);
            vCook["COOKfVol"] = tbfVolum.Text;
            vCook = CriptareCookie.EncodeCookie(vCook);
            Response.Cookies.Add(vCook);
        }
    }
    protected void tbfNrPoz_TextChanged(object sender, EventArgs e)
    {
        Session["SESfNrPoz"] = tbfNrPoz.Text;
        HttpCookie vCook = Request.Cookies["COOKTNT"];
        if (vCook != null)
        {
            vCook = CriptareCookie.DecodeCookie(vCook);
            vCook["COOKfNrPoz"] = tbfNrPoz.Text;
            vCook = CriptareCookie.EncodeCookie(vCook);
            Response.Cookies.Add(vCook);
        }
    }
    protected void tbfNume_TextChanged(object sender, EventArgs e)
    {
        Session["SESfNume"] = tbfNume.Text;
        HttpCookie vCook = Request.Cookies["COOKTNT"];
        if (vCook != null)
        {
            vCook = CriptareCookie.DecodeCookie(vCook);
            vCook["COOKfNume"] = tbfNume.Text;
            vCook = CriptareCookie.EncodeCookie(vCook);
            Response.Cookies.Add(vCook);
        }
    }
    protected void tbfLoc_TextChanged(object sender, EventArgs e)
    {
        Session["SESfLoc"] = tbfLoc.Text;
        HttpCookie vCook = Request.Cookies["COOKTNT"];
        if (vCook != null)
        {
            vCook = CriptareCookie.DecodeCookie(vCook);
            vCook["COOKfLoc"] = tbfLoc.Text;
            vCook = CriptareCookie.EncodeCookie(vCook);
            Response.Cookies.Add(vCook);
        }
    }
    protected void tbfStrada_TextChanged(object sender, EventArgs e)
    {
        Session["SESfStrada"] = tbfStrada.Text;
        HttpCookie vCook = Request.Cookies["COOKTNT"];
        if (vCook != null)
        {
            vCook = CriptareCookie.DecodeCookie(vCook);
            vCook["COOKfStrada"] = tbfStrada.Text;
            vCook = CriptareCookie.EncodeCookie(vCook);
            Response.Cookies.Add(vCook);
        }
    }
    protected void tbfNr_TextChanged(object sender, EventArgs e)
    {
        Session["SESfNr"] = tbfNr.Text;
        HttpCookie vCook = Request.Cookies["COOKTNT"];
        if (vCook != null)
        {
            vCook = CriptareCookie.DecodeCookie(vCook);
            vCook["COOKfNr"] = tbfNr.Text;
            vCook = CriptareCookie.EncodeCookie(vCook);
            Response.Cookies.Add(vCook);
        }
    }
    protected void tbfBl_TextChanged(object sender, EventArgs e)
    {
        Session["SESfBl"] = tbfBl.Text;
        HttpCookie vCook = Request.Cookies["COOKTNT"];
        if (vCook != null)
        {
            vCook = CriptareCookie.DecodeCookie(vCook);
            vCook["COOKfBl"] = tbfBl.Text;
            vCook = CriptareCookie.EncodeCookie(vCook);
            Response.Cookies.Add(vCook);
        }
    }
    protected void ddlFTip_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["SESfTip"] = ddlFTip.SelectedValue;
        HttpCookie vCook = Request.Cookies["COOKTNT"];
        if (vCook != null)
        {
            vCook = CriptareCookie.DecodeCookie(vCook);
            vCook["COOKfTip"] = ddlFTip.SelectedValue;
            vCook = CriptareCookie.EncodeCookie(vCook);
            Response.Cookies.Add(vCook);
        }
    }
    protected void ddlFOrdonareDupa_SelectedIndexChanged(object sender, EventArgs e)
    {
        HttpCookie vCook = Request.Cookies["COOKTNT"];
        if (vCook != null)
        {
            Session["SESordonareGospodarii"] = ddlFOrdonareDupa.SelectedValue;
            vCook = CriptareCookie.DecodeCookie(vCook);
            vCook["COOKordonareGospodarii"] = ddlFOrdonareDupa.SelectedValue;
            vCook = CriptareCookie.EncodeCookie(vCook);
            Response.Cookies.Add(vCook);
        }
        gvGospodarii.DataBind();
    }

    #endregion

    #region lista gospodarii

    protected void gvGospodarii_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(gvGospodarii, e, this);
        /*if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (Session["SESgospodarieId"] != null)
            {
                Label vLblGospodarieId = (Label)e.Row.FindControl("lblGospodarieId");
                if (vLblGospodarieId.Text == Session["SESgospodarieId"].ToString())
                {
                    Session["randCurent"] = e.Row.RowIndex;
                }
            }
        }*/
    }

    protected void gvGospodarii_SelectedIndexChanged(object sender, EventArgs e)
    {
        gvGospodarii.DataBind();

        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select top(1) unitateId from gospodarii where gospodarieId='" + gvGospodarii.SelectedValue.ToString() + "'";
        string vUnitateId = vCmd.ExecuteScalar().ToString();
        vCmd.CommandText = "SELECT TOP (1) unitati.judetId FROM gospodarii INNER JOIN unitati ON gospodarii.unitateId = unitati.unitateId where gospodarieId='" + gvGospodarii.SelectedValue + "'";
        string vJudetId = vCmd.ExecuteScalar().ToString();
        ManipuleazaBD.InchideConexiune(vCon);
        HttpCookie vCook = Request.Cookies["COOKTNT"];
        if (vCook != null)
        {
            Session["SESgospodarieId"] = gvGospodarii.SelectedValue;
            Session["SESgospodarieIdInitial"] = gvGospodarii.DataKeys[gvGospodarii.SelectedIndex].Values[1].ToString();

            Session["SESunitateId"] = vUnitateId;
            Session["SESjudetId"] = vJudetId;
            Session["SESrandCurent"] = gvGospodarii.SelectedIndex + (gvGospodarii.PageIndex * 10);
            vCook = CriptareCookie.DecodeCookie(vCook);
            vCook["COOKgospodarieId"] = gvGospodarii.SelectedValue.ToString();
            vCook["COOKunitateId"] = vUnitateId;
            vCook["COOKrandCurent"] = Convert.ToString(gvGospodarii.SelectedIndex + (gvGospodarii.PageIndex * 10));
            vCook["COOKjudetId"] = vJudetId;
            vCook = CriptareCookie.EncodeCookie(vCook);
            Response.Cookies.Add(vCook);
            ((MasterPage)this.Page.Master).SchimbaGospodaria();
        }
        if (gvGospodarii.SelectedValue != null)
        {
            btModifica.Visible = true;
            btSterge.Visible = true;
            btMasura112.Visible = true;
            btMasura141.Visible = true;
            dosareVanzareButton.Visible = true;
        }
        else
        {
            btModifica.Visible = false;
            btSterge.Visible = false;
            btMasura112.Visible = false;
            btMasura141.Visible = false;
            dosareVanzareButton.Visible = false;
        }

        lblEroare1.Text = "Eroare";
        lblEroare1.Visible = false;
        Panel4.Visible = false;

    }

    protected void gvGospodarii_DataBound(object sender, EventArgs e)
    {
        if (ViewState["primaData"] == null)
        {
            if (Session["SESrandCurent"] != null)
            {
                gvGospodarii.PageIndex = Convert.ToInt32(Math.Floor(Convert.ToDecimal(Session["SESrandCurent"].ToString()) / 10));
                gvGospodarii.SelectedIndex = Convert.ToInt32(Session["SESrandCurent"].ToString()) - (gvGospodarii.PageIndex * 10);
            }
            ViewState["primaData"] = 1;
        }
    }

    protected void SqlGospodarii_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        // verificam ce tip utilizator este
        //  string vTipUtilizator = ManipuleazaBD.fRezultaUnString("SELECT *  FROM utilizatori WHERE utilizatorId = '" + Session["SESutilizatorId"].ToString() + "'", "tipUtilizatorId", Convert.ToInt16(Session["SESan"]));
        //        if (vTipUtilizator == "3" || vTipUtilizator == "5")
        //        {
        //            e.Command.CommandText = @"SELECT        (SELECT        TOP (1) cnp
        //                          FROM            membri
        //                          WHERE        (unitateId = @unitateId) AND (an = @an) AND (gospodarieId = gospodarii.gospodarieId) AND (codRudenie = '1')) AS cnp, gospodarieId, unitateId, 
        //                         volum, nrPozitie, volumInt, nrPozitieInt, codSiruta, gospodarieCui, 
        //                         CASE gospodarii.tip WHEN '1' THEN 'Localnic' WHEN '2' THEN 'Străinaş' WHEN '3' THEN 'Firmă pe raza localităţii' ELSE 'Firmă străinaşă' END AS tip, 
        //                         CASE strainas WHEN 1 THEN sstrada ELSE strada END AS strada, CASE strainas WHEN 1 THEN snr ELSE nr END AS nr, nrInt, bl, sc, et, ap, codExploatatie, codUnic,
        //                          CASE strainas WHEN 1 THEN sjudet ELSE judet END AS judet, CASE strainas WHEN 1 THEN slocalitate ELSE localitate END AS localitate, persJuridica, jUnitate, 
        //                         jSubunitate, jCodFiscal, jNumeReprez, strainas, sStrada, sNr, sBl, sSc, sEtj, sAp, sJudet, sLocalitate, CASE persjuridica WHEN 0 THEN
        //                             (SELECT        TOP (1) nume
        //                               FROM            membri
        //                               WHERE        an = @an AND gospodarieId = gospodarii.gospodarieid AND codrudenie = 1) ELSE junitate END AS nume
        //FROM            gospodarii
        //WHERE        (COALESCE (volum, N'') LIKE @volum) AND (COALESCE (nrPozitie, N'') LIKE @nrPozitie) AND (COALESCE (CASE persjuridica WHEN 0 THEN
        //                             (SELECT        TOP (1) nume
        //                               FROM            membri
        //                               WHERE        an = @an AND gospodarieId = gospodarii.gospodarieid AND codrudenie = 1) ELSE junitate END, N'') LIKE '%' + @nume + '%') AND (tip LIKE @tip) AND 
        //                         (COALESCE (CASE strainas WHEN 1 THEN slocalitate ELSE localitate END, N'') LIKE '%' + @localitate + '%') AND 
        //                         (COALESCE (CASE strainas WHEN 1 THEN sstrada ELSE strada END, N'') LIKE '%' + @strada + '%') AND (COALESCE (CASE strainas WHEN 1 THEN snr ELSE nr END, 
        //                         N'') LIKE @nr) AND (an = @an) AND (COALESCE (CONVERT(nvarchar,
        //                             (SELECT        TOP (1) cnp
        //                               FROM            membri AS membri_1
        //                               WHERE        (unitateId = @unitateId) AND (an = @an) AND (gospodarieId = gospodarii.gospodarieId) AND (codRudenie = '1'))), N'') LIKE @cnp + '%') AND 
        //                         (CONVERT(nvarchar, codUnic) LIKE @codUnic + '%') AND (unitateId = @unitateId) AND (CONVERT(nvarchar, codSiruta) LIKE '%' + @codSiruta + '%') AND 
        //                         (COALESCE (CONVERT(nvarchar, gospodarieCui), N'') LIKE '%' + @codCuiPfa + '%') ORDER BY";
        //        }
        //        else
        {
            // nr. cu mod de cautare
            string nrStringFiltrareSelect = ddlModSelectNr.SelectedValue.Replace("X", tbfNr.Text);
            if (nrStringFiltrareSelect.Length == 0) nrStringFiltrareSelect = "%";


            string vSelectStrainas = "", vCautariSrainas = "";
            if (ViewState["tipAfisareStrainas"].ToString() == "2")
            {
                string strada = ReplaceDiacritics(tbfStrada.Text);
                string localitate = ReplaceDiacritics(tbfLoc.Text);

                // afisare adresa din localitate
                vSelectStrainas = " strada as strada, nr nr, judet AS judet, localitate AS localitate, bl as bl, ";
                vCautariSrainas = "(COALESCE (localitate, N'') COLLATE SQL_Latin1_General_CP1_CI_AI  LIKE '%" + localitate + "%') AND (COALESCE (strada, N'') COLLATE SQL_Latin1_General_CP1_CI_AI    LIKE '%" + strada + "%') AND (COALESCE (nr, N'') LIKE '" + nrStringFiltrareSelect + "') AND (COALESCE (bl, '') LIKE '" + ((tbfBl.Text == "") ? "%" : tbfBl.Text) + "') ";
            }
            else
            {

                string strada = ReplaceDiacritics(tbfStrada.Text);
                string localitate = ReplaceDiacritics(tbfLoc.Text);

                // afisare adresa de domiciliu
                vSelectStrainas = "CASE strainas WHEN 1 THEN sstrada ELSE strada END AS strada, CASE strainas WHEN 1 THEN snr ELSE nr END AS nr, CASE strainas WHEN 1 THEN sjudet ELSE judet END AS judet, CASE strainas WHEN 1 THEN slocalitate ELSE localitate END AS localitate, CASE strainas WHEN 1 THEN sbl ELSE bl END AS bl, ";
                vCautariSrainas = "(COALESCE (CASE strainas WHEN 1 THEN slocalitate ELSE localitate END, N'') COLLATE SQL_Latin1_General_CP1_CI_AI  LIKE '%" + localitate + "%') AND (COALESCE (CASE strainas WHEN 1 THEN sstrada ELSE strada END, N'') COLLATE SQL_Latin1_General_CP1_CI_AI  LIKE '%" + strada + "%') AND (COALESCE (CASE strainas WHEN 1 THEN snr ELSE nr END, N'') LIKE '" + nrStringFiltrareSelect + "') AND (COALESCE (CASE strainas WHEN 1 THEN sBl ELSE bl END, N'') LIKE '" + ((tbfBl.Text == "") ? "%" : tbfBl.Text) + "')  ";
            }
            //e.Command.CommandText = "SELECT TOP(100) (SELECT TOP (1) cnp FROM membri WHERE (unitateId =unitati.unitateId) AND (an = @an) AND (gospodarieId = gospodarii.gospodarieId) AND membri.codRudenie = '1' ) AS cnp, gospodarii.gospodarieId, gospodarii.unitateId, gospodarii.volum, gospodarii.nrPozitie, gospodarii.volumInt, gospodarii.nrPozitieInt, gospodarii.codSiruta, " + vSelectStrainas + " gospodarii.gospodarieCui, CASE gospodarii.tip WHEN '1' THEN 'Localnic' WHEN '2' THEN 'Străinaş' WHEN '3' THEN 'Firmă pe raza localităţii' ELSE 'Firmă străinaşă' END AS tip, gospodarii.nrInt, gospodarii.bl, gospodarii.sc, gospodarii.et, gospodarii.ap, gospodarii.codExploatatie, gospodarii.codUnic, gospodarii.persJuridica, gospodarii.jUnitate, gospodarii.jSubunitate, gospodarii.jCodFiscal, gospodarii.jNumeReprez, gospodarii.strainas, gospodarii.sStrada, gospodarii.sNr, gospodarii.sBl, gospodarii.sSc, gospodarii.sEtj, gospodarii.sAp, gospodarii.sJudet, gospodarii.sLocalitate, CASE persjuridica WHEN 0 THEN (SELECT TOP (1) nume FROM membri WHERE an = @an AND gospodarieId = gospodarii.gospodarieid AND codrudenie = 1) ELSE junitate END AS nume FROM gospodarii INNER JOIN unitati ON gospodarii.unitateId = unitati.unitateId WHERE (COALESCE (gospodarii.volum, N'') LIKE @volum) AND (COALESCE (gospodarii.nrPozitie, N'') LIKE @nrPozitie) AND (COALESCE (CASE persjuridica WHEN 0 THEN (SELECT TOP (1) nume FROM membri WHERE an = @an AND gospodarieId = gospodarii.gospodarieid AND codrudenie = 1) ELSE junitate END, N'') LIKE '%' + @nume + '%') AND (gospodarii.tip LIKE @tip) AND " + vCautariSrainas + "(gospodarii.an = @an) AND (coalesce(CONVERT (nvarchar, (SELECT TOP (1) cnp FROM membri AS membri_1 WHERE (unitateId = unitati.unitateId) AND (an = @an) AND (gospodarieId = gospodarii.gospodarieId) AND membri_1.codRudenie = '1' )),'') LIKE @cnp + '%') AND (CONVERT (nvarchar, gospodarii.codUnic) LIKE @codUnic + '%') AND (CONVERT (nvarchar, unitati.judetId) LIKE @judetId) AND (CONVERT (nvarchar, gospodarii.unitateId) LIKE @unitateId) AND (CONVERT (nvarchar, gospodarii.codSiruta) LIKE '%' + @codSiruta + '%')  AND (COALESCE(CONVERT (nvarchar, gospodarii.gospodarieCui),'') LIKE '%' + @codCuiPfa + '%') ORDER BY";

            // select nou cu conditii pe nume si cnp membru din gospodarii
            e.Command.CommandText = @"
                                        SELECT gospodarii.observatii,
                                               membruCnp AS cnp,
                                               gospodarii.gospodarieId,
                                               gospodarii.gospodarieIdInitial,
                                               gospodarii.unitateId,
                                               gospodarii.volum,
                                               gospodarii.nrPozitie,
                                               gospodarii.volumVechi,
                                               gospodarii.pozitieVeche,
                                               gospodarii.volumInt,
                                               gospodarii.nrPozitieInt,
                                               gospodarii.codSiruta,
                                               " + vSelectStrainas + @" gospodarii.gospodarieCui,
                                               CASE gospodarii.tip
                                                   WHEN '1' THEN 'Localnic'
                                                   WHEN '2' THEN 'Străinaş'
                                                   WHEN '3' THEN 'Firmă pe raza localităţii'
                                                   ELSE 'Firmă străinaşă'
                                               END AS tip,
                                               gospodarii.nrInt,
                                               gospodarii.bl,
                                               gospodarii.sc,
                                               gospodarii.et,
                                               gospodarii.ap,
                                               gospodarii.codExploatatie,
                                               gospodarii.codUnic,
                                               gospodarii.persJuridica,
                                               gospodarii.jUnitate,
                                               gospodarii.jSubunitate,
                                               gospodarii.jCodFiscal,
                                               gospodarii.jNumeReprez,
                                               gospodarii.strainas,
                                               gospodarii.sStrada,
                                               gospodarii.sNr,
                                               gospodarii.sBl,
                                               gospodarii.sSc,
                                               gospodarii.sEtj,
                                               gospodarii.sAp,
                                               gospodarii.sJudet,
                                               gospodarii.sLocalitate,
                                               gospodarii.gospodarieSat,
                                               COALESCE(STUFF('000000000000000000000000', 25-len(volum), len(volum), volum), volum) as volumOrdonat,
                                               COALESCE(STUFF('000000000000000000000000', 25-len(nrPozitie), len(nrPozitie), nrPozitie), nrPozitie) as nrPozitieOrdonat,
                                               CASE persjuridica
                                                   WHEN 0 THEN membruNume
                                                   ELSE junitate
                                               END AS nume
                                        FROM gospodarii
                                        INNER JOIN unitati ON gospodarii.unitateId = unitati.unitateId
                                        WHERE(CONVERT(nvarchar,
                                                        unitati.judetId) LIKE @judetId)
                                          AND(CONVERT(nvarchar,
                                                        gospodarii.unitateId) LIKE @unitateId)
                                          AND(gospodarii.an = @an)";
            // adaug la conditii doar daca este nevoie
            string nrStringFiltrareSelectVolum = ddlModSelectVol.SelectedValue.Replace("X", tbfVolum.Text);

            if (tbfVolum.Text != "")
                //e.Command.CommandText += " AND (COALESCE (gospodarii.volum, N'') = @volum)";
                e.Command.CommandText += " AND (COALESCE (gospodarii.volum, N'') LIKE '" + nrStringFiltrareSelectVolum + "')";
            if (tbfNrPoz.Text != "")
                e.Command.CommandText += " AND (COALESCE (gospodarii.nrPozitie, N'') = @nrPozitie)";
            if (cautaVolumVechiTextBox.Text != "")
                e.Command.CommandText += " AND (COALESCE (gospodarii.volumVechi, N'') = @volumVechi)";
            if (cautaPozitieVecheTextBox.Text != "")
                e.Command.CommandText += " AND (COALESCE (gospodarii.pozitieVeche, N'') = @pozitieVeche)";
            if (tbfNume.Text != "")
            {
                //string nume = Regex.Replace(tbfNume.Text, @"ș", "s");s
                string nume = ReplaceDiacritics(tbfNume.Text);
                e.Command.CommandText += " AND (COALESCE (CASE persjuridica WHEN 0 THEN membruNume ELSE junitate END, N'') COLLATE SQL_Latin1_General_CP1_CI_AI LIKE '%" + nume + "%')  ";
            }
            if (ddlFTip.SelectedValue != "%")
                e.Command.CommandText += " AND (gospodarii.tip = @tip)";
            if (tbfLoc.Text != "" || tbfStrada.Text != "" || tbfNr.Text != "" || tbfBl.Text != "")
                e.Command.CommandText += " AND " + vCautariSrainas;
            if (tbfCnp.Text != "")
                e.Command.CommandText += " AND (coalesce(CONVERT (nvarchar, membruCnp),'') LIKE @cnp + '%')";
            if (tbfCodApia.Text != "")
                e.Command.CommandText += " AND (CONVERT (nvarchar, gospodarii.codUnic) LIKE @codUnic + '%')";
            if (tbFSat.Text != "")
                e.Command.CommandText += " AND (coalesce(CONVERT (nvarchar, gospodarii.gospodarieSat), N'') COLLATE Latin1_General_CI_AI LIKE N'%" + tbFSat.Text + "%' COLLATE Latin1_General_CI_AI)";
            if (tbfLoc.Text != "")
                e.Command.CommandText += " AND (coalesce(CONVERT (nvarchar, gospodarii.localitate), N'') COLLATE Latin1_General_CI_AI LIKE N'%" + tbfLoc.Text + "%' COLLATE Latin1_General_CI_AI)";
            if (tbfSiruta.Text != "")
                e.Command.CommandText += "(CONVERT (nvarchar, gospodarii.codSiruta) LIKE '%' + @codSiruta + '%')";
            if (tbfCui.Text != "")
                e.Command.CommandText += " AND (COALESCE(CONVERT (nvarchar, gospodarii.gospodarieCui),'') LIKE '%' + @codCuiPfa + '%')";
            // daca ultimele 4 caractere sun "AND " le scot
            if (e.Command.CommandText.Substring(e.Command.CommandText.Length - 4, 4) == "AND ")
                e.Command.CommandText = e.Command.CommandText.Remove(e.Command.CommandText.Length - 4, 4);
            e.Command.CommandText += " ORDER BY ";
        }
        switch (ddlFOrdonareDupa.SelectedValue)
        {
            case "0":
                e.Command.CommandText += " volumOrdonat, nrPozitieOrdonat "; //gospodarii.volumInt, gospodarii.nrPozitieInt";
                break;
            case "1":
                e.Command.CommandText += " localitate, strada, nr";
                break;
            case "2":
                e.Command.CommandText += " nume";
                break;
            case "3":
                e.Command.CommandText += "  gospodarii.gospodarieId asc";
                break;
            case "4":
                e.Command.CommandText += "  gospodarii.gospodarieId DESC";
                break;
        }


    }

    private static string ReplaceDiacritics(string input)
    {
        input = Regex.Replace(input, @"ă", "a");
        input = Regex.Replace(input, @"ș", "s");
        input = Regex.Replace(input, @"î", "i");
        input = Regex.Replace(input, @"â", "a");
        input = Regex.Replace(input, @"ț", "t");
        input = Regex.Replace(input, @"Ă", "a");
        input = Regex.Replace(input, @"Ș", "s");
        input = Regex.Replace(input, @"Ț", "t");
        input = Regex.Replace(input, @"Â", "a");
        input = Regex.Replace(input, @"Î", "i");
        return input.ToLower();
    }

    #endregion

    #region butoane principale

    protected void btAdauga_Click(object sender, EventArgs e)
    {
        ViewState["tip"] = "a";

        GolireCampuriAdaugare();

        pnAdaugaGospodarii.Visible = true;
        pnListaGospodarii.Visible = false;
        pnAdaugaMembru.Visible = true;
        //   btSalveazaGospodarie.Text = "adaugă o gospodărie";
        lblEroare.Visible = false;
        lblEroare1.Visible = false;
        // pnPersoanaJuridica.Visible = false;
        pnStrainas.Visible = false;
        pnSedii.Visible = true;
        pnAdaugaMembru.Visible = true;
        tbVolum.Focus();

        ClearControls();
        gvSedii.DataSource = null;
        gvSedii.DataBind();
        FillDropDownLists();
        btAdaugaSediu.Visible = ViewState["tip"] != "a";
        btModificaSediu.Visible = false;
        btStergeSediu.Visible = false;
        alegeAdaugareAdresaSediuCheckBox.Checked = false;
    }

    private void GolireCampuriAdaugare()
    {
        // cautam ultimul volum introdus
        string vUltimulVolum = ManipuleazaBD.fRezultaUnString("SELECT COALESCE(max(volumInt),'1') as x FROM gospodarii WHERE unitateId ='" + Session["SESunitateId"].ToString() + "' ", "x", Convert.ToInt16(Session["SESan"]));
        // aflam ultima valoare nrPozitie pt volumul gasit mai sus
        string vUltimaPozitie = (Convert.ToInt32(ManipuleazaBD.fRezultaUnString("SELECT COALESCE(max(nrPozitieInt),'0') as x FROM gospodarii WHERE unitateId ='" + Session["SESunitateId"].ToString() + "' AND volumInt = '" + vUltimulVolum + "'", "x", Convert.ToInt16(Session["SESan"]))) + 1).ToString();
        tbAp.Text = "";
        tbBl.Text = "";
        tbCodExpl.Text = "";
        tbCodSiruta.Text = "";
        tbCodUnic.Text = "";
        tbMemDataN.Text = "";
        tbEt.Text = "";
        tbJCodFiscal.Text = "";
        tbJNumeReprez.Text = "";
        // luam judetul din unitati
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "SELECT judete.judetDenumire FROM unitati INNER JOIN judete ON unitati.judetId = judete.judetId WHERE (unitati.unitateId = '" + Session["SESunitateId"].ToString() + "')";
        tbJudet.Text = vCmd.ExecuteScalar().ToString();
        ManipuleazaBD.InchideConexiune(vCon);
        tbJUnitate.Text = "";
        tbJSubUnitate.Text = "";
        ddlLoc.DataBind();
        ddlLoc.SelectedIndex = -1;
        tbSat.Text = "";
        tbMemCNP.Text = "";
        tbMemNume.Text = "";
        ddlMemSex.SelectedValue = "1";
        tbMemMentiuni.Text = "";
        tbNr.Text = "";
        tbNrInt.Text = "";
        tbNrPoz.Text = vUltimaPozitie;
        tbSAp.Text = "";
        tbSBl.Text = "";
        tbSc.Text = "";
        tbSEt.Text = "";
        tbSJud.Text = "";
        tbSLoc.Text = "";
        tbSNr.Text = "";
        tbSSc.Text = "";
        tbSStrada.Text = "";
        tbStrada.Text = "";
        ddlTip.SelectedValue = "1";
        tbObservatii.Text = "";
        tbVolum.Text = vUltimulVolum;
        cbPersJuridica.Checked = false;
        cbStrainas.Checked = false;
    }

    protected void btModifica_Click(object sender, EventArgs e)
    {
        ViewState["tip"] = "m";
        // populeaza campuri modificare
        PopuleazaCampuriModificare();
        pnAdaugaGospodarii.Visible = true;
        pnListaGospodarii.Visible = false;
        pnAdaugaMembru.Visible = false;
        // btSalveazaGospodarie.Text = "modifică gospodărie";
        lbTipOperatie.Text = "MODIFICĂ GOSPODĂRIE";
        lblEroare.Visible = false;
        lblEroare1.Visible = false;
        tbVolum.Focus();

        ClearControls();
        LoadGvSedii();
        FillDropDownLists();
        pnSedii.Visible = true;
        btAdaugaSediu.Visible = true;
        btModificaSediu.Visible = false;
        btStergeSediu.Visible = false;
        alegeAdaugareAdresaSediuCheckBox.Checked = false;
        alegeAdaugareAdresaSediuCheckBox.Enabled = true;
    }

    private void PopuleazaCampuriModificare()
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        string vInterogare = "select * from gospodarii where gospodarieId='" + gvGospodarii.SelectedValue + "'";
        vCmd.CommandText = vInterogare;
        SqlDataReader vTabel = vCmd.ExecuteReader();
        if (vTabel.Read())
        {
            tbVolum.Text = vTabel["volum"].ToString();
            tbNrPoz.Text = vTabel["nrPozitie"].ToString();
            if (string.IsNullOrEmpty(vTabel["volumVechi"].ToString()))
            {
                VolumVechiTextBox.Text = vTabel["volum"].ToString();
            }
            else
            {
                VolumVechiTextBox.Text = vTabel["volumVechi"].ToString();
            }
            if (string.IsNullOrEmpty(vTabel["pozitieVeche"].ToString()))
            {
                pozitieVecheTextBox.Text = vTabel["nrPozitie"].ToString();
            }
            else
            {
                pozitieVecheTextBox.Text = vTabel["pozitieVeche"].ToString();
            }
            tbCodSiruta.Text = vTabel["codSiruta"].ToString();
            ddlTip.SelectedValue = vTabel["tip"].ToString();
            tbStrada.Text = vTabel["strada"].ToString();
            tbNr.Text = vTabel["nr"].ToString();
            tbNrInt.Text = vTabel["nrInt"].ToString();
            tbBl.Text = vTabel["bl"].ToString();
            tbSc.Text = vTabel["sc"].ToString();
            tbEt.Text = vTabel["et"].ToString();
            tbAp.Text = vTabel["ap"].ToString();
            tbCodExpl.Text = vTabel["codExploatatie"].ToString();
            tbCodUnic.Text = vTabel["codUnic"].ToString();
            tbJudet.Text = vTabel["judet"].ToString();
            try
            {
                ddlLoc.DataBind();
                foreach (ListItem vLocalitateDdl in ddlLoc.Items)
                {
                    if (vLocalitateDdl.Text == vTabel["localitate"].ToString())
                    {
                        ddlLoc.SelectedValue = vLocalitateDdl.Value;
                        break;
                    }
                }
            }
            catch { }
            cbPersJuridica.Checked = Convert.ToBoolean(vTabel["persJuridica"]);
            cbEraPersoanaJuridica.Checked = Convert.ToBoolean(vTabel["persJuridica"]);
            //if (cbPersJuridica.Checked)
            //    pnPersoanaJuridica.Visible = true;
            //else pnPersoanaJuridica.Visible = false;
            tbJUnitate.Text = vTabel["jUnitate"].ToString();
            tbJSubUnitate.Text = vTabel["jSubunitate"].ToString();
            tbJCodFiscal.Text = vTabel["jCodFiscal"].ToString();
            tbJNumeReprez.Text = vTabel["jNumeReprez"].ToString();
            cbStrainas.Checked = Convert.ToBoolean(vTabel["strainas"]);
            if (cbStrainas.Checked)
                pnStrainas.Visible = true;
            else pnStrainas.Visible = false;
            tbSStrada.Text = vTabel["sStrada"].ToString();
            tbSNr.Text = vTabel["sNr"].ToString();
            tbSBl.Text = vTabel["sBl"].ToString();
            tbSSc.Text = vTabel["sSc"].ToString();
            tbSEt.Text = vTabel["sEtj"].ToString();
            tbSAp.Text = vTabel["sAp"].ToString();
            tbSJud.Text = vTabel["sJudet"].ToString();
            tbSLoc.Text = vTabel["sLocalitate"].ToString();
            tbMemCui.Text = vTabel["gospodarieCui"].ToString();
            tbSat.Text = vTabel["gospodarieSat"].ToString();
            tbObservatii.Text = vTabel["observatii"].ToString();
            taraStrainasTextBox.Text = vTabel["taraStrainas"].ToString();
            numarRolNominalTextBox.Text = vTabel["nrRolNominal"].ToString();
        }
        ManipuleazaBD.InchideConexiune(vCon);
    }

    protected void btSterge_Click(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlTransaction vTranz = ManipuleazaBD.DeschideTranzactie(vCon);
        SqlCommand vCmd = new SqlCommand();
        SqlCommand vCmd1 = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.Transaction = vTranz;
        vCmd1.Connection = vCon;
        vCmd1.Transaction = vTranz;
        try
        {
            int vCountCapitole = 0;
            //daca gospodaria este in istoric nu permite stergerea
            //vCmd.CommandText = "select count(*) as cnt FROM istoricParcela where deLa=" + gvGospodarii.SelectedValue + ""; 
            //vCountCapitole += Convert.ToInt32(vCmd.ExecuteScalar());

            vCmd.CommandText = "select count(*) from parcele where gospodarieId='" + gvGospodarii.SelectedValue + "'";
            vCountCapitole += Convert.ToInt32(vCmd.ExecuteScalar());
            vCmd.CommandText = "select count(*) from paduri where gospodarieId='" + gvGospodarii.SelectedValue + "'";
            vCountCapitole += Convert.ToInt32(vCmd.ExecuteScalar());
            vCmd.CommandText = "select coalesce(sum(col1+col2+col3+col4+col5+col6+col7+col8),0) from capitole where gospodarieId='" + gvGospodarii.SelectedValue + "'";
            vCountCapitole += Convert.ToInt32(vCmd.ExecuteScalar());
            if (vCountCapitole == 0)
            {
                vCmd1.CommandText = "select * from gospodarii where gospodarieId='" + gvGospodarii.SelectedValue + "'";
                string vValoareVeche = "";
                SqlDataReader vTabelGospodarii = vCmd1.ExecuteReader();
                if (vTabelGospodarii.Read())
                    vValoareVeche = "Volum " + vTabelGospodarii["volum"].ToString() + "; nr. pozitie " + vTabelGospodarii["nrPozitie"].ToString();
                vTabelGospodarii.Close();
                vCmd1.CommandText = "select * from membri where gospodarieId='" + gvGospodarii.SelectedValue + "' order by codRudenie";
                SqlDataReader vTabelMembri = vCmd1.ExecuteReader();
                if (vTabelMembri.Read())
                    vValoareVeche += "; cap de gospodarie " + vTabelMembri["nume"].ToString();
                vTabelMembri.Close();

                vCmd.CommandText = "delete from membri where gospodarieId='" + gvGospodarii.SelectedValue + "'";
                vCmd.ExecuteNonQuery();
                vCmd.CommandText = "delete from capitole where gospodarieId='" + gvGospodarii.SelectedValue + "'";
                vCmd.ExecuteNonQuery();
                vCmd.CommandText = "delete from istoricParcela where deLa='" + gvGospodarii.SelectedValue + "'";
                vCmd.ExecuteNonQuery();
                vTranz.Commit();

                Address adreseFirme = new Address();

                adreseFirme.GospodarieId = Convert.ToInt32(gvGospodarii.SelectedValue);

                AddressServices.ConnectionYear = Convert.ToInt16(Session["SESan"]);
                List<Address> listaAdrese = adreseFirme.GetAddresByGospodarie();

                foreach (Address adresa in listaAdrese)
                {
                    AddressServices.ConnectionYear = Convert.ToInt16(Session["SESan"]);
                    adresa.Delete();
                }

                SqlConnection vCon2 = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
                SqlCommand vCmd2 = new SqlCommand();
                vCmd2.Connection = vCon2;
                vCmd2.CommandText = "delete from gospodarii where gospodarieId='" + gvGospodarii.SelectedValue + "'";
                vCmd2.ExecuteNonQuery();
                vCon2.Close();

                lblEroare1.Visible = false;
                gvGospodarii.DataBind();

                ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "gospodarii+membri", "stergere gospodarie", vValoareVeche, vCmd, Convert.ToInt64(Session["SESgospodarieId"]), 4);

            }
            else
            {
                lblEroare1.Visible = true;
                lblEroare1.Text = "Nu poate fi stearsa o gospodarie care are introduse capitole sau o gospodarie care a avut capitole dar au fost mutate/transferate (vanzare, donatie, etc.)";
            }
        }
        catch { vTranz.Rollback(); }
        finally { ManipuleazaBD.InchideConexiune(vCon); }

        gvGospodarii.SelectedIndex = -1;
        Session["SESgospodarieId"] = "";
        ((MasterPage)this.Page.Master).SchimbaGospodaria();

        btModifica.Visible = false;
        btSterge.Visible = false;
        btMasura112.Visible = false;
        btMasura141.Visible = false;
        dosareVanzareButton.Visible = false;
    }

    protected void btTiparire_Click(object sender, EventArgs e)
    {
        string nrStringFiltrareSelect = ddlModSelectNr.SelectedValue.Replace("X", tbfNr.Text);
        ResponseHelper.Redirect("~/printListaGospodarii.aspx?volum=" + Convert.ToString((tbfVolum.Text == "") ? "%" : tbfVolum.Text) + "&nrPozitie=" + Convert.ToString((tbfNrPoz.Text == "") ? "%" : tbfNrPoz.Text) + "&tip=" + ddlFTip.SelectedValue + "&localitate=" + Convert.ToString((tbfLoc.Text == "") ? "%" : tbfLoc.Text) + "&strada=" + Convert.ToString((tbfStrada.Text == "") ? "%" : tbfStrada.Text) + "&nr=" + Convert.ToString((tbfNr.Text == "") ? "%" : nrStringFiltrareSelect) + "&nume=" + Convert.ToString((tbfNume.Text == "") ? "%" : tbfNume.Text) + "&ordonare=" + ddlFOrdonareDupa.SelectedValue, "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
    }

    #endregion

    #region panel adauga modifica

    protected void tbMemCNP_TextChanged(object sender, EventArgs e)
    {
        CNP vCnp = new CNP(tbMemCNP.Text);
        if (vCnp.IsValidB())
        {
            tbMemDataN.Text = vCnp.GetDataNasterii().ToString("dd.MM.yyyy");
            ddlMemSex.SelectedValue = vCnp.GetSex().ToString();
        }
        ddlMemSex.Focus();
    }

    protected void tbNr_TextChanged(object sender, EventArgs e)
    {
        //try { tbNrInt.Text = Convert.ToInt32(tbNr.Text).ToString(); }
        //catch { }
        //tbNrInt.Focus();
    }

    protected void ddlTip_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlTip.SelectedValue == "1")
        {
            cbPersJuridica.Checked = false;
            cbStrainas.Checked = false;
            //   pnPersoanaJuridica.Visible = false;
            pnStrainas.Visible = false;
            if (ViewState["tip"] != null)
                if (ViewState["tip"].ToString() == "a")
                    pnAdaugaMembru.Visible = true;
        }
        else if (ddlTip.SelectedValue == "2")
        {
            cbPersJuridica.Checked = false;
            cbStrainas.Checked = true;
            //    pnPersoanaJuridica.Visible = false;
            pnStrainas.Visible = true;
            if (ViewState["tip"] != null)
                if (ViewState["tip"].ToString() == "a")
                    pnAdaugaMembru.Visible = true;
        }
        else if (ddlTip.SelectedValue == "3")
        {
            cbPersJuridica.Checked = true;
            cbStrainas.Checked = false;
            //  pnPersoanaJuridica.Visible = true;
            pnStrainas.Visible = false;
            if (ViewState["tip"] != null)
                if (ViewState["tip"].ToString() == "a")
                    pnAdaugaMembru.Visible = false;

        }
        else if (ddlTip.SelectedValue == "4")
        {
            cbPersJuridica.Checked = true;
            cbStrainas.Checked = true;
            //  pnPersoanaJuridica.Visible = true;
            pnStrainas.Visible = true;
            if (ViewState["tip"] != null)
                if (ViewState["tip"].ToString() == "a")
                    pnAdaugaMembru.Visible = false;

        }
        tbCodSiruta.Focus();

        pnSedii.Visible = true;

        ClearControls();
    }

    protected void cbPersJuridica_CheckedChanged(object sender, EventArgs e)
    {
        if (cbPersJuridica.Checked)
        {
            //   pnPersoanaJuridica.Visible = true;
            if (cbStrainas.Checked)
                ddlTip.SelectedValue = "4";
            else ddlTip.SelectedValue = "3";
        }
        else
        {
            // pnPersoanaJuridica.Visible = false;
            if (cbStrainas.Checked)
                ddlTip.SelectedValue = "2";
            else ddlTip.SelectedValue = "1";
        }
        tbVolum.Focus();

        //   pnSedii.Visible = ddlTip.SelectedIndex >= 2;
        ClearControls();

    }

    protected void cbStrainas_CheckedChanged(object sender, EventArgs e)
    {
        if (cbStrainas.Checked)
        {
            pnStrainas.Visible = true;
            if (cbPersJuridica.Checked)
                ddlTip.SelectedValue = "4";
            else ddlTip.SelectedValue = "2";
        }
        else
        {
            pnStrainas.Visible = false;
            if (cbPersJuridica.Checked)
                ddlTip.SelectedValue = "3";
            else ddlTip.SelectedValue = "1";
        }
        tbVolum.Focus();
    }

    protected string ScoateNumere(string pScoateNumar)
    {
        switch (pScoateNumar)
        {
            default:
                break;
            case "I":
                pScoateNumar = "1";
                break;
            case "II":
                pScoateNumar = "2";
                break;
            case "III":
                pScoateNumar = "3";
                break;
            case "IV":
                pScoateNumar = "4";
                break;
            case "V":
                pScoateNumar = "5";
                break;
            case "VI":
                pScoateNumar = "6";
                break;
            case "VII":
                pScoateNumar = "7";
                break;
            case "VIII":
                pScoateNumar = "8";
                break;
            case "IX":
                pScoateNumar = "9";
                break;
            case "X":
                pScoateNumar = "10";
                break;
            case "XI":
                pScoateNumar = "11";
                break;
            case "XII":
                pScoateNumar = "12";
                break;
            case "XIII":
                pScoateNumar = "13";
                break;
            case "XIV":
                pScoateNumar = "14";
                break;
            case "XV":
                pScoateNumar = "15";
                break;
            case "XVI":
                pScoateNumar = "16";
                break;
            case "XVII":
                pScoateNumar = "17";
                break;
            case "XVIII":
                pScoateNumar = "18";
                break;
            case "XIX":
                pScoateNumar = "19";
                break;
            case "XX":
                pScoateNumar = "20";
                break;
            case "XXI":
                pScoateNumar = "21";
                break;
            case "XXII":
                pScoateNumar = "22";
                break;
            case "XXIII":
                pScoateNumar = "23";
                break;
            case "XXIV":
                pScoateNumar = "24";
                break;
            case "XXV":
                pScoateNumar = "25";
                break;
            case "XXVI":
                pScoateNumar = "26";
                break;
            case "XXVII":
                pScoateNumar = "27";
                break;
            case "XXVIII":
                pScoateNumar = "28";
                break;
            case "XXIX":
                pScoateNumar = "29";
                break;
            case "XXX":
                pScoateNumar = "30";
                break;
            case "XXXI":
                pScoateNumar = "31";
                break;
            case "XXXII":
                pScoateNumar = "32";
                break;
            case "XXXIII":
                pScoateNumar = "33";
                break;
            case "XXXIV":
                pScoateNumar = "34";
                break;
            case "XXXV":
                pScoateNumar = "35";
                break;
            case "XXXVI":
                pScoateNumar = "36";
                break;
            case "XXXVII":
                pScoateNumar = "37";
                break;
            case "XXXVIII":
                pScoateNumar = "38";
                break;
            case "XXXIX":
                pScoateNumar = "39";
                break;
            case "XL":
                pScoateNumar = "40";
                break;
            case "XLI":
                pScoateNumar = "41";
                break;
            case "XLII":
                pScoateNumar = "42";
                break;
            case "XLIII":
                pScoateNumar = "43";
                break;
            case "XLIV":
                pScoateNumar = "44";
                break;
            case "XLV":
                pScoateNumar = "45";
                break;
            case "XLVI":
                pScoateNumar = "46";
                break;
            case "XLVII":
                pScoateNumar = "47";
                break;
            case "XLVIII":
                pScoateNumar = "48";
                break;
            case "XLIX":
                pScoateNumar = "49";
                break;
            case "L":
                pScoateNumar = "50";
                break;
            case "LI":
                pScoateNumar = "51";
                break;
            case "LII":
                pScoateNumar = "52";
                break;
            case "LIII":
                pScoateNumar = "53";
                break;
            case "LIV":
                pScoateNumar = "54";
                break;
            case "LV":
                pScoateNumar = "55";
                break;
            case "LVI":
                pScoateNumar = "56";
                break;
            case "LVII":
                pScoateNumar = "57";
                break;
            case "LVIII":
                pScoateNumar = "58";
                break;
            case "LIX":
                pScoateNumar = "59";
                break;
            case "LX":
                pScoateNumar = "60";
                break;
            case "LXI":
                pScoateNumar = "61";
                break;
            case "LXII":
                pScoateNumar = "62";
                break;
            case "LXIII":
                pScoateNumar = "63";
                break;
            case "LXIV":
                pScoateNumar = "64";
                break;
            case "LXV":
                pScoateNumar = "65";
                break;
            case "LXVI":
                pScoateNumar = "66";
                break;
            case "LXVII":
                pScoateNumar = "67";
                break;
            case "LXVIII":
                pScoateNumar = "68";
                break;
            case "LXIX":
                pScoateNumar = "69";
                break;
            case "LXX":
                pScoateNumar = "70";
                break;
            case "LXXI":
                pScoateNumar = "71";
                break;
            case "LXXII":
                pScoateNumar = "72";
                break;
            case "LXXIII":
                pScoateNumar = "73";
                break;
            case "LXXIV":
                pScoateNumar = "74";
                break;
            case "LXXV":
                pScoateNumar = "75";
                break;
            case "LXXVI":
                pScoateNumar = "76";
                break;
            case "LXXVII":
                pScoateNumar = "77";
                break;
            case "LXXVIII":
                pScoateNumar = "78";
                break;
            case "LXXIX":
                pScoateNumar = "79";
                break;
            case "LXXX":
                pScoateNumar = "80";
                break;
            case "LXXXI":
                pScoateNumar = "81";
                break;
            case "LXXXII":
                pScoateNumar = "82";
                break;
            case "LXXXIII":
                pScoateNumar = "83";
                break;
            case "LXXXIV":
                pScoateNumar = "84";
                break;
            case "LXXXV":
                pScoateNumar = "85";
                break;
            case "LXXXVI":
                pScoateNumar = "86";
                break;
            case "LXXXVII":
                pScoateNumar = "87";
                break;
            case "LXXXVIII":
                pScoateNumar = "88";
                break;
            case "LXXXIX":
                pScoateNumar = "89";
                break;
            case "XC":
                pScoateNumar = "90";
                break;
            case "XCI":
                pScoateNumar = "91";
                break;
            case "XCII":
                pScoateNumar = "92";
                break;
            case "XCIII":
                pScoateNumar = "93";
                break;
            case "XCIV":
                pScoateNumar = "94";
                break;
            case "XCV":
                pScoateNumar = "95";
                break;
            case "XCVI":
                pScoateNumar = "96";
                break;
            case "XCVII":
                pScoateNumar = "97";
                break;
            case "XCVIII":
                pScoateNumar = "98";
                break;
            case "XCIX":
                pScoateNumar = "99";
                break;
            case "C":
                pScoateNumar = "100";
                break;
        }
        int vContor = 0;
        int vLungime = pScoateNumar.Length;
        string vNumar = "";
        while (vContor < vLungime)
        {
            string vCaracter = pScoateNumar[vContor].ToString();
            try
            {
                vNumar += Convert.ToInt16(vCaracter).ToString();
            }
            catch { }
            vContor++;
        }
        try
        {
            vNumar = Convert.ToInt16(vNumar).ToString();
        }
        catch { vNumar = "0"; }
        return vNumar; ;
    }

    protected void tbVolum_TextChanged(object sender, EventArgs e)
    {

        // aflam ultima valoare nrPozitie pt volumul gasit mai sus
        string vUltimaPozitie = "";
        try
        {
            vUltimaPozitie = (Convert.ToInt32(ManipuleazaBD.fRezultaUnString("SELECT max(nrPozitieInt) as x FROM gospodarii WHERE unitateId ='" + Session["SESunitateId"].ToString() + "' AND volumInt = '" + tbVolum.Text + "'", "x", Convert.ToInt16(Session["SESan"]))) + 1).ToString();
        }
        catch { }
        tbNrPoz.Text = vUltimaPozitie;

    }

    protected void ScrieInCapitolul3()
    {
        // scriem in cap 3 daca s-a modificat statutul de persoana juridica in/din fizica
        // daca a fost bifata si nu mai este sau invers
        if ((cbEraPersoanaJuridica.Checked && !cbPersJuridica.Checked) || (!cbEraPersoanaJuridica.Checked && cbPersJuridica.Checked))
        {
            // citim toate atribuirile de mod utilizare pentru gospodarie in care a primit parcele
            // le introducem intr-o lista de liste de stringuri
            List<string> vCampuri = new List<string> { "gospodarieId", "catreGospodarieId", "anul", "unitateId", "parcelaId", "c3Rand", "catreC3Rand", "c3Ha", "c3Ari" };
            List<List<string>> vInregistrariModUtilizare = ManipuleazaBD.fRezultaListaStringuri("SELECT * FROM parceleModUtilizare WHERE catreGospodarieId ='" + Session["SESgospodarieId"].ToString() + "' AND anul = '" + Session["SESan"].ToString() + "'", vCampuri, Convert.ToInt16(Session["SESan"]));
            // daca a devenit pers juridica atunci adunam valorile gasite la fiecare din gospodariile de la care a primit, la campul 16 din cap3
            // daca nu mai este persoana juridica scadem valorile
            string vSemn = "";
            if (cbPersJuridica.Checked) vSemn = "+";
            else vSemn = "-";
            foreach (List<string> vInregistrareModUtilizare in vInregistrariModUtilizare)
            {
                // scoatem valoare existenta pentru campul 16 din gospodaria care a dat
                List<string> vCampuriHaAri = new List<string> { "col1", "col2", "capitolId" };
                List<string> vValoareExistenta = ManipuleazaBD.fRezultaUnString("SELECT col1, col2, capitolId FROM capitole WHERE an='" + Session["SESan"].ToString() + "'  AND gospodarieId ='" + vInregistrareModUtilizare[0] + "' AND codRand ='16' AND codCapitol = '3' ", vCampuriHaAri, Convert.ToInt16(Session["SESan"]));
                decimal vSuprafataInitiala = Convert.ToDecimal(vValoareExistenta[0].Replace('.', ',')) * 100 + Convert.ToDecimal(vValoareExistenta[1].Replace('.', ','));
                decimal vSuprafataDeAdaugat = Convert.ToDecimal(vInregistrareModUtilizare[7]) * 100 + Convert.ToDecimal(vInregistrareModUtilizare[8].Replace('.', ','));
                decimal vSuprafataDeScris = 0;
                // aplicam semnul
                if (vSemn == "+")
                    vSuprafataDeScris = vSuprafataInitiala + vSuprafataDeAdaugat;
                else
                    vSuprafataDeScris = vSuprafataInitiala - vSuprafataDeAdaugat;
                if (vSuprafataDeScris < 0) vSuprafataDeScris = 0;
                string[] vSuprafataDeScrisString = CalculeazaAriHa("0", "0", vSuprafataDeScris.ToString(), "0");
                ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vSuprafataDeScrisString[0] + "', col2 = CONVERT(decimal(18,4),'" + vSuprafataDeScrisString[1].Replace(',', '.') + "') WHERE (capitolId = '" + vValoareExistenta[2] + "')", Convert.ToInt16(Session["SESan"]));
            }
        }
    }

    protected string[] CalculeazaAriHa(string pHa1, string pHa2, string pAri1, string pAri2)
    {
        string[] vValori = { "0", "0" };
        if (pHa1 == "") pHa1 = "0";
        if (pHa2 == "") pHa2 = "0";
        if (pAri1 == "") pAri1 = "0";
        if (pAri2 == "") pAri2 = "0";
        decimal vValoare = Convert.ToDecimal(pHa1) * 100 + Convert.ToDecimal(pHa2) * 100 + Convert.ToDecimal(pAri1) + Convert.ToDecimal(pAri2);
        vValori[1] = (vValoare % 100).ToString();
        vValori[0] = Convert.ToInt64((vValoare - (vValoare % 100)) / 100).ToString();
        return vValori;
    }

    protected void ddlLoc_PreRender(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;

        vCmd.CommandText = "SELECT        localitateCodSiRuta FROM            localitatiComuna WHERE        (unitateId = " + Session["SESunitateId"].ToString() + ") AND (localitateId = " + ddlLoc.SelectedValue + ")";
        //folosesc try pentru ca este posibil ca sa se afle null in cod si ruta
        try { tbCodSiruta.Text = vCmd.ExecuteScalar().ToString(); }
        catch { }

        ManipuleazaBD.InchideConexiune(vCon);
    }

    #endregion

    #region butoane salveaza inapoi

    protected void btSalveazaGospodarie_Click(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        // daca s-a schimbat in / in juridica scriem in cap 3 rand 16 // doar daca are bifata in unitati
        string vAre3 = ManipuleazaBD.fRezultaUnString("select top(1) unitateAdaugaDinC2BC3 from unitati where unitateId='" + Session["SESunitateId"].ToString() + "'", "unitateAdaugaDinC2BC3", Convert.ToInt16(Session["SESan"]));
        if (vAre3 == "True" && ViewState["tip"].ToString() == "m") ScrieInCapitolul3();
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlTransaction vTranz = ManipuleazaBD.DeschideTranzactie(vCon);
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.Transaction = vTranz;
        SqlCommand vCmd1 = new SqlCommand();
        vCmd1.Connection = vCon;
        vCmd1.Transaction = vTranz;
        Int64 vGospodarieId = 0;

        if (ViewState["tip"].ToString() == "a")
        {
            bool gospodarieSalvata = false;
            // verific daca exista o gospodarie la volumul si numarul introdus
            vCmd.CommandText = "select count(*) from gospodarii where unitateId='" + Session["SESunitateId"].ToString() + "' and volum='" + tbVolum.Text + "' and nrPozitie='" + tbNrPoz.Text + "' AND an='" + Session["SESan"].ToString() + "'";
            int vValidareVolum = Convert.ToInt32(vCmd.ExecuteScalar());
            if (vValidareVolum > 0)
            { lblEroare.Visible = true; lblEroare.Text = "La volumul " + tbVolum.Text + ", poziţia " + tbNrPoz.Text + " există deja o gospodărie"; return; }
            else lblEroare.Visible = false;
            // verific daca data nasterii este valida
            if (!cbPersJuridica.Checked && tbMemDataN.Text != "")
                try { Convert.ToDateTime(tbMemDataN.Text); lblEroare.Visible = false; }
                catch { lblEroare.Text = "Câmpul data naşterii este invalid"; lblEroare.Visible = true; return; }
            try
            {
                // adaug gospodaria iar id-ul gospodariei nou adaugate il tin in vGospodarieId

                vCmd.CommandText = @"INSERT INTO [gospodarii] ([unitateId], [volum], [nrPozitie], [codSiruta], [tip], [strada], [nr], [nrInt], [bl], [sc], [et], [ap], [codExploatatie], [codUnic], [judet], [localitate], [persJuridica], [jUnitate], [jSubunitate], [jCodFiscal], [jNumeReprez], [strainas], [sStrada], [sNr], [sBl], [sSc], [sEtj], [sAp], [sJudet], [sLocalitate], [dataModificare], [oraModificare], [minutModificare], [volumInt], [nrPozitieInt], [an], [gospodarieCui], [gospodarieSat], [observatii], [membruNume], [membruCnp],[taraStrainas],[nrRolNominal]) VALUES ('" + Session["SESunitateId"].ToString() + "','" + tbVolum.Text + "','" + tbNrPoz.Text + "',N'" + tbCodSiruta.Text + "',N'" + ddlTip.SelectedValue + "',N'" + tbStrada.Text + "',N'" + tbNr.Text + "','" + tbNrInt.Text + "',N'" + tbBl.Text + "',N'" + tbSc.Text + "',N'" + tbEt.Text + "',N'" + tbAp.Text + "',N'" + tbCodExpl.Text + "',N'" + tbCodUnic.Text + "',N'" + tbJudet.Text + "',N'" + ddlLoc.SelectedItem + "','" + cbPersJuridica.Checked.ToString() + "',N'" + tbJUnitate.Text + "',N'" + tbJSubUnitate.Text + "',N'" + tbJCodFiscal.Text + "',N'" + tbJNumeReprez.Text + "','" + cbStrainas.Checked.ToString() + "',N'" + tbSStrada.Text + "',N'" + tbSNr.Text + "',N'" + tbSBl.Text + "',N'" + tbSSc.Text + "',N'" + tbSEt.Text + "',N'" + tbSAp.Text + "',N'" + tbSJud.Text + "',N'" + tbSLoc.Text + "', convert(datetime,'" + DateTime.Now.Date + "',104),'" + DateTime.Now.Hour.ToString() + "','" + DateTime.Now.Minute + "', '" + ScoateNumere(tbVolum.Text) + "','" + ScoateNumere(tbNrPoz.Text) + "', '" + Session["SESan"].ToString() + "', '" + tbMemCui.Text + "', N'" + tbSat.Text + "', N'" + tbObservatii.Text + "', N'" + tbMemNume.Text + "', '" + tbMemCNP.Text + "','" + taraStrainasTextBox.Text + "','" + numarRolNominalTextBox.Text + "'); SELECT SCOPE_IDENTITY() AS [SCOPE_IDENTITY]";
                vCmd.CommandText = vCmd.CommandText.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
                vGospodarieId = Convert.ToInt64(vCmd.ExecuteScalar());

                Session["SESgospodarieId"] = vGospodarieId;
                // scriem gospodaridid initial

                vCmd.CommandText = "update gospodarii set gospodarieIdInitial='" + vGospodarieId.ToString() + "' where gospodarieId='" + vGospodarieId.ToString() + "'";
                vCmd.ExecuteNonQuery();

                vCmd.CommandText = "delete from capitole where gospodarieId='" + vGospodarieId.ToString() + "'";
                vCmd.ExecuteNonQuery();
                /*vCmd1.CommandText = "select * from sabloaneCapitole where an='" + Session["SESan"].ToString() + "'";
                SqlDataReader vTabelSabloane = vCmd1.ExecuteReader();
                while (vTabelSabloane.Read())
                {
                    vCmd.CommandText = @"INSERT INTO [capitole] ([unitateId], [gospodarieId], [an], [codCapitol], [codRand], [col1], [col2], [col3], [col4], [col5], [col6], [col7], [col8]) VALUES ('" + Session["SESunitateId"].ToString() + "','" + vGospodaireId.ToString() + "','" + Session["SESan"].ToString() + "','" + vTabelSabloane["capitol"].ToString() + "', '" + vTabelSabloane["codRand"].ToString() + "', '0', '0', '0', '0', '0', '0', '0', '0')";
                    vCmd.ExecuteNonQuery();
                }*/
                // sterg membrii gospodariei nou adaugate si adaug membrul completat
                vCmd.CommandText = "delete from membri where gospodarieId='" + vGospodarieId.ToString() + "'";
                vCmd.ExecuteNonQuery();
                if (!cbPersJuridica.Checked)
                {
                    vCmd.CommandText = @"INSERT INTO [membri] ([unitateId], [gospodarieId], [an], [nume], [codRand], [cnp], [codSex], [codRudenie], [denumireRudenie], [dataNasterii], [mentiuni]) VALUES ('" + Session["SESunitateId"].ToString() + "','" + vGospodarieId.ToString() + "','" + Session["SESan"].ToString() + "',N'" + tbMemNume.Text + "','1','" + tbMemCNP.Text + "','" + ddlMemSex.SelectedValue + "','" + "1" + "',N'" + "cap de gospodărie" + "', convert(datetime,'" + tbMemDataN.Text + "',104),N'" + tbMemMentiuni.Text + "')";
                    vCmd.CommandText = vCmd.CommandText.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
                    vCmd.ExecuteNonQuery();
                }
                try
                {
                    ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "gospodarii+membri", "adaugare gospodarie si membri", "", vCmd, Convert.ToInt64(Session["SESgospodarieId"]), 2);
                }
                catch { }

                // gvGospodarii.SelectedIndex = -1;
                //Session["SESgospodarieId"] = null;
                //((MasterPage)this.Page.Master).SchimbaGospodaria();

                vTranz.Commit();
                gvGospodarii.DataBind();

                gospodarieSalvata = true;
            }
            catch { vTranz.Rollback(); }
            finally { ManipuleazaBD.InchideConexiune(vCon); }

            if (alegeAdaugareAdresaSediuCheckBox.Checked && gospodarieSalvata)
            {
                if (tbStradaFirma.Text == string.Empty || tbNumarFirma.Text == string.Empty || countyDropDownList.SelectedIndex == 0 || cityTextBox.Text == string.Empty)
                {
                    lblEroare1.Text = "Unul sau mai multe dintre campurile strada, numar, oras, localitate din cadrul adaugarii/modificarii sediilor nu este/sunt completat(e)! Sediul nu s-a salvat! ";
                    lblEroare1.Visible = true;
                    Panel4.Visible = true;
                }
                else
                {
                    AdaugaSediu();
                }
            }

            pnAdaugaGospodarii.Visible = false;
            pnListaGospodarii.Visible = true;
            pnAdaugareSediu.Enabled = false;
        }
        else if (ViewState["tip"].ToString() == "m")
        {

            // verific daca exista o gospodarie la volumul si numarul introdus
            try
            {
                vCmd1.CommandText = "select * from gospodarii where gospodarieId='" + gvGospodarii.SelectedValue + "'";
                string vValoareVeche = "";
                SqlDataReader vTabelGospodarii = vCmd1.ExecuteReader();
                if (vTabelGospodarii.Read())
                    vValoareVeche = "Volum " + vTabelGospodarii["volum"].ToString() + "; nr. pozitie " + vTabelGospodarii["nrPozitie"].ToString();
                vTabelGospodarii.Close();
                vCmd.CommandText = "select count(*) from gospodarii where unitateId='" + Session["SESunitateId"].ToString() + "' and volum='" + tbVolum.Text + "' and nrPozitie='" + tbNrPoz.Text + "' and gospodarieId <> '" + gvGospodarii.SelectedValue + "' AND an='" + Session["SESan"].ToString() + "'";
                int vValidareVolum = Convert.ToInt32(vCmd.ExecuteScalar());
                if (vValidareVolum > 0)
                { lblEroare.Visible = true; lblEroare.Text = "La volumul " + tbVolum.Text + ", poziţia " + tbNrPoz.Text + " există deja o gospodărie"; return; }
                else lblEroare.Visible = false;
                vCmd.CommandText = @"UPDATE [gospodarii] SET [unitateId] = '" + Session["SESunitateId"].ToString() + "', [volum] = '" + tbVolum.Text + "', [nrPozitie] = '" + tbNrPoz.Text + "', [codSiruta] = '" + tbCodSiruta.Text + "', [tip] = '" + ddlTip.SelectedValue + "', [strada] = N'" + tbStrada.Text + "', [nr] = '" + tbNr.Text + "', [nrInt] = '" + tbNrInt.Text + "', [bl] = '" + tbBl.Text + "', [sc] = '" + tbSc.Text + "', [et] = '" + tbEt.Text + "', [ap] = '" + tbAp.Text + "', [codExploatatie] = '" + tbCodExpl.Text + "', [codUnic] = '" + tbCodUnic.Text + "', [judet] = N'" + tbJudet.Text + "', [localitate] = N'" + ddlLoc.SelectedItem + "', [persJuridica] = '" + cbPersJuridica.Checked.ToString() + "', [jUnitate] = N'" + tbJUnitate.Text + "', [jSubunitate] = N'" + tbJSubUnitate.Text + "', [jCodFiscal] = '" + tbJCodFiscal.Text + "', [jNumeReprez] = N'" + tbJNumeReprez.Text + "', [strainas] = '" + cbStrainas.Checked.ToString() + "', [sStrada] = N'" + tbSStrada.Text + "', [sNr] = '" + tbSNr.Text + "', [sBl] = '" + tbSBl.Text + "', [sSc] = '" + tbSSc.Text + "', [sEtj] = '" + tbSEt.Text + "', [sAp] = '" + tbSAp.Text + "', [sJudet] = N'" + tbSJud.Text + "', [sLocalitate] = N'" + tbSLoc.Text + "', dataModificare=convert(datetime,'" + DateTime.Now.Date + "',104),oraModificare='" + DateTime.Now.Hour + "', minutModificare='" + DateTime.Now.Minute + "', [volumInt] = '" + ScoateNumere(tbVolum.Text) + "', [nrPozitieInt] = '" + ScoateNumere(tbNrPoz.Text) + "', [gospodarieCui] = '" + tbMemCui.Text + "', [gospodarieSat]=N'" + tbSat.Text + "', observatii =N'" + tbObservatii.Text + "',taraStrainas = '" + taraStrainasTextBox.Text + "',nrRolNominal = '" + numarRolNominalTextBox.Text + "' WHERE (([gospodarieId] = '" + gvGospodarii.SelectedValue + "'))";
                vCmd.CommandText = vCmd.CommandText.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
                vCmd.ExecuteNonQuery();
                ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "gospodarii+membri", "modificare gospodarie si membri", vValoareVeche, vCmd, Convert.ToInt64(Session["SESgospodarieId"]), 3);
                vTranz.Commit();
                gvGospodarii.DataBind();
                pnAdaugaGospodarii.Visible = false;
                pnListaGospodarii.Visible = true;
            }
            catch
            {
                vTranz.Rollback();
            }
            finally
            {
                ManipuleazaBD.InchideConexiune(vCon);
            }
        }
    }

    protected void btAnuleazaSalvarea_Click(object sender, EventArgs e)
    {
        pnAdaugaGospodarii.Visible = false;
        pnListaGospodarii.Visible = true;
        ClearControls();

        alegeAdaugareAdresaSediuCheckBox.Checked = false;
        pnAdaugareSediu.Enabled = alegeAdaugareAdresaSediuCheckBox.Checked;
        lblEroare.Text = "Eroare";
        lblEroare.Visible = false;
    }

    #endregion

    protected void cautaVolumVechiTextBox_TextChanged(object sender, EventArgs e)
    {
        Session["volumVechi"] = cautaVolumVechiTextBox.Text;
        HttpCookie vCook = Request.Cookies["COOKTNT"];
        if (vCook != null)
        {
            vCook = CriptareCookie.DecodeCookie(vCook);
            vCook["COOKfNr"] = tbfNr.Text;
            vCook = CriptareCookie.EncodeCookie(vCook);
            Response.Cookies.Add(vCook);
        }
    }

    protected void cautaPozitieVecheTextBox_TextChanged(object sender, EventArgs e)
    {
        Session["pozitieVeche"] = cautaPozitieVecheTextBox.Text;
        HttpCookie vCook = Request.Cookies["COOKTNT"];
        if (vCook != null)
        {
            vCook = CriptareCookie.DecodeCookie(vCook);
            vCook["COOKfNr"] = tbfNr.Text;
            vCook = CriptareCookie.EncodeCookie(vCook);
            Response.Cookies.Add(vCook);
        }
    }

    #region Sedii

    protected void alegeAdaugareAdresaSediuCheckBox_CheckedChanged(object sender, EventArgs e)
    {
        pnAdaugareSediu.Enabled = alegeAdaugareAdresaSediuCheckBox.Checked;
        btAdaugaSediu.Visible = alegeAdaugareAdresaSediuCheckBox.Checked && ViewState["tip"].ToString() != "a";

        if (alegeAdaugareAdresaSediuCheckBox.Checked)
        {
            ClearControls();
        }
    }

    protected void adaugaAutomatAdresaGospodarieiCheckBox_CheckedChanged(object sender, EventArgs e)
    {
        if (adaugaAutomatAdresaGospodarieiCheckBox.Checked)
        {
            tbStradaFirma.Text = ddlTip.SelectedIndex == 3 ? tbSStrada.Text.Trim().Capitalize() : tbStrada.Text.Trim().Capitalize();
            tbNumarFirma.Text = ddlTip.SelectedIndex == 3 ? tbSNr.Text.Trim() : tbNr.Text.Trim();
            tbBlocFirma.Text = ddlTip.SelectedIndex == 3 ? tbSBl.Text.Trim() : tbBl.Text.Trim();
            tbScaraFirma.Text = ddlTip.SelectedIndex == 3 ? tbSSc.Text.Trim() : tbSc.Text.Trim();
            tbEtajFirma.Text = ddlTip.SelectedIndex == 3 ? tbSEt.Text.Trim() : tbEt.Text.Trim();
            tbApartamentFirma.Text = ddlTip.SelectedIndex == 3 ? tbSAp.Text.Trim() : tbAp.Text.Trim();

            try
            {
                countyDropDownList.Text = ddlTip.SelectedIndex == 3 ? tbSJud.Text.Trim().Capitalize() : tbJudet.Text.Trim().Capitalize();
            }
            catch
            {
            }

            try
            {
                cityTextBox.Text = ddlTip.SelectedIndex == 3 ? tbSLoc.Text.Trim().Capitalize() : tbSat.Text.Trim().Capitalize();
            }
            catch
            {
            }
        }
        else
        {
            ClearControls();
        }
    }

    protected void gvSedii_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(gvSedii, e, this);

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[0].Text = e.Row.Cells[0].Text.Replace("_", " ");
        }
    }

    protected void gvSedii_SelectedIndexChanged(object sender, EventArgs e)
    {
        btModificaSediu.Visible = gvSedii.SelectedIndex != -1;
        btStergeSediu.Visible = gvSedii.SelectedIndex != -1;
        btAdaugaSediu.Visible = gvSedii.SelectedIndex == -1;
        pnAdaugareSediu.Enabled = gvSedii.SelectedIndex != -1;
        alegeAdaugareAdresaSediuCheckBox.Checked = gvSedii.SelectedIndex == -1;
        alegeAdaugareAdresaSediuCheckBox.Enabled = gvSedii.SelectedIndex == -1;

        FillControls();

    }

    protected void btAnuleazaModificarile_Click(object sender, EventArgs e)
    {
        gvSedii.SelectedIndex = -1;

        btModificaSediu.Visible = gvSedii.SelectedIndex != -1;
        btAdaugaSediu.Visible = false;
        btStergeSediu.Visible = false;
        pnAdaugareSediu.Enabled = gvSedii.SelectedIndex != -1;
        alegeAdaugareAdresaSediuCheckBox.Checked = gvSedii.SelectedIndex != -1;
        alegeAdaugareAdresaSediuCheckBox.Enabled = gvSedii.SelectedIndex == -1;

        ClearControls();
    }

    protected void btAdaugaSediu_Click(object sender, EventArgs e)
    {
        if (tbStradaFirma.Text == string.Empty || tbNumarFirma.Text == string.Empty || countyDropDownList.SelectedIndex == 0 || cityTextBox.Text == string.Empty)
        {
            lblEroare.Text = "Unul sau mai multe dintre campurile strada, numar, oras, localitate din cadrul adaugarii/modificarii sediilor nu este/sunt completat(e)!";
            lblEroare.Visible = true;
            return;
        }
        lblEroare.Text = "Eroare";
        lblEroare.Visible = false;

        AdaugaSediu();
        LoadGvSedii();
    }

    protected void btModificaSediu_Click(object sender, EventArgs e)
    {
        if (tbStradaFirma.Text == string.Empty || tbNumarFirma.Text == string.Empty || countyDropDownList.SelectedIndex == 0 || cityTextBox.Text == string.Empty)
        {
            lblEroare.Text = "Unul sau mai multe dintre campurile strada, numar, oras, localitate din cadrul adaugarii/modificarii sediilor nu este/sunt completat(e)!";
            lblEroare.Visible = true;
            return;
        }

        lblEroare.Text = "Eroare";
        lblEroare.Visible = false;

        Address adresa = CreateAddress();

        adresa.Id = Convert.ToUInt64(gvSedii.SelectedValue);

        adresa.Update();

        LoadGvSedii();

    }

    protected void btStergeSediu_Click(object sender, EventArgs e)
    {
        Address adresa = new Address();

        adresa.Id = Convert.ToUInt64(gvSedii.SelectedValue);
        AddressServices.ConnectionYear = Convert.ToInt16(Session["SESan"]);

        adresa.Delete();

        LoadGvSedii();

        btModificaSediu.Visible = false;
        btAdaugaSediu.Visible = true;
    }

    private void AdaugaSediu()
    {
        Address adresa = CreateAddress();

        if (!adresa.IsEmpty())
        {
            adresa.Add();
        }
    }

    private Address CreateAddress()
    {
        Address adresa = new Address();
        Gospodarie.Gospodarii gospodarie = new Gospodarie.Gospodarii();

        adresa.GospodarieId = Convert.ToInt32(Session["SESgospodarieId"].ToString());
        adresa.AddresType = (Address.AddressType)Enum.Parse(typeof(Address.AddressType), ddlTipAdresa.SelectedValue);
        adresa.An = Session["SESan"].ToString();

        adresa.GospodarieIdInitial = gospodarie.GetIdInitialById(adresa.GospodarieId, Convert.ToInt16(Session["SESan"]));
        adresa.Strada = tbStradaFirma.Text;
        adresa.Numar = tbNumarFirma.Text;
        adresa.Bloc = tbBlocFirma.Text;
        adresa.Scara = tbScaraFirma.Text;
        adresa.Etaj = tbEtajFirma.Text;
        adresa.Apartament = tbApartamentFirma.Text;
        adresa.Judet = countyDropDownList.Text;
        adresa.Localitate = cityTextBox.Text;
        adresa.Denumire = denumireTextBox.Text;
        adresa.Observatii = observatiiTextBox.Text;

        return adresa;
    }

    private void LoadGvSedii()
    {
        Address adresa = new Address();
        adresa.GospodarieId = Convert.ToInt32(Session["SESgospodarieId"]);
        AddressServices.ConnectionYear = Convert.ToInt16(Session["SESan"]);

        gvSedii.DataSource = AddressServices.GetAddressByGospodarieId(adresa.GospodarieId);
        gvSedii.DataBind();
    }

    private void FillDropDownLists()
    {
        FillCounties();
    }

    private void FillCounties()
    {
        countyDropDownList.Items.Clear();
        AddressServices.ConnectionYear = Convert.ToInt16(Session["SESan"]);
        countyDropDownList.DataSource = AddressServices.Counties;
        countyDropDownList.DataValueField = "judetDenumire";
        countyDropDownList.DataBind();
        countyDropDownList.Items.Insert(0, "-Selectează un județ-");
    }

    private void FillControls()
    {
        ddlTipAdresa.SelectedValue = ((int)(Address.AddressType)Enum.Parse(typeof(Address.AddressType), gvSedii.Rows[gvSedii.SelectedIndex].Cells[0].Text.Replace(" ", "_"))).ToString();
        denumireTextBox.Text = gvSedii.Rows[gvSedii.SelectedIndex].Cells[1].Text == "&nbsp;" ? string.Empty : gvSedii.Rows[gvSedii.SelectedIndex].Cells[1].Text;

        try
        {
            countyDropDownList.Text = gvSedii.Rows[gvSedii.SelectedIndex].Cells[2].Text;
        }
        catch { }

        try
        {
            cityTextBox.Text = gvSedii.Rows[gvSedii.SelectedIndex].Cells[3].Text;
        }
        catch { }

        tbStradaFirma.Text = gvSedii.Rows[gvSedii.SelectedIndex].Cells[4].Text == "&nbsp;" ? string.Empty : gvSedii.Rows[gvSedii.SelectedIndex].Cells[4].Text;
        tbNumarFirma.Text = gvSedii.Rows[gvSedii.SelectedIndex].Cells[5].Text == "&nbsp;" ? string.Empty : gvSedii.Rows[gvSedii.SelectedIndex].Cells[5].Text;
        tbBlocFirma.Text = gvSedii.Rows[gvSedii.SelectedIndex].Cells[6].Text == "&nbsp;" ? string.Empty : gvSedii.Rows[gvSedii.SelectedIndex].Cells[6].Text;
        tbApartamentFirma.Text = gvSedii.Rows[gvSedii.SelectedIndex].Cells[7].Text == "&nbsp;" ? string.Empty : gvSedii.Rows[gvSedii.SelectedIndex].Cells[7].Text;
        tbEtajFirma.Text = gvSedii.Rows[gvSedii.SelectedIndex].Cells[8].Text == "&nbsp;" ? string.Empty : gvSedii.Rows[gvSedii.SelectedIndex].Cells[8].Text;
        tbScaraFirma.Text = gvSedii.Rows[gvSedii.SelectedIndex].Cells[9].Text == "&nbsp;" ? string.Empty : gvSedii.Rows[gvSedii.SelectedIndex].Cells[9].Text;
        observatiiTextBox.Text = gvSedii.Rows[gvSedii.SelectedIndex].Cells[10].Text == "&nbsp;" ? string.Empty : gvSedii.Rows[gvSedii.SelectedIndex].Cells[10].Text;

    }

    private void ClearControls()
    {
        countyDropDownList.SelectedIndex = -1;
        cityTextBox.Text = string.Empty;
        denumireTextBox.Text = string.Empty;
        observatiiTextBox.Text = string.Empty;
        tbStradaFirma.Text = string.Empty;
        tbNumarFirma.Text = string.Empty;
        tbBlocFirma.Text = string.Empty;
        tbScaraFirma.Text = string.Empty;
        tbEtajFirma.Text = string.Empty;
        tbApartamentFirma.Text = string.Empty;
        gvSedii.SelectedIndex = -1;
        adaugaAutomatAdresaGospodarieiCheckBox.Checked = false;
    }

    #endregion Sedii



    protected void ddlRanduriPePagina_SelectedIndexChanged(object sender, EventArgs e)
    {
        gvGospodarii.PageSize = Convert.ToInt32(ddlRanduriPePagina.SelectedValue);
        gvGospodarii.DataBind();
    }
}
