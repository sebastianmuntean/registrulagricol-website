﻿<%@ Page Title="TNT Registrul Agricol - Unitati" Language="C#" MasterPageFile="~/MasterPage.master"
    AutoEventWireup="true" CodeFile="Unitati.aspx.cs" Inherits="Unitati" UICulture="ro-RO"
    Culture="ro-RO" EnableEventValidation="false" %>

<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Administrare / Unități" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="upUnitati" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnAfiseazaMesaje" runat="server">
                <asp:ValidationSummary ID="valSumUnitati" runat="server" DisplayMode="SingleParagraph"
                    Visible="true" ValidationGroup="GrupValidareUnitati" CssClass="validator" ForeColor="" />
            </asp:Panel>
            <asp:Panel ID="pnListaUnitati" runat="server" Visible="true" CssClass="panel_general">
                <asp:Panel ID="pnCautare" runat="server" CssClass="cauta">
                    <asp:Label ID="lblCauta" runat="server" Text="Caută după: " />
                    <asp:Label ID="lblCJudet" runat="server" Text="Judeţ"></asp:Label>
                    <asp:DropDownList ID="ddlCJudet" AutoPostBack="true" runat="server" OnPreRender="ddlCJudet_PreRender"
                        OnSelectedIndexChanged="ddlCJudet_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:Label ID="lblCUnitateDenumire" runat="server" Text="Denumirea unității" />
                    <asp:TextBox ID="tbCUnitateDenumire" runat="server" AutoPostBack="True" />
<%--                    <ajaxToolkit:AutoCompleteExtender ID="tbCUnitateDenumire_AutoCompleteExtender" runat="server"
                        Enabled="True" ServiceMethod="GetCompletionList" ServicePath="AutoComplete.asmx"
                        TargetControlID="tbCUnitateDenumire" BehaviorID="AutoCompleteEx1" MinimumPrefixLength="1"
                        CompletionInterval="10" EnableCaching="True" CompletionSetCount="5" CompletionListCssClass="autocomplete_completionListElement"
                        CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                        DelimiterCharacters=";, :" ShowOnlyCurrentWordInCompletionListItem="false" FirstRowSelected="False"
                        UseContextKey="True" ContextKey="Unitate">
                    </ajaxToolkit:AutoCompleteExtender>--%>
                </asp:Panel>
                <asp:Panel ID="Panel1" runat="server">
                    <asp:GridView ID="gvUnitati" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        BorderStyle="None" CellPadding="3" DataKeyNames="unitateId" DataSourceID="SqlUnitati"
                        GridLines="Vertical" CssClass="tabela" OnDataBound="gvUnitati_DataBound" OnRowDataBound="gvUnitati_RowDataBound"
                        OnSelectedIndexChanged="gvUnitati_SelectedIndexChanged" AllowPaging="True" PageSize="20">
                        <Columns>
                            <asp:BoundField DataField="unitateId" HeaderText="Nr." InsertVisible="False" ReadOnly="True"
                                SortExpression="unitateId" Visible="false" />
                            <asp:BoundField DataField="unitateDenumire" HeaderText="Denumire" SortExpression="unitateDenumire" />
                            <asp:BoundField DataField="unitateCodSiruta" HeaderText="Cod SIRUTA" SortExpression="unitateCodSiruta" />
                            <asp:BoundField DataField="judetId" HeaderText="județ" SortExpression="judetId" />
                            <asp:BoundField DataField="localitateId" HeaderText="localitate" SortExpression="localitateId" />
                            <asp:BoundField DataField="unitateStrada" HeaderText="Strada" SortExpression="unitateStrada" />
                            <asp:BoundField DataField="unitateNr" HeaderText="Nr." SortExpression="unitateNr" />
                            <asp:BoundField DataField="unitateAp" HeaderText="Ap." SortExpression="unitateAp" />
                            <asp:BoundField DataField="unitateCodPostal" HeaderText="Cod Postal" SortExpression="unitateCodPostal" />
                            <asp:BoundField DataField="unitateCodFiscal" HeaderText="Cod Fiscal" SortExpression="unitateCodFiscal" />
                            <asp:BoundField DataField="unitateActiva" HeaderText="Activa" SortExpression="unitateActiva"
                                Visible="false" />
                            <asp:BoundField DataField="RowNumber" HeaderText="Nr. crt." SortExpression="RowNumber" />
                        </Columns>
                        <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                        <HeaderStyle Font-Bold="True" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlUnitati" runat="server" 
                        DeleteCommand="DELETE FROM [unitati] WHERE [unitateId] = @unitateId" InsertCommand="INSERT INTO [unitati] ([unitateDenumire], [unitateCodSiruta], [judetId], [localitateId], [unitateStrada], [unitateNr], [unitateAp], [unitateCodPostal], [localitateComponentaId], [unitateCodFiscal]) VALUES (@unitateDenumire, @unitateCodSiruta, @judetId, @localitateId, @unitateStrada, @unitateNr, @unitateAp, @unitateCodPostal, @localitateComponentaId, @unitateCodFiscal)"
                        SelectCommand="SELECT unitateId, unitateDenumire, unitateCodSiruta, judetId, localitateId, unitateStrada, unitateNr, unitateAp, unitateCodPostal, localitateComponentaId, unitateCodFiscal, unitateActiva, ROW_NUMBER() OVER(ORDER BY unitateDenumire ASC) AS RowNumber FROM unitati WHERE (unitateDenumire LIKE N'%' + @unitateDenumire + '%') AND (CONVERT (nvarchar, judetId) LIKE @judetId) ORDER BY unitateDenumire"
                        UpdateCommand="UPDATE [unitati] SET [unitateDenumire] = @unitateDenumire, [unitateCodSiruta] = @unitateCodSiruta, [judetId] = @judetId, [localitateId] = @localitateId, [unitateStrada] = @unitateStrada, [unitateNr] = @unitateNr, [unitateAp] = @unitateAp, [unitateCodPostal] = @unitateCodPostal, [localitateComponentaId] = @localitateComponentaId, [unitateCodFiscal] = @unitateCodFiscal, [unitateActiva] = @unitateActiva WHERE [unitateId] = @unitateId"
                        OnSelecting="SqlUnitati_Selecting">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="tbCUnitateDenumire" DefaultValue="%" Name="unitateDenumire"
                                PropertyName="Text" />
                            <asp:ControlParameter ControlID="ddlCJudet" DefaultValue="" Name="judetId" PropertyName="SelectedValue" />
                        </SelectParameters>
                        <DeleteParameters>
                            <asp:Parameter Name="unitateId" Type="Int32" />
                        </DeleteParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="unitateDenumire" Type="String" />
                            <asp:Parameter Name="unitateCodSiruta" Type="Int32" />
                            <asp:Parameter Name="judetId" Type="Int32" />
                            <asp:Parameter Name="localitateId" Type="Int32" />
                            <asp:Parameter Name="unitateStrada" Type="String" />
                            <asp:Parameter Name="unitateNr" Type="String" />
                            <asp:Parameter Name="unitateAp" Type="String" />
                            <asp:Parameter Name="unitateCodPostal" Type="Int32" />
                            <asp:Parameter Name="localitateComponentaId" Type="Int32" />
                            <asp:Parameter Name="unitateCodFiscal" Type="String" />
                            <asp:Parameter Name="unitateActiva" Type="Int32" />
                            <asp:Parameter Name="unitateId" Type="Int32" />
                        </UpdateParameters>
                        <InsertParameters>
                            <asp:Parameter Name="unitateDenumire" Type="String" />
                            <asp:Parameter Name="unitateCodSiruta" Type="String" />
                            <asp:Parameter Name="judetId" Type="Int32" />
                            <asp:Parameter Name="localitateId" Type="Int32" />
                            <asp:Parameter Name="unitateStrada" Type="String" />
                            <asp:Parameter Name="unitateNr" Type="String" />
                            <asp:Parameter Name="unitateAp" Type="String" />
                            <asp:Parameter Name="unitateCodPostal" Type="String" />
                            <asp:Parameter Name="localitateComponentaId" Type="Int32" />
                            <asp:Parameter Name="unitateCodFiscal" Type="String" />
                        </InsertParameters>
                    </asp:SqlDataSource>
                </asp:Panel>
                <asp:Panel ID="pnListaButoanePrincipale" CssClass="butoane" runat="server">
                    <asp:Button CssClass="buton" ID="btModificaUnitate" runat="server" Text="modifică unitatea"
                        OnClick="ArataPanelModificaUnitate" />
                    <asp:Button CssClass="buton" ID="btStergeUnitate" runat="server" Text="șterge unitatea"
                        OnClick="StergeUnitate" ValidationGroup="GrupValidareUnitati" OnClientClick="return confirm(&quot;Sunteți sigur că doriți să ștergeți unitatea?&quot;)" />
                    <asp:Button CssClass="buton" ID="btListaAdaugaUnitate" runat="server" Text="adaugă o unitate"
                        OnClick="ArataAdaugaUnitati" />
                </asp:Panel>
            </asp:Panel>
            <!-- adaugam unitate -->
            <asp:Panel ID="pnAdaugaUnitate" CssClass="panel_general" runat="server" Visible="false">
                <asp:Panel CssClass="adauga" runat="server">
                    <asp:Panel runat="server">
                        <h2>
                            ADAUGĂ / MODIFICĂ O UNITATE</h2>
                    </asp:Panel>
                    <asp:Panel runat="server">
                        <p>
                            <asp:Label ID="lblDenumireUnitate" runat="server" Text="Denumire" />
                            <asp:Label ID="lblDenumireUnitateOriginala" runat="server" Text="Denumire" Visible="false" />
                            <asp:Label ID="lblCifOriginal" runat="server" Text="Denumire" Visible="false" />
                            <asp:Label ID="lblUnitateId" runat="server" Text="" Visible="false" />
                            <asp:TextBox ID="tbDenumireUnitate" runat="server" Text="" Width="300px"></asp:TextBox>
                            <asp:Label ID="lblCIF" runat="server" Text="CIF" />
                            <asp:TextBox ID="tbCIF" runat="server"></asp:TextBox>
                            <asp:Label ID="lblCodSiruta" runat="server" Text="Cod SIRUTA" />
                            <asp:TextBox ID="tbCodSiruta" runat="server"></asp:TextBox>
                        </p>
                        <p>
                            <asp:Label ID="lblJudet" runat="server" Text="Județul" />
                            <asp:DropDownList ID="ddJudet" runat="server" DataSourceID="SqlJudete" DataTextField="judetDenumire"
                                DataValueField="judetId" AutoPostBack="true" />
                            <asp:SqlDataSource ID="SqlJudete" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016 %>"
                                SelectCommand="SELECT * FROM [judete]"></asp:SqlDataSource>
                            <asp:Label ID="lblLocalitate" runat="server" Text="Localitate" />
                            <asp:DropDownList ID="ddLocalitate" runat="server" DataSourceID="SqlLocalitati" DataTextField="localitateDenumire"
                                DataValueField="localitateId" />
                            <asp:SqlDataSource ID="SqlLocalitati" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016 %>"
                                SelectCommand="SELECT [localitateDenumire], [localitateId] FROM [localitati] WHERE ([judetId] = @judetId)">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="ddJudet" Name="judetId" PropertyName="SelectedValue"
                                        Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                            <asp:Label ID="lblActiva" Visible="false" runat="server" Text="Activă?" />
                            <asp:DropDownList ID="ddUnitateActiva" Visible="false" runat="server">
                                <asp:ListItem Selected="True" Text="da" Value="1">       
                                </asp:ListItem>
                                <asp:ListItem Text="nu" Value="0">       
                                </asp:ListItem>
                            </asp:DropDownList>
                        </p>
                        <p>
                            <asp:Label ID="lblAdresa" runat="server" Text="Adresa:" />
                            <asp:Label ID="lblStrada" runat="server" Text="Strada" />
                            <asp:TextBox ID="tbStrada" runat="server" />
                            <asp:Label ID="lblNumar" runat="server" Text="Nr." />
                            <asp:TextBox ID="tbNumar" runat="server" Width="30px" />
                            <asp:Label ID="lblAp" runat="server" Text="Ap." />
                            <asp:TextBox ID="tbAp" runat="server" Width="30px" />
                            <asp:Label ID="lblCodPostal" runat="server" Text="Cod poștal" />
                            <asp:TextBox ID="tbCodPostal" runat="server" Width="100px" />
                            <asp:Label ID="lblLocalitateRang" runat="server" Text="Rangul localitatii" />
                            <asp:DropDownList ID="ddLocalitateRang" runat="server">
                                <asp:ListItem Selected="True" Text="comuna" Value="comuna">       
                                </asp:ListItem>
                                <asp:ListItem Text="oras" Value="oras">       
                                </asp:ListItem>
                                <asp:ListItem Text="municipiu" Value="municipiu">       
                                </asp:ListItem>
                                <asp:ListItem Text="sat" Value="sat">       
                                </asp:ListItem>
                            </asp:DropDownList>
                        </p>
                        <p>
                            <asp:Label ID="lblTipAfisareStrainas" runat="server" Text="Tip afisare adresa strainas"></asp:Label>
                            <asp:DropDownList ID="ddlTipAfisareStrainas" runat="server">
                                <asp:ListItem Text="adresa de domiciliu" Value="1"></asp:ListItem>
                                <asp:ListItem Text="adresa din localitate" Value="2"></asp:ListItem>
                            </asp:DropDownList>
                        </p>
                        <p>
                            <asp:CheckBox ID="cbAdaugaDinC2B" CssClass="checkboxuri" Text="Adaugă automat date din Capitolul 2B in Capitolul 2A"
                                runat="server" /><br />
                            <asp:CheckBox ID="cbAdaugaDinC2BxC4abc" CssClass="checkboxuri" Text="Adaugă automat date din Capitolul 2B in Capitolele 4a,4a1, 4b1, 4b2, 4c"
                                runat="server" /><br />
                            <asp:CheckBox ID="cbAdaugaDinC2BxC3" CssClass="checkboxuri" Text="Adaugă automat date din Capitolul 2B in Capitolul 3"
                                runat="server" /><br />
                            <asp:CheckBox ID="cbFocusLaEnter" CssClass="checkboxuri" Text="Trecerea de la o casuţă la alta se face cu tasta Enter"
                                runat="server" />
                            <br />
                            <asp:CheckBox ID="cbAdaugaAnimale" CssClass="checkboxuri" Text="Adaugă animale în pagină separată"
                                runat="server" />
                               <br />
                            <asp:CheckBox ID="valideazaVolumSiPozitieCheckBox"  CssClass="checkboxuri" Text="Validează volum și pozitie"
                                runat="server" />
                        </p>
                    </asp:Panel>
                    <asp:Panel CssClass="ascunsa" runat="server">
                        <!-- //////////// VALIDATOARE  \\\\\\\\\\\\\\\\\\ -->
                        <asp:CustomValidator ID="valCustDenumireUnitate" runat="server" ErrorMessage="Există deja o unitate cu acest nume. Dacă doriți să modificați datele acesteia, căutați-o în lista de unități, o selectați și apăsați Modifică."
                            ValidationGroup="GrupValidareUnitati"></asp:CustomValidator>
                        <asp:RequiredFieldValidator ID="valTbDenumireUnitate" runat="server" ErrorMessage="Câmpul Denumirea unității este obligatoriu!"
                            ControlToValidate="tbDenumireUnitate" Display="Static" ValidationGroup="GrupValidareUnitati"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="valCodPostal" runat="server" ErrorMessage="Codul Poștal este din 6 cifre!"
                            ValidationExpression="\d{6}$" ControlToValidate="tbCodPostal" ValidationGroup="GrupValidareUnitati"></asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="valTbCIF" runat="server" ErrorMessage="Câmpul Cod fiscal este obligatoriu!"
                            ControlToValidate="tbCIF" Display="Static" ValidationGroup="GrupValidareUnitati"></asp:RequiredFieldValidator>
                        <!-- //////////// VALIDATOARE  \\\\\\\\\\\\\\\\\\ -->
                    </asp:Panel>
                    <asp:Panel ID="pnButoanePrincipale" runat="server" CssClass="butoane">
                        <asp:Button ID="btAdaugaUnitate" runat="server" CssClass="buton" OnClick="AdaugaModificaUnitate"
                            Text="adaugă unitatea" ValidationGroup="GrupValidareUnitati" />
                        <asp:Button ID="btAdaugaModificaUnitate" runat="server" CssClass="buton" OnClick="AdaugaModificaUnitate"
                            Text="modifică unitatea" ValidationGroup="GrupValidareUnitati" />
                        <asp:Button ID="btModificaArataListaUnitati" runat="server" CssClass="buton" OnClick="ModificaArataListaUnitati"
                            Text="lista unităților" />
                        <asp:Button ID="btAdaugaArataListaUnitati" runat="server" CssClass="buton" OnClick="AdaugaArataListaUnitati"
                            Text="lista unităților" />
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
