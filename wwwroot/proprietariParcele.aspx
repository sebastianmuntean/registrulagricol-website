﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="proprietariParcele.aspx.cs" Inherits="proprietariParcele" UICulture="ro-Ro" Culture="ro-Ro"
    EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Proprietari parcela" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="upParcele" runat="server">
        <ContentTemplate>
            <asp:Panel ID="panel" CssClass="panel_general" runat="server">
                <asp:GridView ID="proprietariGridView" runat="server" AutoGenerateColumns="False" CellPadding="4" Width="100%" CssClass="tabela" ShowHeaderWhenEmpty="true" EmptyDataText="Nu exista inregistrari."
                    DataKeyNames="id" ForeColor="#333333" GridLines="Both" OnRowCancelingEdit="proprietariGridView_RowCancelingEdit" 
                    OnRowDeleting="proprietariGridView_RowDeleting" OnRowEditing="proprietariGridView_RowEditing" OnRowUpdating="proprietariGridView_RowUpdating" >
                    <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"/>
                    <Columns>
                        <asp:CommandField HeaderText="Modifică" ControlStyle-Font-Size="Larger" ShowEditButton="True" EditText="Modifică"/>
                        <asp:BoundField DataField="numeProprietar" HeaderText="Nume Proprietar" />
                        <asp:BoundField DataField="cotaParte" HeaderText="Cota parte (%)" />
                        <asp:BoundField DataField="parcelaDenumire" HeaderText="Denumire parcela" ReadOnly="True" />
                        <asp:BoundField DataField="parcelaNrCadastral" HeaderText="Nr Cadastral" ReadOnly="True" />
                        <asp:BoundField DataField="parcelaNrTopo" HeaderText="Nr TOPO"  ReadOnly="True"/>
                        <asp:BoundField DataField="parcelaCF" HeaderText="Nr CF"  ReadOnly="True"/>
                        <asp:BoundField DataField="parcelaSuprafataIntravilanHa" HeaderText="Intravilan Ha" ReadOnly="True" />
                        <asp:BoundField DataField="parcelaSuprafataIntravilanAri" HeaderText="Intravilan Ari" ReadOnly="True"/>
                        <asp:BoundField DataField="parcelaSuprafataExtravilanHa" HeaderText="Extravilan Ha" ReadOnly="True" />
                        <asp:BoundField DataField="parcelaSuprafataExtravilanAri" HeaderText="Extravilan Ari" ReadOnly="True" />
                        <asp:CommandField HeaderText="Șterge" ControlStyle-Font-Size="Larger" ShowDeleteButton="True" DeleteText="Șterge" />
                    </Columns>
                    <RowStyle BackColor="#E3EAEB" />
                    <EditRowStyle BackColor="MenuBar" />
                    <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
                <br />
               
            </asp:Panel>
             <asp:Panel ID="panelButon" CssClass="butoane" runat="server">
                    <asp:Button ID="daugaProprietarButton" runat="server" CssClass="buton" OnClick="adaugaProprietarButton_Click" Text="adaugă proprietar" />
                  <asp:Button ID="listaParcele" runat="server" CssClass="buton" OnClick="listaParcele_Click" Text="listă parcele" />
                </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

