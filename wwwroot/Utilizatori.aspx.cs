﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;

/// <summary>
/// UTILIZATORI
/// Creata la:                  26.01.2011
/// Autor:                      Sebastian Muntean
/// Ultima                      actualizare: 06.04.11
/// Autor:                      Sebastian Muntean - log
/// Modificat:                  Mihai - adaugat supervizor national
/// </summary>

public partial class Utilizatori : System.Web.UI.Page
{

    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        DBConnections connection = new DBConnections(Convert.ToInt16(Session["SESan"]));

        SqlUnitati.ConnectionString = connection.Create();

        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "utilizatori", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
        }
        // daca nu e utilizator admin nu poate accesa pagina
        /*if (Redirect.RedirectUtilizator(Session["SESutilizatorId"].ToString()) == 1)
        {
            Response.Redirect("NuAvetiDrepturi.aspx");
        }*/
        /*    string vUnitatePrincipala = ManipuleazaBD.fRezultaUnString("SELECT unitatePrincipala FROM unitati WHERE unitateId = '" + Session["unitateId"].ToString() + "'", "unitatePrincipala");
            switch (vUnitatePrincipala)
            {
                case "1":
                    tbCUnitate.Visible = true;
                    lblCUnitate.Visible = true;
                    tbCLocalitate_AutoCompleteExtender.ContextKey = "Unitate";
                    break;
                case "2":
                    tbCUnitate.Visible = true;
                    lblCUnitate.Visible = true;
                    tbCLocalitate_AutoCompleteExtender.ContextKey = "UnitateTNT";
                    break;
                default :
                    tbCUnitate.Visible = false;
                    lblCUnitate.Visible = false;
                    ddUnitate.Visible = false;
                    lblUnitate.Visible = false;
                    break;
            }
            */

    }

    protected void ValideazaUtilizatori(object sender, EventArgs e, int pTipButon, out string pExpresie)
    {
        int vTrece = 0;
        while (vTrece == 0)
        {
            // validam concordanta dintre unitate si tip utilizator 
            /*Valideaza.ConcordantaUnitateTipUtilizator(Session["SESutilizatorId"].ToString(), ddUnitate.SelectedValue, ddTipUtilizator.SelectedValue, out vExpresie, out vTrece);
            if (vTrece == 0)
            {
                valCustom.IsValid = false;
                valCustom.ErrorMessage = vExpresie;
                break;
            }*/
            // validam Login, min 8 caractere max 100
            Valideaza.DimensiuneString(lblLogin.Text, tbLogin.Text, 8, 0, out vExpresie, out vTrece);
            if (vTrece == 0)
            {
                valCustom.IsValid = false;
                valCustom.ErrorMessage = vExpresie;
                break;
            }
            // validam Login sa nu mai existe pentru unitatea respectiva 
            string[] vCampuri = new string[] { "utilizatorLogin", "unitateId" };
            string[] vValori = new string[] { tbLogin.Text, ddUnitate.SelectedValue };
            if (tbLogin.Text != lblLoginOriginal.Text || pTipButon == 0) // daca e modificat
            {
                if (ManipuleazaBD.fVerificaExistentaArray("utilizatori", vCampuri, vValori, " AND ", Convert.ToInt16(Session["SESan"])) == 1)
                {
                    vExpresie = "Exista deja un utilizator cu numele: " + tbLogin.Text;
                    valCustom.IsValid = false;
                    valCustom.ErrorMessage = vExpresie;
                    break;
                }
            }
            if (pTipButon == 1)
            {
                if (tbParola.Text != "" && tbParola.Text != "")
                {
                    // verificam parolele
                    clsUtilizatori.ParolaVerifica(tbParola.Text, tbConfirmaParola.Text, gvUtilizatori.SelectedValue.ToString(), out vExpresie, Convert.ToInt16(Session["SESan"]));
                    if (vExpresie != "")
                    {
                        valCustom.IsValid = false;
                        valCustom.ErrorMessage = vExpresie;
                        break;
                    }
                }
                else
                {
                    vExpresie = "Parolele nu se schimba.";
                    break;
                }
            }
            else
            {
                // verificam parolele
                clsUtilizatori.ParolaVerifica(tbParola.Text, tbConfirmaParola.Text, "", out vExpresie, Convert.ToInt16(Session["SESan"]));
                if (vExpresie != "")
                {
                    valCustom.IsValid = false;
                    valCustom.ErrorMessage = vExpresie;
                    break;
                }
            }
            Valideaza.Email(lblUtilizatorEmail.Text, tbUtilizatorEmail.Text, out vExpresie, out vTrece);
            if (vTrece == 0)
            {
                valCustom.IsValid = false;
                valCustom.ErrorMessage = vExpresie;
                break;
            }
        }
        pExpresie = vExpresie;
    }
    private string validareDurataSesiune()
    {
        string vError = "";
        if (tbDurataSesiuneLucru.Text == "")
            vError += "Durata sesiunii de lucru este obligatorie ! ";
        else
            try
            {
                if (Convert.ToInt32(tbDurataSesiuneLucru.Text) > 30)
                    vError += "Durata sesiunii de lucru nu poate fi mai mare de 30 de minute ! ";
                if (Convert.ToInt32(tbDurataSesiuneLucru.Text) < 1)
                    vError += "Duarata sesiunii de lucru nu poate fi mai mică de un minut ! ";
            }
            catch { vError += "Durata sesiunii de lucru este invalidă ! "; }
        return vError;
    }
    protected void AdaugaModificaUtilizator(object sender, EventArgs e)
    {

        if (((Button)sender).ID.ToString() == "btAdaugaModificaUtilizator")
        { ValideazaUtilizatori(sender, e, 1, out vExpresie); }
        else { ValideazaUtilizatori(sender, e, 0, out vExpresie); }
        if (validareDurataSesiune() != "")
        {
            valCustom.IsValid = false;
            valCustom.ErrorMessage = validareDurataSesiune();
            return;
        }
        string vInterogare = "";
        if (valCustom.IsValid != false)
        {
            if (((Button)sender).ID.ToString() == "btAdaugaUtilizator")
            {
                // verific daca este aleasa o unitate
                if (ddUnitate.Items.Count == 0)
                {
                    valCustom.IsValid = false;
                    valCustom.ErrorMessage = "Vă rugăm să alegeţi o unitate";
                    return;
                }
                // daca utilizatorul logat este supervizor national sau functionar cu drepturi pe pagina, tipul utilizatorului adaugat poate fi doar supervizor judetean sau functionar
                SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
                SqlCommand vCmd = new SqlCommand();
                vCmd.Connection = vCon;
                vCmd.CommandText = "select top(1) tipUtilizatorId from utilizatori where utilizatorId='" + Session["SESutilizatorId"].ToString() + "'";
                int vTipUtilizator = Convert.ToInt32(vCmd.ExecuteScalar());
                ManipuleazaBD.InchideConexiune(vCon);
                if ((vTipUtilizator == 2 || vTipUtilizator == 3) && (ddTipUtilizator.SelectedValue == "1" || ddTipUtilizator.SelectedValue == "4"))
                {
                    valCustom.IsValid = false;
                    valCustom.ErrorMessage = "Nu aveţi suficiente drepturi pentru a adăuga acest tip de utilizator";
                    return;
                }
                vInterogare = @"INSERT INTO utilizatori 
                        (utilizatorLogin, utilizatorNume, utilizatorPrenume, utilizatorFunctia, utilizatorAdresa, utilizatorCNP, utilizatorTelefon, utilizatorMobil, unitateId, utilizatorActiv, utilizatorEmail, utilizatorParola, tipUtilizatorId, utilizatorDurataSesiune)
                         VALUES ('" + tbLogin.Text + "','" + tbUtilizatorNume.Text + "','" + tbUtilizatorPrenume.Text + "','" + tbUtilizatorFunctia.Text + "','" + tbUtilizatorAdresa.Text + "','" + tbUtilizatorCNP.Text + "','" + tbUtilizatorTelefon.Text + "','" + tbUtilizatorMobil.Text + "','" + ddUnitate.SelectedValue + "','" + ddUtilizatorActiv.SelectedValue + "','" + tbUtilizatorEmail.Text + "','" + Criptare.Encrypt(tbParola.Text) + "','" + ddTipUtilizator.SelectedValue + "', '" + tbDurataSesiuneLucru.Text + "' ); SELECT CAST(scope_identity() AS int)";
                int vUtilizatorId = -1;
                ManipuleazaBD.fManipuleazaBDCuId(vInterogare, out vUtilizatorId, Convert.ToInt16(Session["SESan"]));
                // salvam in utilizatoriParole
                ManipuleazaBD.fManipuleazaBD("INSERT INTO utilizatoriParole (utilizatorParola,utilizatorId,utilizatorData) VALUES (N'" + Criptare.Encrypt(tbParola.Text) + "', " + vUtilizatorId + ", CONVERT(datetime, '" + DateTime.Now.ToString() + "', 104))", Convert.ToInt16(Session["SESan"]));

                ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "utilizatori", "| adauga utilizator: " + tbLogin.Text + " | unitate: " + ddUnitate.SelectedValue, "", Convert.ToInt64(Session["SESgospodarieId"]), 2);
                gvUtilizatori.SelectedIndex = -1;
                clsUtilizatori.AdaugaDrepturi(Convert.ToInt32(ddTipUtilizator.SelectedValue), vUtilizatorId, Convert.ToInt16(Session["SESan"]));

                InitializeazaCampuri(1);

                pnAdaugaUtilizator.Visible = false;
                pnListaUtilizatori.Visible = true;
                btModificaUtilizator.Visible = false;
                btArataModificaDrepturiUtilizator.Visible = false;
                btStergeUtilizator.Visible = false;
            }
            else
            {
                // verific daca este aleasa o unitate
                if (ddUnitate.Items.Count == 0)
                {
                    valCustom.IsValid = false;
                    valCustom.ErrorMessage = "Vă rugăm să alegeţi o unitate";
                    return;
                }
                // daca nu s-au schimbat parolele
                if (vExpresie != "Parolele nu se schimba.")
                    vInterogare = @"UPDATE utilizatori SET utilizatorLogin ='" + tbLogin.Text +
                       "', utilizatorNume ='" + tbUtilizatorNume.Text +
                       "', utilizatorPrenume ='" + tbUtilizatorPrenume.Text +
                       "', utilizatorFunctia = '" + tbUtilizatorFunctia.Text +
                       "', utilizatorAdresa = '" + tbUtilizatorAdresa.Text +
                       "', utilizatorCNP = '" + tbUtilizatorCNP.Text +
                       "', utilizatorTelefon = '" + tbUtilizatorTelefon.Text +
                       "', utilizatorMobil = '" + tbUtilizatorMobil.Text +
                       "', unitateId = '" + ddUnitate.SelectedValue +
                       "', utilizatorActiv = '" + ddUtilizatorActiv.SelectedValue +
                       "', utilizatorEmail = '" + tbUtilizatorEmail.Text +
                       "', utilizatorParola = '" + Criptare.Encrypt(tbParola.Text) +
                       "', tipUtilizatorId = '" + ddTipUtilizator.SelectedValue +
                       "', utilizatorDurataSesiune = '" + tbDurataSesiuneLucru.Text +
                       "' WHERE utilizatorId ='" + gvUtilizatori.SelectedValue.ToString() + "'";
                else
                    vInterogare = @"UPDATE utilizatori SET utilizatorLogin ='" + tbLogin.Text +
   "', utilizatorNume ='" + tbUtilizatorNume.Text +
   "', utilizatorPrenume ='" + tbUtilizatorPrenume.Text +
   "', utilizatorFunctia = '" + tbUtilizatorFunctia.Text +
   "', utilizatorAdresa = '" + tbUtilizatorAdresa.Text +
   "', utilizatorCNP = '" + tbUtilizatorCNP.Text +
   "', utilizatorTelefon = '" + tbUtilizatorTelefon.Text +
   "', utilizatorMobil = '" + tbUtilizatorMobil.Text +
   "', unitateId = '" + ddUnitate.SelectedValue +
   "', utilizatorActiv = '" + ddUtilizatorActiv.SelectedValue +
   "', utilizatorEmail = '" + tbUtilizatorEmail.Text +
   "', tipUtilizatorId = '" + ddTipUtilizator.SelectedValue +
   "', utilizatorDurataSesiune = '" + tbDurataSesiuneLucru.Text +
   "' WHERE utilizatorId ='" + gvUtilizatori.SelectedValue.ToString() + "'";

                ManipuleazaBD.fManipuleazaBD(vInterogare, Convert.ToInt16(Session["SESan"]));
                // salvam in utilizatoriParole
                ManipuleazaBD.fManipuleazaBD("INSERT INTO utilizatoriParole (utilizatorParola,utilizatorId,utilizatorData) VALUES (N'" + Criptare.Encrypt(tbParola.Text) + "', " + gvUtilizatori.SelectedValue.ToString() + ", CONVERT(datetime, '" + DateTime.Now.ToString() + "', 104))", Convert.ToInt16(Session["SESan"]));

                ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "utilizatori", "| modificare utilizator/id: " + tbLogin.Text + " / " + gvUtilizatori.SelectedValue.ToString() + " | unitate: " + ddUnitate.SelectedValue, "", Convert.ToInt64(Session["SESgospodarieId"]), 3);
                pnAdaugaUtilizator.Visible = false;
                pnListaUtilizatori.Visible = true;
                btModificaUtilizator.Visible = true;
                btArataModificaDrepturiUtilizator.Visible = true;
                btStergeUtilizator.Visible = true;
                InitializeazaCampuri(1);
            }
        }
        else
        {
            pnAdaugaUtilizator.Visible = true;
            pnListaUtilizatori.Visible = false;


            //InitializeazaCampuri(0);
        }
        gvUtilizatori.DataBind();
    }

    string vExpresie = "";
    public int ValideazaCampuri()
    {
        vExpresie = "";
        int vTrece = 1;
        Valideaza.DimensiuneString(lblLogin.Text, tbLogin.Text, 8, 100, out vExpresie, out vTrece);
        return vTrece;
    }
    protected void InitializeazaCampuri(int vCePanel)
    {
        // initializam valorile din formularul de adauga
        ddUtilizatorActiv.SelectedValue = "1";
        if (vCePanel == 1)
        {
            //   pnAdaugaUtilizator.Visible = false;
            //   pnListaUtilizatori.Visible = true;
        }
        else
        {
            // pnAdaugaUtilizator.Visible = true;
            //  pnListaUtilizatori.Visible = false;
        }
        //ddUnitate.SelectedValue = "";
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select top(1) tipUtilizatorId from utilizatori where utilizatorId='" + Session["SESutilizatorId"].ToString() + "'";
        int vTipUtilizator = Convert.ToInt32(vCmd.ExecuteScalar());
        if (vTipUtilizator == 2 || vTipUtilizator == 3 || vTipUtilizator == 5)
            ddlJudet.Enabled = false;
        else ddlJudet.Enabled = true;
        ddlJudet.Items.Clear();
        ddlJudet.Items.Add(new ListItem("-toate-", "%"));
        ddlJudet.DataBind();
        vCmd.CommandText = "SELECT TOP (1) unitati.judetId FROM utilizatori INNER JOIN unitati ON utilizatori.unitateId = unitati.unitateId WHERE (utilizatori.utilizatorId = '" + Session["SESutilizatorId"].ToString() + "')";
        ddlJudet.SelectedValue = vCmd.ExecuteScalar().ToString();
        ManipuleazaBD.InchideConexiune(vCon);
        tbLogin.Text = "";
        tbParola.Text = ""; tbConfirmaParola.Text = "";
        ddTipUtilizator.DataBind();
        try { ddTipUtilizator.SelectedValue = "3"; }
        catch { }
        tbUtilizatorEmail.Text = "";
        tbUtilizatorNume.Text = "";
        tbUtilizatorPrenume.Text = "";
        tbUtilizatorAdresa.Text = "";
        tbUtilizatorFunctia.Text = "";
        tbUtilizatorCNP.Text = "";
        tbUtilizatorTelefon.Text = "";
        tbUtilizatorMobil.Text = "";
        tbDurataSesiuneLucru.Text = "30";
        ddUtilizatorActiv.SelectedValue = "1";

    }
    protected void gvUtilizatori_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(gvUtilizatori, e, this);
    }
    protected void PopulamCampuriUtilizatori()
    {

    }
    protected void ArataPanelModificaUtilizator(object sender, EventArgs e)
    {
        // venim din lista unitati si populam panelul de dauga modifica cu datele existente
        // paneluri
        pnListaUtilizatori.Visible = false;
        pnAdaugaUtilizator.Visible = true;
        // butoanele din adauga/modifica
        btAdaugaModificaUtilizator.Visible = true; // butonul de modifica
        btAdaugaUtilizator.Visible = false; // butonul de adauga - ascundem
        btModificaArataListaUtilizatori.Visible = true;
        btAdaugaArataListaUtilizatori.Visible = false;
        // la modificare nu mai obligam la parola
        valParola.Enabled = false;
        valConfirmareParola.Enabled = false;
        // luam id-ul liniei selectate in GV:
        int vUtilizatorId = Convert.ToInt32(gvUtilizatori.SelectedValue);
        // populam cu datele selectate paneul de adaugare
        // facem un select pentru extragerea tuturor datelor  => id
        string vInterogareSelect = "SELECT utilizatori.utilizatorIp, utilizatori.utilizatorId, utilizatori.utilizatorLogin, utilizatori.utilizatorNume, utilizatori.utilizatorPrenume, utilizatori.utilizatorFunctia, utilizatori.utilizatorAdresa, utilizatori.utilizatorCNP, utilizatori.utilizatorTelefon, utilizatori.utilizatorMobil, utilizatori.unitateId, utilizatori.utilizatorActiv, utilizatori.utilizatorEmail, utilizatori.utilizatorParola, utilizatori.tipUtilizatorId, unitati.judetId, coalesce(utilizatori.utilizatorDurataSesiune,30) as utilizatorDurataSesiune FROM utilizatori INNER JOIN unitati ON utilizatori.unitateId = unitati.unitateId WHERE utilizatori.utilizatorId = " + vUtilizatorId;
        string strConn = ConfigurationManager.ConnectionStrings["TNTRegistruAgricolConnectionString"].ConnectionString;
        SqlConnection vCon = new SqlConnection(strConn);
        SqlCommand vCmd1 = new SqlCommand();
        SqlCommand vCmd2 = new SqlCommand();
        vCmd1.Connection = vCon;
        vCmd2.Connection = vCon;
        vCmd2.CommandType = System.Data.CommandType.Text;
        vCmd2.CommandText = vInterogareSelect;
        vCon.Open();
        try
        {
            SqlDataReader vLinieDeModificat = vCmd2.ExecuteReader();
            if (vLinieDeModificat.Read())
            {
                // daca este utilizator de tip supervizor judetean sau functionar cu drepturi pe pagina afisez numai judetul lui si nu las sa modifice judetul
                vCmd1.CommandText = "select top(1) tipUtilizatorId from utilizatori where utilizatorId='" + Session["SESutilizatorId"].ToString() + "'";
                int vTipUtilizator = Convert.ToInt32(vCmd1.ExecuteScalar());
                if (vTipUtilizator == 2 || vTipUtilizator == 3 || vTipUtilizator == 5)
                    ddlJudet.Enabled = false;
                else ddlJudet.Enabled = true;
                // populam fiecare camp
                ddlJudet.Items.Clear();
                ddlJudet.Items.Add(new ListItem("-toate-", "%"));
                ddlJudet.DataBind();
                ddlJudet.SelectedValue = vLinieDeModificat["judetId"].ToString();
                ddUnitate.Items.Clear();
                ddUnitate.DataBind();
                ddUnitate.SelectedValue = vLinieDeModificat["unitateId"].ToString();
                tbLogin.Text = vLinieDeModificat["utilizatorLogin"].ToString(); lblLoginOriginal.Text = tbLogin.Text;
                tbParola.Text = ""; tbConfirmaParola.Text = "";
                ddTipUtilizator.SelectedValue = vLinieDeModificat["tipUtilizatorId"].ToString();
                tbUtilizatorEmail.Text = vLinieDeModificat["utilizatorEmail"].ToString();
                tbUtilizatorNume.Text = vLinieDeModificat["utilizatorNume"].ToString();
                tbUtilizatorPrenume.Text = vLinieDeModificat["utilizatorPrenume"].ToString();
                tbUtilizatorAdresa.Text = vLinieDeModificat["utilizatorAdresa"].ToString();
                tbUtilizatorFunctia.Text = vLinieDeModificat["utilizatorFunctia"].ToString();
                tbUtilizatorCNP.Text = vLinieDeModificat["utilizatorCNP"].ToString();
                tbUtilizatorTelefon.Text = vLinieDeModificat["utilizatorTelefon"].ToString();
                tbUtilizatorMobil.Text = vLinieDeModificat["utilizatorMobil"].ToString();
                ddUtilizatorActiv.SelectedValue = vLinieDeModificat["utilizatorActiv"].ToString();
                tbDurataSesiuneLucru.Text = vLinieDeModificat["utilizatorDurataSesiune"].ToString();
            }
        }
        catch { }
        vCon.Close();
    }
    protected void StergeUtilizator(object sender, EventArgs e)
    {
        string vUtilizatorId = gvUtilizatori.SelectedValue.ToString();
        if (ManipuleazaBD.fVerificaExistenta("LogOperatii", "utilizatorId", vUtilizatorId, Convert.ToInt16(Session["SESan"])) == 0)
        {
            ManipuleazaBD.fManipuleazaBD("DELETE FROM [utilizatori] WHERE [utilizatorId] = '" + gvUtilizatori.SelectedValue.ToString() + "'", Convert.ToInt16(Session["SESan"]));
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "utilizatori", "| modificare utilizator id: " + gvUtilizatori.SelectedValue.ToString(), "", Convert.ToInt64(Session["SESgospodarieId"]), 3);

            gvUtilizatori.DataBind();
            gvUtilizatori.SelectedIndex = -1;
            btStergeUtilizator.Visible = false;
            btModificaUtilizator.Visible = false;
            btArataModificaDrepturiUtilizator.Visible = false;

        }
        else
        {
            vExpresie = "Utilizatorul are activitate desfășurată, prin urmare nu poate fi șters!";
            valCustom.IsValid = false;
            valCustom.ErrorMessage = vExpresie;
            btModificaUtilizator.Visible = true;
            btArataModificaDrepturiUtilizator.Visible = true;
            btStergeUtilizator.Visible = true;
        }
    }
    protected void ArataAdaugaUtilizatori(object sender, EventArgs e)
    {
        // paneluri
        InitializeazaCampuri(0);

        pnAdaugaUtilizator.Visible = true;
        pnListaUtilizatori.Visible = false;
        btAdaugaUtilizator.Visible = true;
        btAdaugaArataListaUtilizatori.Visible = true;
        // btAdaugaArataListaUtilizatori.Text = "vezi lista (adauga)";
        btModificaArataListaUtilizatori.Visible = false;
        btAdaugaModificaUtilizator.Visible = false;
        valParola.Enabled = true;
        valConfirmareParola.Enabled = true;
        // upUtilizatori.DataBind();
    }

    protected void ModificaArataListaUtilizatori(object sender, EventArgs e)
    {
        InitializeazaCampuri(0);
        pnAdaugaUtilizator.Visible = false;
        pnListaUtilizatori.Visible = true;
        pnModificaDrepturiUtilizator.Visible = false;

        btModificaUtilizator.Visible = true;
        btArataModificaDrepturiUtilizator.Visible = true;
        btStergeUtilizator.Visible = true;
    }
    protected void AdaugaArataListaUtilizatori(object sender, EventArgs e)
    {
        gvUtilizatori.SelectedIndex = -1;
        pnAdaugaUtilizator.Visible = false;
        pnListaUtilizatori.Visible = true;
        btArataModificaDrepturiUtilizator.Visible = false;

        btModificaUtilizator.Visible = false;
        btStergeUtilizator.Visible = false;
        InitializeazaCampuri(1);
    }
    protected void gvUtilizatori_SelectedIndexChanged(object sender, EventArgs e)
    {
        // actiuni la selectare rand; aratam butoanele de sterge si modifica
        if (gvUtilizatori.SelectedRow.Cells[10].Text == "administrator" && ManipuleazaBD.fRezultaUnString("SELECT unitateId FROM utilizatori WHERE utilizatorId = '" + Session["SESutilizatorId"].ToString() + "'", "unitateId", Convert.ToInt16(Session["SESan"])) != ManipuleazaBD.fRezultaUnString("SELECT unitateId FROM unitati WHERE unitateDenumire = '" + gvUtilizatori.SelectedRow.Cells[0].Text + "'", "unitateId", Convert.ToInt16(Session["SESan"])))
        {
            btAdaugaUtilizator.Visible = true;
            btModificaUtilizator.Visible = false;
            btArataModificaDrepturiUtilizator.Visible = false;
            btStergeUtilizator.Visible = false;
            btStergeCoduri.Visible = false;
            gvUtilizatori.SelectedIndex = -1;
        }
        else
        {
            btAdaugaUtilizator.Visible = true;
            btModificaUtilizator.Visible = true;
            btArataModificaDrepturiUtilizator.Visible = true;
            btStergeUtilizator.Visible = true;
            btStergeCoduri.Visible = true;
            gvUtilizatori.DataBind();
        }
    }
    protected void gvUtilizatori_DataBound(object sender, EventArgs e)
    {
        // luam denumirea unitatii
        for (int i = 0; i < gvUtilizatori.Rows.Count; i++)
        {
            if (gvUtilizatori.Rows[i].RowType == DataControlRowType.DataRow)
            {
                try
                {
                    //int vUnitateId = Convert.ToInt32(gvUtilizatori.Rows[i].Cells[0].Text);
                    // string vInterogare = "SELECT unitateDenumire FROM unitati WHERE unitateId='" + vUnitateId.ToString() + "'";
                    // gvUtilizatori.Rows[i].Cells[0].Text = ManipuleazaBD.fRezultaUnString(vInterogare, "unitateDenumire");
                    int vActiv = Convert.ToInt32(gvUtilizatori.Rows[i].Cells[11].Text);
                    if (vActiv == 1) { gvUtilizatori.Rows[i].Cells[11].Text = "da"; } else { gvUtilizatori.Rows[i].Cells[11].Text = "nu"; }


                }
                catch { }
            }
        }
    }
    protected void ArataPanelModificaDrepturiUtilizator(object sender, EventArgs e)
    {
        int vUtilizatorId = Convert.ToInt32(gvUtilizatori.SelectedRow.Cells[2].Text);
        PopuleazaDrepturiUtilizator(vUtilizatorId);
        //clsUtilizatori.CitesteDrepturi(vUtilizatorId);
        pnModificaDrepturiUtilizator.Visible = true;
        pnListaUtilizatori.Visible = false;
        pnAdaugaUtilizator.Visible = false;
        //  SalveazaDrepturiUtilizatori(vUtilizatorId);
    }
    protected void PopuleazaDrepturiUtilizator(int pUtilizatorId)
    {
        string vInterogare = "";
        int j = gvDrepturiUtilizatori.Rows.Count;
        lblNumeUtilizator.Text = j.ToString();
        for (int i = 0; i < gvDrepturiUtilizatori.Rows.Count; i++)
        {
            if (gvDrepturiUtilizatori.Rows[i].RowType == DataControlRowType.DataRow)
            {
                // veificam daca avem inregistrare pentru pagina respectiva
                // si pt utilizatorul ales
                Label vPaginaId = (Label)gvDrepturiUtilizatori.Rows[i].FindControl("paginaId");
                vInterogare = "SELECT TOP(1) utilizatorId, paginaId, dreptCitire, dreptModificare, dreptTiparire, dreptCreare, dreptStergere FROM drepturiUtilizatori WHERE paginaId = '" + vPaginaId.Text + "' AND utilizatorId='" + pUtilizatorId.ToString() + "'";
                string strConn = ConfigurationManager.ConnectionStrings["TNTRegistruAgricolConnectionString"].ConnectionString;
                SqlConnection con = new SqlConnection(strConn);
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = vInterogare;
                con.Open();
                try
                {
                    CheckBox vCbCitire = (CheckBox)gvDrepturiUtilizatori.Rows[i].FindControl("cbCitire");
                    CheckBox vCbModificare = (CheckBox)gvDrepturiUtilizatori.Rows[i].FindControl("cbModificare");
                    CheckBox vCbTiparire = (CheckBox)gvDrepturiUtilizatori.Rows[i].FindControl("cbTiparire");
                    CheckBox vCbCreare = (CheckBox)gvDrepturiUtilizatori.Rows[i].FindControl("cbCreare");
                    CheckBox vCbStergere = (CheckBox)gvDrepturiUtilizatori.Rows[i].FindControl("cbStergere");
                    SqlDataReader vLinieDrepturi = cmd.ExecuteReader();
                    if (vLinieDrepturi.Read())
                    {
                        // daca exista in baza de date populam cu ce exista
                        // daca nu exista populam goale
                        if (vLinieDrepturi["dreptCitire"].ToString() == "0") { vCbCitire.Checked = false; } else { vCbCitire.Checked = true; }
                        if (vLinieDrepturi["dreptModificare"].ToString() == "0") { vCbModificare.Checked = false; } else { vCbModificare.Checked = true; }
                        if (vLinieDrepturi["dreptStergere"].ToString() == "0") { vCbStergere.Checked = false; } else { vCbStergere.Checked = true; }
                        if (vLinieDrepturi["dreptCreare"].ToString() == "0") { vCbCreare.Checked = false; } else { vCbCreare.Checked = true; }
                        if (vLinieDrepturi["dreptTiparire"].ToString() == "0") { vCbTiparire.Checked = false; } else { vCbTiparire.Checked = true; }
                    }
                    else
                    {
                        vCbCitire.Checked = true;
                        vCbModificare.Checked = false;
                        vCbStergere.Checked = false;
                        vCbCreare.Checked = false;
                        vCbTiparire.Checked = false;
                    }
                }
                catch
                {
                }
                con.Close();
            }
            else { }
        }
    }
    protected void SalveazaDrepturiUtilizatori(int pUtilizatorId)
    {
        string vInterogareUpdate = "";
        string vInterogareInsert = "";
        for (int i = 0; i < gvDrepturiUtilizatori.Rows.Count; i++)
        {
            vInterogareUpdate = ""; vInterogareInsert = "";
            if (gvDrepturiUtilizatori.Rows[i].RowType == DataControlRowType.DataRow)
            {
                // verificam daca exista deja linia
                // citim utilizatorId si pagina id                    
                CheckBox vCbCitire = (CheckBox)gvDrepturiUtilizatori.Rows[i].FindControl("cbCitire");
                CheckBox vCbModificare = (CheckBox)gvDrepturiUtilizatori.Rows[i].FindControl("cbModificare");
                CheckBox vCbTiparire = (CheckBox)gvDrepturiUtilizatori.Rows[i].FindControl("cbTiparire");
                CheckBox vCbCreare = (CheckBox)gvDrepturiUtilizatori.Rows[i].FindControl("cbCreare");
                CheckBox vCbStergere = (CheckBox)gvDrepturiUtilizatori.Rows[i].FindControl("cbStergere");
                Label vPaginaId = (Label)gvDrepturiUtilizatori.Rows[i].FindControl("paginaId");
                string[] vCampuri = { "utilizatorId", "paginaId" };
                string[] vValori = { pUtilizatorId.ToString(), vPaginaId.Text };
                if (ManipuleazaBD.fVerificaExistentaArray("drepturiUtilizatori", vCampuri, vValori, " AND ", Convert.ToInt16(Session["SESan"])) == 0)
                {
                    vInterogareInsert = "INSERT INTO drepturiUtilizatori (paginaId, dreptCitire, dreptModificare, dreptTiparire, dreptCreare, dreptStergere,utilizatorId) VALUES ('";
                    vInterogareInsert += vPaginaId.Text.ToString() + "',";

                    if (vCbCitire.Checked == false) { vInterogareInsert += "'0',"; } else { vInterogareInsert += "'1',"; }
                    if (vCbModificare.Checked == false) { vInterogareInsert += "'0',"; } else { vInterogareInsert += "'1',"; }
                    if (vCbTiparire.Checked == false) { vInterogareInsert += "'0',"; } else { vInterogareInsert += "'1',"; }
                    if (vCbCreare.Checked == false) { vInterogareInsert += "'0',"; } else { vInterogareInsert += "'1',"; }
                    if (vCbStergere.Checked == false) { vInterogareInsert += "'0','"; } else { vInterogareInsert += "'1','"; }
                    vInterogareInsert += pUtilizatorId.ToString() + "')";
                    ManipuleazaBD.fManipuleazaBD(vInterogareInsert, Convert.ToInt16(Session["SESan"]));
                    ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "utilizatoriDrepturi", "| adaugare drepturi utilizator/id: " + pUtilizatorId.ToString(), "", Convert.ToInt64(Session["SESgospodarieId"]), 2);

                }
                else
                {
                    vInterogareUpdate = "UPDATE drepturiUtilizatori SET ";
                    if (vCbCitire.Checked == false) { vInterogareUpdate += " dreptCitire = '0' "; } else { vInterogareUpdate += " dreptCitire = '1' "; }
                    if (vCbModificare.Checked == false) { vInterogareUpdate += ", dreptModificare = '0' "; } else { vInterogareUpdate += ", dreptModificare = '1' "; }
                    if (vCbTiparire.Checked == false) { vInterogareUpdate += ", dreptTiparire = '0' "; } else { vInterogareUpdate += ", dreptTiparire = '1' "; }
                    if (vCbCreare.Checked == false) { vInterogareUpdate += ", dreptCreare = '0' "; } else { vInterogareUpdate += ", dreptCreare = '1' "; }
                    if (vCbStergere.Checked == false) { vInterogareUpdate += ", dreptStergere = '0' "; } else { vInterogareUpdate += ", dreptStergere = '1' "; }
                    vInterogareUpdate += " WHERE utilizatorId = '" + pUtilizatorId.ToString() + "' AND paginaId = '" + vPaginaId.Text.ToString() + "' ";
                    ManipuleazaBD.fManipuleazaBD(vInterogareUpdate, Convert.ToInt16(Session["SESan"]));
                    ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "utilizatoriDrepturi", "| modificare drepturi utilizator/id: " + pUtilizatorId.ToString(), "", Convert.ToInt64(Session["SESgospodarieId"]), 3);
                }
            }
            else { }
        }
    }
    protected void ModificaDrepturiUtilizator(object sender, EventArgs e)
    {
        int vUtilizatorId = Convert.ToInt32(gvUtilizatori.SelectedRow.Cells[2].Text);
        SalveazaDrepturiUtilizatori(vUtilizatorId);
        pnAdaugaUtilizator.Visible = false;
        pnListaUtilizatori.Visible = true;
        pnModificaDrepturiUtilizator.Visible = false;
        btModificaUtilizator.Visible = true;
        btArataModificaDrepturiUtilizator.Visible = true;
        btStergeUtilizator.Visible = true;
    }
    protected void SqlUnitati_Init(object sender, EventArgs e)
    {
        //SqlUnitati.SelectCommand = "";
        //SqlUnitati.SelectParameters["pDenUnitate"].DefaultValue = ManipuleazaBD.fRezultaUnString("SELECT unitateDenumire FROM unitati WHERE unitateId='" + Session["unitateId"].ToString() + "'", "unitateDenumire");
        ;
    }
    protected void SqlDdUnitate_Init(object sender, EventArgs e)
    {
        /*       string vUnitatePrincipala = ManipuleazaBD.fRezultaUnString("SELECT unitatePrincipala FROM unitati WHERE unitateId = '" + Session["unitateId"].ToString() + "'", "unitatePrincipala");
               switch (vUnitatePrincipala)
               {
                   case "1":
                       SqlDdUnitate.SelectCommand = "SELECT [unitateId], [unitateDenumire] FROM [unitati] WHERE unitatePrincipala != '2'";
                       break;
                   case "2":
                       SqlDdUnitate.SelectCommand = "SELECT [unitateId], [unitateDenumire] FROM [unitati]"; 
                       break;
                   default:
                       SqlDdUnitate.SelectCommand = "SELECT [unitateId], [unitateDenumire] FROM [unitati] WHERE unitatePrincipala = '" + Session["unitateId"].ToString() + "'"; 
                       ddUnitate.SelectedValue = Session["unitateId"].ToString();
                       break;
               }*/

    }
    protected void SqlDdUnitate_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {

    }
    protected void SqlUnitati_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        // daca utilizatorul este supervizor judetean sau functionar cu drepturi pe pagina are dreptul sa vizualizeze doar unitatile din judet
        vCmd.CommandText = "SELECT TOP (1) unitati.judetId FROM utilizatori INNER JOIN unitati ON utilizatori.unitateId = unitati.unitateId WHERE (utilizatori.utilizatorId = '" + Session["SESutilizatorId"].ToString() + "')";
        string vJudetId = vCmd.ExecuteScalar().ToString();
        vCmd.CommandText = "select top(1) tipUtilizatorId from utilizatori where utilizatorId='" + Session["SESutilizatorId"].ToString() + "'";
        int vTipUtilizator = Convert.ToInt32(vCmd.ExecuteScalar());
        ManipuleazaBD.InchideConexiune(vCon);
        if (vTipUtilizator == 1 || vTipUtilizator == 4)
            vJudetId = "%";
        e.Command.CommandText = "SELECT [utilizatori].[utilizatorLogin], [utilizatori].[utilizatorId], [utilizatori].[utilizatorNume], [utilizatori].[utilizatorPrenume], [utilizatori].[utilizatorFunctia], [utilizatori].[utilizatorMobil], [utilizatori].[utilizatorTelefon], [utilizatori].[utilizatorCNP], [utilizatori].[utilizatorEmail], [utilizatori].[utilizatorActiv], [utilizatori].[unitateId], [unitati].[unitateDenumire], [tipuriUtilizatori].[tipUtilizatorDenumire] FROM [utilizatori] LEFT OUTER JOIN [unitati] ON [utilizatori].[unitateId] = [unitati].[unitateId] LEFT OUTER JOIN [tipuriUtilizatori] ON [utilizatori].[tipUtilizatorId]=[tipuriUtilizatori].[tipUtilizatorId] WHERE ([unitati].[unitateDenumire] like (N'%" + ((tbCUnitate.Text == "") ? "%" : tbCUnitate.Text) + "%')) AND ([utilizatori].[utilizatorNume] like ('%" + ((tbCNume.Text == "") ? "%" : tbCNume.Text) + "%')) AND ([utilizatori].[utilizatorLogin] like ('%" + ((tbCLogin.Text == "") ? "%" : tbCLogin.Text) + "%') AND (CONVERT(nvarchar,judetId) like '" + vJudetId + "'))";
    }
    protected void ddlJudet_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddUnitate.DataBind();
    }
    protected void btStergeCoduri_Click(object sender, EventArgs e)
    {
        // stergem codurile din utilizatorFursec
        ManipuleazaBD.fManipuleazaBD("UPDATE utilizatori SET utilizatorFursec = '' WHERE utilizatorId ='" + gvUtilizatori.SelectedValue + "' ", Convert.ToInt16(Session["SESan"]));

    }
    protected void btStergeIp_Click(object sender, EventArgs e)
    {
        // deblocam ip-ul
        popup.Visible = true;

    }
    protected void btStergeDeblocheaza_Click(object sender, EventArgs e)
    {
        ClassLog.fLog(0, 0, DateTime.Now, "LOGIN", tbIp.Text + " |DEBLOCARE|", "REUSIT", 0, 8, tbIp.Text);
        popup.Visible = false;
    }
    protected void testbutton_Click(object sender, EventArgs e)
    {

    }
    protected void modificaUserButton_Click(object sender, EventArgs e)
    {

    }
}
