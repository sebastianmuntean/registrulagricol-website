﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;
using System.Data.SqlClient;

public partial class printTerenuriInProprietate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "printMatricolaTer", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
           
                DataTable reportDataTable = GetDataTAbleForReport();
                DataTable unitatiDataTable = GetUnitateDenumire();
                ReportViewer1.LocalReport.DataSources.Clear();
                ReportDataSource ds = new ReportDataSource("dsRapoarte_rapTerenuriProprietate", reportDataTable);
                ReportDataSource ds2 = new ReportDataSource("dsRapoarte_dtUnitati", unitatiDataTable);
                ReportViewer1.LocalReport.DataSources.Add(ds);
                ReportViewer1.LocalReport.DataSources.Add(ds2);
                ReportViewer1.LocalReport.Refresh();
                ReportViewer1.Visible = true;
            
        }
    }

    private DataTable GetUnitateDenumire()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        string interogare = @"SELECT        unitati.unitateId, unitati.unitateDenumire, unitati.unitateCodSiruta, unitati.judetId, unitati.localitateId, unitati.unitateStrada, unitati.unitateNr, unitati.unitateAp, 
                         unitati.unitateCodPostal, unitati.localitateComponentaId, unitati.unitateCodFiscal, unitati.unitateActiva, unitati.unitatePrincipala, judete.judetDenumire, 
                         localitati.localitateDenumire
FROM            unitati INNER JOIN
                         judete ON unitati.judetId = judete.judetId INNER JOIN
                         localitati ON unitati.localitateId = localitati.localitateId
WHERE        (unitati.unitateId = " + Session["SESunitateId"].ToString() + ")";
        SqlDataAdapter adapter = new SqlDataAdapter(interogare, connection);
        DataTable table = new DataTable();
        adapter.Fill(table);
        ManipuleazaBD.InchideConexiune(connection);
        return table;
    }

    private DataTable GetDataTAbleForReport()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        string interogare = @"SELECT        raportId, unitateId, utilizatorId, raportNume, raportAdresa, raportVolum, raportPozitia, raportArabil, raportPasuni, raportFanete, raportLivezi, raportVii, raportPaduri, raportTufaris, raportCurti, raportGradini, 
                         raportAdresaS,cnp
FROM            rapTerenuriProprietate
WHERE        (utilizatorId = '"+HttpContext.Current.Session["SESUtilizatorId"]+"')";
        SqlDataAdapter adapter = new SqlDataAdapter(interogare, connection);
        DataTable table = new DataTable();
        adapter.Fill(table);
        ManipuleazaBD.InchideConexiune(connection);
        return table;
    }

    /*protected void Page_SaveStateComplete(object sender, EventArgs e)
    {
        Warning[] warnings;
        string[] streamids;
        string mimeType;
        string encoding;
        string fileNameExtension;
        byte[] bytes = ReportViewer1.LocalReport.Render("PDF", null, out mimeType, out encoding, out fileNameExtension, out streamids, out warnings);
        Response.Buffer = true;
        Response.Clear();
        Response.ContentType = mimeType;
        Response.AddHeader("content-disposition", "inline; filename=Nume" + fileNameExtension);
        Response.BinaryWrite(bytes);
        Response.Flush();
        Response.End();
        Response.Close();
    }*/
    protected void ObjectDataSource1_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        //if (Request.QueryString["val"] == "0")
        //     ObjectDataSource1.SelectMethod = "GetData";
        //else if (Request.QueryString["val"] == "1")
        //    ObjectDataSource1.SelectMethod = "GetData1";
        //else if (Request.QueryString["val"] == "2")
        //    ObjectDataSource1.SelectMethod = "GetData2";
        //else if (Request.QueryString["val"] == "3")
        //    ObjectDataSource1.SelectMethod = "GetData3";

     }

}
