﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

public partial class Capitol2b: System.Web.UI.Page
{
    #region initializare-limbi
   
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if ( vCookie1 != null )
        {
            if ( vCookie1["COOKlimba"] != null )
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if ( Session["SESlimba"] == null )
        {
            Session["SESlimba"] = "ro-RO";
            if ( vCookie1 != null )
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if ( Session["SESlimba"] != null )
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();
    }
    
    #endregion
    
    protected void Page_Load(object sender, EventArgs e)
    {
        DBConnections connection = new DBConnections(Convert.ToInt16(Session["SESan"]));

        SqlParcele.ConnectionString = connection.Create();
        sqlParcelePrimite.ConnectionString = connection.Create();
        SqlCoordonate.ConnectionString = connection.Create();
        SqlIstoricParcela.ConnectionString = connection.Create();

        if (Session["SESgospodarieId"] == null)
        {
            Response.Redirect("gospodarii.aspx");
        }
        
        if ( !IsPostBack )
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "Cap.2b", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
            ViewState["contractIds"] = null;
            ViewState["parcela"] = null;

            btAtribuireCulturi.Visible = false;
        }

        string unitateAdaugaDinC2BC4abc = ManipuleazaBD.fRezultaUnString("select top(1) unitateAdaugaDinC2BC4abc from unitati where unitateId='" + Session["SESunitateId"].ToString() + "'", "unitateAdaugaDinC2BC4abc", Convert.ToInt16(Session["SESan"]));

        ViewState["unitateAdaugaDinC2BC4abc"] = unitateAdaugaDinC2BC4abc;
    }

    #region Parcele

    protected void BtStergeParcelaClick(object sender, EventArgs e)
    {
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        
        string vParcelaId = gvParcele.SelectedValue.ToString();

        if (ViewState["unitateAdaugaDinC2BC4abc"] == "True")
        {
            // stergem din 4a, 4b, 4c daca are atribuiri
            string vExistaAtribuiri4a = ManipuleazaBD.fRezultaUnString("SELECT COUNT(*) as numar4a FROM parceleCatreCapitole WHERE gospodarieId='" + Session["SESgospodarieId"].ToString() + "' AND anul = '" + Session["SESan"].ToString() + "' AND parcelaId='" + ((Label)gvParcele.SelectedRow.Cells[13].FindControl("lblParcelaId")).Text + "'", "numar4a", Convert.ToInt16(Session["SESan"]));
            
            if ( vExistaAtribuiri4a != "0" )
            {
                List<string> vCampuri = new List<string>
                {
                    "id",
                    "c4aRand",
                    "c4aHa",
                    "c4aAri",
                    "c4bRand",
                    "c4bHa",
                    "c4bAri",
                    "c4cRand",
                    "c4cHa",
                    "c4cAri"
                };
                List<List<string>> vListaAtribuiri = ManipuleazaBD.fRezultaListaStringuri("SELECT * FROM parceleCatreCapitole WHERE parcelaId = '" + vParcelaId + "' AND anul ='" + Session["SESan"].ToString() + "'", vCampuri, Convert.ToInt16(Session["SESan"]));
                
                foreach ( List<string> vAtribuire in vListaAtribuiri )
                {
                    StergeAtribuire("4a", vAtribuire[1], vAtribuire[2], vAtribuire[3]);
                    StergeAtribuire("4b", vAtribuire[4], vAtribuire[5], vAtribuire[6]);
                    StergeAtribuire("4c", vAtribuire[7], vAtribuire[8], vAtribuire[9]);
                }
            }

            ManipuleazaBD.fManipuleazaBD("DELETE FROM parceleCatreCapitole WHERE parcelaId = '" + vParcelaId + "'", Convert.ToInt16(Session["SESan"]));
        }

        if (ManipuleazaBD.fRezultaUnString("select top(1) unitateAdaugaDinC2BC3 from unitati where unitateId='" + Session["SESunitateId"].ToString() + "'", "unitateAdaugaDinC2BC3", Convert.ToInt16(Session["SESan"])) == "True")
        {
            // stergem din 3 daca are atribuiri de mod utilizare, pentru gospodariile care dau si care primesc
            string vExistaAtribuiri3 = ManipuleazaBD.fRezultaUnString("SELECT COUNT(*) as numar3 FROM parceleModUtilizare WHERE anul = '" + Session["SESan"].ToString() + "' AND parcelaId='" + vParcelaId + "'", "numar3", Convert.ToInt16(Session["SESan"]));
            if ( vExistaAtribuiri3 != "0" )
            {
                // citim toate atribuirile legate de parcela
                List<string> vCampuri = new List<string>
                {
                    "modUtilizareid",
                    "c3Rand",
                    "c3Ha",
                    "c3Ari",
                    "gospodarieId",
                    "catreGospodarieId",
                    "catreC3Rand"
                };
                List<List<string>> vListaAtribuiri = ManipuleazaBD.fRezultaListaStringuri("SELECT * FROM parceleModUtilizare WHERE parcelaId = '" + vParcelaId + "' AND anul ='" + Session["SESan"].ToString() + "'", vCampuri, Convert.ToInt16(Session["SESan"]));
                foreach (List<string> vAtribuire in vListaAtribuiri)
                {
                    // stergem pt cel ce da
                    StergeModUtilizare("3", vAtribuire[1], vAtribuire[2], vAtribuire[3], vAtribuire[4], false, vAtribuire[5]);
                    // stergem pt cel ce primeste
                    StergeModUtilizare("3", vAtribuire[6], vAtribuire[2], vAtribuire[3], vAtribuire[5], false, "");
                }
                // stergem din modutilizare
                ManipuleazaBD.fManipuleazaBD("DELETE FROM parceleModUtilizare WHERE parcelaId = '" + vParcelaId + "'", Convert.ToInt16(Session["SESan"]));
            }
        }
        ManipuleazaBD.fManipuleazaBD("DELETE FROM [parcele] WHERE [parcelaId] = '" + vParcelaId + "'", Convert.ToInt16(Session["SESan"]));
        ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "parcele", "stergere capitol 2b parcele", "parcela ID: " + vParcelaId, Convert.ToInt64(Session["SESgospodarieId"]), 4);
        ClassLog.UpdateDataUltimeiModificari(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESgospodarieId"]));
        // daca am bifa pe unitati adaug in C2A din C2B
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select top(1) unitateAdaugaDinC2B from unitati where unitateId='" + Session["SESunitateId"].ToString() + "'";
        bool vAdaugaDinC2b = Convert.ToBoolean(vCmd.ExecuteScalar());
        vCmd.CommandText = "select top(1) unitateAdaugaDinC2BC3 from unitati where unitateId='" + Session["SESunitateId"].ToString() + "'";
        bool vAdaugaDinC2bInC3 = Convert.ToBoolean(vCmd.ExecuteScalar());
        ManipuleazaBD.InchideConexiune(vCon);
        if ( vAdaugaDinC2b )
            if ( vAdaugaDinC2bInC3 )
                CalculCapitole.AdaugaCapitol2ADin2B(Session["SESunitateId"].ToString(), Session["SESan"].ToString(), Session["SESgospodarieId"].ToString(), "1");
            else
                CalculCapitole.AdaugaCapitol2ADin2B(Session["SESunitateId"].ToString(), Session["SESan"].ToString(), Session["SESgospodarieId"].ToString(), "0");
        clsCorelatii.VerificaCorelatie(Convert.ToInt32(Session["SESunitateId"]), Convert.ToInt32(Session["SESgospodarieId"]), Convert.ToInt32(Session["SESan"]), "parcele");
        ((MasterPage)this.Page.Master).VerificaCorelatii();
        gvParcele.DataBind();
        gvParcele.SelectedIndex = -1;
        gvParcelePrimite.SelectedIndex = -1;
        btStergeParcela.Visible = false;
        btModificaParcela.Visible = false;
    }

    protected void StergeModUtilizare(string pCapitol, string pRand, string pHa, string pAri, string pGospodarie, bool pAfisamDdl, string pCatreGospodarie)
    {
        // facem acelasi lucru pt cel care primeste si cel care da, cu pRand facem diferenta, la apelare de doua ori
        List<string> vCampuri = new List<string> { };
        List<string> vValori = new List<string> { };
        decimal vSuprafataInitiala = 0;
        decimal vSuprafataDinTabel = 0;
        decimal vSuprafataDeScris = 0;
        string[] vSuprafataDeScrisString = { "0", "0" };
        // gasim linia de capitol pentru randul care se va updata
        vCampuri = new List<string> { "capitolId", "col1", "col2" };
        vValori = ManipuleazaBD.fRezultaUnString("SELECT capitole.capitolId, capitole.col1, capitole.col2 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + pGospodarie + "') AND (sabloaneCapitole.capitol = '" + pCapitol + "') AND (capitole.codRand = '" + pRand + "')", vCampuri, Convert.ToInt16(Session["SESan"]));
        // calculam suprafata in ari
        vSuprafataInitiala = Convert.ToDecimal(vValori[1].Replace('.', ',')) * 100 + Convert.ToDecimal(vValori[2].Replace('.', ','));
        vSuprafataDinTabel = Convert.ToDecimal(pHa.Replace('.', ',')) * 100 + Convert.ToDecimal(pAri.Replace('.', ','));
        vSuprafataDeScris = vSuprafataInitiala - vSuprafataDinTabel;
        if (vSuprafataDeScris < 0) vSuprafataDeScris = 0;
        vSuprafataDeScrisString = CalculeazaAriHa("0", "0", vSuprafataDeScris.ToString(), "0");
        ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vSuprafataDeScrisString[0] + "', col2 = CONVERT(decimal(18,4),'" + vSuprafataDeScrisString[1].Replace(',', '.') + "') WHERE (capitolId = '" + vValori[0] + "')", Convert.ToInt16(Session["SESan"]));
        // verificam daca stergem pentru cel ce da si cel ce primeste e persoana juridica; 
        // daca este atunci modificam si pt randul 16 la cel ce da
        if (pCatreGospodarie != "")
        {
            if (PersoanaJuridica(pCatreGospodarie))
            {
                // gasim linia de capitol pentru randul 16 care se va updata pentru cel care "dă"
                vCampuri = new List<string> { "capitolId", "col1", "col2" };
                vValori = ManipuleazaBD.fRezultaUnString("SELECT capitole.capitolId, capitole.col1, capitole.col2 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + pGospodarie + "') AND (sabloaneCapitole.capitol = '3') AND (capitole.codRand = '16')", vCampuri, Convert.ToInt16(Session["SESan"]));
                // calculam suprafata in ari
                vSuprafataInitiala = Convert.ToDecimal(vValori[1].Replace('.', ',')) * 100 + Convert.ToDecimal(vValori[2].Replace('.', ','));
                vSuprafataDinTabel = Convert.ToDecimal(pHa.Replace('.', ',')) * 100 + Convert.ToDecimal(pAri.Replace('.', ','));
                vSuprafataDeScris = vSuprafataInitiala - vSuprafataDinTabel;
                if (vSuprafataDeScris < 0) vSuprafataDeScris = 0;
                vSuprafataDeScrisString = CalculeazaAriHa("0", "0", vSuprafataDeScris.ToString(), "0");
                ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vSuprafataDeScrisString[0] + "', col2 = CONVERT(decimal(18,4),'" + vSuprafataDeScrisString[1].Replace(',', '.') + "') WHERE (capitolId = '" + vValori[0] + "')", Convert.ToInt16(Session["SESan"]));
            }
        }

        // afisam ddlurile sau nu 
        if (pAfisamDdl)
        {
            //ddlModUtilizareCatreNrPozitie.Enabled = true;
            //ddlModUtilizareCatreVolum.Enabled = true;
            //ddlModUtilizare.Enabled = true;
        }
    }

    protected bool PersoanaJuridica(string pGospodarie)
    {
        if (ManipuleazaBD.fRezultaUnString("SELECT persJuridica FROM gospodarii WHERE gospodarieId = '" + pGospodarie + "' ", "persJuridica", Convert.ToInt16(Session["SESan"])) == "True")
            return true;
        else
            return false;
    }

    protected void AdaugaModificaParcela(object sender, EventArgs e)
    {
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
     
        ValidariNumerice();
        
        string[] vHaAriExtravilan = CalculeazaAriHa(tbExtravilanHa.Text.Replace('.', ','), "0", tbExtravilanAri.Text.Replace('.', ','), "0");
        string[] vHaAriIntravilan = CalculeazaAriHa("0", tbIntravilanHa.Text.Replace('.', ','), "0", tbIntravilanAri.Text.Replace('.', ','));
        
        if ( ((Button)sender).ID.ToString() == "btAdaugaParcela" )
        {
            if (EvitamDublarea() == 1)
            {
                gvParcele.DataBind();
                gvParcele.SelectedIndex = -1;
                gvParcelePrimite.SelectedIndex = -1;
                pnAdaugaParcela.Visible = false;
                pnListaParcele.Visible = true;
                btModificaParcela.Visible = false;
                btStergeParcela.Visible = false;
                btAtribuireCulturi.Visible = false;

                return;
            }

            SetCodRand();

            if ( AdaugaParcela(vHaAriIntravilan, vHaAriExtravilan) )
            {
                ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "parcele", "adaugare parcela denumire: " + tbDenumire.Text, "", Convert.ToInt64(Session["SESgospodarieId"]), 2);
                ClassLog.UpdateDataUltimeiModificari(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESgospodarieId"]));

                gvParcele.SelectedIndex = -1;
                gvParcelePrimite.SelectedIndex = -1;
                pnAdaugaParcela.Visible = false;
                pnListaParcele.Visible = true;
                btModificaParcela.Visible = false;
                btStergeParcela.Visible = false;
                btAtribuireCulturi.Visible = false;
                gvParcele.DataBind();
            }
        }
        else
        {
            if ( ModificaParcela(vHaAriIntravilan, vHaAriExtravilan) )
            {
                ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "parcele", "modificare parcela denumire noua: " + tbDenumire.Text, "parcela ID: " + lblParcelaId.Text, Convert.ToInt64(Session["SESgospodarieId"]), 3);
                ClassLog.UpdateDataUltimeiModificari(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESgospodarieId"]));

                pnAdaugaParcela.Visible = false;
                pnListaParcele.Visible = true;
                btModificaParcela.Visible = false;
                btStergeParcela.Visible = false;
                btAtribuireCulturi.Visible = false;
                gvParcele.DataBind();
                gvParcele.SelectedIndex = -1;
                gvParcelePrimite.SelectedIndex = -1;
            }
        }

        // daca am bifa pe unitati adaug in C2A din C2B
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select top(1) unitateAdaugaDinC2B from unitati where unitateId='" + Session["SESunitateId"].ToString() + "'";
        bool vAdaugaDinC2b = Convert.ToBoolean(vCmd.ExecuteScalar());
        vCmd.CommandText = "select top(1) unitateAdaugaDinC2BC3 from unitati where unitateId='" + Session["SESunitateId"].ToString() + "'";
        bool vAdaugaDinC2bInC3 = Convert.ToBoolean(vCmd.ExecuteScalar());
        ManipuleazaBD.InchideConexiune(vCon);
        if (vAdaugaDinC2b)
        {
            // daca are bifa pt C3 --> 1 , scriem si in c3
            List<List<string>> vListaValoriInitiale = CalculCapitole.vListaValoriInitiale(Convert.ToString(Session["SESgospodarieId"]), Convert.ToString(Session["SESunitateId"]), "2a", Convert.ToInt16(Session["SESan"]));
            if (vAdaugaDinC2bInC3)
                CalculCapitole.AdaugaCapitol2ADin2B(Session["SESunitateId"].ToString(), Session["SESan"].ToString(), Session["SESgospodarieId"].ToString(), "1");
            else
                CalculCapitole.AdaugaCapitol2ADin2B(Session["SESunitateId"].ToString(), Session["SESan"].ToString(), Session["SESgospodarieId"].ToString(), "0");

            CalculCapitole.vInsertCapitoleCentralizate(Convert.ToString(Session["SESgospodarieId"]), Convert.ToString(Session["SESunitateId"]), "2a", Convert.ToString(Session["SESan"]), vListaValoriInitiale);
            clsCorelatii.VerificaCorelatie(Convert.ToInt32(Session["SESunitateId"]), Convert.ToInt32(Session["SESgospodarieId"]), Convert.ToInt32(Session["SESan"]), "parcele");
            clsCorelatii.VerificaCorelatie(Convert.ToInt32(Session["SESunitateId"]), Convert.ToInt32(Session["SESgospodarieId"]), Convert.ToInt32(Session["SESan"]), "2a");
            clsCorelatii.VerificaCorelatie(Convert.ToInt32(Session["SESunitateId"]), Convert.ToInt32(Session["SESgospodarieId"]), Convert.ToInt32(Session["SESan"]), "3");
            clsCorelatii.VerificaCorelatie(Convert.ToInt32(Session["SESunitateId"]), Convert.ToInt32(Session["SESgospodarieId"]), Convert.ToInt32(Session["SESan"]), "paduri");
        }
    }
 
    private void SetCodRand()
    {
        string[] vWhereCampuri =
        {
            "gospodarieId",
            "unitateaId",
            "an"
        };
        string[] vWhereValori =
        {
            Session["SESgospodarieId"].ToString(),
            Session["SESunitateId"].ToString(),
            Session["SESan"].ToString()
        };
        string[] vOperatoriConditionali =
        {
            "=",
            "=",
            "="
        };
        string[] vOperatoriLogici =
        {
            " ",
            " AND ",
            " AND "
        };

        lblCodRand.Text = (ManipuleazaBD.fRezultaUnMaximInt("parcele", "parcelaCodRand", vWhereCampuri, vWhereValori, vOperatoriConditionali, vOperatoriLogici, Convert.ToInt16(Session["SESan"])) + 1).ToString();

        if ( lblCodRand.Text == "0" )
        {
            lblCodRand.Text = "1";
        }
    }
 
    private bool AdaugaParcela(string[] vHaAriIntravilan, string[] vHaAriExtravilan)
    {
        Parcele parcela = new Parcele();
       
        CreateParcela(parcela, vHaAriIntravilan, vHaAriExtravilan);

        return parcela.Adauga();
    }
 
    private bool ModificaParcela(string[] vHaAriIntravilan, string[] vHaAriExtravilan)
    {
        Parcele parcela = new Parcele();

        CreateParcela(parcela, vHaAriIntravilan, vHaAriExtravilan);
        
        parcela.Id = Convert.ToInt64(lblParcelaId.Text);

       return parcela.Modifica();
    }

    private void CreateParcela(Parcele parcela, string[] vHaAriIntravilan, string[] vHaAriExtravilan)
    {
        parcela.Denumire = tbDenumire.Text;
        parcela.CodRand = lblCodRand.Text;

        parcela.SuprafataIntravilan = new Suprafete
        {
            Hectare = Convert.ToDecimal(vHaAriIntravilan[0]),
            Ari = Convert.ToDecimal(vHaAriIntravilan[1].Replace('.', ','))
        };

        parcela.SuprafataExtravilan = new Suprafete
        {
            Hectare = Convert.ToDecimal(vHaAriExtravilan[0]),
            Ari = Convert.ToDecimal(vHaAriExtravilan[1].Replace('.', ','))
        };

        parcela.NrTopo = tbNrTopo.Text;
        parcela.CF = tbCF.Text;
        parcela.Categorie = ddlCategoria.SelectedValue;
        parcela.NrBloc = tbNrBloc.Text;
        parcela.Mentiuni = tbMentiuni.Text;
        parcela.An = Convert.ToInt32(Session["SESan"]);
        parcela.UnitateId = Convert.ToInt32(Session["SESunitateId"]);
        parcela.IdGospodarie = Convert.ToInt64(Session["SESgospodarieId"]);
        parcela.NrCadastral = tbNrCadastral.Text;
        parcela.NrCadastralProvizoriu = tbNrCadastralProvizoriu.Text;
        parcela.Localitate = ddlLocalitate.SelectedValue;
        parcela.Adresa = tbAdresa.Text;
        parcela.Tarla = tbTarla.Text;
        parcela.TitluProprietate = tbTitluProprietate.Text;
        parcela.ClasaBonitate = ddlClasaBonitate.SelectedValue;
        try
        {
            parcela.ProprietarId = Convert.ToInt64(proprietarDropDownList.SelectedValue);
        }
        catch
        {
            
        }
        parcela.Punctaj = ddlPunctaj.SelectedValue;
    }

    protected void ModificaArataListaParcele(object sender, EventArgs e)
    {
        pnAdaugaParcela.Visible = false;
        pnListaParcele.Visible = true;
        btStergeParcela.Visible = false;
        btModificaParcela.Visible = false;
        btAtribuireCulturi.Visible = false;
        gvParcele.DataBind();
        gvParcele.SelectedIndex = -1;
        gvParcelePrimite.SelectedIndex = -1;
    }
    
    protected void AdaugaArataListaParcele(object sender, EventArgs e)
    {
        pnAdaugaParcela.Visible = false;
        pnListaParcele.Visible = true;
        btStergeParcela.Visible = false;
        btModificaParcela.Visible = false;
        btAtribuireCulturi.Visible = false;
        gvParcele.DataBind();
        gvParcele.SelectedIndex = -1;
        gvParcelePrimite.SelectedIndex = -1;
    }
    
    protected void GvParceleSelectedIndexChanged(object sender, EventArgs e)
    {
        Session["idParcela"] = gvParcele.SelectedValue;

        Parcele parcela = new Parcele();

        parcela.Id = Convert.ToInt64(gvParcele.SelectedValue);
        
        ParceleServices.ConnectionYear = Convert.ToInt16(Session["SESan"]);
        parcela = parcela.GetParcelaById();

        parcela.SuprafataData = new Suprafete();

        if ( ParcelaEsteInchiriata(parcela.Id) )
        {
            Contracte contract = new Contracte();
            contract.IdParcela = parcela.Id;

            ContracteServices.ConnectionYear = Convert.ToInt16(Session["SESan"]);
            List<Contracte> listaContracte = contract.GetAllActiveContractsByIdParcela();

            foreach ( Contracte contractDinLista in listaContracte )
            {
                parcela.SuprafataData = parcela.SuprafataData + contractDinLista.Suprafata;
            }
        }

        ViewState["parcela"] = parcela;

        btAtribuireCulturi.Visible = true;
        proprietariButton.Visible = true;
        btAdaugaParcela.Visible = true;
        btModificaParcela.Visible = true;
        btStergeParcela.Visible = !ParcelaEsteInchiriata(parcela.Id);
        btModificaTransfer.Visible = true;
        btModificaTransferaToateParcelele.Visible = true;
        btContracteUtilizare.Visible = true;
        btIstoricParcela.Visible = true;
        gvParcelePrimite.SelectedIndex = -1;
    }
    
    protected void GvParceleRowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(gvParcele, e, this);

        if ( e.Row.RowType == DataControlRowType.DataRow )
        {
            CheckBox vCbAfisare = (CheckBox)e.Row.FindControl("cbAfisare");
            Label parcelaIdLabel = (Label)e.Row.FindControl("lblParcelaIdGv");
            Label situatieLabel = (Label)e.Row.FindControl("situatieLabel");

            Collection<string> listaParceleSelectate = new Collection<string>();
            
            if ( ViewState["parceleSelectate"] == null )
            {
                ViewState["parceleSelectate"] = listaParceleSelectate;
            }
            
            listaParceleSelectate = (Collection<string>)ViewState["parceleSelectate"];
            
            if ( listaParceleSelectate.Contains(parcelaIdLabel.Text) )
            {
                vCbAfisare.Checked = true;
            }
            else
            {
                vCbAfisare.Checked = false;
            }

            if (parcelaIdLabel.Text != string.Empty)
            {
                Contracte contract = new Contracte();
                contract.IdParcela = Convert.ToInt64(parcelaIdLabel.Text);

                ContracteServices.ConnectionYear = Convert.ToInt16(Session["SESan"]);
                contract = contract.GetLastActiveContractByIdParcela();

                if ( contract.TipContract!=Contracte.TipuriContracte.In_folosinta )
                {
                    situatieLabel.Text = contract.TipContract.ToString().Replace("_", " ") + " " +
                                         "(" + contract.Suprafata.ToString() +
                                         " către " + contract.Cumparator + "; " + contract.Numar + "/" + contract.DataSemnarii.ToShortDateString() + ")";

                    if (ViewState["contractIds"] != null)
                    {
                        ViewState["contractIds"] += parcelaIdLabel.Text + ";";
                    }
                    else
                    {
                        ViewState["contractIds"] = contract.IdParcela + ";";
                    }
                }
                else
                {
                    situatieLabel.Text = contract.TipContract.ToString().Replace("_", " ");
                    situatieLabel.ToolTip = "In folosinta";
                }
            }
        }
    }
    
    protected void GvParcelePreRender(object sender, EventArgs e)
    {
        string vInterogareTotal = "";
        
        if ( ddlCCategoria.SelectedValue == "%" || ddlCCategoria.SelectedValue == "" )
        {
            vInterogareTotal = "SELECT count(parcelaId) as x, SUM(CONVERT(decimal(18,4),parcelaSuprafataIntravilanHa)*100) + SUM(CONVERT(decimal(18,4),parcelaSuprafataIntravilanAri)) as intravilan, SUM(CONVERT(decimal(18,4),parcelaSuprafataExtravilanHa)*100) + SUM(CONVERT(decimal(18,4),parcelaSuprafataExtravilanAri)) as extravilan FROM parcele WHERE gospodarieId='" + Session["SESgospodarieId"].ToString() + "' and an='" + Session["SESan"].ToString() + "'";
        }
        else
        {
            vInterogareTotal = "SELECT  count(parcelaId) as x, SUM(CONVERT(decimal(18,4),parcelaSuprafataIntravilanHa)*100) + SUM(CONVERT(decimal(18,4),parcelaSuprafataIntravilanAri)) as intravilan, SUM(CONVERT(decimal(18,4),parcelaSuprafataExtravilanHa)*100) + SUM(CONVERT(decimal(18,4),parcelaSuprafataExtravilanAri)) as extravilan FROM parcele WHERE gospodarieId='" + Session["SESgospodarieId"].ToString() + "' and an='" + Session["SESan"].ToString() + "' AND parcelaCategorie = '" + ddlCCategoria.SelectedValue + "'";
        }
        
        if ( ddlNrPagina.SelectedValue == "%" )
        {
            gvParcele.AllowPaging = false;
        }
        else
        {
            gvParcele.AllowPaging = true;
            gvParcele.PageSize = Convert.ToInt16(ddlNrPagina.SelectedValue);
        }
        
        for ( int i = 0; i < gvParcele.Rows.Count; i++ )
        {
            if ( gvParcele.Rows[i].RowType == DataControlRowType.DataRow )
            {
                try
                {
                    if ( gvParcele.Rows[i].Cells[19].Text == "0" )
                    {
                        gvParcele.Rows[i].Cells[19].Text = "-";
                    }
                    
                    if ( gvParcele.Rows[i].Cells[18].Text == "0" )
                    {
                        gvParcele.Rows[i].Cells[18].Text = "-";
                    }
                }
                catch
                {
                }
            }
            
            string[] vCampuri =
            {
                "intravilan",
                "extravilan",
                "x"
            };

            List<string> vTotaluri = ManipuleazaBD.fRezultaUnString(vInterogareTotal, vCampuri, Convert.ToInt16(Session["SESan"])); 
            
            if ( vTotaluri[0] != "" )
            {
                gvParcele.FooterRow.Cells[2].Text = "Total suprafeţe: <span class='rosie'>" + (Convert.ToInt16(vTotaluri[2]) == 1 ? vTotaluri[2] + "</span> parcelă " : vTotaluri[2] + "</span> parcele ");
                gvParcele.FooterRow.Cells[4].Text = CalculeazaAriHa("0", "0", vTotaluri[0], "0")[0];
                gvParcele.FooterRow.Cells[5].Text = CalculeazaAriHa("0", "0", vTotaluri[0], "0")[1];
                gvParcele.FooterRow.Cells[6].Text = CalculeazaAriHa("0", "0", vTotaluri[1], "0")[0];
                gvParcele.FooterRow.Cells[7].Text = CalculeazaAriHa("0", "0", vTotaluri[1], "0")[1];
            }
        }
    }

    public void GvParcelePrimiteSelectedIndexChanged(object sender, EventArgs e)
    {
        Session["idParcela"] = gvParcelePrimite.SelectedValue;

        Parcele parcela = new Parcele();

        parcela.Id = Convert.ToInt64(gvParcelePrimite.SelectedValue);
        parcela.IdGospodarie = Convert.ToInt64(Session["SESgospodarieId"]);

        ParceleServices.ConnectionYear = Convert.ToInt16(Session["SESan"]);
        
        parcela = parcela.GetParcelaById();
        parcela.IdGospodarie = Convert.ToInt64(Session["SESgospodarieId"]);

        parcela.SuprafataPrimita = new Suprafete
        {
            Hectare = Convert.ToDecimal(gvParcelePrimite.SelectedRow.Cells[2].Text),
            Ari = Convert.ToDecimal(gvParcelePrimite.SelectedRow.Cells[3].Text)
        };

        ViewState["parcela"] = parcela;

        btAtribuireCulturi.Visible = true;
        proprietariButton.Visible = false;
        btAdaugaParcela.Visible = false;
        btModificaParcela.Visible = false;
        btStergeParcela.Visible = false;
        btModificaTransfer.Visible = false;
        btModificaTransferaToateParcelele.Visible = false;
        btContracteUtilizare.Visible = false;
        btIstoricParcela.Visible = false;
        gvParcele.SelectedIndex = -1;
    }

    public void GvParcelePrimiteRowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(gvParcelePrimite, e, this);
    }
    
    protected void AdaugaCoordonata(object sender, GridViewRowEventArgs e)
    {
        //  ManipuleazaBD.fManipuleazaBD("INSERT INTO coordonate (coordonataLatitudine, coordonataLongitudine, parcelaId) VALUES ('" + tbLatitudine.Text + "', '" + tbLongitudine.Text + "', '" + lblParcelaId.Text + "' ) ");
    }
    
    protected void BtTiparesteCapitolClick(object sender, EventArgs e)
    {
        // stergem rapoartele vechi ale utilizatorului
        ManipuleazaBD.fManipuleazaBD("DELETE FROM rapCapitole WHERE utilizatorId = '" + Session["SESutilizatorId"] + "'", Convert.ToInt16(Session["SESan"]));
        
        // umplem tabela de rapoarte
        // scoatem lista anilor din ciclul curent
        string vCiclu = ManipuleazaBD.fRezultaUnString("SELECT * FROM cicluri WHERE cicluAni LIKE '%" + Session["SESan"].ToString() + "%'", "cicluAni", Convert.ToInt16(Session["SESan"]));
        char[] vDelimitator =
        {
            '#'
        };
        string[] vAniDinCiclu = vCiclu.Split(vDelimitator, StringSplitOptions.RemoveEmptyEntries);
        List<string> vListaCampuri = new List<string>
        {
            "codCapitol",
            "capitoleCodRand",
            "denumire1",
            "denumire2",
            "denumire3",
            "denumire4",
            "denumire5"
        };
        // gasim gospodaria initiala pentru a lua datele din totii anii pentru aceasta - dupa gospodarieidinitial
        string vGospodarieIdInitial = ManipuleazaBD.fRezultaUnString("SELECT * FROM gospodarii WHERE gospodarieId = '" + Session["SESgospodarieId"].ToString() + "'", "gospodarieIdInitial", Convert.ToInt16(Session["SESan"]));
        int vCodRand = 0;
        string vComanda = "";
        int vAnOrdine = 1;

        foreach ( string vAn in vAniDinCiclu )
        {
            string vAnCamp = "an" + vAnOrdine.ToString();
            vListaCampuri = new List<string>
            {
                "camp1",
                "camp2",
                "camp3",
                "camp4",
                "camp5",
                "camp6",
                "col1",
                "col2",
                "col3",
                "col4",
                vAnCamp
            };
            List<List<string>> vValori = new List<List<string>>
            {
            };
            vValori = ManipuleazaBD.fRezultaListaStringuri(" SELECT parcele.parcelaDenumire + ' ' + parcele.parcelaTarla as camp1, parcele.parcelaSuprafataIntravilanHa as col1, parcele.parcelaSuprafataIntravilanAri as col2, parcele.parcelaSuprafataExtravilanHa as col3, parcele.parcelaSuprafataExtravilanAri as col4, parcele.parcelaNrTopo as camp2, CASE coalesce(parcele.parcelaCF, '') WHEN '' THEN '' ELSE 'CF: ' + convert(nvarchar, parcele.parcelaCF) + ' / ' END + CASE coalesce(parcele.parcelaNrCadastral, '') WHEN '' THEN '' ELSE 'nr.cad: ' + convert(nvarchar, parcele.parcelaNrCadastral) + ' / ' END + CASE coalesce(parcele.parcelaNrCadastralProvizoriu, '') WHEN '' THEN '' ELSE ' nr.cad.prv. ' + convert(nvarchar, parcele.parcelaNrCadastralProvizoriu) END AS camp3, sabloaneCapitole.denumire1 AS camp4,parcele.parcelaNrBloc AS camp5, CASE coalesce(parcele.parcelaMentiuni, '') WHEN '' THEN '' ELSE convert(nvarchar, parcele.parcelaMentiuni) + ' / ' END  +  convert(nvarchar, parcele.parcelaLocalitate) + ' ' + convert(nvarchar, parcele.parcelaAdresa) + ' ' + CASE parcele.parcelaTitluProprietate WHEN '-' THEN '' WHEN '' THEN '' ELSE 'titlu propr: ' + convert(nvarchar, parcele.parcelaTitluProprietate) + ' / ' END + CASE  parcele.parcelaClasaBonitate WHEN '-' THEN '' ELSE ' cls.bonitate: ' + convert(nvarchar, parcele.parcelaClasaBonitate) + ' / ' END + CASE parcele.parcelaPunctaj WHEN '0' THEN '' ELSE 'punctaj: ' + convert(nvarchar, parcele.parcelaPunctaj) END AS camp6,an" + vAnOrdine.ToString() + " = '" + vAn + "' FROM parcele INNER JOIN  gospodarii ON parcele.gospodarieId = gospodarii.gospodarieId INNER JOIN sabloaneCapitole ON parcele.parcelaCategorie = sabloaneCapitole.codRand AND sabloaneCapitole.capitol = '2a' AND sabloaneCapitole.an = '" + vAn + "' AND parcele.an = '" + vAn + "' WHERE (gospodarii.gospodarieIdInitial = '" + vGospodarieIdInitial + "') ORDER BY camp1 ", vListaCampuri, Convert.ToInt16(Session["SESan"]));
            // construim comanda pentru fiecare rand
            vCodRand = 1;
            if ( vValori.Count != 0 )
            {
                foreach ( List<string> vRandValori in vValori )
                {
                    vComanda += @"INSERT INTO [rapCapitole] ([utilizatorId], [capitol], [codRand], [camp1], [camp2], [camp3], [camp4], [camp5], [camp6], [col1_1], [col1_2], [col1_3], [col1_4], [" + vAnCamp + "]) VALUES ('" +
                                Convert.ToInt32(Session["SESutilizatorId"]) + 
                                "','2b','" + 
                                vCodRand + "','" +
                                vRandValori[0] + "','" +
                                vRandValori[1] + "','" +
                                vRandValori[2] + "','" +
                                vRandValori[3] + "','" +
                                vRandValori[4] + "','" +
                                vRandValori[5] + "','" +
                                vRandValori[6].Replace(',', '.') + "', '" +
                                vRandValori[7].Replace(',', '.') + "', '" +
                                vRandValori[8].Replace(',', '.') + "', '" +
                                vRandValori[9].Replace(',', '.') + "', '" +
                                vRandValori[10] + "');";
                    vCodRand++;
                }
            }
            else
            {
                vComanda += @"INSERT INTO [rapCapitole] ([utilizatorId], [codRand], [capitol], [" + vAnCamp + "]) VALUES ('" +
                            Convert.ToInt32(Session["SESutilizatorId"]) + "','-','2b', '" + vAn + "');";
            }
            vAnOrdine++;
        }
        // scriem in rapCapitole de unde vom lua la print
        ManipuleazaBD.fManipuleazaBD(vComanda, Convert.ToInt16(Session["SESan"]));
        ResponseHelper.Redirect("~/printCapitolul2b.aspx?codCapitol=2b", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
        ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "tiparire", "tiparire capitol 2b parcele", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
    }
    
    protected void BtAdaugaCoordonataClick(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        int pId = 0;
        string vMesaj = "";
        int vTrece = 0;
        string[] vValori =
        {
            tbX.Text,
            tbY.Text
        };
        // validam coordonatele 6 intregi
        Valideaza.Coordonate(vValori, out vMesaj, out vTrece);
        // daca sunt valide toate trei:
        if ( vTrece == 1 )
        {
            // luam datele gospodariei existente pentru a le introduce mai apoi in parcela
            List<string> vVolPoz = new List<string>
            {
                "volum",
                "nrPozitie"
            };
            vVolPoz = ManipuleazaBD.fRezultaUnString("SELECT volum, nrPozitie FROM gospodarii WHERE gospodarieId = '" + Session["SESgospodarieId"] + "'", vVolPoz, Convert.ToInt16(Session["SESan"]));
            // daca e adaugare de parcela, inseram parcela intai si transformam in "modificare":
            if ( lblParcelaId.Text == "0" )
            {
                List<string> vvCampuri = new List<string>
                {
                    "parcelaDenumire",
                    "parcelaCodRand",
                    "parcelaSuprafataIntravilanHa",
                    "parcelaSuprafataIntravilanAri",
                    "parcelaSuprafataExtravilanHa",
                    "parcelaSuprafataExtravilanAri",
                    "parcelaNrTopo",
                    "parcelaCF",
                    "parcelaCategorie",
                    "parcelaNrBloc",
                    "parcelaMentiuni",
                    "an",
                    "unitateaId",
                    "gospodarieId",
                    "parcelaNrCadastral",
                    "parcelaNrCadastralProvizoriu",
                    "parcelaLocalitate",
                    "parcelaAdresa"
                };
                List<string> vvValori = new List<string>
                {
                    tbDenumire.Text,
                    lblCodRand.Text,
                    tbIntravilanHa.Text,
                    tbIntravilanAri.Text,
                    tbExtravilanHa.Text,
                    tbExtravilanAri.Text,
                    tbNrTopo.Text,
                    tbCF.Text,
                    ddlCategoria.SelectedValue,
                    tbNrBloc.Text,
                    tbMentiuni.Text,
                    Session["SESan"].ToString(),
                    Session["SESunitateId"].ToString(),
                    Session["SESgospodarieId"].ToString(),
                    tbNrCadastral.Text,
                    tbNrCadastralProvizoriu.Text,
                    ddlLocalitate.SelectedValue,
                    tbAdresa.Text
                };
                ManipuleazaBD.fManipuleazaBDArray(out pId, "parcele", vvCampuri, vvValori, Convert.ToInt16(Session["SESan"]));
                ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "parcele", "adaugare parcela (la adaugare coordonata) parcela ID: " + pId.ToString(), "", Convert.ToInt64(Session["SESgospodarieId"]), 2);
                ClassLog.UpdateDataUltimeiModificari(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESgospodarieId"]));
                
                //  ManipuleazaBD.fManipuleazaBDCuId("INSERT INTO parcele (unitateaId, gospodarieId, an, parcelaDenumire, volum, nrPozitie, parcelaCodRand, parcelaSuprafataIntravilanHa, parcelaSuprafataIntravilanAri, parcelaSuprafataExtravilanHa, parcelaSuprafataExtravilanAri, parcelaNrTopo, parcelaCF, parcelaCategorie, parcelaNrBloc, parcelaMentiuni,  ) VALUES ('" + Session["SESunitateId"].ToString() + "','" + Session["SESgospodarieId"] + "','" + Session["SESan"] + "', '" + tbDenumire.Text + "', '" + vVolPoz[0] + "', '" + vVolPoz[1] + "','0','0','0','0','0', '', '', '', '', '', ); SELECT CAST(scope_identity() AS int)", out pId);
                gvParcele.DataBind();
                lblParcelaId.Text = pId.ToString();
                btAdaugaParcela.Visible = false;
                btAdaugaModificaParcela.Visible = true;
            }
            ManipuleazaBD.fManipuleazaBD("INSERT INTO coordonate (coordonataX,coordonataY, parcelaId, unitateId, an, volum, nrPozitie, gospodarieId) VALUES ('" + tbX.Text.Replace(',', '.') + "','" + tbY.Text.Replace(',', '.') + "','" + lblParcelaId.Text + "','" + Session["SESunitateId"] + "','" + Session["SESan"] + "', '" + vVolPoz[0] + "', '" + vVolPoz[1] + "','" + Session["SESgospodarieId"] + "')", Convert.ToInt16(Session["SESan"]));
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "coordonate", "adaugarea coordonate parcela ID: " + lblParcelaId.Text, "", Convert.ToInt64(Session["SESgospodarieId"]), 2);
            ClassLog.UpdateDataUltimeiModificari(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESgospodarieId"]));
            gvCoordonate.DataBind();
            tbX.Text = tbY.Text = "";
        }
        else
        {
            valCustom.IsValid = false;
            valCustom.ErrorMessage = vMesaj;
        }
    }
    
    protected void BtModificaTransferClick(object sender, EventArgs e)
    {
        HttpCookie vCook = Request.Cookies["COOKTNT"];
        if ( vCook != null )
        {
            Collection<string> vListaParceleSelectate = new Collection<string>();
            if ( ViewState["parceleSelectate"] == null )
                ViewState["parceleSelectate"] = vListaParceleSelectate;
            vListaParceleSelectate = (Collection<string>)ViewState["parceleSelectate"];
            string vParcele = "";
            if ( vListaParceleSelectate.Count > 0 )
                foreach ( string a in vListaParceleSelectate )
                    vParcele += ";" + a;
            else
                vParcele = gvParcele.SelectedValue.ToString();
            Session["SESparcelaId"] = vParcele;
            vCook = CriptareCookie.DecodeCookie(vCook);
            vCook["COOKparcelaId"] = gvParcele.SelectedValue.ToString();
            vCook = CriptareCookie.EncodeCookie(vCook);
            Response.Cookies.Add(vCook);
        }
        Response.Redirect("~/transferParcela.aspx");
    }
    
    protected void BtTiparesteHartaClick(object sender, EventArgs e)
    {
       // Response.Redirect("~/hartaParcela.aspx");
    }

    protected void CbAlegeCheckedChanged(object sender, EventArgs e)
    {
        Collection<string> vListaParceleSelectate = new Collection<string>();
        if (ViewState["parceleSelectate"] == null)
            ViewState["parceleSelectate"] = vListaParceleSelectate;
        vListaParceleSelectate = (Collection<string>)ViewState["parceleSelectate"];
        CheckBox vCbAfisare = (CheckBox)sender;
        Label vLblParcelaId = (Label)((CheckBox)sender).Parent.FindControl("lblParcelaIdGv");
        if (vListaParceleSelectate.Contains(vLblParcelaId.Text))
            vListaParceleSelectate.Remove(vLblParcelaId.Text);
        if (vCbAfisare.Checked)
            vListaParceleSelectate.Add(vLblParcelaId.Text);
        btModificaTransfer.Visible = true;
    }

    protected void LbAlegeTotClick(object sender, EventArgs e)
    {
        // merg pe fiecare rand si selectez sau deselectez
        if (ViewState["selectat"] == null)
            ViewState["selectat"] = "0";
        Collection<string> vListaParceleSelectate = new Collection<string>();
        if (ViewState["parceleSelectate"] == null)
            ViewState["parceleSelectate"] = vListaParceleSelectate;
        vListaParceleSelectate = (Collection<string>)ViewState["parceleSelectate"];
        gvParcele.AllowPaging = false;
        gvParcele.DataBind();
        for (int a = 0; a < gvParcele.Rows.Count; a++)
        {
            CheckBox vCbAfisare = (CheckBox)gvParcele.Rows[a].FindControl("cbAfisare");
            Label vLblParcelaId = (Label)gvParcele.Rows[a].FindControl("lblParcelaIdGv");
            if (vListaParceleSelectate.Contains(vLblParcelaId.Text))
                vListaParceleSelectate.Remove(vLblParcelaId.Text);
            if (ViewState["selectat"].ToString() == "1")
                vCbAfisare.Checked = false;
            else
            {
                vCbAfisare.Checked = true;
                vListaParceleSelectate.Add(vLblParcelaId.Text);
            }
        }
        gvParcele.AllowPaging = true;
        gvParcele.PageSize = 10;

        if (ViewState["selectat"].ToString() == "1")
            ViewState["selectat"] = "0";
        else
            ViewState["selectat"] = "1";
        btModificaTransfer.Visible = true;
    }

    protected void BtIstoricParcelaClick(object sender, EventArgs e)
    {
        pnIstoricParcela.Visible = true;
        pnListaParcele.Visible = false;
    }

    protected void BtModificaParcelaClick(object sender, EventArgs e)
    {
        pnAdaugaParcela.DefaultButton = btAdaugaModificaParcela.ID;

        pnAdaugaParcela.Visible = true;
        pnListaParcele.Visible = false;
        btAdaugaParcela.Visible = false;
        btAdaugaModificaParcela.Visible = true;
        btModificaArataListaParcele.Visible = true;
        btAdaugaArataListaParcele.Visible = false;

        FillControls();

        if (ParcelaEsteInchiriata(Convert.ToInt64(((Label)gvParcele.SelectedRow.Cells[0].FindControl("lblParcelaIdGv")).Text)))
        {
            valCustom.IsValid = false;

            valCustom.ErrorMessage = "Atenţie! Parcela este parţial sau total dată spre folosinţă altei gospodării! Nu este recomandabil să schimbaţi suprafaţa!";
        }
    }

    protected void BtListaAdaugaParcelaClick(object sender, EventArgs e)
    {
        Initializare();

        pnAdaugaParcela.Visible = true;
        pnListaParcele.Visible = false;
        btAdaugaParcela.Visible = true;
        btAdaugaModificaParcela.Visible = false;
        btModificaArataListaParcele.Visible = false;
        btAdaugaArataListaParcele.Visible = true;
        pnAdaugaParcela.DefaultButton = btAdaugaParcela.ID;
    }

    private void FillControls()
    {
        Parcele parcela = new Parcele();
        proprietarDropDownList.DataBind();
        lblParcelaId.Text = gvParcele.SelectedValue.ToString();

        parcela.Id = Convert.ToInt64(gvParcele.SelectedValue.ToString());

        ParceleServices.ConnectionYear = Convert.ToInt16(Session["SESan"]);
        parcela = parcela.GetParcelaById();

        tbDenumire.Text = parcela.Denumire;
        lblCodRand.Text = parcela.CodRand;
        tbIntravilanHa.Text = (parcela.SuprafataIntravilan).Hectare.ToString();
        tbIntravilanAri.Text = (parcela.SuprafataIntravilan).Ari.ToString();
    
        tbExtravilanHa.Text = parcela.SuprafataExtravilan.Hectare.ToString();
        tbExtravilanAri.Text = parcela.SuprafataExtravilan.Ari.ToString();
        tbNrTopo.Text = parcela.NrTopo;
        tbCF.Text = parcela.CF;

        try
        {
            ddlCategoria.SelectedValue = Convert.ToInt32(parcela.Categorie).ToString();
        }
        catch
        {
        }
        try
        {
            FillProprietariDropDownList();
        }
        catch { }
        try
        {
            proprietarDropDownList.SelectedValue = parcela.ProprietarId.ToString();
        }
        catch
        {
        }

        tbNrBloc.Text = parcela.NrBloc;
        tbMentiuni.Text = parcela.Mentiuni;
        tbAn.Text = parcela.An.ToString();
        tbNrCadastral.Text = parcela.NrCadastral;
        tbNrCadastralProvizoriu.Text = parcela.NrCadastralProvizoriu;

        try
        {
            ddlLocalitate.DataBind();
            ddlLocalitate.SelectedValue = parcela.Localitate;
        }
        catch
        {
        }

        tbAdresa.Text = parcela.Adresa;
        tbTarla.Text = parcela.Tarla;
        tbTitluProprietate.Text = parcela.TitluProprietate;

        try
        {
            ddlClasaBonitate.SelectedValue = parcela.ClasaBonitate;
        }
        catch
        {
            ddlClasaBonitate.SelectedValue = "0";
        }

        try
        {
            ddlPunctaj.SelectedValue = parcela.Punctaj;
        }
        catch
        {
            ddlPunctaj.SelectedValue = "0";
        }

        tbX.Text = tbY.Text = tbZ.Text = "";

        InitializareDdlPunctaj();


    }

    protected void Initializare()
    {
        tbDenumire.Text = "";
        tbMentiuni.Text = "";
        ddlCategoria.DataBind();
        ddlCategoria.SelectedIndex = 0;
        tbCF.Text = "";
        tbExtravilanAri.Text = "0";
        tbExtravilanHa.Text = "0";
        tbIntravilanAri.Text = "0";
        tbIntravilanHa.Text = "0";
        tbNrBloc.Text = "";
        tbNrTopo.Text = "";
        ddlLocalitate.DataBind();
        ddlLocalitate.SelectedIndex = -1;
        tbAdresa.Text = "";
        tbNrCadastral.Text = "";
        tbNrCadastralProvizoriu.Text = "";
        tbTitluProprietate.Text = "";
        tbTarla.Text = "";
        tbX.Text = "";
        tbY.Text = "";
        tbZ.Text = "";
      //  gvCoordonate.DataBind();
        InitializareDdlPunctaj();
        ddlClasaBonitate.SelectedValue = "0";
        lblParcelaId.Text = "0";
        FillProprietariDropDownList();

    }

    protected void InitializareDdlPunctaj()
    {
        ddlPunctaj.Items.Clear();
        ListItem vElement = new ListItem
        {
        };
        vElement.Value = "0";
        vElement.Text = "-";
        ddlPunctaj.Items.Add(vElement);
        for (int i = 1; i <= 100; i++)
        {
            vElement = new ListItem
            {
            };
            vElement.Value = vElement.Text = i.ToString();
            ddlPunctaj.Items.Add(vElement);
        }
        ddlPunctaj.SelectedValue = "0";
    }

    private void FillProprietariDropDownList()
    {
        DataTable membriGospodarie = MembriServices.GetMembri(Convert.ToInt32(Session["SESUnitateId"]), Convert.ToInt32(Session["SESGospodarieId"]), Convert.ToInt32(Session["SESAn"]));
        proprietarDropDownList.DataSource = membriGospodarie;
        proprietarDropDownList.DataBind();
        foreach (DataRow membru in membriGospodarie.Rows)
        {
            if (membru.ItemArray[2].ToString() == "1")
            {
                proprietarDropDownList.SelectedValue = membru.ItemArray[0].ToString();
                break;
            }
        }
    }

    private bool ParcelaEsteInchiriata(long idParcela)
    {
        if (ViewState["contractIds"]== null)
        {
            return false;
        }

        string contractIds = string.Empty;

        contractIds = ViewState["contractIds"].ToString();

        return contractIds.Contains(idParcela.ToString());
    }

    #endregion Parcele

    #region Culturi

    protected void BtAtribuireCulturiClick(object sender, EventArgs e)
    {
        pnListaParcele.Visible = false;
        pnAtribuireCulturi.Visible = true;
        pnButoaneAtribuiri.Visible = true;
        lblDenumireaParcelei.Visible = true;
       
        ResetControls();
    }

    #region Gridviews' Prerender and RowDataBound

    protected void GvAtribuire4aRowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(gvAtribuire4a, e, this);
    }

    protected void GvAtribuire4aPreRender(object sender, EventArgs e)
    {
        if(gvAtribuire4a.FooterRow == null)
        {
            return;
        }

        Parcele parcela = (Parcele)ViewState["parcela"];
        Suprafete suprafataCultivata = new Suprafete();

        foreach (CulturiIn4a cultura4a in parcela.CulturaIn4a)
        {
            suprafataCultivata = suprafataCultivata + (Suprafete)cultura4a.SuprafataCultivata;
        }
     
        ((Label)gvAtribuire4a.FooterRow.FindControl("lblGv4aFooterNumarHa")).Text = suprafataCultivata.ToString(Utils.Marime.ha);
        ((Label)gvAtribuire4a.FooterRow.FindControl("lblGv4aFooterNumarAri")).Text = suprafataCultivata.ToString(Utils.Marime.ari);
    }

    protected void GvAtribuire4a1RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(gvAtribuire4a1, e, this);
    }

    protected void GvAtribuire4a1PreRender(object sender, EventArgs e)
    {
        if (gvAtribuire4a1.FooterRow == null)
        {
            return;
        }

        Parcele parcela = (Parcele)ViewState["parcela"];
        Suprafete suprafataCultivata = new Suprafete();
      
        foreach (CulturiIn4a1 cultura4a1 in parcela.CulturaIn4a1)
        {
            suprafataCultivata = suprafataCultivata + (Suprafete)cultura4a1.SuprafataCultivata;
        }
   
        ((Label)gvAtribuire4a1.FooterRow.FindControl("lblGv4a1FooterNumarHa")).Text = suprafataCultivata.ToString(Utils.Marime.ha);
        ((Label)gvAtribuire4a1.FooterRow.FindControl("lblGv4a1FooterNumarAri")).Text = suprafataCultivata.ToString(Utils.Marime.ari);
    }
    
    protected void GvAtribuire4b1RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(gvAtribuire4b1, e, this);
    }

    protected void GvAtribuire4b1PreRender(object sender, EventArgs e)
    {
        if (gvAtribuire4b1.FooterRow == null)
        {
            return;
        }

        Parcele parcela = (Parcele)ViewState["parcela"];
        Arii suprafataCultivata = new Arii();

        foreach (CulturiIn4b1 cultura4b1 in parcela.CulturaIn4b1)
        {
            suprafataCultivata = suprafataCultivata + (Arii)cultura4b1.SuprafataCultivata;
        }
    
        ((Label)gvAtribuire4b1.FooterRow.FindControl("lblGv4b1FooterNumarAri")).Text = suprafataCultivata.ToString(Utils.Marime.mp);
    }

    protected void GvAtribuire4b2RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(gvAtribuire4b2, e, this);
    }

    protected void GvAtribuire4b2PreRender(object sender, EventArgs e)
    {
        if (gvAtribuire4b2.FooterRow == null)
        {
            return;
        }

        Parcele parcela = (Parcele)ViewState["parcela"];
        Arii suprafataCultivata = new Arii();

        foreach (CulturiIn4b2 cultura4b2 in parcela.CulturaIn4b2)
        {
            suprafataCultivata = suprafataCultivata + (Arii)cultura4b2.SuprafataCultivata;
        }

        ((Label)gvAtribuire4b2.FooterRow.FindControl("lblGv4b2FooterNumarAri")).Text = suprafataCultivata.ToString(Utils.Marime.mp);
    }

    protected void GvAtribuire4cRowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(gvAtribuire4c, e, this);
    }

    protected void GvAtribuire4cPreRender(object sender, EventArgs e)
    {
        if (gvAtribuire4c.FooterRow == null)
        {
            return;
        }

        Parcele parcela = (Parcele)ViewState["parcela"];
        Arii suprafataCultivata = new Arii();

        foreach (CulturiIn4c cultura4c in parcela.CulturaIn4c)
        {
            suprafataCultivata = suprafataCultivata + (Arii)cultura4c.SuprafataCultivata;
        }

        ((Label)gvAtribuire4c.FooterRow.FindControl("lblGv4cFooterNumarAri")).Text = suprafataCultivata.ToString(Utils.Marime.mp);
    }

    #endregion Gridviews' Prerender and RowDataBound

    protected void BtModificaAtribuiriClick(object sender, EventArgs e)
    {
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
   
        vVerificaSesiuni.VerificaSesiuniCookie();

        #region Conditii

        lblMesajPlin.Visible = false;

        if (lblMesajPlin.Visible && btModificaAtribuiri.Text!="modifică" || ViewState["parcela"] == null)
        {
            return;
        }

        Parcele parcela = (Parcele)ViewState["parcela"];

        Suprafete suprafata = new Suprafete
        {
            Hectare = Convert.ToDecimal(tbAtribuireHa.Text),
            Ari = Convert.ToDecimal(tbAtribuireAri.Text.Replace('.', ','))
        };

        if (parcela.SuprafataInFolosinta.CompareTo(suprafata) < 0)
        {
            lblMesajPlin.Text = "Ai introdus o suprafaţă mai mare decât cea disponibilă!";
            lblMesajPlin.Visible = true;

            return;
        }

        if (ParcelaEsteDejaCultivataCuCulturaAleasa(parcela) && ModAdaugareCulturi())
        {
            lblMesajPlin.Text = "Cultura deja există! Vă rugăm să o selectaţi corespunzător şi să o modificaţi.";
            lblMesajPlin.Visible = true;

            return;
        }

        if ( parcela.SuprafataRamasaDeCultivat == 0 && gvAtribuire4a.SelectedIndex==-1 )
        {
            return;
        }

        #endregion Conditii

        if ( ModAdaugareCulturi() )
        {
            AdaugaDateInParcela(parcela);
            AdunaCulturaInCapitole(parcela);

            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "atribuire culturi-->4a,4b,4c", "parcela " + lblDenumireaParcelei.Text + "(" + parcela.Id + ")", "", Convert.ToInt64(Session["SESgospodarieId"]), 2);
            ClassLog.UpdateDataUltimeiModificari(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESgospodarieId"]));
        }
        else
        {
            ModificaDateInParcela(parcela);
            ModificaCulturaInCapitole(parcela);

            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "modificare atribuire culturi-->4a,4b,4c", "parcela " + lblDenumireaParcelei.Text + "(" + parcela.Id + ")", "", Convert.ToInt64(Session["SESgospodarieId"]), 3);
            ClassLog.UpdateDataUltimeiModificari(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESgospodarieId"]));
        }

        ResetControls();
      
        btStergeAtribuire.Visible = false;
        btModificaAtribuiri.Text = "salvează";
        ViewState["parcelaAtribuita"] = "a";
        lblASters.Text = "nu";

    }

    private void ModificaCulturaInCapitole(Parcele parcela)
    {
        if (Utils.AutoAdaugareInCapitole(parcela.UnitateId, Convert.ToInt16(Session["SESan"])))
        {
            Capitole capitol = new Capitole();

            ParseCapitolToEnum(capitol);

            Culturi culturiTotaleInCapitolulSelectat = new CulturiTotale();
            List<Culturi> listaCulturiTotaleInCapitolulSelectat = new List<Culturi>();

            string codRand = culturiDropDownList.Text.Split('.')[0];

            CalculCapitole.PopuleazaTabelaCapitole(parcela.An, parcela.IdGospodarie, Convert.ToInt64(parcela.UnitateId), capitoleDropDownList.SelectedValue);

            culturiTotaleInCapitolulSelectat.Capitol.Cod = capitol.Cod;
            culturiTotaleInCapitolulSelectat.IdGospodarie = parcela.IdGospodarie;
            culturiTotaleInCapitolulSelectat.An = parcela.An;
            culturiTotaleInCapitolulSelectat.UnitateId = parcela.UnitateId;

            listaCulturiTotaleInCapitolulSelectat = culturiTotaleInCapitolulSelectat.GetCulturi();

            culturiTotaleInCapitolulSelectat = (listaCulturiTotaleInCapitolulSelectat).Where(cultura => cultura.CodRand == codRand).ElementAtOrDefault(0);

            switch (capitol.Cod)
            {
                case Utils.CapitolCulturi.A:

                    if (culturiTotaleInCapitolulSelectat != null)
                    {
                        culturiTotaleInCapitolulSelectat.SuprafataCultivata = (Suprafete)parcela.CulturaIn4a[gvAtribuire4a.SelectedIndex].SuprafataCultivata;
                    }

                    culturiTotaleInCapitolulSelectat.UpdateCultura();
                    BindAtribuire4aGridView(parcela.CulturaIn4a);

                    break;

                case Utils.CapitolCulturi.A1:

                    if (culturiTotaleInCapitolulSelectat != null)
                    {
                        culturiTotaleInCapitolulSelectat.SuprafataCultivata = (Suprafete)parcela.CulturaIn4a1[gvAtribuire4a1.SelectedIndex].SuprafataCultivata;
                    }

                    culturiTotaleInCapitolulSelectat.UpdateCultura();
                    BindAtribuire4a1GridView(parcela.CulturaIn4a1);

                    break;

                case Utils.CapitolCulturi.B1:

                    if (culturiTotaleInCapitolulSelectat != null)
                    {
                        culturiTotaleInCapitolulSelectat.SuprafataCultivata = (Suprafete)parcela.CulturaIn4b1[gvAtribuire4b1.SelectedIndex].SuprafataCultivata.ToAri();
                    }

                    culturiTotaleInCapitolulSelectat.UpdateCultura();
                    BindAtribuire4b1GridView(parcela.CulturaIn4b1);

                    break;

                case Utils.CapitolCulturi.B2:

                    if (culturiTotaleInCapitolulSelectat != null)
                    {
                        culturiTotaleInCapitolulSelectat.SuprafataCultivata = (Suprafete)parcela.CulturaIn4b2[gvAtribuire4b2.SelectedIndex].SuprafataCultivata.ToAri();
                    }

                    culturiTotaleInCapitolulSelectat.UpdateCultura();
                    BindAtribuire4b2GridView(parcela.CulturaIn4b2);

                    break;

                case Utils.CapitolCulturi.C:

                    if (culturiTotaleInCapitolulSelectat != null)
                    {
                        culturiTotaleInCapitolulSelectat.SuprafataCultivata = (Suprafete)parcela.CulturaIn4c[gvAtribuire4c.SelectedIndex].SuprafataCultivata.ToAri();
                    }

                    culturiTotaleInCapitolulSelectat.UpdateCultura();
                    BindAtribuire4cGridView(parcela.CulturaIn4c);

                    break;
            }
        }
    }

    private void ModificaDateInParcela(Parcele parcela)
    {
        Capitole capitol = new Capitole();
        Culturi cultura;
        ISuprafete suprafata;

        ParseCapitolToEnum(capitol);

        switch (capitol.Cod)
        {
            case Utils.CapitolCulturi.A:
                cultura = new CulturiIn4a();

                parcela.CulturaIn4a[gvAtribuire4a.SelectedIndex].Denumire = culturiDropDownList.SelectedItem.Text;
               
                suprafata = new Suprafete
                {
                    Hectare = Convert.ToDecimal(tbAtribuireHa.Text),
                    Ari = Convert.ToDecimal(tbAtribuireAri.Text.Replace('.', ','))
                };

                while ( ((Suprafete)suprafata).Ari >= 100 )
                {
                    ((Suprafete)suprafata).Hectare += 1;
                    ((Suprafete)suprafata).Ari -= 100;
                }

                parcela.CulturaIn4a[gvAtribuire4a.SelectedIndex].SuprafataCultivata = suprafata;
                cultura = parcela.CulturaIn4a[gvAtribuire4a.SelectedIndex];

                cultura.UpdateCultura();
                break;

            case Utils.CapitolCulturi.A1:
                cultura = new CulturiIn4a1();

                parcela.CulturaIn4a1[gvAtribuire4a1.SelectedIndex].Denumire = culturiDropDownList.SelectedItem.Text;
               
                suprafata = new Suprafete
                {
                    Hectare = Convert.ToDecimal(tbAtribuireHa.Text),
                    Ari = Convert.ToDecimal(tbAtribuireAri.Text.Replace('.', ','))
                };

                while ( ((Suprafete)suprafata).Ari >= 100 )
                {
                    ((Suprafete)suprafata).Hectare += 1;
                    ((Suprafete)suprafata).Ari -= 100;
                }

                parcela.CulturaIn4a1[gvAtribuire4a1.SelectedIndex].SuprafataCultivata = suprafata;
                cultura = parcela.CulturaIn4a1[gvAtribuire4a1.SelectedIndex];

                cultura.UpdateCultura();
                break;

            case Utils.CapitolCulturi.B1:
                cultura = new CulturiIn4b1();

                parcela.CulturaIn4b1[gvAtribuire4b1.SelectedIndex].Denumire = culturiDropDownList.SelectedItem.Text;
               
                suprafata = new Arii
                {
                    Arie = Convert.ToDecimal(tbAtribuireMp.Text.Replace('.', ','))
                };

                parcela.CulturaIn4b1[gvAtribuire4b1.SelectedIndex].SuprafataCultivata = suprafata;
                cultura = parcela.CulturaIn4b1[gvAtribuire4b1.SelectedIndex];

                cultura.UpdateCultura();
                break;

            case Utils.CapitolCulturi.B2:
                cultura = new CulturiIn4b2();

                parcela.CulturaIn4b2[gvAtribuire4b2.SelectedIndex].Denumire = culturiDropDownList.SelectedItem.Text;
               
                suprafata = new Arii
                {
                    Arie = Convert.ToDecimal(tbAtribuireMp.Text.Replace('.', ','))
                };

                parcela.CulturaIn4b2[gvAtribuire4b2.SelectedIndex].SuprafataCultivata = suprafata;
                cultura = parcela.CulturaIn4b2[gvAtribuire4b2.SelectedIndex];

                cultura.UpdateCultura();
                break;

            case Utils.CapitolCulturi.C:
                cultura = new CulturiIn4c();

                parcela.CulturaIn4c[gvAtribuire4c.SelectedIndex].Denumire = culturiDropDownList.SelectedItem.Text;
               
                suprafata = new Arii
                {
                    Arie = Convert.ToDecimal(tbAtribuireMp.Text.Replace('.', ','))
                };

                parcela.CulturaIn4c[gvAtribuire4c.SelectedIndex].SuprafataCultivata = suprafata;
                cultura = parcela.CulturaIn4c[gvAtribuire4c.SelectedIndex];

                cultura.UpdateCultura();
                break;
        }
    }
 
    private void AdunaCulturaInCapitole(Parcele parcela)
    {
        if ( Utils.AutoAdaugareInCapitole(parcela.UnitateId, Convert.ToInt16(Session["SESan"])) )
        {
            Capitole capitol = new Capitole();

            ParseCapitolToEnum(capitol);

            Culturi culturiTotaleInCapitolulSelectat = new CulturiTotale();
            List<Culturi> listaCulturiTotaleInCapitolulSelectat = new List<Culturi>();

            string codRand = culturiDropDownList.Text.Split('.')[0];

            CalculCapitole.PopuleazaTabelaCapitole(parcela.An, parcela.IdGospodarie, Convert.ToInt64(parcela.UnitateId), capitoleDropDownList.SelectedValue);

            culturiTotaleInCapitolulSelectat.Capitol.Cod = capitol.Cod;
            culturiTotaleInCapitolulSelectat.IdGospodarie = parcela.IdGospodarie;
            culturiTotaleInCapitolulSelectat.An = parcela.An;
            culturiTotaleInCapitolulSelectat.UnitateId = parcela.UnitateId;

            listaCulturiTotaleInCapitolulSelectat = culturiTotaleInCapitolulSelectat.GetCulturi();

            culturiTotaleInCapitolulSelectat = (listaCulturiTotaleInCapitolulSelectat).Where(cultura => cultura.CodRand == codRand).ElementAtOrDefault(0);

            switch(capitol.Cod)
            {
                case Utils.CapitolCulturi.A:  

                    if ( culturiTotaleInCapitolulSelectat != null )
                    {
                        culturiTotaleInCapitolulSelectat.SuprafataCultivata = (Suprafete)culturiTotaleInCapitolulSelectat.SuprafataCultivata + (Suprafete)parcela.CulturaIn4a[parcela.CulturaIn4a.Count - 1].SuprafataCultivata;
                    }
                      
                    culturiTotaleInCapitolulSelectat.UpdateCultura();
                    BindAtribuire4aGridView(parcela.CulturaIn4a);

                    break;

                case Utils.CapitolCulturi.A1:

                    if ( culturiTotaleInCapitolulSelectat != null )
                    {
                        culturiTotaleInCapitolulSelectat.SuprafataCultivata = (Suprafete)culturiTotaleInCapitolulSelectat.SuprafataCultivata + (Suprafete)parcela.CulturaIn4a1[parcela.CulturaIn4a1.Count - 1].SuprafataCultivata;
                    }

                    culturiTotaleInCapitolulSelectat.UpdateCultura();
                    BindAtribuire4a1GridView(parcela.CulturaIn4a1);

                    break;

                case Utils.CapitolCulturi.B1:

                    if ( culturiTotaleInCapitolulSelectat != null )
                    {
                        culturiTotaleInCapitolulSelectat.SuprafataCultivata = (Suprafete)culturiTotaleInCapitolulSelectat.SuprafataCultivata + (Suprafete)parcela.CulturaIn4b1[parcela.CulturaIn4b1.Count - 1].SuprafataCultivata.ToAri();
                    }
                    
                    culturiTotaleInCapitolulSelectat.UpdateCultura();
                    BindAtribuire4b1GridView(parcela.CulturaIn4b1);

                    break;

                case Utils.CapitolCulturi.B2:

                    if ( culturiTotaleInCapitolulSelectat != null )
                    {
                        culturiTotaleInCapitolulSelectat.SuprafataCultivata = (Suprafete)culturiTotaleInCapitolulSelectat.SuprafataCultivata + (Suprafete)parcela.CulturaIn4b2[parcela.CulturaIn4b2.Count - 1].SuprafataCultivata.ToAri();
                    }
                 
                    culturiTotaleInCapitolulSelectat.UpdateCultura();
                    BindAtribuire4b2GridView(parcela.CulturaIn4b2);

                    break;

                case Utils.CapitolCulturi.C:

                    if ( culturiTotaleInCapitolulSelectat != null )
                    {
                        culturiTotaleInCapitolulSelectat.SuprafataCultivata = (Suprafete)culturiTotaleInCapitolulSelectat.SuprafataCultivata + (Suprafete)parcela.CulturaIn4c[parcela.CulturaIn4c.Count - 1].SuprafataCultivata.ToAri();
                    }
                 
                    culturiTotaleInCapitolulSelectat.UpdateCultura();
                    BindAtribuire4cGridView(parcela.CulturaIn4c);

                    break;
            }
        }
    }
 
    private bool ParcelaEsteDejaCultivataCuCulturaAleasa(Parcele parcela)
    {
        Capitole capitol = new Capitole();

        ParseCapitolToEnum(capitol);

        string codRand = culturiDropDownList.Text.Split('.')[0];

        switch (capitol.Cod)
        {
            case Utils.CapitolCulturi.A:
                return parcela.CulturaIn4a.Any(cultura => cultura.CodRand == codRand);
            case Utils.CapitolCulturi.A1:
                return parcela.CulturaIn4a1.Any(cultura => cultura.CodRand == codRand);
            case Utils.CapitolCulturi.B1:
                return parcela.CulturaIn4b1.Any(cultura => cultura.CodRand == codRand);
            case Utils.CapitolCulturi.B2:
                return parcela.CulturaIn4b2.Any(cultura => cultura.CodRand == codRand);
            case Utils.CapitolCulturi.C:
                return parcela.CulturaIn4c.Any(cultura => cultura.CodRand == codRand);
        }

        return false;
    }
 
    private void ParseCapitolToEnum(Capitole capitol)
    {
        if ( capitoleDropDownList.SelectedValue != "4b" )
        {
            capitol.Cod = (Utils.CapitolCulturi)Enum.Parse(typeof( Utils.CapitolCulturi ), capitoleDropDownList.SelectedValue.Replace("4", "").ToUpper());
        }
        else
        {
            capitol.Cod = (Utils.CapitolCulturi)Enum.Parse(typeof( Utils.CapitolCulturi ), capitoleDropDownList.SelectedValue.Replace("4", "").Insert(capitoleDropDownList.SelectedValue.Length - 1, "1").ToUpper());
        }
    }

    protected void BtStergeAtribuireClick(object sender, EventArgs e)
    {
        Parcele parcela = (Parcele)ViewState["parcela"];
      
        StergeDinParceleCatreCapitole(parcela);

        ScadeCulturaDinCapitole(parcela);

        ResetControls();

        ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "stergere atribuire culturi-->4a,4b,4c", "parcela " + lblDenumireaParcelei.Text + "(" + parcela.Id + ")", "", Convert.ToInt64(Session["SESgospodarieId"]), 4);
        ClassLog.UpdateDataUltimeiModificari(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESgospodarieId"]));

        lblMesajPlin.Visible = false;
        btModificaAtribuiri.Text = "salvează";
        ViewState["parcelaAtribuita"] = "a";

        lblASters.Text = "da";
    }
 
    private void StergeDinParceleCatreCapitole(Parcele parcela)
    {
        Capitole capitol = new Capitole();
        bool autostergere = Utils.AutoAdaugareInCapitole(parcela.UnitateId, Convert.ToInt16(Session["SESan"]));

        ParseCapitolToEnum(capitol);

        Culturi cultura;

        switch (capitol.Cod)
        {
            case Utils.CapitolCulturi.A:
                cultura = new CulturiIn4a();
                cultura.Capitol = new Capitole();

                cultura.Id = parcela.CulturaIn4a[gvAtribuire4a.SelectedIndex].Id;
                cultura.Capitol.Cod = capitol.Cod;
                    
                cultura.DeleteCultura();

                if(!autostergere)
                {
                    parcela.CulturaIn4a.Remove(parcela.CulturaIn4a[gvAtribuire4a.SelectedIndex]);
                    BindAtribuire4aGridView(parcela.CulturaIn4a);
                }

                break;

            case Utils.CapitolCulturi.A1:
                cultura = new CulturiIn4a1();
                cultura.Capitol = new Capitole();

                cultura.Id = parcela.CulturaIn4a1[gvAtribuire4a1.SelectedIndex].Id;

                cultura.DeleteCultura();

                if ( !autostergere )
                {
                    parcela.CulturaIn4a1.Remove(parcela.CulturaIn4a1[gvAtribuire4a1.SelectedIndex]);
                    BindAtribuire4a1GridView(parcela.CulturaIn4a1);
                }

                break;

            case Utils.CapitolCulturi.B1:
                cultura = new CulturiIn4b1();
                cultura.Capitol = new Capitole();

                cultura.Id = parcela.CulturaIn4b1[gvAtribuire4b1.SelectedIndex].Id;

                cultura.DeleteCultura();
                
                if ( !autostergere )
                {
                    parcela.CulturaIn4b1.Remove(parcela.CulturaIn4b1[gvAtribuire4b1.SelectedIndex]);
                    BindAtribuire4b1GridView(parcela.CulturaIn4b1);
                }

                break;

            case Utils.CapitolCulturi.B2:
                cultura = new CulturiIn4b2();
                cultura.Capitol = new Capitole();

                cultura.Id = parcela.CulturaIn4b2[gvAtribuire4b2.SelectedIndex].Id;

                cultura.DeleteCultura();
                
                if ( !autostergere )
                {
                    parcela.CulturaIn4b2.Remove(parcela.CulturaIn4b2[gvAtribuire4b2.SelectedIndex]);
                    BindAtribuire4b2GridView(parcela.CulturaIn4b2);
                }

                break;

            case Utils.CapitolCulturi.C:
                cultura = new CulturiIn4c();
                cultura.Capitol = new Capitole();

                cultura.Id = parcela.CulturaIn4c[gvAtribuire4c.SelectedIndex].Id;

                cultura.DeleteCultura();
                
                if ( !autostergere )
                {
                    parcela.CulturaIn4c.Remove(parcela.CulturaIn4c[gvAtribuire4c.SelectedIndex]);
                    BindAtribuire4cGridView(parcela.CulturaIn4c);
                }

                break;
        }
    }
 
    private void ScadeCulturaDinCapitole(Parcele parcela)
    {
        if (Utils.AutoAdaugareInCapitole(parcela.UnitateId, Convert.ToInt16(Session["SESan"])))
        {
            Capitole capitol = new Capitole();

            ParseCapitolToEnum(capitol);

            Culturi culturiTotaleInCapitolulSelectat = new CulturiTotale();
            List<Culturi> listaCulturiTotaleInCapitolulSelectat = new List<Culturi>();

            string codRand = culturiDropDownList.Text.Split('.')[0];

            CalculCapitole.PopuleazaTabelaCapitole(parcela.An, parcela.IdGospodarie, Convert.ToInt64(parcela.UnitateId), capitoleDropDownList.SelectedValue);

            culturiTotaleInCapitolulSelectat.Capitol.Cod = capitol.Cod;
            culturiTotaleInCapitolulSelectat.IdGospodarie = parcela.IdGospodarie;
            culturiTotaleInCapitolulSelectat.An = parcela.An;
            culturiTotaleInCapitolulSelectat.UnitateId = parcela.UnitateId;

            listaCulturiTotaleInCapitolulSelectat = culturiTotaleInCapitolulSelectat.GetCulturi();

            culturiTotaleInCapitolulSelectat = (listaCulturiTotaleInCapitolulSelectat).Where(cultura => cultura.CodRand == codRand).ElementAtOrDefault(0);

            switch (capitol.Cod)
            {
                case Utils.CapitolCulturi.A:

                    if ( culturiTotaleInCapitolulSelectat != null )
                    {
                        culturiTotaleInCapitolulSelectat.SuprafataCultivata = (Suprafete)culturiTotaleInCapitolulSelectat.SuprafataCultivata - (Suprafete)parcela.CulturaIn4a[gvAtribuire4a.SelectedIndex].SuprafataCultivata;
                    }

                    culturiTotaleInCapitolulSelectat.UpdateCultura();

                    parcela.CulturaIn4a.Remove(parcela.CulturaIn4a[gvAtribuire4a.SelectedIndex]);

                    BindAtribuire4aGridView(parcela.CulturaIn4a);

                    break;

                case Utils.CapitolCulturi.A1:

                    if ( culturiTotaleInCapitolulSelectat != null )
                    {
                        culturiTotaleInCapitolulSelectat.SuprafataCultivata = (Suprafete)culturiTotaleInCapitolulSelectat.SuprafataCultivata - (Suprafete)parcela.CulturaIn4a1[gvAtribuire4a1.SelectedIndex].SuprafataCultivata;
                    }

                    culturiTotaleInCapitolulSelectat.UpdateCultura();

                    parcela.CulturaIn4a1.Remove(parcela.CulturaIn4a1[gvAtribuire4a1.SelectedIndex]);

                    BindAtribuire4a1GridView(parcela.CulturaIn4a1);

                    break;

                case Utils.CapitolCulturi.B1:

                    if ( culturiTotaleInCapitolulSelectat != null )
                    {
                        culturiTotaleInCapitolulSelectat.SuprafataCultivata = (Suprafete)culturiTotaleInCapitolulSelectat.SuprafataCultivata - (Suprafete)parcela.CulturaIn4b1[gvAtribuire4b1.SelectedIndex].SuprafataCultivata.ToAri();
                    }

                    culturiTotaleInCapitolulSelectat.UpdateCultura();

                    parcela.CulturaIn4b1.Remove(parcela.CulturaIn4b1[gvAtribuire4b1.SelectedIndex]);

                    BindAtribuire4b1GridView(parcela.CulturaIn4b1);

                    break;

                case Utils.CapitolCulturi.B2:

                    if ( culturiTotaleInCapitolulSelectat != null )
                    {
                        culturiTotaleInCapitolulSelectat.SuprafataCultivata = (Suprafete)culturiTotaleInCapitolulSelectat.SuprafataCultivata - (Suprafete)parcela.CulturaIn4b2[gvAtribuire4b2.SelectedIndex].SuprafataCultivata.ToAri();
                    }

                    culturiTotaleInCapitolulSelectat.UpdateCultura();

                    parcela.CulturaIn4b2.Remove(parcela.CulturaIn4b2[gvAtribuire4b2.SelectedIndex]);

                    BindAtribuire4b2GridView(parcela.CulturaIn4b2);

                    break;

                case Utils.CapitolCulturi.C:

                    if ( culturiTotaleInCapitolulSelectat != null )
                    {
                        culturiTotaleInCapitolulSelectat.SuprafataCultivata = (Suprafete)culturiTotaleInCapitolulSelectat.SuprafataCultivata - (Suprafete)parcela.CulturaIn4c[gvAtribuire4c.SelectedIndex].SuprafataCultivata.ToAri();
                    }

                    culturiTotaleInCapitolulSelectat.UpdateCultura();

                    parcela.CulturaIn4c.Remove(parcela.CulturaIn4c[gvAtribuire4c.SelectedIndex]);

                    BindAtribuire4cGridView(parcela.CulturaIn4c);

                    break;
            }
        }
    }

    protected void CulturiDropDownListSelectedIndexChanged(object sender, EventArgs e)
    {
        ReseteazaControaleLaSchimbareDeCapitolSauCultura();
    }
 
    protected void CapitoleDropDownListSelectedIndexChanged(object sender, EventArgs e)
    {
        ReseteazaControaleLaSchimbareDeCapitolSauCultura();
    }

    protected void GvAtribuire4aSelectedIndexChanged(object sender, EventArgs e)
    {
        btModificaAtribuiri.Text = "modifică";
        ViewState["parcelaAtribuita"] = "m";
        btStergeAtribuire.Visible = true;
        lblASters.Text = "nu"; 
        statusLabel.Text = "Suprafaţa cultivată în capitolul şi cu cultura selectate:";
        gvAtribuire4a1.SelectedIndex = -1;
        gvAtribuire4b1.SelectedIndex = -1;
        gvAtribuire4b2.SelectedIndex = -1;
        gvAtribuire4c.SelectedIndex = -1;
        EnableOrDisableControaleSuprafete();

        Parcele parcela = (Parcele)ViewState["parcela"];

        capitoleDropDownList.SelectedValue = "4a";
        culturiDropDownList.SelectedValue = parcela.CulturaIn4a[gvAtribuire4a.SelectedRow.RowIndex].CodRand;

        tbAtribuireHa.Text = ((Suprafete)parcela.CulturaIn4a[gvAtribuire4a.SelectedRow.RowIndex].SuprafataCultivata).ToString(Utils.Marime.ha).Replace("ha", "").Trim();
        tbAtribuireAri.Text = ((Suprafete)parcela.CulturaIn4a[gvAtribuire4a.SelectedRow.RowIndex].SuprafataCultivata).ToString(Utils.Marime.ari).Replace("ari", "").Trim();
        tbAtribuireMp.Text = "0,0000";
    }
 
    protected void GvAtribuire4a1SelectedIndexChanged(object sender, EventArgs e)
    {
        btModificaAtribuiri.Text = "modifică";
        ViewState["parcelaAtribuita"] = "m";
        btStergeAtribuire.Visible = true;
        statusLabel.Text = "Suprafaţa cultivată în capitolul şi cu cultura selectate:";
        lblASters.Text = "nu";
        gvAtribuire4a.SelectedIndex = -1;
        gvAtribuire4b1.SelectedIndex = -1;
        gvAtribuire4b2.SelectedIndex = -1;
        gvAtribuire4c.SelectedIndex = -1;
        EnableOrDisableControaleSuprafete();

        Parcele parcela = (Parcele)ViewState["parcela"];

        capitoleDropDownList.SelectedValue = "4a1";
        culturiDropDownList.SelectedValue = parcela.CulturaIn4a1[gvAtribuire4a1.SelectedRow.RowIndex].CodRand;

        tbAtribuireHa.Text = ((Suprafete)parcela.CulturaIn4a1[gvAtribuire4a1.SelectedRow.RowIndex].SuprafataCultivata).ToString(Utils.Marime.ha).Replace("ha", "");
        tbAtribuireAri.Text = ((Suprafete)parcela.CulturaIn4a1[gvAtribuire4a1.SelectedRow.RowIndex].SuprafataCultivata).ToString(Utils.Marime.ari).Replace("ari", "");
        tbAtribuireMp.Text = "0,0000";
    }

    protected void GvAtribuire4b1SelectedIndexChanged(object sender, EventArgs e)
    {
        btModificaAtribuiri.Text = "modifică";
        ViewState["parcelaAtribuita"] = "m";
        btStergeAtribuire.Visible = true;
        statusLabel.Text = "Suprafaţa cultivată în capitolul şi cu cultura selectate:";
        lblASters.Text = "nu";
        gvAtribuire4a.SelectedIndex = -1;
        gvAtribuire4a1.SelectedIndex = -1;
        gvAtribuire4b2.SelectedIndex = -1; 
        gvAtribuire4c.SelectedIndex = -1;
        EnableOrDisableControaleSuprafete();

        Parcele parcela = (Parcele)ViewState["parcela"];
     
        capitoleDropDownList.SelectedValue = "4b";
        culturiDropDownList.SelectedValue = parcela.CulturaIn4b1[gvAtribuire4b1.SelectedRow.RowIndex].CodRand;

        tbAtribuireHa.Text = "0";
        tbAtribuireAri.Text = "0,0000";
        tbAtribuireMp.Text = ((Arii)parcela.CulturaIn4b1[gvAtribuire4b1.SelectedRow.RowIndex].SuprafataCultivata).ToString(Utils.Marime.mp).Replace(" mp", "").Trim();
    }

    protected void GvAtribuire4b2SelectedIndexChanged(object sender, EventArgs e)
    {
        btModificaAtribuiri.Text = "modifică";
        ViewState["parcelaAtribuita"] = "m";
        btStergeAtribuire.Visible = true;
        statusLabel.Text = "Suprafaţa cultivată în capitolul şi cu cultura selectate:";
        lblASters.Text = "nu";
        gvAtribuire4a.SelectedIndex = -1;
        gvAtribuire4a1.SelectedIndex = -1;
        gvAtribuire4b1.SelectedIndex = -1;
        gvAtribuire4c.SelectedIndex = -1;
        EnableOrDisableControaleSuprafete();

        Parcele parcela = (Parcele)ViewState["parcela"];

        capitoleDropDownList.SelectedValue = "4b2";
        culturiDropDownList.SelectedValue = parcela.CulturaIn4b2[gvAtribuire4b2.SelectedRow.RowIndex].CodRand;

        tbAtribuireHa.Text = "0";
        tbAtribuireAri.Text = "0,0000";
        tbAtribuireMp.Text = ((Arii)parcela.CulturaIn4b2[gvAtribuire4b2.SelectedRow.RowIndex].SuprafataCultivata).ToString(Utils.Marime.mp).Replace(" mp", "").Trim();
    }

    protected void GvAtribuire4cSelectedIndexChanged(object sender, EventArgs e)
    {
        btModificaAtribuiri.Text = "modifică";
        ViewState["parcelaAtribuita"] = "m";
        btStergeAtribuire.Visible = true;
        statusLabel.Text = "Suprafaţa cultivată în capitolul şi cu cultura selectate:";
        lblASters.Text = "nu";
        gvAtribuire4a.SelectedIndex = -1;
        gvAtribuire4a1.SelectedIndex = -1;
        gvAtribuire4b1.SelectedIndex = -1;
        gvAtribuire4b2.SelectedIndex = -1;
        EnableOrDisableControaleSuprafete();

        Parcele parcela = (Parcele)ViewState["parcela"];

        capitoleDropDownList.SelectedValue = "4c";
        culturiDropDownList.SelectedValue = parcela.CulturaIn4c[gvAtribuire4c.SelectedRow.RowIndex].CodRand;

        tbAtribuireHa.Text = "0";
        tbAtribuireAri.Text = "0,0000";
        tbAtribuireMp.Text = ((Arii)parcela.CulturaIn4c[gvAtribuire4c.SelectedRow.RowIndex].SuprafataCultivata).ToString(Utils.Marime.mp).Replace(" mp", "").Trim();
    }

    protected void BtArataListaParceleClick(object sender, EventArgs e)
    {
        pnListaParcele.Visible = true;
        btAdaugaParcela.Visible = false;
        btModificaParcela.Visible = false;
        btStergeParcela.Visible = false;
        btModificaTransfer.Visible = false;
        pnAtribuireCulturi.Visible = false;
        pnIstoricParcela.Visible = false;
        btAtribuireCulturi.Visible = false;

        lblASters.Text = "nu";

        gvParcele.DataBind();
        gvParcele.SelectedIndex = -1;
        gvParcelePrimite.SelectedIndex = -1;

        gvAtribuire4a.SelectedIndex = -1;
        gvAtribuire4a1.SelectedIndex = -1;
        gvAtribuire4b1.SelectedIndex = -1;
        gvAtribuire4b2.SelectedIndex = -1;
        gvAtribuire4c.SelectedIndex = -1;
        
        capitoleDropDownList.SelectedIndex = 0;
        culturiDropDownList.DataBind();
    }

    private void BindAtribuire4aGridView(List<Culturi> culturi)
    {
        gvAtribuire4a.DataSource = culturi;
        gvAtribuire4a.DataBind();
        gvAtribuire4a.SelectedIndex = -1;
    }

    private void BindAtribuire4a1GridView(List<Culturi> culturi)
    {
        gvAtribuire4a1.DataSource = culturi;
        gvAtribuire4a1.DataBind();
        gvAtribuire4a1.SelectedIndex = -1;
    }

    private void BindAtribuire4b1GridView(List<Culturi> culturi)
    {
        gvAtribuire4b1.DataSource = culturi;
        gvAtribuire4b1.DataBind();
        gvAtribuire4b1.SelectedIndex = -1;
    }

    private void BindAtribuire4b2GridView(List<Culturi> culturi)
    {
        gvAtribuire4b2.DataSource = culturi;
        gvAtribuire4b2.DataBind();
        gvAtribuire4b2.SelectedIndex = -1;
    }

    private void BindAtribuire4cGridView(List<Culturi> culturi)
    {
        gvAtribuire4c.DataSource = culturi;
        gvAtribuire4c.DataBind();
        gvAtribuire4c.SelectedIndex = -1;
    }

    private void InitializeazaListeleDeCulturiInParcela(Parcele parcela)
    {
        parcela.CulturaIn4a = new List<Culturi>();
        parcela.CulturaIn4a1 = new List<Culturi>();
        parcela.CulturaIn4b1 = new List<Culturi>();
        parcela.CulturaIn4b2 = new List<Culturi>();
        parcela.CulturaIn4c = new List<Culturi>();
    }

    private void IncarcaCulturileInParcela(Parcele parcela)
    {
        Culturi culturaIn4a = new CulturiIn4a();

        culturaIn4a.ParcelaId = parcela.Id;
        culturaIn4a.IdGospodarie = parcela.IdGospodarie;
        culturaIn4a.An = parcela.An;
        culturaIn4a.UnitateId = parcela.UnitateId;

        parcela.CulturaIn4a = culturaIn4a.GetCulturi();

        Culturi culturaIn4a1 = new CulturiIn4a1();

        culturaIn4a1.ParcelaId = parcela.Id;
        culturaIn4a1.IdGospodarie = parcela.IdGospodarie;
        culturaIn4a1.An = parcela.An;
        culturaIn4a1.UnitateId = parcela.UnitateId;

        parcela.CulturaIn4a1 = culturaIn4a1.GetCulturi();

        Culturi culturaIn4b1 = new CulturiIn4b1();

        culturaIn4b1.ParcelaId = parcela.Id;
        culturaIn4b1.IdGospodarie = parcela.IdGospodarie;
        culturaIn4b1.An = parcela.An;
        culturaIn4b1.UnitateId = parcela.UnitateId;

        parcela.CulturaIn4b1 = culturaIn4b1.GetCulturi();

        Culturi culturaIn4b2 = new CulturiIn4b2();

        culturaIn4b2.ParcelaId = parcela.Id;
        culturaIn4b2.IdGospodarie = parcela.IdGospodarie;
        culturaIn4b2.An = parcela.An;
        culturaIn4b2.UnitateId = parcela.UnitateId;

        parcela.CulturaIn4b2 = culturaIn4b2.GetCulturi();

        Culturi culturaIn4c = new CulturiIn4c();

        culturaIn4c.ParcelaId = parcela.Id;
        culturaIn4c.IdGospodarie = parcela.IdGospodarie;
        culturaIn4c.An = parcela.An;
        culturaIn4c.UnitateId = parcela.UnitateId;

        parcela.CulturaIn4c = culturaIn4c.GetCulturi();
    }

    private void CalculeazaSuprafataCultivataPeParcela(Parcele parcela)
    {
        parcela.SuprafataCultivata = new Suprafete();

        foreach (CulturiIn4a cultura4a in parcela.CulturaIn4a)
        {
            parcela.SuprafataCultivata = (Suprafete)parcela.SuprafataCultivata + (Suprafete)cultura4a.SuprafataCultivata;
        }
        foreach (CulturiIn4a1 cultura4a1 in parcela.CulturaIn4a1)
        {
            parcela.SuprafataCultivata = (Suprafete)parcela.SuprafataCultivata + (Suprafete)cultura4a1.SuprafataCultivata;
        }
        foreach (CulturiIn4b1 cultura4b1 in parcela.CulturaIn4b1)
        {
            parcela.SuprafataCultivata = (Suprafete)parcela.SuprafataCultivata + (Suprafete)cultura4b1.SuprafataCultivata.ToAri();
        }
        foreach (CulturiIn4b2 cultura4b2 in parcela.CulturaIn4b2)
        {
            parcela.SuprafataCultivata = (Suprafete)parcela.SuprafataCultivata + (Suprafete)cultura4b2.SuprafataCultivata.ToAri();
        }
        foreach (CulturiIn4c cultura4c in parcela.CulturaIn4c)
        {
            parcela.SuprafataCultivata = (Suprafete)parcela.SuprafataCultivata + (Suprafete)cultura4c.SuprafataCultivata.ToAri();
        }
    }

    protected void StergeAtribuire(string pCapitol, string pRand, string pHa, string pAri)
    {
        List<string> vCampuri = new List<string>
        {
        };
        List<string> vValori = new List<string>
        {
        };
        decimal vSuprafataInitiala = 0;
        decimal vSuprafataDinTabel = 0;
        decimal vSuprafataDeScris = 0;
        string[] vSuprafataDeScrisString =
        {
            "0",
            "0"
        };
        // 4a ****************************************
        if ( pCapitol == "4a" )
        {
            // gasim linia de capitol pentru randul care se va updata
            vCampuri = new List<string>
            {
                "capitolId",
                "col1",
                "col2"
            };
            vValori = ManipuleazaBD.fRezultaUnString("SELECT capitole.capitolId, capitole.col1, capitole.col2 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + Session["SESgospodarieId"].ToString() + "') AND (sabloaneCapitole.capitol = '" + pCapitol + "') AND (capitole.codRand = '" + pRand + "')", vCampuri, Convert.ToInt16(Session["SESan"]));
            // calculam suprafata in ari
            vSuprafataInitiala = Convert.ToDecimal(vValori[1].Replace('.', ',')) * 100 + Convert.ToDecimal(vValori[2].Replace('.', ','));
            vSuprafataDinTabel = Convert.ToDecimal(pHa.Replace('.', ',')) * 100 + Convert.ToDecimal(pAri.Replace('.', ','));
            vSuprafataDeScris = vSuprafataInitiala - vSuprafataDinTabel;
            if ( vSuprafataDeScris < 0 )
                vSuprafataDeScris = 0;
            vSuprafataDeScrisString = CalculeazaAriHa("0", "0", vSuprafataDeScris.ToString(), "0");
            ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vSuprafataDeScrisString[0] + "', col2 = CONVERT(decimal(18,4),'" + vSuprafataDeScrisString[1].Replace(',', '.') + "') WHERE (capitolId = '" + vValori[0] + "')", Convert.ToInt16(Session["SESan"]));
        }
        // 4b, 4c ****************************************
        else
        {
            // gasim linia de capitol pentru randul care se va updata
            vCampuri = new List<string>
            {
                "capitolId",
                "col1"
            };
            vValori = ManipuleazaBD.fRezultaUnString("SELECT capitole.capitolId, capitole.col1 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + Session["SESgospodarieId"].ToString() + "') AND (sabloaneCapitole.capitol = '" + pCapitol + "') AND (capitole.codRand = '" + pRand + "')", vCampuri, Convert.ToInt16(Session["SESan"]));
            // calculam suprafata in ari
            try
            {
                vSuprafataInitiala = (Convert.ToDecimal(vValori[1].Replace('.', ','))) / 100;
                
                vSuprafataDinTabel = Convert.ToDecimal(pHa.Replace('.', ',')) * 100 + Convert.ToDecimal(pAri.Replace('.', ','));
            }
            catch
            {
                vSuprafataDinTabel = 0;
            }
            // sumele le mai inmultim cu 100, fiind in mp
            vSuprafataDeScris = (vSuprafataInitiala - vSuprafataDinTabel) * 100;
            
            if ( vSuprafataDeScris < 0 ) 
                vSuprafataDeScris = 0;
            ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vSuprafataDeScris.ToString().Replace(',', '.') + "' WHERE (capitolId = '" + vValori[0] + "')", Convert.ToInt16(Session["SESan"]));
        }
    }
  
    private void ReseteazaControaleLaSchimbareDeCapitolSauCultura()
    {
        Parcele parcela = (Parcele)ViewState["parcela"];
        statusLabel.Text = "Suprafaţa de cultivat în capitolul şi cu cultura selectate:";
        
        tbAtribuireHa.Enabled = capitoleDropDownList.SelectedIndex <= 1;
        tbAtribuireAri.Enabled = capitoleDropDownList.SelectedIndex <= 1;
        tbAtribuireMp.Enabled = capitoleDropDownList.SelectedIndex > 1;

        if(tbAtribuireMp.Enabled)
        {
            tbAtribuireMp.Text = parcela.SuprafataRamasaDeCultivat.ToMetriPatrati().ToString().Replace(" mp","");
            tbAtribuireHa.Text = "0";
            tbAtribuireAri.Text = "0,0000";
        }
        else
        {
            tbAtribuireHa.Text = parcela.SuprafataRamasaDeCultivat.ToString(Utils.Marime.ha).Replace("ha", "").Trim();
            tbAtribuireAri.Text = parcela.SuprafataRamasaDeCultivat.ToString(Utils.Marime.ari).Replace("ari", "").Trim();
            tbAtribuireMp.Text = "0,0000";
        }

        ArataMesajDacaSuprafataEsteCultivataComplet(parcela.SuprafataRamasaDeCultivat);

        btModificaAtribuiri.Text = "adaugă";
        ViewState["parcelaAtribuita"] = "a";
        lblASters.Text = "da";

        gvAtribuire4a.SelectedIndex = -1;
        gvAtribuire4a1.SelectedIndex = -1;
        gvAtribuire4b1.SelectedIndex = -1;
        gvAtribuire4b2.SelectedIndex = -1;
        gvAtribuire4c.SelectedIndex = -1;
       
        btStergeAtribuire.Visible = false;
    }

    private void AdaugaDateInParcela(Parcele parcela)
    {
        parcela.An = Convert.ToInt16(Session["SESan"].ToString());
        parcela.UnitateId = Convert.ToInt16(Session["SESunitateId"].ToString());
        parcela.IdGospodarie = Convert.ToInt64(Session["SESgospodarieId"].ToString());
        
        try
        {
           parcela.Id = Convert.ToInt64(((Label)gvParcele.SelectedRow.Cells[0].FindControl("lblParcelaIdGv")).Text);
        }
        catch{}

        CompleteazaCulturi(parcela);
    }
   
    private void CompleteazaCulturi(Parcele parcela)
    {
        InitializeazaCulturiInParcela(parcela);

        AdaugaCulturaInParcela(parcela);
    }

    private void InitializeazaCulturiInParcela(Parcele parcela)
    {
        parcela.CulturaIn4a.Add( new CulturiIn4a
        {
            SuprafataCultivata = new Suprafete()
        });

        parcela.CulturaIn4a1.Add( new CulturiIn4a1
        {
            SuprafataCultivata = new Suprafete()
        });

        parcela.CulturaIn4b1.Add( new CulturiIn4b1
        {
            SuprafataCultivata = new Arii()
        });

        parcela.CulturaIn4b2.Add( new CulturiIn4b2
        {
            SuprafataCultivata = new Arii()
        });

        parcela.CulturaIn4c.Add( new CulturiIn4c
        {
            SuprafataCultivata = new Arii()
        });
    }
   
    private void AdaugaCulturaInParcela(Parcele parcela)
    {
        Capitole capitol = new Capitole();
        Suprafete suprafata;
        ParseCapitolToEnum(capitol);

        string codRand = culturiDropDownList.Text.Split('.')[0];

        switch (capitol.Cod)
        {
            case Utils.CapitolCulturi.A:
                parcela.CulturaIn4a[parcela.CulturaIn4a.Count - 1].CodRand = codRand;
                parcela.CulturaIn4a[parcela.CulturaIn4a.Count - 1].Denumire = culturiDropDownList.SelectedItem.Text;
                suprafata =  new Suprafete
                {
                    Hectare = Convert.ToDecimal(tbAtribuireHa.Text),
                    Ari = Convert.ToDecimal(tbAtribuireAri.Text.Replace('.', ','))
                };

                while( suprafata.Ari >= 100 )
                {
                    suprafata.Hectare += 1;
                    suprafata.Ari -= 100;
                }
                
                parcela.CulturaIn4a[parcela.CulturaIn4a.Count - 1].SuprafataCultivata = suprafata;

                parcela.CulturaIn4a[parcela.CulturaIn4a.Count - 1].AddCultura(parcela);
                break;

            case Utils.CapitolCulturi.A1:
                parcela.CulturaIn4a1[parcela.CulturaIn4a1.Count - 1].CodRand = codRand;
                parcela.CulturaIn4a1[parcela.CulturaIn4a1.Count - 1].Denumire = culturiDropDownList.SelectedItem.Text;
                suprafata =  new Suprafete
                {
                    Hectare = Convert.ToDecimal(tbAtribuireHa.Text),
                    Ari = Convert.ToDecimal(tbAtribuireAri.Text.Replace('.', ','))
                };

                while( suprafata.Ari >= 100 )
                {
                    suprafata.Hectare += 1;
                    suprafata.Ari -= 100;
                }
                
                parcela.CulturaIn4a1[parcela.CulturaIn4a1.Count - 1].SuprafataCultivata = suprafata;
                parcela.CulturaIn4a1[parcela.CulturaIn4a1.Count - 1].AddCultura(parcela);
                break;

            case Utils.CapitolCulturi.B1:
                parcela.CulturaIn4b1[parcela.CulturaIn4b1.Count - 1].CodRand = codRand;
                parcela.CulturaIn4b1[parcela.CulturaIn4b1.Count - 1].Denumire = culturiDropDownList.SelectedItem.Text;
                parcela.CulturaIn4b1[parcela.CulturaIn4b1.Count - 1].SuprafataCultivata = new Arii
                {
                    Arie = Convert.ToDecimal(tbAtribuireMp.Text.Replace('.', ','))
                };
                parcela.CulturaIn4b1[parcela.CulturaIn4b1.Count - 1].AddCultura(parcela);
                break;

            case Utils.CapitolCulturi.B2:
                parcela.CulturaIn4b2[parcela.CulturaIn4b2.Count - 1].CodRand = codRand;
                parcela.CulturaIn4b2[parcela.CulturaIn4b2.Count - 1].Denumire = culturiDropDownList.SelectedItem.Text;
                parcela.CulturaIn4b2[parcela.CulturaIn4b2.Count - 1].SuprafataCultivata = new Arii
                {
                    Arie = Convert.ToDecimal(tbAtribuireMp.Text.Replace('.', ','))
                };
                parcela.CulturaIn4b2[parcela.CulturaIn4b2.Count - 1].AddCultura(parcela);
                break;

            case Utils.CapitolCulturi.C:
                parcela.CulturaIn4c[parcela.CulturaIn4c.Count - 1].CodRand = codRand;
                parcela.CulturaIn4c[parcela.CulturaIn4c.Count - 1].Denumire = culturiDropDownList.SelectedItem.Text;
                parcela.CulturaIn4c[parcela.CulturaIn4c.Count - 1].SuprafataCultivata = new Arii
                {
                    Arie = Convert.ToDecimal(tbAtribuireMp.Text.Replace('.', ','))
                };
                parcela.CulturaIn4c[parcela.CulturaIn4c.Count - 1].AddCultura(parcela);
                break;
        }
    }

    private bool ModAdaugareCulturi()
    {
        return ViewState["parcelaAtribuita"].ToString() == "a";
    }

    private bool ArataMesajDacaSuprafataEsteCultivataComplet(Suprafete suprafataRamasaDeCultivat)
    {
        if (suprafataRamasaDeCultivat == 0)
        {
            lblMesajPlin.Visible = true;
        
            lblMesajPlin.Text = "Suprafaţa parcelei este complet atribuită. Dacă mai doriţi introducerea de alte culturi, ştergeţi sau modificaţi una din atribuirile din tabelul de mai jos.";
            tbAtribuireAri.Text = "0,0000";
            tbAtribuireHa.Text = "0";

            return true;
        }
        else
        {
            lblMesajPlin.Visible = false;

            return false;
        }
    }
   
    private void EnableOrDisableControaleSuprafete()
    {
        tbAtribuireHa.Enabled = gvAtribuire4a.SelectedIndex != -1 || gvAtribuire4a1.SelectedIndex != -1;
        tbAtribuireAri.Enabled = gvAtribuire4a.SelectedIndex != -1 || gvAtribuire4a1.SelectedIndex != -1;
        tbAtribuireMp.Enabled = gvAtribuire4a.SelectedIndex == -1 && gvAtribuire4a1.SelectedIndex == -1;
    }

    private void ResetControls()
    {
        btStergeAtribuire.Visible = false;
        tbAtribuireHa.Enabled = true;
        tbAtribuireAri.Enabled = true;
        tbAtribuireMp.Enabled = false;
        
        capitoleDropDownList.SelectedIndex = 0;
        culturiDropDownList.SelectedIndex = -1;
        
        btModificaAtribuiri.Text = "adaugă";
       
        statusLabel.Text = "Suprafaţa de cultivat în capitolul şi cu cultura selectate:";

        ViewState["parcelaAtribuita"] = "a";

        Parcele parcela = (Parcele)ViewState["parcela"];

        InitializeazaListeleDeCulturiInParcela(parcela);
        IncarcaCulturileInParcela(parcela);
        CalculeazaSuprafataCultivataPeParcela(parcela);

        ViewState["parcela"] = null;
        ViewState["parcela"] = parcela;

        lblDenumireaParcelei.Text = parcela.Denumire;
        lblSuprafataInFolosinta.Text = parcela.SuprafataInFolosinta.ToString();
        lblSuprafataCultivata.Text = parcela.SuprafataCultivata.ToString();
        lblSuprafataRamasaDeCultivat.Text = parcela.SuprafataRamasaDeCultivat.ToString();
        tbAtribuireHa.Text = parcela.SuprafataRamasaDeCultivat.ToString(Utils.Marime.ha).Replace("ha", "").Trim();
        tbAtribuireAri.Text = parcela.SuprafataRamasaDeCultivat.ToString(Utils.Marime.ari).Replace("ari", "").Trim();
        tbAtribuireMp.Text = "0,0000";
        btModificaAtribuiri.Enabled = tbAtribuireAri.Text != "0,0000" || tbAtribuireHa.Text != "0";

        ArataMesajDacaSuprafataEsteCultivataComplet(parcela.SuprafataRamasaDeCultivat);

        BindAtribuire4aGridView(parcela.CulturaIn4a);
        BindAtribuire4a1GridView(parcela.CulturaIn4a1);
        BindAtribuire4b1GridView(parcela.CulturaIn4b1);
        BindAtribuire4b2GridView(parcela.CulturaIn4b2);
        BindAtribuire4cGridView(parcela.CulturaIn4c);
    }

    #endregion Culturi
   
    protected void TiparireCapitolAnCurentButtonClick(object sender, EventArgs e)
    {
        ResponseHelper.Redirect("~/printCapitol2bAnCurent.aspx", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
    }
    
    protected void ValidariNumerice()
    {
        TextBox[] vControale = new TextBox[]
        {
            tbExtravilanAri,
            tbExtravilanHa,
            tbIntravilanAri,
            tbIntravilanHa
        };
        
        foreach ( TextBox vControl in vControale )
        {
            if ( !Valideaza.ENumeric(vControl.Text.Replace(',', '.')) || (Valideaza.ENumeric(vControl.Text.Replace(',', '.')) && Convert.ToDecimal(vControl.Text.Replace(',', '.')) < 0) )
            {
                vControl.Text = "0";
            }
        }
        
        // extravilan
        Decimal vHa = Convert.ToDecimal(tbExtravilanHa.Text.Replace('.', ',')) * 100;
        Decimal vAri = Convert.ToDecimal(tbExtravilanAri.Text.Replace('.', ','));
        Decimal vHaFinal = Math.Truncate((vAri + vHa) / 100);
        
        Decimal vAriFinal = vHa + vAri - vHaFinal * 100;
        
        tbExtravilanHa.Text = (Convert.ToInt32(vHaFinal)).ToString();
        tbExtravilanAri.Text = vAriFinal.ToString();
        
        // intravilan
        vHa = Convert.ToDecimal(tbIntravilanHa.Text.Replace('.', ',')) * 100;
        vAri = Convert.ToDecimal(tbIntravilanAri.Text.Replace('.', ','));
        vHaFinal = Math.Truncate((vAri + vHa) / 100);
        
        vAriFinal = vHa + vAri - vHaFinal * 100;
        
        tbIntravilanHa.Text = (Convert.ToInt32(vHaFinal)).ToString();
        tbIntravilanAri.Text = vAriFinal.ToString();
    }
    
    protected int GasesteRandul(int pValoare)
    {
        int vRand = -1;
        
        for ( int i = 0; i < gvParcele.Rows.Count; i++ )
        {
            if ( gvParcele.Rows[i].Cells[1].Text == pValoare.ToString() )
            {
                vRand = i;
                break;
            }
        }
        
        return vRand;
    }
    
    protected int EvitamDublarea()
    {
        // cautam parcelaTimpAdaugare
        DateTime vTimpSalvat = new DateTime();
        int vExista = 0;
        
        try
        {
            vTimpSalvat = Convert.ToDateTime(ManipuleazaBD.fRezultaUnString("SELECT TOP(1) coalesce(parcelaTimpAdaugare, '01.01.2000 00:00:00') as  parcelaTimpAdaugare FROM parcele WHERE parcelaDenumire = '" + tbDenumire.Text + "' AND gospodarieId = '" + Session["SESgospodarieId"].ToString() + "' ORDER BY parcelaId DESC", "parcelaTimpAdaugare", Convert.ToInt16(Session["SESan"])));
            
            TimeSpan vTimpTrecut = DateTime.Now - vTimpSalvat;
            long vTicuri = vTimpTrecut.Ticks;
            if ( vTicuri < 100000000 ) 
            {
                vExista = 1;
            }
        }
        catch
        {
        }
        
        return vExista;
    }
    
    protected string[] CalculeazaAriHa(string pHa1, string pHa2, string pAri1, string pAri2)
    {
        string[] vValori =
        {
            "0",
            "0"
        };
        if ( pHa1 == "" )
        {
            pHa1 = "0";
        }
        
        if ( pHa2 == "" )
        {
            pHa2 = "0";
        }
        
        if ( pAri1 == "" )
        {
            pAri1 = "0";
        }
        
        if ( pAri2 == "" )
        {
            pAri2 = "0";
        }
        
        decimal vValoare = Convert.ToDecimal(pHa1) * 100 + Convert.ToDecimal(pHa2) * 100 + Convert.ToDecimal(pAri1) + Convert.ToDecimal(pAri2);
        
        vValori[1] = (vValoare % 100).ToString("F2");
        vValori[0] = Convert.ToInt64((vValoare - (vValoare % 100)) / 100).ToString("F0");
        
        return vValori;
    }
    
    protected string CalculeazaAri(string pHa, string pAri)
    {
        string vValori = "0";
        decimal vValoare = Convert.ToDecimal(pHa) * 100 + Convert.ToDecimal(pAri);
        
        vValori = vValoare.ToString();
        
        return vValori;
    }
    
    protected void ResetButton_Click(object sender, EventArgs e)
    {
        ResetControls();
    }
}
