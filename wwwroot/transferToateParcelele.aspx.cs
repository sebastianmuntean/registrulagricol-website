﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

/// <summary>
/// Pagina transfer toate parcelele
/// Creata la:                  9.03.2012
/// Autor:                      SM
/// Ultima                      actualizare: 11.03.2012
/// Autor:                      SM 
/// </summary> 
public partial class transferToateParcelele : System.Web.UI.Page
{
    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        DBConnections connection = new DBConnections(Convert.ToInt16(Session["SESan"]));

        SqlSabloaneCapitole.ConnectionString = connection.Create();
        SqlTipTransfer.ConnectionString = connection.Create();

        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "transferParcela", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
        }
    }
    protected void gvGospodarii_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(gvGospodarii, e, this);
    }
    protected void tbData_Init(object sender, EventArgs e)
    {
        tbData.Text = DateTime.Now.Date.ToString("dd.MM.yyyy");
    }
    protected void btAnuleazaSalvarea_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Capitol2b.aspx");
    }
    protected void btSalveazaSablon_Click(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();

        // operam in functie de copiaza si muta 

        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlTransaction vTranz = ManipuleazaBD.DeschideTranzactie(vCon);
        SqlCommand vCmd = new SqlCommand();
        SqlCommand vCmd1 = new SqlCommand();
        vCmd1.Connection = vCon;
        vCmd1.Transaction = vTranz;
        vCmd.Connection = vCon;
        vCmd.Transaction = vTranz;
        int vOK = 0;
         // luam toate parcelele gospodariei
        List<string> vCampuri = new List<string> { };
        vCampuri = ManipuleazaBD.NumeleColoanelor("parcele", Convert.ToInt16(Session["SESan"]));
        List<List<string>> vListaParcele = ManipuleazaBD.fRezultaListaStringuri("SELECT * FROM parcele WHERE gospodarieId = '" + Session["SESgospodarieId"].ToString() + "' and an=" + Session["SESan"], vCampuri, Convert.ToInt16(Session["SESan"])); 
        vCmd.CommandText = "";
        // gasim pozitia parcelaid
        int vPozitieId = -1;
        int i = 0;
        foreach (string vNumeColoana in vCampuri)
        {
            if (vNumeColoana == "parcelaId")
            {
                vPozitieId = i;
                break;
            }
            i++;
        }
        try
        {           
            // daca e muta
            if (ddlCopiazaMuta.SelectedValue == "0")
            {
                foreach (List<string> vParcela in vListaParcele)
                {
                    // modifica id-ul gospodariei
                    vCmd.CommandText += "update parcele set gospodarieId='" + gvGospodarii.SelectedValue.ToString() + "' where parcelaId='" + vParcela[vPozitieId] + "';                ";                   
                    // adaug un rand in istoric parcela 
                    vCmd.CommandText += "INSERT INTO istoricParcela (parcelaId, data, motiv, unitateId, deLa, la, documentAtesta, observatii) VALUES ('" + vParcela[vPozitieId] + "', convert(datetime,'" + tbData.Text + "',104), '" + ddlTipTransfer.SelectedValue.ToString() + "', '" + Session["SESunitateId"].ToString() + "', '" + Session["SESgospodarieId"].ToString() + "', '" + gvGospodarii.SelectedValue.ToString() + "', N'" + tbDocument.Text + "', N'" + ddlCopiazaMuta.SelectedItem.Text + " | " + tbObservatii.Text + "');                ";

                }                
            }
            // daca e copiaza
            else if (ddlCopiazaMuta.SelectedValue == "1")
            {
                foreach (List<string> vParcela in vListaParcele)
                {
                    int j = 0;
                    string vInterogareCampuri = "";
                    string vInterogareValori = "";
                    foreach (string vCamp in vCampuri)
                    {
                        if (vCamp != "parcelaId")
                        {
                            vInterogareCampuri += vCamp + " , ";

                            if (vCamp == "gospodarieId")
                                vInterogareValori += "'" + gvGospodarii.SelectedValue.ToString() + "' , ";
                            else if (vCamp == "parcelaIdInitial")
                                vInterogareValori += "'-123456' , ";
                            else if (vCamp.IndexOf("vilan") != -1)
                                vInterogareValori += "'" + vParcela[j].Replace(',', '.') + "' , ";
                            else
                            {
                                if (vCamp.Contains("parcelaTimpAdaugare"))
                                    vInterogareValori += "convert(datetime,'" + vParcela[j] + "',104) , ";
                                else vInterogareValori += "'" + vParcela[j] + "' , ";
                            }
                        }
                        else
                        {

                        }
                        j++;
                    }
                    vInterogareCampuri = vInterogareCampuri.Remove(vInterogareCampuri.Length -2);
                    vInterogareValori = vInterogareValori.Remove(vInterogareValori.Length - 2); ;

                    // modifica id-ul gospodariei
                    vCmd.CommandText += "INSERT INTO parcele (" + vInterogareCampuri + ") VALUES (" + vInterogareValori + ") ;         ";
                    // adaug un rand in istoric parcela 
                    vCmd.CommandText += "INSERT INTO istoricParcela (parcelaId, data, motiv, unitateId, deLa, la, documentAtesta, observatii) VALUES ('" + vParcela[vPozitieId] + "', convert(datetime,'" + tbData.Text + "',104), '" + ddlTipTransfer.SelectedValue.ToString() + "', '" + Session["SESunitateId"].ToString() + "', '" + Session["SESgospodarieId"].ToString() + "', '" + gvGospodarii.SelectedValue.ToString() + "', N'" + tbDocument.Text + "', N'" + ddlCopiazaMuta.SelectedItem.Text + " | " + tbObservatii.Text + "');                       ";
                }    
            }                
            vCmd.CommandText = vCmd.CommandText.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");     
            vCmd.ExecuteNonQuery();
            // scriem parcelaidinitial
            vCmd.CommandText = "UPDATE parcele SET parcelaIdInitial = parcelaId WHERE parcelaIdInitial = -123456 AND gospodarieId = " + gvGospodarii.SelectedValue.ToString();
            vCmd.ExecuteNonQuery();
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "parcele", "transfer parcela", "", Convert.ToInt64(Session["SESgospodarieId"]), 3);
            vTranz.Commit();
            vOK = 1;
        }
        catch { vTranz.Rollback(); }
        finally { ManipuleazaBD.InchideConexiune(vCon); }
        if (vOK == 1)
        {
            // daca am bifa pe unitati adaug in C2A din C2B
            vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
            vCmd = new SqlCommand();
            vCmd.Connection = vCon;
            vCmd.CommandText = "select top(1) unitateAdaugaDinC2B from unitati where unitateId='" + Session["SESunitateId"].ToString() + "'";
            bool vAdaugaDinC2b = Convert.ToBoolean(vCmd.ExecuteScalar());
            ManipuleazaBD.InchideConexiune(vCon);
            if (vAdaugaDinC2b)
            {
                foreach (List<string> vParcela in vListaParcele)
                {
                    CalculCapitole.AdaugaCapitol2ADin2B(Session["SESunitateId"].ToString(), Session["SESan"].ToString(), Session["SESgospodarieId"].ToString(), "0");
                    CalculCapitole.AdaugaCapitol2ADin2B(Session["SESunitateId"].ToString(), Session["SESan"].ToString(), gvGospodarii.SelectedValue.ToString(), "0");
                }
            }
            Response.Redirect("~/Capitol2b.aspx");
        }
    }
    protected void gvGospodarii_SelectedIndexChanged(object sender, EventArgs e)
    {
        btSalveazaSablon.Visible = true;
    }
}
