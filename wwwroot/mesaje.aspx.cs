﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Drawing;

public partial class mesaje : System.Web.UI.Page
{
    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        DBConnections connection = new DBConnections(Convert.ToInt16(Session["SESan"]));

        SqlMesaje.ConnectionString = connection.Create();
        SqlMesajeTrimise.ConnectionString = connection.Create();

        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "mesaje", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
        }
    }
    protected void gvMesaje_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(gvMesaje, e, this);
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
            SqlCommand vCmd = new SqlCommand();
            vCmd.Connection = vCon;
            Label vLblId = (Label)e.Row.FindControl("lblMesajId");
            vCmd.CommandText = "select top(1) dataCitirii from mesaje where mesajId='" + vLblId.Text + "'";
            DateTime? vDataCitirii = null;
            try { vDataCitirii = Convert.ToDateTime(vCmd.ExecuteScalar()); }
            catch { }
            if (vDataCitirii == null)
            {
                e.Row.Font.Bold = true;
                e.Row.ForeColor = Color.Red;
            }
            ManipuleazaBD.InchideConexiune(vCon);
        }
    }
    protected void gvMesaje_SelectedIndexChanged(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "update mesaje set dataCitirii=convert(datetime,'" + DateTime.Now + "',104) where mesajId='" + gvMesaje.SelectedValue + "'";
        vCmd.ExecuteNonQuery();
        gvMesaje.DataBind();
        vCmd.CommandText = "select mesaj from mesaje where mesajId='" + gvMesaje.SelectedValue + "'";
        lblMesaj.Text = vCmd.ExecuteScalar().ToString();
        pnDetaliiMesaj.Visible = true;
        ManipuleazaBD.InchideConexiune(vCon);
    }
    protected void gvMesajeTrimise_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(gvMesajeTrimise, e, this);
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
            SqlCommand vCmd = new SqlCommand();
            vCmd.Connection = vCon;
            Label vLblId = (Label)e.Row.FindControl("lblMesajId");
            vCmd.CommandText = "select top(1) dataCitirii from mesaje where mesajId='" + vLblId.Text + "'";
            DateTime? vDataCitirii = null;
            try { vDataCitirii = Convert.ToDateTime(vCmd.ExecuteScalar()); }
            catch { }
            if (vDataCitirii == null)
            {
                e.Row.Font.Bold = true;
                e.Row.ForeColor = Color.Red;
            }
            ManipuleazaBD.InchideConexiune(vCon);
        }
    }
    protected void gvMesajeTrimise_SelectedIndexChanged(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        //vCmd.CommandText = "update mesaje set dataCitirii=convert(datetime,'" + DateTime.Now + "',104) where mesajId='" + gvMesajeTrimise.SelectedValue + "'";
        //vCmd.ExecuteNonQuery();
        //gvMesajeTrimise.DataBind();
        vCmd.CommandText = "select mesaj from mesaje where mesajId='" + gvMesajeTrimise.SelectedValue + "'";
        lblMesajTrimis.Text = vCmd.ExecuteScalar().ToString();
        pnDetaliiMesajTrimis.Visible = true;
        ManipuleazaBD.InchideConexiune(vCon);
    }
}
