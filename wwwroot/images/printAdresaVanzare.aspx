﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="printAdresaVanzare.aspx.cs" Inherits="printAdresaVanzare" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt"
        Height="400px" Width="400px">
        <LocalReport ReportPath="rapoarte\raportAdresaVanzare.rdlc">
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="rapoarteDosareVanzareDataSet_dtAdresaVanzare" />
            </DataSources>
        </LocalReport>
    </rsweb:ReportViewer>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetAdresa"
        TypeName="rapoarteDosareVanzareDataSetTableAdapters.dtAdresaVanzareTableAdapter">
        <SelectParameters>
            <asp:SessionParameter Name="gospodarieId" SessionField="SESgospodarieId" Type="Int32" />
            <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" Type="Int32" />
            <asp:SessionParameter Name="idOferta" SessionField="SESidOferta" Type="Int32" />
            <asp:SessionParameter Name="an" SessionField="SESan" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>

