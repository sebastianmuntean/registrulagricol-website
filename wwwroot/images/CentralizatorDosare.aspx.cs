﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CentralizatorDosare : System.Web.UI.Page
{
    SqlCommand command = new SqlCommand();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "Cap.2b", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
        }
        RefreshListaDosareGridView();
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["SESgospodarieId"] == null)
        {
            HttpCookie vCookie = Request.Cookies["COOKTNT"];
            vCookie = CriptareCookie.DecodeCookie(vCookie);
            if (vCookie != null)
            {
                if (vCookie["COOKgospodarieId"] != null)
                {
                    Session["SESgospodarieId"] = vCookie["COOKgospodarieId"].ToString();
                }
                else if (Request.Url.AbsolutePath.IndexOf("Gospodarii.aspx") == -1 && Session["SESgospodarieId"] == null)
                    Response.Redirect("~/Gospodarii.aspx");
            }
        }
    }

    protected void printCentralizatorDosareButton_OnClick(Object sender, EventArgs e)
    {
        if (tbNrDosar.Text == string.Empty)
            tbNrDosar.Text = "%";
        if (tbNrRegGeneral.Text == "")
            tbNrRegGeneral.Text = "%";
        if (volumTextBox.Text == "")
            volumTextBox.Text = "%";
        if (pozitieTextBox.Text == "")
            pozitieTextBox.Text = "%";
        if (vanzatorTextBox.Text == "")
            vanzatorTextBox.Text = "%";
        if (deLaTextBox.Text == "")
            deLaTextBox.Text = "01.01.1900";
        if (panaLaTextBox.Text == "")
            panaLaTextBox.Text = "12.12.2099";

        ResponseHelper.Redirect("~/printCentralizatorDosare.aspx?&nrDosar=" + tbNrDosar.Text + "&nrRegistruGeneral=" + tbNrRegGeneral.Text + "&volumInt=" + volumTextBox.Text + "&nrPozitieInt" + pozitieTextBox.Text + "&deLa='" + deLaTextBox.Text + "'&panaLa='" + panaLaTextBox.Text + "'&membruNume=" + vanzatorTextBox.Text, "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
        ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "centralizatorDosare", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
    }

    protected void dosareGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(dosareGridView, e, this);
        e.Row.Cells[0].Visible = false;
    }

    protected void dosareGridView_SelectedIndexChanged(object sender, EventArgs e)
    {
    }

    protected void PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        dosareGridView.PageIndex = e.NewPageIndex;
        RefreshListaDosareGridView();
    }

    private DataTable GetListaDosare()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune();
        DataTable table = CreateCentralizatorDosareTableStructure();
        command.Connection = connection;
        command.CommandText = "select * from OferteVanzare join parcele on OferteVanzare.idParcela = parcele.parcelaId  INNER JOIN sabloaneCapitole ON parcele.parcelaCategorie = sabloaneCapitole.codRand Inner join gospodarii on gospodarii.gospodarieId = OferteVanzare.idGospodarie WHERE (sabloaneCapitole.capitol = '2a')  AND (parcele.an = " + Session["SESan"] + ") AND (sabloaneCapitole.an = " + Session["SESan"] + ") AND (OferteVanzare.unitateId = " + Session["SESunitateId"] + ") AND OferteVanzare.nrDosar LIKE '%" + tbNrDosar.Text + "%' AND OferteVanzare.nrRegistruGeneral LIKE '%" + tbNrRegGeneral.Text + "%'   and (gospodarii.membruNume LIKE '%" + vanzatorTextBox.Text + "%') and (volumInt LIKE  '%" + volumTextBox.Text + "%' ) and (nrPozitieInt LIKE  '%" + pozitieTextBox.Text + "%' )";
        SqlDataReader dataReader = command.ExecuteReader();
        while (dataReader.Read())
        {
            PopulateListaCentralizatorDosareTable(table, dataReader);
        }
        ManipuleazaBD.InchideConexiune(connection);
        return table;
    }

    private static void PopulateListaCentralizatorDosareTable(DataTable table, SqlDataReader dataReader)
    {
        DataRow row = table.NewRow();
        row["idOferta"] = dataReader["idOferta"];
        row["Nr. Cerere"] = dataReader["nrDosar"];
        row["Nr. Registru general"] = dataReader["nrRegistruGeneral"];
        row["Data inregistrarii"] = Convert.ToDateTime(dataReader["dataInregistrarii"]).ToShortDateString();
        row["Denumire parcela"] = dataReader["parcelaDenumire"];
        row["Nr TOPO"] = dataReader["parcelaNrTopo"];
        row["CF"] = dataReader["parcelaCF"];
        row["Nr. Cadastral"] = dataReader["parcelaNrCadastral"];
        row["Nr. Cadastral provizioriu"] = dataReader["parcelaNrCadastralProvizoriu"];
        row["Bloc fizic"] = dataReader["parcelaNrBloc"];
        row["Adresa"] = dataReader["parcelaAdresa"];
        row["Suprafata oferita Ha"] = dataReader["suprafataOferitaHa"];
        row["Suprafata oferita Ari"] = dataReader["suprafataOferitaAri"];
        row["Suma"] = dataReader["valoareParcela"];
        row["Categorie"] = dataReader["denumire4"];
        row["Volum"] = dataReader["volumInt"];
        row["Nr.pozitie"] = dataReader["nrPozitieInt"];
        row["Vanzator"] = dataReader["membruNume"];
        table.Rows.Add(row);
    }

    private static DataTable CreateCentralizatorDosareTableStructure()
    {
        DataTable table = new DataTable();
        table.Columns.Add("idOferta");
        table.Columns.Add("Nr. Cerere");
        table.Columns.Add("Nr. Registru general");
        table.Columns.Add("Data inregistrarii");
        table.Columns.Add("Vanzator");
        table.Columns.Add("Volum");
        table.Columns.Add("Nr.pozitie");
        table.Columns.Add("Denumire parcela");
        table.Columns.Add("Categorie");
        table.Columns.Add("Nr TOPO");
        table.Columns.Add("CF");
        table.Columns.Add("Nr. Cadastral");
        table.Columns.Add("Nr. Cadastral provizioriu");
        table.Columns.Add("Bloc Fizic");
        table.Columns.Add("Adresa");
        table.Columns.Add("Suprafata oferita Ha");
        table.Columns.Add("Suprafata oferita Ari");
        table.Columns.Add("Suma");
        return table;
    }

    private void RefreshListaDosareGridView()
    {
        dosareGridView.DataSource = GetListaDosare();
        dosareGridView.DataBind();
    }
}