﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Gospodarii.aspx.cs" Inherits="Gospodarii" EnableEventValidation="false"
    UICulture="ro-Ro" Culture="ro-Ro" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="<%$ Resources:Resursa, raGospodarii%>" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnListaGospodarii" runat="server" CssClass="panel_general" Visible="true">
                <asp:Panel ID="Panel4" runat="server">
                    <asp:Label ID="lblEroare1" runat="server" CssClass="validator" Text="Eroare" Visible="false" />
                </asp:Panel>
                <asp:Panel ID="pnCautare" runat="server" CssClass="cauta">
                    <asp:Label ID="lbCautaDupa" runat="server" Text="<%$ Resources:Resursa, raCauta%>"></asp:Label>:
                    <asp:Label ID="lbfVolum" runat="server" Text="<%$ Resources:Resursa, raVolum%>"></asp:Label>
                    <asp:TextBox ID="tbfVolum" Width="37px" runat="server" AutoPostBack="True" OnTextChanged="tbfVolum_TextChanged"></asp:TextBox>
                    <asp:Label ID="lbfNrPoz" runat="server" Text="<%$ Resources:Resursa, raNrPozitie%>"></asp:Label>
                    <asp:TextBox ID="tbfNrPoz" Width="37px" runat="server" AutoPostBack="True" OnTextChanged="tbfNrPoz_TextChanged"></asp:TextBox>
                    <asp:Label ID="ldfNume" runat="server" Text="<%$ Resources:Resursa, raNume%>"></asp:Label>
                    <asp:TextBox ID="tbfNume" Width="70px" runat="server" AutoPostBack="True" OnTextChanged="tbfNume_TextChanged"></asp:TextBox>
                    <asp:Label ID="lbfLoc" runat="server" Text="<%$ Resources:Resursa, raLocalitate%>"></asp:Label>
                    <asp:TextBox ID="tbfLoc" Width="70px" runat="server" AutoPostBack="True" OnTextChanged="tbfLoc_TextChanged"></asp:TextBox>
                    <asp:Label ID="lbfStrada" runat="server" Text="<%$ Resources:Resursa, raStr%>"></asp:Label>
                    <asp:TextBox ID="tbfStrada" Width="70px" runat="server" AutoPostBack="True" OnTextChanged="tbfStrada_TextChanged"></asp:TextBox>
                    <asp:Label ID="lbfNr" runat="server" Text="<%$ Resources:Resursa, raNr%>"></asp:Label>
                    <asp:TextBox ID="tbfNr" Width="30px" runat="server" AutoPostBack="True" OnTextChanged="tbfNr_TextChanged"></asp:TextBox>
                    <asp:Label ID="lblFBl" runat="server" Text="Bl"></asp:Label>
                    <asp:TextBox ID="tbfBl" runat="server" Width="30px" AutoPostBack="true" OnTextChanged="tbfBl_TextChanged"></asp:TextBox>
                </asp:Panel>
                <asp:Panel ID="Panel1" runat="server" CssClass="cauta">
                    <asp:Label ID="lblFSat" runat="server" Text="Sat"></asp:Label>
                    <asp:TextBox ID="tbFSat" AutoPostBack="true" Width="70px" runat="server"></asp:TextBox>
                    <asp:Label ID="lblfCnp" runat="server" Text="<%$ Resources:Resursa, raCNP%>"></asp:Label>
                    <asp:TextBox ID="tbfCnp" AutoPostBack="true" runat="server" Width="150"></asp:TextBox>
                    <asp:Label ID="lblfCodApia" runat="server" Text="<%$ Resources:Resursa, raNrAPIA%>"></asp:Label>
                    <asp:TextBox ID="tbfCodApia" AutoPostBack="true" runat="server" Width="70"></asp:TextBox>
                    <asp:Label ID="lblfCui" runat="server" Text="<%$ Resources:Resursa, raCUIPFA%>"></asp:Label>
                    <asp:TextBox ID="tbfCui" AutoPostBack="true" runat="server" Width="70"></asp:TextBox>
                    <asp:Label ID="lblfSiruta" runat="server" Text="<%$ Resources:Resursa, raCodSiruta%>"></asp:Label>
                    <asp:TextBox ID="tbfSiruta" runat="server" AutoPostBack="True" Width="50"></asp:TextBox>
                </asp:Panel>
                <asp:Panel ID="Panel5" runat="server" CssClass="cauta">
                    <asp:Label ID="lblFJudet" runat="server" Text="<%$ Resources:Resursa, raJudet%>"></asp:Label>
                    <asp:DropDownList ID="ddlFJudet" AutoPostBack="true" runat="server" OnPreRender="ddlFJudet_PreRender"
                        OnSelectedIndexChanged="ddlFJudet_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:Label ID="lblUnitate" runat="server" Text="<%$ Resources:Resursa, raUnitatea%>"></asp:Label>
                    <asp:DropDownList ID="ddlUnitate" Width="177px" AutoPostBack="True" runat="server"
                        OnInit="ddlfUnitati_Init" OnPreRender="ddlUnitate_PreRender" OnSelectedIndexChanged="ddlUnitate_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:Label ID="lblFTip" runat="server" Text="<%$ Resources:Resursa, raTip%>"></asp:Label>
                    <asp:DropDownList ID="ddlFTip" AutoPostBack="true" runat="server" Width="110px" OnSelectedIndexChanged="ddlFTip_SelectedIndexChanged">
                        <asp:ListItem Value="%">Toate</asp:ListItem>
                        <asp:ListItem Value="1">Localnic</asp:ListItem>
                        <asp:ListItem Value="2">Străinaş</asp:ListItem>
                        <asp:ListItem Value="3">Firmă pe raza localităţii</asp:ListItem>
                        <asp:ListItem Value="4">Firmă străinaşă</asp:ListItem>
                    </asp:DropDownList>
                    <asp:Label ID="lblOrdonareDupa" runat="server" Text="<%$ Resources:Resursa, raOrdonareDupa%>"></asp:Label>
                    <asp:DropDownList AutoPostBack="true" ID="ddlFOrdonareDupa" runat="server" OnSelectedIndexChanged="ddlFOrdonareDupa_SelectedIndexChanged"
                        OnInit="ddlFOrdonareDupa_Init">
                        <asp:ListItem Text="Volum, nr. poziţie" Value="0"></asp:ListItem>
                        <asp:ListItem Text="Localitate, stradă, nr." Value="1"></asp:ListItem>
                        <asp:ListItem Text="Nume" Value="2"></asp:ListItem>
                        <asp:ListItem Text="ID crescător" Value="3"></asp:ListItem>
                        <asp:ListItem Text="ID descrescător" Value="4"></asp:ListItem>
                    </asp:DropDownList>
                </asp:Panel>
                <asp:Panel ID="pnGrid" runat="server" Visible="true">
                    <asp:GridView ID="gvGospodarii" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        BorderStyle="None" CellPadding="3" GridLines="Vertical" ShowFooter="True" CssClass="tabela"
                        DataKeyNames="gospodarieId" DataSourceID="SqlGospodarii" EmptyDataText="Nu sunt adaugate inregistrari"
                        AllowPaging="True" OnRowDataBound="gvGospodarii_RowDataBound" OnSelectedIndexChanged="gvGospodarii_SelectedIndexChanged"
                        OnDataBound="gvGospodarii_DataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="gospodarieId" SortExpression="gospodarieId" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblGospodarieId" runat="server" Text='<%# Bind("gospodarieId") %>'
                                        ToolTip='<%# Bind("volumInt") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="volum" HeaderText="Volum" SortExpression="volumInt" />
                            <asp:BoundField DataField="nrPozitie" HeaderText="Număr poziţie" SortExpression="nrPozitieInt" />
                            <asp:BoundField DataField="nume" HeaderText="Nume" SortExpression="nume" />
                            <asp:BoundField DataField="cnp" HeaderText="CNP" SortExpression="cnp" />
                            <asp:BoundField DataField="codSiruta" HeaderText="Cod şiruta" SortExpression="codSiruta" />
                            <asp:BoundField DataField="tip" HeaderText="Tip" SortExpression="tip" />
                            <asp:BoundField DataField="judet" HeaderText="Judeţ" SortExpression="judet" />
                            <asp:BoundField DataField="localitate" HeaderText="Localitate" SortExpression="localitate" />
                            <asp:BoundField DataField="strada" HeaderText="Strada" SortExpression="strada" />
                            <asp:BoundField DataField="nr" HeaderText="Număr" SortExpression="nr" />
                            <asp:BoundField DataField="nrInt" Visible="false" HeaderText="Număr întreg" SortExpression="nrInt" />
                            <asp:BoundField DataField="bl" HeaderText="Bloc" SortExpression="bl" />
                            <asp:BoundField DataField="codExploatatie" HeaderText="Cod exploataţie" SortExpression="codExploatatie" />
                            <asp:BoundField DataField="codUnic" HeaderText="Nr.APIA" SortExpression="codUnic" />
                            <asp:BoundField DataField="gospodarieCui" HeaderText="CUI(PFA)" SortExpression="gospodarieCui" />
                            <asp:BoundField DataField="observatii" HeaderText="Observaţii"
                                SortExpression="observatii" />
                        </Columns>
                        <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                        <HeaderStyle Font-Bold="True" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlGospodarii" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString %>"
                        SelectCommand="SELECT top(1) '' AS cnp, gospodarii.gospodarieId, gospodarii.unitateId, gospodarii.volum, gospodarii.nrPozitie, gospodarii.volumInt, gospodarii.nrPozitieInt, gospodarii.codSiruta, gospodarii.gospodarieCui, CASE gospodarii.tip WHEN '1' THEN 'Localnic' WHEN '2' THEN 'Străinaş' WHEN '3' THEN 'Firmă pe raza localităţii' ELSE 'Firmă străinaşă' END AS tip, '' AS nr, gospodarii.nrInt, gospodarii.bl, gospodarii.sc, gospodarii.et, gospodarii.ap, gospodarii.codExploatatie, gospodarii.codUnic, CASE strainas WHEN 1 THEN sjudet ELSE judet END AS judet, CASE strainas WHEN 1 THEN slocalitate ELSE localitate END AS localitate, gospodarii.persJuridica, gospodarii.jUnitate, gospodarii.jSubunitate, gospodarii.jCodFiscal, gospodarii.jNumeReprez, gospodarii.strainas, gospodarii.sStrada, gospodarii.sNr, gospodarii.sBl, gospodarii.sSc, gospodarii.sEtj, gospodarii.sAp, gospodarii.sJudet, gospodarii.sLocalitate, '' AS nume, COALESCE (gospodarii.gospodarieSat, '') AS gospodarieSat, gospodarii.observatii FROM gospodarii INNER JOIN unitati ON gospodarii.unitateId = unitati.unitateId WHERE (COALESCE (gospodarii.volum, N'') LIKE @volum) AND (COALESCE (gospodarii.nrPozitie, N'') LIKE @nrPozitie) AND (COALESCE (CASE persjuridica WHEN 0 THEN (SELECT TOP (1) nume FROM membri WHERE an = @an AND gospodarieId = gospodarii.gospodarieid AND codrudenie = 1) ELSE junitate END, N'') LIKE '%' + @nume + '%') AND (gospodarii.tip LIKE @tip) AND (COALESCE (CASE strainas WHEN 1 THEN slocalitate ELSE localitate END, N'') LIKE '%' + @localitate + '%') AND (COALESCE (CASE strainas WHEN 1 THEN sstrada ELSE strada END, N'') LIKE '%' + @strada + '%') AND (COALESCE (CASE strainas WHEN 1 THEN snr ELSE nr END, N'') LIKE @nr) AND (gospodarii.an = @an) AND (CONVERT (nvarchar, (SELECT TOP (1) cnp FROM membri AS membri_1 WHERE (unitateId = @unitateId) AND (an = @an)"
                        OnSelecting="SqlGospodarii_Selecting">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="ddlUnitate" Name="unitateId" PropertyName="SelectedValue" />
                            <asp:SessionParameter Name="an" SessionField="SESan" />
                            <asp:ControlParameter ControlID="tbfVolum" DefaultValue="%" Name="volum" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfNrPoz" DefaultValue="%" Name="nrPozitie" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfNume" DefaultValue="%" Name="nume" PropertyName="Text" />
                            <asp:ControlParameter ControlID="ddlFTip" Name="tip" PropertyName="SelectedValue" />
                            <asp:ControlParameter ControlID="tbfLoc" DefaultValue="%" Name="localitate" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfStrada" DefaultValue="%" Name="strada" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfNr" DefaultValue="%" Name="nr" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfCnp" DefaultValue="%" Name="cnp" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfCodApia" DefaultValue="%" Name="codUnic" PropertyName="Text" />
                            <asp:ControlParameter ControlID="ddlFJudet" Name="judetId" PropertyName="SelectedValue" />
                            <asp:ControlParameter ControlID="tbfSiruta" Name="codSiruta" PropertyName="Text"
                                DefaultValue="%" />
                            <asp:ControlParameter ControlID="tbfCui" Name="codCuiPfa" PropertyName="Text" DefaultValue="%" />
                            <asp:ControlParameter ControlID="tbFSat" DefaultValue="%" Name="sat" PropertyName="Text" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </asp:Panel>
                <asp:Panel ID="pnButoanePrincipale" CssClass="butoane" runat="server">
                    <asp:Button CssClass="buton" ID="btModifica" Visible="false" runat="server" Text="<%$ Resources:Resursa, raModifica%>"
                        OnClick="btModifica_Click" />
                    <asp:Button CssClass="buton" ID="btSterge" Visible="false" runat="server" Text="<%$ Resources:Resursa, raSterge%>"
                        OnClientClick="return confirm (&quot;Sigur ştergeţi gospodăria aleasă ?&quot;)"
                        OnClick="btSterge_Click" />
                    <asp:Button CssClass="buton" ID="btAdauga" runat="server" Text="<%$ Resources:Resursa, raAdauga%>"
                        OnClick="btAdauga_Click" />
                    <asp:Button CssClass="buton" ID="btTiparire" runat="server" Text="<%$ Resources:Resursa, raTiparesteGospodariile%>"
                        OnClick="btTiparire_Click" Visible="false" />
                    <asp:Button CssClass="buton" ID="btMasura112" runat="server" Text="<%$ Resources:Resursa, raMasura112%>"
                        PostBackUrl="~/masura112.aspx" Visible="false" />
                    <asp:Button CssClass="buton" ID="btMasura141" runat="server" Text="<%$ Resources:Resursa, raMasura141%>"
                        PostBackUrl="~/masura141.aspx" Visible="false" />
                    <asp:Button CssClass="buton" ID="dosareVanzareButton" runat="server"  Text="oferte vânzare extravilan"
                        PostBackUrl="~/DosareVanzare.aspx" Visible="false" />
                </asp:Panel>
            </asp:Panel>
            <!-- adaugam unitate -->
            <asp:Panel ID="pnAdaugaGospodarii" DefaultButton="btSalveazaGospodarie" CssClass="panel_general"
                runat="server" Visible="false">
                <asp:Panel ID="pnlAdauga" CssClass="adauga" runat="server">
                    <asp:Panel ID="pnEroare" runat="server">
                        <h2>
                            <asp:Label ID="lbTipOperatie" runat="server" Text="ADAUGĂ GOSPODĂRIE"></asp:Label></h2>
                        <asp:Label ID="lblEroare" runat="server" CssClass="validator" Text="Eroare" Visible="false" />
                        <asp:ValidationSummary ID="valSumGospodarii" runat="server" DisplayMode="SingleParagraph"
                            Visible="true" ValidationGroup="GrupValidareGospodarii" CssClass="validator"
                            ForeColor="" />
                    </asp:Panel>
                    <asp:Panel ID="Panel3" runat="server">
                        <p>
                            <asp:Label ID="lbVolum" runat="server" Text="Volum" />
                            <asp:TextBox ID="tbVolum" runat="server" OnTextChanged="tbVolum_TextChanged" AutoPostBack="true"></asp:TextBox>
                            <asp:Label ID="lbNrPoz" runat="server" Text="Număr poziţie" />
                            <asp:TextBox ID="tbNrPoz" runat="server"></asp:TextBox>
                            <asp:Label ID="lbTip" runat="server" Text="Tipul" />
                            <asp:DropDownList ID="ddlTip" AutoPostBack="true" runat="server" Width="110px" OnSelectedIndexChanged="ddlTip_SelectedIndexChanged">
                                <asp:ListItem Value="1">Localnic</asp:ListItem>
                                <asp:ListItem Value="2">Străinaş</asp:ListItem>
                                <asp:ListItem Value="3">Firmă pe raza localităţii</asp:ListItem>
                                <asp:ListItem Value="4">Firmă străinaşă</asp:ListItem>
                            </asp:DropDownList>
                            <asp:Label ID="lbCodSiruta" runat="server" Text="Cod şiruta" />
                            <asp:TextBox ID="tbCodSiruta" runat="server"></asp:TextBox>
                        </p>
                        <p>
                            <asp:Label ID="lbStrada" runat="server" Text="Strada" />
                            <asp:TextBox ID="tbStrada" runat="server"></asp:TextBox>
                            <asp:Label ID="lbNr" runat="server" Text="Număr" />
                            <asp:TextBox ID="tbNr" runat="server" AutoPostBack="True" OnTextChanged="tbNr_TextChanged"></asp:TextBox>
                            <asp:Label ID="lbNrInt" runat="server" Text="Număr întreg" />
                            <asp:TextBox ID="tbNrInt" runat="server"></asp:TextBox>
                            <asp:Label ID="lbBl" runat="server" Text="Bloc" />
                            <asp:TextBox ID="tbBl" runat="server"></asp:TextBox>
                        </p>
                        <p>
                            <asp:Label ID="lbSc" runat="server" Text="Scara" />
                            <asp:TextBox ID="tbSc" runat="server"></asp:TextBox>
                            <asp:Label ID="lbEt" runat="server" Text="Etaj" />
                            <asp:TextBox ID="tbEt" runat="server"></asp:TextBox>
                            <asp:Label ID="lbAp" runat="server" Text="Apartament" />
                            <asp:TextBox ID="tbAp" runat="server"></asp:TextBox>
                            <asp:Label ID="lbJudet" runat="server" Text="Judeţ" />
                            <asp:TextBox ID="tbJudet" runat="server"></asp:TextBox>
                        </p>
                        <p>
                            <asp:Label ID="lbLoc" runat="server" Text="Oraş/comună" />
                            <asp:DropDownList ID="ddlLoc" AutoPostBack="true" runat="server" DataSourceID="SqlLocalitati"
                                DataTextField="localitateDenumire" DataValueField="localitateId" OnPreRender="ddlLoc_PreRender">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="SqlLocalitati" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString %>"
                                SelectCommand="SELECT localitateDenumire, localitateId FROM localitatiComuna WHERE (unitateId = @unitateId)">
                                <SelectParameters>
                                    <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                            <asp:Label ID="lblSat" runat="server" Text="Sat"></asp:Label>
                            <asp:TextBox ID="tbSat" Width="100px" runat="server"></asp:TextBox>
                            <asp:Label ID="lbCodExpl" runat="server" Text="Cod exploataţie" />
                            <asp:TextBox ID="tbCodExpl" Width="100px" runat="server"></asp:TextBox>
                            <asp:Label ID="lbCodUnic" runat="server" Text="Număr unic de înregistrare (APIA): RO" />
                            <asp:TextBox ID="tbCodUnic" Width="100px" runat="server"></asp:TextBox>
                        </p>
                        <p>
                            <asp:Label ID="lblMemCui" runat="server" Text="Cod unic de înregistrare (pt. PFA, II etc.)" />
                            <asp:TextBox ID="tbMemCui" runat="server"></asp:TextBox>
                            <asp:Label ID="lblObservatii" runat="server" Text="Observaţii"></asp:Label>
                            <asp:TextBox ID="tbObservatii" Width="400px" runat="server"></asp:TextBox>
                        </p>
                        <asp:Panel ID="pnPersoanaJuridica" Visible="false" runat="server">
                            <h2>Persoană juridică</h2>
                            <p>
                                <asp:CheckBox ID="cbPersJuridica" runat="server" AutoPostBack="true" Text="Persoană juridică"
                                    OnCheckedChanged="cbPersJuridica_CheckedChanged" />
                                <asp:CheckBox ID="cbEraPersoanaJuridica" runat="server" AutoPostBack="false" Text=""
                                    Visible="true" />
                                <asp:Label ID="lbJUnitate" runat="server" Text="Denumirea unităţii" />
                                <asp:TextBox ID="tbJUnitate" runat="server"></asp:TextBox>
                                <asp:Label ID="lbJSubUnitate" runat="server" Text="Denumirea subunităţii" />
                                <asp:TextBox ID="tbJSubUnitate" runat="server"></asp:TextBox>
                            </p>
                            <p>
                                <asp:Label ID="lbJCodFiscal" runat="server" Text="Cod fiscal" />
                                <asp:TextBox ID="tbJCodFiscal" runat="server"></asp:TextBox>
                                <asp:Label ID="lbJNumeReprez" runat="server" Text="Nume reprezentant" />
                                <asp:TextBox ID="tbJNumeReprez" runat="server"></asp:TextBox>
                            </p>
                        </asp:Panel>
                        <asp:Panel ID="pnStrainas" Visible="false" runat="server">
                            <h2>Străinaş</h2>
                            <p>
                                <asp:CheckBox ID="cbStrainas" runat="server" AutoPostBack="true" Text="Străinaş"
                                    OnCheckedChanged="cbStrainas_CheckedChanged" />
                                <asp:Label ID="lbSStrada" runat="server" Text="Strada străinaş" />
                                <asp:TextBox ID="tbSStrada" runat="server"></asp:TextBox>
                                <asp:Label ID="lbSNr" runat="server" Text="Număr străinaş" />
                                <asp:TextBox ID="tbSNr" runat="server"></asp:TextBox>
                                <asp:Label ID="lbSBl" runat="server" Text="Bloc străinaş" />
                                <asp:TextBox ID="tbSBl" runat="server"></asp:TextBox>
                            </p>
                            <p>
                                <asp:Label ID="lbSSc" runat="server" Text="Scara străinaş" />
                                <asp:TextBox ID="tbSSc" runat="server"></asp:TextBox>
                                <asp:Label ID="lbSEt" runat="server" Text="Etaj străinaş" />
                                <asp:TextBox ID="tbSEt" runat="server"></asp:TextBox>
                                <asp:Label ID="lbSAp" runat="server" Text="Apartament străinaş" />
                                <asp:TextBox ID="tbSAp" runat="server"></asp:TextBox>
                            </p>
                            <p>
                                <asp:Label ID="lbSJud" runat="server" Text="Judeţ străinaş" />
                                <asp:TextBox ID="tbSJud" runat="server"></asp:TextBox>
                                <asp:Label ID="lbSLoc" runat="server" Text="Localitate străinaş" />
                                <asp:TextBox ID="tbSLoc" runat="server"></asp:TextBox>
                            </p>
                        </asp:Panel>
                        <asp:Panel ID="pnAdaugaMembru" runat="server">
                            <h2>Cap de gospodărie</h2>
                            <p>
                                <asp:Label ID="lbMemNume" runat="server" Text="Nume membru" />
                                <asp:TextBox ID="tbMemNume" runat="server"></asp:TextBox>
                                <asp:Label ID="lbMemCNP" runat="server" Text="CNP membru / Cod unic de înregistrare " />
                                <asp:TextBox ID="tbMemCNP" runat="server" AutoPostBack="True" OnTextChanged="tbMemCNP_TextChanged"></asp:TextBox>
                                <asp:Label ID="lbMemSex" runat="server" Text="Sex membru" />
                                <asp:DropDownList ID="ddlMemSex" runat="server">
                                    <asp:ListItem Text="M" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="F" Value="2"></asp:ListItem>
                                </asp:DropDownList>
                            </p>
                            <p>
                                <asp:Label ID="lbMemDataN" runat="server" Text="Data naşterii membru" />
                                <asp:TextBox ID="tbMemDataN" runat="server"></asp:TextBox>
                                <asp:Label ID="lbMemMentiuni" runat="server" Text="Menţiuni membru" />
                                <asp:TextBox ID="tbMemMentiuni" runat="server"></asp:TextBox>
                            </p>
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel ID="PanelValidatoareAscuns" CssClass="ascunsa" runat="server">
                        <!-- //////////// VALIDATOARE  \\\\\\\\\\\\\\\\\\ -->
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="tbVolum"
                            ValidationGroup="GrupValidareGospodarii" ErrorMessage="Câmpul volum este obligatoriu"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="tbNrPoz"
                            ValidationGroup="GrupValidareGospodarii" ErrorMessage="Câmpul număr poziţie este obligatoriu"></asp:RequiredFieldValidator>
                        <!-- //////////// VALIDATOARE  \\\\\\\\\\\\\\\\\\ -->
                    </asp:Panel>
                    <asp:Panel ID="pnButoaneAdaugare" runat="server" CssClass="butoane">
                        <asp:Button ID="btSalveazaGospodarie" runat="server" CssClass="buton" Text="<%$ Resources:Resursa, raSalveaza%>"
                            ValidationGroup="GrupValidareGospodarii" OnClick="btSalveazaGospodarie_Click" />
                        <asp:Button ID="btAnuleazaSalvarea" runat="server" CssClass="buton" Text="<%$ Resources:Resursa, raLista%>"
                            OnClick="btAnuleazaSalvarea_Click" />
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
