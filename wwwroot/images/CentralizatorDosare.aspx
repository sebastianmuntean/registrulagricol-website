﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CentralizatorDosare.aspx.cs" Inherits="CentralizatorDosare" EnableEventValidation="false"
    UICulture="ro-Ro" Culture="ro-Ro" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Dosare vanzare teren extravilan" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="dosareUpdatePanel" runat="server">
        <ContentTemplate>

            <%-- Panel lista dosare--%>
            <asp:Panel ID="listaDosarePanel" runat="server" CssClass="panel_general" Visible="true">
                <asp:Panel ID="Panel2" CssClass="adauga" runat="server">
                    <asp:Panel ID="Panel3" runat="server">
                        <h2>Centralizator dosare vânzare</h2>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel ID="cautaDosarePanel" runat="server" CssClass="cauta">
                    <asp:Label ID="Label7" runat="server" Text="Nr. Dosar" />
                    <asp:TextBox ID="tbNrDosar" runat="server" autocomplete="off" AutoPostBack="True"
                        Width="50px" />
                    <asp:Label ID="Label8" runat="server" Text="Nr.Registru General" />
                    <asp:TextBox ID="tbNrRegGeneral" runat="server" autocomplete="off" AutoPostBack="True" Width="50px" />
                    <asp:Label ID="Label2" runat="server" Text="Vol" />
                    <asp:TextBox ID="volumTextBox" runat="server" autocomplete="off" AutoPostBack="True" Width="50px" />
                    <asp:Label ID="Label4" runat="server" Text="Poz" />
                    <asp:TextBox ID="pozitieTextBox" runat="server" autocomplete="off" AutoPostBack="True" Width="50px" />
                    <asp:Label ID="Label3" runat="server" Text="Vanzator" />
                    <asp:TextBox ID="vanzatorTextBox" runat="server" autocomplete="off" AutoPostBack="True" Width="100px" />

                         <asp:Label runat="server" Text="de la"></asp:Label>
                     <asp:TextBox ID="deLaTextBox" runat="server" Width="100px" AutoPostBack="true"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True"
                            Format="dd.MM.yyyy" PopupButtonID="deLaTextBox" TargetControlID="deLaTextBox">
                        </asp:CalendarExtender>
                         <asp:Label ID="Label5" runat="server" Text="pana la"></asp:Label>
                    <asp:TextBox ID="panaLaTextBox" runat="server" Width="100px" AutoPostBack="true"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True"
                            Format="dd.MM.yyyy" PopupButtonID="panaLaTextBox" TargetControlID="panaLaTextBox">
                        </asp:CalendarExtender>


                </asp:Panel>
                <asp:Panel ID="dosareGridPanel" runat="server">
                    <asp:GridView ID="dosareGridView" AllowPaging="True" OnPageIndexChanging="PageIndexChanging" DataKeyNames="idOferta" AllowSorting="True" CssClass="tabela"
                        runat="server" AutoGenerateColumns="true"
                        EnableModelValidation="True" OnRowDataBound="dosareGridView_RowDataBound" OnSelectedIndexChanged="dosareGridView_SelectedIndexChanged">
                        <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                        <FooterStyle CssClass="footer" />
                    </asp:GridView>
                </asp:Panel>
                <asp:Panel ID="pnListaButoanePrincipale" CssClass="butoane" runat="server">
                    <asp:Button CssClass="buton" ID="printCentralizatorDosareButton" runat="server" Text="tipăreste" OnClick="printCentralizatorDosareButton_OnClick" />
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

