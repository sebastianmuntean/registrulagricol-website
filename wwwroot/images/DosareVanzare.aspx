﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DosareVanzare.aspx.cs" Inherits="DosareVanzare" EnableEventValidation="false"
    UICulture="ro-Ro" Culture="ro-Ro" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Oferte vanzare teren extravilan" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="dosareUpdatePanel" runat="server">
        <ContentTemplate>

            <%-- Panel lista oferte--%>
            <asp:Panel ID="listaOfertePanel" runat="server" CssClass="panel_general" Visible="true">
                <asp:Panel ID="Panel2" CssClass="adauga" runat="server">
                    <asp:Panel ID="Panel3" runat="server">
                        <h2>LISTA OFERTE</h2>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel ID="cautaOfertePanel" runat="server" CssClass="cauta">
                    <asp:Label ID="Label7" runat="server" Text="Nr. Cerere" />
                    <asp:TextBox ID="tbNrDosar" runat="server" autocomplete="off" AutoPostBack="True"
                        Width="50px" OnTextChanged="tbNrDosar_TextChanged" />
                    <asp:Label ID="Label8" runat="server" Text="Nr.Registru General" />
                    <asp:TextBox ID="tbNrRegGeneral" runat="server" autocomplete="off" AutoPostBack="True" Width="50px" OnTextChanged="tbNrRegGeneral_TextChanged" />
                    <asp:Label ID="Label3" runat="server" Text="Denumire parcela" />
                    <asp:TextBox ID="tbDenumireParcela" runat="server" autocomplete="off" AutoPostBack="True" Width="120px" OnTextChanged="tbDenumireParcela_TextChanged" />
                    <asp:Label ID="Label9" runat="server" Text="TOPO" />
                    <asp:TextBox ID="tbTopo" runat="server" autocomplete="off" AutoPostBack="True" Width="50px" OnTextChanged="tbTopo_TextChanged" />
                    <asp:Label ID="Label10" runat="server" Text="C.F." />
                    <asp:TextBox ID="tbCF" runat="server" autocomplete="off" AutoPostBack="True" Width="80px" OnTextChanged="tbCF_TextChanged" />
                    <asp:Label ID="Label11" runat="server" Text="Bl.fiz." />
                    <asp:TextBox ID="tbBloc" runat="server" autocomplete="off" AutoPostBack="True" Width="50px" OnTextChanged="tbBloc_TextChanged" />
                    <asp:Label ID="Label12" runat="server" Text="Cadas." />
                    <asp:TextBox ID="tbNumarCadastral" runat="server" autocomplete="off" AutoPostBack="True"
                        Width="50px" OnTextChanged="tbNumarCadastral_TextChanged" />
                    <asp:Label ID="Label13" runat="server" Text="Cad.proviz." />
                    <asp:TextBox ID="tbNumarCadastralProvizoriu" runat="server" AutoPostBack="True" Width="50px" OnTextChanged="tbNumarCadastralProvizoriu_TextChanged" />
                </asp:Panel>
                     <asp:Panel ID="Panel1" runat="server" CssClass="cauta">
                     <asp:Label ID="Labelw5" runat="server" Text="Categoria de folosinţă" />
                     <asp:DropDownList ID="ddlCategoria" runat="server" DataSourceID="sdsCategoria" DataTextField="denumire4"
                                DataValueField="codRand" Width="180" AutoPostBack="true" AppendDataBoundItems="True" OnSelectedIndexChanged="categoriaDropDown_SelectedIndexChanged">
                                <asp:ListItem Value="%">toate categoriile</asp:ListItem>
                            </asp:DropDownList>
                          </asp:Panel>
                <asp:Panel ID="dosareGridPanel" runat="server">
                    <asp:GridView ID="oferteGridView" AllowPaging="True" DataKeyNames="idOferta" AllowSorting="True" CssClass="tabela"
                        runat="server" AutoGenerateColumns="true"
                        EnableModelValidation="True" OnRowDataBound="oferteGridView_RowDataBound" OnSelectedIndexChanged="oferteGridView_SelectedIndexChanged">
                        <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                        <FooterStyle CssClass="footer" />
                    </asp:GridView>
                </asp:Panel>
                <asp:Panel ID="pnListaButoanePrincipale" CssClass="butoane" runat="server">
                    <asp:Button CssClass="buton" ID="adaugaDosarButton" runat="server" Text="adaugă ofertă" OnClick="adaugaDosarButton_OnClick" />
                    <asp:Button CssClass="buton" ID="modificaDosarButton" runat="server" Text="modifică ofertă"
                        Visible="false" OnClick="modificaDosarButton_Click" />
                    <asp:Button CssClass="buton" ID="stergeDosarButton" runat="server" Text="șterge ofertă"
                        ValidationGroup="GrupValidareMembri" OnClientClick="return confirm(&quot;Sunteți sigur că doriți să ștergeți oferta?&quot;)"
                        Visible="false" OnClick="stergeDosarButton_Click" />
                     <asp:Button CssClass="buton" ID="listaCereriButton" runat="server" Text="listă cereri"
                        Visible="false" OnClick="listaCereriButton_Click" />
                    <asp:Button CssClass="buton" ID="tiparesteCerereButton" runat="server" Text="tiparește cerere"
                        Visible="false" OnClick="tiparesteCerereButton_Click" />
                    <asp:Button CssClass="buton" ID="tiparesteOfertaButton" runat="server" Text="tiparește ofertă"
                        Visible="false" OnClick="tiparesteOfertaButton_Click" />
                    <asp:Button CssClass="buton" ID="tiparesteAdeverintaButton" runat="server" Text="tiparește adeverință"
                        Visible="false" OnClick="tiparesteAdeverintaButton_Click" />
                    <asp:Button CssClass="buton" ID="tiparesteAdresaButton" runat="server" Text="tiparește adresă"
                        Visible="false" OnClick="tiparesteAdresaButton_Click" />
                    <asp:Button CssClass="buton" ID="tiparesteCerereCulturaButton" runat="server" Text="tiparește cerere catre directia de cultura"
                        Visible="false" OnClick="tiparesteCerereCulturaButton_Click" />
                    
                </asp:Panel>
            </asp:Panel>

            <%--Panel adaugare oferta noua--%>
            <asp:Panel ID="adaugaDosarPanel" CssClass="panel_general" runat="server" Visible="false">
                <asp:Panel ID="adaugaDosarHeaderPanel" CssClass="adauga" runat="server">
                    <asp:Panel ID="dosarTitluPanel" runat="server">
                        <h2>ADAUGĂ/MODIFICĂ OFERTĂ NOUĂ</h2>
                    </asp:Panel>
                    <asp:Panel ID="listaParcelePanel" runat="server" CssClass="panel_general" Visible="true">
                        <asp:Panel ID="pnCautare" runat="server" CssClass="cauta">
                            <asp:Label ID="lblCDenumire" runat="server" Text="Denumire parcela" />
                            <asp:TextBox ID="tbCDenumire" runat="server" autocomplete="off" AutoPostBack="True"
                                Width="120px" OnTextChanged="tbCDenumire_TextChanged" />
                            <asp:Label ID="lblCTopo" runat="server" Text="TOPO" />
                            <asp:TextBox ID="tbCTopo" runat="server" autocomplete="off" AutoPostBack="True" Width="50px" OnTextChanged="tbCTopo_TextChanged" />
                            <asp:Label ID="lblCCF" runat="server" Text="C.F." />
                            <asp:TextBox ID="tbCCF" runat="server" autocomplete="off" AutoPostBack="True" Width="50px" OnTextChanged="tbCCF_TextChanged" />
                            <asp:Label ID="lblCBloc" runat="server" Text="Bl.fiz." />
                            <asp:TextBox ID="tbCBloc" runat="server" autocomplete="off" AutoPostBack="True" Width="50px" OnTextChanged="tbCBloc_TextChanged" />
                            <asp:Label ID="lblCNrCadastral" runat="server" Text="Cadas." />
                            <asp:TextBox ID="tbCNrCadastral" runat="server" autocomplete="off" AutoPostBack="True"
                                Width="50px" OnTextChanged="tbCNrCadastral_TextChanged" />
                            <asp:Label ID="lblCNrCadastralProvizoriu" runat="server" Text="Cad.proviz." />
                            <asp:TextBox ID="tbCNrCadastralProvizoriu" runat="server" AutoPostBack="True" Width="50px" OnTextChanged="tbCNrCadastralProvizoriu_TextChanged" />
                        </asp:Panel>
                        <asp:Panel ID="Panel12" runat="server" CssClass="cauta">
                            <asp:Label ID="Labelx" runat="server" Text="Categoria de folosinţă" />
                            <asp:DropDownList ID="ddlCCategoria" runat="server" DataSourceID="sdsCategoria" DataTextField="denumire4"
                                DataValueField="codRand" Width="180" AutoPostBack="true" AppendDataBoundItems="True" OnSelectedIndexChanged="categoriaDropDown_SelectedIndexChanged">
                                <asp:ListItem Value="%">toate categoriile</asp:ListItem>
                            </asp:DropDownList>
                            <asp:Label ID="lblNrPagina" runat="server" Text="Nr. parcele afişate pe pagina" />
                            <asp:DropDownList ID="ddlNrPagina" runat="server" Width="80" AutoPostBack="true"
                                AppendDataBoundItems="True">
                                <asp:ListItem Value="10">10</asp:ListItem>
                                <asp:ListItem Value="20">20</asp:ListItem>
                                <asp:ListItem Value="50">50</asp:ListItem>
                                <asp:ListItem Value="%">toate</asp:ListItem>
                            </asp:DropDownList>
                        </asp:Panel>
                        <asp:Panel ID="pnParcele" runat="server">
                            <asp:Label runat="server" Visible="true" ID="parceleLabel" ForeColor="Red" Font-Size="Larger"></asp:Label>
                            <asp:GridView ID="parceleGridView" AllowPaging="True" OnPageIndexChanging="PageIndexChanging" DataKeyNames="parcelaId" AllowSorting="True"
                                CssClass="tabela" runat="server" OnPreRender="parceleGridView_PreRender" OnRowDataBound="parceleGridView_RowDataBound" OnSelectedIndexChanged="parceleGridView_SelectedIndexChanged"
                                ShowFooter="True">
                                <FooterStyle CssClass="footer" />
                                <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                            </asp:GridView>
                        </asp:Panel>
                    </asp:Panel>
                    <asp:SqlDataSource ID="sdsCategoria" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString %>"
                        SelectCommand="SELECT codRand, denumire4 FROM sabloaneCapitole WHERE (an = @an) AND (capitol = '2a') AND (denumire4<>'') AND (CHARINDEX('+', formula) = 0) AND (CHARINDEX('-', formula) = 0)">
                        <SelectParameters>
                            <asp:SessionParameter Name="an" SessionField="SESan" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:Panel ID="ofertaTextBoxuriPanel" Font-Size="Medium" runat="server" CssClass="TextIndicator_TextBox1">
                        <asp:Label ID="mesajEroareOferteLabel" runat="server" BackColor="Red"></asp:Label>
                        <br>
                        <table>
                            <tr>
                        <td><asp:Label ID="nrDosarLabel" runat="server" Text="Nr. Cerere"></asp:Label></td>
                        <td><asp:TextBox ID="nrDosarTextBox" runat="server" Width="70px"></asp:TextBox></td>
                        <td><asp:CheckBox ID="nrDosarCheckBox" runat="server" AutoPostBack="true" OnCheckedChanged="nrDosarCheckBox_OnCheckedChanged" /></td>
                        <td><asp:Label ID="nrCerereCkeckBoxLabel" runat="server" Text="Preia nr. cerere din registratura"></asp:Label></td>
                                </tr>
                            <tr>
                        <td><asp:Label ID="Label1" runat="server" Text="Nr. Registru general"></asp:Label></td>
                        <td><asp:TextBox ID="nrDosarRegGeneralTextBox" runat="server" Width="70px"></asp:TextBox></td>
                        <td><asp:CheckBox ID="nrRegistruGeneralCheckBox" runat="server"  AutoPostBack="true" OnCheckedChanged="nrRegistruGeneralCheckBox_OnCheckedChanged" /></td>
                        <td><asp:Label ID="nrRegistruGeneralLabel" runat="server" Text="Preia nr. registru general din registratura"></asp:Label></td>
                               </tr> 
                        </table>
                        <asp:Label ID="suprafataLabel" runat="server" Text="suprafata Ha"></asp:Label>
                        <asp:TextBox ID="suprafataHaTextBox" runat="server" Width="90px"></asp:TextBox>
                        <asp:Label ID="Label6" runat="server" Text="suprafata Ari"></asp:Label>
                        <asp:TextBox ID="suprafataAriTextBox" runat="server" Width="90px"></asp:TextBox>
                        <asp:Label ID="sumaLabel" runat="server" Text="Suma (lei)"></asp:Label>
                        <asp:TextBox ID="sumaTextBox" runat="server" Width="90px"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="a" runat="server" TargetControlID="nrDosarTextBox" ValidChars="1234567890" />
                        <ajaxToolkit:FilteredTextBoxExtender ID="b" runat="server" TargetControlID="nrDosarRegGeneralTextBox" ValidChars="1234567890" />
                        <ajaxToolkit:FilteredTextBoxExtender ID="c" runat="server" TargetControlID="suprafataHaTextBox" ValidChars="1234567890,." />
                        <ajaxToolkit:FilteredTextBoxExtender ID="d" runat="server" TargetControlID="suprafataAriTextBox" ValidChars="1234567890,." />
                        <ajaxToolkit:FilteredTextBoxExtender ID="e" runat="server" TargetControlID="sumaTextBox" ValidChars="1234567890,." />
                        <ajaxToolkit:FilteredTextBoxExtender ID="f" runat="server" TargetControlID="nrBuletinTextBox" ValidChars="1234567890" />
                        <asp:Label ID="lblDataNasterii" runat="server" Text=" Data înregistrării " />
                        <asp:TextBox ID="dataInregistrariiTextBox" runat="server" Width="80" Format="dd.MM.yyyy" onchange="this.value=ValidareData(this.value, 'dd.MM.yyyy')"></asp:TextBox>
                        <asp:CalendarExtender ID="dataIntregistrariiTextBox_CalendarExtender" runat="server" Enabled="True"
                            Format="dd.MM.yyyy" PopupButtonID="dataIntregistrariiTextBox" TargetControlID="dataInregistrariiTextBox">
                        </asp:CalendarExtender>
                    </asp:Panel>
                    <asp:Panel ID="ofertaDetaliiTextBoxuriPanel" Font-Size="Medium" runat="server" CssClass="TextIndicator_TextBox1">
                        <br>
                        <br>
                        <asp:Label ID="l" runat="server" Text="Act identitate vânzător" ForeColor="Black" Font-Size="Large"></asp:Label>
                        <br>
                        <asp:Label ID="Label4" Text="CI/BI" runat="server"></asp:Label>&nbsp<asp:TextBox ID="buletinTextBox" runat="server" Width="50px"></asp:TextBox>
                        <asp:Label ID="Label5" Text="seria" runat="server"></asp:Label><asp:TextBox ID="serieBuletinTextBox" runat="server" Width="50px"></asp:TextBox>
                        <asp:Label ID="Label14" Text="nr." runat="server"></asp:Label><asp:TextBox ID="nrBuletinTextBox" runat="server" Width="70px"></asp:TextBox>
                        <asp:Label ID="Label15" Text="eliberat de" runat="server"></asp:Label><asp:TextBox ID="eliberatDeTextBox" runat="server" Width="240px"></asp:TextBox>
                        <asp:Label ID="Label29" Text="la data de" runat="server"></asp:Label>&nbsp<asp:TextBox ID="dataEliberariiBuletinTextBox" runat="server" Width="80" Format="dd.MM.yyyy" onchange="this.value=ValidareData(this.value, 'dd.MM.yyyy')"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True"
                            Format="dd.MM.yyyy" PopupButtonID="dataEliberariiBuletinTextBox" TargetControlID="dataEliberariiBuletinTextBox">
                        </asp:CalendarExtender>
                        <br>
                        <br>
                        <asp:Label ID="Label42" runat="server" Text="Locul si data nasterii vânzător" ForeColor="Black" Font-Size="Large"></asp:Label>
                        <br>
                        <asp:Label ID="Label16" Text="Data nasterii" runat="server"></asp:Label>&nbsp<asp:TextBox ID="dataNasteriiTextBox" runat="server" Width="80" Format="dd.MM.yyyy" onchange="this.value=ValidareData(this.value, 'dd.MM.yyyy')"></asp:TextBox>
                        <asp:CalendarExtender ID="dataNasterii_CalendarExtender" runat="server" Enabled="True"
                            Format="dd.MM.yyyy" PopupButtonID="dataNasteriiTextBox" TargetControlID="dataNasteriiTextBox">
                        </asp:CalendarExtender>
                        <asp:Label ID="Label17" Text="localitatea nasterii" runat="server"></asp:Label><asp:TextBox ID="localitateaNasteriiTexBox" runat="server" Width="170px"></asp:TextBox>
                        <asp:Label ID="Label18" Text="judetul nasterii" runat="server"></asp:Label><asp:TextBox ID="judetulNasteriiTextBox" runat="server" Width="150px"></asp:TextBox>
                        <br>
                        <br>
                        <asp:Label ID="Label43" runat="server" Text="Detalii vânzător" ForeColor="Black" Font-Size="Large"></asp:Label>
                        <br>
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="Label19" Text="cod postal" runat="server"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="codPostalTextBox" runat="server" Width="90px"></asp:TextBox></td>
                                <td>
                                    <asp:Label ID="Label20" Text="tara" runat="server"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="taraTextBox" runat="server" Width="170px"></asp:TextBox></td>
                                <td>
                                    <asp:Label ID="Label21" Text="telefon" runat="server"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="telefonTextBox" runat="server" Width="120px"></asp:TextBox></td>
                                <td>
                                    <asp:Label ID="Label22" Text="fax" runat="server"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="faxTextBox" runat="server" Width="120px"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label23" Text="email" runat="server"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="emailTextBox" runat="server" Width="170px"></asp:TextBox></td>
                                <td>
                                    <asp:Label ID="Label26" Text="site" runat="server"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="siteTextBox" runat="server" Width="170px"></asp:TextBox></td>
                                <td>
                                    <asp:Label ID="Label24" Text="cetatenia" runat="server"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="cetateniaTextBox" runat="server" Width="120px"></asp:TextBox></td>
                                <td>
                                    <asp:Label ID="Label25" Text="starea civila" runat="server"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="stareaCivilaTextBox" runat="server" Width="120px"></asp:TextBox></td>
                            </tr>
                        </table>
                        <br>
                        <br>
                        <%--<asp:Label ID="Label45" runat="server" Text="Se completeaza doar pentru persoanele fizice" ForeColor="Black" Font-Size="Large"></asp:Label>--%>
                        <br>
                        <asp:Label ID="Label30" Text="Prin(nume si prenume)" runat="server"></asp:Label><asp:TextBox ID="prinTextBox" runat="server" Width="170px"></asp:TextBox>
                        <asp:Label ID="Label31" Text="CNP/CIF" runat="server"></asp:Label><asp:TextBox ID="prinCNPCIFTextBox" runat="server" Width="150px"></asp:TextBox>
                        <asp:Label ID="Label27" Text="in calitate de" runat="server"></asp:Label><asp:TextBox ID="inCalitateDeTextBox" runat="server" Width="170px"></asp:TextBox>
                        <asp:Label ID="Label28" Text="conform" runat="server"></asp:Label><asp:TextBox ID="conformTextBox" runat="server" Width="200px"></asp:TextBox>
                        <br>
                        <br>
                        <asp:Label ID="Label46" runat="server" Text="Se completeaza doar pentru persoanele juridice" ForeColor="Black" Font-Size="Large"></asp:Label>
                        <br>
                        <asp:Label ID="Label32" Text="pentru" runat="server"></asp:Label><asp:TextBox ID="pentruTextBox" runat="server" Width="150px"></asp:TextBox>
                        <asp:Label ID="Label33" Text="avand numar de ordine in" runat="server"></asp:Label><asp:TextBox ID="nrOrdineTextBox" runat="server" Width="70px"></asp:TextBox>
                        <asp:Label ID="Label34" Text="CIF/CUI" runat="server"></asp:Label><asp:TextBox ID="pentruCNPCIFTextBox" runat="server" Width="150px"></asp:TextBox>
                        <br>
                        <asp:Label runat="server" ForeColor="Black" Text="avand in vedere dispozitiile Legii nr. 17/2014 privind unele masuri de reglementare a vanzarii-cumpararii <br>
terenurilor agricole situate in extravilan si de modificare a Legii nr. 268/2001 privind privatizarea societatilor<br>
comerciale ce detin in administrare terenuri proprietate publica si privata a statului cu destinatie agricola si<br>
infiintarea Agentiei Domeniilor Statului, cu modificarile ulterioare, solicit prin prezenta cerere afisarea ofertei de<br>
vanzare anexata, in termenul prevazut de Legea nr. 17/2014, cu modificarile ulterioare."></asp:Label>
                        <br>
                        <asp:Label ID="Label44" runat="server" Text="Am cunostinta despre existenta urmatorilor preemptori pentru exercitarea dreptului de preemptiune asupra ofertei mele de vanzare:" ForeColor="Black" Font-Size="Large"></asp:Label>
                        <br>
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="Label35" Text="coproprietari" runat="server"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="coproprietariTextBox" runat="server" Width="500px" Height="50px" TextMode="MultiLine"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label36" Text="arendasi" runat="server"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="arendasiTextBox" runat="server" Width="500px" Height="50px" TextMode="MultiLine"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label37" Text="proprietari vecini" runat="server"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="veciniTextBox" runat="server" Width="500px" Height="50px" TextMode="MultiLine"></asp:TextBox></td>
                                </table>
                        <br>
                        <asp:Label runat="server" Text="In sustinerea cererii, depun urmatoarele acte doveditoare:" ForeColor="Black" Font-Size="Large"></asp:Label>
                        <br>
                        <asp:Label runat="server" Text="1."></asp:Label><asp:TextBox ID="act1TextBox" runat="server" Width="250px"></asp:TextBox>
                        <br>
                        <asp:Label runat="server" Text="2."></asp:Label><asp:TextBox ID="act2TextBox" runat="server" Width="250px"></asp:TextBox>
                        <br>
                        <asp:Label runat="server" Text="3."></asp:Label><asp:TextBox ID="act3TextBox" runat="server" Width="250px"></asp:TextBox>
                        <br>
                        <asp:Label runat="server" Text="4."></asp:Label><asp:TextBox ID="act4TextBox" runat="server" Width="250px"></asp:TextBox>
                        <br>
                        <asp:Label runat="server" Text="5."></asp:Label><asp:TextBox ID="act5TextBox" runat="server" Width="250px"></asp:TextBox>
                        <br>
                        <br>
                        <asp:Label ID="Label38" Text="terenul este inscris in cartea funciara cu numarul" runat="server"></asp:Label>&nbsp<asp:TextBox ID="nrCarteFunciaraTextBox" runat="server" Width="70px"></asp:TextBox>
                        <asp:Label ID="Label39" Text="care face obiectul de vanzare apartine" runat="server"></asp:Label>&nbsp<asp:TextBox ID="apartineTextBox" runat="server" Width="170px"></asp:TextBox>
                        <br>
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="Label40" Text="condtitii de vanzare" runat="server"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="conditiiVanzareTextBox" TextMode="MultiLine" runat="server" Width="700px" Height="60"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label41" Text="observatii" runat="server"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="observatiiTextBox" TextMode="MultiLine" runat="server" Width="700px" Height="60"></asp:TextBox></td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="panelButoane" CssClass="butoane" runat="server">
                        <asp:Button CssClass="buton" ID="salveazaDosarButton" runat="server" Text="salveaza oferta" Visible="false" OnClick="salveazaDosarButton_Click" />
                        <asp:Button CssClass="buton" ID="listaDosareButton" runat="server" Text="listă oferte" OnClick="listaDosareButton_Click" />
                        <asp:Button CssClass="buton" ID="listaParceleButton" runat="server" Text="lista parcele" Visible="false" OnClick="listaParceleButton_Click" />
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>

           <%-- panel lista ofertanti--%>
              <asp:Panel ID="listaCereriPanel" runat="server" CssClass="panel_general" Visible="false">
                <asp:Panel ID="Panel5" CssClass="adauga" runat="server">
                    <asp:Panel ID="Panel6" runat="server">
                        <h2>LISTA CERERI</h2>
                    </asp:Panel>
                </asp:Panel>
                   <asp:Panel ID="listaCereriGridViewPanel" runat="server">
                        <asp:Panel ID="Panel9" runat="server" CssClass="cauta">
                            <asp:Label ID="Label53" runat="server" Text="Ofertant" />
                            <asp:TextBox ID="ofertantTextBox" runat="server" autocomplete="off" AutoPostBack="True" OnTextChanged="ofertantTextBox_TextChanged"></asp:TextBox>
                            </asp:Panel>
                    <asp:GridView ID="listaCereriGridView" AllowPaging="True" DataKeyNames="idCerere" AllowSorting="True" CssClass="tabela"
                        runat="server" AutoGenerateColumns="true"
                        EnableModelValidation="True" OnRowDataBound="listaCereriGridView_RowDataBound" OnSelectedIndexChanged="listaCereriGridView_SelectedIndexChanged">
                        <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                        <FooterStyle CssClass="footer" />
                    </asp:GridView>
                </asp:Panel>
                   <asp:Panel ID="butoanePanel" CssClass="butoane" runat="server">
                    <asp:Button CssClass="buton" ID="adaugaCerereButton" runat="server" Text="adaugă cerere" OnClick="adaugaCerereButton_Click" />
                       <asp:Button CssClass="buton" ID="modificaCerereButton" runat="server" Text="modifică cerere" OnClick="modificaCerereButton_Click" Visible ="false" />
                       <asp:Button CssClass="buton" ID="stergeCerereButton" runat="server" Text="sterge cerere" OnClick="stergeCerereButton_Click" Visible="false" ValidationGroup="GrupValidareMembri" OnClientClick="return confirm(&quot;Sunteți sigur că doriți să ștergeți cererea?&quot;)"/>
                       <asp:Button CssClass="buton" ID="listaOferteButton" runat="server" Text="lista oferte" OnClick="listaOferteButton_Click" />
                       <asp:Button CssClass="buton" ID="tiparesteAcceptareButton" runat="server" Text="tiparește comunicare de acceptare"
                        Visible="false" OnClick="tiparesteAcceptareButton_Click" />
                       </asp:Panel>
                  </asp:Panel>

            <%--panel adauga cerere--%>
            <asp:Panel ID="adaugaCererePanel" CssClass="panel_general" runat="server" Visible="false">
                <asp:Panel ID="Panel7" CssClass="adauga" runat="server">
                    <asp:Panel ID="Panel8" runat="server">
                        <h2>ADAUGĂ/MODIFICĂ CERERE NOUĂ</h2>
                    </asp:Panel>
                    <asp:Panel ID="cerereTextBoxuriPanel" Font-Size="Medium" runat="server" CssClass="TextIndicator_TextBox1">
                        <asp:Label ID="mesajEroareCereriLabel" runat="server" BackColor="Red"></asp:Label>
                        <br>
                        <table>
                            <tr>
                        <td><asp:Label runat="server" Text="Nume si prenume ofertant"></asp:Label></td>
                       <td> <asp:TextBox ID="numeOfertantTextBox" runat="server" Width="250px"></asp:TextBox></td>
                        </tr>
                            <tr>
                        <td><asp:Label ID="Label45" runat="server" Text="suma oferita (lei)"></asp:Label></td>
                        <td><asp:TextBox ID="sumaOferitaTextBox" runat="server" Width="90px"></asp:TextBox></td>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="sumaOferitaTextBox" ValidChars=",.1234567890" />
                        </tr>
                            <tr>
                        <td><asp:Label runat="server" Text="data inregistrarii"></asp:Label></td>
                        <td><asp:TextBox ID="dataInregistrariiOfertantTextBox" runat="server" Width="90px"></asp:TextBox></td>
                                </tr>
                        <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True"
                            Format="dd.MM.yyyy" PopupButtonID="dataIntregistrariiTextBox" TargetControlID="dataInregistrariiOfertantTextBox">
                        </asp:CalendarExtender>
                        </table>

                           <br>
                        <asp:Label ID="Label47" runat="server" Text="In sustinerea comunicarii de acceptare si a calitatii de preemptor, depun urmatoarele acte doveditoare:" ForeColor="Black" Font-Size="Large"></asp:Label>
                        <br>
                        <asp:Label ID="Label48" runat="server" Text="1."></asp:Label><asp:TextBox ID="cerereAct1TextBox" runat="server" Width="250px"></asp:TextBox>
                        <br>
                        <asp:Label ID="Label49" runat="server" Text="2."></asp:Label><asp:TextBox ID="cerereAct2TextBox" runat="server" Width="250px"></asp:TextBox>
                        <br>
                        <asp:Label ID="Label50" runat="server" Text="3."></asp:Label><asp:TextBox ID="cerereAct3TextBox" runat="server" Width="250px"></asp:TextBox>
                        <br>
                        <asp:Label ID="Label51" runat="server" Text="4."></asp:Label><asp:TextBox ID="cerereAct4TextBox" runat="server" Width="250px"></asp:TextBox>
                        <br>
                        <asp:Label ID="Label52" runat="server" Text="5."></asp:Label><asp:TextBox ID="cerereAct5TextBox" runat="server" Width="250px"></asp:TextBox>
                        <br>

                        </asp:Panel>
                    <asp:Panel ID="panel4" CssClass="butoane" runat="server">
                        <asp:Button CssClass="buton" ID="salveazaCerereButton" runat="server" Text="salveaza cererea" OnClick="salveazaCerereButton_Click" />
                        <asp:Button CssClass="buton" ID="backToListaCereriButton" runat="server" Text="lista cereri" OnClick="backToListaCereriButton_Click" />
                        <asp:Button CssClass="buton" ID="backToListaOferteButton" runat="server" Text="lista oferte" OnClick="backToListaOferteButton_Click" />
                    </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

