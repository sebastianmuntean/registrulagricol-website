﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="printCentralizatorDosare.aspx.cs" Inherits="printCentralizatorDosare" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt"
        Height="400px" Width="400px">
        <LocalReport ReportPath="rapoarte\raportCentralizatorDosare.rdlc">
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="rapoarteDosareVanzareDataSet_dtCentralizatorDosare" />
            </DataSources>
        </LocalReport>
    </rsweb:ReportViewer>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetCentralizatorDosare"
        TypeName="rapoarteDosareVanzareDataSetTableAdapters.dtCentralizatorDosareTableAdapter">
        <SelectParameters>
            <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" Type="Int32" />
             <asp:QueryStringParameter DefaultValue="%" Name="nrDosar" QueryStringField="nrDosar" 
                Type="String" />
            <asp:QueryStringParameter DefaultValue="%" Name="nrRegistruGeneral" QueryStringField="nrRegistruGeneral" 
                Type="String" />
            <asp:QueryStringParameter DefaultValue="%" Name="membruNume" QueryStringField="membruNume" 
                Type="String" />
            <asp:QueryStringParameter DefaultValue="%" Name="volumInt" QueryStringField="volumInt" 
                Type="String" />
            <asp:QueryStringParameter DefaultValue="%" Name="nrPozitieInt" QueryStringField="nrPozitieInt" 
                Type="String" />
            <asp:QueryStringParameter Name="deLa" QueryStringField="deLa" 
                Type="DateTime" />
            <asp:QueryStringParameter Name="panaLa" QueryStringField="panaLa" 
                Type="DateTime" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>

