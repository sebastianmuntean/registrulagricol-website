﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DosareVanzare : System.Web.UI.Page
{
    SqlCommand command = new SqlCommand();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "Cap.2b", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
            RefreshListaOferteGridView();
        }
        
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["SESgospodarieId"] == null)
        {
            HttpCookie vCookie = Request.Cookies["COOKTNT"];
            vCookie = CriptareCookie.DecodeCookie(vCookie);
            if (vCookie != null)
            {
                if (vCookie["COOKgospodarieId"] != null)
                {
                    Session["SESgospodarieId"] = vCookie["COOKgospodarieId"].ToString();
                }
                else if (Request.Url.AbsolutePath.IndexOf("Gospodarii.aspx") == -1 && Session["SESgospodarieId"] == null)
                    Response.Redirect("~/Gospodarii.aspx");
            }
        }
    }

    protected void oferteGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(oferteGridView, e, this);
        e.Row.Cells[0].Visible = false;
    }

    protected void oferteGridView_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetVisibilityOnOferteGridviewSelectedIndexChanged();
        Session["SESidOferta"] = oferteGridView.SelectedValue;
    }

    protected void parceleGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(parceleGridView, e, this);
        e.Row.Cells[0].Visible = false;
    }

    protected void listaCereriGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(listaCereriGridView, e, this);
        e.Row.Cells[0].Visible = false;
    }

    protected void listaCereriGridView_SelectedIndexChanged(object sender, EventArgs e)
    {
        stergeCerereButton.Visible = true;
        tiparesteAcceptareButton.Visible = true;
        modificaCerereButton.Visible = true;
        Session["SESidCerere"] = listaCereriGridView.SelectedValue;
    }

    protected void parceleGridView_SelectedIndexChanged(object sender, EventArgs e)
    {
        salveazaDosarButton.Visible = true;
        ofertaTextBoxuriPanel.Visible = true;      
        if (ViewState["oferta"].ToString() == "adauga")
        {
            ofertaDetaliiTextBoxuriPanel.Visible = true;
            listaParcelePanel.Visible = false;
            listaParceleButton.Visible = true;
            DisablePJTextBoxes();
        }
        else
        {
            listaParceleButton.Visible = false;
        }
        FillSuprafeteTextBox();
    }

    protected void parceleGridView_PreRender(object sender, EventArgs e)
    {
        string vInterogareTotal = "";
        if (ddlCCategoria.SelectedValue == "%" || ddlCCategoria.SelectedValue == "")
        {
            vInterogareTotal = "SELECT count(parcelaId) as x, SUM(CONVERT(decimal(18,4),parcelaSuprafataIntravilanHa)*100) + SUM(CONVERT(decimal(18,4),parcelaSuprafataIntravilanAri)) as intravilan, SUM(CONVERT(decimal(18,4),parcelaSuprafataExtravilanHa)*100) + SUM(CONVERT(decimal(18,4),parcelaSuprafataExtravilanAri)) as extravilan FROM parcele WHERE gospodarieId='" + Session["SESgospodarieId"].ToString() + "' and an='" + Session["SESan"].ToString() + "'";
        }
        else
        {
            vInterogareTotal = "SELECT  count(parcelaId) as x, SUM(CONVERT(decimal(18,4),parcelaSuprafataIntravilanHa)*100) + SUM(CONVERT(decimal(18,4),parcelaSuprafataIntravilanAri)) as intravilan, SUM(CONVERT(decimal(18,4),parcelaSuprafataExtravilanHa)*100) + SUM(CONVERT(decimal(18,4),parcelaSuprafataExtravilanAri)) as extravilan FROM parcele WHERE gospodarieId='" + Session["SESgospodarieId"].ToString() + "' and an='" + Session["SESan"].ToString() + "' AND parcelaCategorie = '" + ddlCCategoria.SelectedValue + "'";
        }
        // cate parcele sa afiseze pe pagina
        if (ddlNrPagina.SelectedValue == "%")
        {
            parceleGridView.AllowPaging = false;
        }
        else
        {
            parceleGridView.AllowPaging = true;
            parceleGridView.PageSize = Convert.ToInt16(ddlNrPagina.SelectedValue);
        }

        for (int i = 0; i < parceleGridView.Rows.Count; i++)
        {
            if (parceleGridView.Rows[i].RowType == DataControlRowType.DataRow)
            {
                try
                {
                    if (parceleGridView.Rows[i].Cells[19].Text == "0")
                        parceleGridView.Rows[i].Cells[19].Text = "-";
                    if (parceleGridView.Rows[i].Cells[18].Text == "0")
                        parceleGridView.Rows[i].Cells[18].Text = "-";
                }
                catch { }
            }
        }
        AfiseazaMesajDacaExistaSauNuParceleExtravilane();
    }

    protected void PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        parceleGridView.PageIndex = e.NewPageIndex;
        RefreshListaParceleGridView();
    }

    private DataTable GetListaParceleExtravilan()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune();
        DataTable table = CreateListaParceleTableStructure();
        command.Connection = connection;
        command.CommandText = "SELECT parcelaCodRand, parcele.parcelaId, COALESCE(parcele.parcelaDenumire,'') as parcelaDenumire, COALESCE(parcele.parcelaCodRand,'') as parcelaCodRand, COALESCE(parcele.parcelaSuprafataExtravilanHa,'0') as parcelaSuprafataExtravilanHa, COALESCE(parcele.parcelaSuprafataExtravilanAri,'0') as parcelaSuprafataExtravilanAri, COALESCE(parcele.parcelaNrTopo,'') as parcelaNrTopo, COALESCE(parcele.parcelaCategorie,'') as parcelaCategorie, COALESCE(parcele.parcelaCF,'') as parcelaCF, COALESCE (sabloaneCapitole.denumire4, N' --- ') AS denumire4, COALESCE(parcele.parcelaNrBloc,'') as parcelaNrBloc, COALESCE(parcele.parcelaMentiuni,'') as parcelaMentiuni, COALESCE(parcele.an,'') as an, COALESCE(parcele.parcelaLocalitate,'') as parcelaLocalitate, COALESCE(parcele.parcelaAdresa,'') as parcelaAdresa, COALESCE(parcele.parcelaNrCadastralProvizoriu,'') as parcelaNrCadastralProvizoriu, COALESCE(parcele.parcelaNrCadastral,'') as parcelaNrCadastral, COALESCE(parcele.parcelaTarla,'') as parcelaTarla, COALESCE(parcele.parcelaTitluProprietate,'') as parcelaTitluProprietate, COALESCE(parcele.parcelaClasaBonitate,'0') as parcelaClasaBonitate, COALESCE(parcele.parcelaPunctaj,'0') as parcelaPunctaj FROM parcele INNER JOIN sabloaneCapitole ON parcele.parcelaCategorie = sabloaneCapitole.codRand WHERE (sabloaneCapitole.capitol = '2a') AND (parcele.gospodarieId = " + Session["SESgospodarieId"] + ") AND (parcele.an = " + Session["SESan"] + ") AND (sabloaneCapitole.an = " + Session["SESan"] + ")  AND (parcele.parcelaCategorie LIKE  '" + ddlCCategoria.SelectedValue + "' ) AND (parcele.parcelaCF LIKE '%" + tbCCF.Text + "%') AND (parcele.parcelaDenumire LIKE '%" + tbCDenumire.Text + "%') AND (parcele.parcelaNrBloc LIKE '%" + tbCBloc.Text + "%') AND (parcele.parcelaNrTopo LIKE '%" + tbCTopo.Text + "%') AND (parcele.parcelaNrCadastral LIKE '%" + tbCNrCadastral.Text + "%') AND (parcele.parcelaNrCadastralProvizoriu LIKE '%" + tbCNrCadastralProvizoriu.Text + "%') and ((parcele.parcelaSuprafataExtravilanAri > 0) or (parcele.parcelaSuprafataExtravilanHa > 0))";
        SqlDataReader dataReader = command.ExecuteReader();
        while (dataReader.Read())
        {
            PopulateListaParceleTable(table, dataReader);
        }
        ManipuleazaBD.InchideConexiune(connection);
        return table;
    }

    private static void PopulateListaParceleTable(DataTable table, SqlDataReader dataReader)
    {
        DataRow row = table.NewRow();
        row["parcelaId"] = dataReader["parcelaId"];
        row["Denumire Parcela"] = dataReader["parcelaDenumire"];
        row["Extravilan Ha"] = dataReader["parcelaSuprafataExtravilanHa"];
        row["Extravilan Ari"] = dataReader["parcelaSuprafataExtravilanAri"];
        row["Nr TOPO"] = dataReader["parcelaNrTopo"];
        row["CF"] = dataReader["parcelaCF"];
        row["Nr. Cadastral"] = dataReader["parcelaNrCadastral"];
        row["Nr. Cadastral provizioriu"] = dataReader["parcelaNrCadastralProvizoriu"];
        row["Bloc fizic"] = dataReader["parcelaNrBloc"];
        row["Adresa"] = dataReader["parcelaAdresa"];
        row["Categorie"] = dataReader["denumire4"];
        table.Rows.Add(row);
    }

    private static DataTable CreateListaParceleTableStructure()
    {
        DataTable table = new DataTable();
        table.Columns.Add("parcelaId");
        table.Columns.Add("Denumire parcela");
        table.Columns.Add("Extravilan Ha");
        table.Columns.Add("Extravilan Ari");
        table.Columns.Add("Categorie");
        table.Columns.Add("Nr TOPO");
        table.Columns.Add("CF");
        table.Columns.Add("Nr. Cadastral");
        table.Columns.Add("Nr. Cadastral provizioriu");
        table.Columns.Add("Bloc Fizic");
        table.Columns.Add("Adresa");
        return table;
    }

    private DataTable GetListaOferte()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune();
        DataTable table = CreateListaOferteTableStructure();
        command.Connection = connection;
        command.CommandText = "select * FROM OferteVanzare JOIN parcele on OferteVanzare.idParcela = parcele.parcelaId  INNER JOIN sabloaneCapitole ON parcele.parcelaCategorie = sabloaneCapitole.codRand WHERE (sabloaneCapitole.capitol = '2a') AND (parcele.gospodarieId = " + Session["SESgospodarieId"] + ") AND (parcele.an = " + Session["SESan"] + ") AND (sabloaneCapitole.an = " + Session["SESan"] + ") AND OferteVanzare.nrDosar LIKE '%" + tbNrDosar.Text + "%' AND OferteVanzare.nrRegistruGeneral LIKE '%" + tbNrRegGeneral.Text + "%'  AND (parcele.parcelaCF LIKE '%" + tbCF.Text + "%') AND (parcele.parcelaDenumire LIKE '%" + tbDenumireParcela.Text + "%') AND (parcele.parcelaNrBloc LIKE '%" + tbBloc.Text + "%') AND (parcele.parcelaNrTopo LIKE '%" + tbTopo.Text + "%') AND (parcele.parcelaNrCadastral LIKE '%" + tbNumarCadastral.Text + "%') AND (parcele.parcelaNrCadastralProvizoriu LIKE '%" + tbNumarCadastralProvizoriu.Text + "%')  AND (parcele.parcelaCategorie LIKE  '" + ddlCategoria.SelectedValue + "' ) and OferteVanzare.an =" + Session["SESan"] + "";
        SqlDataReader dataReader = command.ExecuteReader();

        while (dataReader.Read())
        {
            PopulateListaOferteTable(table, dataReader);
        }
        ManipuleazaBD.InchideConexiune(connection);
        return table;
    }
    
    private static void PopulateListaOferteTable(DataTable table, SqlDataReader dataReader)
    {
        DataRow row = table.NewRow();
        row["idOferta"] = dataReader["idOferta"];
        row["Nr. Cerere"] = dataReader["nrDosar"];
        row["Nr. Registru general"] = dataReader["nrRegistruGeneral"];
        row["Data inregistrarii"] = Convert.ToDateTime(dataReader["dataInregistrarii"]).ToShortDateString();
        row["Denumire parcela"] = dataReader["parcelaDenumire"];
        row["Nr TOPO"] = dataReader["parcelaNrTopo"];
        row["CF"] = dataReader["parcelaCF"];
        row["Nr. Cadastral"] = dataReader["parcelaNrCadastral"];
        row["Nr. Cadastral provizioriu"] = dataReader["parcelaNrCadastralProvizoriu"];
        row["Bloc fizic"] = dataReader["parcelaNrBloc"];
        row["Adresa"] = dataReader["parcelaAdresa"];
        row["Suprafata oferita Ha"] = dataReader["suprafataOferitaHa"];
        row["Suprafata oferita Ari"] = dataReader["suprafataOferitaAri"];
        row["Valoare parcela (lei)"] = dataReader["valoareParcela"];
        row["Categorie"] = dataReader["denumire4"];
        table.Rows.Add(row);
    }

    private static DataTable CreateListaOferteTableStructure()
    {
        DataTable table = new DataTable();
        table.Columns.Add("idOferta");
        table.Columns.Add("Nr. Cerere");
        table.Columns.Add("Nr. Registru general");
        table.Columns.Add("Data inregistrarii");
        table.Columns.Add("Denumire parcela");
        table.Columns.Add("Categorie");
        table.Columns.Add("Nr TOPO");
        table.Columns.Add("CF");
        table.Columns.Add("Nr. Cadastral");
        table.Columns.Add("Nr. Cadastral provizioriu");
        table.Columns.Add("Bloc Fizic");
        table.Columns.Add("Adresa");
        table.Columns.Add("Suprafata oferita Ha");
        table.Columns.Add("Suprafata oferita Ari");
        table.Columns.Add("Valoare parcela (lei)");
        return table;
    }

    private DataTable GetListaCereri()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune();
        DataTable table = CreateListaCereriTableStructure();
        command.Connection = connection;
        command.CommandText = "Select idCerere,numeOfertant,CereriVanzare.dataInregistrarii,sumaOferita from CereriVanzare join OferteVanzare on CereriVanzare.idOferta = OferteVanzare.idOferta where(CereriVanzare.idOferta = '" + Session["SESidOferta"] + "') and numeOfertant LIKE '%"+ofertantTextBox.Text+"%'";
        SqlDataReader dataReader = command.ExecuteReader();

        while (dataReader.Read())
        {
            PopulateListaCereriTable(table, dataReader);
        }
        ManipuleazaBD.InchideConexiune(connection);
        return table;

    }

    private void PopulateListaCereriTable(DataTable table, SqlDataReader dataReader)
    {
        DataRow row = table.NewRow();
        row["idCerere"] = dataReader["idCerere"];
        row["Nume ofertant"] = dataReader["numeOfertant"];
        row["Data inregistrarii"] = Convert.ToDateTime(dataReader["dataInregistrarii"]).ToShortDateString();
        row["Suma oferita (lei)"] = dataReader["sumaOferita"];
        table.Rows.Add(row);
    }

    private DataTable CreateListaCereriTableStructure()
    {
        DataTable table = new DataTable();
        table.Columns.Add("idCerere");
        table.Columns.Add("Nume ofertant");
        table.Columns.Add("Data inregistrarii");
        table.Columns.Add("Suma oferita (lei)");
        return table;
    }

    private void SaveOferta()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune();
        command.Connection = connection;
        int idParcela = Convert.ToInt32(parceleGridView.SelectedValue);
        command.CommandText = "INSERT INTO OferteVanzare (idParcela,idGospodarie,nrDosar,nrRegistruGeneral,suprafataOferitaHa,suprafataOferitaAri,valoareParcela, dataInregistrarii,unitateId,an) VALUES (" + idParcela + "," + Session["SESgospodarieId"] + "," + nrDosarTextBox.Text + "," + nrDosarRegGeneralTextBox.Text + "," + suprafataHaTextBox.Text + "," + suprafataAriTextBox.Text + "," + sumaTextBox.Text + ",convert(datetime,'" + dataInregistrariiTextBox.Text + "',104)," + Session["SESunitateId"] + "," + Session["SESan"] + ") select scope_identity()";
        int idOferta = Convert.ToInt32(command.ExecuteScalar());
        command.CommandText = string.Empty;
        command.CommandText = "INSERT INTO OferteVanzareDetalii(idOferta,buletin,serieBuletin,nrBuletin,eliberatDe,dataEliberariiBuletin,dataNasterii,localitateaNasterii,judetulNasterii,codPostal,tara,telefon,fax,email,site,cetatenia,stareCivila,inCalitateDe,conform,coproprietari,arendasi,vecini,nrCarteFunciara,apartine,conditiiVanzare,observatii,prin,pentru,prinCNPCIF,pentruCNPCIF,nrOrdine,act1,act2,act3,act4,act5) VALUES ('" + idOferta + "','" + buletinTextBox.Text + "','" + serieBuletinTextBox.Text + "','" + nrBuletinTextBox.Text + "','" + eliberatDeTextBox.Text + "',convert(datetime,'" + dataEliberariiBuletinTextBox.Text + "',104),convert(datetime,'" + dataNasteriiTextBox.Text + "',104),'" + localitateaNasteriiTexBox.Text + "','" + judetulNasteriiTextBox.Text + "','" + codPostalTextBox.Text + "','" + taraTextBox.Text + "','" + telefonTextBox.Text + "','" + faxTextBox.Text + "','" + emailTextBox.Text + "','" + siteTextBox.Text + "','" + cetateniaTextBox.Text + "','" + stareaCivilaTextBox.Text + "','" + inCalitateDeTextBox.Text + "','" + conformTextBox.Text + "','" + coproprietariTextBox.Text + "','" + arendasiTextBox.Text + "','" + veciniTextBox.Text + "','" + nrCarteFunciaraTextBox.Text + "','" + apartineTextBox.Text + "','" + conditiiVanzareTextBox.Text + "','" + observatiiTextBox.Text + "','" + prinTextBox.Text + "','" + pentruTextBox.Text + "','" + prinCNPCIFTextBox.Text + "','" + pentruCNPCIFTextBox.Text + "','" + nrOrdineTextBox.Text + "','" + act1TextBox.Text + "','" + act2TextBox.Text + "','" + act3TextBox.Text + "','" + act4TextBox.Text + "','" + act5TextBox.Text + "')";
        command.ExecuteNonQuery();
        ManipuleazaBD.InchideConexiune(connection);
    }

    private void SaveCerere()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune();
        command.Connection = connection;
        command.CommandText = "INSERT into CereriVanzare (idOferta,numeOfertant,sumaOferita,dataInregistrarii,act1,act2,act3,act4,act5) values ('" + oferteGridView.SelectedValue + "','" + numeOfertantTextBox.Text + "','" + sumaOferitaTextBox.Text + "', convert(datetime,'" + dataInregistrariiOfertantTextBox.Text + "',104),'" + cerereAct1TextBox.Text + "','" + cerereAct2TextBox.Text + "','" + cerereAct3TextBox.Text + "','" + cerereAct4TextBox.Text + "','" + cerereAct5TextBox.Text + "')";
        command.ExecuteNonQuery();
        ManipuleazaBD.InchideConexiune(connection);

    }

    private void DeleteOferta()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune();
        command.Connection = connection;
        int idOferta = Convert.ToInt32(oferteGridView.SelectedValue);
        command.CommandText = "DELETE FROM OferteVanzare WHERE idOferta = " + idOferta + " and idGospodarie =" + Session["SESgospodarieId"] + " and unitateId = " + Session["SESunitateId"] + " and OferteVanzare.an =" + Session["SESan"] + ";Delete from OferteVanzareDetalii where idOferta = " + idOferta + ";Delete from CereriVanzare where idOferta ="+idOferta+"";
        command.ExecuteNonQuery();
        ManipuleazaBD.InchideConexiune(connection);
        RefreshListaOferteGridView();
    }

    protected void DeleteCerere()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune();
        command.Connection = connection;
        int idCerere = Convert.ToInt32(listaCereriGridView.SelectedValue);
        command.CommandText = "Delete from CereriVanzare where idCerere = " + idCerere + "";
        command.ExecuteNonQuery();
        ManipuleazaBD.InchideConexiune(connection);
        RefreshListaCereriGridView();
       
    }

    private void UpdateOferta()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune();
        command.Connection = connection;
        command.CommandText = "update OferteVanzare set idParcela = " + parceleGridView.SelectedValue + ", nrDosar = " + nrDosarTextBox.Text + ", nrRegistruGeneral = " + nrDosarRegGeneralTextBox.Text + ", suprafataOferitaHa =" + suprafataHaTextBox.Text + ",suprafataOferitaAri=" + suprafataAriTextBox.Text + ", valoareParcela = " + sumaTextBox.Text + ", dataInregistrarii = convert(datetime,'" + dataInregistrariiTextBox.Text + "',104) where idOferta = " + oferteGridView.SelectedValue + " and idGospodarie = " + Session["SESgospodarieId"] + " and unitateId = " + Session["SESunitateId"] + " and OferteVanzare.an = " + Session["SESan"] + "";
        command.ExecuteNonQuery();
        ManipuleazaBD.InchideConexiune(connection);
    }

    protected void UpdateCerere()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune();
        command.Connection = connection;
        command.CommandText = "update CereriVanzare set numeOfertant ='" + numeOfertantTextBox.Text + "', sumaOferita ='" + sumaOferitaTextBox.Text + "',dataInregistrarii=convert(datetime,'" + dataInregistrariiOfertantTextBox.Text + "',104), act1='" + cerereAct1TextBox.Text + "', act2='" + cerereAct2TextBox.Text + "', act3='" + cerereAct3TextBox.Text + "', act4='" + cerereAct4TextBox.Text + "', act5='" + cerereAct5TextBox.Text + "'  where idCerere = " + Session["SESidCerere"] + "";
        command.ExecuteNonQuery();
        ManipuleazaBD.InchideConexiune(connection);
    }



    private void FillTextBoxesModificaOferta()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune();
        command.Connection = connection;
        command.CommandText = "SELECT nrDosar, nrRegistruGeneral, suprafataOferitaHa, suprafataOferitaAri, valoareParcela, dataInregistrarii FROM OferteVanzare WHERE idOferta = " + oferteGridView.SelectedValue + " and idGospodarie = " + Session["SESgospodarieId"] + " and unitateId = " + Session["SESunitateId"] + " and OferteVanzare.an = " + Session["SESan"] + "";
        SqlDataReader dataReader = command.ExecuteReader();
        if (dataReader.Read())
        {
            nrDosarTextBox.Text = dataReader["nrDosar"].ToString();
            nrDosarRegGeneralTextBox.Text = dataReader["nrRegistruGeneral"].ToString();
            suprafataHaTextBox.Text = dataReader["suprafataOferitaHa"].ToString();
            suprafataAriTextBox.Text = dataReader["suprafataOferitaAri"].ToString();
            sumaTextBox.Text = dataReader["valoareParcela"].ToString();
            dataInregistrariiTextBox.Text = dataReader["dataInregistrarii"].ToString();
        }
        dataReader.Close();
        ManipuleazaBD.InchideConexiune(connection);
    }

    private void FillTextBoxesModificaCerere()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune();
        command.Connection = connection;
        command.CommandText = "SELECT * FROM CereriVanzare WHERE idCerere = " + listaCereriGridView.SelectedValue + "";
        SqlDataReader dataReader = command.ExecuteReader();
        if (dataReader.Read())
        {
            numeOfertantTextBox.Text = dataReader["numeOfertant"].ToString();
            sumaOferitaTextBox.Text = dataReader["sumaOferita"].ToString();
            dataInregistrariiOfertantTextBox.Text = Convert.ToDateTime(dataReader["dataInregistrarii"]).ToShortDateString();
            cerereAct1TextBox.Text = dataReader["act1"].ToString();
            cerereAct2TextBox.Text = dataReader["act2"].ToString();
            cerereAct3TextBox.Text = dataReader["act3"].ToString();
            cerereAct4TextBox.Text = dataReader["act4"].ToString();
            cerereAct5TextBox.Text = dataReader["act5"].ToString();
        }
        dataReader.Close();
        ManipuleazaBD.InchideConexiune(connection);
    }

    private void FillSuprafeteTextBox()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune();
        command.Connection = connection;
        command.CommandText = "select parcelaSuprafataExtravilanHa,parcelaSuprafataExtravilanAri FROM parcele WHERE parcelaId = " + parceleGridView.SelectedValue + " and gospodarieId = " + Session["SESgospodarieId"] + " and unitateaId = " + Session["SESunitateId"] + "";
        SqlDataReader dataReader = command.ExecuteReader();
        if (dataReader.Read())
        {
            suprafataHaTextBox.Text = dataReader["parcelaSuprafataExtravilanHa"].ToString();
            suprafataAriTextBox.Text = dataReader["parcelaSuprafataExtravilanAri"].ToString();
        }
        dataReader.Close();
        ManipuleazaBD.InchideConexiune(connection);
    }

    private bool IsPersoanaFizica()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune();
        command.Connection = connection;
        command.CommandText = "select * from OferteVanzare join gospodarii on OferteVanzare.idGospodarie = gospodarii.gospodarieId join parcele on parcele.parcelaid = OferteVanzare.idParcela where gospodarii.gospodarieId = " + Session["SESgospodarieId"] + " and unitateaId = " + Session["SESunitateId"] + " and persJuridica = 0";
        SqlDataReader dataReader = command.ExecuteReader();
        if (dataReader.Read())
        {
            return true;
        }
        dataReader.Close();
        ManipuleazaBD.InchideConexiune(connection);
        return false;
    }

    protected void nrRegistruGeneralCheckBox_OnCheckedChanged(object sender, EventArgs e)
    {
        if (nrRegistruGeneralCheckBox.Checked == true)
        {
            nrDosarRegGeneralTextBox.Enabled = false;
        }
        else
        {
            nrDosarRegGeneralTextBox.Enabled = true;
        }
    }

    protected void nrDosarCheckBox_OnCheckedChanged(object sender, EventArgs e)
    {
        if (nrDosarCheckBox.Checked == true)
        {
            nrDosarTextBox.Enabled = false;
        }
        else
        {
            nrDosarTextBox.Enabled = true;
        }
    }

    protected void ClearCerereTextBoxes()
    {
        foreach (Control c in this.ofertaDetaliiTextBoxuriPanel.Controls)
        {
            if (c is TextBox)
            {
                ((TextBox)c).Text = string.Empty;
            }
        }
    }

    protected void ClearAdaugaCerereTextBoxes()
    {

        foreach (Control c in this.cerereTextBoxuriPanel.Controls)
        {
            if (c is TextBox)
            {
                ((TextBox)c).Text = string.Empty;
            }
        }
    }

    protected void ClearTextBoxes()
    {
        foreach (Control c in this.ofertaTextBoxuriPanel.Controls)
        {
            if (c is TextBox)
            {
                ((TextBox)c).Text = string.Empty;
            }
        }
    }

    protected void DisablePJTextBoxes()
    {
        if (IsPersoanaFizica())
        {
            pentruTextBox.Enabled = false;
            pentruCNPCIFTextBox.Enabled = false;
            nrOrdineTextBox.Enabled = false;
        }
        else
        {
            pentruTextBox.Enabled = true;
            pentruCNPCIFTextBox.Enabled = true;
            nrOrdineTextBox.Enabled = true;
        }
    }

    #region butoane
    protected void adaugaDosarButton_OnClick(object sender, EventArgs e)
    {
        ViewState["oferta"] = "adauga";
        mesajEroareOferteLabel.Text = string.Empty;
        SetVisibilityOnAdaugaOfertaButton();
        RefreshListaParceleGridView();
        parceleGridView.SelectedIndex = -1;
        ClearTextBoxes();
        ClearCerereTextBoxes();
        AfiseazaMesajDacaExistaSauNuParceleExtravilane();
    }

    private void AfiseazaMesajDacaExistaSauNuParceleExtravilane()
    {
        if (parceleGridView.Rows.Count == 0)
        {
            parceleLabel.Text = "Nu aveti parcele extravilane";
        }
        else
        {
            parceleLabel.Text = "Selectati o parcela";
        }
    }

    protected void modificaDosarButton_Click(object sender, EventArgs e)
    {
        ViewState["oferta"] = "modifica";
        ddlCCategoria.SelectedValue = "%";
        mesajEroareOferteLabel.Text = string.Empty;
        SetVisibilityOnModificaOfertaButton();
        FillTextBoxesModificaOferta();
        RefreshListaParceleGridView();
    }

    protected void listaParceleButton_Click(object sender, EventArgs e)
    {
        SetVisibilityOnListaParceleButton();
        parceleGridView.SelectedIndex = -1;
        ClearCerereTextBoxes();
        ClearTextBoxes();
    }

    protected void salveazaDosarButton_Click(object sender, EventArgs e)
    {
        if (ValidateOfertaVanzareInputs() && ValidateSuprafateInputs())
        {
            if (ViewState["oferta"].ToString() == "adauga")
            {
                SaveOferta();
                SetVisibilityOnSalveazaOfertaButton();
            }
            else
                if (ViewState["oferta"].ToString() == "modifica")
                {
                    UpdateOferta();
                    SetVisibilityOnSalveazaOfertaButton();
                }
        }
        else
            return;
        adaugaDosarPanel.Visible = false;
        listaOfertePanel.Visible = true;
        RefreshListaOferteGridView();
        adaugaDosarButton.Visible = true;
        modificaDosarButton.Visible = false;
        stergeDosarButton.Visible = false;
        oferteGridView.SelectedIndex = -1;
    }

    protected void stergeDosarButton_Click(object sender, EventArgs e)
    {
        DeleteOferta();
        SetVisibilityOnStergeOfertaButton();
        oferteGridView.SelectedIndex = -1;
    }

    protected void listaCereriButton_Click(object sender, EventArgs e)
    {
        listaOfertePanel.Visible = false;
        listaCereriPanel.Visible = true;
        listaCereriGridView.SelectedIndex = -1;
        stergeCerereButton.Visible = false;
        tiparesteAcceptareButton.Visible = false;
        modificaCerereButton.Visible = false;
        RefreshListaCereriGridView();
    }

    protected void adaugaCerereButton_Click(object sender, EventArgs e)
    {
        ViewState["cerere"] = "adauga";
        listaCereriPanel.Visible = false;
        ClearAdaugaCerereTextBoxes();
        mesajEroareCereriLabel.Text = "";
        adaugaCererePanel.Visible = true;
    }

    protected void modificaCerereButton_Click(object sender, EventArgs e)
    {
        ViewState["cerere"] = "modifica";
        listaCereriPanel.Visible = false;
        adaugaCererePanel.Visible = true;
        cerereTextBoxuriPanel.Visible = true;
        mesajEroareCereriLabel.Text = "";
        FillTextBoxesModificaCerere();
    }


    protected void stergeCerereButton_Click(object sender, EventArgs e)
    {
        DeleteCerere();
        SetVisibilityOnStergeCerereButton();
        listaCereriGridView.SelectedIndex = -1;
    }

    //protected void salveazaModificareDosarButton_OnClick(object sender, EventArgs e)
    //{
    //    UpdateOferta();
    //    RefreshListaOferteGridView();
    //    SetVisibilityOnSalveazaModificareOfertaButton();
    //    oferteGridView.SelectedIndex = -1;

    //}

    protected void listaDosareButton_Click(object sender, EventArgs e)
    {
        SetVisibilityOnListaOferteButton();
        listaCereriButton.Visible = false;
        oferteGridView.SelectedIndex = -1;
    }

    protected void salveazaCerereButton_Click(object sender, EventArgs e)
    {
        if (ValidateSumaOferita() && ValidateCerereInputs())
        {
            if (ViewState["cerere"].ToString() == "adauga")
            {
                SaveCerere();
                RefreshListaCereriGridView();
                SetVisibilityOnSalveazaCerereButton();
                listaCereriGridView.SelectedIndex = -1;
            }
            else
            {
                if (ViewState["cerere"].ToString() == "modifica")
                {
                    UpdateCerere();
                    RefreshListaCereriGridView();
                    SetVisibilityOnSalveazaCerereButton();
                    listaCereriGridView.SelectedIndex = -1;
                }
            }
            
        }
        else
            return;
    }


    protected void listaOferteButton_Click(object sender, EventArgs e)
    {
        listaCereriPanel.Visible = false;
        listaOfertePanel.Visible = true;
        SetVisibilityOnStergeOfertaButton();
        oferteGridView.SelectedIndex = -1;
    }

    protected void backToListaOferteButton_Click(object sender, EventArgs e)
    {
        listaCereriPanel.Visible = false;
        adaugaCererePanel.Visible = false;
        SetVisibilityOnStergeOfertaButton();
        oferteGridView.SelectedIndex = -1;
        listaOfertePanel.Visible = true;
    }
    protected void backToListaCereriButton_Click(object sender, EventArgs e)
    {
        adaugaCererePanel.Visible = false;
        listaCereriPanel.Visible = true;
        stergeCerereButton.Visible = false;
        tiparesteAcceptareButton.Visible = false;
        modificaCerereButton.Visible = false;
        listaCereriGridView.SelectedIndex = -1;
    }
    #endregion
    #region validariImputuri
    private bool ValidateOfertaVanzareInputs()
    {
        if (nrDosarTextBox.Text == string.Empty || nrDosarRegGeneralTextBox.Text == string.Empty || suprafataHaTextBox.Text == string.Empty || sumaTextBox.Text == string.Empty || suprafataAriTextBox.Text == string.Empty || dataInregistrariiTextBox.Text == string.Empty)
        {
            mesajEroareOferteLabel.Text = "Introduceti numarul dosarului, numarul registrului general, suma si data intregistrarii";
            Page.SetFocus(ofertaTextBoxuriPanel);
            return false;
        }
        return true;
    }

    private bool ValidateSumaOferita()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune();
        command.Connection = connection;
        command.CommandText = "select valoareParcela from OferteVanzare where idOferta = '" + oferteGridView.SelectedValue + "'";
        SqlDataReader dataReader = command.ExecuteReader();
        if (dataReader.Read())
        {
            if (Convert.ToDecimal(sumaOferitaTextBox.Text) < Convert.ToDecimal(dataReader["valoareParcela"]))
            {
                mesajEroareCereriLabel.Text = "suma oferita nu trebuie sa fie mai mica decat valoarea parcelei";
                return false;
            }
        }
        ManipuleazaBD.InchideConexiune(connection);
        return true;
    }

    private bool ValidateSuprafateInputs()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune();
        command.Connection = connection;
        command.CommandText = "select parcelaSuprafataExtravilanHa,parcelaSuprafataExtravilanAri from parcele where parcelaId = " + parceleGridView.SelectedValue + " and gospodarieId = " + Session["SESgospodarieId"] + " and unitateaId = " + Session["SESunitateId"] + "";
        SqlDataReader dataReader = command.ExecuteReader();
        if (dataReader.Read())
        {
            if (Convert.ToDecimal(suprafataHaTextBox.Text) > Convert.ToDecimal(dataReader["parcelaSuprafataExtravilanHa"]) || Convert.ToDecimal(suprafataAriTextBox.Text) > Convert.ToDecimal(dataReader["parcelaSuprafataExtravilanAri"]))
            {
                mesajEroareOferteLabel.Text = "Suprafata oferita spre vanzare nu trebuie sa depaseasca suprafata totala a parcelei";
                Page.SetFocus(ofertaTextBoxuriPanel);
                return false;
            }
        }
        dataReader.Close();
        ManipuleazaBD.InchideConexiune(connection);
        return true;
    }

    private bool ValidateCerereInputs()
    {
        if (numeOfertantTextBox.Text == string.Empty || dataInregistrariiOfertantTextBox.Text == string.Empty || sumaOferitaTextBox.Text == string.Empty)
        {
            mesajEroareCereriLabel.Text = "Completati numele, suma si data";
            return false;
        }
        return true;
    }
    #endregion
    #region tipariri
    protected void tiparesteCerereButton_Click(object sender, EventArgs e)
    {
        if (IsPersoanaFizica())
        {
            ResponseHelper.Redirect("~/printCerereVanzarePF.aspx", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "cerereVanzarePF", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
        }
        else
        {
            ResponseHelper.Redirect("~/printCerereVanzarePJ.aspx", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "cerereVanzarePJ", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
        }
    }

    protected void tiparesteOfertaButton_Click(object sender, EventArgs e)
    {
        if (IsPersoanaFizica())
        {
            ResponseHelper.Redirect("~/printOfertaVanzarePF.aspx", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "ofertaVanzarePF", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
        }
        else
        {
            ResponseHelper.Redirect("~/printOfertaVanzarePJ.aspx", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "ofertaVanzarePJ", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
        }
    }

    protected void tiparesteAdeverintaButton_Click(object sender, EventArgs e)
    {
        ResponseHelper.Redirect("~/printAdeverintaVanzare.aspx", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
        ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "adeverinta", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
    }

    protected void tiparesteAdresaButton_Click(object sender, EventArgs e)
    {
        ResponseHelper.Redirect("~/printAdresaVanzare.aspx", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
        ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "adresa", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
    }

    protected void tiparesteCerereCulturaButton_Click(object sender, EventArgs e)
    {
        if (IsPersoanaFizica())
        {
            ResponseHelper.Redirect("~/printCerereCulturaPF.aspx", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "cerereCulturaPF", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
        }
        else
        {
            ResponseHelper.Redirect("~/printCerereCulturaPJ.aspx", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "cerereCulturaPJ", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
        }
    }

    protected void tiparesteAcceptareButton_Click(object sender, EventArgs e)
    {
        if (IsPersoanaFizica())
        {
            ResponseHelper.Redirect("~/printComunicareDeAcceptarePF.aspx", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "comunicareAcceptarePF", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
        }
        else
        {
            ResponseHelper.Redirect("~/printComunicareDeAcceptarePJ.aspx", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "comunicareAcceptarePJ", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
        }
    }
    #endregion
    #region filtrari

    //filtrare lista parcele
    protected void categoriaDropDown_SelectedIndexChanged(object sender, EventArgs e)
    {
        RefreshListaParceleGridView();
        RefreshListaOferteGridView();
        if (parceleGridView.Rows.Count == 0)
        {
            ofertaTextBoxuriPanel.Visible = false;
        }
        if (oferteGridView.Rows.Count == 0)
        {
            foreach (Control c in this.pnListaButoanePrincipale.Controls)
            {
                if (c is Button)
                {
                    if (c != adaugaDosarButton)
                        ((Button)c).Visible = false;
                }
            }
        }
        else
        {
            oferteGridView.SelectedIndex = -1;
        }
    }

    protected void tbCBloc_TextChanged(object sender, EventArgs e)
    {
        RefreshListaParceleGridView();
    }

    protected void tbCNrCadastral_TextChanged(object sender, EventArgs e)
    {
        RefreshListaParceleGridView();
    }
    protected void tbCNrCadastralProvizoriu_TextChanged(object sender, EventArgs e)
    {
        RefreshListaParceleGridView();
    }
    protected void tbCTopo_TextChanged(object sender, EventArgs e)
    {
        RefreshListaParceleGridView();
    }

    protected void tbCCF_TextChanged(object sender, EventArgs e)
    {
        RefreshListaParceleGridView();
    }
    protected void tbCDenumire_TextChanged(object sender, EventArgs e)
    {
        RefreshListaParceleGridView();
    }

    //filtrare lista oferte
    protected void tbNrDosar_TextChanged(object sender, EventArgs e)
    {
        RefreshListaOferteGridView();
    }
    protected void tbNrRegGeneral_TextChanged(object sender, EventArgs e)
    {
        RefreshListaOferteGridView();
    }
    protected void tbTopo_TextChanged(object sender, EventArgs e)
    {
        RefreshListaOferteGridView();
    }
    protected void tbDenumireParcela_TextChanged(object sender, EventArgs e)
    {
        RefreshListaOferteGridView();
    }
    protected void tbCF_TextChanged(object sender, EventArgs e)
    {
        RefreshListaOferteGridView();
    }
    protected void tbBloc_TextChanged(object sender, EventArgs e)
    {
        RefreshListaOferteGridView();
    }
    protected void tbNumarCadastral_TextChanged(object sender, EventArgs e)
    {
        RefreshListaOferteGridView();
    }
    protected void tbNumarCadastralProvizoriu_TextChanged(object sender, EventArgs e)
    {
        RefreshListaOferteGridView();
    }

    //filtrari lista cereri
    protected void ofertantTextBox_TextChanged(object sender, EventArgs e)
    {
        RefreshListaCereriGridView();
    }
    #endregion
    #region refreshGriduri
    private void RefreshListaParceleGridView()
    {
        parceleGridView.DataSource = GetListaParceleExtravilan();
        parceleGridView.DataBind();
    }

    private void RefreshListaOferteGridView()
    {
        oferteGridView.DataSource = GetListaOferte();
        oferteGridView.DataBind();
    }

    private void RefreshListaCereriGridView()
    {
        listaCereriGridView.DataSource = GetListaCereri();
        listaCereriGridView.DataBind();
    }
    #endregion
    #region vizibilitatiButoane
    private void SetVisibilityOnAdaugaOfertaButton()
    {
        adaugaDosarPanel.Visible = true;
        listaOfertePanel.Visible = false;
        salveazaDosarButton.Visible = false;
        ofertaTextBoxuriPanel.Visible = false;
        ofertaDetaliiTextBoxuriPanel.Visible = false;
        listaParcelePanel.Visible = true;
        listaParceleButton.Visible = false;
    }
    private void SetVisibilityOnModificaOfertaButton()
    {
        adaugaDosarPanel.Visible = true;
        ofertaTextBoxuriPanel.Visible = true;
        salveazaDosarButton.Visible = true;
        listaParcelePanel.Visible = true;
        listaParceleButton.Visible = false;
        listaOfertePanel.Visible = false;
        salveazaDosarButton.Visible = false;
        ofertaDetaliiTextBoxuriPanel.Visible = false;
    }
    private void SetVisibilityOnListaParceleButton()
    {
        listaParcelePanel.Visible = true;
        ofertaDetaliiTextBoxuriPanel.Visible = false;
        ofertaTextBoxuriPanel.Visible = false;
        listaParceleButton.Visible = false;
        salveazaDosarButton.Visible = false;
    }
    private void SetVisibilityOnSalveazaOfertaButton()
    {
        tiparesteCerereButton.Visible = false;
        tiparesteOfertaButton.Visible = false;
        tiparesteAdeverintaButton.Visible = false;
        tiparesteAdresaButton.Visible = false;
        tiparesteCerereCulturaButton.Visible = false;
    }
    private void SetVisibilityOnStergeOfertaButton()
    {
        modificaDosarButton.Visible = false;
        stergeDosarButton.Visible = false;
        tiparesteCerereButton.Visible = false;
        tiparesteOfertaButton.Visible = false;
        tiparesteAdeverintaButton.Visible = false;
        tiparesteAdresaButton.Visible = false;
        tiparesteCerereCulturaButton.Visible = false;
        listaCereriButton.Visible = false;
    }
    private void SetVisibilityOnSalveazaModificareOfertaButton()
    {
        modificaDosarButton.Visible = false;
        stergeDosarButton.Visible = false;
        listaOfertePanel.Visible = true;
    }
    private void SetVisibilityOnListaOferteButton()
    {
        adaugaDosarPanel.Visible = false;
        listaOfertePanel.Visible = true;
        modificaDosarButton.Visible = false;
        stergeDosarButton.Visible = false;
        tiparesteCerereButton.Visible = false;
        tiparesteOfertaButton.Visible = false;
        tiparesteAdeverintaButton.Visible = false;
        tiparesteAdresaButton.Visible = false;
        tiparesteCerereCulturaButton.Visible = false;
        tiparesteAcceptareButton.Visible = false;
        ofertaDetaliiTextBoxuriPanel.Visible = false;
    }
    private void SetVisibilityOnOferteGridviewSelectedIndexChanged()
    {
        modificaDosarButton.Visible = true;
        stergeDosarButton.Visible = true;
        tiparesteCerereButton.Visible = true;
        tiparesteOfertaButton.Visible = true;
        tiparesteAdeverintaButton.Visible = true;
        tiparesteAdresaButton.Visible = true;
        tiparesteCerereCulturaButton.Visible = true;
        listaCereriButton.Visible = true;
    }
    private void SetVisibilityOnSalveazaCerereButton()
    {
        adaugaCererePanel.Visible = false;
        listaCereriPanel.Visible = true;
        modificaCerereButton.Visible = false;
        stergeCerereButton.Visible = false;
        tiparesteAcceptareButton.Visible = false;
    }
    private void SetVisibilityOnStergeCerereButton()
    {
        stergeCerereButton.Visible = false;
        tiparesteAcceptareButton.Visible = false;
        modificaCerereButton.Visible = false;
    }
    #endregion
}