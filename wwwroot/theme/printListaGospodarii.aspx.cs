﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;

public partial class printListaGospodarii : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "printListaGospodarii", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
        }
        if (Request.QueryString["ordonare"] == "0")
            ObjectDataSource1.SelectMethod = "GetData";
        else if (Request.QueryString["ordonare"] == "1")
            ObjectDataSource1.SelectMethod = "GetData1";
        else if (Request.QueryString["ordonare"] == "2")
            ObjectDataSource1.SelectMethod = "GetData2";
        else if (Request.QueryString["ordonare"] == "3")
            ObjectDataSource1.SelectMethod = "GetData3";
        else if (Request.QueryString["ordonare"] == "4")
            ObjectDataSource1.SelectMethod = "GetData4";
        rvListaGospodarii.LocalReport.Refresh();
    }
    protected void Page_SaveStateComplete(object sender, EventArgs e)
    {
        Warning[] warnings;
        string[] streamids;
        string mimeType;
        string encoding;
        string fileNameExtension;
        byte[] bytes = rvListaGospodarii.LocalReport.Render("PDF", null, out mimeType, out encoding, out fileNameExtension, out streamids, out warnings);
        Response.Buffer = true;
        Response.Clear();
        Response.ContentType = mimeType;
        Response.AddHeader("content-disposition", "inline; filename=Nume" + fileNameExtension);
        Response.BinaryWrite(bytes);
        Response.Flush();
        Response.End();
        Response.Close();
    }
}
