﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ThinkGeo.MapSuite.Core;
using ThinkGeo.MapSuite.WebEdition;
using System.Data.SqlClient;
using System.Collections.ObjectModel;
using System.IO;
using System.Drawing;
using AjaxControlToolkit;

public partial class hartaParcela : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DBConnections connection = new DBConnections(Convert.ToInt16(Session["SESan"]));

        SqlLayers.ConnectionString = connection.Create();
        SqlParcele.ConnectionString = connection.Create();

        afisareCuloriGrid();
        if (ViewState["postback"] != null)
            this.harta.Click += new EventHandler<MapClickedEventArgs>(harta_Click);
        else this.harta.Click -= new EventHandler<MapClickedEventArgs>(harta_Click);
        if (!Page.IsPostBack)
        {
            if (verificareExistentaHarta() == 0)
            {
                valEroare.IsValid = false;
                valEroare.ErrorMessage = "Hartă inexistentă";
                upLista.Visible = false;
                return;
            }
            // background harta
            harta.MapBackground.BackgroundBrush = new GeoSolidBrush(GeoColor.FromHtml("#E5E3DF"));
            // unitatea de masura
            harta.MapUnit = GeographyUnit.Meter;
            // afiseaza coordonate mouse
            harta.MapTools.MouseCoordinate.Enabled = true;

            // layer sid
            MrSidRasterLayer layerHartaSid = new MrSidRasterLayer(MapPath(@"~\Harti\" + extrageCodFiscal() + @"\harta.sid"));
            layerHartaSid.UpperThreshold = double.MaxValue;
            layerHartaSid.LowerThreshold = 0;
            harta.StaticOverlay.Layers.Add(layerHartaSid);
            harta.StaticOverlay.ServerCache = new ServerCache(MapPath("~/CacheHarta"));
            harta.StaticOverlay.ClientCache = new ClientCache(TimeSpan.FromDays(30), "cacheHartaTNT");

            // layer pe care se genereaza si se salveaza parcele
            InMemoryFeatureLayer layerPrincipal = new InMemoryFeatureLayer();
            layerPrincipal.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = PointStyles.CreateSimpleCircleStyle(GeoColor.FromArgb(180, 102, 255, 102), 10, GeoColor.StandardColors.DarkGreen, 1);
            layerPrincipal.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = LineStyles.CreateSimpleLineStyle(GeoColor.StandardColors.Blue, 2, true);
            layerPrincipal.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = AreaStyles.CreateSimpleAreaStyle(GeoColor.FromArgb(180, 102, 255, 102), GeoColor.StandardColors.DarkGreen, 1);
            layerPrincipal.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            layerPrincipal.DrawingQuality = DrawingQuality.HighQuality;

            LayerOverlay overlayPrincipal = new LayerOverlay("overlayPrincipal");
            overlayPrincipal.IsBaseOverlay = false;
            overlayPrincipal.TileType = TileType.SingleTile;
            overlayPrincipal.Layers.Add("layerPrincipal", layerPrincipal);
            //overlayPrincipal.ServerCache = new ServerCache(MapPath("~/CacheHarta"));
            harta.CustomOverlays.Add(overlayPrincipal);

            // parcurg layere si afisez pe harta
            /*SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
            SqlCommand vCmd = new SqlCommand();
            vCmd.Connection = vCon;
            vCmd.CommandText = "select layerId, coalesce(layerCuloare,'0000CC') as layerCuloare, layerDenumire from hartaLayer where unitateId='" + Session["SESunitateId"].ToString() + "' order by layerDenumire";
            SqlDataReader vTabel = vCmd.ExecuteReader();
            while (vTabel.Read())
            {
                InMemoryFeatureLayer layerPrincipal1 = new InMemoryFeatureLayer();
                layerPrincipal1.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = PointStyles.CreateSimpleCircleStyle(GeoColor.FromArgb(180, 102, 255, 102), 10, GeoColor.FromHtml("#" + vTabel["layerCuloare"].ToString()), 1);
                layerPrincipal1.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = LineStyles.CreateSimpleLineStyle(GeoColor.FromHtml("#" + vTabel["layerCuloare"].ToString()), 2, true);
                layerPrincipal1.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = AreaStyles.CreateSimpleAreaStyle(GeoColor.FromArgb(180, 102, 255, 102), GeoColor.FromHtml("#" + vTabel["layerCuloare"].ToString()), 1);
                layerPrincipal1.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
                layerPrincipal1.DrawingQuality = DrawingQuality.HighQuality;
                
                LayerOverlay overlayPrincipal1 = new LayerOverlay("overlayPrincipal" + vTabel["layerId"].ToString());
                overlayPrincipal1.IsBaseOverlay = false;
                overlayPrincipal1.TileType = TileType.SingleTile;
                overlayPrincipal1.Layers.Add("layerPrincipal" + vTabel["layerId"].ToString(), layerPrincipal1);
                harta.CustomOverlays.Add(overlayPrincipal1);
                //deseneazaLinii(overlayPrincipal1, layerPrincipal1, vTabel["layerId"].ToString());
            }
            ManipuleazaBD.InchideConexiune(vCon);*/

            // generez harta
            deseneazaLinii();

            // layer pe care se afiseaza textele
            InMemoryFeatureLayer layerTexte = new InMemoryFeatureLayer();
            layerTexte.ZoomLevelSet.ZoomLevel18.DefaultPointStyle = PointStyles.CreateSimpleCircleStyle(GeoColor.FromArgb(180, 102, 255, 102), 2, GeoColor.StandardColors.Yellow, 1);
            layerTexte.ZoomLevelSet.ZoomLevel18.DefaultLineStyle = LineStyles.CreateSimpleLineStyle(GeoColor.StandardColors.Blue, 2, true);
            layerTexte.ZoomLevelSet.ZoomLevel18.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            layerTexte.DrawingQuality = DrawingQuality.HighQuality;

            layerTexte.Open();
            layerTexte.Columns.Add(new FeatureSourceColumn("Nume"));
            layerTexte.Close();
            layerTexte.ZoomLevelSet.ZoomLevel18.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("Nume", "Arial", 10, DrawingFontStyles.Regular, GeoColor.StandardColors.Yellow);
            layerTexte.ZoomLevelSet.ZoomLevel18.DefaultTextStyle.TextLineSegmentRatio = 2;
            layerTexte.ZoomLevelSet.ZoomLevel18.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

            LayerOverlay overlayTexte = new LayerOverlay("overlayTexte");
            overlayTexte.IsBaseOverlay = false;
            overlayTexte.TileType = TileType.SingleTile;
            overlayTexte.Layers.Add("layerTexte", layerTexte);
            //overlayPrincipal.ServerCache = new ServerCache(MapPath("~/CacheHarta"));
            harta.CustomOverlays.Add(overlayTexte);

            scrieTexte();

            // fac un layer nou pentru linii selectii
            InMemoryFeatureLayer layerSelectie = new InMemoryFeatureLayer();
            layerSelectie.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = PointStyles.CreateSimpleCircleStyle(GeoColor.FromArgb(180, 102, 255, 102), 10, GeoColor.StandardColors.DarkGreen, 1);
            layerSelectie.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = LineStyles.CreateSimpleLineStyle(GeoColor.StandardColors.Red, 4, true);
            layerSelectie.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = AreaStyles.CreateSimpleAreaStyle(GeoColor.FromArgb(180, 102, 255, 102), GeoColor.StandardColors.DarkGreen, 1);
            layerSelectie.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            layerSelectie.DrawingQuality = DrawingQuality.HighQuality;

            LayerOverlay overlaySelectie = new LayerOverlay("overlaySelectie");
            overlaySelectie.IsBaseOverlay = false;
            overlaySelectie.TileType = TileType.SingleTile;
            overlaySelectie.Layers.Add("layerSelectie", layerSelectie);
            harta.CustomOverlays.Add(overlaySelectie);

            // fac un layer pentru parcelele selectate
            InMemoryFeatureLayer layerParcele = new InMemoryFeatureLayer();
            layerParcele.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = PointStyles.CreateSimpleCircleStyle(GeoColor.FromArgb(180, 102, 255, 102), 10, GeoColor.StandardColors.DarkGreen, 1);
            layerParcele.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = LineStyles.CreateSimpleLineStyle(GeoColor.StandardColors.Green, 2, true);
            layerParcele.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = AreaStyles.CreateSimpleAreaStyle(GeoColor.FromArgb(180, 102, 255, 102), GeoColor.StandardColors.DarkGreen, 1);
            layerParcele.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            layerParcele.DrawingQuality = DrawingQuality.HighQuality;

            LayerOverlay overlayParcele = new LayerOverlay("overlayParcele");
            overlayParcele.IsBaseOverlay = false;
            overlayParcele.TileType = TileType.SingleTile;
            overlayParcele.Layers.Add("layerParcele", layerParcele);
            //overlayParcele.ServerCache = new ServerCache(MapPath("~/CacheHarta"));
            harta.CustomOverlays.Add(overlayParcele);

            // fac un layer pentru parcelele selectate
            InMemoryFeatureLayer layerParceleMultiple = new InMemoryFeatureLayer();
            layerParceleMultiple.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = PointStyles.CreateSimpleCircleStyle(GeoColor.FromArgb(180, 102, 255, 200), 10, GeoColor.StandardColors.DarkGreen, 1);
            layerParceleMultiple.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = LineStyles.CreateSimpleLineStyle(GeoColor.StandardColors.Green, 2, true);
            layerParceleMultiple.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = AreaStyles.CreateSimpleAreaStyle(GeoColor.FromArgb(180, 102, 255, 200), GeoColor.StandardColors.DarkGreen, 1);
            layerParceleMultiple.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            layerParceleMultiple.DrawingQuality = DrawingQuality.HighQuality;

            LayerOverlay overlayParceleMultiple = new LayerOverlay("overlayParceleMultiple");
            overlayParceleMultiple.IsBaseOverlay = false;
            overlayParceleMultiple.TileType = TileType.SingleTile;
            overlayParceleMultiple.Layers.Add("layerParceleMultiple", layerParceleMultiple);
            //overlayParceleMultiple.ServerCache = new ServerCache(MapPath("~/CacheHarta"));
            harta.CustomOverlays.Add(overlayParceleMultiple);

            harta.ZoomTo(new PointShape(579230, 531669), 100);
        }
    }
    private int verificareExistentaHarta()
    {
        int vVerificare = 0;
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select count(*) from coordonate where unitateId='" + Session["SESunitateId"].ToString() + "'";
        vVerificare += Convert.ToInt32(vCmd.ExecuteScalar());
        vCmd.CommandText = "select count(*) from hartaShapes where unitateId='" + Session["SESunitateId"].ToString() + "'";
        vVerificare += Convert.ToInt32(vCmd.ExecuteScalar());
        vCmd.CommandText = "select count(*) from hartaTexte where unitateId='" + Session["SESunitateId"].ToString() + "'";
        vVerificare += Convert.ToInt32(vCmd.ExecuteScalar());
        if (File.Exists(MapPath(@"~\Harti\" + extrageCodFiscal() + @"\harta.sid")))
            vVerificare++;
        ManipuleazaBD.InchideConexiune(vCon);
        return vVerificare;
    }
    private string extrageCodFiscal()
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select top(1) unitateCodFiscal from unitati where unitateId='" + Session["SESunitateId"].ToString() + "'";
        string vCodFiscal = vCmd.ExecuteScalar().ToString();
        ManipuleazaBD.InchideConexiune(vCon);
        return vCodFiscal;
    }
    //private void deseneazaLinii(LayerOverlay overlayPrincipal, InMemoryFeatureLayer layerPrincipal, string vLayerId)
    private void deseneazaLinii()
    {
        LayerOverlay overlayPrincipal = (LayerOverlay)harta.CustomOverlays["overlayPrincipal"];
        InMemoryFeatureLayer layerPrincipal = (InMemoryFeatureLayer)overlayPrincipal.Layers["layerPrincipal"];
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "SELECT shapeId, shapePunctId, shapeOrdine, layerId, unitateId, shapePunctX, shapePunctY, shapePunctZ FROM hartaShapes where unitateId='" + Session["SESunitateId"].ToString() + "' ORDER BY shapeId, shapeOrdine";
        //vCmd.CommandText = "SELECT shapeId, shapePunctId, shapeOrdine, layerId, unitateId, shapePunctX, shapePunctY, shapePunctZ FROM hartaShapes where unitateId='" + Session["SESunitateId"].ToString() + "' and layerId = '" + vLayerId + "' ORDER BY shapeId, shapeOrdine";
        SqlDataReader vTabel = vCmd.ExecuteReader();
        List<ListaSelect> vListaSelect = new List<ListaSelect>();
        while (vTabel.Read())
        {
            ListaSelect vElementLista = new ListaSelect()
            {
                Col1 = vTabel["shapeId"].ToString(),
                Col2 = vTabel["shapePunctId"].ToString(),
                Col3 = vTabel["shapeOrdine"].ToString(),
                Col4 = vTabel["layerId"].ToString(),
                Col5 = Convert.ToDecimal(vTabel["shapePunctX"].ToString()).ToString().Replace(".", ","),
                Col6 = Convert.ToDecimal(vTabel["shapePunctY"].ToString()).ToString().Replace(".", ","),
                Col7 = Convert.ToDecimal(vTabel["shapePunctZ"].ToString()).ToString().Replace(".", ",")
            };
            vListaSelect.Add(vElementLista);
        }
        vTabel.Close();
        List<Vertex> vListaPuncte = new List<Vertex>();
        for (int a = 0; a < vListaSelect.Count; a++)
        {
            if (a + 1 < vListaSelect.Count)
            {
                if (vListaSelect[a + 1].Col1 != vListaSelect[a].Col1)
                {
                    Vertex vVertex = new Vertex(Convert.ToDouble(vListaSelect[a].Col6), Convert.ToDouble(vListaSelect[a].Col5));
                    vListaPuncte.Add(vVertex);
                    // adaug pe harta
                    Feature vFeature = new Feature();
                    if (vListaPuncte.Count == 1)
                        vFeature = new Feature(new PointShape(new Vertex(vListaPuncte[0].X, vListaPuncte[0].Y)));
                    else vFeature = new Feature(new LineShape(vListaPuncte));
                    layerPrincipal.InternalFeatures.Add(vListaSelect[a].Col1, vFeature);
                    // golire lista
                    vListaPuncte.Clear();
                }
                else
                {
                    // adaug punct
                    Vertex vVertex = new Vertex(Convert.ToDouble(vListaSelect[a].Col6), Convert.ToDouble(vListaSelect[a].Col5));
                    vListaPuncte.Add(vVertex);
                }
            }
            else
            {
                Vertex vVertex = new Vertex(Convert.ToDouble(vListaSelect[a].Col6), Convert.ToDouble(vListaSelect[a].Col5));
                vListaPuncte.Add(vVertex);
                Feature vFeature = new Feature();
                if (vListaPuncte.Count == 1)
                    vFeature = new Feature(new PointShape(new Vertex(vListaPuncte[0].X, vListaPuncte[0].Y)));
                else vFeature = new Feature(new LineShape(vListaPuncte));
                layerPrincipal.InternalFeatures.Add(vListaSelect[a].Col1, vFeature);
                vListaPuncte.Clear();
            }
        }
        overlayPrincipal.Redraw();
        ManipuleazaBD.InchideConexiune(vCon);
    }
    private void scrieTexte()
    {
        LayerOverlay overlayTexte = (LayerOverlay)harta.CustomOverlays["overlayTexte"];
        InMemoryFeatureLayer layerTexte = (InMemoryFeatureLayer)overlayTexte.Layers["layerTexte"];
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "SELECT textId, unitateId, layerId, textFont, textFlags, textHeight, textAngle, textDenumire, textPunctX, textPunctX1, textPunctY, textPunctY1, textPunctZ FROM hartaTexte where unitateId='" + Session["SESunitateId"].ToString() + "'";
        SqlDataReader vTabel = vCmd.ExecuteReader();
        while (vTabel.Read())
        {
            PointShape vPunct = new PointShape(Convert.ToDouble(vTabel["textPunctY"].ToString().Replace(".", ",")), Convert.ToDouble(vTabel["textPunctX"].ToString().Replace(".", ",")));
            Feature vFeature = new Feature(vPunct);
            vFeature.ColumnValues.Add("Nume", vTabel["textDenumire"].ToString());
            layerTexte.InternalFeatures.Add(vTabel["textId"].ToString(), vFeature);
        }
        vTabel.Close();
        ManipuleazaBD.InchideConexiune(vCon);
    }
    protected void lbAfisareParcele_Click(object sender, EventArgs e)
    {
        ViewState["postback"] = 1;
        LayerOverlay overlayParcele = (LayerOverlay)harta.CustomOverlays["overlayParcele"];
        InMemoryFeatureLayer layerParcele = (InMemoryFeatureLayer)overlayParcele.Layers["layerParcele"];
        layerParcele.Open();
        layerParcele.InternalFeatures.Clear();
        layerParcele.Close();
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        // extrag parcelele selectate in gvParcele
        for (int a = 0; a < gvParcele.Rows.Count; a++)
        {
            CheckBox vCbAfisare = (CheckBox)gvParcele.Rows[a].FindControl("cbAfisare");
            if (vCbAfisare.Checked)
            {
                Label vLblParcelaId = (Label)gvParcele.Rows[a].FindControl("lblParcelaId");
                // sterg parcela cu id-ul curent de pe harta
                if (layerParcele.InternalFeatures.Count > 0)
                    if (layerParcele.InternalFeatures.Contains(vLblParcelaId.Text))
                        layerParcele.InternalFeatures.Remove(vLblParcelaId.Text);
                vCmd.CommandText = "SELECT coordonataX, coordonataY FROM coordonate WHERE (parcelaId = '" + vLblParcelaId.Text + "') AND (an = '" + Session["SESan"].ToString() + "') ORDER BY coordonataOrdine";
                SqlDataReader vTabel = vCmd.ExecuteReader();
                PolygonShape vPoligon = new PolygonShape();
                while (vTabel.Read())
                {
                    Vertex vVertex = new Vertex(Convert.ToDouble(vTabel["coordonataX"].ToString().Replace(".", ",")), Convert.ToDouble(vTabel["coordonataY"].ToString().Replace(".", ",")));
                    vPoligon.OuterRing.Vertices.Add(vVertex);
                }
                vTabel.Close();
                if (vPoligon.OuterRing.Vertices.Count > 0)
                    if (vPoligon.OuterRing.Vertices[0] == vPoligon.OuterRing.Vertices[vPoligon.OuterRing.Vertices.Count - 1])
                    {
                        layerParcele.InternalFeatures.Add(vLblParcelaId.Text, new Feature(vPoligon.GetWellKnownText(), vLblParcelaId.Text));
                    }
            }
        }
        ManipuleazaBD.InchideConexiune(vCon);
        overlayParcele.Redraw();
        Page_Load(sender, e);
    }
    protected void harta_Click(object sender, MapClickedEventArgs e)
    {
        pnlToolTip.Style.Remove("top");
        pnlToolTip.Style.Remove("left");
        pnlToolTip.Style.Remove("display");
        pnlToolTip.Style.Add("left", "-10000px");
        pnlToolTip.Style.Add("top", "-10000px");
        pnlToolTip.Style.Add("display", "none");

        if (ViewState["leagaLinii"] != null)
        {
            // uneste linii poligon
            List<LineShape> vListaFeatures = new List<LineShape>();
            if (ViewState["listaLinii"] != null)
                vListaFeatures = (List<LineShape>)ViewState["listaLinii"];
            LayerOverlay overlayPrincipal = (LayerOverlay)harta.CustomOverlays["overlayPrincipal"];
            InMemoryFeatureLayer layerPrincipal = (InMemoryFeatureLayer)overlayPrincipal.Layers["layerPrincipal"];
            layerPrincipal.Open();
            LayerOverlay overlaySelectie = (LayerOverlay)harta.CustomOverlays["overlaySelectie"];
            InMemoryFeatureLayer layerSelectie = (InMemoryFeatureLayer)overlaySelectie.Layers["layerSelectie"];
            layerSelectie.Open();
            // pozitie click
            PointShape clickWorldPosition = e.Position;
            // convertexc pozitia in pozitie pe ecran
            ScreenPointF clickScreenPoint = harta.ToScreenCoordinate(clickWorldPosition);
            // 5 is the radius to buffer the click point.
            ScreenPointF clickNextScreenPoint = new ScreenPointF(clickScreenPoint.X + 5, clickScreenPoint.Y);
            // calculate the distance is world coordinate of the radius.
            double worldDistance = ExtentHelper.GetWorldDistanceBetweenTwoScreenPoints(harta.CurrentExtent, clickScreenPoint, clickNextScreenPoint, (float)harta.WidthInPixels, (float)harta.HeightInPixels, GeographyUnit.Meter, DistanceUnit.Meter);
            // calculate the buffer which you need.
            MultipolygonShape clickArea = clickWorldPosition.Buffer(worldDistance, GeographyUnit.Meter, DistanceUnit.Meter);
            Collection<Feature> features = layerPrincipal.QueryTools.GetFeaturesIntersecting(clickArea, ReturningColumnsType.AllColumns);
            // daca am gasit o linie o adaug in lista linii
            if (features.Count > 0)
            {
                // daca este de tip linie o adaug in lista
                if (features[0].GetShape().GetType().Name == "LineShape")
                {
                    if (!layerSelectie.InternalFeatures.Contains(features[0].Id))
                    {
                        layerPrincipal.InternalFeatures.Remove(features[0].Id);
                        layerSelectie.InternalFeatures.Add(features[0].Id, features[0]);
                        vListaFeatures.Add((LineShape)features[0].GetShape());
                    }
                    else
                    {
                        layerSelectie.InternalFeatures.Remove(features[0].Id);
                        layerPrincipal.InternalFeatures.Add(features[0].Id, features[0]);
                        for (int a = 0; a < vListaFeatures.Count; a++)
                        {
                            if (vListaFeatures[a].Id == features[0].Id)
                            {
                                vListaFeatures.RemoveAt(a);
                                break;
                            }
                        }
                    }
                    ViewState["listaLinii"] = vListaFeatures;
                }
            }
            overlayPrincipal.Redraw();
            overlaySelectie.Redraw();
        }
        else
        {
            LayerOverlay overlayParcele = (LayerOverlay)harta.CustomOverlays["overlayParcele"];
            InMemoryFeatureLayer layerParcele = (InMemoryFeatureLayer)overlayParcele.Layers["layerParcele"];
            layerParcele.Open();
            Collection<Feature> selectedFeatures = layerParcele.QueryTools.GetFeaturesContaining(e.Position, ReturningColumnsType.NoColumns);
            if (ViewState["parceleMultiple"] != null)
            {
                if (selectedFeatures.Count > 0)
                {
                    LayerOverlay overlayParceleMultiple = (LayerOverlay)harta.CustomOverlays["overlayParceleMultiple"];
                    InMemoryFeatureLayer layerParceleMultiple = (InMemoryFeatureLayer)overlayParceleMultiple.Layers["layerParceleMultiple"];

                    if (layerParceleMultiple.InternalFeatures.Contains(selectedFeatures[0]))
                        layerParceleMultiple.InternalFeatures.Remove(selectedFeatures[0]);
                    else layerParceleMultiple.InternalFeatures.Add(selectedFeatures[0]);
                    overlayParceleMultiple.Redraw();
                    return;
                }
                else return;
            }
            CloudPopup popup;
            if (harta.Popups.Count == 0)
            {
                popup = new CloudPopup("Popup", e.Position, string.Empty);
                popup.AutoSize = true;
                harta.Popups.Add(popup);


            }
            else
            {
                popup = (CloudPopup)harta.Popups["Popup"];
                popup.Position = e.Position;
            }
            // se afla coordonatele ecranului

            // Calculez aria si o afisez intr-un popup
            if (selectedFeatures.Count > 0)
            {
                // caut detalii despre parcela aleasa
                SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
                SqlCommand vCmd = new SqlCommand();
                vCmd.Connection = vCon;
                BaseShape vShape = selectedFeatures[0].GetShape();
                vCmd.CommandText = "SELECT gospodarii.volum, gospodarii.nrPozitie, CASE gospodarii.tip WHEN '1' THEN 'Localnic' WHEN '2' THEN 'Străinaş' WHEN '3' THEN 'Firmă pe raza localităţii' ELSE 'Firmă străinaşă' END AS tip, CASE strainas WHEN 1 THEN sstrada ELSE strada END AS strada, CASE strainas WHEN 1 THEN snr ELSE nr END AS nr, gospodarii.nrInt, gospodarii.bl, gospodarii.sc, gospodarii.et, gospodarii.ap, CASE strainas WHEN 1 THEN sjudet ELSE judet END AS judet, CASE strainas WHEN 1 THEN slocalitate ELSE localitate END AS localitate, CASE persjuridica WHEN 0 THEN (SELECT TOP (1) nume FROM membri WHERE an = '" + Session["SESan"].ToString() + "' AND gospodarieId = gospodarii.gospodarieid AND codrudenie = 1) ELSE junitate END AS nume, parcele.parcelaNrTopo, parcele.parcelaCF, parcele.parcelaDenumire FROM gospodarii INNER JOIN parcele ON gospodarii.gospodarieId = parcele.gospodarieId AND gospodarii.an = parcele.an WHERE (parcele.parcelaId = '" + vShape.Id + "') AND (gospodarii.an = '" + Session["SESan"].ToString() + "')";
                SqlDataReader vTabel = vCmd.ExecuteReader();
                string vDetalii = "Parcela aleasă are o suprafaţă de ";
                if (vTabel.Read())
                {
                    vDetalii = vTabel["nume"].ToString() + ", volumul " + vTabel["volum"].ToString() + ", poziţia " + vTabel["nrPozitie"].ToString() + " din loc. " + vTabel["localitate"].ToString() + ", str. " + vTabel["strada"].ToString() + ", nr. " + vTabel["nr"].ToString() + " deţine parcela < " + vTabel["parcelaDenumire"].ToString() + " >, CF " + ((vTabel["parcelaCF"].ToString() == "") ? "-" : vTabel["parcelaCF"].ToString()) + ", Nr. topografic " + ((vTabel["parcelaNrTopo"].ToString() == "") ? "-" : vTabel["parcelaNrTopo"].ToString()) + " care are o suprafaţă de ";
                }
                vTabel.Close();
                ManipuleazaBD.InchideConexiune(vCon);
                popup.IsVisible = true;
                AreaBaseShape areaShape = (AreaBaseShape)selectedFeatures[0].GetShape();
                double area = areaShape.GetArea(GeographyUnit.Meter, AreaUnit.SquareMeters);
                // transform metri in ha si in ari
                double vAri = area / 100;
                double vHa = Math.Truncate(vAri / 100);
                vAri = vAri - (vHa * 100);
                object[] vObiecte = new object[] { vDetalii, area, vHa, vAri };
                //popup.ContentHtml = string.Format(@"<div style='color:#0065ce;font-size:10px; font-family:verdana; padding:4px;'>{0}<span style='color:red'>{1:N0}</span> metri pătraţi (<span style='color:red'>{2:N0}</span> ha şi <span style='color:red'>{3:N2}</span> ari).</div><br/>", vObiecte); 
                try
                {
                    pnlToolTip.InnerHtml = string.Format(@"<div id='tooltip_inchide' style='font-size: 9px; color: Red; position: relative; cursor:pointer; float: right; top: 0px;
                    right: 0; width: 30px; height: 13px;'><u>închide</u></div><div style='color:#0065ce;font-size:10px; font-family:verdana; padding:4px;'>{0}<span style='color:red'>{1:N0}</span> metri pătraţi (<span style='color:red'>{2:N0}</span> ha şi <span style='color:red'>{3:N2}</span> ari).</div><br/>", vObiecte);
                    ScreenPointF clickScreenPoint1 = harta.ToScreenCoordinate(e.Position);
                    double x = clickScreenPoint1.X;

                    ///////////////////////////////
                    pnlToolTip.Style.Remove("top");
                    pnlToolTip.Style.Remove("left");
                    pnlToolTip.Style.Remove("display");
                    string vX = "";
                    string vY = "";
                    vX = clickScreenPoint1.X.ToString().Substring(0, clickScreenPoint1.X.ToString().IndexOf(",")) + "px";
                    vY = clickScreenPoint1.Y.ToString().Substring(0, clickScreenPoint1.Y.ToString().IndexOf(",")) + "px";
                    //vX = vX.Remove(vX.IndexOf('.'));
                    //  vY = vY.Remove(vY.IndexOf('.'));
                    pnlToolTip.Style.Add("left", vX);
                    pnlToolTip.Style.Add("top", vY);
                    pnlToolTip.Style.Add("display", "block");

                }
                catch { }
            }
            else
            {
                popup.IsVisible = false;
            }

        }



    }
    protected void lbCalculDistanta_Click(object sender, EventArgs e)
    {
        LayerOverlay overlayParceleMultiple = (LayerOverlay)harta.CustomOverlays["overlayParceleMultiple"];
        InMemoryFeatureLayer layerParceleMultiple = (InMemoryFeatureLayer)overlayParceleMultiple.Layers["layerParceleMultiple"];
        if (layerParceleMultiple.InternalFeatures.Count > 0)
        {
            // calculez suprafete pentru parcelele selectate
            double vSuprafata = 0;
            foreach (Feature vFeature in layerParceleMultiple.InternalFeatures)
            {
                AreaBaseShape areaShape = (AreaBaseShape)vFeature.GetShape();
                vSuprafata += areaShape.GetArea(GeographyUnit.Meter, AreaUnit.SquareMeters);
            }
            ViewState["parceleMultiple"] = null;
            double vAri = vSuprafata / 100;
            double vHa = Math.Truncate(vAri / 100);
            vAri = vAri - (vHa * 100);
            valEroare.IsValid = false;
            valEroare.ErrorMessage = "Suprafaţa " + vSuprafata.ToString("F") + " metri (" + vHa.ToString() + " ha şi " + vAri.ToString("F") + " ari)";
            layerParceleMultiple.InternalFeatures.Clear();
            overlayParceleMultiple.Redraw();
        }
        else
        {
            // calculez suprafete si distante pntru figurile geometrice desenate
            if (harta.EditOverlay.Features.Count > 0)
            {
                Feature vFeature = harta.EditOverlay.Features[harta.EditOverlay.Features.Count - 1];
                if (vFeature.GetShape().GetType().Name == "LineShape")
                {
                    LineShape vLinie = (LineShape)vFeature.GetShape();
                    double vDistanta = vLinie.GetLength(GeographyUnit.Meter, DistanceUnit.Meter);
                    valEroare.IsValid = false;
                    valEroare.ErrorMessage = "Distanţa " + vDistanta.ToString("F") + " metri";
                }
                else if (vFeature.GetShape().GetType().Name != "PointShape")
                {
                    AreaBaseShape areaShape = (AreaBaseShape)vFeature.GetShape();
                    double vSuprafata = areaShape.GetArea(GeographyUnit.Meter, AreaUnit.SquareMeters);
                    double vAri = vSuprafata / 100;
                    double vHa = Math.Truncate(vAri / 100);
                    vAri = vAri - (vHa * 100);
                    valEroare.IsValid = false;
                    valEroare.ErrorMessage = "Suprafaţa " + vSuprafata.ToString("F") + " metri (" + vHa.ToString() + " ha şi " + vAri.ToString("F") + " ari)";
                }
            }
            else
            {
                valEroare.IsValid = false;
                valEroare.ErrorMessage = "Alegeţi o figură geometrică de mai sus şi apoi apăsaţi acest buton ! ";
            }
            harta.EditOverlay.Features.Clear();
        }
    }

    #region parcele pentru afisare pe harta

    protected void lbSelectareParcele_Click(object sender, EventArgs e)
    {
        LayerOverlay overlayParceleMultiple = (LayerOverlay)harta.CustomOverlays["overlayParceleMultiple"];
        InMemoryFeatureLayer layerParceleMultiple = (InMemoryFeatureLayer)overlayParceleMultiple.Layers["layerParceleMultiple"];
        layerParceleMultiple.InternalFeatures.Clear();
        ViewState["parceleMultiple"] = 1;
        ViewState["postback"] = 1;
    }
    protected void gvParcele_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(gvParcele, e, this);
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // pun intr-un viewstate toate parcelele selectate
            Collection<string> vListaParceleSelectate = new Collection<string>();
            if (ViewState["parceleSelectate"] == null)
                ViewState["parceleSelectate"] = vListaParceleSelectate;
            vListaParceleSelectate = (Collection<string>)ViewState["parceleSelectate"];
            CheckBox vCbAfisare = (CheckBox)e.Row.FindControl("cbAfisare");
            Label vLblParcelaId = (Label)e.Row.FindControl("lblParcelaId");
            if (vLblParcelaId.Text == "476586")
            { }
            if (vListaParceleSelectate.Contains(vLblParcelaId.Text))
            {
                vCbAfisare.Checked = true;
            }
            else
                vCbAfisare.Checked = false;
        }
    }
    protected void lbSelecteazaTot_Click(object sender, EventArgs e)
    {
        // merg pe fiecare rand si selectez sau deselectez
        if (ViewState["selectat"] == null)
            ViewState["selectat"] = "0";
        Collection<string> vListaParceleSelectate = new Collection<string>();
        if (ViewState["parceleSelectate"] == null)
            ViewState["parceleSelectate"] = vListaParceleSelectate;
        vListaParceleSelectate = (Collection<string>)ViewState["parceleSelectate"];
        gvParcele.AllowPaging = false;
        gvParcele.DataBind();
        for (int a = 0; a < gvParcele.Rows.Count; a++)
        {
            CheckBox vCbAfisare = (CheckBox)gvParcele.Rows[a].FindControl("cbAfisare");
            Label vLblParcelaId = (Label)gvParcele.Rows[a].FindControl("lblParcelaId");
            if (vListaParceleSelectate.Contains(vLblParcelaId.Text))
                vListaParceleSelectate.Remove(vLblParcelaId.Text);
            if (ViewState["selectat"].ToString() == "1")
                vCbAfisare.Checked = false;
            else
            {
                vCbAfisare.Checked = true;
                vListaParceleSelectate.Add(vLblParcelaId.Text);
            }
        }
        gvParcele.AllowPaging = true;
        gvParcele.PageSize = 10;

        if (ViewState["selectat"].ToString() == "1")
            ViewState["selectat"] = "0";
        else ViewState["selectat"] = "1";
    }
    protected void cbAfisare_CheckedChanged(object sender, EventArgs e)
    {
        Collection<string> vListaParceleSelectate = new Collection<string>();
        if (ViewState["parceleSelectate"] == null)
            ViewState["parceleSelectate"] = vListaParceleSelectate;
        vListaParceleSelectate = (Collection<string>)ViewState["parceleSelectate"];
        CheckBox vCbAfisare = (CheckBox)sender;
        Label vLblParcelaId = (Label)((CheckBox)sender).Parent.FindControl("lblParcelaId");
        if (vListaParceleSelectate.Contains(vLblParcelaId.Text))
            vListaParceleSelectate.Remove(vLblParcelaId.Text);
        if (vCbAfisare.Checked)
            vListaParceleSelectate.Add(vLblParcelaId.Text);
    }

    #endregion

    #region generare parcele din linii selectate

    protected void lbLeagaLinii_Click(object sender, EventArgs e)
    {
        ViewState["leagaLinii"] = 1;
        ViewState["postback"] = 1;
        Page_Load(sender, e);
        // golesc linii din layerSelectie
        golireLiniiDinLayerSelectie();
    }
    private void golireLiniiDinLayerSelectie()
    {
        LayerOverlay overlayPrincipal = (LayerOverlay)harta.CustomOverlays["overlayPrincipal"];
        InMemoryFeatureLayer layerPrincipal = (InMemoryFeatureLayer)overlayPrincipal.Layers["layerPrincipal"];
        layerPrincipal.Open();
        LayerOverlay overlaySelectie = (LayerOverlay)harta.CustomOverlays["overlaySelectie"];
        InMemoryFeatureLayer layerSelectie = (InMemoryFeatureLayer)overlaySelectie.Layers["layerSelectie"];
        layerSelectie.Open();
        List<Feature> vListaFeature = new List<Feature>();
        foreach (Feature vFeature in layerSelectie.InternalFeatures)
            vListaFeature.Add(vFeature);
        foreach (Feature vFeature in vListaFeature)
        {
            layerSelectie.InternalFeatures.Remove(vFeature.Id);
            layerPrincipal.InternalFeatures.Remove(vFeature.Id);
            //layerPrincipal.InternalFeatures.Add(vFeature.Id, vFeature);
        }
    }
    protected void lbTransformaInPoligon_Click(object sender, EventArgs e)
    {
        if (verificareSalvare() != "")
        {
            valEroare.IsValid = false;
            valEroare.ErrorMessage = verificareSalvare();
            return;
        }
        if (ViewState["listaLinii"] != null)
            unireLiniiInPoligon();
    }
    private void unireLiniiInPoligon()
    {
        if (ViewState["listaLinii"] == null)
        {
            golireLiniiDinLayerSelectie();
            return;
        }
        LayerOverlay overlayParcele = (LayerOverlay)harta.CustomOverlays["overlayParcele"];
        InMemoryFeatureLayer layerParcele = (InMemoryFeatureLayer)overlayParcele.Layers["layerParcele"];
        layerParcele.Open();
        List<LineShape> vListaLinii = (List<LineShape>)ViewState["listaLinii"];
        PolygonShape vPoligon = new PolygonShape();
        Collection<Vertex> vListaVertex = new Collection<Vertex>();
        Vertex vVertexStart = vListaLinii[0].Vertices[0];
        vListaVertex.Add(vVertexStart);
        Collection<LineShape> vListaLiniiTemp = new Collection<LineShape>();
        foreach (LineShape vLinie in vListaLinii)
            vListaLiniiTemp.Add(vLinie);
        Vertex vUltimulPunct = new Vertex(0, 0);
        vListaVertex = adaugaVertex(vListaLiniiTemp, vListaLinii[0], vListaVertex, vUltimulPunct);

        foreach (Vertex vVertex in vListaVertex)
            vPoligon.OuterRing.Vertices.Add(vVertex);
        //try
        {
            if (vPoligon.OuterRing.Vertices[0] != vPoligon.OuterRing.Vertices[vPoligon.OuterRing.Vertices.Count - 1])
                vPoligon.OuterRing.Vertices.Add(vPoligon.OuterRing.Vertices[0]);
            // sterg parcela cu id-ul curent de pe harta
            if (layerParcele.InternalFeatures.Count > 0)
                if (layerParcele.InternalFeatures.Contains(gvParcele.SelectedValue.ToString()))
                    layerParcele.InternalFeatures.Remove(gvParcele.SelectedValue.ToString());

            layerParcele.InternalFeatures.Add(gvParcele.SelectedValue.ToString(), new Feature(vPoligon));
            // adaug punctele intr-o lista si in viewstate
            List<Vertex> vListaPunctePoligon = new List<Vertex>();
            foreach (Vertex vVertex in vPoligon.OuterRing.Vertices)
                vListaPunctePoligon.Add(vVertex);
            ViewState["listaPoligon"] = vListaPunctePoligon;
        }
        //catch { }
        overlayParcele.Redraw();
    }
    private void cautaVertexLinie(ref Collection<Vertex> vListaVertex, LineShape vLinie, ref Vertex vUltimulPunct)
    {
        Collection<Vertex> vPuncteTemp = new Collection<Vertex>();
        if (vUltimulPunct.X != 0 && vUltimulPunct.Y != 0)
            vListaVertex.Add(vUltimulPunct);
        else
        {
            vUltimulPunct = vLinie.Vertices[0];
            vListaVertex.Add(vUltimulPunct);
        }
        PointShape vPunct = new PointShape(vUltimulPunct);
        for (int a = 0; a < vLinie.Vertices.Count; a++)
            vPuncteTemp.Add(vLinie.Vertices[a]);
        vPuncteTemp.Remove(vUltimulPunct);
        vLinie = new LineShape(vPuncteTemp);
        if (vLinie.Vertices.Count > 1)
        {
            PointShape vPunctTemp = vPunct;
            vPunct = vLinie.GetClosestPointTo(vPunct, GeographyUnit.Meter);
            Vertex vVertexVerificare = new Vertex(vPunct.X, vPunct.Y);
            if (vLinie.Vertices.Contains(vVertexVerificare))
                vUltimulPunct = new Vertex(vPunct.X, vPunct.Y);
            else vUltimulPunct = vLinie.Vertices[0];
        }
        else vUltimulPunct = vLinie.Vertices[0];
        if (vLinie.Vertices.Count > 1)
            cautaVertexLinie(ref vListaVertex, vLinie, ref vUltimulPunct);
        else
            if (vUltimulPunct.X != 0 && vUltimulPunct.Y != 0)
                vListaVertex.Add(vUltimulPunct);

    }
    private Collection<Vertex> adaugaVertex(Collection<LineShape> vListaLiniiTemp, LineShape vLinie, Collection<Vertex> vListaVertex, Vertex vUltimulPunct)
    {
        Collection<Vertex> vListaVertexLinie = new Collection<Vertex>();
        if (vUltimulPunct.X == 0 && vUltimulPunct.Y == 0)
            vUltimulPunct = vListaLiniiTemp[0].Vertices[0];
        cautaVertexLinie(ref vListaVertexLinie, vLinie, ref vUltimulPunct);

        for (int a = 0; a < vListaVertexLinie.Count; a++)
        {
            if (!vListaVertex.Contains(vListaVertexLinie[a]))
            {
                vListaVertex.Add(vListaVertexLinie[a]);
            }
        }
        vListaLiniiTemp.Remove(vLinie);
        // caut urmatoarea linie
        LineShape vLinieUrmatoare = new LineShape();
        foreach (LineShape vLinieRamas in vListaLiniiTemp)
        {
            foreach (Vertex vVertexRamas in vLinieRamas.Vertices)
            {
                if (vVertexRamas == vUltimulPunct)
                {
                    vLinieUrmatoare = vLinieRamas;
                    break;
                }
            }
        }
        if (vLinieUrmatoare.Vertices.Count > 0)
        {
            vListaVertex = adaugaVertex(vListaLiniiTemp, vLinieUrmatoare, vListaVertex, vUltimulPunct);
        }
        return vListaVertex;
    }
    private string verificareSalvare()
    {
        string vError = "";
        if (gvParcele.SelectedIndex == -1)
            vError += "Alegeţi o parcelă ! ";
        return vError;
    }
    protected void lbSalvareParcela_Click(object sender, EventArgs e)
    {
        if (verificareSalvare() != "")
        {
            valEroare.IsValid = false;
            valEroare.ErrorMessage = verificareSalvare();
            return;
        }
        if (ViewState["listaLinii"] != null)
            unireLiniiInPoligon();
        else
        {
            valEroare.IsValid = false;
            valEroare.ErrorMessage = "Uniţi liniile pentru a forma un poligon ! ";
            return;
        }
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlTransaction vTranz = ManipuleazaBD.DeschideTranzactie(vCon);
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.Transaction = vTranz;
        try
        {
            vCmd.CommandText = "select top(1) gospodarieId from parcele where parcelaId='" + gvParcele.SelectedValue.ToString() + "'";
            string vGospodarieId = vCmd.ExecuteScalar().ToString(), vUnitateId = Session["SESunitateId"].ToString(), vAn = Session["SESAn"].ToString(), vParcelaId = gvParcele.SelectedValue.ToString();
            vCmd.CommandText = "select top(1) volum from gospodarii where gospodarieId='" + vGospodarieId + "'";
            string vVolum = vCmd.ExecuteScalar().ToString();
            vCmd.CommandText = "select top(1) nrPozitie from gospodarii where gospodarieId='" + vGospodarieId + "'";
            string vPozitie = vCmd.ExecuteScalar().ToString();
            // sterg coordonatele deja existente
            vCmd.CommandText = "delete from coordonate where parcelaId='" + gvParcele.SelectedValue.ToString() + "'";
            vCmd.ExecuteNonQuery();
            // adaug noile coordonate
            List<Vertex> vListapoligon = (List<Vertex>)ViewState["listaPoligon"];
            string vInterogare = "";
            for (int a = 0; a < vListapoligon.Count; a++)
            {
                vInterogare += "INSERT INTO coordonate (coordonataX, coordonataY, coordonataZ, unitateId, parcelaId, an, volum, nrPozitie, gospodarieId, coordonataOrdine) VALUES ('" + vListapoligon[a].X.ToString().Replace(",", ".") + "', '" + vListapoligon[a].Y.ToString().Replace(",", ".") + "', 0, '" + vUnitateId + "', '" + vParcelaId + "', '" + vAn + "', '" + vVolum + "', '" + vPozitie + "', '" + vGospodarieId + "', '" + a.ToString() + "');";
            }
            if (vInterogare != "")
            {
                vCmd.CommandText = vInterogare;
                vCmd.ExecuteNonQuery();
            }
            vTranz.Commit();
            // golesc viewstate-ul cu poligon
            ViewState["listaPoligon"] = null;
            ViewState["listaLinii"] = null;
            ViewState["leagaLinii"] = null;
            ViewState["postback"] = null;
            valEroare.IsValid = false;
            valEroare.ErrorMessage = "Parcela a fost salvată cu succes ! ";
        }
        catch { vTranz.Rollback(); }
        finally { ManipuleazaBD.InchideConexiune(vCon); }
    }

    #endregion

    #region layere si culori

    private void afisareCuloriGrid()
    {
        for (int a = 0; a < dlLayers.Items.Count; a++)
        {
            TextBox vTbCuloareGrid = (TextBox)dlLayers.Items[a].FindControl("tbCuloareLayer");
            vTbCuloareGrid.ForeColor = Color.FromName("#" + vTbCuloareGrid.Text);
            vTbCuloareGrid.BackColor = Color.FromName("#" + vTbCuloareGrid.Text);
        }
    }
    protected void dlLayers_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        TextBox vTbCuloareGrid = (TextBox)e.Item.FindControl("tbCuloareLayer");
        vTbCuloareGrid.ForeColor = Color.FromName("#" + vTbCuloareGrid.Text);
        vTbCuloareGrid.BackColor = Color.FromName("#" + vTbCuloareGrid.Text);
    }
    protected void lbAfisareLayers_Click(object sender, EventArgs e)
    {

    }

    #endregion

    #region modificare harta

    protected void lbModificareShapes_Click(object sender, EventArgs e)
    {

    }
    protected void lbSalvareModificari_Click(object sender, EventArgs e)
    {

    }

    #endregion
    protected void gvParcele_PageIndexChanged(object sender, EventArgs e)
    {
        int sa = gvParcele.PageIndex;
        
    }
    protected void gvParcele_PreRender(object sender, EventArgs e)
    {
   /*     for (int b = 0; b < gvParcele.Rows.Count; b++)
        {
            if (gvParcele.Rows[b].RowType == DataControlRowType.DataRow)
            {
                // pun intr-un viewstate toate parcelele selectate
                Collection<string> vListaParceleSelectate = new Collection<string>();
                if (ViewState["parceleSelectate"] == null)
                    ViewState["parceleSelectate"] = vListaParceleSelectate;
                vListaParceleSelectate = (Collection<string>)ViewState["parceleSelectate"];
                CheckBox vCbAfisare = (CheckBox)gvParcele.Rows[b].FindControl("cbAfisare");
                Label vLblParcelaId = (Label)gvParcele.Rows[b].FindControl("lblParcelaId");
                if (vListaParceleSelectate.Contains(vLblParcelaId.Text))
                {
                    vCbAfisare.Checked = true;
                }
                else
                    vCbAfisare.Checked = false;
            }
        }*/
    }
}
