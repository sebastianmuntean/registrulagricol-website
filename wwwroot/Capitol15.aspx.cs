﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Capitol15 : System.Web.UI.Page
{
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["SESgospodarieId"] == null)
        {
            HttpCookie vCookie = Request.Cookies["COOKTNT"];
            vCookie = CriptareCookie.DecodeCookie(vCookie);
            if (vCookie != null)
            {
                if (vCookie["COOKgospodarieId"] != null)
                {
                    Session["SESgospodarieId"] = vCookie["COOKgospodarieId"].ToString();
                }
                else if (Request.Url.AbsolutePath.IndexOf("Gospodarii.aspx") == -1 && Session["SESgospodarieId"] == null)
                    Response.Redirect("~/Gospodarii.aspx");
            }
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        DBConnections connection = new DBConnections(Convert.ToInt16(Session["SESan"]));

        SqlModUtilizareContracteGrid.ConnectionString = connection.Create();
        SqlListaParcele.ConnectionString = connection.Create();
        SqlSabloaneCapitole.ConnectionString = connection.Create();

        Verifica2b2a();
        if (!IsPostBack)
        {
            lblTextGospodariaCurenta.Text = GospodariaCurentaDetalii(Session["SESgospodarieId"].ToString());
            tbCFinal.Text = tbCInceput.Text = DateTime.Now.ToString().Remove(11);
        }
        if (!IsPostBack)
        {
            // verific daca au picat sesiunile
            VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
            vVerificaSesiuni.VerificaSesiuniCookie();
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "Contracte", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
        }
    }
    protected void Verifica2b2a()
    {
        // afisam butonul de contracte doar daca e bifa 
        //      if (ManipuleazaBD.fRezultaUnString("SELECT unitateAdaugaDinC2B FROM unitati WHERE unitateId = '" + Session["SESunitateId"].ToString() + "'", "unitateAdaugaDinC2B") == "False")
        //       Response.Redirect("~/Gospodarii.aspx");
    }
    protected string GospodariaCurentaDetalii(string pGospodarie)
    {
        // pTip = id-ul gospodariei
        string vDetalii = "";
        if (ManipuleazaBD.fVerificaExistentaExtins("gospodarii", "persJuridica", "True", " AND gospodarieId ='" + pGospodarie + "'", Convert.ToInt16(Session["SESan"])) == 1)
        {
            string[] vCampuriRezultate = { "volum", "nrPozitie", "jUnitate" };
            int vContor = 0;
            foreach (string vElement in ManipuleazaBD.fRezultaUnString("SELECT TOP(1) volum, nrPozitie, jUnitate FROM gospodarii WHERE unitateId = '" + Session["SESunitateId"].ToString() + "' AND gospodarieId='" + pGospodarie + "'", vCampuriRezultate, Convert.ToInt16(Session["SESan"])))
            {
                switch (vContor)
                {
                    case 0:
                        vDetalii += "Vol." + vElement.ToString();
                        break;
                    case 1:
                        vDetalii += " / Poz." + vElement.ToString();
                        break;
                    case 2:
                        vDetalii += " / " + vElement.ToString();
                        break;
                }
                vContor++;
            }
        }
        else
        {
            string[] vCampuriRezultate = { "volum", "nrPozitie", "nume" };
            int vContor = 0;
            foreach (string vElement in ManipuleazaBD.fRezultaUnString("SELECT TOP(1) volum, nrPozitie, nume FROM gospodarii INNER JOIN membri ON gospodarii.gospodarieId = membri.gospodarieId WHERE gospodarii.unitateId = '" + Session["SESunitateId"].ToString() + "' AND gospodarii.gospodarieId='" + pGospodarie + "' AND membri.codRudenie='1'", vCampuriRezultate, Convert.ToInt16(Session["SESan"])))
            {
                switch (vContor)
                {
                    case 0:
                        vDetalii += "Vol." + vElement.ToString();
                        break;
                    case 1:
                        vDetalii += " / Poz." + vElement.ToString();
                        break;
                    case 2:
                        vDetalii += " / " + vElement.ToString();
                        break;
                }
                vContor++;
            }

        }
        int vNumar = clsCorelatii.NumarCorelatiiInvalidate(Session["SESgospodarieId"].ToString(), Session["SESUnitateId"].ToString(), Session["SESan"].ToString());
        return vDetalii;
    }
    protected bool PersoanaJuridica(string pGospodarie)
    {
        if (ManipuleazaBD.fRezultaUnString("SELECT persJuridica FROM gospodarii WHERE gospodarieId = '" + pGospodarie + "' ", "persJuridica", Convert.ToInt16(Session["SESan"])) == "True")
            return true;
        else
            return false;
    }

    protected void btContracteUtilizare_Click(object sender, EventArgs e)
    {
        // adaugam contracte pentru modul de utilizare
        pnContracteUtilizare.Visible = true;
        pnlListaContracte.Visible = false;
    }
    protected void InitializareContracte()
    {
        if (ViewState["contractMod"].ToString() == "a")
        {
            tbContractDinData.Text = DateTime.Now.ToString().Remove(11);
            tbContractNr.Text = "";
            try
            {
                ddlDataFinalContract.SelectedValue = Session["SESan"].ToString();
                ddlDataInceputContract.SelectedValue = Session["SESan"].ToString();
            }
            catch
            {
            }
            tbDetaliiContract.Text = "";
            redeventaTextBox.Text = "";
            pnlContracteDetalii.Visible = false;
            btAdaugaModUtilizareContractSalveaza.Visible = false;
            ddlModUtilizareContract.SelectedIndex = 0;
            gvGospodarii.DataBind();
            gvGospodarii.SelectedIndex = -1;
            lblGospodarieId.Text = "";
            lblGospodarieIdGrid.Text = "";
            lblSiGospodaria.Text = "???";
            btAlegeGospodaria.Text = "alege o gospodărie";
        }
        else if (ViewState["contractMod"].ToString() == "m")
        {
            pnlContracteDetalii.Visible = true;
            btAdaugaModUtilizareContractSalveaza.Visible = true;
            // luam datele din contract si le si salvam in alte variabile
            List<string> vCampuri = new List<string> { "contractNr", "contractDataSemnarii", "contractTip", "modUtilizareId", "gospodarieId", "catreGospodarieId", "contractDataInceput", "contractDataFinal", "contractDetalii", "c3Ha", "c3Ari", "anul", "catreC3Rand", "contractId", "contractGospodarieIdPrimeste", "contractGospodarieIdDa", "parcelaId", "c3Rand", "redeventa" };
            List<List<string>> vValori = ManipuleazaBD.fRezultaListaStringuri("SELECT     parceleAtribuiriContracte.contractNr, parceleAtribuiriContracte.contractDataSemnarii, parceleAtribuiriContracte.contractTip, parceleModUtilizare.modUtilizareId, parceleModUtilizare.c3Rand, parceleModUtilizare.c3Ha, parceleModUtilizare.c3Ari, parceleModUtilizare.anul, parceleModUtilizare.gospodarieId, parceleModUtilizare.catreGospodarieId, parceleModUtilizare.catreC3Rand, parceleModUtilizare.contractId, parceleAtribuiriContracte.contractDataFinal, parceleAtribuiriContracte.contractDataInceput, parceleAtribuiriContracte.contractGospodarieIdPrimeste, parceleAtribuiriContracte.contractGospodarieIdDa, parceleAtribuiriContracte.contractDetalii, parceleModUtilizare.parcelaId,redeventa FROM         parceleModUtilizare INNER JOIN parceleAtribuiriContracte ON parceleModUtilizare.contractId = parceleAtribuiriContracte.contractId AND parceleAtribuiriContracte.contractActiv = 1 WHERE  (parceleModUtilizare.contractId='" + ((Label)gvModUtilizareContracte.SelectedRow.FindControl("lblGvContracteId")).Text + "')", vCampuri, Convert.ToInt16(Session["SESan"]));
            //nr contract
            tbContractNr.Text = vValori[0][0];
            // data semnarii
            tbContractDinData.Text = vValori[0][1].Remove(11);
            redeventaTextBox.Text = vValori[0][18].ToString();
            // mod utilizare
            // daca sesgospodarie e acelasi cu gospodarieDa adauga 7 la mod utilizare
            gvGospodarii.DataBind();
            if (vValori[0][4] == Session["SESgospodarieId"].ToString())
            {
                lblTextGospodariaCurenta.Text = GospodariaCurentaDetalii(vValori[0][4]);
                lblSiGospodaria.Text = GospodariaCurentaDetalii(vValori[0][5]);
                ddlModUtilizareContract.SelectedValue = Convert.ToInt16(Convert.ToDecimal(vValori[0][2]) + 7).ToString();
                lblGospodarieIdGrid.Text = vValori[0][4];
                lblGospodarieId.Text = vValori[0][5];
            }
            else
            {
                lblTextGospodariaCurenta.Text = GospodariaCurentaDetalii(vValori[0][5]);
                lblSiGospodaria.Text = GospodariaCurentaDetalii(vValori[0][4]);
                ddlModUtilizareContract.SelectedValue = vValori[0][12];
                lblGospodarieIdGrid.Text = vValori[0][4];
                lblGospodarieId.Text = vValori[0][4];
            }
            RefacemLista();

            btAlegeGospodaria.Text = "schimbă gospodăria";
            // REFACEM GRIDUL CU PARCELE
            // 4 - care da, 5 - care primeste
            // daca sesgosp e 4 atunci 
            // date
            ddlDataInceputContract.SelectedValue = vValori[0][6].Remove(11).Substring(6).Trim();
            ddlDataFinalContract.SelectedValue = vValori[0][7].Remove(11).Substring(6).Trim();
            //tbDataInceputContract.Text = vValori[0][6].Remove(11);
            // tbDataFinalContract.Text = vValori[0][7].Remove(11);
            // detalii contract
            tbDetaliiContract.Text = vValori[0][8];

            btAdaugaModUtilizareContractSalveaza.Visible = true;

        }
    }
    protected List<List<string>> ValoriModificare()
    {
        List<string> vCampuri = new List<string> { "contractNr", "contractDataSemnarii", "c3Rand", "modUtilizareId", "gospodarieId", "catreGospodarieId", "contractDataInceput", "contractDataFinal", "contractDetalii", "c3Ha", "c3Ari", "anul", "catreC3Rand", "contractId", "contractGospodarieIdPrimeste", "contractGospodarieIdDa", "parcelaId", "modUtilizareId" };
        List<List<string>> vValori = ManipuleazaBD.fRezultaListaStringuri("SELECT     parceleAtribuiriContracte.contractNr, parceleAtribuiriContracte.contractDataSemnarii, parceleAtribuiriContracte.contractTip, parceleModUtilizare.modUtilizareId, parceleModUtilizare.c3Rand, parceleModUtilizare.c3Ha, parceleModUtilizare.c3Ari, parceleModUtilizare.anul, parceleModUtilizare.gospodarieId, parceleModUtilizare.catreGospodarieId, parceleModUtilizare.catreC3Rand, parceleModUtilizare.contractId, parceleAtribuiriContracte.contractDataFinal, parceleAtribuiriContracte.contractDataInceput, parceleAtribuiriContracte.contractGospodarieIdPrimeste, parceleAtribuiriContracte.contractGospodarieIdDa, parceleAtribuiriContracte.contractDetalii, parceleModUtilizare.parcelaId FROM         parceleModUtilizare INNER JOIN parceleAtribuiriContracte ON parceleModUtilizare.contractId = parceleAtribuiriContracte.contractId AND parceleAtribuiriContracte.contractActiv = 1 WHERE  (parceleModUtilizare.contractId='" + ((Label)gvModUtilizareContracte.SelectedRow.FindControl("lblGvContracteId")).Text + "')", vCampuri, Convert.ToInt16(Session["SESan"]));
        //   gvListaParcele.DataBind();
        return vValori;
    }
    protected void RefacemLista()
    {
        // luam datele din contract si le si salvam in alte variabile
        List<string> vCampuri = new List<string> { "contractNr", "contractDataSemnarii", "contractTip", "modUtilizareId", "gospodarieId", "catreGospodarieId", "contractDataInceput", "contractDataFinal", "contractDetalii", "c3Ha", "c3Ari", "anul", "catreC3Rand", "contractId", "contractGospodarieIdPrimeste", "contractGospodarieIdDa", "parcelaId" };
        List<List<string>> vValori = ManipuleazaBD.fRezultaListaStringuri("SELECT     parceleAtribuiriContracte.contractNr, parceleAtribuiriContracte.contractDataSemnarii, parceleAtribuiriContracte.contractTip, parceleModUtilizare.modUtilizareId, parceleModUtilizare.c3Rand, parceleModUtilizare.c3Ha, parceleModUtilizare.c3Ari, parceleModUtilizare.anul, parceleModUtilizare.gospodarieId, parceleModUtilizare.catreGospodarieId, parceleModUtilizare.catreC3Rand, parceleModUtilizare.contractId, parceleAtribuiriContracte.contractDataFinal, parceleAtribuiriContracte.contractDataInceput, parceleAtribuiriContracte.contractGospodarieIdPrimeste, parceleAtribuiriContracte.contractGospodarieIdDa, parceleAtribuiriContracte.contractDetalii, parceleModUtilizare.parcelaId FROM         parceleModUtilizare INNER JOIN parceleAtribuiriContracte ON parceleModUtilizare.contractId = parceleAtribuiriContracte.contractId AND parceleAtribuiriContracte.contractActiv = 1 WHERE  (parceleModUtilizare.contractId='" + ((Label)gvModUtilizareContracte.SelectedRow.FindControl("lblGvContracteId")).Text + "')", vCampuri, Convert.ToInt16(Session["SESan"]));
        gvListaParcele.DataBind();
        // scoatem o variabila cu nr de parcele

        // in functie de parcelele din contract le bifam, iar restul le debifam
        decimal vAriAlocati = 0;
        decimal vAriTotal = 0;
        for (int i = 0; i < gvListaParcele.Rows.Count; i++)
        {
            if (gvListaParcele.Rows[i].RowType == DataControlRowType.DataRow)
            {
                ((TextBox)gvListaParcele.Rows[i].FindControl("tbAlocaHa")).Enabled = false;
                ((TextBox)gvListaParcele.Rows[i].FindControl("tbAlocaAri")).Enabled = false;
                //       ((CheckBox)gvListaParcele.Rows[i].FindControl("ckbParcelaFaceObiectul")).Enabled = false;
                ((CheckBox)gvListaParcele.Rows[i].FindControl("ckbParcelaFaceObiectul")).Checked = false;
            }
        }
        for (int i = 0; i < gvListaParcele.Rows.Count; i++)
        {
            if (gvListaParcele.Rows[i].RowType == DataControlRowType.DataRow)
            {
                try
                {
                    vAriTotal = Convert.ToDecimal(((Label)gvListaParcele.Rows[i].FindControl("lblHaTotal")).Text) * 100 + Convert.ToDecimal(((Label)gvListaParcele.Rows[i].FindControl("lblAriTotal")).Text);
                    vAriAlocati = Convert.ToDecimal(((Label)gvListaParcele.Rows[i].FindControl("lblHaMaxim")).Text) * 100 + Convert.ToDecimal(((Label)gvListaParcele.Rows[i].FindControl("lblAriMaxim")).Text);

                    string[] vDisponibil = CalculeazaAriHa("0", "0", Convert.ToString(vAriTotal - vAriAlocati), "0");
                    if ((vAriTotal - vAriAlocati) > 0)
                    {
                        ((CheckBox)gvListaParcele.Rows[i].FindControl("ckbParcelaFaceObiectul")).Enabled = true;
                    }

                    // daca e una din parcele bifam si facem tb enabled
                    // daca nu, facem unchecked si tb disabled
                    for (int x = 0; x < vValori.Count(); x++)
                    {
                        if (((Label)gvListaParcele.Rows[i].FindControl("lblParcelaId")).Text == vValori[x][16])
                        {
                            ((TextBox)gvListaParcele.Rows[i].FindControl("tbAlocaHa")).Text = vValori[x][9];
                            ((TextBox)gvListaParcele.Rows[i].FindControl("tbAlocaAri")).Text = vValori[x][10];
                            ((CheckBox)gvListaParcele.Rows[i].FindControl("ckbParcelaFaceObiectul")).Checked = true;
                            ((CheckBox)gvListaParcele.Rows[i].FindControl("ckbParcelaFaceObiectul")).Enabled = true;
                            ((TextBox)gvListaParcele.Rows[i].FindControl("tbAlocaHa")).Enabled = true;
                            ((TextBox)gvListaParcele.Rows[i].FindControl("tbAlocaAri")).Enabled = true;
                            ((Label)gvListaParcele.Rows[i].FindControl("lblHaMaxim")).Text = (Convert.ToDecimal(((Label)gvListaParcele.Rows[i].FindControl("lblHaMaxim")).Text) + Convert.ToDecimal(((TextBox)gvListaParcele.Rows[i].FindControl("tbAlocaHa")).Text)).ToString();
                            ((Label)gvListaParcele.Rows[i].FindControl("lblAriMaxim")).Text = (Convert.ToDecimal(((Label)gvListaParcele.Rows[i].FindControl("lblAriMaxim")).Text) + Convert.ToDecimal(((TextBox)gvListaParcele.Rows[i].FindControl("tbAlocaAri")).Text)).ToString();
                        }
                    }
                    if (((Label)gvListaParcele.Rows[i].FindControl("lblHaMaxim")).Text == "0" && ((Label)gvListaParcele.Rows[i].FindControl("lblAriMaxim")).Text == "0,0000")
                    {
                        ((CheckBox)gvListaParcele.Rows[i].FindControl("ckbParcelaFaceObiectul")).Enabled = false;
                    }
                }
                catch { }
            }
        }
    }
    protected void ddlModUtilizareCatreNrPozitieContract_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void SqlModUtilizareCatreVolumContract_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gvModUtilizareContracte_DataBound(object sender, EventArgs e)
    {
        // cautam numele capului de gospodarie
        for (int i = 0; i < gvModUtilizareContracte.Rows.Count; i++)
        {
            if (gvModUtilizareContracte.Rows[i].RowType == DataControlRowType.DataRow)
            {
                try
                {
                    ((Label)gvModUtilizareContracte.Rows[i].FindControl("lblNumeDa")).Text = GospodariaCurentaDetalii(((Label)gvModUtilizareContracte.Rows[i].FindControl("lblIdDa")).Text);
                    ((Label)gvModUtilizareContracte.Rows[i].FindControl("lblNumePrimeste")).Text = GospodariaCurentaDetalii(((Label)gvModUtilizareContracte.Rows[i].FindControl("lblIdPrimeste")).Text);
                    // scriem expirat sau nu 
                    if (Convert.ToDateTime(((Label)gvModUtilizareContracte.Rows[i].FindControl("lblDataFinal")).Text) < DateTime.Now.AddDays(-1))
                        ((Label)gvModUtilizareContracte.Rows[i].FindControl("lblExpirat")).Visible = true;
                    else ((Label)gvModUtilizareContracte.Rows[i].FindControl("lblExpirat")).Visible = false;
                    if (Convert.ToDateTime(((Label)gvModUtilizareContracte.Rows[i].FindControl("lblDataInceput")).Text) > DateTime.Now)
                        ((Label)gvModUtilizareContracte.Rows[i].FindControl("lblNeinceput")).Visible = true;
                    else if (Convert.ToDateTime(((Label)gvModUtilizareContracte.Rows[i].FindControl("lblDataFinal")).Text) > DateTime.Now.AddDays(-1))
                    {
                        ((Label)gvModUtilizareContracte.Rows[i].FindControl("lblNeinceput")).Visible = false;
                        ((Label)gvModUtilizareContracte.Rows[i].FindControl("lblInCurs")).Visible = true;
                    }
                    else
                    {
                        ((Label)gvModUtilizareContracte.Rows[i].FindControl("lblNeinceput")).Visible = false;

                    }
                }
                catch { }
            }
        }
    }
    protected void gvModUtilizareContracte_PreRender(object sender, EventArgs e)
    {

    }
    protected void gvModUtilizareContracte_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(gvModUtilizareContracte, e, this);
        if (e.Row.RowType == DataControlRowType.Header)
        {
            ViewState["ha"] = 0;
            ViewState["ari"] = 0;
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label vLblHa = (Label)e.Row.FindControl("lblHa");
            Label vLblAri = (Label)e.Row.FindControl("lblAri");
            string ha = vLblHa.Text.Remove(vLblHa.Text.IndexOf("/"));
            string ari = vLblHa.Text.Remove(vLblHa.Text.IndexOf("/"));
            ViewState["ha"] = Convert.ToDecimal(ViewState["ha"].ToString()) + Convert.ToDecimal(ha.Replace(".",","));
            ViewState["ari"] = Convert.ToDecimal(ViewState["ari"].ToString()) + Convert.ToDecimal(ari.Replace(".", ","));
        }
        else if (e.Row.RowType == DataControlRowType.Footer)
        {
            Label vLblHaT = (Label)e.Row.FindControl("lblHaT");
            Label vLblAriT = (Label)e.Row.FindControl("lblAriT");
            decimal vHa = Convert.ToDecimal(ViewState["ha"].ToString()), vAri = Convert.ToDecimal(ViewState["ari"].ToString());
            vHa = vHa + Math.Truncate(vAri / 100);
            vAri -= Math.Truncate(vAri / 100) * 100;
            vLblHaT.Text = vHa.ToString();
            vLblAriT.Text = vAri.ToString();
        }
    }
    protected void gvModUtilizareContracte_SelectedIndexChanged(object sender, EventArgs e)
    {
        btStergeContracte.Visible = true;
        btAdaugaContracte.Visible = true;
        btModificaContracte.Visible = true;

        //  pnlContracteDetalii.Visible = true;
    }
    protected string[] TipAtribuire()
    {
        string[] vTip = { "", "" };
        switch (ddlModUtilizareContract.SelectedValue)
        {
            case "3":
                vTip[0] = "3";
                vTip[1] = "10";
                break;
            case "4":
                vTip[0] = "4";
                vTip[1] = "11";
                break;
            case "5":
                vTip[0] = "5";
                vTip[1] = "12";
                break;
            case "6":
                vTip[0] = "6";
                vTip[1] = "13";
                break;
            case "7":
                vTip[0] = "7";
                vTip[1] = "14";
                break;
            case "8":
                vTip[0] = "8";
                vTip[1] = "15";
                break;
            case "10":
                vTip[0] = "3";
                vTip[1] = "10";
                break;
            case "11":
                vTip[0] = "4";
                vTip[1] = "11";
                break;
            case "12":
                vTip[0] = "5";
                vTip[1] = "12";
                break;
            case "13":
                vTip[0] = "6";
                vTip[1] = "13";
                break;
            case "14":
                vTip[0] = "7";
                vTip[1] = "14";
                break;
            case "15":
                vTip[0] = "8";
                vTip[1] = "15";
                break;
        }
        return vTip;
    }
    protected void SalveazaAdauga(string pDa, string pPrimeste, string pContractId, string pTip)
    {
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();

        /***********************************
         * 
         * ADAUGARE
         * 
         * **********************************/
        // daca e prima atribuire pentru gospodarie stergem tot din 3, mai putin primul rand , pe care il modificam la adaugarea de parcele
        string vExistaModUtilizare = ManipuleazaBD.fRezultaUnString("SELECT COUNT(*) as numar3 FROM parceleModUtilizare WHERE (gospodarieId='" + pDa + "' OR catreGospodarieId='" + pDa + "') AND anul = '" + Session["SESan"].ToString() + "'", "numar3", Convert.ToInt16(Session["SESan"]));
        if (vExistaModUtilizare == "0")
        {
            // facem update = 0 din cap3 toate inregistrarile pt gospodarie
            ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '0', col2 = '0' WHERE (gospodarieId = '" + pDa + "')  AND (an = '" + Session["SESan"].ToString() + "') AND (codCapitol = '3') AND (codRand <> '1') ", Convert.ToInt16(Session["SESan"]));
        }
        // daca e prima atribuire pentru catreGospodarie stergem tot din 3
        vExistaModUtilizare = ManipuleazaBD.fRezultaUnString("SELECT COUNT(*) as numar3 FROM parceleModUtilizare WHERE (gospodarieId='" + pPrimeste + "' OR catreGospodarieId='" + pPrimeste + "') AND anul = '" + Session["SESan"].ToString() + "'", "numar3", Convert.ToInt16(Session["SESan"]));
        if (vExistaModUtilizare == "0")
        {
            // facem update = 0 din cap3 toate inregistrarile pt gospodarie
            ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '0', col2 = '0' WHERE (gospodarieId = '" + pPrimeste + "')  AND (an = '" + Session["SESan"].ToString() + "') AND (codCapitol = '3') AND (codRand <> '1')", Convert.ToInt16(Session["SESan"]));
        }
        // daca e adaugare doar adaugam valorile la ce gasim deja in 3
        // parcurgem tot gridul si luam unde valorile tb nu sunt nule
        if (ViewState["contractMod"].ToString() == "a")
        {
            int vContor = 0;

            for (int i = 0; i < gvListaParcele.Rows.Count; i++)
            {
                if (gvListaParcele.Rows[i].RowType == DataControlRowType.DataRow && ((CheckBox)gvListaParcele.Rows[i].FindControl("ckbParcelaFaceObiectul")).Checked == true)
                {
                    vContor++;
                    TextBox[] vControl = new TextBox[] { (TextBox)gvListaParcele.Rows[i].FindControl("tbAlocaHa"), (TextBox)gvListaParcele.Rows[i].FindControl("tbAlocaAri") };
                    for (int j = 0; j < 2; j++)
                    {
                        try
                        {
                            if (!Valideaza.ENumeric(vControl[j].Text.Replace(',', '.')) ||
            (Valideaza.ENumeric(vControl[j].Text.Replace(',', '.')) && Convert.ToDecimal(vControl[j].Text.Replace(',', '.')) < 0))
                            {
                                vControl[j].Text = "0";
                            }
                        }
                        catch { }
                    }
                    // calculam ari total introdusi in tb, daca nu e nul facem inserturi
                    decimal vAriIntrodusi = Convert.ToDecimal(vControl[0].Text.Replace('.', ',')) * 100 + Convert.ToDecimal(vControl[1].Text.Replace('.', ','));
                    if (vAriIntrodusi > 0)
                    {
                        ManipuleazaBD.fManipuleazaBD("INSERT INTO parceleModUtilizare (gospodarieId, catreGospodarieId, anul, unitateId, parcelaId, c3Rand, catreC3Rand, c3Ha, c3Ari, contractId) VALUES ('" + pDa + "','" + pPrimeste + "','" + Session["SESan"].ToString() + "', '" + Session["SESunitateId"].ToString() + "','" + ((Label)gvListaParcele.Rows[i].FindControl("lblParcelaId")).Text + "','" + TipAtribuire()[1] + "','" + TipAtribuire()[0] + "', '" + vControl[0].Text.Replace(',', '.') + "', '" + vControl[1].Text.Replace(',', '.') + "', '" + pContractId + "')", Convert.ToInt16(Session["SESan"]));
                        // verificam sa nu fie gol capitolul 3; daca e gol se insereaza si astfel putem face update fara erori
                        CalculCapitole.PopuleazaTabelaCapitole(Convert.ToInt32(Session["SESan"]), Convert.ToInt64(pDa), Convert.ToInt64(Session["SESunitateId"]), "3");

                        // gasim linia de capitol pentru randul care se va updata pentru cel care "dă"
                        // 1. pentru randul respectiv, de la 10 la 15
                        List<string> vCampuri = new List<string> { "capitolId", "col1", "col2" };
                        List<string> vValori = ManipuleazaBD.fRezultaUnString("SELECT capitole.capitolId, capitole.col1, capitole.col2 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + pDa + "') AND (sabloaneCapitole.capitol = '3') AND (capitole.codRand = '" + TipAtribuire()[1] + "')", vCampuri, Convert.ToInt16(Session["SESan"]));
                        // calculam arii totali existenti + introdusi
                        string vScrieHa = CalculeazaAriHa("0", vValori[1], vAriIntrodusi.ToString(), vValori[2])[0].Replace(',', '.');
                        string vScrieAri = CalculeazaAriHa("0", vValori[1], vAriIntrodusi.ToString(), vValori[2])[1].Replace(',', '.');

                        // scriem in 3; adunam la ce exista valoarea din textbox-uri
                        ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vScrieHa + "', col2 = '" + vScrieAri + "' WHERE (capitolId = '" + vValori[0] + "')", Convert.ToInt16(Session["SESan"]));
                        // 2. adaugam si la 9 si -17 (totaluri) - suprafete date
                        vValori = ManipuleazaBD.fRezultaUnString("SELECT capitole.capitolId, capitole.col1, capitole.col2 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + pDa + "') AND (sabloaneCapitole.capitol = '3') AND (capitole.codRand = '9')", vCampuri, Convert.ToInt16(Session["SESan"]));
                        vScrieHa = CalculeazaAriHa("0", vValori[1], vAriIntrodusi.ToString(), vValori[2])[0].Replace(',', '.');
                        vScrieAri = CalculeazaAriHa("0", vValori[1], vAriIntrodusi.ToString(), vValori[2])[1].Replace(',', '.');
                        // scriem in 3; adunam la ce exista valoarea din textbox-uri randul 9
                        ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vScrieHa + "', col2 = '" + vScrieAri + "' WHERE (capitolId = '" + vValori[0] + "')", Convert.ToInt16(Session["SESan"]));

                        // scadem din 17 --- suprafata utilizata, la care adaugam randul 1 suprafata in proprietate
                        // facem direct suma 1+2-9
                        string vValori129 = "ari";
                        decimal vValoriRanduri129 = Convert.ToDecimal(ManipuleazaBD.fRezultaUnString("SELECT     SUM(CASE capitole.codRand WHEN '9' THEN (- CONVERT(decimal(10,5), capitole.col1) * 100 - CONVERT(decimal(10,5), capitole.col2)) ELSE (CONVERT(decimal(10,5), capitole.col1) * 100 + CONVERT(decimal(10,5), capitole.col2)) END) AS ari FROM         capitole INNER JOIN   sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + pDa + "') AND (sabloaneCapitole.capitol = '3')  AND (capitole.codRand = '1' OR    capitole.codRand = '9' OR capitole.codRand = '2')", vValori129, Convert.ToInt16(Session["SESan"])).Replace('.', ','));
                        // nu ne mai intereseazace exista, scriem direct suma, folosim doar valoarea cod capitol [0]
                        vValori = ManipuleazaBD.fRezultaUnString("SELECT capitole.capitolId, capitole.col1, capitole.col2 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + pDa + "') AND (sabloaneCapitole.capitol = '3') AND (capitole.codRand = '17')", vCampuri, Convert.ToInt16(Session["SESan"]));
                        vScrieHa = CalculeazaAriHa("0", "0", (vValoriRanduri129).ToString(), "0")[0].Replace(',', '.');
                        vScrieAri = CalculeazaAriHa("0", "0", (vValoriRanduri129).ToString(), "0")[1].Replace(',', '.');
                        // scriem in 3; adunam la ce exista valoarea din textbox-uri randul 9
                        ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vScrieHa + "', col2 = '" + vScrieAri + "' WHERE (capitolId = '" + vValori[0] + "')", Convert.ToInt16(Session["SESan"]));

                        // verificam daca cel ce primeste e persoana juridica; 
                        // daca este atunci adaugam si pt randul 16 la cel ce da
                        if (PersoanaJuridica(pPrimeste))
                        {
                            // gasim linia de capitol pentru randul 16 care se va updata pentru cel care "dă"
                            vCampuri = new List<string> { "capitolId", "col1", "col2" };
                            vValori = ManipuleazaBD.fRezultaUnString("SELECT capitole.capitolId, capitole.col1, capitole.col2 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + pDa + "') AND (sabloaneCapitole.capitol = '3') AND (capitole.codRand = '16')", vCampuri, Convert.ToInt16(Session["SESan"]));
                            vScrieHa = CalculeazaAriHa("0", vValori[1], (vAriIntrodusi).ToString(), vValori[2])[0].Replace(',', '.');
                            vScrieAri = CalculeazaAriHa("0", vValori[1], (vAriIntrodusi).ToString(), vValori[2])[1].Replace(',', '.');
                            // scriem in 3; adunam la ce exista valoarea din textbox-uri
                            ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vScrieHa + "', col2 = '" + vScrieAri + "' WHERE (capitolId = '" + vValori[0] + "')", Convert.ToInt16(Session["SESan"]));
                        }
                        // verificam sa nu fie gol capitolul 3; daca e gol se insereaza si astfel putem face update fara erori - pt "primeste"
                        CalculCapitole.PopuleazaTabelaCapitole(Convert.ToInt32(Session["SESan"]), Convert.ToInt64(pPrimeste), Convert.ToInt64(Session["SESunitateId"]), "3");
                        // gasim linia de capitol pentru randul care se va updata pentru cel care "primeste"
                        // 2. pentru randul respectiv, de la 3 la 8
                        vCampuri = new List<string> { "capitolId", "col1", "col2" };
                        vValori = ManipuleazaBD.fRezultaUnString("SELECT capitole.capitolId, capitole.col1, capitole.col2 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + pPrimeste + "') AND (sabloaneCapitole.capitol = '3') AND (capitole.codRand = '" + TipAtribuire()[0] + "')", vCampuri, Convert.ToInt16(Session["SESan"]));
                        vScrieHa = CalculeazaAriHa("0", vValori[1], (vAriIntrodusi).ToString(), vValori[2])[0].Replace(',', '.');
                        vScrieAri = CalculeazaAriHa("0", vValori[1], (vAriIntrodusi).ToString(), vValori[2])[1].Replace(',', '.');
                        // scriem in 3; adunam la ce exista valoarea din textbox-uri
                        ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vScrieHa + "', col2 = '" + vScrieAri + "' WHERE (capitolId = '" + vValori[0] + "')", Convert.ToInt16(Session["SESan"]));
                        // 2. adaugam si la 2 si +17 (totaluri) - suprafete primite
                        vValori = ManipuleazaBD.fRezultaUnString("SELECT capitole.capitolId, capitole.col1, capitole.col2 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + pPrimeste + "') AND (sabloaneCapitole.capitol = '3') AND (capitole.codRand = '2')", vCampuri, Convert.ToInt16(Session["SESan"]));
                        vScrieHa = CalculeazaAriHa("0", vValori[1], (vAriIntrodusi).ToString(), vValori[2])[0].Replace(',', '.');
                        vScrieAri = CalculeazaAriHa("0", vValori[1], (vAriIntrodusi).ToString(), vValori[2])[1].Replace(',', '.');
                        // scriem in 3; adunam la ce exista valoarea din textbox-uri
                        ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vScrieHa + "', col2 = '" + vScrieAri + "' WHERE (capitolId = '" + vValori[0] + "')", Convert.ToInt16(Session["SESan"]));
                        vValori = ManipuleazaBD.fRezultaUnString("SELECT capitole.capitolId, capitole.col1, capitole.col2 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + pPrimeste + "') AND (sabloaneCapitole.capitol = '3') AND (capitole.codRand = '17')", vCampuri, Convert.ToInt16(Session["SESan"]));
                        vScrieHa = CalculeazaAriHa("0", vValori[1], (vAriIntrodusi).ToString(), vValori[2])[0].Replace(',', '.');
                        vScrieAri = CalculeazaAriHa("0", vValori[1], (vAriIntrodusi).ToString(), vValori[2])[1].Replace(',', '.');
                        // scriem in 3; adunam la ce exista valoarea din textbox-uri
                        ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vScrieHa + "', col2 = '" + vScrieAri + "' WHERE (capitolId = '" + vValori[0] + "')", Convert.ToInt16(Session["SESan"]));


                        ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "atribuire mod utilizare --> cap.3", "parcela " + gvListaParcele.Rows[i].Cells[1].Text + "(id: " + ((Label)gvListaParcele.Rows[i].FindControl("lblParcelaId")).Text + ")", "", Convert.ToInt64(Session["SESgospodarieId"]), 2);
                        ClassLog.UpdateDataUltimeiModificari(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(pDa));
                        ClassLog.UpdateDataUltimeiModificari(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(pPrimeste));
                    }
                    else
                    {
                        vContor = 0;
                    }
                }
            }
            if (vContor == 0)
            {
                valCustom.IsValid = false;
                valCustom.ErrorMessage = "Suprafaţa nu poate fi zero!";
                return;
            }
            pnContracteUtilizare.Visible = false;
            pnlListaContracte.Visible = true;
            btAdaugaContracte.Visible = true;
        }
        /***********************************************************************************************/
        else if (ViewState["contractMod"].ToString() == "m")
        {
            //**************  ADAUGAM NOILE MODIFICARI  *****************************//
            // luam valorile existente pentru a le scadea din 3
            List<List<string>> vValoriModificare = ValoriModificare();

            int vContor = 0;
            for (int i = 0; i < gvListaParcele.Rows.Count; i++)
            {

                if (gvListaParcele.Rows[i].RowType == DataControlRowType.DataRow && ((CheckBox)gvListaParcele.Rows[i].FindControl("ckbParcelaFaceObiectul")).Checked == true)
                {
                    vContor++;
                    TextBox[] vControl = new TextBox[] { (TextBox)gvListaParcele.Rows[i].FindControl("tbAlocaHa"), (TextBox)gvListaParcele.Rows[i].FindControl("tbAlocaAri") };
                    for (int j = 0; j < 2; j++)
                    {
                        try
                        {
                            if (!Valideaza.ENumeric(vControl[j].Text.Replace(',', '.')) ||
            (Valideaza.ENumeric(vControl[j].Text.Replace('.', ',')) && Convert.ToDecimal(vControl[j].Text.Replace('.', ',')) < 0))
                            {
                                vControl[j].Text = "0";
                            }
                        }
                        catch { }
                    }
                    // calculam ari total introdusi in tb, daca nu e nul facem inserturi
                    decimal vAriIntrodusi = Convert.ToDecimal(vControl[0].Text.Replace('.', ',')) * 100 + Convert.ToDecimal(vControl[1].Text.Replace('.', ','));
                    if (vAriIntrodusi > 0)
                    {
                        ManipuleazaBD.fManipuleazaBD("INSERT INTO parceleModUtilizare (gospodarieId, catreGospodarieId, anul, unitateId, parcelaId, c3Rand, catreC3Rand, c3Ha, c3Ari, contractId) VALUES ('" + pDa + "','" + pPrimeste + "','" + Session["SESan"].ToString() + "', '" + Session["SESunitateId"].ToString() + "','" + ((Label)gvListaParcele.Rows[i].FindControl("lblParcelaId")).Text + "','" + TipAtribuire()[1] + "','" + TipAtribuire()[0] + "', '" + vControl[0].Text.Replace(',', '.') + "', '" + vControl[1].Text.Replace(',', '.') + "', '" + vValoriModificare[0][13] + "')", Convert.ToInt16(Session["SESan"]));
                        // verificam sa nu fie gol capitolul 3; daca e gol se insereaza si astfel putem face update fara erori
                        CalculCapitole.PopuleazaTabelaCapitole(Convert.ToInt32(Session["SESan"]), Convert.ToInt64(pDa), Convert.ToInt64(Session["SESunitateId"]), "3");

                        // gasim linia de capitol pentru randul care se va updata pentru cel care "dă"
                        // 1. pentru randul respectiv, de la 10 la 15
                        List<string> vCampuri = new List<string> { "capitolId", "col1", "col2" };
                        List<string> vValori = ManipuleazaBD.fRezultaUnString("SELECT capitole.capitolId, capitole.col1, capitole.col2 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + pDa + "') AND (sabloaneCapitole.capitol = '3') AND (capitole.codRand = '" + TipAtribuire()[1] + "')", vCampuri, Convert.ToInt16(Session["SESan"]));
                        // calculam arii totali existenti + introdusi
                        string vScrieHa = CalculeazaAriHa("0", vValori[1], vAriIntrodusi.ToString(), vValori[2])[0].Replace(',', '.');
                        string vScrieAri = CalculeazaAriHa("0", vValori[1], vAriIntrodusi.ToString(), vValori[2])[1].Replace(',', '.');

                        // scriem in 3; adunam la ce exista valoarea din textbox-uri
                        ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vScrieHa + "', col2 = '" + vScrieAri + "' WHERE (capitolId = '" + vValori[0] + "')", Convert.ToInt16(Session["SESan"]));
                        // 2. adaugam si la 9 si -17 (totaluri) - suprafete date
                        vValori = ManipuleazaBD.fRezultaUnString("SELECT capitole.capitolId, capitole.col1, capitole.col2 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + pDa + "') AND (sabloaneCapitole.capitol = '3') AND (capitole.codRand = '9')", vCampuri, Convert.ToInt16(Session["SESan"]));
                        vScrieHa = CalculeazaAriHa("0", vValori[1], vAriIntrodusi.ToString(), vValori[2])[0].Replace(',', '.');
                        vScrieAri = CalculeazaAriHa("0", vValori[1], vAriIntrodusi.ToString(), vValori[2])[1].Replace(',', '.');
                        // scriem in 3; adunam la ce exista valoarea din textbox-uri randul 9
                        ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vScrieHa + "', col2 = '" + vScrieAri + "' WHERE (capitolId = '" + vValori[0] + "')", Convert.ToInt16(Session["SESan"]));

                        // scadem din 17 --- suprafata utilizata, la care adaugam randul 1 suprafata in proprietate
                        // facem direct suma 1+2-9
                        string vValori129 = "ari";
                        decimal vValoriRanduri129 = Convert.ToDecimal(ManipuleazaBD.fRezultaUnString("SELECT     SUM(CASE capitole.codRand WHEN '9' THEN (- CONVERT(decimal(10,5), capitole.col1) * 100 - CONVERT(decimal(10,5), capitole.col2)) ELSE (CONVERT(decimal(10,5), capitole.col1) * 100 + CONVERT(decimal(10,5), capitole.col2)) END) AS ari FROM         capitole INNER JOIN   sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + pDa + "') AND (sabloaneCapitole.capitol = '3')  AND (capitole.codRand = '1' OR    capitole.codRand = '9' OR capitole.codRand = '2')", vValori129, Convert.ToInt16(Session["SESan"])).Replace('.', ','));
                        // nu ne mai intereseazace exista, scriem direct suma, folosim doar valoarea cod capitol [0]
                        vValori = ManipuleazaBD.fRezultaUnString("SELECT capitole.capitolId, capitole.col1, capitole.col2 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + pDa + "') AND (sabloaneCapitole.capitol = '3') AND (capitole.codRand = '17')", vCampuri, Convert.ToInt16(Session["SESan"]));
                        vScrieHa = CalculeazaAriHa("0", "0", (vValoriRanduri129).ToString(), "0")[0].Replace(',', '.');
                        vScrieAri = CalculeazaAriHa("0", "0", (vValoriRanduri129).ToString(), "0")[1].Replace(',', '.');
                        // scriem in 3; adunam la ce exista valoarea din textbox-uri randul 9
                        ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vScrieHa + "', col2 = '" + vScrieAri + "' WHERE (capitolId = '" + vValori[0] + "')", Convert.ToInt16(Session["SESan"]));

                        // verificam daca cel ce primeste e persoana juridica; 
                        // daca este atunci adaugam si pt randul 16 la cel ce da
                        if (PersoanaJuridica(pPrimeste))
                        {
                            // gasim linia de capitol pentru randul 16 care se va updata pentru cel care "dă"
                            vCampuri = new List<string> { "capitolId", "col1", "col2" };
                            vValori = ManipuleazaBD.fRezultaUnString("SELECT capitole.capitolId, capitole.col1, capitole.col2 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + pDa + "') AND (sabloaneCapitole.capitol = '3') AND (capitole.codRand = '16')", vCampuri, Convert.ToInt16(Session["SESan"]));
                            vScrieHa = CalculeazaAriHa("0", vValori[1], (vAriIntrodusi).ToString(), vValori[2])[0].Replace(',', '.');
                            vScrieAri = CalculeazaAriHa("0", vValori[1], (vAriIntrodusi).ToString(), vValori[2])[1].Replace(',', '.');
                            // scriem in 3; adunam la ce exista valoarea din textbox-uri
                            ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vScrieHa + "', col2 = '" + vScrieAri + "' WHERE (capitolId = '" + vValori[0] + "')", Convert.ToInt16(Session["SESan"]));
                        }
                        // verificam sa nu fie gol capitolul 3; daca e gol se insereaza si astfel putem face update fara erori - pt "primeste"
                        CalculCapitole.PopuleazaTabelaCapitole(Convert.ToInt32(Session["SESan"]), Convert.ToInt64(pPrimeste), Convert.ToInt64(Session["SESunitateId"]), "3");
                        // gasim linia de capitol pentru randul care se va updata pentru cel care "primeste"
                        // 2. pentru randul respectiv, de la 3 la 8
                        vCampuri = new List<string> { "capitolId", "col1", "col2" };
                        vValori = ManipuleazaBD.fRezultaUnString("SELECT capitole.capitolId, capitole.col1, capitole.col2 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + pPrimeste + "') AND (sabloaneCapitole.capitol = '3') AND (capitole.codRand = '" + TipAtribuire()[0] + "')", vCampuri, Convert.ToInt16(Session["SESan"]));
                        vScrieHa = CalculeazaAriHa("0", vValori[1], (vAriIntrodusi).ToString(), vValori[2])[0].Replace(',', '.');
                        vScrieAri = CalculeazaAriHa("0", vValori[1], (vAriIntrodusi).ToString(), vValori[2])[1].Replace(',', '.');
                        // scriem in 3; adunam la ce exista valoarea din textbox-uri
                        ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vScrieHa + "', col2 = '" + vScrieAri + "' WHERE (capitolId = '" + vValori[0] + "')", Convert.ToInt16(Session["SESan"]));
                        // 2. adaugam si la 2 si +17 (totaluri) - suprafete primite
                        vValori = ManipuleazaBD.fRezultaUnString("SELECT capitole.capitolId, capitole.col1, capitole.col2 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + pPrimeste + "') AND (sabloaneCapitole.capitol = '3') AND (capitole.codRand = '2')", vCampuri, Convert.ToInt16(Session["SESan"]));
                        vScrieHa = CalculeazaAriHa("0", vValori[1], (vAriIntrodusi).ToString(), vValori[2])[0].Replace(',', '.');
                        vScrieAri = CalculeazaAriHa("0", vValori[1], (vAriIntrodusi).ToString(), vValori[2])[1].Replace(',', '.');
                        // scriem in 3; adunam la ce exista valoarea din textbox-uri
                        ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vScrieHa + "', col2 = '" + vScrieAri + "' WHERE (capitolId = '" + vValori[0] + "')", Convert.ToInt16(Session["SESan"]));
                        vValori = ManipuleazaBD.fRezultaUnString("SELECT capitole.capitolId, capitole.col1, capitole.col2 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + pPrimeste + "') AND (sabloaneCapitole.capitol = '3') AND (capitole.codRand = '17')", vCampuri, Convert.ToInt16(Session["SESan"]));
                        vScrieHa = CalculeazaAriHa("0", vValori[1], (vAriIntrodusi).ToString(), vValori[2])[0].Replace(',', '.');
                        vScrieAri = CalculeazaAriHa("0", vValori[1], (vAriIntrodusi).ToString(), vValori[2])[1].Replace(',', '.');
                        // scriem in 3; adunam la ce exista valoarea din textbox-uri
                        ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vScrieHa + "', col2 = '" + vScrieAri + "' WHERE (capitolId = '" + vValori[0] + "')", Convert.ToInt16(Session["SESan"]));


                        ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "atribuire mod utilizare --> cap.3", "parcela " + gvListaParcele.Rows[i].Cells[1].Text + "(id: " + ((Label)gvListaParcele.Rows[i].FindControl("lblParcelaId")).Text + ")", "", Convert.ToInt64(Session["SESgospodarieId"]), 2);
                        ClassLog.UpdateDataUltimeiModificari(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(pDa));
                        ClassLog.UpdateDataUltimeiModificari(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(pPrimeste));
                    }
                    else
                    {
                        vContor = 0;
                    }
                }

            }
            if (vContor == 0)
            {
                valCustom.IsValid = false;
                valCustom.ErrorMessage = "Suprafaţa nu poate fi zero!";
                return;
            }

            //**************  SCADEM  *****************************//

            for (int x = 0; x < vValoriModificare.Count(); x++)
            {
                //   List<string> vCampuri = new List<string> { "contractNr", "contractDataSemnarii", "contractTip", "modUtilizareId", "gospodarieId", "catreGospodarieId", "contractDataInceput", "contractDataFinal", "contractDetalii", "c3Ha", "c3Ari", "anul", "catreC3Rand", "contractId", "contractGospodarieIdPrimeste", "contractGospodarieIdDa", "parcelaId" , "modUtilizareId"};
                // stergem valorile din parcelemodutilizare
                ManipuleazaBD.fManipuleazaBD("DELETE parceleModUtilizare WHERE modUtilizareId = '" + vValoriModificare[x][17] + "'", Convert.ToInt16(Session["SESan"]));

                // verificam sa nu fie gol capitolul 3; daca e gol se insereaza si astfel putem face update fara erori
                CalculCapitole.PopuleazaTabelaCapitole(Convert.ToInt32(Session["SESan"]), Convert.ToInt64(vValoriModificare[x][4]), Convert.ToInt64(Session["SESunitateId"]), "3");

                // gasim linia de capitol pentru randul care se va updata pentru cel care "dă"
                // 1. pentru randul respectiv, de la 10 la 15
                List<string> vCampuri = new List<string> { "capitolId", "col1", "col2" };
                List<string> vValori = ManipuleazaBD.fRezultaUnString("SELECT capitole.capitolId, capitole.col1, capitole.col2 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + vValoriModificare[x][4] + "') AND (sabloaneCapitole.capitol = '3') AND (capitole.codRand = '" + vValoriModificare[x][2] + "')", vCampuri, Convert.ToInt16(Session["SESan"]));
                // calculam arii totali existenti + introdusi
                string vScrieHa = CalculeazaAriHa((-Convert.ToDecimal(vValoriModificare[x][9])).ToString(), vValori[1], (-Convert.ToDecimal(vValoriModificare[x][10])).ToString(), vValori[2])[0].Replace(',', '.');
                string vScrieAri = CalculeazaAriHa((-Convert.ToDecimal(vValoriModificare[x][9])).ToString(), vValori[1], (-Convert.ToDecimal(vValoriModificare[x][10])).ToString(), vValori[2])[1].Replace(',', '.');
                if (Convert.ToDecimal(vScrieAri) < 0) vScrieAri = "0";
                if (Convert.ToDecimal(vScrieHa) < 0) vScrieHa = "0";

                // scriem in 3; adunam la ce exista valoarea din textbox-uri
                ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vScrieHa + "', col2 = '" + vScrieAri + "' WHERE (capitolId = '" + vValori[0] + "')", Convert.ToInt16(Session["SESan"]));
                // 2. adaugam si la 9 si -17 (totaluri) - suprafete date
                vValori = ManipuleazaBD.fRezultaUnString("SELECT capitole.capitolId, capitole.col1, capitole.col2 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + vValoriModificare[x][4] + "') AND (sabloaneCapitole.capitol = '3') AND (capitole.codRand = '9')", vCampuri, Convert.ToInt16(Session["SESan"]));
                vScrieHa = CalculeazaAriHa((-Convert.ToDecimal(vValoriModificare[x][9])).ToString(), vValori[1], (-Convert.ToDecimal(vValoriModificare[x][10])).ToString(), vValori[2])[0].Replace(',', '.');
                vScrieAri = CalculeazaAriHa((-Convert.ToDecimal(vValoriModificare[x][9])).ToString(), vValori[1], (-Convert.ToDecimal(vValoriModificare[x][10])).ToString(), vValori[2])[1].Replace(',', '.');
                if (Convert.ToDecimal(vScrieAri) < 0) vScrieAri = "0";
                if (Convert.ToDecimal(vScrieHa) < 0) vScrieHa = "0";
                // scriem in 3; adunam la ce exista valoarea din textbox-uri randul 9
                ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vScrieHa + "', col2 = '" + vScrieAri + "' WHERE (capitolId = '" + vValori[0] + "')", Convert.ToInt16(Session["SESan"]));

                // scadem din 17 --- suprafata utilizata, la care adaugam randul 1 suprafata in proprietate
                // facem direct suma 1+2-9
                string vValori129 = "ari";
                decimal vValoriRanduri129 = Convert.ToDecimal(ManipuleazaBD.fRezultaUnString("SELECT     SUM(CASE capitole.codRand WHEN '9' THEN (- CONVERT(decimal(10,5), capitole.col1) * 100 - CONVERT(decimal(10,5), capitole.col2)) ELSE (CONVERT(decimal(10,5), capitole.col1) * 100 + CONVERT(decimal(10,5), capitole.col2)) END) AS ari FROM         capitole INNER JOIN   sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + vValoriModificare[x][4] + "') AND (sabloaneCapitole.capitol = '3')  AND (capitole.codRand = '1' OR    capitole.codRand = '9' OR capitole.codRand = '2')", vValori129, Convert.ToInt16(Session["SESan"])).Replace('.', ','));
                // nu ne mai intereseazace exista, scriem direct suma, folosim doar valoarea cod capitol [0]
                vValori = ManipuleazaBD.fRezultaUnString("SELECT capitole.capitolId, capitole.col1, capitole.col2 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + vValoriModificare[x][4] + "') AND (sabloaneCapitole.capitol = '3') AND (capitole.codRand = '17')", vCampuri, Convert.ToInt16(Session["SESan"]));
                vScrieHa = CalculeazaAriHa("0", "0", (vValoriRanduri129).ToString(), "0")[0].Replace(',', '.');
                vScrieAri = CalculeazaAriHa("0", "0", (vValoriRanduri129).ToString(), "0")[1].Replace(',', '.');
                if (Convert.ToDecimal(vScrieAri) < 0) vScrieAri = "0";
                if (Convert.ToDecimal(vScrieHa) < 0) vScrieHa = "0";
                // scriem in 3; adunam la ce exista valoarea din textbox-uri randul 9
                ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vScrieHa + "', col2 = '" + vScrieAri + "' WHERE (capitolId = '" + vValori[0] + "')", Convert.ToInt16(Session["SESan"]));
                // verificam daca cel ce primeste e persoana juridica; 
                // daca este atunci adaugam si pt randul 16 la cel ce da
                if (PersoanaJuridica(vValoriModificare[x][5]))
                {
                    // gasim linia de capitol pentru randul 16 care se va updata pentru cel care "dă"
                    vCampuri = new List<string> { "capitolId", "col1", "col2" };
                    vValori = ManipuleazaBD.fRezultaUnString("SELECT capitole.capitolId, capitole.col1, capitole.col2 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + vValoriModificare[x][4] + "') AND (sabloaneCapitole.capitol = '3') AND (capitole.codRand = '16')", vCampuri, Convert.ToInt16(Session["SESan"]));
                    vScrieHa = CalculeazaAriHa((-Convert.ToDecimal(vValoriModificare[x][9])).ToString(), vValori[1], (-Convert.ToDecimal(vValoriModificare[x][10])).ToString(), vValori[2])[0].Replace(',', '.');
                    vScrieAri = CalculeazaAriHa((-Convert.ToDecimal(vValoriModificare[x][9])).ToString(), vValori[1], (-Convert.ToDecimal(vValoriModificare[x][10])).ToString(), vValori[2])[1].Replace(',', '.');
                    if (Convert.ToDecimal(vScrieAri) < 0) vScrieAri = "0";
                    if (Convert.ToDecimal(vScrieHa) < 0) vScrieHa = "0";
                    // scriem in 3; adunam la ce exista valoarea din textbox-uri
                    ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vScrieHa + "', col2 = '" + vScrieAri + "' WHERE (capitolId = '" + vValori[0] + "')", Convert.ToInt16(Session["SESan"]));

                }
                //********************primeste**************************//
                // verificam sa nu fie gol capitolul 3; daca e gol se insereaza si astfel putem face update fara erori - pt "primeste"
                CalculCapitole.PopuleazaTabelaCapitole(Convert.ToInt32(Session["SESan"]), Convert.ToInt64(vValoriModificare[x][5]), Convert.ToInt64(Session["SESunitateId"]), "3");
                // stabilim codrand pentru primeste
                string vCodRand = vValoriModificare[x][2];
                if (Convert.ToDecimal(vValoriModificare[x][2]) > 9)
                    vCodRand = (Convert.ToDecimal(vCodRand) - 7).ToString();
                else
                    vCodRand = (Convert.ToDecimal(vCodRand) + 7).ToString();
                // gasim linia de capitol pentru randul care se va updata pentru cel care "primeste"
                // 2. pentru randul respectiv, de la 3 la 8
                vCampuri = new List<string> { "capitolId", "col1", "col2" };
                vValori = ManipuleazaBD.fRezultaUnString("SELECT capitole.capitolId, capitole.col1, capitole.col2 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + vValoriModificare[x][5] + "') AND (sabloaneCapitole.capitol = '3') AND (capitole.codRand = '" + vCodRand + "')", vCampuri, Convert.ToInt16(Session["SESan"]));
                vScrieHa = CalculeazaAriHa((-Convert.ToDecimal(vValoriModificare[x][9])).ToString(), vValori[1], (-Convert.ToDecimal(vValoriModificare[x][10])).ToString(), vValori[2])[0].Replace(',', '.');
                vScrieAri = CalculeazaAriHa((-Convert.ToDecimal(vValoriModificare[x][9])).ToString(), vValori[1], (-Convert.ToDecimal(vValoriModificare[x][10])).ToString(), vValori[2])[1].Replace(',', '.');
                if (Convert.ToDecimal(vScrieAri) < 0) vScrieAri = "0";
                if (Convert.ToDecimal(vScrieHa) < 0) vScrieHa = "0";

                // scriem in 3; adunam la ce exista valoarea din textbox-uri
                ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vScrieHa + "', col2 = '" + vScrieAri + "' WHERE (capitolId = '" + vValori[0] + "')", Convert.ToInt16(Session["SESan"]));
                // 2. adaugam si la 2 si +17 (totaluri) - suprafete primite
                vValori = ManipuleazaBD.fRezultaUnString("SELECT capitole.capitolId, capitole.col1, capitole.col2 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + vValoriModificare[x][5] + "') AND (sabloaneCapitole.capitol = '3') AND (capitole.codRand = '2')", vCampuri, Convert.ToInt16(Session["SESan"]));
                vScrieHa = CalculeazaAriHa((-Convert.ToDecimal(vValoriModificare[x][9])).ToString(), vValori[1], (-Convert.ToDecimal(vValoriModificare[x][10])).ToString(), vValori[2])[0].Replace(',', '.');
                vScrieAri = CalculeazaAriHa((-Convert.ToDecimal(vValoriModificare[x][9])).ToString(), vValori[1], (-Convert.ToDecimal(vValoriModificare[x][10])).ToString(), vValori[2])[1].Replace(',', '.');
                if (Convert.ToDecimal(vScrieAri) < 0) vScrieAri = "0";
                if (Convert.ToDecimal(vScrieHa) < 0) vScrieHa = "0";
                // scriem in 3; adunam la ce exista valoarea din textbox-uri
                ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vScrieHa + "', col2 = '" + vScrieAri + "' WHERE (capitolId = '" + vValori[0] + "')", Convert.ToInt16(Session["SESan"]));
                vValori = ManipuleazaBD.fRezultaUnString("SELECT capitole.capitolId, capitole.col1, capitole.col2 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + vValoriModificare[x][5] + "') AND (sabloaneCapitole.capitol = '3') AND (capitole.codRand = '17')", vCampuri, Convert.ToInt16(Session["SESan"]));
                vScrieHa = CalculeazaAriHa((-Convert.ToDecimal(vValoriModificare[x][9])).ToString(), vValori[1], (-Convert.ToDecimal(vValoriModificare[x][10])).ToString(), vValori[2])[0].Replace(',', '.');
                vScrieAri = CalculeazaAriHa((-Convert.ToDecimal(vValoriModificare[x][9])).ToString(), vValori[1], (-Convert.ToDecimal(vValoriModificare[x][10])).ToString(), vValori[2])[1].Replace(',', '.');
                if (Convert.ToDecimal(vScrieAri) < 0) vScrieAri = "0";
                if (Convert.ToDecimal(vScrieHa) < 0) vScrieHa = "0";
                // scriem in 3; adunam la ce exista valoarea din textbox-uri
                ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vScrieHa + "', col2 = '" + vScrieAri + "' WHERE (capitolId = '" + vValori[0] + "')", Convert.ToInt16(Session["SESan"]));


                ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "atribuire mod utilizare --> cap.3", "parcela care da: " + vValoriModificare[x][4] + " - care a primit, id: " + vValoriModificare[x][5] + ")", "", Convert.ToInt64(Session["SESgospodarieId"]), 2);
                ClassLog.UpdateDataUltimeiModificari(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(vValoriModificare[x][4]));
                ClassLog.UpdateDataUltimeiModificari(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(vValoriModificare[x][5]));

                //**************  FINAL SCADEM  *****************************//
                // return;
            }

        }
        //*******************************************
        // REZILIERE
        //*******************************************
        else if (ViewState["contractMod"].ToString() == "s")
        {
            // luam valorile existente pentru a le scadea din 3
            List<List<string>> vValoriModificare = ValoriModificare();
            //**************  SCADEM  *****************************//
            for (int x = 0; x < vValoriModificare.Count(); x++)
            {
                // stergem valorile din parcelemodutilizare
                ManipuleazaBD.fManipuleazaBD("DELETE parceleModUtilizare WHERE modUtilizareId = '" + vValoriModificare[x][17] + "'", Convert.ToInt16(Session["SESan"]));
                // verificam sa nu fie gol capitolul 3; daca e gol se insereaza si astfel putem face update fara erori
                // gasim linia de capitol pentru randul care se va updata pentru cel care "dă"
                // 1. pentru randul respectiv, de la 10 la 15
                List<string> vCampuri = new List<string> { "capitolId", "col1", "col2" };
                List<string> vValori = ManipuleazaBD.fRezultaUnString("SELECT capitole.capitolId, capitole.col1, capitole.col2 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + vValoriModificare[x][4] + "') AND (sabloaneCapitole.capitol = '3') AND (capitole.codRand = '" + vValoriModificare[x][2] + "')", vCampuri, Convert.ToInt16(Session["SESan"]));
                // calculam arii totali existenti + introdusi
                string vScrieHa = CalculeazaAriHa((-Convert.ToDecimal(vValoriModificare[x][9])).ToString(), vValori[1], (-Convert.ToDecimal(vValoriModificare[x][10])).ToString(), vValori[2])[0].Replace(',', '.');
                string vScrieAri = CalculeazaAriHa((-Convert.ToDecimal(vValoriModificare[x][9])).ToString(), vValori[1], (-Convert.ToDecimal(vValoriModificare[x][10])).ToString(), vValori[2])[1].Replace(',', '.');
                if (Convert.ToDecimal(vScrieAri) < 0) vScrieAri = "0";
                if (Convert.ToDecimal(vScrieHa) < 0) vScrieHa = "0";
                // scriem in 3; adunam la ce exista valoarea din textbox-uri
                ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vScrieHa + "', col2 = '" + vScrieAri + "' WHERE (capitolId = '" + vValori[0] + "')", Convert.ToInt16(Session["SESan"]));
                // 2. adaugam si la 9 si -17 (totaluri) - suprafete date
                vValori = ManipuleazaBD.fRezultaUnString("SELECT capitole.capitolId, capitole.col1, capitole.col2 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + vValoriModificare[x][4] + "') AND (sabloaneCapitole.capitol = '3') AND (capitole.codRand = '9')", vCampuri, Convert.ToInt16(Session["SESan"]));
                vScrieHa = CalculeazaAriHa((-Convert.ToDecimal(vValoriModificare[x][9])).ToString(), vValori[1], (-Convert.ToDecimal(vValoriModificare[x][10])).ToString(), vValori[2])[0].Replace(',', '.');
                vScrieAri = CalculeazaAriHa((-Convert.ToDecimal(vValoriModificare[x][9])).ToString(), vValori[1], (-Convert.ToDecimal(vValoriModificare[x][10])).ToString(), vValori[2])[1].Replace(',', '.');
                if (Convert.ToDecimal(vScrieAri) < 0) vScrieAri = "0";
                if (Convert.ToDecimal(vScrieHa) < 0) vScrieHa = "0";
                // scriem in 3; adunam la ce exista valoarea din textbox-uri randul 9
                ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vScrieHa + "', col2 = '" + vScrieAri + "' WHERE (capitolId = '" + vValori[0] + "')", Convert.ToInt16(Session["SESan"]));

                // scadem din 17 --- suprafata utilizata, la care adaugam randul 1 suprafata in proprietate
                // facem direct suma 1+2-9
                string vValori129 = "ari";
                decimal vValoriRanduri129 = Convert.ToDecimal(ManipuleazaBD.fRezultaUnString("SELECT     SUM(CASE capitole.codRand WHEN '9' THEN (- CONVERT(decimal(10,5), capitole.col1) * 100 - CONVERT(decimal(10,5), capitole.col2)) ELSE (CONVERT(decimal(10,5), capitole.col1) * 100 + CONVERT(decimal(10,5), capitole.col2)) END) AS ari FROM         capitole INNER JOIN   sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + vValoriModificare[x][4] + "') AND (sabloaneCapitole.capitol = '3')  AND (capitole.codRand = '1' OR    capitole.codRand = '9' OR capitole.codRand = '2')", vValori129, Convert.ToInt16(Session["SESan"])).Replace('.', ','));
                // nu ne mai intereseazace exista, scriem direct suma, folosim doar valoarea cod capitol [0]
                vValori = ManipuleazaBD.fRezultaUnString("SELECT capitole.capitolId, capitole.col1, capitole.col2 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + vValoriModificare[x][4] + "') AND (sabloaneCapitole.capitol = '3') AND (capitole.codRand = '17')", vCampuri, Convert.ToInt16(Session["SESan"]));
                vScrieHa = CalculeazaAriHa("0", "0", (vValoriRanduri129).ToString(), "0")[0].Replace(',', '.');
                vScrieAri = CalculeazaAriHa("0", "0", (vValoriRanduri129).ToString(), "0")[1].Replace(',', '.');
                if (Convert.ToDecimal(vScrieAri) < 0) vScrieAri = "0";
                if (Convert.ToDecimal(vScrieHa) < 0) vScrieHa = "0";
                // scriem in 3; adunam la ce exista valoarea din textbox-uri randul 9
                ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vScrieHa + "', col2 = '" + vScrieAri + "' WHERE (capitolId = '" + vValori[0] + "')", Convert.ToInt16(Session["SESan"]));
                // verificam daca cel ce primeste e persoana juridica; 
                // daca este atunci adaugam si pt randul 16 la cel ce da
                if (PersoanaJuridica(vValoriModificare[x][5]))
                {
                    // gasim linia de capitol pentru randul 16 care se va updata pentru cel care "dă"
                    vCampuri = new List<string> { "capitolId", "col1", "col2" };
                    vValori = ManipuleazaBD.fRezultaUnString("SELECT capitole.capitolId, capitole.col1, capitole.col2 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + vValoriModificare[x][4] + "') AND (sabloaneCapitole.capitol = '3') AND (capitole.codRand = '16')", vCampuri, Convert.ToInt16(Session["SESan"]));
                    vScrieHa = CalculeazaAriHa((-Convert.ToDecimal(vValoriModificare[x][9])).ToString(), vValori[1], (-Convert.ToDecimal(vValoriModificare[x][10])).ToString(), vValori[2])[0].Replace(',', '.');
                    vScrieAri = CalculeazaAriHa((-Convert.ToDecimal(vValoriModificare[x][9])).ToString(), vValori[1], (-Convert.ToDecimal(vValoriModificare[x][10])).ToString(), vValori[2])[1].Replace(',', '.');
                    if (Convert.ToDecimal(vScrieAri) < 0) vScrieAri = "0";
                    if (Convert.ToDecimal(vScrieHa) < 0) vScrieHa = "0";
                    // scriem in 3; adunam la ce exista valoarea din textbox-uri
                    ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vScrieHa + "', col2 = '" + vScrieAri + "' WHERE (capitolId = '" + vValori[0] + "')", Convert.ToInt16(Session["SESan"]));

                }
                //********************primeste**************************//
                // verificam sa nu fie gol capitolul 3; daca e gol se insereaza si astfel putem face update fara erori - pt "primeste"
                CalculCapitole.PopuleazaTabelaCapitole(Convert.ToInt32(Session["SESan"]), Convert.ToInt64(vValoriModificare[x][5]), Convert.ToInt64(Session["SESunitateId"]), "3");
                // stabilim codrand pentru primeste
                string vCodRand = vValoriModificare[x][2];
                if (Convert.ToDecimal(vValoriModificare[x][2]) > 9)
                    vCodRand = (Convert.ToDecimal(vCodRand) - 7).ToString();
                else
                    vCodRand = (Convert.ToDecimal(vCodRand) + 7).ToString();
                // gasim linia de capitol pentru randul care se va updata pentru cel care "primeste"
                // 2. pentru randul respectiv, de la 3 la 8
                vCampuri = new List<string> { "capitolId", "col1", "col2" };
                vValori = ManipuleazaBD.fRezultaUnString("SELECT capitole.capitolId, capitole.col1, capitole.col2 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + vValoriModificare[x][5] + "') AND (sabloaneCapitole.capitol = '3') AND (capitole.codRand = '" + vCodRand + "')", vCampuri, Convert.ToInt16(Session["SESan"]));
                vScrieHa = CalculeazaAriHa((-Convert.ToDecimal(vValoriModificare[x][9])).ToString(), vValori[1], (-Convert.ToDecimal(vValoriModificare[x][10])).ToString(), vValori[2])[0].Replace(',', '.');
                vScrieAri = CalculeazaAriHa((-Convert.ToDecimal(vValoriModificare[x][9])).ToString(), vValori[1], (-Convert.ToDecimal(vValoriModificare[x][10])).ToString(), vValori[2])[1].Replace(',', '.');
                if (Convert.ToDecimal(vScrieAri) < 0) vScrieAri = "0";
                if (Convert.ToDecimal(vScrieHa) < 0) vScrieHa = "0";
                // scriem in 3; adunam la ce exista valoarea din textbox-uri
                ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vScrieHa + "', col2 = '" + vScrieAri + "' WHERE (capitolId = '" + vValori[0] + "')", Convert.ToInt16(Session["SESan"]));
                // 2. adaugam si la 2 si +17 (totaluri) - suprafete primite
                vValori = ManipuleazaBD.fRezultaUnString("SELECT capitole.capitolId, capitole.col1, capitole.col2 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + vValoriModificare[x][5] + "') AND (sabloaneCapitole.capitol = '3') AND (capitole.codRand = '2')", vCampuri, Convert.ToInt16(Session["SESan"]));
                vScrieHa = CalculeazaAriHa((-Convert.ToDecimal(vValoriModificare[x][9])).ToString(), vValori[1], (-Convert.ToDecimal(vValoriModificare[x][10])).ToString(), vValori[2])[0].Replace(',', '.');
                vScrieAri = CalculeazaAriHa((-Convert.ToDecimal(vValoriModificare[x][9])).ToString(), vValori[1], (-Convert.ToDecimal(vValoriModificare[x][10])).ToString(), vValori[2])[1].Replace(',', '.');
                if (Convert.ToDecimal(vScrieAri) < 0) vScrieAri = "0";
                if (Convert.ToDecimal(vScrieHa) < 0) vScrieHa = "0";
                // scriem in 3; adunam la ce exista valoarea din textbox-uri
                ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vScrieHa + "', col2 = '" + vScrieAri + "' WHERE (capitolId = '" + vValori[0] + "')", Convert.ToInt16(Session["SESan"]));
                vValori = ManipuleazaBD.fRezultaUnString("SELECT capitole.capitolId, capitole.col1, capitole.col2 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.gospodarieId = '" + vValoriModificare[x][5] + "') AND (sabloaneCapitole.capitol = '3') AND (capitole.codRand = '17')", vCampuri, Convert.ToInt16(Session["SESan"]));
                vScrieHa = CalculeazaAriHa((-Convert.ToDecimal(vValoriModificare[x][9])).ToString(), vValori[1], (-Convert.ToDecimal(vValoriModificare[x][10])).ToString(), vValori[2])[0].Replace(',', '.');
                vScrieAri = CalculeazaAriHa((-Convert.ToDecimal(vValoriModificare[x][9])).ToString(), vValori[1], (-Convert.ToDecimal(vValoriModificare[x][10])).ToString(), vValori[2])[1].Replace(',', '.');
                if (Convert.ToDecimal(vScrieAri) < 0) vScrieAri = "0";
                if (Convert.ToDecimal(vScrieHa) < 0) vScrieHa = "0";
                // scriem in 3; adunam la ce exista valoarea din textbox-uri
                ManipuleazaBD.fManipuleazaBD("UPDATE capitole SET col1 = '" + vScrieHa + "', col2 = '" + vScrieAri + "' WHERE (capitolId = '" + vValori[0] + "')", Convert.ToInt16(Session["SESan"]));


                ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "atribuire mod utilizare --> cap.3", "parcela care da: " + vValoriModificare[x][4] + " - care a primit, id: " + vValoriModificare[x][5] + ")", "", Convert.ToInt64(Session["SESgospodarieId"]), 2);
                ClassLog.UpdateDataUltimeiModificari(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(vValoriModificare[x][4]));
                ClassLog.UpdateDataUltimeiModificari(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(vValoriModificare[x][5]));

                //**************  FINAL SCADEM  *****************************//
                //   return;
            }
        }

    }

    protected void btListaContracte_Click(object sender, EventArgs e)
    {
        pnContracteUtilizare.Visible = false;
        pnlListaContracte.Visible = true;
        gvModUtilizareContracte.DataBind();
        gvModUtilizareContracte.SelectedIndex = -1;
        btAdaugaContracte.Visible = true;
        pnlReziliaza.Visible = false;
    }
    protected void btModUtilizareContractSalveaza_Click(object sender, EventArgs e)
    {
        // salvam in contracte si parceleaModUtilizare
        // in functie de tip alegem care da si care primeste
        string vDa = "";
        string vPrimeste = "";
        string vTip1 = "";
        string[] vTip = { "", "" };
        // validam datele din textboxuri si le rescriem cu maximum posibil 
        ValidamNumerice();
        //return;
        switch (ddlModUtilizareContract.SelectedValue)
        {
            case "3":
                vTip1 = "0";
                vTip = new string[] { "3", "10" };
                vPrimeste = Session["SESgospodarieId"].ToString();
                vDa = lblGospodarieId.Text;
                break;
            case "4":
                vTip1 = "0";
                vTip = new string[] { "4", "11" };
                vPrimeste = Session["SESgospodarieId"].ToString();
                vDa = lblGospodarieId.Text;
                break;
            case "5":
                vTip1 = "0";
                vTip = new string[] { "5", "12" };
                vPrimeste = Session["SESgospodarieId"].ToString();
                vDa = lblGospodarieId.Text;
                break;
            case "6":
                vTip1 = "0";
                vTip = new string[] { "6", "13" };
                vPrimeste = Session["SESgospodarieId"].ToString();
                vDa = lblGospodarieId.Text;
                break;
            case "7":
                vTip1 = "0";
                vTip = new string[] { "7", "14" };
                vPrimeste = Session["SESgospodarieId"].ToString();
                vDa = lblGospodarieId.Text;
                break;
            case "8":
                vTip1 = "0";
                vTip = new string[] { "8", "15" };
                vPrimeste = Session["SESgospodarieId"].ToString();
                vDa = lblGospodarieId.Text;
                break;
            case "10":
                vTip1 = "1";
                vTip = new string[] { "3", "3" };
                vDa = Session["SESgospodarieId"].ToString();
                vPrimeste = lblGospodarieId.Text;
                break;
            case "11":
                vTip1 = "1";
                vTip = new string[] { "4", "4" };
                vDa = Session["SESgospodarieId"].ToString();
                vPrimeste = lblGospodarieId.Text;
                break;
            case "12":
                vTip1 = "1";
                vTip = new string[] { "5", "5" };
                vDa = Session["SESgospodarieId"].ToString();
                vPrimeste = lblGospodarieId.Text;
                break;
            case "13":
                vTip1 = "1";
                vTip = new string[] { "6", "6" };
                vDa = Session["SESgospodarieId"].ToString();
                vPrimeste = lblGospodarieId.Text;
                break;
            case "14":
                vTip1 = "1";
                vTip = new string[] { "7", "7" };
                vDa = Session["SESgospodarieId"].ToString();
                vPrimeste = lblGospodarieId.Text;
                break;
            case "15":
                vTip1 = "1";
                vTip = new string[] { "8", "8" };
                vDa = Session["SESgospodarieId"].ToString();
                vPrimeste = lblGospodarieId.Text;
                break;

        }
        // verificam sa fie datele corecte
        DateTime vDataInceput = Convert.ToDateTime("01.01." + Session["SESan"].ToString());
        DateTime vDataFinal = Convert.ToDateTime("31.12." + Session["SESan"].ToString());
        DateTime vDataSemnarii = DateTime.Now;
        try
        {
            vDataInceput = Convert.ToDateTime("01.01." + ddlDataInceputContract.SelectedValue);

            //            vDataInceput = Convert.ToDateTime((tbDataInceputContract.Text).ToString().Substring(0, 10));
        }
        catch
        {
            vDataInceput = Convert.ToDateTime(DateTime.Now.ToString().Substring(0, 10));
        }
        try
        {
            vDataFinal = Convert.ToDateTime("31.12." + ddlDataFinalContract.SelectedValue);
            //            vDataFinal = Convert.ToDateTime((tbDataFinalContract.Text).ToString().Substring(0, 10));
        }
        catch
        {
            vDataFinal = Convert.ToDateTime(DateTime.Now.ToString().Substring(0, 10));
        }
      
        if (redeventaTextBox.Text == "")
        {
            valCustom.IsValid = false;
            valCustom.ErrorMessage = "Introduceti redeventa";
            return;
        }
        if (vDataInceput >= vDataFinal)
        {
            valCustom.IsValid = false;
            valCustom.ErrorMessage = "Data de începere a contractului trebuie să fie mai mare decât cea finalizării acestuia! Vă rugăm să corectaţi pentru a trece mai departe.";
            return;
        }
        if (lblGospodarieId.Text == "")
        {
            valCustom.IsValid = false;
            valCustom.ErrorMessage = "Contractul trebuie să aibă două gospodării selectate! Vă rugăm să alegeţi a doua gospodărie pentru a continua.";
            return;
        }
        try
        {
            vDataSemnarii = Convert.ToDateTime((tbContractDinData.Text).ToString().Substring(0, 10));
        }
        catch
        {
            vDataSemnarii = Convert.ToDateTime(DateTime.Now.ToString().Substring(0, 10));
        }
        int vContractId = 0;
        if (ViewState["contractMod"].ToString() == "a")
        {
            string vInsert = "INSERT INTO parceleAtribuiriContracte (unitateId, contractTip, contractGospodarieIdDa, contractGospodarieIdPrimeste, contractDataInceput, contractDataFinal, contractDetalii, contractNr, contractDataSemnarii, contractActiv,redeventa) VALUES (" + Convert.ToInt64(Session["SESunitateId"]) + ",'" + vTip[0] + "', '" + vDa + "', '" + vPrimeste + "', Convert(datetime,'01.01." + ddlDataInceputContract.SelectedValue + "', 104), Convert(datetime,'31.12." + ddlDataFinalContract.SelectedValue + "',104), '" + tbDetaliiContract.Text + "', '" + tbContractNr.Text + "',CONVERT(datetime,'" + vDataSemnarii.ToString() + "', 104), 'True','" + Convert.ToDecimal(redeventaTextBox.Text.Replace(".", ",")).ToString().Replace(",", ".") + "')";
            //            string vInsert = "INSERT INTO parceleAtribuiriContracte (contractTip, contractGospodarieIdDa, contractGospodarieIdPrimeste, contractDataInceput, contractDataFinal, contractDetalii, contractNr, contractDataSemnarii, contractActiv) VALUES ('" + vTip[0] + "', '" + vDa + "', '" + vPrimeste + "', Convert(datetime,'" + vDataInceput.ToString() + "', 104), Convert(datetime,'" + vDataFinal.ToString() + "',104), '" + tbDetaliiContract.Text + "', '" + tbContractNr.Text + "',CONVERT(datetime,'" + vDataSemnarii.ToString() + "', 104), 'True')";
            ManipuleazaBD.fManipuleazaBDCuId(vInsert, out vContractId, Convert.ToInt16(Session["SESan"]));
            SalveazaAdauga(vDa, vPrimeste, vContractId.ToString(), vTip1);
        }
        else
        {
            SalveazaAdauga(vDa, vPrimeste, vContractId.ToString(), vTip1);
            string vInsert = "UPDATE parceleAtribuiriContracte SET contractNr = '" + tbContractNr.Text + "',  contractTip='" + vTip[0] + "', contractGospodarieIdDa = '" + vDa + "', contractGospodarieIdPrimeste='" + vPrimeste + "', contractDetalii = '" + tbDetaliiContract.Text + "',contractDataInceput = Convert(datetime,'01.01." + ddlDataInceputContract.SelectedValue + "', 104), contractDataFinal = Convert(datetime,'31.12." + ddlDataFinalContract.SelectedValue + "',104), contractDataSemnarii = CONVERT(datetime,'" + vDataSemnarii.ToString() + "', 104), contractActiv = 'True',redeventa = '"+Convert.ToDecimal(redeventaTextBox.Text.Replace(".",",")).ToString().Replace(",", ".") +"' WHERE contractId = '" + ((Label)gvModUtilizareContracte.SelectedRow.FindControl("lblGvContracteId")).Text + "'";
            //            string vInsert = "UPDATE parceleAtribuiriContracte SET contractNr = '" + tbContractNr.Text + "',  contractTip='" + vTip[0] + "', contractGospodarieIdDa = '" + vDa + "', contractGospodarieIdPrimeste='" + vPrimeste + "', contractDetalii = '" + tbDetaliiContract.Text + "',contractDataInceput = Convert(datetime,'" + vDataInceput.ToString() + "', 104), contractDataFinal = Convert(datetime,'" + vDataFinal.ToString() + "',104), contractDataSemnarii = CONVERT(datetime,'" + vDataSemnarii.ToString() + "', 104), contractActiv = 'True' WHERE contractId = '" + ((Label)gvModUtilizareContracte.SelectedRow.FindControl("lblGvContracteId")).Text + "'";
            ManipuleazaBD.fManipuleazaBDCuId(vInsert, out vContractId, Convert.ToInt16(Session["SESan"]));
            pnContracteUtilizare.Visible = false;
            pnlListaContracte.Visible = true;
            btAdaugaContracte.Visible = true;
            gvModUtilizareContracte.DataBind();
            gvModUtilizareContracte.SelectedIndex = -1;

        }

        gvModUtilizareContracte.DataBind();
    }

    protected void btContracteArataListaParcele_Click(object sender, EventArgs e)
    {
        pnContracteUtilizare.Visible = false;
    }
    protected void btAdaugaContracte_Click(object sender, EventArgs e)
    {
        ViewState["contractMod"] = "a";
        btStergeContracte.Visible = false;
        btAdaugaContracte.Visible = false;
        btModificaContracte.Visible = false;
        gvModUtilizareContracte.DataBind();
        gvModUtilizareContracte.SelectedIndex = -1;
        btContracteUtilizare_Click(sender, e);
        pnlListaContracte.Visible = false;
        pnContracteUtilizare.Visible = true;
        pnlReziliaza.Visible = false;
        InitializareContracte();
    }
    protected void btModificaContracte_Click(object sender, EventArgs e)
    {
        ViewState["contractMod"] = "m";
        btStergeContracte.Visible = false;
        btAdaugaContracte.Visible = false;
        btModificaContracte.Visible = false;
        btContracteUtilizare_Click(sender, e);
        pnlListaContracte.Visible = false;
        pnContracteUtilizare.Visible = true;
        pnlReziliaza.Visible = false;
        InitializareContracte();

    }
    protected void btStergeContracte_Click(object sender, EventArgs e)
    {
        ViewState["contractMod"] = "s";
        btStergeContracte.Visible = false;
        btModificaContracte.Visible = false;
        pnlListaContracte.Visible = false;
        pnContracteUtilizare.Visible = false;
        pnlReziliaza.Visible = true;
    }
    protected void ddlModUtilizareContract_Init1(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        ddlModUtilizareContract.Items.Clear();
        ListItem vElement = new ListItem { };
        vElement.Text = "alege tipul de contract";
        vElement.Value = "0";
        if (ddlModUtilizareContract.Items.IndexOf(vElement) == -1)
            ddlModUtilizareContract.Items.Add(vElement);
        List<string> vCampuri = new List<string> { "denumire", "cod" };
        List<List<string>> vListaValori = ManipuleazaBD.fRezultaListaStringuri(@"SELECT CONVERT(nvarchar,codRand) AS cod, CONVERT(nvarchar,codRand) + '. ' + denumire5 AS denumire FROM sabloaneCapitole
                                                          WHERE (an = '" + Session["SESan"].ToString() + @"') AND (capitol = '3') AND (denumire5<>'')
                                                            AND (CHARINDEX('+', formula) = 0) AND (CHARINDEX('-', formula) = 0 and codRand in (3,10))
                                                            AND (legatura >= 0)", vCampuri, Convert.ToInt16(Session["SESan"]));
        vElement = new ListItem { };
        foreach (List<string> vValoare in vListaValori)
        {
            vElement = new ListItem { };
            vElement.Text = vValoare[0];
            vElement.Value = vValoare[1];
            ddlModUtilizareContract.Items.Add(vElement);
        }
    }
    protected void ddlModUtilizareContract_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlModUtilizareContract.SelectedValue != "0")
        {
            pnlContracteDetalii.Visible = true;
            if (Convert.ToInt16(ddlModUtilizareContract.SelectedValue) > 9)
                lblGospodarieIdGrid.Text = Session["SESgospodarieId"].ToString();
            else
                lblGospodarieIdGrid.Text = lblGospodarieId.Text;
            gvListaParcele.DataBind();
            btAdaugaModUtilizareContractSalveaza.Visible = true;
        }
        else
        {
            pnlContracteDetalii.Visible = false;

        }
        if (ViewState["contractMod"].ToString() == "m")
        {
            RefacemLista();
        }
        else gvListaParcele.DataBind();
    }
    protected void gvGospodarii_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(gvGospodarii, e, this);
    }
    protected void gvGospodarii_SelectedIndexChanged(object sender, EventArgs e)
    {
        // la click pe o gospodarie inchidem pop-up-ul
        popup.Visible = false;
        lblSiGospodaria.Text = GospodariaCurentaDetalii(gvGospodarii.SelectedValue.ToString());
        lblGospodarieId.Text = gvGospodarii.SelectedValue.ToString();
        btAlegeGospodaria.Text = " (schimbă gospodăria)";
        btAdaugaModUtilizareContractSalveaza.Visible = true;
        if (Convert.ToInt16(ddlModUtilizareContract.SelectedValue) > 9)
            lblGospodarieIdGrid.Text = Session["SESgospodarieId"].ToString();
        else
            lblGospodarieIdGrid.Text = lblGospodarieId.Text;


        if (ViewState["contractMod"].ToString() == "m")
        {
            pnlContracteDetalii.Visible = true;
            btAdaugaModUtilizareContractSalveaza.Visible = true;
            RefacemLista();
        }
        else gvListaParcele.DataBind();
    }
    protected void btAlegeGospodaria_Click(object sender, EventArgs e)
    {
        // aratam pop-up-ul de alegere a gospodariei partenere
        popup.Visible = true;
    }
    protected void ckbParcelaFaceObiectul_CheckedChanged(object sender, EventArgs e)
    {
        // daca schimba ckb activam sau dezactivam textboxurile
        if (((CheckBox)sender).Checked == false)
        {
            ((TextBox)((CheckBox)sender).Parent.Parent.FindControl("tbAlocaHa")).Enabled = false;
            ((TextBox)((CheckBox)sender).Parent.Parent.FindControl("tbAlocaAri")).Enabled = false;

        }
        else
        {
            ((TextBox)((CheckBox)sender).Parent.Parent.FindControl("tbAlocaHa")).Enabled = true;
            ((TextBox)((CheckBox)sender).Parent.Parent.FindControl("tbAlocaAri")).Enabled = true;
        }

    }
    protected void ckbParcelaSelecteazaTot_CheckedChanged(object sender, EventArgs e)
    {
        // daca schimba ckb activam sau dezactivam textboxurile
        if (((CheckBox)sender).Checked == false)
        {
            for (int i = 0; i < gvListaParcele.Rows.Count; i++)
            {
                ((CheckBox)gvListaParcele.Rows[i].FindControl("ckbParcelaFaceObiectul")).Checked = false;
                ((TextBox)gvListaParcele.Rows[i].FindControl("tbAlocaHa")).Enabled = false;
                ((TextBox)gvListaParcele.Rows[i].FindControl("tbAlocaAri")).Enabled = false;
            }
        }
        else
        {
            for (int i = 0; i < gvListaParcele.Rows.Count; i++)
            {
                ((CheckBox)gvListaParcele.Rows[i].FindControl("ckbParcelaFaceObiectul")).Checked = true;
                ((TextBox)gvListaParcele.Rows[i].FindControl("tbAlocaHa")).Enabled = true;
                ((TextBox)gvListaParcele.Rows[i].FindControl("tbAlocaAri")).Enabled = true;
            }
        }
    }
    protected void gvListaParcele_DataBound(object sender, EventArgs e)
    {
        decimal vAriAlocati = 0;
        decimal vAriTotal = 0;

        for (int i = 0; i < gvListaParcele.Rows.Count; i++)
        {
            if (gvListaParcele.Rows[i].RowType == DataControlRowType.DataRow)
            {
                try
                {
                    vAriTotal = Convert.ToDecimal(((Label)gvListaParcele.Rows[i].FindControl("lblHaTotal")).Text) * 100 + Convert.ToDecimal(((Label)gvListaParcele.Rows[i].FindControl("lblAriTotal")).Text);
                    vAriAlocati = Convert.ToDecimal(((Label)gvListaParcele.Rows[i].FindControl("lblHaMaxim")).Text) * 100 + Convert.ToDecimal(((Label)gvListaParcele.Rows[i].FindControl("lblAriMaxim")).Text);
                    string[] vDisponibil = CalculeazaAriHa("0", "0", Convert.ToString(vAriTotal - vAriAlocati), "0");
                    ((Label)gvListaParcele.Rows[i].FindControl("lblHaMaxim")).Text = vDisponibil[0];
                    ((Label)gvListaParcele.Rows[i].FindControl("lblAriMaxim")).Text = vDisponibil[1];
                    ((TextBox)gvListaParcele.Rows[i].FindControl("tbAlocaHa")).Text = vDisponibil[0];
                    ((TextBox)gvListaParcele.Rows[i].FindControl("tbAlocaAri")).Text = vDisponibil[1];
                    if (Convert.ToDecimal(vDisponibil[0]) > 0)
                        ((TextBox)gvListaParcele.Rows[i].FindControl("tbAlocaHa")).Enabled = true;
                    else ((TextBox)gvListaParcele.Rows[i].FindControl("tbAlocaHa")).Enabled = false;
                    if ((vAriTotal - vAriAlocati) > 0)
                    {
                        if (((CheckBox)gvListaParcele.Rows[i].FindControl("ckbParcelaFaceObiectul")).Checked == true)
                        {
                            ((TextBox)gvListaParcele.Rows[i].FindControl("tbAlocaAri")).Enabled = true;
                        }
                    }
                    else
                    {
                        ((TextBox)gvListaParcele.Rows[i].FindControl("tbAlocaAri")).Enabled = false;
                        ((CheckBox)gvListaParcele.Rows[i].FindControl("ckbParcelaFaceObiectul")).Enabled = false;
                        ((CheckBox)gvListaParcele.Rows[i].FindControl("ckbParcelaFaceObiectul")).Checked = false;
                    }
                }
                catch { }
            }
        }
    }
    protected string[] CalculeazaAriHa(string pHa1, string pHa2, string pAri1, string pAri2)
    {
        string[] vValori = { "0", "0" };
        if (pHa1 == "") pHa1 = "0";
        if (pHa2 == "") pHa2 = "0";
        if (pAri1 == "") pAri1 = "0";
        if (pAri2 == "") pAri2 = "0";
        decimal vValoare = Convert.ToDecimal(pHa1) * 100 + Convert.ToDecimal(pHa2) * 100 + Convert.ToDecimal(pAri1) + Convert.ToDecimal(pAri2);
        vValori[1] = (vValoare % 100).ToString();
        vValori[0] = Convert.ToInt64((vValoare - (vValoare % 100)) / 100).ToString();
        return vValori;
    }
    protected string CalculeazaAri(string pHa, string pAri)
    {
        string vValori = "0";
        decimal vValoare = Convert.ToDecimal(pHa) * 100 + Convert.ToDecimal(pAri);
        vValori = vValoare.ToString();
        return vValori;
    }
    protected void btSalveazaReziliere_Click(object sender, EventArgs e)
    {
        // reziliem contractul
        // daca e bifata anularea datelor stergem din 3 
        string vDa = ((Label)gvModUtilizareContracte.SelectedRow.FindControl("lblIdDa")).Text;
        string vPrimeste = ((Label)gvModUtilizareContracte.SelectedRow.FindControl("lblIdPrimeste")).Text;
        string vTip1 = "";
        string[] vTip = { "", "" };
        int vContractId = 0;

        if (ckbAnulatiDin3.Checked)
        {
            SalveazaAdauga(vDa, vPrimeste, vContractId.ToString(), vTip1);
        }

        string vInsert = "UPDATE parceleAtribuiriContracte SET contractDataFinal = CONVERT(datetime,'" + tbDataRezilierii.Text + "', 104), contractDataSemnarii = Convert(datetime,'" + DateTime.Now.ToString() + "', 104), contractDetalii = '" + tbMotivulRezilierii.Text + "', contractActiv = 'false' WHERE contractId = '" + ((Label)gvModUtilizareContracte.SelectedRow.FindControl("lblGvContracteId")).Text + "'";

        ManipuleazaBD.fManipuleazaBD(vInsert, Convert.ToInt16(Session["SESan"]));
        
        pnlReziliaza.Visible = false;
        pnlListaContracte.Visible = true;
        btStergeContracte.Visible = false;
        btAdaugaContracte.Visible = true;
        btModificaContracte.Visible = false;
        
        gvModUtilizareContracte.DataBind();
        gvModUtilizareContracte.SelectedIndex = -1;
    }
    protected void btModUtilizareSalveaza_Click(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        ValidamNumerice();
    }
    protected void ValidamNumerice()
    {
        for (int i = 0; i < gvListaParcele.Rows.Count; i++)
        {
            if (gvListaParcele.Rows[i].RowType == DataControlRowType.DataRow)
            {
                TextBox[] vControl = new TextBox[] { (TextBox)gvListaParcele.Rows[i].FindControl("tbAlocaHa"), (TextBox)gvListaParcele.Rows[i].FindControl("tbAlocaAri") };
                for (int j = 0; j < 2; j++)
                {
                    try
                    {
                        if (!Valideaza.ENumeric(vControl[j].Text.Replace(',', '.')) ||
        (Valideaza.ENumeric(vControl[j].Text.Replace(',', '.')) && Convert.ToDecimal(vControl[j].Text.Replace(',', '.')) < 0))
                        {
                            vControl[j].Text = "0";
                        }
                    }
                    catch { }
                }
                // calculam ari total introdusi in tb
                // total parcela
                decimal vAriTotal = Convert.ToDecimal(((Label)gvListaParcele.Rows[i].FindControl("lblHaTotal")).Text) * 100 + Convert.ToDecimal(((Label)gvListaParcele.Rows[i].FindControl("lblAriTotal")).Text);
                // total alocati deja
                decimal vAriAlocati = Convert.ToDecimal(((Label)gvListaParcele.Rows[i].FindControl("lblHaMaxim")).Text) * 100 + Convert.ToDecimal(((Label)gvListaParcele.Rows[i].FindControl("lblAriMaxim")).Text);
                // total introdusi in tb
                decimal vAriIntrodusi = Convert.ToDecimal(vControl[0].Text.Replace('.', ',')) * 100 + Convert.ToDecimal(vControl[1].Text.Replace('.', ','));
                // daca sunt mai multi decat disponibili punem maximul posibil
                if (vAriIntrodusi > vAriAlocati) vAriIntrodusi = vAriAlocati;
                vControl[0].Text = (CalculeazaAriHa("0", "0", Convert.ToString(vAriIntrodusi), "0")[0]).ToString();
                vControl[1].Text = (CalculeazaAriHa("0", "0", Convert.ToString(vAriIntrodusi), "0")[1]).ToString();
            }
        }
    }

    protected void tbCInceput_TextChanged(object sender, EventArgs e)
    {
        // inlucuim datele daca se introduc aiurea
        try
        {
            if (Convert.ToDateTime(tbCInceput.Text) > Convert.ToDateTime(tbCFinal.Text))
                tbCFinal.Text = tbCInceput.Text;
        }
        catch
        {
            try
            {
                tbCInceput.Text = Convert.ToDateTime(tbCFinal.Text).ToString();
            }
            catch
            {
                tbCInceput.Text = tbCFinal.Text = DateTime.Now.ToString().Remove(11);
            }
        }
    }
    protected void tbCFinal_TextChanged(object sender, EventArgs e)
    {
        // inlucuim datele daca se introduc aiurea
        try
        {
            if (Convert.ToDateTime(tbCFinal.Text) < Convert.ToDateTime(tbCInceput.Text))
                tbCInceput.Text = tbCFinal.Text;
        }
        catch
        {
            try
            {
                tbCFinal.Text = Convert.ToDateTime(tbCInceput.Text).ToString();
            }
            catch
            {
                tbCFinal.Text = tbCInceput.Text = DateTime.Now.ToString().Remove(11);
            }
        }
    }
    protected void tbDataInceputContract_TextChanged(object sender, EventArgs e)
    {
        // inlucuim datele daca se introduc aiurea
        /*     if (tbDataFinalContract.Text.Trim() != "")
             {
                 try
                 {
                     if (Convert.ToDateTime(tbDataInceputContract.Text) > Convert.ToDateTime(tbDataFinalContract.Text) && tbDataFinalContract.Text.Trim() != "")

                         tbDataFinalContract.Text = tbDataInceputContract.Text;

                 }
                 catch
                 {
                     try
                     {
                         tbDataInceputContract.Text = Convert.ToDateTime(tbDataFinalContract.Text).ToString();
                     }
                     catch
                     {
                         tbDataInceputContract.Text = tbDataFinalContract.Text = DateTime.Now.ToString().Remove(11);
                     }
                 }
             }
             // daca e data inceput din anul urmator punem ultima data din anul curent
             try
             {
                 if (Convert.ToDateTime(tbDataInceputContract.Text) > Convert.ToDateTime("31.12." + Session["SESan"].ToString()))
                 {
                     tbDataInceputContract.Text = "31.12." + Session["SESan"].ToString();
                     valCustom.IsValid = false;
                     valCustom.ErrorMessage = "Contractul trebuie sa cuprindă şi cel puţin o zi din anul " + Session["SESan"].ToString() + "!";
                 }
             }
             catch
             {
                tbDataInceputContract.Text = DateTime.Now.ToString().Remove(11);
             }
         * */
    }
    protected void tbDataFinalContract_TextChanged(object sender, EventArgs e)
    {
        // inlucuim datele daca se introduc aiurea
        /*  if (tbDataInceputContract.Text.Trim() != "")
          {
              try
              {
                  if (Convert.ToDateTime(tbDataFinalContract.Text) < Convert.ToDateTime(tbDataInceputContract.Text) && tbDataInceputContract.Text.Trim() != "")
                      tbDataInceputContract.Text = tbDataFinalContract.Text;
              }
              catch
              {
                  try
                  {
                      tbDataFinalContract.Text = Convert.ToDateTime(tbDataInceputContract.Text).ToString();
                  }
                  catch
                  {
                      tbDataFinalContract.Text = tbDataInceputContract.Text = DateTime.Now.ToString().Remove(11);
                  }
              }
          }
          // daca e data final din anul precedent punem prima zi din anul curent
          try
          {
              if (Convert.ToDateTime(tbDataFinalContract.Text) < Convert.ToDateTime("01.01." + Session["SESan"].ToString()))
              {
                  tbDataFinalContract.Text = "01.01." + Session["SESan"].ToString();
                  valCustom.IsValid = false;
                  valCustom.ErrorMessage = "Contractul trebuie sa cuprindă şi cel puţin o zi din anul " + Session["SESan"].ToString() + "!";
              }
          }
          catch
          {
              tbDataInceputContract.Text = DateTime.Now.ToString().Remove(11);
          }
         * */
    }


    #region neutilizate


    protected void ddlModUtilizareContract_Init(object sender, EventArgs e)
    {

    }
    protected void ddlModUtilizareContract_Load(object sender, EventArgs e)
    { }
    protected void ddlModUtilizareContract_PreRender(object sender, EventArgs e)
    {
    }
    protected void gvListaParcele_SelectedIndexChanged(object sender, EventArgs e)
    {
    }
    protected void gvListaParcele_RowDataBound(object sender, GridViewRowEventArgs e)
    {
    }
    protected void gvListaParcele_PreRender(object sender, EventArgs e)
    {
    }
    protected void SqlModUtilizareContracteGrid_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
    }
    #endregion

    protected void btTiparireContracte_Click(object sender, EventArgs e)
    {
        //Session["rcaUnitateId"] = "%";
        //Session["rcaJudetId"] = "%";
        //Session["rcaVolum"] = "%";
        //Session["rcaPozitie"] = "%";
        //Session["rcaNrDeLa"] = "0";
        //Session["rcaNrLa"] = "999999999";
        //Session["rcaStrainas"] = "%";
        //Session["rcaStrada"] = "%";
        //Session["rcaLocalitate"] = "%";
        //Session["rcaData1"] = Convert.ToDateTime(tbCInceput.Text);
        //Session["rcaData2"] = Convert.ToDateTime(tbCFinal.Text);
        //Session["rcaGospodarieId"] = Session["SESgospodarieId"].ToString();
        //Session["tipContract"] = tipContractDropDown.SelectedValue;
        //ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "tiparire", "tiparire contracte arenda", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
        //ResponseHelper.Redirect("~/printContracteArenda.aspx", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
        ResponseHelper.Redirect("~/printCapitolul15.aspx?codCapitol=15", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
    }
    protected void tbCNrContract_TextChanged(object sender, EventArgs e)
    {
        gvModUtilizareContracte.DataBind();
    }
    protected void tipContractDropDown_SelectedIndexChanged(object sender, EventArgs e)
    {
        gvModUtilizareContracte.DataBind();
    }
}