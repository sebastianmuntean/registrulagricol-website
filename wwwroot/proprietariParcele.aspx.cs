﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;

public partial class proprietariParcele : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ProprietariParcele.SetCapDeGospodarieAsDefaultProprietar();
            BindData();
        }
    }

    private void BindData()
    {
        SqlConnection con = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlDataAdapter da = new SqlDataAdapter(@"SELECT id , coalesce(numeProprietar,'') as numeProprietar , coalesce(cotaParte,'') as cotaParte, parcele.parcelaDenumire, parcele.parcelaNrCadastral, parcele.parcelaNrTopo,parcele.parcelaCF, parcele.parcelaSuprafataIntravilanHa, parcele.parcelaSuprafataIntravilanAri, parcele.parcelaSuprafataExtravilanHa, parcele.parcelaSuprafataExtravilanAri  FROM  proprietariparcele join parcele on parcele.parcelaId = proprietariparcele.idParcela  where proprietariparcele.an =" + Session["SESan"] + " AND idParcela = " + Session["idParcela"].ToString() + "", con);
        DataTable dt = new DataTable();
        try
        {
            da.Fill(dt);
            proprietariGridView.DataSource = dt;
            proprietariGridView.DataBind();
            proprietariGridView.Rows[0].BackColor = Color.Red;
        }
        catch
        {
            ManipuleazaBD.InchideConexiune(con);
        }
        ManipuleazaBD.InchideConexiune(con);
    }

    protected void proprietariGridView_RowEditing(object sender, GridViewEditEventArgs e)
    {
        proprietariGridView.EditIndex = e.NewEditIndex;
        BindData();
    }
    protected void proprietariGridView_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        proprietariGridView.EditIndex = -1;
        BindData();
    }
    protected void proprietariGridView_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        SqlConnection con = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand cmd = new SqlCommand();

        if (((LinkButton)proprietariGridView.Rows[0].Cells[0].Controls[0]).Text == "Insert")
        {
            cmd.CommandText = "INSERT INTO proprietariParcele (numeProprietar,cotaParte,idParcela,an, idGospodarie) values (@numeProprietar, @cotaParte, @idParcela, @an, '" + Session["SESgospodarieId"] + "')";
            cmd.Parameters.Add("@idParcela", SqlDbType.Int).Value = Convert.ToInt32(Session["idParcela"]);
            cmd.Parameters.Add("@numeProprietar", SqlDbType.VarChar).Value = ((TextBox)proprietariGridView.Rows[0].Cells[1].Controls[0]).Text;
            cmd.Parameters.Add("@cotaParte", SqlDbType.VarChar).Value = ((TextBox)proprietariGridView.Rows[0].Cells[2].Controls[0]).Text;
            cmd.Parameters.AddWithValue("@an", Session["SESan"]);
            cmd.Connection = con;
            cmd.ExecuteNonQuery();
        }
        else
        {
            cmd.CommandText = "UPDATE proprietariparcele  set numeProprietar = @numeProprietar, cotaParte=@cotaParte where idParcela = @idParcela and idGospodarie = '" + Session["SESgospodarieId"] + "' and id=@id ";
            cmd.Parameters.Add("@id", SqlDbType.Int).Value = Convert.ToInt32(proprietariGridView.DataKeys[e.RowIndex].Value);
            cmd.Parameters.Add("@idParcela", SqlDbType.Int).Value = Convert.ToInt32(Session["idParcela"]);
            cmd.Parameters.Add("@numeProprietar", SqlDbType.VarChar).Value = ((TextBox)proprietariGridView.Rows[e.RowIndex].Cells[1].Controls[0]).Text;
            cmd.Parameters.Add("@cotaParte", SqlDbType.VarChar).Value = ((TextBox)proprietariGridView.Rows[e.RowIndex].Cells[2].Controls[0]).Text;
            cmd.Connection = con;
            cmd.ExecuteNonQuery();
        }


        proprietariGridView.EditIndex = -1;
        BindData();
        ManipuleazaBD.InchideConexiune(con);
    }


    protected void adaugaProprietarButton_Click(object sender, EventArgs e)
    {
        SqlConnection con = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlDataAdapter da = new SqlDataAdapter(@"SELECT id , coalesce(numeProprietar,'') as numeProprietar , coalesce(cotaParte,'') as cotaParte, parcele.parcelaDenumire, parcele.parcelaNrCadastral ,parcele.parcelaNrTopo,parcele.parcelaCF, parcele.parcelaSuprafataIntravilanHa,parcele.parcelaSuprafataIntravilanAri, parcele.parcelaSuprafataExtravilanHa, parcele.parcelaSuprafataExtravilanAri  FROM  proprietariparcele join parcele on parcele.parcelaId = proprietariparcele.idParcela   where proprietariparcele.an =" + Session["SESan"] + " AND idParcela = " + Session["idParcela"].ToString() + "", con);
        DataTable dt = new DataTable();
        da.Fill(dt);
        DataRow dr = dt.NewRow();
        dt.Rows.InsertAt(dr, 0);
        proprietariGridView.EditIndex = 0;
        proprietariGridView.DataSource = dt;
        proprietariGridView.DataBind();
        ((LinkButton)proprietariGridView.Rows[0].Cells[0].Controls[0]).Text = "Insert";
        ManipuleazaBD.InchideConexiune(con);
    }

    protected void proprietariGridView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        SqlConnection con = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "DELETE FROM proprietariparcele WHERE idParcela = @idParcela and idGospodarie = @idGospodarie and id=@id";
        cmd.Parameters.Add("@id", SqlDbType.Int).Value = Convert.ToInt32(proprietariGridView.DataKeys[e.RowIndex].Value);
        cmd.Parameters.Add("@idParcela", SqlDbType.Int).Value = Convert.ToInt32(Session["idParcela"]);
        cmd.Parameters.Add("@idGospodarie", SqlDbType.VarChar).Value = Session["SESgospodarieId"];
        cmd.Connection = con;
        cmd.ExecuteNonQuery();
        BindData();
    }

    protected void listaParcele_Click(object sender, EventArgs e)
    {
        Response.Redirect(@"capitol2b.aspx");
    }
}