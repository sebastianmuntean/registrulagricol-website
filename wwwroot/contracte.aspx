﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="contracte.aspx.cs" Inherits="contracte" UICulture="ro-Ro" Culture="ro-Ro"
    EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="CONTRACTE PRIVIND MODUL DE UTILIZARE A SUPRAFEŢELOR AGRICOLE" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="upParcele" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel ID="pnAfiseazaMesaje" runat="server">
                <asp:ValidationSummary ID="valSumParcele" runat="server" DisplayMode="SingleParagraph"
                    Visible="true" ValidationGroup="GrupValidareParcele" CssClass="validator" ForeColor="" />
            </asp:Panel>
            <asp:Panel ID="Panel5" CssClass="ascunsa" runat="server">
                <!-- //////////// VALIDATOARE  \\\\\\\\\\\\\\\\\\ -->
                <asp:CustomValidator ID="valCustom" runat="server" ErrorMessage="x" ValidationGroup="GrupValidareParcele"></asp:CustomValidator>
            </asp:Panel>
            <asp:Panel ID="pnlListaContracte" CssClass="panel_general" runat="server" Visible="true">
                <asp:Panel ID="Panel7" CssClass="" runat="server" Visible="true">
                    <asp:Panel ID="Panel6" runat="server" CssClass="cauta">
                        <asp:Label ID="lblCCauta" runat="server" Text="Caută: " />
                        <asp:Label ID="lblCInceput" runat="server" Text="Derulate în perioada" />
                        <asp:TextBox ID="tbCInceput" runat="server" Width="80" Format="dd.MM.yyyy" onchange="this.value=ValidareData(this.value, 'dd.MM.yyyy')"
                            AutoPostBack="true" OnTextChanged="tbCInceput_TextChanged"></asp:TextBox>
                        <asp:CalendarExtender ID="caltbCInceput" runat="server" Enabled="True" Format="dd.MM.yyyy"
                            PopupButtonID="tbCInceput" TargetControlID="tbCInceput">
                        </asp:CalendarExtender>
                        <asp:Label ID="lblCFinal" runat="server" Text="-" />
                        <asp:TextBox ID="tbCFinal" AutoPostBack="true" runat="server" Width="80" Format="dd.MM.yyyy"
                            onchange="this.value=ValidareData(this.value, 'dd.MM.yyyy')" OnTextChanged="tbCFinal_TextChanged"></asp:TextBox>
                        <asp:CalendarExtender ID="caltbCFinal" runat="server" Enabled="True" Format="dd.MM.yyyy"
                            PopupButtonID="tbCFinal" TargetControlID="tbCFinal">
                        </asp:CalendarExtender>
                        <asp:Label ID="lblCNrContract" runat="server" Text="Nr. contract"></asp:Label>
                        <asp:TextBox ID="tbCNrContract" AutoPostBack="true" runat="server" OnTextChanged="tbCNrContract_TextChanged"></asp:TextBox>
                        <asp:Label ID="Label3" runat="server" Text="Tip contract"></asp:Label>
                        <asp:DropDownList ID="tipContractDropDown" runat="server" AutoPostBack="true" OnSelectedIndexChanged="tipContractDropDown_SelectedIndexChanged" AppendDataBoundItems="True">
                            <asp:ListItem Text="toate" Value="%"></asp:ListItem>
                            <asp:ListItem Text="primite in arenda" Value="3"></asp:ListItem>
                            <asp:ListItem Text="primite in parte" Value="4"></asp:ListItem>
                            <asp:ListItem Text="primite cu titlu gratuit" Value="5"></asp:ListItem>
                            <asp:ListItem Text="primite in concesiune" Value="6"></asp:ListItem>
                            <asp:ListItem Text="primite in asociere" Value="7"></asp:ListItem>
                            <asp:ListItem Text="primite sub alte forme" Value="8"></asp:ListItem>
                            <asp:ListItem Text="date in arenda" Value="10"></asp:ListItem>
                            <asp:ListItem Text="date in parte" Value="11"></asp:ListItem>
                            <asp:ListItem Text="date cu titlu gratuit" Value="12"></asp:ListItem>
                            <asp:ListItem Text="date in concesiune" Value="13"></asp:ListItem>
                            <asp:ListItem Text="date in asociere" Value="14"></asp:ListItem>
                            <asp:ListItem Text="date sub alte forme" Value="15"></asp:ListItem>
                            <%--<asp:ListItem Text="din suprafata agricola data - la unitati cu personalitate juridica" Value="16"></asp:ListItem>--%>
                        </asp:DropDownList>
                        <asp:Label ID="lblIncludeReziliateExpirate" runat="server" Text="Include expirate/reziliate" />
                        <asp:CheckBox ID="ckbIncludeReziliateExpirate" runat="server" AutoPostBack="true" />
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel ID="Panel8" CssClass="" runat="server" Visible="true">
                    <asp:GridView ID="gvModUtilizareContracte" runat="server" AutoGenerateColumns="False"
                        DataSourceID="SqlModUtilizareContracteGrid" AllowPaging="True" AllowSorting="True" PageSize="20"
                        CssClass="tabela" OnRowDataBound="gvModUtilizareContracte_RowDataBound"
                        DataKeyNames="contractId" OnSelectedIndexChanged="gvModUtilizareContracte_SelectedIndexChanged"
                        ShowFooter="True" EmptyDataText="nu există contracte introduse" OnDataBound="gvModUtilizareContracte_DataBound" EnableModelValidation="True">
                        <Columns>
                            <asp:BoundField DataField="denumire1" HeaderText="Tip contract" SortExpression="denumire1" />
                            <asp:BoundField DataField="contractId" HeaderText="contractId" SortExpression="contractId"
                                Visible="False" />
                            <asp:BoundField DataField="contractNr" HeaderText="contract număr" SortExpression="contractNr"
                                Visible="true" />
                            <asp:BoundField DataField="contractDataSemnarii" HeaderText="data semnării" SortExpression="contractDataSemnarii"
                                Visible="true" DataFormatString="{0:d}" />
                            <asp:BoundField DataField="contractGospodarieIdDa" HeaderText="contractGospodarieIdDa"
                                SortExpression="contractGospodarieIdDa" Visible="false" />
                            <asp:BoundField DataField="contractGospodarieIdPrimeste" HeaderText="contractGospodarieIdPrimeste"
                                SortExpression="contractGospodarieIdPrimeste" Visible="false" />
                            <asp:TemplateField HeaderText="valabil între anii" SortExpression="contractDataFinal">
                                <ItemTemplate>
                                    <asp:Label ID="lblDataInceput" runat="server" Text='<%# Bind("contractDataInceput", "{0:yyyy}") %>'></asp:Label>

                                    <asp:Label ID="lblDataFinal" runat="server" Text='<%# Bind("contractDataFinal", "{0:yyyy}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Activ / Inactiv" SortExpression="Activ">
                                <ItemTemplate>
                                    <asp:Label ID="lblGvContracteActiv" runat="server" Text='<%# Bind("Activ") %>'></asp:Label>
                                    <asp:Label ID="lblExpirat" runat="server" CssClass="validator rosu" Text="expirat/reziliat"
                                        Visible="false"></asp:Label>
                                    <asp:Label ID="lblInCurs" runat="server" CssClass="validator verde" Text="în curs"
                                        Visible="false"></asp:Label>
                                    <asp:Label ID="lblNeinceput" runat="server" CssClass="validator galben" Text="neînceput"
                                        Visible="false"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="detalii contract" SortExpression="contractDetalii">
                                <ItemTemplate>
                                    <asp:Label ID="lblGvContracteDetalii" runat="server" Text='<%# Bind("contractDetalii") %>'></asp:Label>
                                    <asp:Label ID="lblGvContracteId" runat="server" Text='<%# Bind("contractId") %>'
                                        Visible="false"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="gospodăria care dă" SortExpression="gospodarie1">
                                <ItemTemplate>
                                    <asp:Label ID="lblNumeDa" runat="server" Text=''></asp:Label>
                                    <asp:Label ID="lblGospodarieDa" runat="server" Text='<%# Bind("gospodarie1") %>'></asp:Label>
                                    <asp:Label ID="lblIdDa" runat="server" Text='<%# Bind("idDa") %>' Visible="false"></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    TOTAL GENERAL (toate paginile): 
                                    <asp:Label ID="lblHaTotalGeneral" runat="server" Text='<%# Bind("haTotal") %>' Font-Bold="true"></asp:Label>ha 
                                    <asp:Label ID="lblAriTotalGeneral" runat="server" Text='<%# Bind("ariTotal") %>' Font-Bold="true"></asp:Label>ari
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="gospodăria care primeşte" SortExpression="gospodarie">
                                <ItemTemplate>
                                    <asp:Label ID="lblNumePrimeste" runat="server" Text=''></asp:Label>
                                    <asp:Label ID="lblGospodariePrimeste" runat="server" Text='<%# Bind("gospodarie") %>'></asp:Label>
                                    <asp:Label ID="lblIdPrimeste" runat="server" Text='<%# Bind("idPrimeste") %>' Visible="false"></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="Label2" runat="server" Text="TOTAL PAGINA" Font-Bold="true"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Total Ha" SortExpression="ha">
                                <ItemTemplate>
                                    <asp:Label ID="lblHa" runat="server" Text='<%# Bind("ha", "{0:F0}") %>'></asp:Label>
                                    <asp:Label ID="lblHaTotalPeRand" runat="server" Text='<%# Bind("haTotal", "{0:F0}") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblAriTotalPeRand" runat="server" Text='<%# Bind("ariTotal", "{0:F2}") %>' Visible="false"></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblHaT" runat="server" Text="0,00" Font-Bold="true"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Total Ari" SortExpression="ari">
                                <ItemTemplate>
                                    <asp:Label ID="lblAri" runat="server" Text='<%# Bind("ari", "{0:F2}") %>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblAriT" runat="server" Text="0,00" Font-Bold="true"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Redevența" SortExpression="redeventa">
                                <ItemTemplate>
                                    <asp:Label ID="lblRedeventa" runat="server" Text='<%# Bind("redeventa", "{0:F2}") %>'></asp:Label>
                                    <asp:Label ID="lblMonedaText" runat="server" Text='<%# Bind("MonedaDenumire") %>'></asp:Label>
                                    <asp:Label ID="lblMonedaId" runat="server" Text='<%# Bind("MonedaId") %>' Visible="false"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField ReadOnly="True" HeaderText="Redevența(lei)"
                                InsertVisible="False" DataField="redeventa"
                                SortExpression="redeventa" Visible="false">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundField>
                        </Columns>
                        <FooterStyle BackColor="#FFFF99" ForeColor="#990000" />
                        <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlModUtilizareContracteGrid" runat="server" SelectCommand="SELECT">
                        <SelectParameters>
                            <asp:SessionParameter Name="gospodarie" SessionField="SESgospodarieId" />
                            <asp:SessionParameter Name="an" SessionField="SESan" Type="Int32" />
                            <asp:ControlParameter ControlID="tbCInceput" Name="data1" PropertyName="Text" DefaultValue="" />
                            <asp:ControlParameter ControlID="tbCFinal" Name="data2" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbCNrContract" DefaultValue="%" Name="Numar" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tipContractDropDown" Name="tipContract" PropertyName="Text" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </asp:Panel>
                <asp:Panel ID="Panel17" runat="server" CssClass="butoane">
                    <asp:Button ID="btAdaugaContracte" runat="server" CssClass="btn btn-success" Text="adaugă contract nou"
                        OnClick="btAdaugaContracte_Click" />
                    <asp:Button ID="btModificaContracte" runat="server" CssClass="btn btn-warning" Text="modifică"
                        OnClick="btModificaContracte_Click" Visible="false" />
                    <asp:Button ID="btStergeContracte" runat="server" CssClass="btn btn-danger" Text="reziliază"
                        OnClick="btStergeContracte_Click" Visible="false" />
                    <asp:Button ID="btListaParcelelor" runat="server" CssClass="btn btn-primary" Text="lista parcelelor"
                        Visible="true" PostBackUrl="~/Capitol2b.aspx" />
                    <asp:Button ID="btTiparireContracte" runat="server" CssClass="btn btn-info" Text="tipărire contracte"
                        Visible="true" OnClick="btTiparireContracte_Click" />
                    <asp:Button ID="Button1" runat="server" CssClass="btn btn-info" Text="salvează listă contracte în Excel"
                        Visible="true" OnClick="btTiparireContracteXls_Click" />
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="pnContracteUtilizare" CssClass="panel_general" runat="server" Visible="false">
                <asp:Panel ID="Panel13" CssClass="adauga" runat="server">
                    <asp:Panel ID="Panel14" runat="server">
                        <h2>DETALII CONTRACT
                        </h2>
                    </asp:Panel>
                    <asp:Panel ID="pnlDetalii1" runat="server" CssClass="linie_clear">
                        <asp:Label ID="Label7" runat="server" Text="Nr. contract" CssClass="indent" />
                        <asp:TextBox ID="tbContractNr" runat="server" Width="80"></asp:TextBox>
                        <asp:Label ID="Label8" runat="server" Text="din data" CssClass="indent" />
                        <asp:TextBox ID="tbContractDinData" runat="server" Width="80" Format="dd.MM.yyyy"
                            onchange="this.value=ValidareData(this.value, 'dd.MM.yyyy')"></asp:TextBox>
                        <asp:CalendarExtender ID="tbContractDinData_CalendarExtender" runat="server" Enabled="True"
                            Format="dd.MM.yyyy" PopupButtonID="tbContractDinData" TargetControlID="tbContractDinData">
                        </asp:CalendarExtender>
                    </asp:Panel>
                    <asp:Panel ID="Panel15" runat="server" CssClass="linie_clear">
                        <asp:Label ID="Label6" runat="server" Text="Redeventa" CssClass="indent" />
                        <asp:TextBox ID="redeventaTextBox" runat="server" Width="80px"></asp:TextBox>
                        <asp:Label ID="lblMonede" runat="server" Text="Moneda" CssClass="indent" />
                        <asp:DropDownList ID="ddlMonede" runat="server" DataSourceID="SqlDataSourceMonede" DataTextField="Denumire" DataValueField="Id">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="SqlDataSourceMonede" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString %>" SelectCommand="SELECT Id, Denumire + ' (' + Simbol + ')' AS Denumire FROM dbo.Monede"></asp:SqlDataSource>
                    </asp:Panel>
                    <asp:Panel ID="pnlDetalii2" runat="server" CssClass="linie_clear">
                        <asp:Label ID="Label4" runat="server" Text="Mod de utilizare:" CssClass="indent" />
                        <asp:DropDownList ID="ddlModUtilizareContract" runat="server" AutoPostBack="true"
                            OnSelectedIndexChanged="ddlModUtilizareContract_SelectedIndexChanged" OnInit="ddlModUtilizareContract_Init1"
                            OnLoad="ddlModUtilizareContract_Load" OnPreRender="ddlModUtilizareContract_PreRender">
                        </asp:DropDownList>
                    </asp:Panel>
                    <asp:Panel ID="pnlContracteDetalii" runat="server" Visible="false" CssClass="linie_clear">
                        <asp:Panel ID="Panel1" runat="server" Visible="true" CssClass="linie_clear">
                            <asp:Label ID="Label5" runat="server" Text="Între gospodăria curentă:" />
                            <strong>
                                <asp:Label ID="lblTextGospodariaCurenta" runat="server" Text="" /></strong>
                            <asp:Label ID="lblTextSi" runat="server" Text="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; şi &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" />
                            <strong>
                                <asp:Label ID="lblSiGospodaria" runat="server" Text=" ??? " /></strong>
                            <asp:LinkButton ID="btAlegeGospodaria" runat="server" OnClick="btAlegeGospodaria_Click">alege o gospodărie</asp:LinkButton>
                            <asp:Label ID="lblGospodarieId" runat="server" Text="" Visible="false" />
                            <asp:Label ID="lblGospodarieIdGrid" runat="server" Text="" Visible="false" />
                            <asp:Label ID="lblGospodarieIdSens" runat="server" Text="" Visible="false" />
                        </asp:Panel>
                        <asp:Panel ID="pnl" runat="server" Visible="true" CssClass="linie_clear">
                            <asp:Label ID="lblTextDataInceputContract" runat="server" CssClass="indent" Text="Valabil pentru anii agricoli: " />
                            <asp:DropDownList ID="ddlDataInceputContract" runat="server">
                                <asp:ListItem Text="2007" Value="2007"></asp:ListItem>
                                <asp:ListItem Text="2008" Value="2008"></asp:ListItem>
                                <asp:ListItem Text="2009" Value="2009"></asp:ListItem>
                                <asp:ListItem Text="2010" Value="2010"></asp:ListItem>
                                <asp:ListItem Text="2011" Value="2011"></asp:ListItem>
                                <asp:ListItem Text="2012" Value="2012"></asp:ListItem>
                                <asp:ListItem Text="2013" Value="2013"></asp:ListItem>
                                <asp:ListItem Text="2014" Value="2014"></asp:ListItem>
                                <asp:ListItem Text="2015" Value="2015"></asp:ListItem>
                                <asp:ListItem Text="2016" Value="2016"></asp:ListItem>
                                <asp:ListItem Text="2017" Value="2017"></asp:ListItem>
                                <asp:ListItem Text="2018" Value="2018"></asp:ListItem>
                                <asp:ListItem Text="2019" Value="2019"></asp:ListItem>
                                <asp:ListItem Text="2020" Value="2020"></asp:ListItem>
                                <asp:ListItem Text="2021" Value="2021"></asp:ListItem>
                                <asp:ListItem Text="2022" Value="2022"></asp:ListItem>
                                <asp:ListItem Text="2023" Value="2023"></asp:ListItem>
                                <asp:ListItem Text="2024" Value="2024"></asp:ListItem>
                                <asp:ListItem Text="2025" Value="2025"></asp:ListItem>
                                <asp:ListItem Text="2026" Value="2026"></asp:ListItem>
                                <asp:ListItem Text="2027" Value="2027"></asp:ListItem>
                                <asp:ListItem Text="2028" Value="2028"></asp:ListItem>
                                <asp:ListItem Text="2029" Value="2029"></asp:ListItem>
                                <asp:ListItem Text="2030" Value="2030"></asp:ListItem>
                                <asp:ListItem Text="2031" Value="2031"></asp:ListItem>
                                <asp:ListItem Text="2032" Value="2032"></asp:ListItem>
                                <asp:ListItem Text="2033" Value="2033"></asp:ListItem>
                                <asp:ListItem Text="2034" Value="2034"></asp:ListItem>
                                <asp:ListItem Text="2035" Value="2035"></asp:ListItem>
                                <asp:ListItem Text="2036" Value="2036"></asp:ListItem>
                                <asp:ListItem Text="2037" Value="2037"></asp:ListItem>
                                <asp:ListItem Text="2038" Value="2038"></asp:ListItem>
                                <asp:ListItem Text="2039" Value="2039"></asp:ListItem>
                                <asp:ListItem Text="2040" Value="2040"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:Label ID="lblTextDataFinalContract" runat="server" Text=" - " />
                            <asp:DropDownList ID="ddlDataFinalContract" runat="server">
                                <asp:ListItem Text="2011" Value="2011"></asp:ListItem>
                                <asp:ListItem Text="2012" Value="2012"></asp:ListItem>
                                <asp:ListItem Text="2013" Value="2013"></asp:ListItem>
                                <asp:ListItem Text="2014" Value="2014"></asp:ListItem>
                                <asp:ListItem Text="2015" Value="2015"></asp:ListItem>
                                <asp:ListItem Text="2016" Value="2016"></asp:ListItem>
                                <asp:ListItem Text="2017" Value="2017"></asp:ListItem>
                                <asp:ListItem Text="2018" Value="2018"></asp:ListItem>
                                <asp:ListItem Text="2019" Value="2019"></asp:ListItem>
                                <asp:ListItem Text="2020" Value="2020"></asp:ListItem>
                                <asp:ListItem Text="2021" Value="2021"></asp:ListItem>
                                <asp:ListItem Text="2022" Value="2022"></asp:ListItem>
                                <asp:ListItem Text="2023" Value="2023"></asp:ListItem>
                                <asp:ListItem Text="2024" Value="2024"></asp:ListItem>
                                <asp:ListItem Text="2025" Value="2025"></asp:ListItem>
                                <asp:ListItem Text="2026" Value="2026"></asp:ListItem>
                                <asp:ListItem Text="2027" Value="2027"></asp:ListItem>
                                <asp:ListItem Text="2028" Value="2028"></asp:ListItem>
                                <asp:ListItem Text="2029" Value="2029"></asp:ListItem>
                                <asp:ListItem Text="2030" Value="2030"></asp:ListItem>
                                <asp:ListItem Text="2031" Value="2031"></asp:ListItem>
                                <asp:ListItem Text="2032" Value="2032"></asp:ListItem>
                                <asp:ListItem Text="2033" Value="2033"></asp:ListItem>
                                <asp:ListItem Text="2034" Value="2034"></asp:ListItem>
                                <asp:ListItem Text="2035" Value="2035"></asp:ListItem>
                                <asp:ListItem Text="2036" Value="2036"></asp:ListItem>
                                <asp:ListItem Text="2037" Value="2037"></asp:ListItem>
                                <asp:ListItem Text="2038" Value="2038"></asp:ListItem>
                                <asp:ListItem Text="2039" Value="2039"></asp:ListItem>
                                <asp:ListItem Text="2040" Value="2040"></asp:ListItem>
                            </asp:DropDownList>
                        </asp:Panel>
                        <asp:Panel ID="Panel2" runat="server" Visible="true" CssClass="linie_clear">
                            <asp:Label ID="lblDetaliiContract" runat="server" CssClass="indent" Text="Detalii contract:" />
                            <br />
                            <asp:TextBox ID="tbDetaliiContract" runat="server" Rows="4" Width="600" TextMode="MultiLine"></asp:TextBox>
                        </asp:Panel>
                        <asp:Panel ID="Panel3" runat="server" Visible="true" CssClass="linie_clear">
                            <asp:Label ID="lblTextSelecteaza" runat="server" Text="Bifează parcelele care fac obiectul contractului:"
                                Visible="true"></asp:Label>
                            <asp:GridView ID="gvListaParcele" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                CssClass="tabela" DataSourceID="SqlListaParcele" OnSelectedIndexChanged="gvListaParcele_SelectedIndexChanged"
                                OnDataBound="gvListaParcele_DataBound" OnPreRender="gvListaParcele_PreRender"
                                OnRowDataBound="gvListaParcele_RowDataBound" ShowFooter="True" EnableModelValidation="True">
                                <Columns>
                                    <asp:TemplateField HeaderText="face obiectul contractului">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="ckbParcelaFaceObiectul" runat="server" Checked="false" AutoPostBack="true"
                                                OnCheckedChanged="ckbParcelaFaceObiectul_CheckedChanged" />
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:CheckBox ID="ckbParcelaSelecteazaTot" runat="server" Checked="false" AutoPostBack="true"
                                                OnCheckedChanged="ckbParcelaSelecteazaTot_CheckedChanged" />
                                            <br />
                                            selectează toate
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="detaliiParcela" HeaderText="detalii parcela" ReadOnly="True"
                                        SortExpression="detaliiParcela" ItemStyle-HorizontalAlign="Left">
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="suprafaţa totală" SortExpression="haTotal">
                                        <ItemTemplate>
                                            <asp:Label ID="lblHaTotal" runat="server" Text='<%# Bind("haTotal") %>'></asp:Label>ha
                                            <asp:Label ID="lblAriTotal" runat="server" Text='<%# Bind("ariTotal") %>'></asp:Label>ari
                                            <asp:Label ID="lblParcelaId" runat="server" Text='<%# Bind("parcelaId") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="suprafaţa aferentă acestui contract">
                                        <ItemTemplate>
                                            <asp:TextBox ID="tbAlocaHa" runat="server" Width="40"></asp:TextBox>ha
                                            <asp:TextBox ID="tbAlocaAri" runat="server" Width="60"></asp:TextBox>ari (maxim
                                            <asp:Label ID="lblHaMaxim" runat="server" Text='<%# Bind("haOcupate") %>'></asp:Label>ha
                                            <asp:Label ID="lblAriMaxim" runat="server" Text='<%# Bind("ariOcupate") %>'></asp:Label>ari)
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#FFFDDE" Font-Bold="true" ForeColor="Red" />
                            </asp:GridView>
                            <asp:SqlDataSource ID="SqlListaParcele" runat="server"
                                SelectCommand="SELECT  parcele.parcelaId, parcele.parcelaDenumire + ', ' + sabloaneCapitole.denumire1 + '(' + COALESCE (parcele.parcelaCategorie, N'-') + ') ' + CASE COALESCE (parcele.parcelaNrBloc , N'-') WHEN '' THEN '' ELSE ', bloc fizic ' + COALESCE (parcele.parcelaNrBloc , N'-') END + CASE COALESCE (parcele.parcelaNrTopo , N'-') WHEN '' THEN '' ELSE ', nr.topo ' + COALESCE (parcele.parcelaNrTopo , N'-') END + CASE COALESCE (parcele.parcelaNrCadastral , N'-') WHEN '' THEN '' ELSE ',  nr. cadastral ' + COALESCE (parcele.parcelaNrCadastral , N'-') END + CASE COALESCE (parcele.parcelaLocalitate , N'-') WHEN '' THEN '' ELSE ',  din ' + COALESCE (parcele.parcelaLocalitate , N'-') END + CASE COALESCE (parcele.parcelaAdresa , N'-') WHEN '' THEN '' ELSE ',  ' + COALESCE (parcele.parcelaAdresa , N'-') END AS detaliiParcela, parcele.parcelaSuprafataIntravilanHa + parcele.parcelaSuprafataExtravilanHa AS haTotal, parcele.parcelaSuprafataIntravilanAri + parcele.parcelaSuprafataExtravilanAri AS ariTotal, SUM(CASE contractactiv WHEN 'True' THEN COALESCE (parceleModUtilizare.c3Ha , '0') ELSE '0' END) AS haOcupate, SUM(CASE contractactiv WHEN 'True' THEN COALESCE (parceleModUtilizare.c3Ari , '0') ELSE '0' END) AS ariOcupate FROM parceleAtribuiriContracte INNER JOIN parceleModUtilizare ON parceleAtribuiriContracte.contractId = parceleModUtilizare.contractId AND YEAR(parceleAtribuiriContracte.contractDataFinal) &gt;= @an RIGHT OUTER JOIN parcele INNER JOIN sabloaneCapitole ON parcele.parcelaCategorie = sabloaneCapitole.codRand ON parceleModUtilizare.parcelaId = parcele.parcelaId WHERE (parcele.an = @an) AND (parcele.gospodarieId = @gospodarieId) AND (sabloaneCapitole.capitol = '2a') AND (sabloaneCapitole.an = @an)
                                 GROUP BY parcele.parcelaId, parcele.parcelaNrTopo, parcele.parcelaNrCadastral, parcele.parcelaDenumire, parcele.parcelaCategorie, parcele.parcelaNrBloc, parcele.parcelaSuprafataExtravilanHa, parcele.parcelaAdresa, parcele.parcelaLocalitate, parcele.parcelaSuprafataIntravilanHa, parcele.parcelaSuprafataIntravilanAri, parcele.parcelaSuprafataExtravilanAri, sabloaneCapitole.denumire1 ORDER BY parcele.parcelaDenumire">
                                <SelectParameters>
                                    <asp:SessionParameter Name="an" SessionField="SESan" Type="Int32" />
                                    <asp:ControlParameter ControlID="lblGospodarieIdGrid" Name="gospodarieId" PropertyName="Text"
                                        Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel ID="Panel16" runat="server" CssClass="butoane">
                        <asp:Button CssClass="buton" ID="btAdaugaModUtilizareContractSalveaza" runat="server"
                            Text="salvează contract" Visible="false" OnClick="btModUtilizareContractSalveaza_Click" />
                        <asp:Button CssClass="buton" ID="btListaContracte" runat="server" Text="listă contracte"
                            Visible="true" OnClick="btListaContracte_Click" />
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="pnlReziliaza" CssClass="panel_general" runat="server" Visible="false">
                <asp:Panel ID="Panel10" CssClass="adauga" runat="server">
                    <asp:Panel ID="Panel11" runat="server">
                        <h2>REZILIAZĂ CONTRACTUL
                        </h2>
                    </asp:Panel>
                    <asp:Panel ID="pnlReziliaza1" runat="server" CssClass="linie_clear">
                        <asp:Label ID="lblDataRezilierii" runat="server" Text="Data rezilierii" CssClass="indent" />
                        <asp:TextBox ID="tbDataRezilierii" runat="server" Width="80" Format="dd.MM.yyyy"
                            onchange="this.value=ValidareData(this.value, 'dd.MM.yyyy')"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd.MM.yyyy"
                            PopupButtonID="tbDataRezilierii" TargetControlID="tbDataRezilierii">
                        </asp:CalendarExtender>
                    </asp:Panel>
                    <asp:Panel ID="Panel12" runat="server" Visible="true" CssClass="linie_clear">
                        <asp:Label ID="Label1" runat="server" CssClass="indent" Text="Detalii reziliere:" />
                        <br />
                        <asp:TextBox ID="tbMotivulRezilierii" runat="server" Rows="4" Width="600" TextMode="MultiLine"></asp:TextBox>
                    </asp:Panel>
                    <asp:Panel ID="Panel9" runat="server" Visible="true" CssClass="linie_clear">
                        <asp:Label ID="lblAnulatiDin3" runat="server" CssClass="indent" Text="Anulaţi datele deja introduse în Capitolul 3?"
                            Width="270" />
                        <asp:CheckBox ID="ckbAnulatiDin3" runat="server" />
                    </asp:Panel>
                    <asp:Panel ID="Panel23" runat="server" CssClass="butoane">
                        <asp:Button CssClass="buton" ID="btSalveazaRezilierii" runat="server" Text="reziliază contractul"
                            Visible="true" OnClick="btSalveazaReziliere_Click" />
                        <asp:Button CssClass="buton" ID="btListaContracteReziliere" runat="server" Text="listă contracte"
                            Visible="true" OnClick="btListaContracte_Click" />
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="popup" runat="server" Visible="false">
                <asp:Panel ID="pnlPopUpGospodarii" runat="server" CssClass="popupBack" Visible="true">
                </asp:Panel>
                <asp:Panel ID="pnListaGospodarii" runat="server" CssClass="panel_general popupPanel"
                    Visible="true">
                    <asp:Panel ID="Panel4" runat="server">
                        <asp:Label ID="lblAlegetiGospodariMesaj" runat="server" CssClass="heading" Text="Alegeţi una din gospodăriile de mai jos"
                            Visible="true" />
                    </asp:Panel>
                    <asp:Panel ID="pnCautare" runat="server" CssClass="cauta">
                        <asp:Label ID="lbCautaDupa" runat="server" Text="Caută după:"></asp:Label>
                        <asp:Label ID="lbfVolum" runat="server" Text="Volum"></asp:Label>
                        <asp:TextBox ID="tbfVolum" Width="20px" runat="server" AutoPostBack="True"></asp:TextBox>
                        <asp:Label ID="lbfNrPoz" runat="server" Text="Nr. poziţie"></asp:Label>
                        <asp:TextBox ID="tbfNrPoz" Width="20px" runat="server" AutoPostBack="True"></asp:TextBox>
                        <asp:Label ID="ldfNume" runat="server" Text="Nume"></asp:Label>
                        <asp:TextBox ID="tbfNume" Width="80px" runat="server" AutoPostBack="True"></asp:TextBox>
                        <asp:Label ID="cnpLabel" runat="server" Text="CNP"></asp:Label>
                        <asp:TextBox ID="cnptextBox" Width="120px" runat="server" AutoPostBack="True"></asp:TextBox>
                        <asp:Label ID="lbfLoc" runat="server" Text="Localitate"></asp:Label>
                        <asp:TextBox ID="tbfLoc" Width="80px" runat="server" AutoPostBack="True"></asp:TextBox>
                        <asp:Label ID="lbfStrada" runat="server" Text="Stradă"></asp:Label>
                        <asp:TextBox ID="tbfStrada" Width="80px" runat="server" AutoPostBack="True"></asp:TextBox>
                        <asp:Label ID="lbfNr" runat="server" Text="Nr."></asp:Label>
                        <asp:TextBox ID="tbfNr" Width="20px" runat="server" AutoPostBack="True"></asp:TextBox>
                    </asp:Panel>
                    <asp:Panel ID="pnGrid" runat="server" Visible="true">
                        <asp:GridView ID="gvGospodarii" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            BorderStyle="None" CellPadding="3" GridLines="Vertical" ShowFooter="True" CssClass="tabela"
                            DataKeyNames="gospodarieId" DataSourceID="SqlSabloaneCapitole" EmptyDataText="Nu sunt adaugate inregistrari"
                            AllowPaging="True" OnRowDataBound="gvGospodarii_RowDataBound" OnSelectedIndexChanged="gvGospodarii_SelectedIndexChanged">
                            <Columns>
                                <asp:BoundField DataField="volum" HeaderText="Volum" SortExpression="volum" />
                                <asp:BoundField DataField="nrPozitie" HeaderText="Număr poziţie" SortExpression="nrPozitie" />
                                <asp:BoundField DataField="nume" HeaderText="Nume" SortExpression="nume" />
                                <asp:BoundField DataField="cnp" HeaderText="CNP" SortExpression="cnp" />
                                <asp:BoundField DataField="codSiruta" HeaderText="Cod şiruta" SortExpression="codSiruta" />
                                <asp:BoundField DataField="tip" HeaderText="Tip" SortExpression="tip" />
                                <asp:BoundField DataField="strada" HeaderText="Strada" SortExpression="strada" />
                                <asp:BoundField DataField="nr" HeaderText="Număr" SortExpression="nr" />
                                <asp:BoundField DataField="nrInt" HeaderText="Număr întreg" SortExpression="nrInt" />
                                <asp:BoundField DataField="codExploatatie" HeaderText="Cod exploataţie" SortExpression="codExploatatie" />
                                <asp:BoundField DataField="codUnic" HeaderText="Cod unic" SortExpression="codUnic" />
                                <asp:BoundField DataField="judet" HeaderText="Judeţ" SortExpression="judet" />
                                <asp:BoundField DataField="localitate" HeaderText="Localitate" SortExpression="localitate" />
                            </Columns>
                            <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                            <HeaderStyle Font-Bold="True" />
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlSabloaneCapitole" runat="server"
                            SelectCommand="SELECT gospodarii.gospodarieId, gospodarii.unitateId, gospodarii.volum, gospodarii.nrPozitie, gospodarii.volumInt, gospodarii.nrPozitieInt, gospodarii.codSiruta, gospodarii.membruCNP as cnp,gospodarii.gospodarieCui, CASE gospodarii.tip WHEN '1' THEN 'Localnic' WHEN '2' THEN 'Străinaş' WHEN '3' THEN 'Firmă pe raza localităţii' ELSE 'Firmă străinaşă' END AS tip, CASE strainas WHEN 1 THEN sstrada ELSE strada END AS strada, CASE strainas WHEN 1 THEN snr ELSE nr END AS nr, gospodarii.nrInt, gospodarii.bl, gospodarii.sc, gospodarii.et, gospodarii.ap, gospodarii.codExploatatie, gospodarii.codUnic, CASE strainas WHEN 1 THEN sjudet ELSE judet END AS judet, CASE strainas WHEN 1 THEN slocalitate ELSE localitate END AS localitate, gospodarii.persJuridica, gospodarii.jUnitate, gospodarii.jSubunitate, gospodarii.jCodFiscal, gospodarii.jNumeReprez, gospodarii.strainas, gospodarii.sStrada, gospodarii.sNr, gospodarii.sBl, gospodarii.sSc, gospodarii.sEtj, gospodarii.sAp, gospodarii.sJudet, gospodarii.sLocalitate, CASE persjuridica WHEN 0 THEN (SELECT TOP (1) nume FROM membri WHERE an = @an AND gospodarieId = gospodarii.gospodarieid AND codrudenie = 1) ELSE junitate END AS nume FROM gospodarii INNER JOIN unitati ON gospodarii.unitateId = unitati.unitateId WHERE (COALESCE (gospodarii.volum, N'') LIKE @volum) AND (COALESCE (gospodarii.nrPozitie, N'') LIKE @nrPozitie) AND (COALESCE (gospodarii.membruCNP, N'') LIKE '%' + @cnp + '%') AND (COALESCE (CASE persjuridica WHEN 0 THEN (SELECT TOP (1) nume FROM membri WHERE an = @an AND gospodarieId = gospodarii.gospodarieid AND codrudenie = 1) ELSE junitate END, N'') LIKE '%' + @nume + '%') AND (COALESCE (CASE strainas WHEN 1 THEN slocalitate ELSE localitate END, N'') LIKE '%' + @localitate + '%') AND (COALESCE (CASE strainas WHEN 1 THEN sstrada ELSE strada END, N'') LIKE '%' + @strada + '%') AND (COALESCE (CASE strainas WHEN 1 THEN snr ELSE nr END, N'') LIKE @nr) AND (gospodarii.an = @an) AND (CONVERT (nvarchar, gospodarii.unitateId) = @unitateId) ">
                            <SelectParameters>
                                <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" />
                                <asp:ControlParameter ControlID="tbfVolum" DefaultValue="%" Name="volum" PropertyName="Text" />
                                <asp:ControlParameter ControlID="tbfNrPoz" DefaultValue="%" Name="nrPozitie" PropertyName="Text" />
                                <asp:ControlParameter ControlID="tbfLoc" DefaultValue="%" Name="localitate" PropertyName="Text" />
                                <asp:ControlParameter ControlID="tbfStrada" DefaultValue="%" Name="strada" PropertyName="Text" />
                                <asp:ControlParameter ControlID="tbfNr" DefaultValue="%" Name="nr" PropertyName="Text" />
                                <asp:ControlParameter ControlID="tbfNume" DefaultValue="%" Name="nume" PropertyName="Text" />
                                <asp:ControlParameter ControlID="cnpTextBox" DefaultValue="%" Name="cnp" PropertyName="Text" />
                                <asp:SessionParameter Name="an" SessionField="SESan" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
