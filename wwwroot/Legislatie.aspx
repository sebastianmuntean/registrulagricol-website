﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" %>

<script runat="server">

    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Legislaţie" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="pnl1" runat="server" Visible="true">
        <asp:Panel ID="pnTabel" runat="server" CssClass="panel_general" Visible="true">
            <asp:Panel ID="pnCautare" runat="server" CssClass="cauta">
                <asp:Label ID="Label1" runat="server" Text="Legislaţia României privind Registrul Agricol"></asp:Label>
            </asp:Panel>
            <asp:Panel ID="Panel1" runat="server" CssClass="centralizatoare" Visible="true">
                <asp:HyperLink Target="_blank" ID="hlkLegislatie1" runat="server" NavigateUrl="http://legex.ro/ORDIN-95-4-21-2010-107288.aspx">ORDIN 95 4-21-2010</asp:HyperLink>
                <asp:HyperLink Target="_blank" ID="hlkLegislatie2" runat="server" NavigateUrl="http://legex.ro/ORDONANTA-28-27.08.2008-89138.aspx">ORDONANTA 28 27.08.2008</asp:HyperLink>
                <asp:HyperLink Target="_blank" ID="hlkLegislatie3" runat="server" NavigateUrl="http://legex.ro/LEGE-98-08.04.2009-95219.aspx">LEGE 98 08.04.2009</asp:HyperLink>
                <asp:HyperLink Target="_blank" ID="hlkLegislatie4" runat="server" NavigateUrl="http://legex.ro/HOTARARE-1632-29.12.2009-101870.aspx">HOTARARE 1632 29.12.2009</asp:HyperLink>
                <asp:HyperLink Target="_blank" ID="hlkLegislatie5" runat="server" NavigateUrl="http://legex.ro/ORDIN-39-28.04.2010-104304.aspx">ORDIN 39 28.04.2010</asp:HyperLink>
                <asp:HyperLink Target="_blank" ID="hlkLegislatie6" runat="server" NavigateUrl="http://www.legex.ro/Hotararea-218-2015-139751.aspx">Hotărârea 218 25.03.2015</asp:HyperLink>
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
</asp:Content>
