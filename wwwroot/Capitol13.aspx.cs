﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

public partial class Capitol13 : System.Web.UI.Page
{
    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        DBConnections connection = new DBConnections(Convert.ToInt16(Session["SESan"]));

        SqlSuccesibili.ConnectionString = connection.Create();

        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "Cap.13", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
            CompleteazaDateDefunct();
            CompleteazaJudeteSiLocalitati();
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["SESgospodarieId"] == null)
        {
            HttpCookie vCookie = Request.Cookies["COOKTNT"];
            vCookie = CriptareCookie.DecodeCookie(vCookie);
            if (vCookie != null)
            {
                if (vCookie["COOKgospodarieId"] != null)
                {
                    Session["SESgospodarieId"] = vCookie["COOKgospodarieId"].ToString();
                }
                else if (Request.Url.AbsolutePath.IndexOf("Gospodarii.aspx") == -1 && Session["SESgospodarieId"] == null)
                    Response.Redirect("~/Gospodarii.aspx");
            }
        }
    }

    protected void SuccesibiliGridView_SelectedIndexChanged(object sender, EventArgs e)
    {
        AdaugaSuccesibilPanelButton.Visible = true;
        btModificaSuccesibil.Visible = true;
        stergeSuccesibilButton.Visible = true;
        succesibiliGridView.DataBind();
    }

    protected void succesibiliGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(succesibiliGridView, e, this);
    }

    protected void btTiparesteCapitol_Click(object sender, EventArgs e)
    {
        ResponseHelper.Redirect("~/printCapitolul13.aspx?codCapitol=13", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
        ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "tipareste", "tiparire lista succesibili", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
    }

    protected void DefunctDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        CompleteazaDataDeces();
    }

    protected void JudeteDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (judeteDropDownList.SelectedIndex <= 0)
        {
            return;
        }

        localitatiDropDownList.Items.Clear();
        localitatiDropDownList.Items.Insert(0, new ListItem("- Selectati o localitate -"));
        
        CompleteazaLocalitati();
    }

    protected void ModificaSuccesibilButton_Click(object sender, EventArgs e)
    {

        ModificaSuccesibil();

        ResetControls();
        adaugaSuccesibilPanel.Visible = false;
        listaSuccesibiliPanel.Visible = true;
        succesibiliGridView.DataBind();
    }

    protected void ArataModificaSuccesibilPanel_Click(object sender, EventArgs e)
    {
        adaugaSuccesibilPanel.Visible = true;
        listaSuccesibiliPanel.Visible = false;
        adaugaSuccesibilButton.Visible = false;
        modificaSuccesibilButton.Visible = true;
        arataListaSuccesibiliButton.Visible = true;
        CompleteazaDateDeModificat();
    }

    protected void ModificaArataListaSuccesibili(object sender, EventArgs e)
    {
        adaugaSuccesibilPanel.Visible = false;
        listaSuccesibiliPanel.Visible = true;
        stergeSuccesibilButton.Visible = true;
        btModificaSuccesibil.Visible = true;
    }

    protected void ArataListaSuccesibiliButton_Click(object sender, EventArgs e)
    {
        ResetControls();
        succesibiliGridView.DataBind();
        adaugaSuccesibilPanel.Visible = false;
        listaSuccesibiliPanel.Visible = true;
    }

    protected void AdaugaSuccesibilButton_Click(object sender, EventArgs e)
    {
        AdaugaSuccesibil();
        succesibiliGridView.DataBind();
        succesibiliGridView.SelectedIndex = -1;
        stergeSuccesibilButton.Visible = false;
        btModificaSuccesibil.Visible = false;
        ResetControls();
        listaSuccesibiliPanel.Visible = true;
        adaugaSuccesibilPanel.Visible = false;
    }

    protected void AdaugaSuccesibilPanelButton_Click(object sender, EventArgs e)
    {
        adaugaSuccesibilPanel.Visible = true;
        listaSuccesibiliPanel.Visible = false;
        stergeSuccesibilButton.Visible = false;
        adaugaSuccesibilButton.Visible = true;
        modificaSuccesibilButton.Visible = false;
        btModificaSuccesibil.Visible = false;
        arataListaSuccesibiliButton.Visible = true;
    }

    protected void StergeSuccesibilulButtonClick(object sender, EventArgs e)
    {
        StergeSuccesibil();
    }

    #endregion Events

    #region Methods

    private void AdaugaSuccesibil()
    {
        Succesibili succesibil = CreateSuccesibil();

        try
        {
            succesibil.Salveaza();
            valCustom.IsValid = true;
            valCustom.ErrorMessage = "Salvare reusita!";
        }
        catch
        {
            valCustom.IsValid = false;
            valCustom.ErrorMessage = "Salvare esuata!";
        }

        ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "succesibil", "adauga succesibil", "nume succesibil adaugat: " + succesibil.Nume, Convert.ToInt64(Session["SESgospodarieId"]), 3);
        ClassLog.UpdateDataUltimeiModificari(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESgospodarieId"]));

    }
 
    private void StergeSuccesibil()
    {
        string succesibilId = succesibiliGridView.SelectedValue.ToString();
        string vNume = succesibiliGridView.SelectedRow.Cells[3].Text;

        Succesibili succesibil = new Succesibili();
        succesibil.IdSuccesibil = Convert.ToInt64(succesibilId);
        succesibil.Sterge();

        ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "succesibil", "sterge succesibil ID: " + succesibilId, "nume succesibil sters: " + vNume, Convert.ToInt64(Session["SESgospodarieId"]), 4);
        ClassLog.UpdateDataUltimeiModificari(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESgospodarieId"]));

        succesibiliGridView.DataBind();
        succesibiliGridView.SelectedIndex = -1;
        stergeSuccesibilButton.Visible = false;
        btModificaSuccesibil.Visible = false;
    }

    private void ModificaSuccesibil()
    {
        Succesibili succesibil = CreateSuccesibil();
        succesibil.IdSuccesibil = Convert.ToInt64(succesibiliGridView.SelectedValue.ToString());
        try
        {
            succesibil.Modifica();
            valCustom.IsValid = true;
            valCustom.ErrorMessage = "Salvare reusita!";
        }
        catch
        {
            valCustom.IsValid = false;
            valCustom.ErrorMessage = "Salvare esuata!";
        }

    }

    private Succesibili CreateSuccesibil()
    {
        Succesibili succesibil = new Succesibili();

        succesibil.An = Convert.ToInt32(Session["SESAn"]);
        succesibil.IdUnitate = Convert.ToInt32(Session["SESunitateId"]);
        succesibil.IdJudet = judeteDropDownList.SelectedValue;
        succesibil.DataInregistrare = Convert.ToDateTime(dataInregistrareTextBox.Text).Date;
        succesibil.DenumireBirouNotarial = binTextBox.Text;
        succesibil.IdDefunct = Convert.ToInt64(defunctDropDownList.SelectedValue);
        succesibil.IdLocalitate = localitatiDropDownList.SelectedValue;
        succesibil.NrInregistrare = nrTextBox.Text;
        succesibil.NrLocuinta = numarTextBox.Text;
        succesibil.Nume = tbNumePrenume.Text;
        succesibil.Strada = stradaTextBox.Text;
        succesibil.IdGospodarie = Convert.ToInt32(Session["SESgospodarieId"]);
        return succesibil;
    }

    private void CompleteazaDateDeModificat()
    {
        CompleteazaJudete();
        tbNumePrenume.Text = succesibiliGridView.SelectedRow.Cells[3].Text;
        judeteDropDownList.SelectedValue = countiesDataTable.Select( "judetDenumire = '" + succesibiliGridView.SelectedRow.Cells[4].Text.Split(',')[1].Trim() + "'")[0]["judetId"].ToString();
        CompleteazaLocalitati();
        localitatiDropDownList.SelectedValue = localitatiDataTable.Select("localitateDenumire = '" + succesibiliGridView.SelectedRow.Cells[4].Text.Split(',')[0].Trim() + "' AND judetDenumire = '" + succesibiliGridView.SelectedRow.Cells[4].Text.Split(',')[1].Trim() + "'")[0]["localitateId"].ToString();
        stradaTextBox.Text = succesibiliGridView.SelectedRow.Cells[5].Text.Split('/')[0].Trim();
        nrTextBox.Text = succesibiliGridView.SelectedRow.Cells[5].Text.Split('/')[1].Trim();
        numarTextBox.Text = ((Label)(succesibiliGridView.SelectedRow.Cells[6].Controls[1])).Text.Trim();
        dataInregistrareTextBox.Text = ((Label)(succesibiliGridView.SelectedRow.Cells[6].Controls[3])).Text.Trim();
        binTextBox.Text = succesibiliGridView.SelectedRow.Cells[7].Text.Trim();
    }
    
    private void ResetControls()
    {
        judeteDropDownList.SelectedIndex = 0;
        dataInregistrareTextBox.Text = string.Empty;
        binTextBox.Text = string.Empty;
        localitatiDropDownList.SelectedIndex = 0;
        nrTextBox.Text = string.Empty;
        numarTextBox.Text = string.Empty;
        tbNumePrenume.Text = string.Empty;
        stradaTextBox.Text = string.Empty;
    }

    private void CompleteazaJudeteSiLocalitati()
    {
        CompleteazaJudete();
        CompleteazaLocalitati();
    }

    private void CompleteazaJudete()
    {
        judeteDropDownList.Items.Clear();
        judeteDropDownList.SelectedIndex = -1;
        judeteDropDownList.DataValueField = "judetId";
        judeteDropDownList.DataTextField = "judetDenumire";
        judeteDropDownList.DataSource = GetCounties();
        judeteDropDownList.DataBind();
        judeteDropDownList.Items.Insert(0, new ListItem("- Selectati un judet -"));
    }

    static DataTable countiesDataTable = new DataTable();
    private DataTable GetCounties()
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));

        SqlDataAdapter dataAdapter = new SqlDataAdapter(
          @"SELECT judetId, judetDenumire FROM Judete ",
          vCon);

        dataAdapter.Fill(countiesDataTable);

        ManipuleazaBD.InchideConexiune(vCon);

        return countiesDataTable;
    }

    private void CompleteazaLocalitati()
    {
        localitatiDropDownList.Items.Clear();
        localitatiDropDownList.SelectedIndex = -1;
        localitatiDropDownList.DataValueField = "localitateId";
        localitatiDropDownList.DataTextField = "localitateDenumire";
        
        if(judeteDropDownList.SelectedIndex != 0)
        {
            localitatiDropDownList.DataSource = GetCities(judeteDropDownList.SelectedValue);
            localitatiDropDownList.DataBind();
        }
        
        localitatiDropDownList.Items.Insert(0, new ListItem("- Selectati o localitate -"));

    }

    DataTable localitatiDataTable = new DataTable();
    public  DataTable GetCities(string county)
    {
        if(localitatiDataTable.Rows.Count > 0)
        {
            localitatiDataTable.Clear();
        }

        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlDataAdapter dataAdapter = new SqlDataAdapter(
            @"SELECT localitateId, localitateDenumire, judetDenumire FROM Localitati WHERE judetId = '" + county + "'",
            vCon);

        dataAdapter.Fill(localitatiDataTable);

        ManipuleazaBD.InchideConexiune(vCon);

        return localitatiDataTable;
    }
  
    Dictionary<Int64, string> dictionarNumeDefuncti = new Dictionary<long, string>();
    Dictionary<Int64, DateTime> dictionarDataDeces = new Dictionary<long, DateTime>();

    private void CompleteazaDateDefunct()
    {
        CompleteazaNumeDefunct();
        CompleteazaDataDeces();
    }

    private void CompleteazaNumeDefunct()
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;

        vCmd.CommandText = @"SELECT nume, decedatData, capitolId  FROM membri WHERE an = " + Session["SESAn"] + " AND decedat = 'True' AND gospodarieId = " + Session["SESgospodarieId"].ToString();
        SqlDataReader read = vCmd.ExecuteReader();

        if (read.FieldCount > 0)
        {
            while (read.Read())
            {
                dictionarNumeDefuncti.Add(Convert.ToInt64(read["capitolId"]), read["nume"].ToString());
                dictionarDataDeces.Add(Convert.ToInt64(read["capitolId"]), Convert.ToDateTime(read["decedatData"]));
            }

            defunctDropDownList.DataValueField = "Key";
            defunctDropDownList.DataTextField = "Value";
            defunctDropDownList.DataSource = dictionarNumeDefuncti;
            defunctDropDownList.DataBind();
        }
        read.Close();
        ManipuleazaBD.InchideConexiune(vCon);
    }

    private void CompleteazaDataDeces()
    {
        if (dictionarDataDeces.Count == 0)
        {
            return;
        }
        var test = from items in dictionarDataDeces
                   where items.Key == Convert.ToInt64(defunctDropDownList.SelectedValue)
                   select items.Value;

        dataDecesTextBox.Text = test.ToArray()[0].ToShortDateString();
    }

    #endregion Methods

}
