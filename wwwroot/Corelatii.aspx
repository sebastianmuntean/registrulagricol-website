﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Corelatii.aspx.cs" Inherits="Corelatii"  EnableEventValidation="false" UICulture="ro-Ro" Culture="ro-Ro" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Administrare / Corelații" />                        
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="upCorelatii" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnAfiseazaMesaje" runat="server">
                <asp:ValidationSummary ID="valSumCorelatii" runat="server" DisplayMode="SingleParagraph"
                    Visible="true" ValidationGroup="GrupValidareCorelatii" CssClass="validator"
                    ForeColor="" />
            </asp:Panel>
            <!-- lista utilizatori -->
            <asp:Panel ID="pnListaCorelatii" runat="server" CssClass="panel_general" Visible="true">
                <asp:Panel ID="pnCautare" runat="server" CssClass="cauta">
                    <asp:Label ID="lblCauta" runat="server" Text="Caută după:" />
                    <!-- autocomplete unitate -->
                    <asp:Label ID="lblCCapitol" runat="server" Text="Capitol" />
                    <asp:TextBox ID="tbCCapitol" runat="server" autocomplete="off" AutoPostBack="True" />
                    <ajaxtoolkit:autocompleteextender id="tbCCapitol_AutoCompleteExtender" runat="server"
                        enabled="True" servicemethod="GetCompletionList" servicepath="AutoComplete.asmx"
                        targetcontrolid="tbCCapitol" behaviorid="AutoCompleteEx1" minimumprefixlength="1"
                        completioninterval="10" enablecaching="True" completionsetcount="10" completionlistcssclass="autocomplete_completionListElement"
                        completionlistitemcssclass="autocomplete_listItem" completionlisthighlighteditemcssclass="autocomplete_highlightedListItem"
                        delimitercharacters=";, :" showonlycurrentwordincompletionlistitem="false" firstrowselected="False"
                        usecontextkey="True" contextkey="Capitol">
                    </ajaxtoolkit:autocompleteextender>
                    <!-- autocomplete login -->
                    <asp:Label ID="lblCRand" runat="server" Text="Rând" />
                    <asp:TextBox ID="tbCRand" runat="server" autocomplete="off" AutoPostBack="True" />
                    <!-- autocomplete nume / prenume -->
                    <asp:Label ID="lblCColoana" runat="server" Text="Coloana" />
                    <asp:TextBox ID="tbCColoana" runat="server" autocomplete="off" AutoPostBack="True" />
                </asp:Panel>
                <asp:Panel runat="server">
                    <asp:GridView ID="gvCorelatii" runat="server" AllowPaging="True" AllowSorting="True" DataKeyNames="corelatieId"
                        AutoGenerateColumns="False" DataSourceID="SqlCorelatii" CssClass="tabela" OnRowDataBound="gvCorelatii_RowDataBound"
                        OnDataBound="gvCorelatii_DataBound" OnSelectedIndexChanged="gvCorelatii_SelectedIndexChanged">
                        <Columns>
                            <asp:BoundField DataField="corelatieId" HeaderText="Nr." SortExpression="corelatieId"
                                Visible="true" />
                            <asp:BoundField DataField="camp1" HeaderText="Câmpul 1" SortExpression="camp1" />
                            <asp:BoundField DataField="semn" HeaderText="semn" SortExpression="semn" />
                            <asp:BoundField DataField="camp2" HeaderText="Câmpul 2" SortExpression="camp2" />
                        </Columns>
                        <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlCorelatii" runat="server" 
                        SelectCommand="SELECT * FROM [corelatii] " OnInit="SqlCorelatii_Init">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="tbCCapitol" DefaultValue="%" Name="camp1" PropertyName="Text"
                                Type="String" />
                            <asp:ControlParameter ControlID="tbCColoana" DefaultValue="%" Name="camp2" PropertyName="Text"
                                Type="String" />
                            <asp:ControlParameter ControlID="tbCRand" DefaultValue="%" Name="camp12" PropertyName="Text"
                                Type="String" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </asp:Panel>
                <asp:Panel ID="pnListaButoanePrincipale" CssClass="butoane" runat="server">
                    <asp:Button CssClass="buton" ID="btModificaCorelatie" runat="server" Text="modificare corelație"
                        OnClick="ArataAdaugaCorelatie" Visible="false" /> 
                                       
                    <asp:Button CssClass="buton" ID="btStergeCorelatie" runat="server" Text="șterge corelația"
                        OnClick="StergeCorelația" ValidationGroup="GrupValidareCorelatii" OnClientClick="return confirm(&quot;Sunteți sigur că doriți să ștergeți corelația?&quot;)" Visible="false" />
                    <asp:Button CssClass="buton" ID="btListaAdaugaCorelatie" runat="server" Text="adaugă o corelație"
                        OnClick="ArataAdaugaCorelatie" />
                </asp:Panel>
            </asp:Panel>
            <!-- adaugam utilizatori -->
            <asp:Panel ID="pnAdaugaCorelatie" CssClass="panel_general" runat="server" Visible="false">
                <asp:Panel ID="Panel1" CssClass="adauga" runat="server">
                    <asp:Panel ID="Panel2" runat="server">
                        <h2>
                            ADAUGĂ / MODIFICĂ O CORELAŢIE</h2>
                    </asp:Panel>
                    <asp:Panel ID="Panel3" runat="server">
                        <p runat="server" visible="false">
                            <asp:Label ID="lblCampul1" runat="server" Text="Câmpul 1 - Capitolul: " />
                            <asp:DropDownList ID="ddCapitolCamp1" runat="server" DataSourceID="SqlCapitole" DataTextField="capitol"
                                DataValueField="capitol">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="SqlCapitole" runat="server" 
                                SelectCommand="SELECT DISTINCT capitol FROM sabloaneCapitole ORDER BY capitol">
                            </asp:SqlDataSource>
                            <asp:Label ID="lblRandCamp1" runat="server" Text=" Rândul: " />
                            <asp:TextBox ID="tbRandCamp1" runat="server" Width="50"></asp:TextBox> 
                            <asp:Label ID="lblColoanaCamp1" runat="server" Text=" Coloana: " />
                            <asp:TextBox ID="tbColoanaCamp1" runat="server" Width="50"></asp:TextBox>
                            <asp:Label ID="lblUMCamp1" runat="server" Text=" UM: " />
                            <asp:DropDownList ID="ddUMCamp1" runat="server">
                                <asp:ListItem Selected="True" Value="1">ha</asp:ListItem>
                                <asp:ListItem Value="2">ari</asp:ListItem>
                                <asp:ListItem Value="0">-</asp:ListItem>
                            </asp:DropDownList>

                        </p>
                        <p>
                            <asp:GridView ID="gvInainteEgal" runat="server" AutoGenerateColumns="False" DataSourceID="SqlRanduri"
                                OnInit="gvDupaEgal_OnInit" CssClass="tabelaCorelatii" OnDataBound="gvDupaEgal_DataBound">
                                <Columns>
                                    <asp:BoundField DataField="id" HeaderText="Câmpul 1 = " />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddOperatie" runat="server" OnSelectedIndexChanged="ddOperatie_SelectedIndexChanged">
                                                <asp:ListItem>final</asp:ListItem>
                                                <asp:ListItem Selected="True">+</asp:ListItem>
                                                <asp:ListItem>-</asp:ListItem>
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            operaţia
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddCapitolCamp2" runat="server" DataSourceID="SqlCapitole" DataTextField="capitol"
                                                DataValueField="capitol">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            Capitolul
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:TextBox ID="tbRandCamp2" runat="server" Width="50"></asp:TextBox>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            Rândul
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:TextBox ID="tbColoanaCamp2" runat="server" Width="50"></asp:TextBox>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            Coloana
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddUMCamp2" runat="server">
                                                <asp:ListItem Selected="True" Value="1">ha</asp:ListItem>
                                                <asp:ListItem Value="2">ari</asp:ListItem>
                                                <asp:ListItem Value="0">-</asp:ListItem>
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            Unitatea de măsură
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                                SelectCommand="SELECT TOP(10) [id] FROM [grid] ORDER BY [id]"></asp:SqlDataSource>
                        </p>
                        <p>
                          <asp:Label ID="Egal" runat="server" Text=" Semn " />
                            <asp:DropDownList ID="ddSemn" runat="server">
                                <asp:ListItem Selected="True" Value="0">=</asp:ListItem>
                                <asp:ListItem Value="1"><=</asp:ListItem>
                                <asp:ListItem Value="2">>=</asp:ListItem>
                                <asp:ListItem Value="3">><></asp:ListItem>
                            </asp:DropDownList>
                        </p>
                        <p>
                            <asp:GridView ID="gvDupaEgal" runat="server" AutoGenerateColumns="False" DataSourceID="SqlRanduri"
                                OnInit="gvDupaEgal_OnInit" CssClass="tabelaCorelatii" OnDataBound="gvDupaEgal_DataBound">
                                <Columns>
                                    <asp:BoundField DataField="id" HeaderText="Câmpul 2 = " />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddOperatie" runat="server" OnSelectedIndexChanged="ddOperatie_SelectedIndexChanged">
                                                <asp:ListItem>final</asp:ListItem>
                                                <asp:ListItem Selected="True">+</asp:ListItem>
                                                <asp:ListItem>-</asp:ListItem>
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            operaţia
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddCapitolCamp2" runat="server" DataSourceID="SqlCapitole" DataTextField="capitol"
                                                DataValueField="capitol">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            Capitolul
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:TextBox ID="tbRandCamp2" runat="server" Width="50"></asp:TextBox>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            Rândul
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:TextBox ID="tbColoanaCamp2" runat="server" Width="50"></asp:TextBox>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            Coloana
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddUMCamp2" runat="server">
                                                <asp:ListItem Selected="True" Value="1">ha</asp:ListItem>
                                                <asp:ListItem Value="2">ari</asp:ListItem>
                                                <asp:ListItem Value="0">-</asp:ListItem>
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            Unitatea de măsură
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:SqlDataSource ID="SqlRanduri" runat="server" 
                                SelectCommand="SELECT TOP(10) [id] FROM [grid] ORDER BY [id]"></asp:SqlDataSource>
                        </p>
                        
                    </asp:Panel>
                    <asp:Panel ID="pnButoanePrincipale" runat="server" CssClass="butoane">
                        <asp:Button CssClass="buton" ID="btAdaugaCorelatie" runat="server" Text="adaugă o corelaţie"
                            OnClick="AdaugaModificaCorelatie2" ValidationGroup="GrupValidareCorelatii" Visible="false" />
                        <asp:Button ID="btAdaugaModificaCorelatie" runat="server" CssClass="buton" OnClick="AdaugaModificaCorelatie"
                            Text="modificare corelaţie" ValidationGroup="GrupValidareCorelatii" Visible="false" />
                        <asp:Button ID="btModificaArataListaCorelatii" runat="server" CssClass="buton" OnClick="ModificaArataListaCorelatii"
                            Text="lista corelaţiilor" Visible="false" />
                        <asp:Button ID="btAdaugaArataListaCorelatii" runat="server" CssClass="buton" OnClick="AdaugaArataListaCorelatii"
                            Text="lista corelaţiilor" Visible="false" />
                        <asp:Button ID="Button1" runat="server" CssClass="buton" OnClick="AdaugaParcele"
                            Text="parcele/paduri" Visible="true" />

                    </asp:Panel>
                    <asp:Panel ID="Panel4" CssClass="ascunsa" runat="server">
                        <!-- //////////// VALIDATOARE  \\\\\\\\\\\\\\\\\\ -->

                        
                    
                        
                        
                        <asp:CustomValidator ID="valCustom" runat="server" ErrorMessage="" ValidationGroup="GrupValidareCorelatii"></asp:CustomValidator>
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
            
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
