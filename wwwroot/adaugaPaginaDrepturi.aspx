﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="adaugaPaginaDrepturi.aspx.cs" Inherits="adaugaPaginaDrepturi" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    adauga o pagina in sabloane drepturi si in drepturi utilizatori
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="upLocalitati" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnAfiseazaMesaje" runat="server">
                <asp:ValidationSummary ID="valCustomSummary" runat="server" DisplayMode="SingleParagraph"
                    Visible="true" CssClass="validator" ForeColor="" />
            </asp:Panel>
            <asp:Panel ID="Panel5" CssClass="ascunsa" runat="server">
                <!-- //////////// VALIDATOARE  \\\\\\\\\\\\\\\\\\ -->
                <asp:CustomValidator ID="valCustom" runat="server" ErrorMessage=""></asp:CustomValidator>
            </asp:Panel>
            <asp:Panel ID="pnListaLocalitati" runat="server" CssClass="panel_general" Visible="true">
                <asp:Panel ID="pnCautare" runat="server" CssClass="cauta">
                    <asp:Label ID="lblPagina" runat="server" Text="Pagina"></asp:Label>
                    <asp:DropDownList ID="ddlPagina" AutoPostBack="true" Width="200px" runat="server"
                        DataSourceID="SqlPagini" DataTextField="paginaDenumire" DataValueField="paginaId">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlPagini" runat="server" 
                        SelectCommand="SELECT [paginaId], [paginaDenumire] FROM [pagini] ORDER BY [paginaDenumire]">
                    </asp:SqlDataSource>
                    <asp:Label ID="lblSablon" runat="server" Text="Tip utilizator"></asp:Label>
                    <asp:DropDownList ID="ddlTipUtilizator" AutoPostBack="true" runat="server" DataSourceID="SqlTipuriUtilizatori"
                        DataTextField="tipUtilizatorDenumire" DataValueField="tipUtilizatorId" 
                        onselectedindexchanged="ddlTipUtilizator_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlTipuriUtilizatori" runat="server"
                        SelectCommand="SELECT [tipUtilizatorId], [tipUtilizatorDenumire] FROM [tipuriUtilizatori]">
                    </asp:SqlDataSource>
                </asp:Panel>
                <asp:Panel ID="pnGrid" runat="server" Visible="true">
                    <asp:GridView ID="gvSabloane" runat="server" CssClass="tabela" AutoGenerateColumns="False"
                        DataSourceID="SqlListaPagini" PageSize="100" OnDataBound="gvSabloane_DataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="pagina">
                                <ItemTemplate>
                                    <asp:Label ID="paginaDenumire" runat="server" Text='<%# Bind("paginaDenumire") %>'
                                        Visible="true"></asp:Label>
                                    <asp:Label ID="paginaId" runat="server" Text='<%# Bind("paginaId") %>' Visible="false"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Citire">
                                <ItemTemplate>
                                    <asp:CheckBox ID="cbCitire" runat="server" Enabled="True" Checked="true" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Stergere">
                                <ItemTemplate>
                                    <asp:CheckBox ID="cbStergere" runat="server" Enabled="True" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Creare">
                                <ItemTemplate>
                                    <asp:CheckBox ID="cbCreare" runat="server" Enabled="True" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Modificare">
                                <ItemTemplate>
                                    <asp:CheckBox ID="cbModificare" runat="server" Enabled="True" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Tiparire">
                                <ItemTemplate>
                                    <asp:CheckBox ID="cbTiparire" runat="server" Enabled="True" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlListaPagini" runat="server" 
                        SelectCommand="SELECT paginaId, paginaDenumire FROM pagini WHERE (paginaId = @paginaId)">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="ddlPagina" Name="paginaId" PropertyName="SelectedValue" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </asp:Panel>
                <asp:Panel ID="pnButoane" CssClass="butoane" runat="server">
                    <asp:Button ID="btAdauga" CssClass="buton" runat="server" Text="Salvează şablonul pentru toti utilizatorii de tipul ales"
                        OnClick="btSabloane_Salveaza" />
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
