﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class masura141 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DBConnections connection = new DBConnections(Convert.ToInt16(Session["SESan"]));

        SqlMembri.ConnectionString = connection.Create();

        TabelMasura141();
    }
    protected void TabelMasura141()
    {
        // tabelul 1
        decimal vValoareUDE = 0;
        lblTabel.Text = "<table class='tabela'><tr><th colspan='5'>STABILIREA DIMENSIUNII ECONOMICE A FERMEI - ANUL 0</th></tr><tr><th colspan='5'>STRUCTURA CULTURILOR ŞI CALCUL UDE</th></tr><tr><th>Nr. crt</th><th>Cultura</th><th>Suprafaţa (ha)</th><th>UDE / ha</th><th>UDE</th></tr>";
        List<List<string>> vLista = clsAdeverinte.Masura141CalculTabel1(Session["SESgospodarieId"].ToString(), Session["SESan"].ToString());
        foreach (List<string> vRand in vLista)
        {
            if (vRand[0] != "total")
            {
                try
                {
                    lblTabel.Text += "<tr><td>" + vRand[4] + "</td><td>" + vRand[0] + "</td><td>" + vRand[6] + "</td><td>" + vRand[3] + "</td><td>" + vRand[7] + "</td></tr>";
                }
                catch { }
            }
            else
            { 
                lblTabel.Text += "<tr><th colspan='4'>TOTAL UDE DIN PRODUCŢIA VEGETALĂ</th><th>" + vRand[7] + "</th></tr>";
                vValoareUDE += Convert.ToDecimal(vRand[7]);
            }
        }
        lblTabel.Text += "<tr><td colspan='5'>&nbsp;</td></tr>";
        lblTabel.Text += "<tr><th colspan='5'>STRUCTURA PRODUCŢIEI ZOOTEHNICE ŞI CALCUL UDE</th></tr><tr><th>Nr. crt</th><th>Animale/ păsări</th><th>Număr capete</th><th>UDE / cap</th><th>UDE</th></tr>";
        vLista = clsAdeverinte.Masura141CalculTabel2(Session["SESgospodarieId"].ToString(), Session["SESan"].ToString());
        foreach (List<string> vRand in vLista)
        {
            if (vRand[0] != "total")
            {
                try
                {
                    lblTabel.Text += "<tr><td>" + vRand[4] + "</td><td>" + vRand[0] + "</td><td>" + vRand[6] + "</td><td>" + vRand[3] + "</td><td>" + vRand[7] + "</td></tr>";
                }
                catch { }
            }
            else
            {
                lblTabel.Text += "<tr><th colspan='4'>TOTAL UDE DIN PRODUCŢIA ZOOTEHNICĂ</th><th>" + vRand[7] + "</th></tr>";
                vValoareUDE += Convert.ToDecimal(vRand[7]);
            }
        }
        string vFormat = "0.000";
        lblTabel.Text += "<tr><td colspan='5'>&nbsp;</td></tr>";
        lblTabel.Text += "<tr><th colspan='4'>TOTAL UDE DIN PRODUCŢIA VEGETALĂ ŞI ZOOTEHNICĂ</th><th>" + vValoareUDE.ToString(vFormat) + "</th></tr>";
        lblValoareTotalaUDE.Text = "Total UDE din producţia vegetală şi zootehnică: " + vValoareUDE.ToString(vFormat);
        if (vValoareUDE >= 2 && vValoareUDE <= 8) lblValoareTotalaUDE.Text += " <span class='eligibil'> " + Resources.Resursa.raGospodarieEligibila + "</span>";
        else lblValoareTotalaUDE.Text += " <span class='neeligibil'> " + Resources.Resursa.raGospodarieNeeligibila + " </span>";
        // tabelul 2
    }
    protected void gvMembri_DataBound(object sender, EventArgs e)
    {
        List<string> vMembriEligibili = new List<string> { };
        for (int i = 0; i < gvMembri.Rows.Count; i++)
        {
            // schimbam codsex si vedem ce membri sunt eligibili
            if (gvMembri.Rows[i].RowType == DataControlRowType.DataRow)
            {
                TimeSpan vVarsta = new TimeSpan();
                int vAni = 0;
                int vLuni = 0;
                int vZile = 0;
                TimeSpanToDate(DateTime.Now, Convert.ToDateTime(gvMembri.Rows[i].Cells[6].Text), out vAni, out vLuni, out vZile);
                if ( vAni >= 18 && vAni <=62 )
                {
                    vMembriEligibili.Add(gvMembri.Rows[i].Cells[2].Text);
                    gvMembri.Rows[i].Cells[2].Text += "<span class='eligibil'> " + Resources.Resursa.raMembruEligibil + " </span>";
                }
                if (gvMembri.Rows[i].Cells[4].Text == "1")
                {
                    gvMembri.Rows[i].Cells[4].Text = Resources.Resursa.raMasculin;
                }
                else
                {
                    gvMembri.Rows[i].Cells[4].Text = Resources.Resursa.raFeminin;
                }
                if (gvMembri.Rows[i].Cells[6].Text == "01.01.1900")
                {
                    gvMembri.Rows[i].Cells[6].Text = " - ";
                }
            }
        }
        // scriem mesaj membri eligibili 
        if (vMembriEligibili.Count > 0)
        {
            lblMembriEligibili.Text = "<div>" + Resources.Resursa.raMembruEligibilMesaj141 + "<br /><strong>";
            foreach (string vMembru in vMembriEligibili)
                lblMembriEligibili.Text +=vMembru + "<br />";
            lblMembriEligibili.Text += "</strong></div>";
        }
    }
    public void TimeSpanToDate(DateTime d1, DateTime d2, out int years, out int months, out int days)
    {
        // compute & return the difference of two dates,
        // returning years, months & days
        // d1 should be the larger (newest) of the two dates
        // we want d1 to be the larger (newest) date
        // flip if we need to
        if (d1 < d2)
        {
            DateTime d3 = d2;
            d2 = d1;
            d1 = d3;
        }

        // compute difference in total months
        months = 12 * (d1.Year - d2.Year) + (d1.Month - d2.Month);

        // based upon the 'days',
        // adjust months & compute actual days difference
        if (d1.Day < d2.Day)
        {
            months--;
            days = DateTime.DaysInMonth(d2.Year, d2.Month) - d2.Day + d1.Day;
        }
        else
        {
            days = d1.Day - d2.Day;
        }
        // compute years & actual months
        years = months / 12;
        months -= years * 12;
    }
}
