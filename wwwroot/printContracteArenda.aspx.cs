﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using Microsoft.Reporting.WebForms;

public partial class printContracteArenda : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
            SqlCommand vCmd1 = new SqlCommand();
            SqlCommand vCmd2 = new SqlCommand();

            using (vCmd1.Connection)
            {
                vCmd1.Connection = vCon;

                vCmd1.CommandText = "delete from rapContracteArenda where utilizatorId='" + Session["SESutilizatorId"].ToString() + "'";
                vCmd1.ExecuteNonQuery();

                vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
                vCmd2.Connection = vCon;
                using (vCmd2.Connection)
                {
                    vCmd2.CommandText = @"
                DECLARE @lista table(idInitial bigint, id bigint);
                INSERT INTO @lista
                            SELECT gospodarii.gospodarieIdInitial, gospodarii.gospodarieid
                FROM gospodarii
                INNER JOIN unitati
                  ON gospodarii.unitateId = unitati.unitateId
                WHERE
                    (CONVERT(nvarchar, gospodarii.unitateId) LIKE '" + Session["rcaUnitateId"].ToString() + @"')
                AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + Session["rcaJudetId"].ToString() + @"')
                AND (gospodarii.volum LIKE '" + Session["rcaVolum"].ToString() + @"')
                AND (gospodarii.nrPozitie LIKE '" + Session["rcaPozitie"].ToString() + @"')
                AND (gospodarii.nrInt >= '" + Session["rcaNrDeLa"].ToString() + @"') 
                AND (gospodarii.nrInt <= '" + Session["rcaNrLa"].ToString() + @"')
                AND (CONVERT(nvarchar, gospodarii.strainas) LIKE '" + Session["rcaStrainas"].ToString() + @"')
                AND (gospodarii.strada LIKE '" + Session["rcaStrada"].ToString() + @"')
                AND (gospodarii.localitate LIKE '" + Session["rcaLocalitate"].ToString() + @"')
                AND (gospodarii.An = '" + Session["SESan"].ToString() + @"')
                ;

                SELECT
                  parceleModUtilizare.modUtilizareId,
                  gDa.gospodarieIdInitial as gInitialDa,
                  gPrimeste.gospodarieIdInitial  as gInitialPrimeste,
                  gDa.volum as gVolumDa,
                  gDa.nrPozitie as gnrPozitieDa,
                  gPrimeste.volum  as gVolumPrimeste,
                  gPrimeste.nrPozitie  as gnrPozitiePrimeste,
                    parceleAtribuiriContracte.contractId,
                  CASE WHEN gDa.persJuridica = 1 
		                THEN gDa.jUnitate 
                        ELSE COALESCE ((SELECT TOP (1) nume 
						                FROM membri 
						                WHERE (membri.gospodarieId = gDa.gospodarieId) 
							                AND (membri.an = '" + Session["SESan"].ToString() + @"') 
			     		                ORDER BY membri.codRudenie), '')
                        END AS numeDa,
                  CASE WHEN gPrimeste.persJuridica = 1 
		                THEN gPrimeste.jUnitate 
                        ELSE COALESCE ((SELECT TOP (1) nume 
						                FROM membri 
						                WHERE (membri.gospodarieId = gPrimeste.gospodarieId) 
							                AND (membri.an = '" + Session["SESan"].ToString() + @"') 
			     		                ORDER BY membri.codRudenie), '')
                        END AS numePrimeste,

                  parceleAtribuiriContracte.contractGospodarieIdDa,
                  parceleAtribuiriContracte.contractGospodarieIdPrimeste,
                  parcele.parcelaNrCadastral,
                  parcele.parcelaNrTopo,

                  parceleAtribuiriContracte.unitateId,

                  parceleAtribuiriContracte.contractDataInceput,
                  parceleAtribuiriContracte.contractDataFinal,
                  parceleAtribuiriContracte.contractDetalii,
                  parceleAtribuiriContracte.contractTip,
                  parceleAtribuiriContracte.contractDataSemnarii,
                  parceleAtribuiriContracte.contractNr,
                  parceleAtribuiriContracte.contractIdImport,
                  SUM(parceleModUtilizare.c3Ha) AS ha,
                  SUM(parceleModUtilizare.c3Ari) AS ari,
                  parceleAtribuiriContracte.contractSub,
                  parceleAtribuiriContracte.contractActiv,
                  parceleModUtilizare.parcelaId,
                  parcele.parcelaDenumire + ' - ' + sabloaneCapitole.denumire1 AS detaliiParcela,
                  sabloaneCapitole_1.denumire1 AS tipContract
                FROM parceleAtribuiriContracte
                FULL JOIN parceleModUtilizare
                  ON parceleAtribuiriContracte.contractId = parceleModUtilizare.contractId
                FULL JOIN parcele
                  ON parceleModUtilizare.parcelaId = parcele.parcelaId
                INNER JOIN gospodarii as gDa
	                ON gDa.gospodarieId = parceleAtribuiriContracte.contractGospodarieIdDa
                INNER JOIN gospodarii as gPrimeste
	                ON gPrimeste.gospodarieId = parceleAtribuiriContracte.contractGospodarieIdPrimeste
                INNER JOIN sabloaneCapitole
                  ON parcele.parcelaCategorie = sabloaneCapitole.codRand
                INNER JOIN sabloaneCapitole AS sabloaneCapitole_1
                  ON parceleModUtilizare.c3Rand = sabloaneCapitole_1.codRand
                WHERE (parceleAtribuiriContracte.contractDataFinal >= CONVERT(datetime, '" + Session["rcaData1"].ToString() + @"', 104))
                AND (parceleAtribuiriContracte.contractDataInceput <= CONVERT(datetime, '" + Session["rcaData2"].ToString() + @"', 104))
                AND (parcele.an = '" + Session["SESan"].ToString() + @"')
                AND (sabloaneCapitole.capitol = '2a')
                AND (sabloaneCapitole_1.an = '" + Session["SESan"].ToString() + @"')
                AND (sabloaneCapitole_1.capitol = '3')
                AND (sabloaneCapitole.an = '" + Session["SESan"].ToString() + @"')
                AND parcele.unitateaId = " + Session["rcaUnitateId"].ToString() + @"
	                AND (CONVERT(nvarchar(2),contractTip) LIKE '" + Session["rcaTipContract"].ToString() + @"')
                AND   
	                (	gDa.gospodarieIdInitial
			                IN (SELECT idInitial from @lista)
		                OR
		                gPrimeste.gospodarieIdInitial
			                IN (SELECT idInitial from @lista)
                )

                GROUP BY 
	                parceleAtribuiriContracte.contractId,
	                gDa.gospodarieIdInitial,
	                gPrimeste.gospodarieIdInitial,
	                gDa.volum,
	                gDa.nrPozitie,
	                gPrimeste.volum,
	                gPrimeste.nrPozitie,
		
                    gDa.persJuridica,
	                gDa.jUnitate,
	                gDa.gospodarieId,
                    gPrimeste.persJuridica,
	                gPrimeste.jUnitate,
	                gPrimeste.gospodarieId,

	                parceleModUtilizare.modUtilizareId,
	
                         parceleAtribuiriContracte.unitateId,
                         parceleAtribuiriContracte.contractGospodarieIdDa,
                         parceleAtribuiriContracte.contractGospodarieIdPrimeste,
                         parceleAtribuiriContracte.contractDataInceput,
                         parceleAtribuiriContracte.contractDataFinal,
                         parceleAtribuiriContracte.contractDetalii,
                         parceleAtribuiriContracte.contractTip,
                         parceleAtribuiriContracte.contractDataSemnarii,
                         parceleAtribuiriContracte.contractNr,
                         parceleAtribuiriContracte.contractIdImport,
                         parceleAtribuiriContracte.contractSub,
                         parceleAtribuiriContracte.contractActiv,
                         parceleModUtilizare.parcelaId,
                         parcele.parcelaDenumire,
                         sabloaneCapitole.denumire1,
                         sabloaneCapitole_1.denumire1,
                         parcele.parcelaNrCadastral,
                         parcele.parcelaNrTopo

		                 ORDER BY gVolumDa,gnrPozitieDa,  contractId, gVolumPrimeste                

                ";
                    SqlDataReader vTabel = vCmd2.ExecuteReader();
                    vCmd1.CommandText = string.Empty;
                    int contor = 0;
                    while (vTabel.Read())
                    {
                                                vCmd1.CommandText += @"
                INSERT INTO rapContracteArenda 
                    (   unitateId, 
                        utilizatorId, 
                        nrContract, 
                        dataContract, 
                        gospodarieIdDa, 
                        gospodarieIdPrimeste, 
                        detaliiContract, 
                        volumDa, 
                        nrPozitieDa, 
                        numeDa, 
                        volumPrimeste, 
                        nrPozitiePrimeste, 
                        numePrimeste,  
                        detaliiParcela, 
                        ha, 
                        ari, 
                        contractId, 
                        tipContract) 
                VALUES ('"
                                + Session["rcaUnitateId"].ToString()
                                + @"', '" + Session["SESutilizatorId"].ToString() + @"', 
                        '" + vTabel["contractNr"].ToString() + @"', 
                        CONVERT(datetime,'" + Convert.ToDateTime(vTabel["contractDataSemnarii"].ToString()) + @"',104), 
                        '" + vTabel["contractGospodarieIdDa"].ToString() + @"', 
                        '" + vTabel["contractGospodarieIdPrimeste"].ToString() + @"',
                        N'" + vTabel["contractDetalii"].ToString() + @"', 
                        '" + vTabel["gVolumDa"].ToString() + @"', 
                        '" + vTabel["gnrPozitieDa"].ToString() + @"', 
                        N'" + vTabel["numeDa"].ToString() + @"', 
                        '" + vTabel["gVolumPrimeste"].ToString() + @"', 
                        '" + vTabel["gnrPozitiePrimeste"].ToString() + @"', 
                        N'" + vTabel["numePrimeste"].ToString() + @"', 
                        '" + vTabel["detaliiParcela"].ToString() + " " + vTabel["parcelaNrCadastral"].ToString() + " " + vTabel["parcelaNrTopo"].ToString() + @"', 
                        '" + vTabel["ha"].ToString().Replace(",", ".") + @"', 
                        '" + vTabel["ari"].ToString().Replace(",", ".") + @"', 
                        '" + vTabel["contractId"].ToString() + @"', 
                        '" + vTabel["tipContract"].ToString() + "');";
                        contor++;
                        if (contor > 250)
                        {
                            contor = 0;
                            vCmd1.ExecuteNonQuery();
                            vCmd1.CommandText = string.Empty;
                        }
                    }
                    vTabel.Close();
                }
                if (vCmd1.CommandText != null && vCmd1.CommandText.Length > 1) vCmd1.ExecuteNonQuery();
            }


            //vCmd2.CommandText = "SELECT gospodarii.gospodarieIdInitial FROM gospodarii INNER JOIN unitati ON gospodarii.unitateId = unitati.unitateId WHERE (CONVERT(nvarchar, gospodarii.gospodarieId) LIKE '" + Session["rcaGospodarieId"].ToString() + "') AND (CONVERT(nvarchar, gospodarii.unitateId) LIKE '" + Session["rcaUnitateId"].ToString() + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + Session["rcaJudetId"].ToString() + "') AND (gospodarii.volum LIKE '" + Session["rcaVolum"].ToString() + "') AND (gospodarii.nrPozitie LIKE '" + Session["rcaPozitie"].ToString() + "') AND (gospodarii.nrInt >= '" + Session["rcaNrDeLa"].ToString() + "') AND (gospodarii.nrInt <= '" + Session["rcaNrLa"].ToString() + "') AND (CONVERT(nvarchar, gospodarii.strainas) LIKE '" + Session["rcaStrainas"].ToString() + "') AND (gospodarii.strada LIKE '" + Session["rcaStrada"].ToString() + "') AND (gospodarii.localitate LIKE '" + Session["rcaLocalitate"].ToString() + "')";
            //SqlDataReader vTabel = vCmd2.ExecuteReader();
            //List<string> vListaGospodarii = new List<string>();
            //while (vTabel.Read())
            //{
            //     vListaGospodarii.Add(vTabel["gospodarieIdInitial"].ToString());
            //    //vListaGospodarii.Add(Session["rcaGospodarieId"].ToString());

            //}
            //vTabel.Close();

            //foreach (string a in vListaGospodarii)
            //{

            //vCmd2.CommandText = "SELECT parcele.parcelaNrCadastral,parcele.parcelaNrTopo,parceleAtribuiriContracte.contractId, parceleAtribuiriContracte.unitateId, parceleAtribuiriContracte.contractGospodarieIdDa, parceleAtribuiriContracte.contractGospodarieIdPrimeste, parceleAtribuiriContracte.contractDataInceput, parceleAtribuiriContracte.contractDataFinal, parceleAtribuiriContracte.contractDetalii, parceleAtribuiriContracte.contractTip, parceleAtribuiriContracte.contractDataSemnarii, parceleAtribuiriContracte.contractNr, parceleAtribuiriContracte.contractIdImport, SUM(parceleModUtilizare.c3Ha) AS ha, SUM(parceleModUtilizare.c3Ari) AS ari, parceleAtribuiriContracte.contractSub, parceleAtribuiriContracte.contractActiv, parceleModUtilizare.parcelaId, parcele.parcelaDenumire + ' - ' + sabloaneCapitole.denumire1 AS detaliiParcela, sabloaneCapitole_1.denumire1 AS tipContract FROM parceleAtribuiriContracte INNER JOIN parceleModUtilizare ON parceleAtribuiriContracte.contractId = parceleModUtilizare.contractId INNER JOIN parcele ON parceleModUtilizare.parcelaId = parcele.parcelaId INNER JOIN sabloaneCapitole ON parcele.parcelaCategorie = sabloaneCapitole.codRand INNER JOIN sabloaneCapitole AS sabloaneCapitole_1 ON parceleModUtilizare.c3Rand = sabloaneCapitole_1.codRand WHERE (parceleAtribuiriContracte.contractDataFinal >= CONVERT(datetime, '" + Session["rcaData1"].ToString() + "', 104)) AND (parceleAtribuiriContracte.contractDataInceput <= CONVERT(datetime, '" + Session["rcaData2"].ToString() + "', 104)) AND (parcele.an = '" + Session["SESan"].ToString() + "') AND (sabloaneCapitole.capitol = '2a') AND (CONVERT(nvarchar, parceleAtribuiriContracte.contractGospodarieIdDa) = '" + a + "' OR CONVERT(nvarchar, parceleAtribuiriContracte.contractGospodarieIdPrimeste) = '" + a + "') AND (sabloaneCapitole_1.an = '" + Session["SESan"].ToString() + "') AND (sabloaneCapitole_1.capitol = '3') AND (sabloaneCapitole.an = '" + Session["SESan"].ToString() + "') GROUP BY parceleAtribuiriContracte.contractId, parceleAtribuiriContracte.unitateId, parceleAtribuiriContracte.contractGospodarieIdDa, parceleAtribuiriContracte.contractGospodarieIdPrimeste, parceleAtribuiriContracte.contractDataInceput, parceleAtribuiriContracte.contractDataFinal, parceleAtribuiriContracte.contractDetalii, parceleAtribuiriContracte.contractTip, parceleAtribuiriContracte.contractDataSemnarii, parceleAtribuiriContracte.contractNr, parceleAtribuiriContracte.contractIdImport, parceleAtribuiriContracte.contractSub, parceleAtribuiriContracte.contractActiv, parceleModUtilizare.parcelaId, parcele.parcelaDenumire, sabloaneCapitole.denumire1, sabloaneCapitole_1.denumire1,parcele.parcelaNrCadastral,parcele.parcelaNrTopo";
            //vTabel = vCmd2.ExecuteReader();
            //while (vTabel.Read())
            //{
            //    string vVolumDa = "", vPozitieDa = "", vNumeDa = "", vVolumPrimeste = "", vPozitiePrimeste = "", vNumePrimeste = "";
            //    decimal vHa = Convert.ToDecimal(vTabel["ha"].ToString().Replace(".", ",")), vAri = Convert.ToDecimal(vTabel["ari"].ToString().Replace(".", ","));
            //    decimal vSuprafata = vHa * 100 + vAri;
            //    vHa = Math.Truncate(vSuprafata / 100);
            //    vAri = vSuprafata - Math.Truncate(vSuprafata / 100) * 100;
            //   // vCmd1.CommandText = "SELECT volum, nrPozitie, COALESCE ((SELECT TOP (1) nume FROM membri WHERE (gospodarieId = gospodarii.gospodarieId) AND (an = '" + Session["SESan"].ToString() + "') ORDER BY codRudenie), '') AS nume FROM gospodarii WHERE(gospodarieId = '" + GasesteIdFinal(Convert.ToInt64(vTabel["contractGospodarieIdDa"]), Convert.ToInt32(Session["SESan"])) + "')";
            //    vCmd1.CommandText = @"SELECT volum, nrPozitie,
            //            CASE WHEN persJuridica = 1 THEN jUnitate 
            //                 ELSE COALESCE ((SELECT TOP (1) nume FROM membri WHERE (gospodarieId = gospodarii.gospodarieId) AND (an = '" + Session["SESan"].ToString() + @"') ORDER BY codRudenie), '')
            //                 END AS nume 
            //            FROM gospodarii 
            //            WHERE(gospodarieId = '" + vTabel["contractGospodarieIdDa"] + "')";
            //    SqlDataReader vTabel1 = vCmd1.ExecuteReader();
            //    if (vTabel1.Read())
            //    {
            //        vVolumDa = vTabel1["volum"].ToString();
            //        vPozitieDa = vTabel1["nrPozitie"].ToString();
            //        vNumeDa = vTabel1["nume"].ToString();
            //    }
            //    vTabel1.Close();
            //    //vCmd1.CommandText = "SELECT volum, nrPozitie, COALESCE ((SELECT TOP (1) nume FROM membri WHERE (gospodarieId = gospodarii.gospodarieId) AND (an = '" + Session["SESan"].ToString() + "') ORDER BY codRudenie), '') AS nume FROM gospodarii WHERE(gospodarieId = '" + GasesteIdFinal(Convert.ToInt64(vTabel["contractGospodarieIdPrimeste"]), Convert.ToInt32(Session["SESan"]))+ "')";

            //    vCmd1.CommandText = @"SELECT volum, nrPozitie,
            //            CASE WHEN persJuridica = 1 THEN jUnitate 
            //                 ELSE COALESCE ((SELECT TOP (1) nume FROM membri WHERE (gospodarieId = gospodarii.gospodarieId) AND (an = '" + Session["SESan"].ToString() + @"') ORDER BY codRudenie), '')
            //                 END AS nume 
            //            FROM gospodarii 
            //            WHERE(gospodarieId = '" + vTabel["contractGospodarieIdPrimeste"] + "')";
            //    vTabel1 = vCmd1.ExecuteReader();
            //    if (vTabel1.Read())
            //    {
            //        vVolumPrimeste = vTabel1["volum"].ToString();
            //        vPozitiePrimeste = vTabel1["nrPozitie"].ToString();
            //        vNumePrimeste = vTabel1["nume"].ToString();
            //    }
            //    vTabel1.Close();
            //    vCmd1.CommandText = "INSERT INTO rapContracteArenda (unitateId, utilizatorId, nrContract, dataContract, gospodarieIdDa, gospodarieIdPrimeste, detaliiContract, volumDa, nrPozitieDa, numeDa, volumPrimeste, nrPozitiePrimeste, numePrimeste, detaliiParcela, ha, ari, contractId, tipContract) VALUES ('" + vTabel["unitateId"].ToString() + "', '" + Session["SESutilizatorId"].ToString() + "', '" + vTabel["contractNr"].ToString() + "', convert(datetime,'" + Convert.ToDateTime(vTabel["contractDataSemnarii"].ToString()) + "',104), '" + vTabel["contractGospodarieIdDa"].ToString() + "', '" + vTabel["contractGospodarieIdPrimeste"].ToString() + "', N'" + vTabel["contractDetalii"].ToString() + "', '" + vVolumDa + "', '" + vPozitieDa + "', N'" + vNumeDa + "', '" + vVolumPrimeste + "', '" + vPozitiePrimeste + "', '" + vNumePrimeste + "', '" + vTabel["detaliiParcela"].ToString() + " " + vTabel["parcelaNrCadastral"].ToString() + " " + vTabel["parcelaNrTopo"].ToString() + "', '" + vHa.ToString().Replace(",", ".") + "', '" + vAri.ToString().Replace(",", ".") + "', '" + vTabel["contractId"].ToString() + "', '" + vTabel["tipContract"].ToString() + "')";
            //    vCmd1.ExecuteNonQuery();
            //}
            //vTabel.Close();
            //}
            //ManipuleazaBD.InchideConexiune(vCon);
        }
    }

    private long GasesteIdFinal(long idInitial, int an)
    {
        Gospodarie.Gospodarii gospodarie = new Gospodarie.Gospodarii();

        return gospodarie.GetIdsByIdAndAnForIdFinal(idInitial, an);
    }
    protected void Page_SaveStateComplete(object sender, EventArgs e)
    {
        Warning[] warnings;
        string[] streamids;
        string mimeType;
        string encoding;
        string fileNameExtension;
        byte[] bytes = ReportViewer1.LocalReport.Render("PDF", null, out mimeType, out encoding, out fileNameExtension, out streamids, out warnings);
        Response.Buffer = true;
        Response.Clear();
        Response.ContentType = mimeType;
        Response.AddHeader("content-disposition", "inline; filename=Nume" + fileNameExtension);
        Response.BinaryWrite(bytes);
        Response.Flush();
        Response.End();
        Response.Close();
    }
}
