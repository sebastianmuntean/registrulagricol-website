﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="adevAdaugaAdeverinte.aspx.cs" Inherits="adevAdaugaAdeverinte" EnableEventValidation="false"
    UICulture="ro-Ro" Culture="ro-Ro" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript">
        function AutoCompleteExtenderTipSablon_ItemSelected(source, eventArgs) {
            var vControl = document.getElementById("ctl00_ContentPlaceHolder1_tbTipSablonId");
            var vStr = eventArgs.get_value();
            vControl.value = vStr;
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Adeverinţe / Adăugare adeverinţă"></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="upListaAdevAnteturi" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnAfiseazaMesaje" runat="server">
                <asp:ValidationSummary ID="valSumAdeverinte" runat="server" DisplayMode="SingleParagraph"
                    Visible="true" ValidationGroup="GrupValidareAdeverinte" CssClass="validator"
                    ForeColor="" />
            </asp:Panel>
            <asp:Panel ID="pnGrid" runat="server" CssClass="panel_general">
                <asp:Panel ID="pnCautare" runat="server" CssClass="cauta">
                    <asp:Label ID="lblCCauta" runat="server" Text="Caută după:" />
                    <asp:Label ID="lblCMembru" runat="server" Text="Membru" />
                    <asp:TextBox ID="tbCMembru" runat="server" AutoPostBack="True" Width="100px" />
                    <asp:Label ID="lblCChitanta" runat="server" Text="Chitanța" />
                    <asp:TextBox ID="tbCChitanta" runat="server" AutoPostBack="True" Width="60px" />
                    <asp:Label ID="lblCIesire" runat="server" Text="Nr. ieşire" />
                    <asp:TextBox ID="tbCIesire" runat="server" AutoPostBack="True" Width="60px" />
                    <asp:Label ID="lblCInregistrare" runat="server" Text="Nr. înreg." />
                    <asp:TextBox ID="tbCInregistrare" runat="server" AutoPostBack="True" Width="60px" />
                    <asp:Label ID="lblCData" runat="server" Text="Emise din" />
                    <asp:TextBox ID="tbCData" runat="server" AutoPostBack="True" Width="80px" onchange="this.value=ValidareData(this.value, 'dd.MM.yyyy')" />
                    <asp:CalendarExtender ID="tbCData_CalendarExtender" runat="server" Enabled="True"
                        Format="dd.MM.yyyy" PopupButtonID="tbCData" TargetControlID="tbCData">
                    </asp:CalendarExtender>
                    <asp:Label ID="lblCDataFinal" runat="server" Text="până în" />
                    <asp:TextBox ID="tbCDataFinal" runat="server" AutoPostBack="True" Width="80px" onchange="this.value=ValidareData(this.value, 'dd.MM.yyyy')" />
                    <asp:CalendarExtender ID="tbCDataFinal_CalendarExtender" runat="server" Enabled="True"
                        Format="dd.MM.yyyy" PopupButtonID="tbCDataFinal" TargetControlID="tbCDataFinal">
                    </asp:CalendarExtender>
                </asp:Panel>
                <asp:GridView ID="gvAdeverinte" runat="server" AllowPaging="True" CssClass="tabela"
                    BorderStyle="None" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="adeverintaId"
                    DataSourceID="sdsAdeverinte" OnSelectedIndexChanged="gvAdeverinte_SelectedIndexChanged"
                    CellPadding="3" GridLines="Vertical" EmptyDataText="Nu sunt adaugate inregistrari"
                    OnRowDataBound="gvAdeverinte_RowDataBound">
                    <Columns>
                        <asp:BoundField DataField="numeMembru" HeaderText="Membru" SortExpression="numeMembru" />
                        <asp:BoundField DataField="numarChitanta" HeaderText="Număr chitanţă" SortExpression="numarChitanta" />
                        <asp:BoundField DataField="numarInregistrare" HeaderText="Număr înregistrare" SortExpression="numarInregistrare" />
                        <asp:BoundField DataField="numarIesire" HeaderText="Număr ieşire" SortExpression="numarIesire" />
                        <asp:BoundField DataField="data" DataFormatString="{0:d}" HeaderText="Data" HtmlEncode="False"
                            SortExpression="data" />
                        <asp:BoundField DataField="motiv" HeaderText="Conţinut" SortExpression="motiv" Visible="false" />
                    </Columns>
                    <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                    <HeaderStyle Font-Bold="True" />
                </asp:GridView>
                <asp:SqlDataSource ID="sdsAdeverinte" runat="server"
                    SelectCommand="SELECT [adeverintaId], [numeMembru], [numarChitanta], [numarInregistrare], [numarIesire], [data], [motiv] FROM [adeverinte] WHERE ( ([data] >= CONVERT(datetime, @datainitiala)) AND ([data] <= CONVERT(datetime, @datafinala, 104)) AND ([gospodarieId] = @gospodarieId) AND ([numarChitanta] LIKE '%' + @numarChitanta + '%') AND ([numarIesire] LIKE '%' + @numarIesire + '%') AND ([numarInregistrare] LIKE '%' + @numarInregistrare + '%') AND ([numeMembru] LIKE '%' + @numeMembru + '%'))">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="tbCData" Name="datainitiala" PropertyName="Text"
                            DbType="Date" DefaultValue="01.01.2000" />
                        <asp:ControlParameter ControlID="tbCDataFinal" Name="datafinala" PropertyName="Text"
                            DbType="Date" DefaultValue="01.01.3000" />
                        <asp:SessionParameter Name="gospodarieId" SessionField="SESgospodarieId" Type="Int64" />
                        <asp:ControlParameter ControlID="tbCChitanta" DefaultValue="%" Name="numarChitanta"
                            PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="tbCIesire" DefaultValue="%" Name="numarIesire" PropertyName="Text"
                            Type="String" />
                        <asp:ControlParameter ControlID="tbCInregistrare" DefaultValue="%" Name="numarInregistrare"
                            PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="tbCMembru" DefaultValue="%" Name="numeMembru" PropertyName="Text"
                            Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </asp:Panel>
            <asp:Panel ID="pnButoanePrincipale" CssClass="butoane" runat="server">
                <asp:Button CssClass="buton" ID="btModifica" Visible="false" runat="server" Text="modifică adeverinţă"
                    OnClick="btModifica_Click" />
                <asp:Button CssClass="buton" ID="btSterge" Visible="false" runat="server" Text="şterge adeverinţă"
                    OnClientClick="return confirm (&quot;Sigur ştergeţi adeverința aleasă ?&quot;)"
                    OnClick="btSterge_Click" />
                <asp:Button CssClass="buton" ID="btAdauga" runat="server" Text="adaugă adeverinţă"
                    OnClick="btAdauga_Click" />
                <asp:Button CssClass="buton" ID="btTiparire" Visible="false" runat="server" Text="tipărire adeverinţă"
                    OnClick="btTiparire_Click" />
            </asp:Panel>
            <asp:Panel ID="pnAdaugaAdeverinta" CssClass="panel_general" runat="server" Visible="false">
                <asp:Panel ID="Panel1" CssClass="adauga" runat="server">
                    <asp:Panel ID="Panel2" runat="server">
                        <h2>
                            ADAUGĂ / MODIFICĂ O ADEVERINŢĂ</h2>
                    </asp:Panel>
                    <asp:UpdatePanel ID="upAdauga" runat="server" OnLoad="upAdauga_Load">
                        <ContentTemplate>
                            <asp:Panel ID="pn9Coloane" runat="server">
                                <p>
                                    <asp:Label ID="lblTipSablon" runat="server" Text="Tip adeverinţă"></asp:Label>
                                    <asp:TextBox ID="tbTipSablon" runat="server" AutoPostBack="true" OnTextChanged="tbTipSablon_TextChanged"
                                        Width="400px"></asp:TextBox>
                                    <asp:TextBoxWatermarkExtender ID="tbTipSablon_TextBoxWatermarkExtender" runat="server"
                                        Enabled="True" TargetControlID="tbTipSablon" WatermarkCssClass="watermarkTextBox"
                                        WatermarkText="Cautare tip adeverinta">
                                    </asp:TextBoxWatermarkExtender>
                                    <asp:AutoCompleteExtender ID="AutoCompleteExtenderTipSablon" runat="server" BehaviorID="AutoCompleteEx3"
                                        CompletionInterval="10" CompletionListCssClass="autocomplete_completionListElement"
                                        CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem" CompletionListItemCssClass="autocomplete_listItem"
                                        CompletionSetCount="5" ContextKey="cont" DelimiterCharacters=";,:" EnableCaching="True"
                                        Enabled="True" FirstRowSelected="True" MinimumPrefixLength="1" OnClientItemSelected="AutoCompleteExtenderTipSablon_ItemSelected"
                                        ServiceMethod="GetCompletionList" ServicePath="~/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="false"
                                        TargetControlID="tbTipSablon" UseContextKey="True">
                                    </asp:AutoCompleteExtender>
                                    <asp:Label Text="an" runat="server"></asp:Label>
                                    <asp:TextBox ID="adeverintaAnTextBox" AutoPostBack="true" runat ="server" Width="50px"></asp:TextBox>
                                </p>
                                <asp:Panel ID="pnlRest" runat="server" Visible="true">
                                    <p>
                                        <asp:Label ID="lblMembru" runat="server" Text="Membru"></asp:Label>
                                        <asp:DropDownList ID="ddlMembru" runat="server" DataSourceID="sdsMembri" DataTextField="nume"
                                            DataValueField="id" AutoPostBack="true" OnSelectedIndexChanged="ddlMembru_SelectedIndexChanged"
                                            Width="200px">
                                        </asp:DropDownList>
                                        <asp:SqlDataSource ID="sdsMembri" runat="server" 
                                            SelectCommand="SELECT CASE persjuridica WHEN 'False' THEN coalesce(nume,'') ELSE coalesce(junitate, '') END AS nume, CASE persjuridica WHEN 'False' THEN capitolId ELSE 0 END AS id FROM membri INNER JOIN gospodarii ON membri.gospodarieId = gospodarii.gospodarieId WHERE (membri.gospodarieId = @gospodarieId)">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="gospodarieId" SessionField="SESgospodarieId" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </p>
                                    <p>
                                        <asp:Label ID="lblNumarChitanta" runat="server" Text="Număr chitanţă"></asp:Label>
                                        <asp:TextBox ID="tbNumarChitanta" runat="server" AutoPostBack="true"></asp:TextBox>
                                        <asp:Label ID="lblNumarInreg" runat="server" Text="Număr înregistrare"></asp:Label>
                                        <asp:TextBox ID="tbNumarInreg" runat="server" AutoPostBack="true"></asp:TextBox>
                                        <asp:Label ID="lblNumarIesire" runat="server" Text="Număr ieşire"></asp:Label>
                                        <asp:TextBox ID="tbNumarIesire" runat="server" AutoPostBack="true"></asp:TextBox>
                                    </p>
                                    <p>
                                        <asp:Label ID="lblData" runat="server" Text="Data"></asp:Label>
                                        <asp:TextBox ID="tbData" runat="server" AutoPostBack="true"></asp:TextBox>
                                    </p>
                                    <p>
                                        <asp:GridView ID="gvCampuriNecompletate" runat="server" CssClass="gv_adeverinte"
                                            AutoGenerateColumns="False" DataSourceID="sdsCampuriNecompletate" OnRowDataBound="gvCampuriNecompletate_RowDataBound" EnableModelValidation="True" Width="1267px">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Câmpuri suplimentare" SortExpression="id">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCamp" runat="server" Text=""></asp:Label>
                                                        <asp:TextBox ID="tbCamp" AutoPostBack="true" runat="server" OnTextChanged="tbCamp_TextChanged"></asp:TextBox>
                                                        <asp:DropDownList ID="ddlCampMembruDataNasterii" AutoPostBack="true" Visible="false"
                                                            runat="server" DataSourceID="sdsMembriDataNasteriiCampuri" DataTextField="numeDataNasterii"
                                                            DataValueField="numeDataNasterii" OnSelectedIndexChanged="ddlCamp_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                        <asp:SqlDataSource ID="sdsMembriDataNasteriiCampuri" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016 %>"
                                                            SelectCommand="SELECT nume, dataNasterii, nume + N', CNP ' + coalesce(cnp,'-') + ', născut(ă) la data de ' + CASE CONVERT(nvarchar, dataNasterii, 104) WHEN '01.01.1900' THEN '____________' ELSE coalesce(CONVERT(nvarchar, dataNasterii, 104),'_____________') END + ', ' AS numeDataNasterii FROM membri WHERE (gospodarieId = @gospodarieId) ORDER BY nume">
                                                            <SelectParameters>
                                                                <asp:SessionParameter Name="gospodarieId" SessionField="SESgospodarieId" Type="Int32" />
                                                            </SelectParameters>
                                                        </asp:SqlDataSource>
                                                        <asp:DropDownList ID="ddlCampMembru" AutoPostBack="true" Visible="false" runat="server"
                                                            DataSourceID="sdsMembriCampuri" DataTextField="nume" DataValueField="nume" OnSelectedIndexChanged="ddlCamp_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                        <asp:SqlDataSource ID="sdsMembriCampuri" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016 %>"
                                                            SelectCommand="SELECT nume FROM membri WHERE (gospodarieId = @gospodarieId) ORDER BY nume">
                                                            <SelectParameters>
                                                                <asp:SessionParameter Name="gospodarieId" SessionField="SESgospodarieId" Type="Int32" />
                                                            </SelectParameters>
                                                        </asp:SqlDataSource>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                        <asp:SqlDataSource ID="sdsCampuriNecompletate" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016 %>"
                                            SelectCommand="SELECT * FROM [grid]" OnInit="sdsCampuriNecompletate_Init">
                                        </asp:SqlDataSource>
                                    </p>
                                    <asp:Panel ID="Panel3" runat="server">
                                        <h2>
                                            Antet adeverinţă</h2>
                                        <asp:Label ID="lblAntet" runat="server" Text="Antet" Width="700px"></asp:Label>
                                    </asp:Panel>
                                    <asp:Panel ID="Panel4" runat="server">
                                        <h2>
                                            Conţinut adeverinţă</h2>
                                        <asp:Label ID="lblContinut" runat="server" Text="Conţinut" Width="700px"></asp:Label>
                                        <asp:TextBox ID="tbContinut" TextMode="MultiLine" Rows="3" Width="700px" runat="server"
                                            Visible="false"></asp:TextBox>
                                    </asp:Panel>
                                    <asp:Panel ID="Panel5" runat="server">
                                        <h2>
                                            Subsol adeverinţă</h2>
                                        <asp:Label ID="lblSubsol" runat="server" Text="Subsol" Width="700px"></asp:Label>
                                    </asp:Panel>
                                </asp:Panel>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
                <asp:Panel ID="Panel6" runat="server" CssClass="">
                    <asp:RadioButtonList ID="rbPePagina" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="1">Una pe pagină</asp:ListItem>
                        <asp:ListItem Selected="True" Value="2">Două pe pagină</asp:ListItem>
                    </asp:RadioButtonList>
                </asp:Panel>
                <asp:Panel ID="pnButoaneAdaugare" runat="server" CssClass="butoane">
                    <asp:Button ID="btSalveaza" runat="server" CssClass="buton" Text="adaugă o adeverinţă"
                        OnClick="btSalveaza_Click" />
                    <asp:Button ID="btEditareAvansata" runat="server" CssClass="buton" Text="editare avansată"
                        OnClick="btEditareAvansata_Click" />
                    <asp:Button ID="btAnuleazaSalvarea" runat="server" CssClass="buton" Text="listă adeverinţe"
                        OnClick="btAnuleazaSalvarea_Click" />
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="pnVal" CssClass="ascunsa" runat="server">
                <!-- //////////// VALIDATOARE  \\\\\\\\\\\\\\\\\\ -->
                <asp:CustomValidator ID="valCustom" runat="server" ErrorMessage="" ValidationGroup="GrupValidareAdeverinte"></asp:CustomValidator>
            </asp:Panel>
            <asp:TextBox ID="tbTipSablonId" Style="display: none; visibility: hidden;" runat="server"></asp:TextBox>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
