﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class reparaCapitol3DinContracte : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btAdauga_Click(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd1 = new SqlCommand();
        SqlCommand vCmd2 = new SqlCommand();
        vCmd1.Connection = vCon;
        vCmd2.Connection = vCon;
        vCmd1.CommandText = "select unitateId from unitati where convert(nvarchar,unitateId) like '" + ddlUnitate.SelectedValue + "'";
        List<string> vListaUnitati = new List<string>();
        SqlDataReader vTabel = vCmd1.ExecuteReader();
        while (vTabel.Read())
            vListaUnitati.Add(vTabel["unitateId"].ToString());
        vTabel.Close();
        // parcurg unitati
        foreach (string vUnitateId in vListaUnitati)
        {
            vCmd1.CommandText = "SELECT parceleAtribuiriContracte.contractGospodarieIdDa, parceleAtribuiriContracte.contractGospodarieIdPrimeste, parceleModUtilizare.c3Rand, SUM(parceleModUtilizare.c3Ha) AS c3Ha, SUM(parceleModUtilizare.c3Ari) AS c3Ari, parceleModUtilizare.catreC3Rand, parceleAtribuiriContracte.contractDataFinal FROM parceleAtribuiriContracte INNER JOIN parceleModUtilizare ON parceleAtribuiriContracte.contractId = parceleModUtilizare.contractId INNER JOIN gospodarii AS gospodariiDa ON parceleAtribuiriContracte.contractGospodarieIdDa = gospodariiDa.gospodarieId INNER JOIN gospodarii AS gospodariiPrimeste ON parceleAtribuiriContracte.contractGospodarieIdPrimeste = gospodariiPrimeste.gospodarieId WHERE (YEAR(parceleAtribuiriContracte.contractDataFinal) >= " + (Convert.ToInt32(Session["SESan"])-1).ToString() + ") AND (gospodariiDa.unitateId = '" + vUnitateId + "') AND (gospodariiPrimeste.unitateId = '" + vUnitateId + "') AND (gospodariiDa.an >= '" + (Convert.ToInt32(Session["SESan"])-1).ToString() + "') AND (gospodariiPrimeste.an >= '" +(Convert.ToInt32(Session["SESan"])-1).ToString() + "') GROUP BY parceleAtribuiriContracte.contractGospodarieIdDa, parceleAtribuiriContracte.contractGospodarieIdPrimeste, parceleModUtilizare.c3Rand, parceleModUtilizare.catreC3Rand, parceleAtribuiriContracte.contractDataFinal";
            List<ListaSelect> vListaContracte = new List<ListaSelect>();
            vTabel = vCmd1.ExecuteReader();
            while (vTabel.Read())
            {
                ListaSelect vElementLista = new ListaSelect()
                {
                    Col1 = vTabel["contractGospodarieIdDa"].ToString(),
                    Col2 = vTabel["contractGospodarieIdPrimeste"].ToString(),
                    Col3 = vTabel["c3Rand"].ToString(),
                    Col4 = vTabel["c3Ha"].ToString(),
                    Col5 = vTabel["c3Ari"].ToString(),
                    Col6 = vTabel["catreC3Rand"].ToString(),
                    Col7=vTabel["contractDataFinal"].ToString()
                };
                vListaContracte.Add(vElementLista);
            }
            vTabel.Close();
            List<string> vListaScazutC3 = new List<string>();
            foreach (ListaSelect vContract in vListaContracte)
            {
                // daca nu am golit C3 il golesc
                // pentru gospodaria care da
                if (vListaScazutC3.Count(a => a == vContract.Col1) == 0)
                {
                    vCmd1.CommandText = "UPDATE capitole SET col1 = '0', col2 = '0' WHERE (gospodarieId = '" + vContract.Col1 + "')  AND (an = '" + Session["SESan"].ToString() + "') AND (codCapitol = '3') AND (codRand <> '1') ";
                    vCmd1.ExecuteNonQuery();
                    vListaScazutC3.Add(vContract.Col1);
                }
                // pentru gospodaria care primeste
                if (vListaScazutC3.Count(a => a == vContract.Col2) == 0)
                {
                    vCmd1.CommandText = "UPDATE capitole SET col1 = '0', col2 = '0' WHERE (gospodarieId = '" + vContract.Col2 + "')  AND (an = '" + Session["SESan"].ToString() + "') AND (codCapitol = '3') AND (codRand <> '1') ";
                    vCmd1.ExecuteNonQuery();
                    vListaScazutC3.Add(vContract.Col2);
                }

                if (Convert.ToDateTime(vContract.Col7).Year < Convert.ToInt32(Session["SESan"]))
                {
                    vContract.Col4 = "0";
                    vContract.Col5 = "0";
                }

                // modific datele in C3 pentru gospodaria care da
                updateC3(vCmd1, vContract.Col1, vContract.Col3, vContract.Col4, vContract.Col5);
                // modific datele in C3 pentru gospodaria care primeste
                updateC3(vCmd1, vContract.Col2, vContract.Col6, vContract.Col4, vContract.Col5);
            }
        }
        ManipuleazaBD.InchideConexiune(vCon);
    }
    private void updateC3(SqlCommand pCmd, string pGospodarieId, string pRand, string pHa, string pAri)
    {
        if (pGospodarieId == "620576")
        { }
        SqlCommand vCmd = pCmd;
        string vInterogare = "";
        // iau valoarea din C3
        decimal vHaC3 = Convert.ToDecimal(pHa), vAriC3 = Convert.ToDecimal(pAri);
        vCmd.CommandText = "select col1 from capitole where gospodarieId='" + pGospodarieId + "' and codCapitol='3' and codRand='" + pRand + "'";
        vHaC3 += Convert.ToDecimal(vCmd.ExecuteScalar());
        vCmd.CommandText = "select col2 from capitole where gospodarieId='" + pGospodarieId + "' and codCapitol='3' and codRand='" + pRand + "'";
        vAriC3 += Convert.ToDecimal(vCmd.ExecuteScalar());
        vHaC3 += Math.Truncate(vAriC3 / 100);
        vAriC3 -= Math.Truncate(vAriC3 / 100) * 100;
        // fac update pe randul pRand
        vInterogare += "update capitole set col1='" + vHaC3.ToString().Replace(",", ".") + "', col2='" + vAriC3.ToString().Replace(",", ".") + "' where gospodarieId='" + pGospodarieId + "' and codCapitol='3' and codRand='" + pRand + "'; ";
        // fac totaluri
        // pentru randul 2
        if (Convert.ToInt32(pRand) >= 3 && Convert.ToInt32(pRand) <= 8)
        {
            // adun la randul 2 suprafata
            decimal vHaR2 = Convert.ToDecimal(pHa), vAriR2 = Convert.ToDecimal(pAri);
            vCmd.CommandText = "select col1 from capitole where gospodarieId='" + pGospodarieId + "' and codCapitol='3' and codRand='2'";
            vHaR2 += Convert.ToDecimal(vCmd.ExecuteScalar());
            vCmd.CommandText = "select col2 from capitole where gospodarieId='" + pGospodarieId + "' and codCapitol='3' and codRand='2'";
            vAriR2 += Convert.ToDecimal(vCmd.ExecuteScalar());
            vHaR2 += Math.Truncate(vAriR2 / 100);
            vAriR2 -= Math.Truncate(vAriR2 / 100) * 100;
            // fac update pe randul 2
            vInterogare += "update capitole set col1='" + vHaR2.ToString().Replace(",", ".") + "', col2='" + vAriR2.ToString().Replace(",", ".") + "' where gospodarieId='" + pGospodarieId + "' and codCapitol='3' and codRand='2'; ";
            // fac totalul de la randul 17
            decimal vHaR17 = Convert.ToDecimal(pHa), vAriR17 = Convert.ToDecimal(pAri);
            vCmd.CommandText = "select col1 from capitole where gospodarieId='" + pGospodarieId + "' and codCapitol='3' and codRand='17'";
            vHaR17 += Convert.ToDecimal(vCmd.ExecuteScalar());
            vCmd.CommandText = "select col2 from capitole where gospodarieId='" + pGospodarieId + "' and codCapitol='3' and codRand='17'";
            vAriR17 += Convert.ToDecimal(vCmd.ExecuteScalar());
            vHaR17 += Math.Truncate(vAriR17 / 100);
            vAriR17 -= Math.Truncate(vAriR17 / 100) * 100;
            // fac update pe randul 17
            vInterogare += "update capitole set col1='" + vHaR17.ToString().Replace(",", ".") + "', col2='" + vAriR17.ToString().Replace(",", ".") + "' where gospodarieId='" + pGospodarieId + "' and codCapitol='3' and codRand='17'; ";
        }
        // pentru randul 9
        if (Convert.ToInt32(pRand) >= 10 && Convert.ToInt32(pRand) <= 16)
        {
            // adun la randul 9 suprafata
            decimal vHaR9 = Convert.ToDecimal(pHa), vAriR9 = Convert.ToDecimal(pAri);
            vCmd.CommandText = "select col1 from capitole where gospodarieId='" + pGospodarieId + "' and codCapitol='3' and codRand='9'";
            vHaR9 += Convert.ToDecimal(vCmd.ExecuteScalar());
            vCmd.CommandText = "select col2 from capitole where gospodarieId='" + pGospodarieId + "' and codCapitol='3' and codRand='9'";
            vAriR9 += Convert.ToDecimal(vCmd.ExecuteScalar());
            vHaR9 += Math.Truncate(vAriR9 / 100);
            vAriR9 -= Math.Truncate(vAriR9 / 100) * 100;
            // fac update pe randul 9
            vInterogare += "update capitole set col1='" + vHaR9.ToString().Replace(",", ".") + "', col2='" + vAriR9.ToString().Replace(",", ".") + "' where gospodarieId='" + pGospodarieId + "' and codCapitol='3' and codRand='9'; ";
            // fac totalul de la randul 17 - scad din 17 suma
            decimal vHaR17 = Convert.ToDecimal(pHa), vAriR17 = Convert.ToDecimal(pAri);
            vCmd.CommandText = "select col1 from capitole where gospodarieId='" + pGospodarieId + "' and codCapitol='3' and codRand='17'";
            vHaR17 = Convert.ToDecimal(vCmd.ExecuteScalar()) - vHaR17;
            vCmd.CommandText = "select col2 from capitole where gospodarieId='" + pGospodarieId + "' and codCapitol='3' and codRand='17'";
            vAriR17 = Convert.ToDecimal(vCmd.ExecuteScalar()) - vAriR17;
            vHaR17 += Math.Truncate(vAriR17 / 100);
            vAriR17 -= Math.Truncate(vAriR17 / 100) * 100;
            // fac update pe randul 17
            vInterogare += "update capitole set col1='" + vHaR17.ToString().Replace(",", ".") + "', col2='" + vAriR17.ToString().Replace(",", ".") + "' where gospodarieId='" + pGospodarieId + "' and codCapitol='3' and codRand='17'; ";
        }
        // execut comenzile de update
        if (vInterogare != "")
        {
            vCmd.CommandText = vInterogare;
            vCmd.ExecuteNonQuery();
            vInterogare = "";
        }
    }
}