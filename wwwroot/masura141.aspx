﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="masura141.aspx.cs" Inherits="masura141" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="<%$ Resources:Resursa, raMasura141Titlu%>" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="pnMasura141" runat="server" CssClass="panel_general" Visible="true">
        <!-- lista Membri -->
        <asp:Panel ID="pnListaMembri" runat="server" CssClass="panel_general" Visible="true">
            <asp:Panel ID="pnMembri" runat="server">
                <asp:GridView ID="gvMembri" AllowPaging="True" AllowSorting="True" CssClass="tabela"
                    runat="server" AutoGenerateColumns="False" DataKeyNames="capitolId" DataSourceID="SqlMembri"
                    OnDataBound="gvMembri_DataBound">
                    <Columns>
                        <asp:BoundField DataField="capitolId" HeaderText="Nr." InsertVisible="False" ReadOnly="True"
                            SortExpression="capitolId" Visible="false" />
                        <asp:BoundField DataField="codRand" HeaderText="rand" SortExpression="codRand" Visible="false" />
                        <asp:BoundField DataField="nume" HeaderText="nume şi prenume" SortExpression="nume" />
                        <asp:BoundField DataField="denumireRudenie" HeaderText="grad de rudenie" SortExpression="denumireRudenie" />
                        <asp:BoundField DataField="codSex" HeaderText="sex" SortExpression="codSex" />
                        <asp:BoundField DataField="mentiuni" HeaderText="menţiuni" SortExpression="mentiuni" />
                        <asp:BoundField DataField="dataNasterii" HeaderText="data naşterii" SortExpression="dataNasterii"
                            DataFormatString="{0:d}" HtmlEncode="False" />
                        <asp:BoundField DataField="cnp" HeaderText="CNP" SortExpression="cnp" />
                    </Columns>
                    <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                </asp:GridView>
                <asp:SqlDataSource ID="SqlMembri" runat="server" 
                    SelectCommand="SELECT [codSex], [codRand], [mentiuni], [dataNasterii], [denumireRudenie], [cnp], [nume], [capitolId] FROM [membri] WHERE ([gospodarieId] = @gospodarieId)">
                    <SelectParameters>
                        <asp:SessionParameter DefaultValue="0" Name="gospodarieId" SessionField="SESgospodarieId"
                            Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </asp:Panel>
        </asp:Panel>
        <asp:Panel ID="Panel1" runat="server" CssClass="adauga">
        <div>
            <asp:Label ID="lblMembriEligibili" runat="server" Text=""></asp:Label></div>
             <div><asp:Label ID="lblValoareTotalaUDE" runat="server" Text=""></asp:Label></div>

            <div class="citat">
            <span  class="citat">Din Ghidul Solicitantului pentru Măsura 141 - <a href="http://www.apdrp.ro/" target="_blank">www.apdrp.ro</a>:</span><br />
            <span class="citat">
           2.1 Cine poate beneficia de fonduri nerambursabile
Beneficiarii eligibili pentru sprijinul nerambursabil acordat prin Măsura 141 sunt
persoanele fizice în vârstă de până la 62 de ani (neîmpliniţi la data depunerii Cererii de
finanţare), care desfăsoară activităţi economice, în principal activităţi agricole si a căror
exploataţie agricolă:<br />
- are o dimensiune economică cuprinsă între 2 si 8 UDE1;<br />
- este situată pe teritoriul ţării;<br />
- este înregistrată în Registrul unic de identificare /Registrul agricol;<br />
- comercializează o parte din producţia agricolă obţinută.<br />
Persoanele fizice pot desfăsura activităţi economice si se pot înregistra si autoriza în
conformitate cu prevederile Ordonanţei de Urgenţă a Guvernului nr. 44/2008, cu
modificările si completările ulterioare:<br />
- individual si independent, ca persoane fizice autorizate;<br />
- ca întreprinzători titulari ai unei întreprinderi individuale;<br />
- ca membri ai unei întreprinderi familiale.<br />
IMPORTANT! Sprijinul acordat prin acesta măsură se aplică la nivel naţional.<br />
IMPORTANT! Persoanele fizice neautorizate la data depunerii Cererii de finanţare,
pot fi beneficiari eligibili ai acestei măsuri, dacă se înregistrează si autorizează în
condiţiile Ordonanţei de Urgenţă a Guvernului nr. 44/2008 cu completările si
modificările ulterioare, până la data semnării Deciziei de finanţare cu APDRP.<br />
Persoanele fizice autorizate în condiţiile Ordonanţei de Urgenţă a Guvernului nr.
44/2008 cu completările si modificările ulterioare nu pot solicita sprijin
nerambursabil ca persoană fizică neautorizată.<br />
Persoanele fizice autorizate în condiţiile Ordonanţei de Urgenţă a Guvernului nr.
44/2008 cu completările si modificările ulterioare, trebuie să deţină, până la data
semnării Deciziei de finanţare, ca activitate principală, una din activităţile prevăzute
la capitolul "Agricultură din cod CAEN Rev. 2."<br />
ATENŢIE! Un singur membru al familiei poate obţine sprijinul pentru aceeasi
exploataţie agricolă (gospodărie familială).<br />
            </span>
            </div>
        </asp:Panel>
        <asp:Panel ID="Panel2" runat="server">
            <asp:Label ID="lblTabel" runat="server" Text=""></asp:Label>
        </asp:Panel>
    </asp:Panel>
</asp:Content>
