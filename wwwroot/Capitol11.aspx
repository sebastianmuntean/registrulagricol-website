﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="capitol11.aspx.cs" Inherits="capitol11_nou" UICulture="ro-Ro" Culture="ro-Ro"
    EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <%-- <asp:Label ID="url" runat="server" Text="Capitole / CAPITOLUL XI: Construcţii existente la începutul anului pe raza localităţii" />--%>
    <asp:Label ID="url" runat="server" Text="Capitole / CAPITOLUL XI: Clădiri existente la începutul anului pe raza localității" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="upCladiri" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnAfiseazaMesaje" runat="server">
                <asp:ValidationSummary ID="validator" runat="server" DisplayMode="SingleParagraph"
                    Visible="true" ValidationGroup="GrupValidare" CssClass="validator" ForeColor="" />
            </asp:Panel>
            <asp:Panel ID="cladiriPanel" runat="server" CssClass="panel_general" Visible="true">
                <asp:Panel ID="pnCautare" runat="server" CssClass="cauta">
                   <%-- <asp:Label ID="searchAdresaLabel" runat="server" Text="Adresa" />
                    <asp:TextBox ID="searchAdresaTextbox" runat="server" autocomplete="off" AutoPostBack="True" Width="100px" />
                    <asp:Label ID="searchZonaLabel" runat="server" Text="Zona" />
                    <asp:TextBox ID="searchZonaTextBox" runat="server" autocomplete="off" AutoPostBack="True" Width="50px" />--%>
                </asp:Panel>
                <asp:Panel ID="buildingsPanel" runat="server">
                    <asp:GridView ID="cladiriGridView" AllowPaging="True" AllowSorting="True" CssClass="tabela" runat="server" AutoGenerateColumns="False" DataKeyNames="id" EmptyDataText="Nu există date" OnRowDataBound="CladiriGridViewRowDataBound" OnSelectedIndexChanged="CladiriGridViewSelectedIndexChanged"  ShowFooter="True">
                        <Columns>
                            <asp:BoundField DataField="id" HeaderText="id" Visible="false" />
                            <asp:BoundField DataField="adresa" HeaderText="Adresa" SortExpression="adresa" />
                            <asp:BoundField DataField="zona" HeaderText="Zona" SortExpression="zona" />
                            <asp:BoundField DataField="suprafata" HeaderText="Suprafaţa" SortExpression="suprafata" />
                            <asp:BoundField DataField="tip" HeaderText="Tipul" SortExpression="tip" />
                            <asp:BoundField DataField="destinatie" HeaderText="Destinaţia" SortExpression="destinatie" />
                            <asp:BoundField DataField="anTerminare" HeaderText="Anul terminării" SortExpression="anTerminare" />
                        </Columns>
                        <FooterStyle CssClass="footer" />
                        <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                    </asp:GridView>
                </asp:Panel>
                <asp:Panel ID="mainButtonsPanel" CssClass="butoane" runat="server">
                    <asp:Button CssClass="buton" ID="toUpdatePanelButton" runat="server" Text="modifică" OnClick="ToUpdatePanelButtonClick" Visible="false" />
                    <asp:Button CssClass="buton" ID="deleteButton" runat="server" Text="şterge" OnClick="DeleteButtonClick" ValidationGroup="GrupValidareParcele" OnClientClick="return confirm(&quot;Sunteți sigur că doriți să ștergeți cladirea?&quot;)" Visible="false" />
                    <asp:Button CssClass="buton" ID="toAddPanelButton" runat="server" Text="adaugă" OnClick="ToAddPanelButtonClick" />
                    <asp:Button ID="chapterListButton" runat="server" CssClass="buton" Text="listă gospodării" Visible="true" PostBackUrl="~/Gospodarii.aspx" />
                    <asp:Button ID="printChapterButton" runat="server" CssClass="buton" Text="tipărire capitol" Visible="true" OnClick="PrintChapterButtonClick" />
                     <asp:Button ID="btTiparireExcel" runat="server" Text="salveaza in Excel" CssClass="buton" OnClick="PrintChapterButtonClickExcel" />
                     <asp:Button ID="reparaButton" runat="server" CssClass="buton" Text="adu cladiri lipsa" Visible="true" OnClick="ReparaButtonClick" />

                </asp:Panel>
            </asp:Panel>

            <asp:Panel ID="addBuildingGeneralPanel" CssClass="panel_general" runat="server" Visible="false">
                <asp:Panel ID="addBuildingPanel" CssClass="adauga" runat="server">
                    <asp:Panel ID="pnAdTitlu" runat="server">
                        <h2>ADAUGĂ / MODIFICĂ CLADIREA SELECTATĂ</h2>
                    </asp:Panel>
                    <asp:Panel ID="controlsForAddBuildingPanel" runat="server">
                        <p>
                            <asp:Label ID="addAdresaLabel" runat="server" Text="Adresa" />
                            <asp:TextBox ID="addAdresaTextBox" runat="server" Width="450px"></asp:TextBox>
                            <asp:Label ID="addZonaLabel" runat="server" Text="Zona" />
                            <asp:TextBox ID="addZonaTextBox" runat="server" Width="550px"></asp:TextBox>
                            <br /><br />
                            <asp:Label ID="addSuprafataLabel" runat="server" Text="Suprafaţa" />
                            <asp:TextBox ID="addSuprafataTextBox" runat="server" Width="100px"></asp:TextBox>
                             <asp:Label ID="addTypeLabel" runat="server" Text="Tipul" />
                            <asp:TextBox ID="addTypeTextBox" runat="server" Width="100px"></asp:TextBox>
                            <asp:Label ID="addDestinationLabel" runat="server" Text="Destinaţia" />
                            <asp:TextBox ID="addDestinationTextBox" runat="server" Width="50px"></asp:TextBox>
                             <asp:Label ID="addFinishYearLabel" runat="server" Text="Anul terminării" />
                            <asp:TextBox ID="addFinishYearTextBox" runat="server" Width="50px"></asp:TextBox>
                        </p>
                        <p>
                            <asp:Panel ID="buttonsPanel" runat="server" CssClass="butoane">  <%--<asp:Button Text="Apasa aici" runat="server" OnClick="RelocateButtonClick" ID="addButton" Visible ="true" />--%>
                                <asp:Button CssClass="buton" ID="addButton" runat="server" Text="adaugă" OnClick="AddButtonClick" ValidationGroup="GrupValidare" Visible="false" />
                                <asp:Button ID="updateButton" runat="server" CssClass="buton" OnClick="UpdateButtonClick" Text="modifică" ValidationGroup="GrupValidare" Visible="false" />
                                <asp:Button ID="btModificaArataLista" runat="server" CssClass="buton" OnClick="ModificaArataLista" Text="lista clădirilor" Visible="false" />
                                <asp:Button ID="btAdaugaArataListaCladiri" runat="server" CssClass="buton" OnClick="BtAdaugaArataListaCladiriClick" Text="lista clădirilor" Visible="false" />
                            </asp:Panel>
                            <asp:Panel ID="Panel5" CssClass="ascunsa" runat="server">
                                <asp:RequiredFieldValidator ID="addAdresaValidator" ControlToValidate="addAdresaTextBox" ValidationGroup="GrupValidare" runat="server" ErrorMessage=" Adresa este obligatorie! "></asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="valCustom" runat="server" ErrorMessage="x" ValidationGroup="GrupValidare"></asp:CustomValidator>
                            </asp:Panel>
                        </p>
                    </asp:Panel>
                </asp:Panel>
              

            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

