﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class reparaAdeverinteInchidereAn : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btSalveazaAdeverinte_Click(object sender, EventArgs e)
    {
        SqlConnection vConn = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vConn;

        SqlConnection vConn2 = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd2 = new SqlCommand();
        vCmd2.Connection = vConn2;
        

        vCmd.CommandText = "select a.adeverintaId as adeverintaId, g.gospodarieId as gospodarieID, g.membruCnp as membruCnp, a.unitateID, a.utilizatorID, a.numeMembru, a.numarChitanta, a.numarInregistrare, a.numarIesire, a.data, a.antet, a.text, a.textTiparit, a.motiv, a.subsol, a.ora, a.minut, a.tipSablon from gospodarii as g inner join adeverinte as a on g.gospodarieId=a.gospodarieId where g.an='" + (Convert.ToInt32(Session["sesAn"]) - 1) + "' and year(a.data)='" + Session["sesAn"] + "' and g.unitateId='"+ddlUnitatea.SelectedValue+"'";
        SqlDataReader vTable = vCmd.ExecuteReader();
        string vInterogare = "";
        while (vTable.Read())
        {
            string vGospodarieIdAnCurent="";
            vCmd2.CommandText = "select gospodarieId from gospodarii where unitateId='" + ddlUnitatea.SelectedValue + "' and an='" + Session["sesAn"] + "' and membruCnp='" + vTable["membruCnp"] + "'";
            vGospodarieIdAnCurent = vCmd2.ExecuteScalar().ToString();

           
            vCmd2.CommandText = "update adeverinte set gospodarieId='" + vGospodarieIdAnCurent + "' where unitateID='" + ddlUnitatea.SelectedValue + "' and adeverintaId='" + vTable["adeverintaId"] + "';";
            vInterogare += vCmd2.CommandText;
        }
        vTable.Close();
        string a = vInterogare;
        vCmd2.CommandText = a;
        vCmd2.ExecuteNonQuery();

        ManipuleazaBD.InchideConexiune(vConn2);
        ManipuleazaBD.InchideConexiune(vConn);
    }
}