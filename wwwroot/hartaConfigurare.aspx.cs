﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ThinkGeo.MapSuite.Core;
using ThinkGeo.MapSuite.WebEdition;
using System.Data.SqlClient;
using System.Collections.ObjectModel;

public partial class hartaConfigurare : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        DBConnections connection = new DBConnections(Convert.ToInt16(Session["SESan"]));

        SqlGospodarii.ConnectionString = connection.Create();
        SqlParcele.ConnectionString = connection.Create();

        if (ViewState["postback"] == null)
            ViewState["postback"] = 0;
        if (ViewState["postback"].ToString() == "1")
            this.harta.Click += new EventHandler<MapClickedEventArgs>(harta_Click);
        if (!Page.IsPostBack)
        {
            // background harta
            harta.MapBackground.BackgroundBrush = new GeoSolidBrush(GeoColor.FromHtml("#E5E3DF"));
            // unitatea de masura
            harta.MapUnit = GeographyUnit.Meter;
            // afiseaza coordonate mouse
            harta.MapTools.MouseCoordinate.Enabled = true;
            
            // layer sid
            MrSidRasterLayer layerHartaSid = new MrSidRasterLayer(MapPath(@"~\Harti\" + extrageCodFiscal() + @"\harta.sid"));
            layerHartaSid.UpperThreshold = double.MaxValue;
            layerHartaSid.LowerThreshold = 0;
            harta.StaticOverlay.Layers.Add(layerHartaSid);
            harta.StaticOverlay.ServerCache = new ServerCache(MapPath("~/CacheHarta"));
            harta.StaticOverlay.ClientCache = new ClientCache(TimeSpan.FromDays(30), "cacheHartaTNT");

            // layer pe care se genereaza si se salveaza parcele
            InMemoryFeatureLayer layerPrincipal = new InMemoryFeatureLayer();
            layerPrincipal.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = PointStyles.CreateSimpleCircleStyle(GeoColor.FromArgb(180, 102, 255, 102), 10, GeoColor.StandardColors.DarkGreen, 1);
            layerPrincipal.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = LineStyles.CreateSimpleLineStyle(GeoColor.StandardColors.Blue, 2, true);
            layerPrincipal.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = AreaStyles.CreateSimpleAreaStyle(GeoColor.FromArgb(180, 102, 255, 102), GeoColor.StandardColors.DarkGreen, 1);
            layerPrincipal.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            layerPrincipal.DrawingQuality = DrawingQuality.HighQuality;

            LayerOverlay overlayPrincipal = new LayerOverlay("overlayPrincipal");
            overlayPrincipal.IsBaseOverlay = false;
            overlayPrincipal.TileType = TileType.SingleTile;
            overlayPrincipal.Layers.Add("layerPrincipal", layerPrincipal);
            //overlayPrincipal.ServerCache = new ServerCache(MapPath("~/Cache"));
            harta.CustomOverlays.Add(overlayPrincipal);

            // fac un layer nou pentru linii selectii
            InMemoryFeatureLayer layerSelectie = new InMemoryFeatureLayer();
            layerSelectie.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = PointStyles.CreateSimpleCircleStyle(GeoColor.FromArgb(180, 102, 255, 102), 10, GeoColor.StandardColors.DarkGreen, 1);
            layerSelectie.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = LineStyles.CreateSimpleLineStyle(GeoColor.StandardColors.Red, 4, true);
            layerSelectie.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = AreaStyles.CreateSimpleAreaStyle(GeoColor.FromArgb(180, 102, 255, 102), GeoColor.StandardColors.DarkGreen, 1);
            layerSelectie.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            layerSelectie.DrawingQuality = DrawingQuality.HighQuality;

            LayerOverlay overlaySelectie = new LayerOverlay("overlaySelectie");
            overlaySelectie.IsBaseOverlay = false;
            overlaySelectie.TileType = TileType.SingleTile;
            overlaySelectie.Layers.Add("layerSelectie", layerSelectie);
            harta.CustomOverlays.Add(overlaySelectie);

            // fac un layer pentru parcelele selectate
            InMemoryFeatureLayer layerParcele = new InMemoryFeatureLayer();
            layerParcele.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = PointStyles.CreateSimpleCircleStyle(GeoColor.FromArgb(180, 102, 255, 102), 2, GeoColor.StandardColors.DarkGreen, 1);
            layerParcele.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = LineStyles.CreateSimpleLineStyle(GeoColor.StandardColors.Green, 2, true);
            layerParcele.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = AreaStyles.CreateSimpleAreaStyle(GeoColor.FromArgb(180, 102, 255, 102), GeoColor.StandardColors.DarkGreen, 1);
            layerParcele.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            layerParcele.DrawingQuality = DrawingQuality.HighQuality;

            LayerOverlay overlayParcele = new LayerOverlay("overlayParcele");
            overlayParcele.IsBaseOverlay = false;
            overlayParcele.TileType = TileType.SingleTile;
            overlayParcele.Layers.Add("layerParcele", layerParcele);
            harta.CustomOverlays.Add(overlayParcele);

            // generez harta
            deseneazaLinii();
            deseneazaParceleExistente();

            // layer pe care se afiseaza textele
            InMemoryFeatureLayer layerTexte = new InMemoryFeatureLayer();
            layerTexte.ZoomLevelSet.ZoomLevel18.DefaultPointStyle = PointStyles.CreateSimpleCircleStyle(GeoColor.FromArgb(180, 102, 255, 102), 2, GeoColor.StandardColors.Yellow, 1);
            layerTexte.ZoomLevelSet.ZoomLevel18.DefaultLineStyle = LineStyles.CreateSimpleLineStyle(GeoColor.StandardColors.Blue, 2, true);
            layerTexte.ZoomLevelSet.ZoomLevel18.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            layerTexte.DrawingQuality = DrawingQuality.HighQuality;

            layerTexte.Open();
            layerTexte.Columns.Add(new FeatureSourceColumn("Nume"));
            layerTexte.Close();
            layerTexte.ZoomLevelSet.ZoomLevel18.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("Nume", "Arial", 10, DrawingFontStyles.Regular, GeoColor.StandardColors.Yellow);
            layerTexte.ZoomLevelSet.ZoomLevel18.DefaultTextStyle.TextLineSegmentRatio = 2;
            layerTexte.ZoomLevelSet.ZoomLevel18.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

            LayerOverlay overlayTexte = new LayerOverlay("overlayTexte");
            overlayTexte.IsBaseOverlay = false;
            overlayTexte.TileType = TileType.SingleTile;
            overlayTexte.Layers.Add("layerTexte", layerTexte);
            //overlayPrincipal.ServerCache = new ServerCache(MapPath("~/CacheHarta"));
            harta.CustomOverlays.Add(overlayTexte);

            scrieTexte();
            harta.ZoomTo(new PointShape(579230, 531669), 100);
        }
    }
    private string extrageCodFiscal()
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select top(1) unitateCodFiscal from unitati where unitateId='" + Session["SESunitateId"].ToString() + "'";
        string vCodFiscal = vCmd.ExecuteScalar().ToString();
        ManipuleazaBD.InchideConexiune(vCon);
        return vCodFiscal;
    }
    private void deseneazaParceleExistente()
    {
        LayerOverlay overlayParcele = (LayerOverlay)harta.CustomOverlays["overlayParcele"];
        InMemoryFeatureLayer layerParcele = (InMemoryFeatureLayer)overlayParcele.Layers["layerParcele"];
        layerParcele.Open();
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "SELECT coordonate.coordonataId, coordonate.coordonataOrdine, coordonate.coordonataX, coordonate.coordonataY, coordonate.coordonataZ, coordonate.parcelaId, 0 AS layerId, parcele.parcelaDenumire, parcele.parcelaCF, parcele.parcelaNrTopo FROM coordonate INNER JOIN parcele ON coordonate.parcelaId = parcele.parcelaId AND coordonate.an = parcele.an WHERE (coordonate.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (coordonate.an = '" + Session["SESan"].ToString() + "') ORDER BY parcelaId, coordonataOrdine";
        SqlDataReader vTabel = vCmd.ExecuteReader();
        List<ListaSelect> vListaSelect = new List<ListaSelect>();
        while (vTabel.Read())
        {
            ListaSelect vElementLista = new ListaSelect()
            {
                Col1 = vTabel["parcelaId"].ToString(),
                Col2 = vTabel["coordonataId"].ToString(),
                Col3 = vTabel["coordonataOrdine"].ToString(),
                Col4 = vTabel["layerId"].ToString(),
                Col5 = Convert.ToDecimal(vTabel["coordonataX"].ToString()).ToString().Replace(".", ","),
                Col6 = Convert.ToDecimal(vTabel["coordonataY"].ToString()).ToString().Replace(".", ","),
                Col7 = Convert.ToDecimal(vTabel["coordonataZ"].ToString()).ToString().Replace(".", ","),
                Col8 = vTabel["parcelaDenumire"].ToString(),
                Col9 = vTabel["parcelaNrTopo"].ToString(),
                Col10 = vTabel["parcelaCF"].ToString()
            };
            vListaSelect.Add(vElementLista);
        }
        vTabel.Close();
        List<Vertex> vListaPuncte = new List<Vertex>();
        PolygonShape vPoligon = new PolygonShape();
        for (int a = 0; a < vListaSelect.Count; a++)
        {
            if (a + 1 < vListaSelect.Count)
            {
                if (vListaSelect[a + 1].Col1 != vListaSelect[a].Col1)
                {
                    Vertex vVertex = new Vertex(Convert.ToDouble(vListaSelect[a].Col5), Convert.ToDouble(vListaSelect[a].Col6));
                    vListaPuncte.Add(vVertex);
                    // adaug pe harta
                    deseneazaPoligon(layerParcele, vListaSelect, vListaPuncte, vPoligon, a);
                    vListaPuncte.Clear();
                }
                else
                {
                    // adaug punct
                    Vertex vVertex = new Vertex(Convert.ToDouble(vListaSelect[a].Col5), Convert.ToDouble(vListaSelect[a].Col6));
                    vListaPuncte.Add(vVertex);
                }
            }
            else
            {
                Vertex vVertex = new Vertex(Convert.ToDouble(vListaSelect[a].Col5), Convert.ToDouble(vListaSelect[a].Col6));
                vListaPuncte.Add(vVertex);
                deseneazaPoligon(layerParcele, vListaSelect, vListaPuncte, vPoligon, a);
                vListaPuncte.Clear();
            }
        }
        ManipuleazaBD.InchideConexiune(vCon);
    }

    private static void deseneazaPoligon(InMemoryFeatureLayer layerParcele, List<ListaSelect> vListaSelect, List<Vertex> vListaPuncte, PolygonShape vPoligon, int a)
    {
        foreach (Vertex v in vListaPuncte)
            vPoligon.OuterRing.Vertices.Add(v);
        if (vPoligon.OuterRing.Vertices.Count > 0)
        {
            if (vPoligon.OuterRing.Vertices[0] == vPoligon.OuterRing.Vertices[vPoligon.OuterRing.Vertices.Count - 1])
            {
                // sterg poligoanele care au acelasi id cu id-ul curent
                if (layerParcele.InternalFeatures.Count > 0)
                {
                    if (layerParcele.InternalFeatures.Contains(vListaSelect[a].Col1))
                        layerParcele.InternalFeatures.Remove(vListaSelect[a].Col1);
                }
                Feature vFeature = new Feature(vPoligon);

                //vFeature.ColumnValues.Add("NumeParcela", vListaSelect[a].Col8 + ", TOPO: " + vListaSelect[a].Col9 + ", CF: " + vListaSelect[a].Col10);
                layerParcele.InternalFeatures.Add(vListaSelect[a].Col1, vFeature);
                // golesc poligon
                vPoligon.OuterRing.Vertices.Clear();
            }
        }
    }
    
    private void deseneazaLinii()
    {
        LayerOverlay overlayPrincipal = (LayerOverlay)harta.CustomOverlays["overlayPrincipal"];
        InMemoryFeatureLayer layerPrincipal = (InMemoryFeatureLayer)overlayPrincipal.Layers["layerPrincipal"];
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "SELECT shapeId, shapePunctId, shapeOrdine, layerId, unitateId, shapePunctX, shapePunctY, shapePunctZ FROM hartaShapes where unitateId='" + Session["SESunitateId"].ToString() + "' ORDER BY shapeId, shapeOrdine";
        SqlDataReader vTabel = vCmd.ExecuteReader();
        List<ListaSelect> vListaSelect = new List<ListaSelect>();
        while (vTabel.Read())
        {
            ListaSelect vElementLista = new ListaSelect()
            {
                Col1 = vTabel["shapeId"].ToString(),
                Col2 = vTabel["shapePunctId"].ToString(),
                Col3 = vTabel["shapeOrdine"].ToString(),
                Col4 = vTabel["layerId"].ToString(),
                Col5 = Convert.ToDecimal(vTabel["shapePunctX"].ToString()).ToString().Replace(".", ","),
                Col6 = Convert.ToDecimal(vTabel["shapePunctY"].ToString()).ToString().Replace(".", ","),
                Col7 = Convert.ToDecimal(vTabel["shapePunctZ"].ToString()).ToString().Replace(".", ",")
            };
            vListaSelect.Add(vElementLista);
        }
        vTabel.Close();
        List<Vertex> vListaPuncte = new List<Vertex>();
        for (int a = 0; a < vListaSelect.Count; a++)
        {
            if (a + 1 < vListaSelect.Count)
            {
                if (vListaSelect[a + 1].Col1 != vListaSelect[a].Col1)
                {
                    Vertex vVertex = new Vertex(Convert.ToDouble(vListaSelect[a].Col6), Convert.ToDouble(vListaSelect[a].Col5));
                    vListaPuncte.Add(vVertex);
                    // adaug pe harta
                    layerPrincipal.InternalFeatures.Add(vListaSelect[a].Col1, new Feature(new LineShape(vListaPuncte)));
                    // golire lista
                    vListaPuncte.Clear();
                }
                else
                {
                    // adaug punct
                    Vertex vVertex = new Vertex(Convert.ToDouble(vListaSelect[a].Col6), Convert.ToDouble(vListaSelect[a].Col5));
                    vListaPuncte.Add(vVertex);
                }
            }
            else
            {
                Vertex vVertex = new Vertex(Convert.ToDouble(vListaSelect[a].Col6), Convert.ToDouble(vListaSelect[a].Col5));
                vListaPuncte.Add(vVertex);
                layerPrincipal.InternalFeatures.Add(vListaSelect[a].Col1, new Feature(new LineShape(vListaPuncte)));
                vListaPuncte.Clear();
            }
        }
        overlayPrincipal.Redraw();
        ManipuleazaBD.InchideConexiune(vCon);
    }
    private void scrieTexte()
    {
        LayerOverlay overlayTexte = (LayerOverlay)harta.CustomOverlays["overlayTexte"];
        InMemoryFeatureLayer layerTexte = (InMemoryFeatureLayer)overlayTexte.Layers["layerTexte"];
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "SELECT textId, unitateId, layerId, textFont, textFlags, textHeight, textAngle, textDenumire, textPunctX, textPunctX1, textPunctY, textPunctY1, textPunctZ FROM hartaTexte";
        SqlDataReader vTabel = vCmd.ExecuteReader();
        while (vTabel.Read())
        {
            PointShape vPunct = new PointShape(Convert.ToDouble(vTabel["textPunctY"].ToString().Replace(".", ",")), Convert.ToDouble(vTabel["textPunctX"].ToString().Replace(".", ",")));
            Feature vFeature = new Feature(vPunct);
            vFeature.ColumnValues.Add("Nume", vTabel["textDenumire"].ToString());
            layerTexte.InternalFeatures.Add(vTabel["textId"].ToString(), vFeature);
        }
        vTabel.Close();
        ManipuleazaBD.InchideConexiune(vCon);
    }
    protected void buttonSubmit_Click(object sender, ImageClickEventArgs e)
    {
        /*ViewState["postback"] = 0;
        LayerOverlay overlayPrincipal = (LayerOverlay)harta.CustomOverlays["overlayPrincipal"];
        InMemoryFeatureLayer layerPrincipal = (InMemoryFeatureLayer)overlayPrincipal.Layers["layerPrincipal"];

        foreach (Feature feature in harta.EditOverlay.Features)
        {
            if (!layerPrincipal.InternalFeatures.Contains(feature.Id))
            {
                layerPrincipal.InternalFeatures.Add(feature.Id, feature);
            }
        }
        harta.EditOverlay.Features.Clear();
        harta.EditOverlay.TrackMode = TrackMode.None;
        overlayPrincipal.Redraw();*/
    }
    protected void buttonEditShape_Click(object sender, ImageClickEventArgs e)
    {
        /*ViewState["postback"] = 0;
        LayerOverlay overlayPrincipal = (LayerOverlay)harta.CustomOverlays["overlayPrincipal"];
        InMemoryFeatureLayer layerPrincipal = (InMemoryFeatureLayer)overlayPrincipal.Layers["layerPrincipal"];
        LayerOverlay overlaySelectie = (LayerOverlay)harta.CustomOverlays["overlaySelectie"];
        InMemoryFeatureLayer layerSelectie = (InMemoryFeatureLayer)overlaySelectie.Layers["layerSelectie"];
        foreach (Feature feature in layerPrincipal.InternalFeatures)
        {
            harta.EditOverlay.Features.Add(feature.Id, feature);
        }
        foreach (Feature feature in layerSelectie.InternalFeatures)
        {
            harta.EditOverlay.Features.Add(feature.Id, feature);
        }
        layerPrincipal.InternalFeatures.Clear();
        layerSelectie.InternalFeatures.Clear();
        harta.EditOverlay.TrackMode = TrackMode.Edit;
        overlayPrincipal.Redraw();
        overlaySelectie.Redraw();*/
    }
    protected void harta_Click(object sender, MapClickedEventArgs e)
    {
        List<LineShape> vListaFeatures = new List<LineShape>();
        if (ViewState["listaLinii"] != null)
            vListaFeatures = (List<LineShape>)ViewState["listaLinii"];
        LayerOverlay overlayPrincipal = (LayerOverlay)harta.CustomOverlays["overlayPrincipal"];
        InMemoryFeatureLayer layerPrincipal = (InMemoryFeatureLayer)overlayPrincipal.Layers["layerPrincipal"];
        layerPrincipal.Open();
        LayerOverlay overlaySelectie = (LayerOverlay)harta.CustomOverlays["overlaySelectie"];
        InMemoryFeatureLayer layerSelectie = (InMemoryFeatureLayer)overlaySelectie.Layers["layerSelectie"];
        layerSelectie.Open();
        // pozitie click
        PointShape clickWorldPosition = e.Position;
        // convertexc pozitia in pozitie pe ecran
        ScreenPointF clickScreenPoint = harta.ToScreenCoordinate(clickWorldPosition);
        // 5 is the radius to buffer the click point.
        ScreenPointF clickNextScreenPoint = new ScreenPointF(clickScreenPoint.X + 5, clickScreenPoint.Y);
        // calculate the distance is world coordinate of the radius.
        double worldDistance = ExtentHelper.GetWorldDistanceBetweenTwoScreenPoints(harta.CurrentExtent, clickScreenPoint, clickNextScreenPoint, (float)harta.WidthInPixels, (float)harta.HeightInPixels, GeographyUnit.Meter, DistanceUnit.Meter);
        // calculate the buffer which you need.
        MultipolygonShape clickArea = clickWorldPosition.Buffer(worldDistance, GeographyUnit.Meter, DistanceUnit.Meter);
        Collection<Feature> features = layerPrincipal.QueryTools.GetFeaturesIntersecting(clickArea, ReturningColumnsType.AllColumns);
        // daca am gasit o linie o adaug in lista linii
        if (features.Count > 0)
        {
            // daca este de tip linie o adaug in lista
            if (features[0].GetShape().GetType().Name == "LineShape")
            {
                if (!layerSelectie.InternalFeatures.Contains(features[0].Id))
                {
                    layerPrincipal.InternalFeatures.Remove(features[0].Id);
                    layerSelectie.InternalFeatures.Add(features[0].Id, features[0]);
                    vListaFeatures.Add((LineShape)features[0].GetShape());
                }
                else
                {
                    layerSelectie.InternalFeatures.Remove(features[0].Id);
                    layerPrincipal.InternalFeatures.Add(features[0].Id, features[0]);
                    for (int a = 0; a < vListaFeatures.Count; a++)
                    {
                        if (vListaFeatures[a].Id == features[0].Id)
                        {
                            vListaFeatures.RemoveAt(a);
                            break;
                        }
                    }
                }
                ViewState["listaLinii"] = vListaFeatures;
            }
        }
        overlayPrincipal.Redraw();
        overlaySelectie.Redraw();
    }
    protected void lbLeagaLinii_Click(object sender, EventArgs e)
    {
        ViewState["postback"] = 1;
        Page_Load(sender, e);
        // golesc linii din layerSelectie
        golireLiniiDinLayerSelectie();
    }
    private void golireLiniiDinLayerSelectie()
    {
        LayerOverlay overlayPrincipal = (LayerOverlay)harta.CustomOverlays["overlayPrincipal"];
        InMemoryFeatureLayer layerPrincipal = (InMemoryFeatureLayer)overlayPrincipal.Layers["layerPrincipal"];
        layerPrincipal.Open();
        LayerOverlay overlaySelectie = (LayerOverlay)harta.CustomOverlays["overlaySelectie"];
        InMemoryFeatureLayer layerSelectie = (InMemoryFeatureLayer)overlaySelectie.Layers["layerSelectie"];
        layerSelectie.Open();
        List<Feature> vListaFeature = new List<Feature>();
        foreach (Feature vFeature in layerSelectie.InternalFeatures)
            vListaFeature.Add(vFeature);
        foreach (Feature vFeature in vListaFeature)
        {
            layerSelectie.InternalFeatures.Remove(vFeature.Id);
            layerPrincipal.InternalFeatures.Remove(vFeature.Id);
            //layerPrincipal.InternalFeatures.Add(vFeature.Id, vFeature);
        }
    }
    protected void lbTransformaInPoligon_Click(object sender, EventArgs e)
    {
        unireLiniiInPoligon();
    }
    private void unireLiniiInPoligon()
    {
        if (ViewState["listaLinii"] == null)
        {
            golireLiniiDinLayerSelectie();
            return;
        }
        LayerOverlay overlayParcele = (LayerOverlay)harta.CustomOverlays["overlayParcele"];
        InMemoryFeatureLayer layerParcele = (InMemoryFeatureLayer)overlayParcele.Layers["layerParcele"];
        layerParcele.Open();
        List<LineShape> vListaLinii = (List<LineShape>)ViewState["listaLinii"];
        PolygonShape vPoligon = new PolygonShape();
        Collection<Vertex> vListaVertex = new Collection<Vertex>();
        Vertex vVertexStart = vListaLinii[0].Vertices[0];
        vListaVertex.Add(vVertexStart);
        Collection<LineShape> vListaLiniiTemp = new Collection<LineShape>();
        foreach (LineShape vLinie in vListaLinii)
            vListaLiniiTemp.Add(vLinie);
        Vertex vUltimulPunct = new Vertex(0, 0);
        vListaVertex = adaugaVertex(vListaLiniiTemp, vListaLinii[0], vListaVertex, vUltimulPunct);

        foreach (Vertex vVertex in vListaVertex)
            vPoligon.OuterRing.Vertices.Add(vVertex);
        //try
        {
            if (vPoligon.OuterRing.Vertices[0] != vPoligon.OuterRing.Vertices[vPoligon.OuterRing.Vertices.Count - 1])
                vPoligon.OuterRing.Vertices.Add(vPoligon.OuterRing.Vertices[0]);
            // sterg parcela cu id-ul curent de pe harta
            if (layerParcele.InternalFeatures.Count > 0)
                if (layerParcele.InternalFeatures.Contains(gvParcele.SelectedValue.ToString()))
                    layerParcele.InternalFeatures.Remove(gvParcele.SelectedValue.ToString());
            
            layerParcele.InternalFeatures.Add(gvParcele.SelectedValue.ToString(), new Feature(vPoligon));
            // adaug punctele intr-o lista si in viewstate
            List<Vertex> vListaPunctePoligon = new List<Vertex>();
            foreach (Vertex vVertex in vPoligon.OuterRing.Vertices)
                vListaPunctePoligon.Add(vVertex);
            ViewState["listaPoligon"] = vListaPunctePoligon;
            ViewState["listaLinii"] = null;
        }
        //catch { }
        overlayParcele.Redraw();
    }
    private void cautaVertexLinie(ref Collection<Vertex> vListaVertex, LineShape vLinie, ref Vertex vUltimulPunct)
    {
        Collection<Vertex> vPuncteTemp = new Collection<Vertex>();
        if (vUltimulPunct.X != 0 && vUltimulPunct.Y != 0)
            vListaVertex.Add(vUltimulPunct);
        else
        {
            vUltimulPunct = vLinie.Vertices[0];
            vListaVertex.Add(vUltimulPunct);
        }
        PointShape vPunct = new PointShape(vUltimulPunct);
        for (int a = 0; a < vLinie.Vertices.Count; a++)
            vPuncteTemp.Add(vLinie.Vertices[a]);
        vPuncteTemp.Remove(vUltimulPunct);
        vLinie = new LineShape(vPuncteTemp);
        if (vLinie.Vertices.Count > 1)
        {
            PointShape vPunctTemp = vPunct;
            vPunct = vLinie.GetClosestPointTo(vPunct, GeographyUnit.Meter);
            Vertex vVertexVerificare = new Vertex(vPunct.X, vPunct.Y);
            if (vLinie.Vertices.Contains(vVertexVerificare))
                vUltimulPunct = new Vertex(vPunct.X, vPunct.Y);
            else vUltimulPunct = vLinie.Vertices[0];
        }
        else vUltimulPunct = vLinie.Vertices[0];
        if (vLinie.Vertices.Count > 1)
            cautaVertexLinie(ref vListaVertex, vLinie, ref vUltimulPunct);
        else
            if (vUltimulPunct.X != 0 && vUltimulPunct.Y != 0)
                vListaVertex.Add(vUltimulPunct);
        
    }
    private Collection<Vertex> adaugaVertex(Collection<LineShape> vListaLiniiTemp, LineShape vLinie, Collection<Vertex> vListaVertex, Vertex vUltimulPunct)
    {
        Collection<Vertex> vListaVertexLinie = new Collection<Vertex>();
        if (vUltimulPunct.X == 0 && vUltimulPunct.Y == 0)
            vUltimulPunct = vListaLiniiTemp[0].Vertices[0];
        cautaVertexLinie(ref vListaVertexLinie, vLinie, ref vUltimulPunct);

        for (int a = 0; a < vListaVertexLinie.Count; a++)
        {
            if (!vListaVertex.Contains(vListaVertexLinie[a]))
            {
                vListaVertex.Add(vListaVertexLinie[a]);
            }
        }
        vListaLiniiTemp.Remove(vLinie);
        // caut urmatoarea linie
        LineShape vLinieUrmatoare = new LineShape();
        foreach (LineShape vLinieRamas in vListaLiniiTemp)
        {
            foreach (Vertex vVertexRamas in vLinieRamas.Vertices)
            {
                if (vVertexRamas == vUltimulPunct)
                {
                    vLinieUrmatoare = vLinieRamas;
                    break;
                }
            }
        }
        if (vLinieUrmatoare.Vertices.Count > 0)
        {
            vListaVertex = adaugaVertex(vListaLiniiTemp, vLinieUrmatoare, vListaVertex, vUltimulPunct);
        }
        return vListaVertex;
    }
    protected void gvGospodarii_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(gvGospodarii, e, this);
    }
    protected void gvGospodarii_SelectedIndexChanged(object sender, EventArgs e)
    {
        gvParcele.DataBind();
    }
    protected void gvParcele_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(gvParcele, e, this);
    }
    private string verificareSalvare()
    {
        string vError = "";
        if (gvGospodarii.SelectedIndex == -1)
            vError += "Alegeţi o gospodărie ! ";
        if (gvParcele.SelectedIndex == -1)
            vError += "Alegeţi o parcelă ! ";
        return vError;
    }
    protected void lbSalvareParcela_Click(object sender, EventArgs e)
    {
        if (verificareSalvare() != "")
        {
            valEroare.IsValid = false;
            valEroare.ErrorMessage = verificareSalvare();
            return;
        }
        if (ViewState["listaLinii"] != null)
            unireLiniiInPoligon();
        else
        {
            valEroare.IsValid = false;
            valEroare.ErrorMessage = "Uniţi liniile pentru a forma un poligon ! ";
            return;
        }
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlTransaction vTranz = ManipuleazaBD.DeschideTranzactie(vCon);
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.Transaction = vTranz;
        try
        {
            string vGospodarieId = gvGospodarii.SelectedValue.ToString(), vUnitateId = Session["SESunitateId"].ToString(), vAn = Session["SESAn"].ToString(), vParcelaId = gvParcele.SelectedValue.ToString();
            vCmd.CommandText = "select top(1) volum from gospodarii where gospodarieId='" + vGospodarieId + "'";
            string vVolum = vCmd.ExecuteScalar().ToString();
            vCmd.CommandText = "select top(1) nrPozitie from gospodarii where gospodarieId='" + vGospodarieId + "'";
            string vPozitie = vCmd.ExecuteScalar().ToString();
            // sterg coordonatele deja existente
            vCmd.CommandText = "delete from coordonate where parcelaId='" + gvParcele.SelectedValue.ToString() + "'";
            vCmd.ExecuteNonQuery();
            // adaug noile coordonate
            List<Vertex> vListapoligon = (List<Vertex>)ViewState["listaPoligon"];
            string vInterogare = "";
            for (int a = 0; a < vListapoligon.Count; a++)
            {
                vInterogare += "INSERT INTO coordonate (coordonataX, coordonataY, coordonataZ, unitateId, parcelaId, an, volum, nrPozitie, gospodarieId, coordonataOrdine) VALUES ('" + vListapoligon[a].X.ToString().Replace(",", ".") + "', '" + vListapoligon[a].Y.ToString().Replace(",", ".") + "', 0, '" + vUnitateId + "', '" + vParcelaId + "', '" + vAn + "', '" + vVolum + "', '" + vPozitie + "', '" + vGospodarieId + "', '" + a.ToString() + "');";
            }
            if (vInterogare != "")
            {
                vCmd.CommandText = vInterogare;
                vCmd.ExecuteNonQuery();
            }
            vTranz.Commit();
            // golesc viewstate-ul cu poligon
            ViewState["listaPoligon"] = null;
            valEroare.IsValid = false;
            valEroare.ErrorMessage = "Parcela a fost salvată cu succes ! ";
        }
        catch { vTranz.Rollback(); }
        finally { ManipuleazaBD.InchideConexiune(vCon); }
    }
}
