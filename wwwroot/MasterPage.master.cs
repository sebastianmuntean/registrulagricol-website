﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Threading;

/// <summary>
/// Master page
/// Creata la:                  15.02.2011
/// Autor:                      Laza Tudor Mihai
/// Ultima                      actualizare: 01.03.2011
/// Autor:                      Laza Tudor Mihai
/// </summary> 
public partial class MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Init(object sender, EventArgs e)
    {
        if (tbTabIndex.Text == "")
            tbTabIndex.Text = "30";
        Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("ro-RO");
        HttpCookie vCookie = Request.Cookies["COOKTNT"];
        vCookie = CriptareCookie.DecodeCookie(vCookie);
        if (Session["SESunitateId"] == null || Session["SESutilizatorId"] == null || Session["SESan"] == null || Session["SESordonareGospodarii"] == null || Session["SESjudetId"] == null)
        {
            if (vCookie != null)
            {
                if (vCookie["COOKunitateId"] != null)
                {
                    Session["SESunitateId"] = vCookie["COOKunitateId"].ToString();
                }
                else { Response.Redirect("Login.aspx"); }
                if (vCookie["COOKjudetId"] != null)
                {
                    Session["SESjudetId"] = vCookie["COOKjudetId"].ToString();
                }
                else { Response.Redirect("Login.aspx"); }
                if (vCookie["COOKutilizatorId"] != null)
                {
                    Session["SESutilizatorId"] = vCookie["COOKutilizatorId"].ToString();
                }
                else { Response.Redirect("Login.aspx"); }
                if (vCookie["COOKan"] != null)
                {
                    Session["SESan"] = vCookie["COOKan"].ToString();
                }
                if (vCookie["COOKordonareGospodarii"] != null)
                    Session["SESordonareGospodarii"] = vCookie["COOKordonareGospodarii"].ToString();
                else Session["SESordonareGospodarii"] = "0";
            }
            else { Response.Redirect("Login.aspx"); }
        }
        if (vCookie != null)
        {
            if (vCookie["COOKgospodarieId"] != null)
            {
                Session["SESgospodarieId"] = vCookie["COOKgospodarieId"].ToString();
            }
            if (vCookie["COOKrandCurent"] != null)
                Session["SESrandCurent"] = vCookie["COOKrandCurent"].ToString();
            if (vCookie["COOKfVol"] != null)
                Session["SESfVol"] = vCookie["COOKfVol"].ToString();
            if (vCookie["COOKfNrPoz"] != null)
                Session["SESfNrPoz"] = vCookie["COOKfNrPoz"].ToString();
            if (vCookie["COOKfNume"] != null)
                Session["SESfNume"] = vCookie["COOKfNume"].ToString();
            if (vCookie["COOKfLoc"] != null)
                Session["SESfLoc"] = vCookie["COOKfLoc"].ToString();
            if (vCookie["COOKfStrada"] != null)
                Session["SESfStrada"] = vCookie["COOKfStrada"].ToString();
            if (vCookie["COOKfNr"] != null)
                Session["SESfNr"] = vCookie["COOKfNr"].ToString();
            if (vCookie["COOKfTip"] != null)
                Session["SESfTip"] = vCookie["COOKfTip"].ToString();
        }

        if (Session["SESparcelaId"] == null)
        {
            if (vCookie != null)
            {
                if (vCookie["COOKparcelaId"] != null)
                {
                    Session["SESparcelaId"] = vCookie["COOKparcelaId"].ToString();
                }
            }
        }
        // aratam asteptati
        if (Request.Url.Segments[Request.Url.Segments.Length - 1].IndexOf("Capitol2b.aspx") != -1)
            UpdateProgressTimer.Visible = true;
        else if (Request.Url.Segments[Request.Url.Segments.Length - 1].IndexOf("Capitol1.aspx") != -1)
            UpdateProgressTimer.Visible = true;
        else if (Request.Url.Segments[Request.Url.Segments.Length - 1].IndexOf("Capitol") != -1)
            UpdateProgressTimer.Visible = false;
        else
            UpdateProgressTimer.Visible = true;

        if (Convert.ToInt32(Session["SESan"].ToString()) > 2014)
        {
            lblMCapitolul2c.Visible = false;
            test.Visible = false;
        }
        else
        {
            lblMCapitolul2c.Visible = true;
            test.Visible = true;
        }
    }
    private Control FindControlRecursive(Control pRoot, Int32 pCitire, Int32 pStergere, Int32 pCreare, Int32 pModificare, Int32 pTiparire, Int32 pUnitati, Int32 pJudete)
    {
        // are drept de citire
        if (pRoot.GetType().ToString() == "System.Web.UI.UpdatePanel")
        {
            if (pRoot.ID.StartsWith("up"))
                if (pCitire == 0)
                    Response.Redirect("~/NuAvetiDrepturi.aspx");
        }
        // nu are drept de administrator
        if (pRoot.GetType().ToString() == "System.Web.UI.WebControls.Label" || pRoot.GetType().ToString() == "System.Web.UI.WebControls.DropDownList")
        {
            if (pRoot.ID == "lblUnitate" || pRoot.ID == "ddlUnitate")
                if (pUnitati == 0)
                    pRoot.Visible = false;
            if (pRoot.ID == "lblFJudet" || pRoot.ID == "ddlFJudet")
                if (pJudete == 0)
                    pRoot.Visible = false;
            if (pRoot.ID == "lblFDeLaUnitate" || pRoot.ID == "ddlFDeLaUnitate")
                if (pUnitati == 0)
                    pRoot.Visible = false;
            if (pRoot.ID == "lblFDeLaJudet" || pRoot.ID == "ddlFDeLaJudet")
                if (pJudete == 0)
                    pRoot.Visible = false;
            if (pRoot.ID == "lblFLaUnitate" || pRoot.ID == "ddlFLaUnitate")
                if (pUnitati == 0)
                    pRoot.Visible = false;
            if (pRoot.ID == "lblFLaJudet" || pRoot.ID == "ddlFLaJudet")
                if (pJudete == 0)
                    pRoot.Visible = false;

        }
        if (pRoot.GetType().ToString() == "System.Web.UI.WebControls.Button")
        {
            // are drept de creare
            if (pRoot.ID.StartsWith("btAdauga") || pRoot.ID.StartsWith("btListaAdauga"))
                if (pCreare == 0)
                    pRoot.Visible = false;
            // are drept de modificare
            if (pRoot.ID.StartsWith("btModifica") || pRoot.ID.StartsWith("btArataModifica"))
                if (pModificare == 0)
                    pRoot.Visible = false;
            // are drept de stergere
            if (pRoot.ID.StartsWith("btSterge"))
                if (pStergere == 0)
                    pRoot.Visible = false;
            // are drept de tiparire
            if (pRoot.ID.StartsWith("btTiparire") || pRoot.ID.StartsWith("btTipareste"))
                if (pTiparire == 0)
                    pRoot.Visible = false;
        }
        if (pRoot.HasControls())
        {
            foreach (Control vCtrl in pRoot.Controls)
            {
                Control vFoundCtrl = FindControlRecursive(vCtrl, pCitire, pStergere, pCreare, pModificare, pTiparire, pUnitati, pJudete);
                if (vFoundCtrl != null)
                    return vFoundCtrl;
            }
        }
        return null;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!IsPostBack)
        //{
        //    if (!Request.IsSecureConnection)
        //    {
        //        string redirectUrl = Request.Url.ToString().Replace("http:", "https:");
        //        Response.Redirect(redirectUrl, false);
        //        HttpContext.Current.ApplicationInstance.CompleteRequest();
        //    }
        //}

        SchimbaGospodaria();
        //VerificareSesiuni vVerificareSesiuni = new VerificareSesiuni();
        //vVerificareSesiuni.VerificaSesiuniCookie();
        HttpCookie vCookie = Request.Cookies["COOKTNT"];
        HttpCookie vCookieExpira = Request.Cookies["COOKTNTExpira"];
        vCookie = CriptareCookie.DecodeCookie(vCookie);
        if (vCookie == null || vCookieExpira == null)
        {
            Response.Redirect("Login.aspx");
        }
        else
        {
            if (Convert.ToDateTime(Request.Cookies["COOKTNTExpira"]["COOKexpira"]) >= DateTime.Now.AddMinutes(13) && Request.Url.Segments[Request.Url.Segments.Length - 1].IndexOf("adev") != -1)
            { }
            else
                if (Convert.ToDateTime(Request.Cookies["COOKTNTExpira"]["COOKexpira"]) > DateTime.Now || (Request.Cookies["COOKTNTExpira"]["COOKexpira"] == null && vCookie["COOKunitateId"] != null))
                {
                    int vDurataSesiuneLucru = Convert.ToInt32(ManipuleazaBD.fRezultaUnString("select top(1) coalesce(utilizatorDurataSesiune,30) as utilizatorDurataSesiune from utilizatori where utilizatorId='" + Session["SESutilizatorId"].ToString() + "'", "utilizatorDurataSesiune", Convert.ToInt16(DateTime.Now.Year)));
                    Response.Cookies["COOKTNTExpira"]["COOKexpira"] = DateTime.Now.AddMinutes(vDurataSesiuneLucru).ToString();
                    //  lblCookie.Text = Request.Cookies["COOKTNTExpira"]["COOKexpira"];
                }
                else
                    if (Request.Url.Segments[Request.Url.Segments.Length - 1].IndexOf("adev") != -1)
                    { Response.Cookies["COOKTNTExpira"]["COOKexpira"] = DateTime.Now.AddMinutes(130).ToString(); }
                    else
                    {
                        Response.Redirect("Login.aspx");
                    }
            // daca sunt pe pagina cu harta ascund detaliile din stanga
            if (Request.Url.Segments[Request.Url.Segments.Length - 1].StartsWith("harta"))
                upGospodarie.Visible = false;
            else upGospodarie.Visible = true;

        }
        if (Session["SESmesaj"] != null)
            if (Session["SESmesaj"].ToString() != "")
            {
                //lblMesaj.Text = "</br>" + Session["mesaj"].ToString();
                if (!IsPostBack)
                {
                    mesaj.InnerText = Session["SESmesaj"].ToString();
                    pnIntrebare.Visible = true;
                }
            }

        if (Convert.ToInt32(Session["SESan"].ToString()) > 2014)
        {
            lblMCapitolul2c.Visible = false;
            test.Visible = false;
        }
        else
        {
            lblMCapitolul2c.Visible = true;
            test.Visible = true;
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        // verific ce drepturi are pe pagina curenta
        string vUrl = Request.Url.Segments[Request.Url.Segments.Length - 1];
        if (vUrl.StartsWith("Capitol"))
            vUrl = "Capitol";
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select tipUtilizatorId from utilizatori where utilizatorId='" + Session["SESutilizatorId"].ToString() + "'";
        int vUnitatiVizibil = Convert.ToInt32(vCmd.ExecuteScalar());
        vCmd.CommandText = "SELECT drepturiUtilizatori.dreptCreare, drepturiUtilizatori.dreptCitire, drepturiUtilizatori.dreptModificare, drepturiUtilizatori.dreptStergere, drepturiUtilizatori.dreptTiparire FROM drepturiUtilizatori INNER JOIN pagini ON drepturiUtilizatori.paginaId = pagini.paginaId WHERE (drepturiUtilizatori.utilizatorId = '" + Session["SESutilizatorId"] + "') AND (pagini.paginaUrl = '" + vUrl + "')";
        SqlDataReader vTabel = vCmd.ExecuteReader();
        int vCitire = 1, vStergere = 1, vCreare = 1, vModificare = 1, vTiparire = 1, vUnitati = 0, vJudete = 0;
        if (vUnitatiVizibil == 1 || vUnitatiVizibil == 2)
            vUnitati = 1;

        if (vTabel.Read())
        {
            vCitire = Convert.ToInt32(vTabel["dreptCitire"].ToString());
            vStergere = Convert.ToInt32(vTabel["dreptStergere"].ToString());
            vCreare = Convert.ToInt32(vTabel["dreptCreare"].ToString());
            vModificare = Convert.ToInt32(vTabel["dreptModificare"].ToString());
            vTiparire = Convert.ToInt32(vTabel["dreptTiparire"].ToString());
        }
        // daca e pe un an inchis nu afisam butoane de sterge creeaza si modifica
        if (ManipuleazaBD.fRezultaUnString("SELECT unitateAnDeschis FROM unitati WHERE unitateID = '" + Session["SESunitateID"].ToString() + "'", "unitateAnDeschis", Convert.ToInt16(DateTime.Now.Year)) != Session["SESan"].ToString())
        {
            vStergere = 0;
            vCreare = 0;
            vModificare = 0;
        }
        vTabel.Close();
        vCmd.CommandText = "select top(1) tipUtilizatorId from utilizatori where utilizatorId='" + Session["SESutilizatorId"].ToString() + "'";
        int vTipUtilizator = Convert.ToInt32(vCmd.ExecuteScalar());
        if (vTipUtilizator == 1 || vTipUtilizator == 4)
            vJudete = 1;
        ManipuleazaBD.InchideConexiune(vCon);
        foreach (Control c in ContentPlaceHolder1.Controls)
        {
            FindControlRecursive(c, vCitire, vStergere, vCreare, vModificare, vTiparire, vUnitati, vJudete);
        }

    }

    protected int NrGospodarii(int pUnitateId)
    {
        int vNrGospodarii = Convert.ToInt32(ManipuleazaBD.fRezultaUnStringScalar("SELECT COUNT(*) AS numar FROM gospodarii INNER JOIN unitati ON gospodarii.unitateId = unitati.unitateId WHERE convert(nvarchar,gospodarii.unitateId) like '" + Convert.ToString((pUnitateId == 0) ? "%" : pUnitateId.ToString()) + "' and convert(nvarchar,judetId) like '" + Session["SESjudetId"].ToString() + "' AND an='" + Session["SESan"].ToString() + "'", "numar", Convert.ToInt16(Session["SESan"])));
        return vNrGospodarii;
    }
    protected int NrUnitati()
    {
        int vNrUnitati = Convert.ToInt32(ManipuleazaBD.fRezultaUnStringScalar("select count(*) as countUnitati from unitati where convert(nvarchar,judetId) like '" + Session["SESjudetId"].ToString() + "'", "countUnitati", Convert.ToInt16(Session["SESan"])));
        return vNrUnitati;
    }
    public void SchimbaGospodaria()
    {


        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        string vUnitateDenumire = ManipuleazaBD.fRezultaUnStringScalar("SELECT unitateDenumire FROM unitati WHERE unitateId = '" + Session["SESunitateId"].ToString() + "'", "denumireUnitate", Convert.ToInt16(Session["SESan"]));
        if (Session["SESunitateId"].ToString() == "0")
        {
            lblUnitate.Text = Resources.Resursa.raTOATE + " | " + Resources.Resursa.raUnitati + ": " + NrUnitati().ToString() + " " + Resources.Resursa.raCu + " " + NrGospodarii(Convert.ToInt32(Session["SESunitateId"])).ToString() + " " + Resources.Resursa.raGospodarii;
        }
        else
            lblUnitate.Text = vUnitateDenumire.ToString() + " | " + NrGospodarii(Convert.ToInt32(Session["SESunitateId"])).ToString() + " " + Resources.Resursa.raGospodarii;
        // afisam anul ales, inchis sau deschis
        lkbAnulAles.Text = Session["SESan"].ToString();
        if (Session["SESan"].ToString() != ManipuleazaBD.fRezultaUnString("SELECT unitateAnDeschis FROM unitati WHERE unitateID = '" + Session["SESunitateID"].ToString() + "'", "unitateAnDeschis", Convert.ToInt16(DateTime.Now.Year)))
            lkbAnulAles.Text = Session["SESan"].ToString() + " - An închis!";
        else lkbAnulAles.Text = Session["SESan"].ToString();


        if (Session["SESgospodarieId"] != null && Session["SESgospodarieId"] != "NULA" && Session["SESgospodarieId"] != "")
        {
            lblAlegeti.Visible = false;
            lblGospodarie.Text = "";
            if (ManipuleazaBD.fVerificaExistentaExtins("gospodarii", "persJuridica", "True", " AND gospodarieId ='" + Session["SESgospodarieId"].ToString() + "'", Convert.ToInt16(Session["SESan"])) == 1)
            {
                string[] vCampuriRezultate = { "volum", "nrPozitie", "jUnitate" };
                int vContor = 0;
                foreach (string vElement in ManipuleazaBD.fRezultaUnString("SELECT TOP(1) volum, nrPozitie, jUnitate, gospodarieIdInitial FROM gospodarii WHERE unitateId = '" + Session["SESunitateId"].ToString() + "' AND gospodarieId='" + Session["SESgospodarieId"].ToString() + "'", vCampuriRezultate, Convert.ToInt16(Session["SESan"])))
                {
                    switch (vContor)
                    {
                        case 0:
                            lblGospodarie.Text += Resources.Resursa.raVolum + " " + vElement.ToString();
                            Session["SESvolum"] = vElement.ToString();
                            break;
                        case 1:
                            lblGospodarie.Text += " / " + Resources.Resursa.raNrPozitie + " " + vElement.ToString();
                            Session["SESnrPozitie"] = vElement.ToString();
                            break;
                        case 2:
                            lblGospodarie.Text += " / " + vElement.ToString();
                            break;
                        case 3:
                            Session["SESgospodarieIdInitial"] = vElement.ToString();
                            break;

                    }
                    vContor++;
                }
            }
            else
            {
                string[] vCampuriRezultate = { "volum", "nrPozitie", "nume" };
                int vContor = 0;
                foreach (string vElement in ManipuleazaBD.fRezultaUnString("SELECT TOP(1) volum, nrPozitie, nume, gospodarieIdInitial  FROM gospodarii INNER JOIN membri ON gospodarii.gospodarieId = membri.gospodarieId WHERE gospodarii.unitateId = '" + Session["SESunitateId"].ToString() + "' AND gospodarii.gospodarieId='" + Session["SESgospodarieId"].ToString() + "' AND membri.codRudenie='1'", vCampuriRezultate, Convert.ToInt16(Session["SESan"])))
                {
                    switch (vContor)
                    {
                        case 0:
                            lblGospodarie.Text += Resources.Resursa.raVolum + " " + vElement.ToString();
                            Session["SESvolum"] = vElement.ToString();
                            break;
                        case 1:
                            lblGospodarie.Text += " / " + Resources.Resursa.raNrPozitie + " " + vElement.ToString();
                            Session["SESnrPozitie"] = vElement.ToString();
                            break;
                        case 2:
                            lblGospodarie.Text += " / " + vElement.ToString();
                            break;
                        case 3:
                            Session["SESgospodarieIdInitial"] = vElement.ToString();
                            break;
                    }
                    vContor++;
                }

            }
            int vNumar = clsCorelatii.NumarCorelatiiInvalidate(Session["SESgospodarieId"].ToString(), Session["SESUnitateId"].ToString(), Session["SESan"].ToString());
            if (vNumar > 0)
            {
                lblMesajeInvalidare.Visible = true;
                lblMesajeInvalidare.Text = Resources.Resursa.raEroareCorelatii1 + " " + vNumar.ToString() + " " + Resources.Resursa.raEroareCorelatii2;

            }
            else { lblMesajeInvalidare.Visible = false; }
            //lblGospodarie.Text += " / " + Session["SESgospodarieId"].ToString() + " / anul " + Session["SESan"].ToString() ; 
        }
        else
        {
            lblGospodarie.Text = "<span class=\"validator\"> " + Resources.Resursa.raSelectatiOGospodarie + "! </span>";
        }

    }
    public void VerificaCorelatii()
    {
        int vNumar = clsCorelatii.NumarCorelatiiInvalidate(Session["SESgospodarieId"].ToString(), Session["SESUnitateId"].ToString(), Session["SESan"].ToString());
        if (vNumar > 0)
        {
            lblMesajeInvalidare.Visible = true;
            lblMesajeInvalidare.Text = "Aveţi " + vNumar.ToString() + " corelaţii invalide!";
        }
        else { lblMesajeInvalidare.Visible = false; }
    }
    protected void lbSituatieLaZi_Click(object sender, EventArgs e)
    {
        if (Session["SESgospodarieId"] == null)
            return;
        ResponseHelper.Redirect("~/printSituatieLaZi.aspx", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
    }
    protected void lbToateCapitolele_Click(object sender, EventArgs e)
    {
        if (Session["SESgospodarieId"] == null)
            return;
        ResponseHelper.Redirect("~/printCapitoleToate.aspx", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
    }
    protected void lbToateCapitoleleRa_Click(object sender, EventArgs e)
    {
        if (Session["SESgospodarieId"] == null)
            return;
        ResponseHelper.Redirect("~/printCapitoleToateCompact.aspx", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
    }


    protected void lbToateCapitoleleXls_Click(object sender, EventArgs e)
    {
        if (Session["SESgospodarieId"] == null)
            return;
        ResponseHelper.Redirect("~/printCapitoleToateXls.aspx", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
    }
    protected void lbToateCapitoleleRaXls_Click(object sender, EventArgs e)
    {
        if (Session["SESgospodarieId"] == null)
            return;
        ResponseHelper.Redirect("~/printCapitoleToateCompactXls.aspx", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
    }


    protected void imgHelp_PreRender(object sender, EventArgs e)
    {
        string vUrl = Request.Url.Segments[Request.Url.Segments.Length - 1];
        if (vUrl.IndexOf("Capitol") != -1)
            vUrl = "Capitol";
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "SELECT top(1) paginaId FROM pagini WHERE (pagini.paginaUrl = '" + vUrl + "')";
        Int32 vPaginaId = Convert.ToInt32(vCmd.ExecuteScalar());
        imgHelp.NavigateUrl = "~/help.aspx?pagina=" + vPaginaId.ToString();
        ManipuleazaBD.InchideConexiune(vCon);
    }
    protected void imgMesaje_PreRender(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select count(*) from mesaje where utilizatorId='" + Session["SESutilizatorId"].ToString() + "' and dataCitirii is null";
        if (Convert.ToInt32(vCmd.ExecuteScalar()) == 0)
            imgMesaje.Visible = false;
        else imgMesaje.Visible = true;
        ManipuleazaBD.InchideConexiune(vCon);
    }
    protected void lkbAnulAles_Click(object sender, EventArgs e)
    {
        // aratam popupul de unde schimba anul
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        try
        {
            string vGospodarie = Session["SESgospodarieId"].ToString();
            popup.Visible = true;
            lblAlegeti.Visible = false;
        }
        catch
        {
            lblAlegeti.Visible = true;
            popup.Visible = false;
        }
    }
    protected void ddlAni_Init(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        // formam dropdownul cu ani
        ddlAni.Items.Clear();
        string[] vDelimitator = { "#" };
        ListItem vListItem = new ListItem();
        vListItem.Value = ManipuleazaBD.fRezultaUnString("SELECT unitateAnDeschis FROM unitati WHERE unitateID = '" + Session["SESunitateID"].ToString() + "'", "unitateAnDeschis", Convert.ToInt16(DateTime.Now.Year));
        vListItem.Text = ManipuleazaBD.fRezultaUnString("SELECT unitateAnDeschis FROM unitati WHERE unitateID = '" + Session["SESunitateID"].ToString() + "'", "unitateAnDeschis", Convert.ToInt16(DateTime.Now.Year));
        ddlAni.Items.Add(vListItem);
        foreach (string vAn in ManipuleazaBD.fRezultaUnString("SELECT unitateAniInchisi FROM unitati WHERE unitateID = '" + Session["SESunitateID"].ToString() + "'", "unitateAniInchisi", Convert.ToInt16(DateTime.Now.Year)).Split(vDelimitator, System.StringSplitOptions.RemoveEmptyEntries))
        {
            vListItem = new ListItem();
            vListItem.Value = vAn;
            vListItem.Text = vAn;
            ddlAni.Items.Add(vListItem);
        }

    //    Session["SESan"] = ManipuleazaBD.fRezultaUnString("SELECT unitateAnDeschis FROM unitati WHERE unitateID = '" + Session["SESunitateID"].ToString() + "'", "unitateAnDeschis", Convert.ToInt16(DateTime.Now.Year));
        ddlAni.SelectedValue = Session["SESan"].ToString();
    }
    protected void lkbAnRenunta_Click(object sender, EventArgs e)
    {
        // inchidem popupul
        popup.Visible = false;
        Session["SESan"] = ddlAni.SelectedValue;
        SchimbaGospodaria();
    }
    protected void ddlAni_SelectedIndexChanged(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        Session["SESan"] = ddlAni.SelectedValue;
        string vNouaGospodarie = ManipuleazaBD.fRezultaUnString("SELECT gospodarieId FROM gospodarii WHERE gospodarieIdInitial = '" + ManipuleazaBD.fRezultaUnString("SELECT gospodarieIdInitial FROM gospodarii WHERE gospodarieId = '" + Session["SESgospodarieId"].ToString() + "'", "gospodarieIdInitial", Convert.ToInt16(Session["SESan"])) + "' AND an='" + ddlAni.SelectedValue + "'", "gospodarieId", Convert.ToInt16(Session["SESan"]));
        int vDurataSesiuneLucru = Convert.ToInt32(ManipuleazaBD.fRezultaUnString("select top(1) coalesce(utilizatorDurataSesiune,30) as utilizatorDurataSesiune from utilizatori where utilizatorId='" + Session["SESutilizatorId"].ToString() + "'", "utilizatorDurataSesiune", Convert.ToInt16(Session["SESan"])));
        Session["SESgospodarieId"] = vNouaGospodarie;
        HttpCookie vCook = HttpContext.Current.Request.Cookies["COOKTNT"];
        vCook = CriptareCookie.DecodeCookie(vCook);
        vCook["COOKan"] = ddlAni.SelectedValue;
        vCook["COOKgospodarieId"] = vNouaGospodarie;
        vCook["COOKexpira"] = DateTime.Now.AddMinutes(vDurataSesiuneLucru).ToString();
        vCook = CriptareCookie.EncodeCookie(vCook);
        vCook.Expires = DateTime.Now.AddMinutes(2005);
        Response.Cookies.Add(vCook);
        popup.Visible = false;
        SchimbaGospodaria();

        if (Convert.ToInt32(Session["SESan"].ToString()) > 2014)
        {
            lblMCapitolul2c.Visible = false;
            test.Visible = false;
        }
        else
        {
            lblMCapitolul2c.Visible = true;
            test.Visible = true;
        }
    }
    protected void btDa_Click(object sender, EventArgs e)
    {
        pnIntrebare.Visible = false;
    }
    protected void lbAtestateProducator_Click(object sender, EventArgs e)
    {
        ResponseHelper.Redirect("~/printRaportAtestateProducatorAnexa3.aspx", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
    }
}
