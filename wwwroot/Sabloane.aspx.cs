﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;

public partial class Sabloane : System.Web.UI.Page 
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "sabloane", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
        }
        // daca nu e unitate TNT nu poate accesa pagina
        if (Redirect.RedirectUtilizator(Session["SESutilizatorId"].ToString(), Convert.ToInt16(HttpContext.Current.Session["SESan"])) == 1)
        {
            Response.Redirect("NuAvetiDrepturi.aspx");
        }
    }

    protected void btSabloane_Salveaza(object sender, EventArgs e)
    {
        string vInterogareUpdate = "";
        string vInterogareInsert = "";
        string vInterogareSelect = "";

        for (int i = 0; i < gvSabloane.Rows.Count; i++)
        {
            vInterogareUpdate = ""; vInterogareInsert = ""; vInterogareSelect = "";
            if (gvSabloane.Rows[i].RowType == DataControlRowType.DataRow)
            {
                // verificam daca exista deja linia
                // citim tipulutilizator si pagina id                    
                CheckBox vCbCitire = (CheckBox)gvSabloane.Rows[i].FindControl("cbCitire");
                CheckBox vCbModificare = (CheckBox)gvSabloane.Rows[i].FindControl("cbModificare");
                CheckBox vCbTiparire = (CheckBox)gvSabloane.Rows[i].FindControl("cbTiparire");
                CheckBox vCbCreare = (CheckBox)gvSabloane.Rows[i].FindControl("cbCreare");
                CheckBox vCbStergere = (CheckBox)gvSabloane.Rows[i].FindControl("cbStergere");
                DropDownList vTipUtilizator = (DropDownList)TipUtilizator;
                Label vPaginaId = (Label)gvSabloane.Rows[i].FindControl("paginaId");
                vInterogareSelect = "SELECT COUNT(*) as exista FROM sabloaneDrepturi WHERE paginaId='" + vPaginaId.Text + "' AND tipUtilizatorId = '" + vTipUtilizator.Text + "'";
                
                if (clsSabloane.VerificaLinieSablon(vInterogareSelect) == 0)
                {
                    vInterogareInsert = "INSERT INTO sabloaneDrepturi (paginaId, dreptCitire, dreptModificare, dreptTiparire, dreptCreare, dreptStergere,tipUtilizatorId) VALUES ('";
                    vInterogareInsert += vPaginaId.Text.ToString() + "',";

                    if (vCbCitire.Checked == false) { vInterogareInsert += "'0',"; } else { vInterogareInsert += "'1',"; }
                    if (vCbModificare.Checked == false) { vInterogareInsert += "'0',"; } else { vInterogareInsert += "'1',"; }
                    if (vCbTiparire.Checked == false) { vInterogareInsert += "'0',"; } else { vInterogareInsert += "'1',"; }
                    if (vCbCreare.Checked == false) { vInterogareInsert += "'0',"; } else { vInterogareInsert += "'1',"; }
                    if (vCbStergere.Checked == false) { vInterogareInsert += "'0','"; } else { vInterogareInsert += "'1','"; }
                    vInterogareInsert += vTipUtilizator.SelectedItem.Value + "')";
                    ManipuleazaBD.fManipuleazaBD(vInterogareInsert, Convert.ToInt16(Session["SESan"]));
                }
                else
                {
                    vInterogareUpdate = "UPDATE sabloaneDrepturi SET ";
                    if (vCbCitire.Checked == false) { vInterogareUpdate += " dreptCitire = '0' "; } else { vInterogareUpdate += " dreptCitire = '1' "; }
                    if (vCbModificare.Checked == false) { vInterogareUpdate += ", dreptModificare = '0' "; } else { vInterogareUpdate += ", dreptModificare = '1' "; }
                    if (vCbTiparire.Checked == false) { vInterogareUpdate += ", dreptTiparire = '0' "; } else { vInterogareUpdate += ", dreptTiparire = '1' "; }
                    if (vCbCreare.Checked == false) { vInterogareUpdate += ", dreptCreare = '0' "; } else { vInterogareUpdate += ", dreptCreare = '1' "; }
                    if (vCbStergere.Checked == false) { vInterogareUpdate += ", dreptStergere = '0' "; } else { vInterogareUpdate += ", dreptStergere = '1' "; }
                    vInterogareUpdate += " WHERE tipUtilizatorId = '" + vTipUtilizator.SelectedItem.Value + "' AND paginaId = '" + vPaginaId.Text.ToString() + "' ";
                    ManipuleazaBD.fManipuleazaBD(vInterogareUpdate, Convert.ToInt16(Session["SESan"]));
                }
            }
            else { }
        } 

    }
    protected void TipUtilizator_DataBound(object sender, EventArgs e)
    {
        PopuleazaSablon();
    }
    protected void TipUtilizator_SelectedIndexChanged(object sender, EventArgs e)
    {
        PopuleazaSablon();
    }
    private void PopuleazaSablon()
    {
        string vInterogare = "";
        int j = gvSabloane.Rows.Count;
        for (int i = 0; i < gvSabloane.Rows.Count; i++)
        {
            if (gvSabloane.Rows[i].RowType == DataControlRowType.DataRow)
            {
                // veificam daca avem inregistrare pentru pagina respectiva
                // si pt tipul de utilizator ales
                Label vPaginaId = (Label)gvSabloane.Rows[i].FindControl("paginaId");
                DropDownList vTipUtilizator = (DropDownList)((DropDownList)TipUtilizator);
                vInterogare = "SELECT COUNT (*) as exista, tipUtilizatorId, paginaId, dreptCitire, dreptModificare, dreptTiparire, dreptCreare, dreptStergere FROM sabloaneDrepturi WHERE paginaId = '" + vPaginaId.Text + "' AND tipUtilizatorId='";
                try { vInterogare += vTipUtilizator.SelectedItem.Value + "' GROUP BY tipUtilizatorId, paginaId, dreptCitire, dreptModificare, dreptTiparire, dreptCreare, dreptStergere"; }
                catch { }

                string strConn = ConfigurationManager.ConnectionStrings["TNTRegistruAgricolConnectionString"].ConnectionString;
                SqlConnection con = new SqlConnection(strConn);
                SqlCommand cmd = new SqlCommand(); 
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = vInterogare;
                con.Open();
                try
                {
                    CheckBox vCbCitire = (CheckBox)gvSabloane.Rows[i].FindControl("cbCitire");
                    CheckBox vCbModificare = (CheckBox)gvSabloane.Rows[i].FindControl("cbModificare");
                    CheckBox vCbTiparire = (CheckBox)gvSabloane.Rows[i].FindControl("cbTiparire");
                    CheckBox vCbCreare = (CheckBox)gvSabloane.Rows[i].FindControl("cbCreare");
                    CheckBox vCbStergere = (CheckBox)gvSabloane.Rows[i].FindControl("cbStergere");
                    SqlDataReader vLinieSabloane = cmd.ExecuteReader();
                    if (vLinieSabloane.Read())
                    {
                        // daca exista in baza de date populam cu ce exista
                        // daca nu exista populam goale

                        if (Convert.ToInt32(vLinieSabloane["exista"]) > 0)
                        {
                            if (vLinieSabloane["dreptCitire"].ToString() == "0") { vCbCitire.Checked = false; } else { vCbCitire.Checked = true; }
                            if (vLinieSabloane["dreptModificare"].ToString() == "0") { vCbModificare.Checked = false; } else { vCbModificare.Checked = true; }
                            if (vLinieSabloane["dreptStergere"].ToString() == "0") { vCbStergere.Checked = false; } else { vCbStergere.Checked = true; }
                            if (vLinieSabloane["dreptCreare"].ToString() == "0") { vCbCreare.Checked = false; } else { vCbCreare.Checked = true; }
                            if (vLinieSabloane["dreptTiparire"].ToString() == "0") { vCbTiparire.Checked = false; } else { vCbTiparire.Checked = true; }
                        }
                        
                        
                    }
                    else
                    {
                        vCbCitire.Checked = true;
                        vCbModificare.Checked = false;
                        vCbStergere.Checked = false; 
                        vCbCreare.Checked = false; 
                        vCbTiparire.Checked = false; 
                    }
                }
                catch 
                { 
                
                
                
                }
                con.Close();
            }
            else { }
        } 
    }
}
