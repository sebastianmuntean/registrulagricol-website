﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="semnalareEroare.aspx.cs" Inherits="semnalareEroare" Culture="ro-RO"
    UICulture="ro-RO" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Semnalează o eroare" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="pnPanelGeneral" runat="server">
        <asp:Panel ID="pnAdaugaUtilizator" CssClass="panel_general" runat="server">
            <asp:Panel ID="pnAfiseazaMesaje" runat="server">
                <asp:Label ID="lblEroare" runat="server" CssClass="validator" Text="Eroare" Visible="false" />
            </asp:Panel>
            <asp:Panel ID="pnMaster" CssClass="adauga" runat="server">
                <asp:Panel ID="pnDetalii" runat="server">
                    <p>
                        <asp:Label runat="server" ID="lblMesaj" Text="Vă rugăm să scrieţi eroarea observată în căsuţa de mai jos"></asp:Label>
                    </p>
                    <p>
                        <asp:TextBox ID="tbMesaj" runat="server" TextMode="MultiLine" Rows="20" Width="800px"></asp:TextBox>
                        
                        <script type="text/javascript">
                        //<![CDATA[

                        CKEDITOR.replace('ctl00_ContentPlaceHolder1_tbMesaj',
					{
					    contentsCss: ['http://www.legex.ro/styles/mo.css', CKEDITOR.basePath + 'mo.css'],
					    fullPage: true,
					    	height : 300,
					    	
					    
					});

                        //]]>
                        </script>
                        
                    </p>
                </asp:Panel>
                <asp:Panel ID="pnButoanePrincipale" runat="server" CssClass="butoane">
                    <asp:Button ID="btModificaCont" runat="server" CssClass="buton" Text="semnalează eroare"
                        OnClick="btModificaCont_Click" />
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
        <asp:Panel ID="Panel7" CssClass="ascunsa" runat="server">
        </asp:Panel>
    </asp:Panel>
</asp:Content>
