﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class mentiuni : System.Web.UI.Page
{
    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        DBConnections connection = new DBConnections(Convert.ToInt16(Session["SESan"]));

        SqlMentiuni.ConnectionString = connection.Create();

        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "mentiuni", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
        }
    }
    protected void gvMentiuni_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(gvMentiuni, e, this);
    }
    protected void gvMentiuni_SelectedIndexChanged(object sender, EventArgs e)
    {
        btModifica.Visible = true;
        btSterge.Visible = true;
    }
    protected void btAdauga_Click(object sender, EventArgs e)
    {
        ViewState["tip"] = "a";
        tbText.Text = "";
        lblAdaugaMentiuni.Text = "ADAUGĂ MENȚIUNE";
        pnCautare.Visible = false;
        pnGrid.Visible = false;
        pnButoane.Visible = false;
        pnAdaugaMentiuni.Visible = true;
    }
    protected void btModifica_Click(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select mentiuneText from mentiuni where mentiuneId='" + gvMentiuni.SelectedValue + "'";
        SqlDataReader vTabel = vCmd.ExecuteReader();
        if (vTabel.Read())
        {
            ViewState["tip"] = "m";
            tbText.Text = vTabel["mentiuneText"].ToString();
            lblAdaugaMentiuni.Text = "MODIFICARE MENȚIUNE";
            pnCautare.Visible = false;
            pnGrid.Visible = false;
            pnButoane.Visible = false;
            pnAdaugaMentiuni.Visible = true;
        }
        ManipuleazaBD.InchideConexiune(vCon);
    }
    protected void btSterge_Click(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "delete from mentiuni where mentiuneId='" + gvMentiuni.SelectedValue + "'";
        vCmd.ExecuteNonQuery();
        ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "mentiuni", "stergere mentiuni", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
        gvMentiuni.DataBind();
        ManipuleazaBD.InchideConexiune(vCon);
    }
    protected void btAnuleazaMentiune_Click(object sender, EventArgs e)
    {
        pnCautare.Visible = true;
        pnGrid.Visible = true;
        pnButoane.Visible = true;
        pnAdaugaMentiuni.Visible = false;
    }
    protected void btSalveazaMentiune_Click(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        if (ViewState["tip"].ToString() == "a")
        { 
            // adaugare mentiune
            vCmd.CommandText = "insert into mentiuni (unitateId, gospodarieId, mentiuneText) values ('" + Session["SESunitateId"].ToString() + "', '" + Session["SESgospodarieId"].ToString() + "', N'" + tbText.Text + "')";
            vCmd.CommandText = vCmd.CommandText.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
            vCmd.ExecuteNonQuery();
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "mentiuni", "adaugare mentiuni", "", Convert.ToInt64(Session["SESgospodarieId"]), 2);
        }
        else if (ViewState["tip"].ToString() == "m")
        { 
            // modificare mentiune
            vCmd.CommandText = "update mentiuni set mentiuneText=N'" + tbText.Text + "' where mentiuneId='" + gvMentiuni.SelectedValue + "'";
            vCmd.CommandText = vCmd.CommandText.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
            vCmd.ExecuteNonQuery();
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "mentiuni", "modificare mentiuni", "", Convert.ToInt64(Session["SESgospodarieId"]), 3);
        }
        pnCautare.Visible = true;
        pnGrid.Visible = true;
        pnButoane.Visible = true;
        pnAdaugaMentiuni.Visible = false;
        gvMentiuni.DataBind();
        ManipuleazaBD.InchideConexiune(vCon);
    }
    protected void btTiparire_Click(object sender, EventArgs e)
    {
        if (Convert.ToInt32(Session["SESAn"]) < 2015)
        {
            ResponseHelper.Redirect("~/printMentiuni.aspx", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "mentiuni", "tiparire mentiuni", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
        }
        else
        {
            ResponseHelper.Redirect("~/printCapitol16.aspx", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "capitol16", "tiparire capitol16", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
        }
    }
}
