﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;

public partial class Pagini : System.Web.UI.Page
{
    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        DBConnections connection = new DBConnections(Convert.ToInt16(Session["SESan"]));

        SqlPagini.ConnectionString = connection.Create();

        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "pagini", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
        }
        // daca nu e unitate TNT nu poate accesa pagina
        if (Redirect.RedirectUtilizator(Session["SESutilizatorId"].ToString(),Convert.ToInt16(HttpContext.Current.Session["SESan"])) == 1)
        {
            Response.Redirect("NuAvetiDrepturi.aspx");
        }
    }
    protected void AdaugaPagina(object sender, EventArgs e)
    {
        string strConn = ConfigurationManager.ConnectionStrings["TNTRegistruAgricolConnectionString"].ConnectionString;
        SqlConnection con = new SqlConnection(strConn);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandType = System.Data.CommandType.Text;
        TextBox vTbDenumirePagina = (TextBox)((Button)sender).Parent.FindControl("tbPaginaDenumire");
        TextBox vTbDescrierePagina = (TextBox)((Button)sender).Parent.FindControl("tbDescrierePagina");
        TextBox vTbURLPagina = (TextBox)((Button)sender).Parent.FindControl("tbPaginaUrl");

        DropDownList vDdPaginaActiva = (DropDownList)((Button)sender).Parent.FindControl("ddPaginaActiva");
        cmd.CommandText = "INSERT INTO pagini (paginaDenumire, paginaDescriere, paginaActiva, paginaUrl) VALUES ('" + vTbDenumirePagina.Text + "', '" + vTbDescrierePagina.Text + "', '"+ vDdPaginaActiva.SelectedValue +"', '" + vTbURLPagina.Text + "' )";
        con.Open();
        try
        {
            cmd.ExecuteNonQuery();
        }
        catch { }
        con.Close();
        gvPagini.DataBind();
    }

    protected void SqlPagini_Deleted(object sender, SqlDataSourceStatusEventArgs e)
    {

    }
}
