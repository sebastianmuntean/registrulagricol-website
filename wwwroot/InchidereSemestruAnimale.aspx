﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="InchidereSemestruAnimale.aspx.cs" Inherits="InchidereSemestruAnimale" UICulture="ro-Ro" Culture="ro-Ro" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Administrare / Închidere semestru animale" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="upParcele" runat="server">
        <ContentTemplate>
            <br />
            <br />
            <asp:Label ID="Label2" Font-Size="Large" ForeColor="Red" runat="server" Text="ATENŢIE !!! Rulaţi o singură dată."></asp:Label>
            <br />
            <br />
            <label id="textLabel" style="text-align: justify; font-size: large; margin-left: 25px">Procedura se realizează după următoarele reguli: </label>

            <div style="width: auto; margin-right: 100px">
                <ol style="text-align: justify; font-size: large; margin-left: 25px">
                    <li>Se execută pentru fiecare gospodărie în parte.
                    </li>
                    <li>Sunt necesare datele din capitolele 8 si 7.
                    </li>
                    <li>Aceste date trebuie să fie corelate în mod corespunzător. 
                        <ul style="margin-left: 50px;">
                            <li>De exemplu, dacă în capitolul 7, avem la Cod Rând 1 (Bovine - total) o sumă, aceasta trebuie să se regăsească la capitolul 8, Cod Rând 1 (Bovine total existente la începutul semestrului). Şi tot aşa.
                            </li>
                        </ul>
                    </li>
                    <li>Dacă se execută închiderea fără a se fi vizitat şi actualizat datele din capitolul 8, dar sunt date în capitolul 7, procedura completează capitolul 8 cu zerouri. Acest lucru este necesar pentru următoarea regulă:
                    </li>
                    <li>Venim în sprijinul dumneavoastră şi facem calculele pentru capitolul 8, la totalurile de la începutul semestrului şi de la sfârşitul semestrului. Însă, nu vom şti niciodată ce s-a cumpărat şi cât sau ce s-a vândut şi cât. De aceea, indicat ar fi să introduceţi dvs. toate datele.
                    </li>
                    <li>Calculele se execută doar dacă datele sunt diferite de 0, exceptând situaţia de la regula 4. 
                    </li>
                    <li>Animalele sunt trecute, acolo unde este cazul, în capitolul 7, la rândurile corespunzătoare în semestrul 2. Cu alte cuvinte, cresc.
                         <ul style="margin-left: 50px;">
                             <li>De exemplu, să presupunem că, în semestrul 1, s-a completat atât Cod Rând 5 cu 1, cât şi Cod Rând 6, tot cu 1. Cu alte cuvinte, avem 1 bivol mascul sub 1 an, mai mult chiar, acesta este sub 6 luni (Cod Rând 6). Acesta va fi trecut, în semestrul 2, la Cod Rând 5: Cod Rând 6 va indica 0, iar Cod Rând 5 va indica 1. Si tot aşa.
                             </li>
                         </ul>
                        Acolo unde nu este cazul, se copiază pur şi simplu datele.
                    </li>
                    <br />
                    <ul style="color: red">
                        Nu uitaţi că acest transfer este necesar să fie făcut înainte de a se închide anul.
                        <br />
                        Vă recomandăm să porniţi închiderea la sfârşitul programului dvs. de lucru.
                    </ul>
                </ol>
            </div>

            <asp:Panel ID="panel" runat="server" CssClass="panel_general">
                <br />
                <asp:CheckBoxList ID="unitatiCheckBoxList" RepeatColumns="20" RepeatDirection="Vertical" CellSpacing="5" AutoPostBack="true" Visible="true" runat="server"></asp:CheckBoxList>
                <br />
                <asp:Label ID="corelatiiLabel" runat="server" ForeColor="#ff3300" Font-Size="Large"></asp:Label>
                <br />
                <asp:GridView ID="corelatiiGridView" CssClass="tabela" runat="server"></asp:GridView>
                <br />
                <asp:Panel ID="panelButoane" CssClass="butoane" runat="server">
                    <asp:Button ID="inchidereSemestruAnimaleButton" Text="Inchide semestrul" CssClass="buton" runat="server" OnClick="inchidereSemestruAnimaleButton_Click" />
                    <asp:Button ID="exportToExcelButton" Text="Export to Excel" Visible="true" CssClass="buton" runat="server" OnClick="exportToExcelButton_Click" />
                    <asp:Button ID="exportToWordButton" Text="Export to Word" Visible="true" CssClass="buton" runat="server" OnClick="exportToWordButton_Click" />
                </asp:Panel>
                <asp:Panel ID="pnInchideMaiMulteDeLaPanaLa" CssClass="butoane" runat="server" Visible="false">
                    <asp:Button CssClass="buton" ID="btInchideMaiMulteDeLaPanaLa" runat="server" Text="transfera animale pentru mai multe unitati" OnClick="btInchideMaiMulteDeLaPanaLa_Click" />
                    <asp:TextBox ID="tbDeLa" runat="server"></asp:TextBox>
                    <asp:TextBox ID="tbPanaLa" runat="server"></asp:TextBox>
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>






