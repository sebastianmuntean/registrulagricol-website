﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class corelatiiUnitate : System.Web.UI.Page
{
    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btCorelatii_Click(object sender, EventArgs e)
    {
        // facem o lista cu toate gospodariile de pe unitate
        int vContor = 0;
        List<string> vGospodariiId = ManipuleazaBD.fRezultaListaStringuri("SELECT gospodarieId FROM gospodarii WHERE unitateId='" + Session["SESunitateId"].ToString() + "'", "gospodarieId", Convert.ToInt16(Session["SESan"]));
        foreach ( string vGospodarieID in vGospodariiId)
        {
            string[] vCapitole = { "2a", "2b", "2c", "3", "4a", "4b", "4c", "5a", "5b", "5c", "5d", "6", "7", "8", "9", "10a", "10b", "11", "parcele", "paduri" };
            foreach (string vCapitol in vCapitole)
            {
                clsCorelatii.VerificaCorelatie(Convert.ToInt32(Session["SESunitateId"]), Convert.ToInt32(vGospodarieID), Convert.ToInt32(Session["SESan"]), vCapitol);
            }
            vContor++;
            //if (vContor == 3) break;
        }
        valCustom.IsValid = false;
        valCustom.ErrorMessage = "Au fost verificate un număr de " + vContor.ToString() + " din " + vGospodariiId.Count().ToString() + " gospodării!";
        ((MasterPage)this.Page.Master).VerificaCorelatii();
    }
    protected void btListaGospodarii_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Gospodarii.aspx");
    }
}
