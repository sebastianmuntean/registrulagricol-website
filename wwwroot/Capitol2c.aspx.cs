﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
/// <summary>
/// Adaugare capitol
/// Creata la:                  ??.02.2011
/// Autor:                      SM
/// Ultima                      actualizare: 01.04.2011
/// Autor:                      SM - adaugare log
/// </summary> 
public partial class Capitol2c : System.Web.UI.Page
{
    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "Cap.2c", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["SESgospodarieId"] == null)
        {
            HttpCookie vCookie = Request.Cookies["COOKTNT"];
            vCookie = CriptareCookie.DecodeCookie(vCookie);
            if (vCookie != null)
            {
                if (vCookie["COOKgospodarieId"] != null)
                {
                    Session["SESgospodarieId"] = vCookie["COOKgospodarieId"].ToString();
                }
                else if (Request.Url.AbsolutePath.IndexOf("Gospodarii.aspx") == -1 && Session["SESgospodarieId"] == null)
                    Response.Redirect("~/Gospodarii.aspx");
            }
        }
    }
    protected void Initializare()
    {
        tbProprietar.Text = ManipuleazaBD.fRezultaUnString("SELECT TOP(1) nume FROM membri  WHERE gospodarieId='" + Session["SESgospodarieId"].ToString() + "' AND membri.codRudenie='1'", "nume", Convert.ToInt16(Session["SESan"]));
        tbUP.Text = "";
        tbUA.Text = "";
        tbSuprafataGr2Ari.Text = "0";
        tbSuprafataGr2Ha.Text = "0";
        tbHa.Text = "0";
        tbAri.Text = "0";
    }
    protected void ArataAdaugaPaduri(object sender, EventArgs e)
    {
        pnAdaugaPaduri.Visible = true;
        pnListaPaduri.Visible = false;
       
        // daca e adaugare
        if (((Button)sender).ID.ToString() == "btListaAdaugaPaduri")
        {
            Initializare();
            // verificam daca exista un cap de gospodarie; daca exista si nu e cel modificat nu afisam cap de gospodarie in dd
            btAdaugaPaduri.Visible = true;
            btAdaugaModificaPaduri.Visible = false;
            btModificaArataListaPaduri.Visible = false;
            btAdaugaArataListaPaduri.Visible = true;
        }
        // daca e modificare
        else
        {
            // if (gvPaduri.SelectedValue != 

            btAdaugaPaduri.Visible = false;
            btAdaugaModificaPaduri.Visible = true;
            btModificaArataListaPaduri.Visible = true;
            btAdaugaArataListaPaduri.Visible = false;
            // luam valorile campurilor 
            string vPaduriId = gvPaduri.SelectedValue.ToString();
            string[] vWhereCampuri = { "paduriId" };
            string[] vWhereValori = { vPaduriId };
            string[] vOperatoriConditionali = { "=" };
            string[] vOperatoriLogici = { " " };
            string[] vCampuriRezultate = { "paduriProprietar", "paduriCodRand", "paduriUnitateDeProductie", "paduriUnitateaAmenajistica", "paduriSuprafataHa", "paduriSuprafataAri", "paduriSuprafataGrupa2Ha", "paduriSuprafataGrupa2Ari", "an"};
            List<ListaSelect> vListaCampuri = ManipuleazaBD.fSelectCuRezultatMultiplu("Paduri", vWhereCampuri, vWhereValori, vOperatoriConditionali, vOperatoriLogici, vCampuriRezultate, Convert.ToInt16(Session["SESan"]));
            foreach (ListaSelect vPaduriCampuri in vListaCampuri)
            {
                tbProprietar.Text = vPaduriCampuri.Col1;
                lblCodRand.Text = vPaduriCampuri.Col2;
                tbUP.Text = vPaduriCampuri.Col3;
                tbUA.Text = vPaduriCampuri.Col4;
                tbHa.Text = vPaduriCampuri.Col5;
                tbAri.Text = vPaduriCampuri.Col6;
                tbSuprafataGr2Ha.Text = vPaduriCampuri.Col7;
                tbSuprafataGr2Ari.Text = vPaduriCampuri.Col8;
            }
        }
    }

    protected void StergePaduri(object sender, EventArgs e)
    {
        string vPaduriId = gvPaduri.SelectedValue.ToString();
        ManipuleazaBD.fManipuleazaBD("DELETE FROM [paduri] WHERE [paduriId] = '" + vPaduriId + "'", Convert.ToInt16(Session["SESan"]));
        ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "paduri", "sterge padure ID: " + vPaduriId, "", Convert.ToInt64(Session["SESgospodarieId"]), 4);
        ClassLog.UpdateDataUltimeiModificari(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESgospodarieId"]));
        clsCorelatii.VerificaCorelatie(Convert.ToInt32(Session["SESunitateId"]), Convert.ToInt32(Session["SESgospodarieId"]), Convert.ToInt32(Session["SESan"]), "paduri");
        ((MasterPage)this.Page.Master).VerificaCorelatii();
        gvPaduri.DataBind();
        gvPaduri.SelectedIndex = -1;
        btStergePaduri.Visible = false;
        btModificaPaduri.Visible = false;
    }
    protected void ValidamNumerice()
    { 
        TextBox[] vControale = new TextBox[]{tbAri, tbHa, tbSuprafataGr2Ari, tbSuprafataGr2Ha};
        foreach (TextBox vControl in vControale)
        {
            if (!Valideaza.ENumeric(vControl.Text.Replace(',', '.')) || 
                (Valideaza.ENumeric(vControl.Text.Replace(',', '.')) && Convert.ToDecimal(vControl.Text.Replace(',', '.'))<0)  )
            {
                vControl.Text = "0";
            }
        }
        // total
        Decimal vHa = Convert.ToDecimal(tbHa.Text.Replace('.', ','));
        Decimal vAri = Convert.ToDecimal(tbAri.Text.Replace('.', ','));
        vHa += Math.Floor( vAri / 100);
        tbHa.Text = (Convert.ToInt32(vHa)).ToString();
        vAri -= Math.Floor( vAri / 100)*100;
        tbAri.Text = vAri.ToString();
        // grII
        vHa = Convert.ToDecimal(tbSuprafataGr2Ha.Text.Replace('.', ','));
        vAri = Convert.ToDecimal(tbSuprafataGr2Ari.Text.Replace('.', ','));
        vHa += Math.Floor(vAri / 100);
        tbSuprafataGr2Ha.Text = (Convert.ToInt32(vHa)).ToString();
        vAri -= Math.Floor(vAri / 100) * 100;
        tbSuprafataGr2Ari.Text = vAri.ToString();
    }

    protected void AdaugaModificaPaduri(object sender, EventArgs e)
    {
        
        // aflam numarul maxim de Paduri si dam valoare lui codRand
        // il folosim doar daca e adaugare
        // validam textboxurile numerice; daca nu sunt numerice => = 0
        ValidamNumerice();

        if (((Button)sender).ID.ToString() == "btAdaugaPaduri")
        {
            string[] vWhereCampuri = { "gospodarieId", "unitateId", "an" };
            string[] vWhereValori = { Session["SESgospodarieId"].ToString(), Session["SESunitateId"].ToString(), Session["SESan"].ToString() };
            string[] vOperatoriConditionali = { "=", "=", "=" };
            string[] vOperatoriLogici = { " ", " AND ", " AND " };
            lblCodRand.Text = (ManipuleazaBD.fRezultaUnMaximInt("paduri", "paduriCodRand", vWhereCampuri, vWhereValori, vOperatoriConditionali, vOperatoriLogici, Convert.ToInt16(Session["SESan"])) + 1).ToString();
            if (lblCodRand.Text == "0") { lblCodRand.Text = "1"; }
        }
        else
        {
           
        }
        string[] vCampuri = { "paduriProprietar", "paduriCodRand", "paduriUnitateDeProductie", "paduriUnitateaAmenajistica", "paduriSuprafataHa", "paduriSuprafataAri", "paduriSuprafataGrupa2Ha", "paduriSuprafataGrupa2Ari", "an", "unitateId", "gospodarieId" };
        string[] vValori = { tbProprietar.Text, lblCodRand.Text, tbUP.Text, tbUA.Text, tbHa.Text, tbAri.Text.Replace(',', '.'), tbSuprafataGr2Ha.Text, tbSuprafataGr2Ari.Text.Replace(',', '.'), Session["SESan"].ToString(), Session["SESunitateId"].ToString(), Session["SESgospodarieId"].ToString() };

        if (((Button)sender).ID.ToString() == "btAdaugaPaduri")
        {
            ManipuleazaBD.fManipuleazaBDArray(0, "paduri", vCampuri, vValori, "", 0, Convert.ToInt16(Session["SESan"]));
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "paduri", "adauga padure proprietar: " + tbProprietar.Text, "", Convert.ToInt64(Session["SESgospodarieId"]), 2);
            ClassLog.UpdateDataUltimeiModificari(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESgospodarieId"]));
            gvPaduri.SelectedIndex = -1;
            pnAdaugaPaduri.Visible = false;
            pnListaPaduri.Visible = true;
            btModificaPaduri.Visible = false;
            btStergePaduri.Visible = false;
        }
        else
        {
            ManipuleazaBD.fManipuleazaBDArray(1, "paduri", vCampuri, vValori, "paduriId", Convert.ToInt32(gvPaduri.SelectedValue), Convert.ToInt16(Session["SESan"]));
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "paduri", "modifica padure ID: " +  Convert.ToInt32(gvPaduri.SelectedValue), "", Convert.ToInt64(Session["SESgospodarieId"]), 3);
            ClassLog.UpdateDataUltimeiModificari(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESgospodarieId"]));
            pnAdaugaPaduri.Visible = false;
            pnListaPaduri.Visible = true;
            btModificaPaduri.Visible = true;
            btStergePaduri.Visible = true;
            gvPaduri.DataBind();
        }
        clsCorelatii.VerificaCorelatie(Convert.ToInt32(Session["SESunitateId"]), Convert.ToInt32(Session["SESgospodarieId"]), Convert.ToInt32(Session["SESan"]), "paduri");
        ((MasterPage)this.Page.Master).VerificaCorelatii();
        gvPaduri.DataBind();
        if (((Button)sender).ID.ToString() == "btAdaugaPaduri") { gvPaduri.SelectedIndex = -1; }
        else
        {
            int vValoareSelectata = Convert.ToInt32(gvPaduri.SelectedValue);
            gvPaduri.SelectedIndex = GasesteRandul(vValoareSelectata);
        }
        
    }
    protected int GasesteRandul(int pValoare)
    {
        int vRand = -1;
        for (int i = 0; i < gvPaduri.Rows.Count; i++)
        {
            if (gvPaduri.Rows[i].Cells[0].Text == pValoare.ToString())
            {
                vRand = i;
                break;
            }
        }
        return vRand;
    }
    protected void ModificaArataListaPaduri(object sender, EventArgs e)
    {
        pnAdaugaPaduri.Visible = false;
        pnListaPaduri.Visible = true;
        btStergePaduri.Visible = true;
        btModificaPaduri.Visible = true;
    }
    protected void AdaugaArataListaPaduri(object sender, EventArgs e)
    {
        pnAdaugaPaduri.Visible = false;
        pnListaPaduri.Visible = true;
        btStergePaduri.Visible = false;
        btModificaPaduri.Visible = false;
        gvPaduri.SelectedIndex = -1;
    }
    protected void gvPaduri_SelectedIndexChanged(object sender, EventArgs e)
    {
        // actiuni la selectare rand; aratam butoanele de sterge si modifica
        btAdaugaPaduri.Visible = true;
        btModificaPaduri.Visible = true;
        btStergePaduri.Visible = true;
        gvPaduri.DataBind();
    }
    protected void gvPaduri_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(gvPaduri, e, this);
    }
    protected void gvPaduri_DataBound(object sender, EventArgs e)
    {

    }

    protected void btTiparesteCapitol_Click(object sender, EventArgs e)
    {
        ResponseHelper.Redirect("~/printCapitolul2c.aspx?codCapitol=2c", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
        ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "tipareste", "tiparire paduri ", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
    }

}
