﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;


public partial class adevSubsoluri : System.Web.UI.Page
{

    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "adevAnteturi", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
            tbTextAvansat.DataBind();
            ViewState["adauga"] = "";
        }
        if (ddlNumarColoane.SelectedValue == "0")
        {
            lblLegenda.Visible = false;
            dlEmbleme.Visible = false;
            pnlButRefresh.Visible = false;
            btAdaugaAdevAntet.Visible = false;
        }
        else
        {
            lblLegenda.Visible = true;
            dlEmbleme.Visible = true;
            dlEmbleme.DataBind();
            pnlButRefresh.Visible = true;
            tbTextAvansat.Visible = true;
        }

    //    ddlNumarColoane_SelectedIndexChanged(sender, e);
        lblLegenda.Text =
    @"Legenda: 
*************************************** 
ARANJARE TEXT
***************************************
[paragraf]  (paragraf nou) 
!!!Atentie!!! Pentru paragraf nou folositi tasta ENTER sau 
SHIFT+ENTER (pentru ruperea randului care sa pastreze aceeasi distanta intre randuri)

*************************************** 
DETALII UNITATE
***************************************
[unitate]  denumirea unităţii         
[judet]  judeţul 
[localitate-rang] orașului, municipiului, comunei, satului
[adresa-unitate] adresa unitatii      
[telefon-unitate] telefon unitate
[fax-unitate] fax       
[email-unitate] sau [email] email unitate
[web-unitate] pagina internet a unitatii
[cod-postal-unitate] codul postal al unitatii
[primar]  primar
[viceprimar]  vice-primar 
[secretar]  secretar
[contabil]  contabil 
[sefserviciuagricol] sef serviciu agricol  
[inspector], [inspector1], [inspector2], [inspector3]   inspectori ([inspector] preia datele din inspector1) 
[utilizator] numele si prenumele utilizatorului care emite adeverinta

";
    }

    protected void AdaugaModificaAdevAntet(object sender, EventArgs e)
    {
        // schimbam textul pt actualizare url imagini
        AfiseazaImagini();
        string vInterogare = "";
        int vValoareSelectata = Convert.ToInt32(gvListaAdevAnteturi.SelectedValue);
        // la adevtip scriem din ddlist daca nu, din sesiune
        // daca e TNT scriem 1
        string vTipUnitate = "0";
        try
        {
            vTipUnitate = ddAdevTip.SelectedValue;
        }
        catch
        {
            vTipUnitate = Session["SESunitateId"].ToString();
        }
        if (((Button)sender).ID.ToString() == "btAdaugaAdevAntet")
        {
            vInterogare = "INSERT INTO adeverinteSubsoluri (unitateId,adevTip,adevSubsolCol1,adevSubsolCol2,adevSubsolCol3,adevSubsolCol4,adevSubsolCol5,adevSubsolCol6,adevSubsolCol7,adevSubsolCol8,adevSubsolCol9, adevSubsolStil, adevSubsolTip, adevSubsolDenumire, adevSubsolNrColoane, adevSubsolText) VALUES ('" + Session["SESunitateId"] + "','" + vTipUnitate + "','','','','','','','','','','subsol_1', '1', '" + tbDenumireAntet.Text + "', '" + ddlNumarColoane.SelectedValue + "', N'" + tbTextAvansat.Text + "')";
            ManipuleazaBD.fManipuleazaBD(vInterogare, Convert.ToInt16(Session["SESan"]));
            gvListaAdevAnteturi.SelectedIndex = -1;
            pnAdaugaAdevAntet.Visible = false;
            pnListaAdevAnteturi.Visible = true;
            btModificaAdevAntet.Visible = false;
            btStergeAdevAntet.Visible = false;
        }
        else
        {
            vInterogare = @"UPDATE adeverinteSubsoluri SET adevTip = '" + vTipUnitate + "', adevSubsolDenumire =N'" + tbDenumireAntet.Text + "', adevSubsolNrColoane = '" + ddlNumarColoane.SelectedValue + "', adevSubsolText = N'" + tbTextAvansat.Text + "'  WHERE adevSubsolId =N'" + vValoareSelectata.ToString() + "'";
            ManipuleazaBD.fManipuleazaBD(vInterogare, Convert.ToInt16(Session["SESan"]));
            pnAdaugaAdevAntet.Visible = false;
            pnListaAdevAnteturi.Visible = true;
            btModificaAdevAntet.Visible = false;
            btStergeAdevAntet.Visible = false;
            gvListaAdevAnteturi.SelectedIndex = GasesteRandul(vValoareSelectata);
        }
        gvListaAdevAnteturi.DataBind();
    }
    protected int GasesteRandul(int pValoare)
    {
        int vRand = -1;
        for (int i = 0; i < gvListaAdevAnteturi.Rows.Count; i++)
        {
            if (gvListaAdevAnteturi.Rows[i].Cells[0].Text == pValoare.ToString())
            {
                vRand = i;
                break;
            }
        }
        return vRand;
    }
    protected void ModificaArataListaAdevAntet(object sender, EventArgs e)
    {
        pnAdaugaAdevAntet.Visible = false;
        pnListaAdevAnteturi.Visible = true;
        btModificaAdevAntet.Visible = true;
        btStergeAdevAntet.Visible = true;

    }
    protected void AdaugaArataListaAdevAntet(object sender, EventArgs e)
    {
        pnAdaugaAdevAntet.Visible = false;
        pnListaAdevAnteturi.Visible = true;
        btModificaAdevAntet.Visible = false;
        btStergeAdevAntet.Visible = false;
    }
    protected void InitializareCampuri(int pTip, string pAdevAntetId)
    {

        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        // pTip = 0 adaugare, 1 modificare
        // adaugare
        if (pTip == 0)
        {
            lblLegenda.Visible = false;
            dlEmbleme.Visible = false;
            pnlButRefresh.Visible = false;
            ddlNumarColoane.Enabled = true;
            tbTextAvansat.Visible = false;
            //ddlNumarColoane.DataBind();
            ddAdevTip.DataBind();
            // daca nu e utilizator administrator = 0, nu afisam tipul de adeverinta si selectam tipul de adeverinta cu unitatea utilizatorului
            if (ManipuleazaBD.fRezultaUnString("SELECT TOP(1) tipUtilizatorId FROM utilizatori WHERE utilizatorId ='" + Session["SESutilizatorId"].ToString() + "'", "tipUtilizatorId", Convert.ToInt16(Session["SESan"])) == "1")
            {
                lblAdevTip.Visible = true;
                ddAdevTip.Visible = true;
            }
            else
            {
                lblAdevTip.Visible = false;
                ddAdevTip.Visible = false;
                ddAdevTip.DataBind();
                ddAdevTip.SelectedValue = ManipuleazaBD.fRezultaUnString("SELECT TOP(1) unitateId FROM utilizatori WHERE utilizatorId ='" + Session["SESutilizatorId"].ToString() + "'", "unitateId", Convert.ToInt16(Session["SESan"])).ToString();
            }
            tbDenumireAntet.Text = "";
            // lasam sa aleaga nr coloane
            try
            {
                ddlNumarColoane.SelectedValue = "0";
            }
            catch
            {
                ddlNumarColoane.Items.Clear();
                ListItem vListItem = new ListItem();
                vListItem.Value = "0";
                vListItem.Text = "alege numărul de coloane";
                ddlNumarColoane.Items.Add(vListItem);
                vListItem = new ListItem();
                vListItem.Value = "2";
                vListItem.Text = "două coloane";
                ddlNumarColoane.Items.Add(vListItem);                
                vListItem = new ListItem();
                vListItem.Value = "1";
                vListItem.Text = "o coloană (pe mijloc)";
                ddlNumarColoane.Items.Add(vListItem);                
                vListItem = new ListItem();
                vListItem.Value = "3";
                vListItem.Text = "trei coloane";
                ddlNumarColoane.Items.Add(vListItem);
                ddlNumarColoane.SelectedValue = "0";

            }                
            btAdaugaAdevAntet.Visible = false;
            btAdaugaModificaAdevAntet.Visible = false;
            try
            {            
             //   ddAdevTip.SelectedValue = "0";
            }
            catch { }
        }
        // modificare
        else
        {
            // daca nu e utilizator administrator = 0, nu afisam tipul de adeverinta si selectam tipul de adeverinta existent
            ddAdevTip.DataBind();
            ddAdevTip.SelectedValue = ManipuleazaBD.fRezultaUnString("SELECT adevTip FROM adeverinteSubsoluri WHERE adevSubsolId ='" + gvListaAdevAnteturi.SelectedValue + "'", "adevTip", Convert.ToInt16(Session["SESan"]));
            // alegem numarul de coloane si facem disable
            ddlNumarColoane.SelectedValue = ManipuleazaBD.fRezultaUnString("SELECT adevSubsolNrColoane FROM adeverinteSubsoluri WHERE adevSubsolId ='" + gvListaAdevAnteturi.SelectedValue + "'", "adevSubsolNrColoane", Convert.ToInt16(Session["SESan"]));
            ddlNumarColoane.Enabled = false;
            if (ManipuleazaBD.fRezultaUnString("SELECT TOP(1) tipUtilizatorId FROM utilizatori WHERE utilizatorId ='" + Session["SESutilizatorId"].ToString() + "'", "tipUtilizatorId", Convert.ToInt16(Session["SESan"])) == "1")
            {
                lblAdevTip.Visible = true;
                ddAdevTip.Visible = true;
            }
            else
            {
                lblAdevTip.Visible = false;
                ddAdevTip.Visible = false;
            }
            tbTextAvansat.Text = ManipuleazaBD.fRezultaUnString("SELECT adevSubsolText FROM adeverinteSubsoluri WHERE adevSubsolId = '" + pAdevAntetId + "'", "adevSubsolText", Convert.ToInt16(Session["SESan"]));
            tbDenumireAntet.Text = ManipuleazaBD.fRezultaUnString("SELECT adevSubsolDenumire FROM adeverinteSubsoluri WHERE adevSubsolId = '" + pAdevAntetId + "'", "adevSubsolDenumire", Convert.ToInt16(Session["SESan"]));
            tbTextAvansat.DataBind();
            btAdaugaAdevAntet.Visible = false;
            btAdaugaModificaAdevAntet.Visible = true;
        }
        


    }
    protected void ArataAdaugaAdevAntet(object sender, EventArgs e)
    {
        int vValoareSelectata = Convert.ToInt32(gvListaAdevAnteturi.SelectedValue);
        pnAdaugaAdevAntet.Visible = true;
        pnListaAdevAnteturi.Visible = false;
        if (((Button)sender).ID.ToString() == "btListaAdaugaAdevAntet")
        {
            InitializareCampuri(0, "");
            gvListaAdevAnteturi.SelectedIndex = -1;
            // butoane adauga si lista-adauga active
            btAdaugaArataListaAdevAntet.Visible = true;
            btAdaugaModificaAdevAntet.Visible = false;
            btAdaugaAdevAntet.Visible = false;
            btModificaArataListaAdevAntet.Visible = false;
            ViewState["adauga"] = "a";
        }
        else
        {
            InitializareCampuri(1,vValoareSelectata.ToString());
            btAdaugaAdevAntet.Visible = false;
            btAdaugaArataListaAdevAntet.Visible = false;
            btAdaugaModificaAdevAntet.Visible = true;
            btAdaugaAdevAntet.Visible = false;
            btModificaArataListaAdevAntet.Visible = true;
            lblLegenda.Visible = true;
            dlEmbleme.Visible = true;
            dlEmbleme.DataBind();
            pnlButRefresh.Visible = true;
            tbTextAvansat.Visible = true;
            ViewState["adauga"] = "m";
        }
    }
    protected void StergeAntetul(object sender, EventArgs e)
    {
        string vAdevAntetId = gvListaAdevAnteturi.SelectedValue.ToString();
        ManipuleazaBD.fManipuleazaBD("DELETE FROM [adeverinteSubsoluri] WHERE [adevSubsolId] = '" + vAdevAntetId + "'", Convert.ToInt16(Session["SESan"]));
        gvListaAdevAnteturi.DataBind();
        gvListaAdevAnteturi.SelectedIndex = -1;
        btModificaAdevAntet.Visible = false;
        btStergeAdevAntet.Visible = false;
    }
    protected void gvListaAdevAnteturi_SelectedIndexChanged(object sender, EventArgs e)
    {
        // actiuni la selectare rand; aratam butoanele de sterge si modifica
        btAdaugaAdevAntet.Visible = true;
        btModificaAdevAntet.Visible = true;
        btStergeAdevAntet.Visible = true;
        gvListaAdevAnteturi.DataBind();
    }
    protected void gvListaAdevAnteturi_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(gvListaAdevAnteturi, e, this);
    }
    protected void gvListaAdevAnteturi_DataBound(object sender, EventArgs e)
    {
        
    }
    protected void gvListaAdevAnteturi_PreRender(object sender, EventArgs e)
    {
        for (int i = 0; i < gvListaAdevAnteturi.Rows.Count; i++)
        {
            if (gvListaAdevAnteturi.Rows[i].RowType == DataControlRowType.DataRow)
            {
                ((Label)gvListaAdevAnteturi.Rows[i].FindControl("lblText")).Text = ((Label)gvListaAdevAnteturi.Rows[i].FindControl("lblText")).Text.Replace("adeverinta_antet", "adev_antet_tabel").Replace("adev_general", "adev_antet_tabel").Replace("adev_subsoluri", String.Empty).Replace("css/master.css", String.Empty).Replace("font-size:", "x:").Replace("width:", "x:").Replace("height:", "x:");
            }
        }
    }
    protected void SqlListaAdevAnteturi_Init(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        List<string> vCampuri = new List<string> { "tipUtilizatorId", "unitateId" };
        List<string> vRezultate = new List<string> { };
        vRezultate.Clear();
        vRezultate = ManipuleazaBD.fRezultaUnString("SELECT TOP(1) tipUtilizatorId, unitateId FROM utilizatori WHERE utilizatorId ='" + Session["SESutilizatorId"].ToString() + "'", vCampuri, Convert.ToInt16(Session["SESan"]));
        if (vRezultate[0] != "1")
        {
            SqlListaAdevAnteturi.SelectCommand = "SELECT  adeverinteSubsoluri.adevSubsolText, adeverinteSubsoluri.adevSubsolCol1, adeverinteSubsoluri.adevSubsolCol2, adeverinteSubsoluri.adevSubsolCol3, adeverinteSubsoluri.adevSubsolCol4, adeverinteSubsoluri.adevSubsolCol5, adeverinteSubsoluri.adevSubsolCol6, adeverinteSubsoluri.adevSubsolCol7, adeverinteSubsoluri.adevSubsolCol8, adeverinteSubsoluri.adevSubsolCol9, adeverinteSubsoluri.adevSubsolId, unitati_1.unitateId, adeverinteSubsoluri.adevTip, unitati_1.unitateDenumire, adeverinteSubsoluri.adevSubsolDenumire, adeverinteSubsoluri.adevSubsolTip, unitati.unitateDenumire AS unitateTipAdev FROM adeverinteSubsoluri INNER JOIN unitati ON adeverinteSubsoluri.adevTip = unitati.unitateId LEFT OUTER JOIN unitati AS unitati_1 ON unitati_1.unitateId = adeverinteSubsoluri.unitateId WHERE (adeverinteSubsoluri.adevSubsolTip = '1') AND [adeverinteSubsoluri].[adevTip] = '" + vRezultate[1] + "'";
        }
        else
        {
            SqlListaAdevAnteturi.SelectCommand = "SELECT    adeverinteSubsoluri.adevSubsolText, adeverinteSubsoluri.adevSubsolCol1, adeverinteSubsoluri.adevSubsolCol2, adeverinteSubsoluri.adevSubsolCol3, adeverinteSubsoluri.adevSubsolCol4, adeverinteSubsoluri.adevSubsolCol5, adeverinteSubsoluri.adevSubsolCol6, adeverinteSubsoluri.adevSubsolCol7, adeverinteSubsoluri.adevSubsolCol8, adeverinteSubsoluri.adevSubsolCol9, adeverinteSubsoluri.adevSubsolId, unitati_1.unitateId, adeverinteSubsoluri.adevTip, unitati_1.unitateDenumire, adeverinteSubsoluri.adevSubsolDenumire, adeverinteSubsoluri.adevSubsolTip, (CASE WHEN adeverinteSubsoluri.adevTip = '' THEN 'toate unitatile' ELSE unitati.unitateDenumire END) AS unitateTipAdev FROM         adeverinteSubsoluri LEFT OUTER JOIN unitati ON adeverinteSubsoluri.adevTip = unitati.unitateId LEFT OUTER JOIN unitati AS unitati_1 ON unitati_1.unitateId = adeverinteSubsoluri.unitateId WHERE (adeverinteSubsoluri.adevSubsolTip = '1')";
        }
    }
    protected void ddAdevTip_DataBound(object sender, EventArgs e)
    {
        ListItem liAdevTip = new ListItem();
        liAdevTip.Value = "0";
        liAdevTip.Text = " vizibilă pentru toate unitățile";
        liAdevTip.Selected = true;
        ddAdevTip.Items.Add(liAdevTip);
    }
    protected void ddlNumarColoane_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            ListItem vItem = new ListItem();
            vItem.Value = "0";
            vItem.Text = "alege numărul de coloane";
            ddlNumarColoane.Items.Remove(vItem);
           // ddlNumarColoane.Items.RemoveAt(0);
            //ddlNumarColoane.DataBind();
        }
        catch { }
        tbTextAvansat.Visible = true;
        if (ViewState["adauga"].ToString() == "a")
        {

            switch (ddlNumarColoane.SelectedValue)
            {
                case "1":
                    // coloana pe mijloc, implicit 5 randuri
                    tbTextAvansat.Text = "<link href=\"css/master.css\" rel=\"stylesheet\" type=\"text/css\"><table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"adeverinta_antet\"><tbody><tr><td class=\"adev_1col_mijloc\">coloana mijloc</td></tr></tbody></table>";
                    tbTextAvansat.DataBind();
                    break;
                case "2":
                    //  2 coloane, implicit 1 rand
                    tbTextAvansat.Text = "<link href=\"css/master.css\" rel=\"stylesheet\" type=\"text/css\"><table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"adeverinta_antet\"><tbody><tr><td class=\"adev_2col_stanga\">coloana stanga</td><td class=\"adev_2col_dreapta\">coloana dreapta</td></tr></tbody></table>";
                    tbTextAvansat.DataBind();
                    break;
                case "3":
                    //  3 coloane, implicit 1 rand

                    tbTextAvansat.Text = "<link href=\"css/master.css\" rel=\"stylesheet\" type=\"text/css\"><table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"adeverinta_antet\"><tbody><tr><td class=\"adev_3col_stanga\">coloana stanga</td><td class=\"adev_3col_mijloc\">coloana mijloc</td><td class=\"adev_3col_dreapta\">coloana dreapta</td></tr></tbody></table>";

                    tbTextAvansat.DataBind();
                    break;
            }
        }
        else
        { 
          //  tbTextAvansat.Text = ManipuleazaBD.
        }
        tbTextAvansat.DataBind();
        if (tbTextAvansat.Text != "") ViewState["texte"] = tbTextAvansat.Text;
        btAdaugaAdevAntet.Visible = true;

    }
    protected void AfiseazaImagini()
    {
        // cautam stringul <img alt="" src="
        string vCautat = "<img alt='' src='";
        //vCautat = vCautat.Replace("\\", "");
        tbTextAvansat.Text = tbTextAvansat.Text.Replace('"', '\'');
        int vContor = tbTextAvansat.Text.IndexOf(vCautat);
      // int vContor = ViewState["texte"].ToString().IndexOf("<img alt=\"\" src=\"\"");
        while (vContor > 0)
        { 
            // id-ul imaginii incepe la vContor + 17
            // verificam sa nu fie o imagine deja schimbata
            if (tbTextAvansat.Text.IndexOf("Imagini/", vContor) != 0)
            {
                vContor += 17;
                int vIncepe = vContor;
                int vTermina = tbTextAvansat.Text.IndexOf("'", vContor) - vContor;
                string vId = tbTextAvansat.Text.Substring(vContor, tbTextAvansat.Text.IndexOf("'", vContor) - vContor);
                // cautam imaginea
                // daca e imaginea unei unitati distincte, nu TNT
                string vIdPlus = "";
                if (Convert.ToInt32(vId) > 100)
                {
                    string vCF = ManipuleazaBD.fRezultaUnString("SELECT unitateCodFiscal FROM unitati WHERE unitateId='" + Session["SESunitateId"].ToString() + "'", "unitateCodFiscal", Convert.ToInt16(Session["SESan"]));
                    // numele complet a imaginii
                    vIdPlus = ManipuleazaBD.fRezultaUnString("SELECT fisierNume FROM fisiere WHERE unitateCodFiscal ='" + vCF + "' AND fisierNumar = '" + vId + "'", "fisierNume", Convert.ToInt16(Session["SESan"]));
                }
                else
                { 
                    // daca e o poza universal vizibila
                    // numele complet a imaginii
                    vIdPlus = ManipuleazaBD.fRezultaUnString("SELECT fisierNume FROM fisiere WHERE unitateCodFiscal ='14146589' AND fisierNumar = '" + vId + "'", "fisierNume", Convert.ToInt16(Session["SESan"]));
                }

                // alegem clasa css
                string vClasa = "";
                switch (ddlNumarColoane.SelectedValue)
                {
                    case "1":
                        vClasa = " class='o_coloana' align='middle' ";
                        break;
                    case "2":
                        vClasa = " class='doua_coloane' align='middle' ";
                        break;
                    case "3":
                        vClasa = " class='trei_coloane' align='middle' ";
                        break;
                }

                // completam textul cu numele imaginii si clasa
                tbTextAvansat.Text = tbTextAvansat.Text.Remove(vIncepe, vId.Length+1).Insert(vIncepe, "Imagini/Embleme/" + vIdPlus + "'" + vClasa + " align=''");
                // trecem la imaginea urmatoare
                vContor = tbTextAvansat.Text.IndexOf(vCautat, vContor + vIdPlus.Length + 16 + vClasa.Length);
            }
            else
            {
                // trecem la imaginea urmatoare
                vContor = tbTextAvansat.Text.IndexOf(vCautat, vContor + 16);
            }
        }
        tbTextAvansat.Text = tbTextAvansat.Text.Replace("'", "\"");
    }
    protected void tbCol_Load(object sender, EventArgs e)
    {

        


    }
    protected void bt1_Click(object sender, EventArgs e)
    {
      //  tbTextAvansat.Text = "<link href=\"css/master.css\" rel=\"stylesheet\" type=\"text/css\"><div class=\"adev_general\"><div class=\"adev_rand\"><div class=\"adev_3col_stanga\">" + ManipuleazaBD.fRezultaUnString("SELECT adevSubsolCol1 FROM adeverinteSubsoluri WHERE adevSubsolId = '" + gvListaAdevAnteturi.SelectedValue + "'", "adevSubsolCol1") + "</div><div class=\"adev_3col_mijloc\">" + ManipuleazaBD.fRezultaUnString("SELECT adevSubsolCol4 FROM adeverinteSubsoluri WHERE adevSubsolId = '" + gvListaAdevAnteturi.SelectedValue + "'", "adevSubsolCol4") + "</div><div class=\"adev_3col_dreapta\">" + ManipuleazaBD.fRezultaUnString("SELECT adevSubsolCol7 FROM adeverinteSubsoluri WHERE adevSubsolId = '" + gvListaAdevAnteturi.SelectedValue + "'", "adevSubsolCol7") + "</div></div><div class=\"adev_rand\"><div class=\"adev_3col_stanga\">" + ManipuleazaBD.fRezultaUnString("SELECT adevSubsolCol2 FROM adeverinteSubsoluri WHERE adevSubsolId = '" + gvListaAdevAnteturi.SelectedValue + "'", "adevSubsolCol2") + "</div><div class=\"adev_3col_mijloc\">" + ManipuleazaBD.fRezultaUnString("SELECT adevSubsolCol5 FROM adeverinteSubsoluri WHERE adevSubsolId = '" + gvListaAdevAnteturi.SelectedValue + "'", "adevSubsolCol5") + "</div><div class=\"adev_3col_dreapta\">" + ManipuleazaBD.fRezultaUnString("SELECT adevSubsolCol8 FROM adeverinteSubsoluri WHERE adevSubsolId = '" + gvListaAdevAnteturi.SelectedValue + "'", "adevSubsolCol8") + "</div></div><div class=\"adev_rand\"><div class=\"adev_3col_stanga\">" + ManipuleazaBD.fRezultaUnString("SELECT adevSubsolCol3 FROM adeverinteSubsoluri WHERE adevSubsolId = '" + gvListaAdevAnteturi.SelectedValue + "'", "adevSubsolCol3") + "</div><div class=\"adev_3col_mijloc\">" + ManipuleazaBD.fRezultaUnString("SELECT adevSubsolCol6 FROM adeverinteSubsoluri WHERE adevSubsolId = '" + gvListaAdevAnteturi.SelectedValue + "'", "adevSubsolCol6") + "</div><div class=\"adev_3col_dreapta\">" + ManipuleazaBD.fRezultaUnString("SELECT adevSubsolCol9 FROM adeverinteSubsoluri WHERE adevSubsolId = '" + gvListaAdevAnteturi.SelectedValue + "'", "adevSubsolCol9") + "</div></div></div>";


        tbTextAvansat.Text = "<link href=\"css/master.css\" rel=\"stylesheet\" type=\"text/css\"><table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"adeverinta_antet\"><tbody><tr><td class=\"adev_3col_stanga\">" + ManipuleazaBD.fRezultaUnString("SELECT adevSubsolCol1 FROM adeverinteSubsoluri WHERE adevSubsolId = '" + gvListaAdevAnteturi.SelectedValue + "'", "adevSubsolCol1", Convert.ToInt16(Session["SESan"])) + "</td><td class=\"adev_3col_mijloc\">" + ManipuleazaBD.fRezultaUnString("SELECT adevSubsolCol4 FROM adeverinteSubsoluri WHERE adevSubsolId = '" + gvListaAdevAnteturi.SelectedValue + "'", "adevSubsolCol4", Convert.ToInt16(Session["SESan"])) + "</td><td class=\"adev_3col_dreapta\">" + ManipuleazaBD.fRezultaUnString("SELECT adevSubsolCol7 FROM adeverinteSubsoluri WHERE adevSubsolId = '" + gvListaAdevAnteturi.SelectedValue + "'", "adevSubsolCol7", Convert.ToInt16(Session["SESan"])) + "</td></tr><tr><td class=\"adev_3col_stanga\">" + ManipuleazaBD.fRezultaUnString("SELECT adevSubsolCol2 FROM adeverinteSubsoluri WHERE adevSubsolId = '" + gvListaAdevAnteturi.SelectedValue + "'", "adevSubsolCol2", Convert.ToInt16(Session["SESan"])) + "</td><td class=\"adev_3col_mijloc\">" + ManipuleazaBD.fRezultaUnString("SELECT adevSubsolCol5 FROM adeverinteSubsoluri WHERE adevSubsolId = '" + gvListaAdevAnteturi.SelectedValue + "'", "adevSubsolCol5", Convert.ToInt16(Session["SESan"])) + "</td><td class=\"adev_3col_dreapta\">" + ManipuleazaBD.fRezultaUnString("SELECT adevSubsolCol8 FROM adeverinteSubsoluri WHERE adevSubsolId = '" + gvListaAdevAnteturi.SelectedValue + "'", "adevSubsolCol8", Convert.ToInt16(Session["SESan"])) + "</td></tr><tr><td class=\"adev_3col_stanga\">" + ManipuleazaBD.fRezultaUnString("SELECT adevSubsolCol3 FROM adeverinteSubsoluri WHERE adevSubsolId = '" + gvListaAdevAnteturi.SelectedValue + "'", "adevSubsolCol3", Convert.ToInt16(Session["SESan"])) + "</td><td class=\"adev_3col_mijloc\">" + ManipuleazaBD.fRezultaUnString("SELECT adevSubsolCol6 FROM adeverinteSubsoluri WHERE adevSubsolId = '" + gvListaAdevAnteturi.SelectedValue + "'", "adevSubsolCol6", Convert.ToInt16(Session["SESan"])) + "</td><td class=\"adev_3col_dreapta\">" + ManipuleazaBD.fRezultaUnString("SELECT adevSubsolCol9 FROM adeverinteSubsoluri WHERE adevSubsolId = '" + gvListaAdevAnteturi.SelectedValue + "'", "adevSubsolCol9", Convert.ToInt16(Session["SESan"])) + "</td></tr></tbody></table>";

    }
    protected void bt2_Click(object sender, EventArgs e)
    {
        tbTextAvansat.Text = "<link href=\"css/master.css\" rel=\"stylesheet\" type=\"text/css\"><table class=\"adeverinta_antet\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\"><tr><td width=\"50%\">" + ManipuleazaBD.fRezultaUnString("SELECT adevSubsolCol1 FROM adeverinteSubsoluri WHERE adevSubsolId = '" + gvListaAdevAnteturi.SelectedValue + "'", "adevSubsolCol1", Convert.ToInt16(Session["SESan"])) + "</td><td width=\"50%\">" + ManipuleazaBD.fRezultaUnString("SELECT adevSubsolCol6 FROM adeverinteSubsoluri WHERE adevSubsolId = '" + gvListaAdevAnteturi.SelectedValue + "'", "adevSubsolCol6", Convert.ToInt16(Session["SESan"])) + "</td></tr><tr><td width=\"50%\">" + ManipuleazaBD.fRezultaUnString("SELECT adevSubsolCol2 FROM adeverinteSubsoluri WHERE adevSubsolId = '" + gvListaAdevAnteturi.SelectedValue + "'", "adevSubsolCol2", Convert.ToInt16(Session["SESan"])) + "</td><td width=\"50%\">" + ManipuleazaBD.fRezultaUnString("SELECT adevSubsolCol7 FROM adeverinteSubsoluri WHERE adevSubsolId = '" + gvListaAdevAnteturi.SelectedValue + "'", "adevSubsolCol7", Convert.ToInt16(Session["SESan"])) + "</td></tr><tr><td width=\"50%\">" + ManipuleazaBD.fRezultaUnString("SELECT adevSubsolCol3 FROM adeverinteSubsoluri WHERE adevSubsolId = '" + gvListaAdevAnteturi.SelectedValue + "'", "adevSubsolCol3", Convert.ToInt16(Session["SESan"])) + "</td><td width=\"50%\">" + ManipuleazaBD.fRezultaUnString("SELECT adevSubsolCol8 FROM adeverinteSubsoluri WHERE adevSubsolId = '" + gvListaAdevAnteturi.SelectedValue + "'", "adevSubsolCol8", Convert.ToInt16(Session["SESan"])) + "</td></tr><tr><td width=\"50%\">" + ManipuleazaBD.fRezultaUnString("SELECT adevSubsolCol4 FROM adeverinteSubsoluri WHERE adevSubsolId = '" + gvListaAdevAnteturi.SelectedValue + "'", "adevSubsolCol4", Convert.ToInt16(Session["SESan"])) + "</td><td width=\"50%\">" + ManipuleazaBD.fRezultaUnString("SELECT adevSubsolCol9 FROM adeverinteSubsoluri WHERE adevSubsolId = '" + gvListaAdevAnteturi.SelectedValue + "'", "adevSubsolCol9", Convert.ToInt16(Session["SESan"])) + "</td></tr><tr><td width=\"50%\">" + ManipuleazaBD.fRezultaUnString("SELECT adevSubsolCol5 FROM adeverinteSubsoluri WHERE adevSubsolId = '" + gvListaAdevAnteturi.SelectedValue + "'", "adevSubsolCol5", Convert.ToInt16(Session["SESan"])) + "</td><td width=\"50%\">&nbsp;</td></tr></table>";
    }
    protected void SqlEmbleme_Init(object sender, EventArgs e)
    {
        SqlEmbleme.SelectParameters["unitateCodFiscal"].DefaultValue = ManipuleazaBD.fRezultaUnString("SELECT unitateCodFiscal  FROM unitati WHERE unitateId ='" + Session["SESunitateId"].ToString() + "'", "unitateCodFiscal", Convert.ToInt16(Session["SESan"]));
    }
    protected void dlEmbleme_PreRender(object sender, EventArgs e)
    {
        for (int i = 0; i < dlEmbleme.Items.Count; i++)
        {
                if (File.Exists(Server.MapPath(".\\Imagini\\Embleme\\" + ((Label)dlEmbleme.Items[i].FindControl("lblDlNumeFisier")).Text)))
                {
                    ((System.Web.UI.WebControls.Image)dlEmbleme.Items[i].FindControl("imgDlEmblema")).Visible = true;
                    ((System.Web.UI.WebControls.Image)dlEmbleme.Items[i].FindControl("imgDlEmblema")).ImageUrl = "~\\Imagini\\Embleme\\" + ((Label)dlEmbleme.Items[i].FindControl("lblDlNumeFisier")).Text;
                    ((System.Web.UI.WebControls.Image)dlEmbleme.Items[i].FindControl("imgDlEmblema")).ToolTip = ((Label)dlEmbleme.Items[i].FindControl("lblDlDescriere")).Text;
                    
                    ((Label)dlEmbleme.Items[i].FindControl("lblDlNumeFisier")).Text = ((Label)dlEmbleme.Items[i].FindControl("lblDlNumeFisier")).Text.Substring(((Label)dlEmbleme.Items[i].FindControl("lblDlNumeFisier")).Text.LastIndexOf("-n") + 2, ((Label)dlEmbleme.Items[i].FindControl("lblDlNumeFisier")).Text.LastIndexOf('.') - ((Label)dlEmbleme.Items[i].FindControl("lblDlNumeFisier")).Text.LastIndexOf("-n") - 2);
                }
                else
                {
                    ((System.Web.UI.WebControls.Image)dlEmbleme.Items[i].FindControl("imgDlEmblema")).Visible = false;
                }
        }
    }
    protected void btImagini_Click(object sender, EventArgs e)
    {
        AfiseazaImagini();
        tbTextAvansat.DataBind();
    }
}    
