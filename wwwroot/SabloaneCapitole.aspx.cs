﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;

public partial class SabloaneCapitole : System.Web.UI.Page
{
    protected void Page_Init(object sender, EventArgs e)
    {
        tbfAn.Text = Convert.ToInt32(DateTime.Now.Date.Year).ToString();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "sabloaneCapitole", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
        }
    }
    protected void gvCapitole_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(gvCapitole, e, this);
    }
    protected void gvCapitole_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gvCapitole_DataBound(object sender, EventArgs e)
    {

    }
    protected void btSalveazaSablon_Click(object sender, EventArgs e)
    {
        string vInterogare = "";
        //string vInterogareSelect = "SELECT COUNT(*) FROM SabloaneCapitole WHERE  An='" + tbAn.Text + "', Capitol='" + tbCapitol.Text + "' , CodRand='" + tbCodRand.Text + "'";
        string[] vCampuri = { "an", "capitol", "codRand" };
        string[] vParametrii = { tbAn.Text, tbCapitol.Text, tbCodRand.Text };
        int vInterogareSelect = ManipuleazaBD.fVerificaExistentaArray("SabloaneCapitole", vCampuri, vParametrii, " AND ", Convert.ToInt16(Session["SESan"]));
        if (ViewState["tip"].ToString() == "a")
        {
            if (vInterogareSelect == 0)
            {
                vInterogare = "INSERT INTO SabloaneCapitole(an,capitol,codRand,formula,denumire1,denumire2,denumire3,denumire4,denumire5) VALUES ('" + tbAn.Text + "','" + tbCapitol.Text + "','" + tbCodRand.Text + "','" + tbFormula.Text + "',N'" + tbCol1.Text + "',N'" + tbCol2.Text + "',N'" + tbCol3.Text + "',N'" + tbCol4.Text + "',N'" + tbCol5.Text + "')";
            }
            else
            {
                VerDacaExistaCodRand.ErrorMessage = "Exista o alta inregistrarea cu acelasi An,Capitol si Cod rand! ";
                VerDacaExistaCodRand.IsValid = false;
                return;
            }
        }
        string vInterogareExista = "select count(*) as nr from sabloaneCapitole where an='" + tbAn.Text + "' and capitol='" + tbCapitol.Text + "' and codRand='" + tbCodRand.Text + "' and sablonCapitoleId <> '" + gvCapitole.SelectedValue + "'";
        int vVerificareExista = Convert.ToInt32(ManipuleazaBD.fRezultaUnString(vInterogareExista, "nr", Convert.ToInt16(Session["SESan"])));
        Int64 vIdSablonCapitole = Convert.ToInt64(gvCapitole.SelectedValue);
        if (ViewState["tip"].ToString() == "m")
        {
            if (vVerificareExista == 0)
            {

                vInterogare = "UPDATE sabloaneCapitole SET an='" + tbAn.Text + "', capitol='" + tbCapitol.Text + "' , codRand='" + tbCodRand.Text + "' ,formula='" + tbFormula.Text + "' , denumire1=N'" + tbCol1.Text + "', denumire2=N'" + tbCol2.Text + "',denumire3=N'" + tbCol3.Text + "',denumire4=N'" + tbCol4.Text + "',denumire5=N'" + tbCol5.Text + "' WHERE sablonCapitoleId = '" + vIdSablonCapitole + "'";
            }
            else
            {
                VerDacaExistaCodRand.ErrorMessage = "Exista deja o inregistrare avand aceste caracteristici!";
                VerDacaExistaCodRand.IsValid = false;
                return;
            }
        }
        ManipuleazaBD.fManipuleazaBD(vInterogare, Convert.ToInt16(Session["SESan"]));
        gvCapitole.DataBind();
        pnAdaugaSabloneCapitole.Visible = false;
        pnCautare.Visible = true;
        pnGrid.Visible = true;
        pnButoanePrincipale.Visible = true;

    }
    protected void btAnuleazaSalvarea_Click(object sender, EventArgs e)
    {
        pnAdaugaSabloneCapitole.Visible = false;
        pnCautare.Visible = true;
        pnGrid.Visible = true;
        pnButoanePrincipale.Visible = true;
    }
    protected void btModifica_Click(object sender, EventArgs e)
    {
        ViewState["tip"] = "m";
        string vInterogareSelect = "SELECT * FROM sabloaneCapitole WHERE sablonCapitoleId =  " + gvCapitole.SelectedValue;
        string strConn = ConfigurationManager.ConnectionStrings["TNTRegistruAgricolConnectionString"].ConnectionString;
        SqlConnection con = new SqlConnection(strConn);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = vInterogareSelect;
        con.Open();
        try
        {
            SqlDataReader vLinieDeModificat = cmd.ExecuteReader();
            if (vLinieDeModificat.Read())
            {
                tbCol1.Text = vLinieDeModificat["denumire1"].ToString();
                tbCol2.Text = vLinieDeModificat["denumire2"].ToString();
                tbCol3.Text = vLinieDeModificat["denumire3"].ToString();
                tbCol4.Text = vLinieDeModificat["denumire4"].ToString();
                tbCol5.Text = vLinieDeModificat["denumire5"].ToString();
                tbAn.Text = vLinieDeModificat["an"].ToString();
                tbCapitol.Text = vLinieDeModificat["capitol"].ToString();
                tbFormula.Text = vLinieDeModificat["formula"].ToString();
                tbCodRand.Text = vLinieDeModificat["codRand"].ToString();
            }
        }
        catch { }
        pnAdaugaSabloneCapitole.Visible = true;
        pnCautare.Visible = false;
        pnGrid.Visible = false;
        pnButoanePrincipale.Visible = false;
        btSalveazaSablon.Text = "modifică şablonul";

    }
    protected void btSterge_Click(object sender, EventArgs e)
    {
        if (gvCapitole.SelectedValue != null)
        {
            ManipuleazaBD.fManipuleazaBD("DELETE FROM [sabloaneCapitole] WHERE [sablonCapitoleId] = '" + gvCapitole.SelectedValue.ToString() + "'", Convert.ToInt16(Session["SESan"]));
            gvCapitole.DataBind();
        }
    }
    protected void btAdauga_Click(object sender, EventArgs e)
    {
        ViewState["tip"] = "a";
        tbAn.Text = Convert.ToInt32(DateTime.Now.Date.Year).ToString();
        tbCol1.Text = "";
        tbCol2.Text = "";
        tbCol3.Text = "";
        tbCol4.Text = "";
        tbCol5.Text = "";
        tbCodRand.Text = "";
        tbCapitol.Text = "";
        tbCodRand.Text = "";
        tbFormula.Text = "";
        pnAdaugaSabloneCapitole.Visible = true;
        pnCautare.Visible = false;
        pnGrid.Visible = false;
        pnButoanePrincipale.Visible = false;
        btSalveazaSablon.Text = "adaugă un şablon";
    }
    protected void VerDacaExiataCodRand_ServerValidate(object source, ServerValidateEventArgs args)
    {
        try
        {
            if (ManipuleazaBD.fVerificaExistenta("sabloaneCapitole", "an", tbCodRand.Text, Convert.ToInt16(Session["SESan"])) == 1)
            { args.IsValid = false; }
        }
        catch
        {
            args.IsValid = false;
        }
    }
}
