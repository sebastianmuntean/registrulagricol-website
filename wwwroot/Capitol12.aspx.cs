﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class capitol12test : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DBConnections connection = new DBConnections(Convert.ToInt16(Session["SESan"]));

        SqlDataSourceAfisMembrii.ConnectionString = connection.Create();

        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "capitol12", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
            LoadCertificate();
        }

    }
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["SESgospodarieId"] == null)
        {
            HttpCookie vCookie = Request.Cookies["COOKTNT"];
            vCookie = CriptareCookie.DecodeCookie(vCookie);
            if (vCookie != null)
            {
                if (vCookie["COOKgospodarieId"] != null)
                {
                    Session["SESgospodarieId"] = vCookie["COOKgospodarieId"].ToString();
                }
                else if (Request.Url.AbsolutePath.IndexOf("Gospodarii.aspx") == -1 && Session["SESgospodarieId"] == null)
                    Response.Redirect("~/Gospodarii.aspx");
            }
        }
    }
    private void LoadCertificate()
    {
        int gospodarieId = Convert.ToInt32(Session["SESGospodarieId"]);
        int unitateId = Convert.ToInt32(Session["SESUnitateId"]);
        int an = Convert.ToInt32(Session["SESAn"]);
        certificateGridView.DataSource = CertificateProducator.GetCertificate(unitateId, gospodarieId, an);
        certificateGridView.DataBind();

        if(certificateGridView.Rows.Count == 0)
        {

            certificateGridView.EmptyDataText = "Nu sunt inregistrari";
            certificateGridView.Visible = true;
        }
    }
    protected void certificateGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(certificateGridView, e, this);
        e.Row.Cells[0].Visible = false;
    }
    protected void certificateGridView_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (certificateGridView.SelectedIndex == -1)
        {
            modificaAtestatButton.Visible = false;
            stergeAtestatButton.Visible = false;
            return;
        }
        modificaAtestatButton.Visible = true;
        stergeAtestatButton.Visible = true;
        Session["CertificatId"] = certificateGridView.SelectedValue;
    }
    protected void adaugaAtestatButton_Click(object sender, EventArgs e)
    {
        Session["actiune"] = "adauga";
        tbSerieCP.Text = "";
        tbNrCp.Text = "";
        tbNrCertificat.Text = "";
        tbData.Text = DateTime.Now.ToString("dd.MM.yyyy");
        dataAvizTextBox.Text = DateTime.Now.ToString("dd.MM.yyyy");
        dataAtestatTextBox.Text = DateTime.Now.ToString("dd.MM.yyyy");
        lblCertProdId.Text = "-1";

        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        if (Session["SESgospodarieId"] != null)
        {
            pnlAfis.Visible = false;
            pnlAdaugaModifica.Visible = true;
            ViewState["tip"] = "a";
            lblEroare1.Visible = false;
            lblEroare1.Text = "";
            pnListaButoanePrincipale.Visible = false;
            updateGridProduse();
        }
        else
        {
            lblEroare1.Visible = true;
            lblEroare1.Text = "Selectaţi o gospodărie !";
        }
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        SqlCommand vCmd1 = new SqlCommand();
        vCmd1.Connection = vCon;
        SqlTransaction vTrans = ManipuleazaBD.DeschideTranzactie(vCon);
        vCmd.Transaction = vTrans;
        vCmd1.Transaction = vTrans;
        try
        {
            if (lblCertProdId.Text.Equals("-1"))
            {
                vCmd.CommandText = "INSERT INTO certProd (certProdData,unitateId, gospodarieId, an, certProdNr,  certificatProducatorSerie, certificatProducatorNr) VALUES        (convert(datetime,'" + DateTime.Now.ToString("dd.MM.yyyy") + "',104),'" + Session["SESunitateId"].ToString() + "','" + Session["SESgospodarieId"].ToString() + "','" + Session["SESan"].ToString() + "','" + tbNrCertificat.Text + "','" + tbSerieCP.Text + "','" + tbNrCp.Text + "');SELECT SCOPE_IDENTITY() AS [SCOPE_IDENTITY]";
                vCmd.CommandText = vCmd.CommandText.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
                lblCertProdId.Text = vCmd.ExecuteScalar().ToString();
                //preluare din registrul agricol

                vCmd.CommandText = "SELECT membri.capitolId, membri.unitateId, membri.gospodarieId, membri.an, membri.nume, membri.codRand, membri.cnp, membri.codSex, membri.codRudenie, membri.denumireRudenie, membri.dataNasterii, membri.mentiuni, gospodarii.judet, gospodarii.localitate, gospodarii.strada, gospodarii.nr FROM membri INNER JOIN gospodarii ON membri.gospodarieId = gospodarii.gospodarieId WHERE (membri.gospodarieId = '" + Session["SESgospodarieId"].ToString() + "') ORDER BY codRudenie";

                SqlDataReader vSdR = vCmd.ExecuteReader();
                while (vSdR.Read())
                {
                    string vTip = "-1";
                    if (vSdR["codRudenie"].ToString().Equals("1"))
                        vTip = "0";
                    else vTip = "1";
                    string vNume = vSdR["nume"].ToString();//, vPrenume = "";
                    //try { vPrenume = vNume.Substring(vNume.IndexOf(" ") + 1, vNume.Length - vNume.IndexOf(" ") - 1); }
                    //catch {}
                    vCmd1.CommandText = "INSERT INTO membriCp (membriCpNume, membriCpJudet, membriCpLocalitate, membriCpStrada, membriCpNr, membriCpActIdenSerie, membriCpActIdentNr, membriCpCNP, membriCpTip,unitateId, certProdId) VALUES ('" + vNume + "','" + vSdR["judet"].ToString() + "','" + vSdR["localitate"].ToString() + "','" + vSdR["strada"].ToString() + "','" + vSdR["nr"].ToString() + "','', '','" + vSdR["cnp"].ToString() + "','" + vTip + "','" + Session["SESunitateId"].ToString() + "','" + lblCertProdId.Text + "')";
                    vCmd1.CommandText = vCmd1.CommandText.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
                    vCmd1.ExecuteNonQuery();


                }
                vSdR.Close();
            }
            vTrans.Commit();
        }
        catch { vTrans.Rollback(); }
        ManipuleazaBD.InchideConexiune(vCon);
        certificateGridView.DataBind();
    }

    protected void modificaAtestatButton_Click(object sender, EventArgs e)
    {
        LoadCertificat();
    }

    protected void listaGospodariiButton_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/gospodarii.aspx");
    }
    protected void stergeAtestatButton_Click(object sender, EventArgs e)
    {
        DeleteCertificat();
        LoadCertificate();
        modificaAtestatButton.Visible = false;
        stergeAtestatButton.Visible = false;
    }
    private void DeleteCertificat()
    {
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        SqlTransaction vTrans = ManipuleazaBD.DeschideTranzactie(vCon);
        vCmd.Connection = vCon;
        vCmd.Transaction = vTrans;
        try
        {
            vCmd.CommandText = "DELETE FROM membriCp WHERE (unitateId = " + Session["SESunitateId"].ToString() + ") AND (certProdId = " + certificateGridView.SelectedValue + ")";
            vCmd.ExecuteNonQuery();
            vCmd.CommandText = "DELETE FROM produsCp WHERE  (unitateId = " + Session["SESunitateId"].ToString() + ") AND (certProdId = " + certificateGridView.SelectedValue + ")";
            vCmd.ExecuteNonQuery();
            vCmd.CommandText = "DELETE FROM produsComercializat WHERE (unitateId = " + Session["SESunitateId"].ToString() + ") AND (certProdId = " + certificateGridView.SelectedValue + ")";
            vCmd.ExecuteNonQuery();
            vCmd.CommandText = "DELETE FROM certProd WHERE (unitateId = " + Session["SESunitateId"].ToString() + ") AND (certProdId = " + certificateGridView.SelectedValue + ")";
            vCmd.ExecuteNonQuery();
            vTrans.Commit();
        }
        catch { vTrans.Rollback(); }
        ManipuleazaBD.InchideConexiune(vCon);

    }

    protected void tiparireButton_Click(object sender, EventArgs e)
    {
        ResponseHelper.Redirect("~/printCapitolul12.aspx?codCapitol=12", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
    }

    protected void tiparireButtonXls_Click(object sender, EventArgs e)
    {
        ResponseHelper.Redirect("~/printCapitolul12Xls.aspx?codCapitol=12", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
    }

    protected void gvMembrii_PreRender(object sender, EventArgs e)
    {
        if (gvMembrii.SelectedValue == null)
        {
            btModificaMembri.Visible = false;
            btStergeMembri.Visible = false;

        }
    }
    protected void gvMembrii_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(gvMembrii, e, this);
    }
    protected void gvMembrii_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (gvMembrii.SelectedValue == null)
        {
            btModificaMembri.Visible = false;
            btStergeMembri.Visible = false;
        }
        else
        {
            btModificaMembri.Visible = true;
            btStergeMembri.Visible = true;
        }
    }
    protected void btModificaMembri_Click(object sender, EventArgs e)
    {
        pnlAdaugaModificaMembrii.Visible = true;
        pnlAfisMembrii.Visible = false;
        pnlButoaneSalveazaInapoi.Visible = false;
        pnlAdaugaModificaH.Visible = false;
        ViewState["tipMembrii"] = "m";
        lblEroare1.Visible = false;
        lblEroare1.Text = "";
        pnlProduse.Visible = false;


        SqlConnection vCon =  ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;

        vCmd.CommandText = "SELECT     membriCpNume, membriCpJudet, membriCpLocalitate, membriCpStrada, membriCpNr, membriCpActIdenSerie, membriCpActIdentNr, membriCpCNP, membriCpTip FROM            membriCp WHERE        (membriCpId = " + gvMembrii.SelectedValue + ") AND (unitateId = " + Session["SESunitateId"].ToString() + ") AND (certProdId = " + lblCertProdId.Text + ")";
        SqlDataReader vSdR = vCmd.ExecuteReader();
        while (vSdR.Read())
        {
            ddlTip.SelectedValue = vSdR["membriCpTip"].ToString();
            tbNume.Text = vSdR["membriCpNume"].ToString();
            tbJudet.Text = vSdR["membriCpJudet"].ToString();
            tbLocalitate.Text = vSdR["membriCpLocalitate"].ToString();
            tbStrada.Text = vSdR["membriCpStrada"].ToString();
            tbNr.Text = vSdR["membriCpNr"].ToString();
            tbSerie.Text = vSdR["membriCpActIdenSerie"].ToString();
            tbNrActIdentitate.Text = vSdR["membriCpActIdentNr"].ToString();
            tbCNP.Text = vSdR["membriCpCNP"].ToString();

        }
        vSdR.Close();
        ManipuleazaBD.InchideConexiune(vCon);

    }
    protected void btStergeMembri_Click(object sender, EventArgs e)
    {
        SqlConnection vCon =  ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "DELETE FROM membriCp WHERE        (membriCpId =" + gvMembrii.SelectedValue + ") AND (unitateId = " + Session["SESunitateId"].ToString() + ") AND (certProdId = " + lblCertProdId.Text + ")";
        vCmd.ExecuteNonQuery();
        gvMembrii.DataBind();

        ManipuleazaBD.InchideConexiune(vCon);
    }
    protected void btAdaugaMembri_Click(object sender, EventArgs e)
    {
        ddlTip.SelectedValue = "0";
        tbNume.Text = "";
        tbJudet.Text = "";
        tbLocalitate.Text = "";
        tbStrada.Text = "";
        tbNr.Text = "";
        tbSerie.Text = "";
        tbNrActIdentitate.Text = "";
        tbCNP.Text = "";

        pnlAdaugaModificaMembrii.Visible = true;
        pnlAfisMembrii.Visible = false;
        pnlButoaneSalveazaInapoi.Visible = false;
        pnlAdaugaModificaH.Visible = false;
        ViewState["tipMembrii"] = "a";
        lblEroare1.Visible = false;
        lblEroare1.Text = "";
        pnlProduse.Visible = false;

    }
    protected void btModificaPreluareMembri_Click(object sender, EventArgs e)
    {
        SqlConnection vCon =  ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd1 = new SqlCommand();
        SqlCommand vCmd2 = new SqlCommand();
        vCmd1.Connection = vCon;
        vCmd2.Connection = vCon;
        vCmd2.CommandText = "SELECT membri.capitolId, membri.unitateId, membri.gospodarieId, membri.an, membri.nume, membri.codRand, membri.cnp, membri.codSex, membri.codRudenie, membri.denumireRudenie, membri.dataNasterii, membri.mentiuni, gospodarii.judet, gospodarii.localitate, gospodarii.strada, gospodarii.nr FROM membri INNER JOIN gospodarii ON membri.gospodarieId = gospodarii.gospodarieId WHERE (gospodarieId = '" + Session["SESgospodarieId"].ToString() + "') ORDER BY codRudenie";
        SqlDataReader vTabel = vCmd2.ExecuteReader();
        while (vTabel.Read())
        {
            string vNume = vTabel["nume"].ToString(), vPrenume = "";
            try { vPrenume = vNume.Substring(vNume.IndexOf(" ") + 1, vNume.Length - vNume.IndexOf(" ") - 1); }
            catch { }
            string vTip = "1";
            if (vTabel["codRudenie"].ToString() != "1")
                vTip = "2";
            vCmd1.CommandText = "INSERT INTO membriCp (membriCpNume,membriCpPrenume, membriCpJudet, membriCpLocalitate, membriCpStrada, membriCpNr, membriCpActIdenSerie, membriCpActIdentNr, membriCpCNP, membriCpTip,unitateId, certProdId) VALUES ('" + vNume + "','" + vPrenume + "','" + vTabel["judet"].ToString() + "','" + vTabel["localitate"].ToString() + "','" + vTabel["strada"].ToString() + "','" + vTabel["nr"].ToString() + "',','','" + vTabel["cnp"].ToString() + "','" + vTip + "','" + Session["SESunitateId"].ToString() + "','" + lblCertProdId.Text + "')";
            vCmd1.CommandText = vCmd1.CommandText.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
            vCmd1.ExecuteNonQuery();
        }
        vTabel.Close();
        ManipuleazaBD.InchideConexiune(vCon);
    }
    protected void btSalveazaM_Click(object sender, EventArgs e)
    {
        lblEroare1.Visible = false;
        lblEroare1.Text = "";
        CNP vCnp = new CNP(tbCNP.Text);
        if (vCnp.IsValidB())
        {
            SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
            SqlCommand vCmd = new SqlCommand();
            vCmd.Connection = vCon;
            SqlTransaction vTrans = ManipuleazaBD.DeschideTranzactie(vCon);
            vCmd.Transaction = vTrans;

            try
            {
                if (ViewState["tipMembrii"].Equals("a"))
                {
                    if (ddlTip.SelectedValue.Equals("0"))
                    {
                        vCmd.CommandText = "SELECT        COUNT(*) AS Expr1 FROM            membriCp WHERE        (certProdId = " + lblCertProdId.Text + ") AND (unitateId =" + Session["SESunitateId"].ToString() + ") AND (membriCpTip = 0)";
                        string vTmp = vCmd.ExecuteScalar().ToString();
                        if (vTmp.Equals("0"))
                        {
                            vCmd.CommandText = "INSERT INTO membriCp (membriCpNume, membriCpJudet, membriCpLocalitate, membriCpStrada, membriCpNr, membriCpActIdenSerie, membriCpActIdentNr, membriCpCNP, membriCpTip,unitateId, certProdId) VALUES        ('" + tbNume.Text + "','" + tbJudet.Text + "','" + tbLocalitate.Text + "','" + tbStrada.Text + "','" + tbNr.Text + "','" + tbSerieCP.Text + "','" + tbNrActIdentitate.Text + "','" + tbCNP.Text + "','" + ddlTip.SelectedValue + "','" + Session["SESunitateId"].ToString() + "','" + lblCertProdId.Text + "')";
                            vCmd.CommandText = vCmd.CommandText.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş").Replace("Ş","Ş");
                            vCmd.ExecuteNonQuery();
                        }
                        else
                        {
                            lblEroare1.Visible = true;
                            lblEroare1.Text = "Exista deja un titular !";
                            tbNr.Focus();
                        }
                    }
                    if (ddlTip.SelectedValue.Equals("1"))
                    {
                        vCmd.CommandText = "SELECT        COUNT(*) AS Expr1 FROM            membriCp WHERE        (certProdId = " + lblCertProdId.Text + ") AND (unitateId =" + Session["SESunitateId"].ToString() + ") AND (membriCpTip = 1)";
                        string vTmp = vCmd.ExecuteScalar().ToString();
                        if (!vTmp.Equals("3"))
                        {
                            vCmd.CommandText = "INSERT INTO membriCp (membriCpNume, membriCpJudet, membriCpLocalitate, membriCpStrada, membriCpNr, membriCpActIdenSerie, membriCpActIdentNr, membriCpCNP, membriCpTip,unitateId, certProdId) VALUES        ('" + tbNume.Text + "','" + tbJudet.Text + "','" + tbLocalitate.Text + "','" + tbStrada.Text + "','" + tbNr.Text + "','" + tbSerieCP.Text + "','" + tbNrActIdentitate.Text + "','" + tbCNP.Text + "','" + ddlTip.SelectedValue + "','" + Session["SESunitateId"].ToString() + "','" + lblCertProdId.Text + "')";
                            vCmd.CommandText = vCmd.CommandText.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
                            vCmd.ExecuteNonQuery();
                        }
                        else
                        {
                            lblEroare1.Visible = true;
                            lblEroare1.Text = "Exista deja 3 membrii !";
                            tbNr.Focus();
                        }
                    }
                }
                if (ViewState["tipMembrii"].Equals("m"))
                {
                    vCmd.CommandText = "UPDATE       membriCp SET  membriCpNume ='" + tbNume.Text + "',membriCpPrenume = '',membriCpJudet ='" + tbJudet.Text + "', membriCpLocalitate ='" + tbLocalitate.Text + "', membriCpStrada ='" + tbStrada.Text + "', membriCpNr ='" + tbNr.Text + "', membriCpActIdenSerie ='" + tbSerie.Text + "', membriCpActIdentNr ='" + tbNrActIdentitate.Text + "', membriCpCNP ='" + tbCNP.Text + "', membriCpTip ='" + ddlTip.SelectedValue + "' WHERE        (membriCpId ='" + gvMembrii.SelectedValue + "') AND (unitateId = " + Session["SESunitateId"].ToString() + ") AND (certProdId ='" + lblCertProdId.Text + "')";
                    vCmd.CommandText = vCmd.CommandText.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
                    vCmd.ExecuteNonQuery();
                }
                vTrans.Commit();
            }
            catch { vTrans.Rollback(); }
            ManipuleazaBD.InchideConexiune(vCon);
            if (!lblEroare1.Visible)
            {
                pnlAdaugaModificaMembrii.Visible = false;
                pnlAfisMembrii.Visible = true;
                pnlButoaneSalveazaInapoi.Visible = true;
                pnlAdaugaModificaH.Visible = true;
                pnlProduse.Visible = true;
                gvMembrii.DataBind();
            }
        }
        else
        {
            lblEroare1.Visible = true;
            lblEroare1.Text = "CNP invalid !";
            tbNr.Focus();
        }
    }
    protected void btInapoiM_Click(object sender, EventArgs e)
    {
        //if (!lblEroare1.Visible)
        {
            pnlAdaugaModificaMembrii.Visible = false;
            pnlAfisMembrii.Visible = true;
            pnlButoaneSalveazaInapoi.Visible = true;
            pnlAdaugaModificaH.Visible = true;
            pnlProduse.Visible = true;
            lblEroare1.Visible = false;
            lblEroare1.Text = "";
        }
    }
    protected void btDa_OnClick(object sender, EventArgs e)
    {
        pnlDa.Visible = true;
        pnlPopUp.Visible = false;
        pnlButoanePopup.Visible = false;
        //populez gridu
        SqlConnection vCon =  ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        List<List<string>> vToate = new List<List<string>>();

        vCmd.CommandText = "SELECT      produsCpTip,  produsCpDenumire, produsCpProductieComercializata,produsCpIndex FROM            produsCp  WHERE        (unitateId = " + Session["SESunitateId"].ToString() + ") AND (certProdId = " + lblCertProdId.Text + ")";
        SqlDataReader vSdR = vCmd.ExecuteReader();
        while (vSdR.Read())
        {
            List<string> vElem = new List<string>();
            vElem.Add(vSdR["produsCpDenumire"].ToString());
            vElem.Add(Convert.ToDecimal(vSdR["produsCpProductieComercializata"].ToString()).ToString("N"));
            vElem.Add(vSdR["produsCpIndex"].ToString());
            vElem.Add(vSdR["produsCpTip"].ToString());
            vToate.Add(vElem);
        }

        ManipuleazaBD.InchideConexiune(vCon);
        gvProdusePopup.DataSource = vToate;
        gvProdusePopup.DataBind();
    }
    protected void btNu_OnClick(object sender, EventArgs e)
    {
        pnlPopupAdaugaProduseComercializate.Visible = false;
        pnlAfis.Visible = true;
        pnlAdaugaModifica.Visible = false;
        certificateGridView.DataBind();
    }
    protected void gvProdusePopup_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            TextBox tbAnul1 = (TextBox)e.Row.FindControl("tbAnul1");
            TextBox tbAnul2 = (TextBox)e.Row.FindControl("tbAnul2");
            //poate exista in baza de date 
            SqlConnection vCon =  ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
            SqlCommand vCmd = new SqlCommand();
            vCmd.Connection = vCon;

            vCmd.CommandText = "SELECT        TOP (1) produsComerAnT34, produsComerAnT12  FROM            produsComercializat WHERE        (certProdId = " + lblCertProdId.Text + ") AND (unitateId = " + Session["SESunitateId"].ToString() + ")";
            SqlDataReader vSdR = vCmd.ExecuteReader();
            if (vSdR.Read())
            {
                tbAnul1.Text = vSdR["produsComerAnT34"].ToString();
                tbAnul2.Text = vSdR["produsComerAnT12"].ToString();
            }
            else
            {
                tbAnul2.Text = tbAnul1.Text = DateTime.Now.Year.ToString();
            }
            ManipuleazaBD.InchideConexiune(vCon);

        }
        if (ViewState["tip"].Equals("m"))
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblIndex = (Label)e.Row.FindControl("lblIndex");
                Label lblTip = (Label)e.Row.FindControl("lblTip");
                TextBox tbT1 = (TextBox)e.Row.FindControl("tbT1");
                TextBox tbT2 = (TextBox)e.Row.FindControl("tbT2");
                TextBox tbT3 = (TextBox)e.Row.FindControl("tbT3");
                TextBox tbT4 = (TextBox)e.Row.FindControl("tbT4");



                SqlConnection vCon =  ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
                SqlCommand vCmd = new SqlCommand();
                vCmd.Connection = vCon;

                vCmd.CommandText = "SELECT        produsComerT3, produsComerT4, produsComerT1, produsComerT2 FROM            produsComercializat WHERE        (certProdId = " + lblCertProdId.Text + ") AND (unitateId = " + Session["SESunitateId"].ToString() + ")AND (produsComerIndex = " + lblIndex.Text + ") AND (produsComerTip = " + lblTip.Text + ")";
                SqlDataReader vSdR = vCmd.ExecuteReader();
                if (vSdR.Read())
                {
                    if (Convert.ToDecimal(vSdR["produsComerT1"].ToString()) != 0)
                        tbT1.Text = Convert.ToDecimal(vSdR["produsComerT1"].ToString()).ToString("N");
                    if (Convert.ToDecimal(vSdR["produsComerT2"].ToString()) != 0)
                        tbT2.Text = Convert.ToDecimal(vSdR["produsComerT2"].ToString()).ToString("N");
                    if (Convert.ToDecimal(vSdR["produsComerT3"].ToString()) != 0)
                        tbT3.Text = Convert.ToDecimal(vSdR["produsComerT3"].ToString()).ToString("N");
                    if (Convert.ToDecimal(vSdR["produsComerT4"].ToString()) != 0)
                        tbT4.Text = Convert.ToDecimal(vSdR["produsComerT4"].ToString()).ToString("N");
                }
                vSdR.Close();
                ManipuleazaBD.InchideConexiune(vCon);
            }
        }
    }
    protected void btSalveazaPopup_OnClick(object sender, EventArgs e)
    {
        string vEroare = valideazaGvProdusePopup();
        if (vEroare.Length == 0)
        {
            SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
            SqlCommand vCmd = new SqlCommand();
            SqlTransaction vTrans = ManipuleazaBD.DeschideTranzactie(vCon);
            vCmd.Connection = vCon;
            vCmd.Transaction = vTrans;
            try
            {
                TextBox tbAnual1 = (TextBox)gvProdusePopup.HeaderRow.FindControl("tbAnul1");
                TextBox tbAnual2 = (TextBox)gvProdusePopup.HeaderRow.FindControl("tbAnul2");
                //prima data sterg toate produsele 
                vCmd.CommandText = "DELETE FROM produsComercializat  WHERE        (certProdId = " + lblCertProdId.Text + ") AND (unitateId = " + Session["SESunitateId"].ToString() + ")";
                vCmd.ExecuteNonQuery();
                //dupa care adaug
                for (int i = 0; i < gvProdusePopup.Rows.Count; i++)
                {

                    TextBox tbT1 = (TextBox)gvProdusePopup.Rows[i].FindControl("tbT1");
                    TextBox tbT2 = (TextBox)gvProdusePopup.Rows[i].FindControl("tbT2");
                    TextBox tbT3 = (TextBox)gvProdusePopup.Rows[i].FindControl("tbT3");
                    TextBox tbT4 = (TextBox)gvProdusePopup.Rows[i].FindControl("tbT4");
                    Label lblDisponibil = (Label)gvProdusePopup.Rows[i].FindControl("lblDisponibil");
                    Label lblIndex = (Label)gvProdusePopup.Rows[i].FindControl("lblIndex");
                    Label lblTip = (Label)gvProdusePopup.Rows[i].FindControl("lblTip");
                    TextBox tbProdusul = (TextBox)gvProdusePopup.Rows[i].FindControl("tbProdusul");

                    decimal vT1 = 0, vT2 = 0, vT3 = 0, vT4 = 0;
                    try { vT1 += (Convert.ToDecimal(tbT1.Text)); }
                    catch { }
                    try { vT2 = (Convert.ToDecimal(tbT2.Text)); }
                    catch { }
                    try { vT3 = (Convert.ToDecimal(tbT3.Text)); }
                    catch { }
                    try { vT4 = (Convert.ToDecimal(tbT4.Text)); }
                    catch { }

                    if (tbProdusul.Text.Length > 0 && (vT1 + vT2 + vT3 + vT4) != 0)
                    {
                        vCmd.CommandText = "INSERT INTO produsComercializat (certProdId, unitateId, produsComerDenumire, produsComerAnT34, produsComerT3, produsComerT4, produsComerAnT12, produsComerT1, produsComerT2,produsComerIndex,produsComerTip) VALUES        ('" + lblCertProdId.Text + "','" + Session["SESunitateId"].ToString() + "','" + tbProdusul.Text + "','" + tbAnual1.Text + "','" + vT3.ToString().Replace(",", ".") + "','" + vT4.ToString().Replace(",", ".") + "','" + tbAnual2.Text + "','" + vT1.ToString().Replace(",", ".") + "','" + vT2.ToString().Replace(",", ".") + "','" + lblIndex.Text + "','" + lblTip.Text + "')";
                        vCmd.CommandText = vCmd.CommandText.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
                        vCmd.ExecuteNonQuery();
                    }
                }
                vTrans.Commit();
            }
            catch { vTrans.Rollback(); }

            ManipuleazaBD.InchideConexiune(vCon);
            pnlPopupAdaugaProduseComercializate.Visible = false;
            pnlAfis.Visible = true;
            pnlAdaugaModifica.Visible = false;
            certificateGridView.DataBind();
            pnlDa.Visible = false;
            pnlPopUp.Visible = true;
            pnlButoanePopup.Visible = true;

        }
        else
        {
            valCustom.IsValid = false;
            valCustom.ErrorMessage = "Atenţie pe trimestre ai o cantitate mai mare ca şi disponibil la produsele : " + vEroare.Substring(0, vEroare.Length - 1) + " !";
        }
    }
    private string valideazaGvProdusePopup()
    {
        string vError = "";
        for (int i = 0; i < gvProdusePopup.Rows.Count; i++)
        {
            TextBox tbT1 = (TextBox)gvProdusePopup.Rows[i].FindControl("tbT1");
            TextBox tbT2 = (TextBox)gvProdusePopup.Rows[i].FindControl("tbT2");
            TextBox tbT3 = (TextBox)gvProdusePopup.Rows[i].FindControl("tbT3");
            TextBox tbT4 = (TextBox)gvProdusePopup.Rows[i].FindControl("tbT4");
            Label lblDisponibil = (Label)gvProdusePopup.Rows[i].FindControl("lblDisponibil");
            TextBox tbProdusul = (TextBox)gvProdusePopup.Rows[i].FindControl("tbProdusul");

            decimal vToate = 0;
            try { vToate += (Convert.ToDecimal(tbT1.Text)); }
            catch { }
            try { vToate += (Convert.ToDecimal(tbT2.Text)); }
            catch { }
            try { vToate += (Convert.ToDecimal(tbT3.Text)); }
            catch { }
            try { vToate += (Convert.ToDecimal(tbT4.Text)); }
            catch { }

            if (Convert.ToDecimal(lblDisponibil.Text) < vToate)
                vError += tbProdusul.Text + ",";
        }
        return vError;
    }
    protected void btInapoiPopup_OnClick(object sender, EventArgs e)
    {
        pnlPopupAdaugaProduseComercializate.Visible = false;
        pnlAfis.Visible = true;
        pnlAdaugaModifica.Visible = false;
        certificateGridView.DataBind();
        pnlDa.Visible = false;
        pnlPopUp.Visible = true;
        pnlButoanePopup.Visible = true;
    }
    private void LoadCertificat()
    {
        Session["actiune"] = "modifica";
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        if (Session["SESgospodarieId"] != null)
        {
            lblCertProdId.Text = certificateGridView.SelectedValue.ToString();
            //lblCertProdId.Text = Session["CertificatId"].ToString();
            pnlAfis.Visible = false;
            pnlAdaugaModifica.Visible = true;
            pnListaButoanePrincipale.Visible = false;
            ViewState["tip"] = "m";
            lblEroare1.Visible = false;
            lblEroare1.Text = "";
            updateGridProduse();

            SqlConnection vCon =  ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
            SqlCommand vCmd = new SqlCommand();
            vCmd.Connection = vCon;

            vCmd.CommandText = "SELECT        certProdNr, certProdData, certificatProducatorSerie, certificatProducatorNr,nrAtestat,nrAviz,dataAtestat,dataAviz,serieAtestat  FROM            certProd WHERE        (certProdId = " + lblCertProdId.Text + ") AND (unitateId = " + Session["SESunitateId"].ToString() + ") AND (an = " + Session["SESan"].ToString() + ")";
            SqlDataReader vSdR = vCmd.ExecuteReader();

            while (vSdR.Read())
            {
                tbSerieCP.Text = vSdR["certificatProducatorSerie"].ToString();
                tbNrCp.Text = vSdR["certificatProducatorNr"].ToString();
                tbNrCertificat.Text = vSdR["certProdNr"].ToString();
                tbData.Text = Convert.ToDateTime(vSdR["certProdData"].ToString()).ToString("dd.MM.yyyy");
                numarAtestatTextBox.Text = vSdR["nrAtestat"].ToString();
                numarAvizTextBox.Text = vSdR["nrAviz"].ToString();
                dataAtestatTextBox.Text = Convert.ToDateTime(vSdR["dataAtestat"].ToString()).ToString("dd.MM.yyyy");
                dataAvizTextBox.Text = Convert.ToDateTime(vSdR["dataAviz"].ToString()).ToString("dd.MM.yyyy");
                serieAtestatTextBox.Text = vSdR["serieAtestat"].ToString();
            }
            ManipuleazaBD.InchideConexiune(vCon);
        }
        else
        {
            lblEroare1.Visible = true;
            lblEroare1.Text = "Selectaţi o gospodărie !";
        }
    }
    protected void updateGridProduse()
    {
        List<string> vElementeV = new List<string>();
        vElementeV.Add("Grâu (kg)");
        vElementeV.Add("Porumb (kg)");
        vElementeV.Add("Alte cereale (kg)");
        vElementeV.Add("Cartofi (kg)");
        vElementeV.Add("Legume total , de:");
        vElementeV.Add("tomate (kg)");
        vElementeV.Add("varză (kg)");
        vElementeV.Add("ardei (kg)");
        vElementeV.Add("ceapă (kg)");
        vElementeV.Add("alte leg. (kg)");
        vElementeV.Add("Pepeni (kg)");
        vElementeV.Add("Struguri (kg)");
        vElementeV.Add("Fructe (kg)");
        vElementeV.Add("Flori (buc.)");
        vElementeV.Add("Alte produse vegetale (kg)");
        vElementeV.Add("");
        vElementeV.Add("");
        vElementeV.Add("");
        vElementeV.Add("");
        vElementeV.Add("");
        vElementeV.Add("");
        vElementeV.Add("");
        vElementeV.Add("");
        vElementeV.Add("");
        vElementeV.Add("");
        //asta e alt grid       
        gvProduseV.DataSource = vElementeV;
        gvProduseV.DataBind();
        List<string> vElementeA = new List<string>();
        vElementeA.Add("Bovine");
        vElementeA.Add("Porcine");
        vElementeA.Add("Ovine şi caprine");
        vElementeA.Add("Cabaline");
        vElementeA.Add("Păsări");
        vElementeA.Add("Ouă (buc.)");
        vElementeA.Add("Lactate (L)");
        vElementeA.Add("Miere (kg)");
        vElementeA.Add("Carne (kg)");
        vElementeA.Add("Alte produse animale");
        vElementeA.Add("");
        vElementeA.Add("");
        vElementeA.Add("");
        vElementeA.Add("");
        vElementeA.Add("");
        vElementeA.Add("");
        vElementeA.Add("");
        vElementeA.Add("");
        vElementeA.Add("");
        vElementeA.Add("");
        gvProduseA.DataSource = vElementeA;
        gvProduseA.DataBind();

        SqlConnection vCon =  ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        SqlDataReader vSdR = null;

        if (ViewState["tip"].Equals("m"))
        {

            for (int i = 0; i < gvProduseV.Rows.Count; i++)
            {
                TextBox tbProdus = (TextBox)gvProduseV.Rows[i].FindControl("tbProdus");
                TextBox tbHa = (TextBox)gvProduseV.Rows[i].FindControl("tbHa");
                TextBox tbAri = (TextBox)gvProduseV.Rows[i].FindControl("tbAri");
                TextBox tbProductieEstimata = (TextBox)gvProduseV.Rows[i].FindControl("tbProductieEstimata");
                TextBox tbProductieComercializata = (TextBox)gvProduseV.Rows[i].FindControl("tbProductieComercializata");

                vCmd.CommandText = "SELECT produsCpDenumire, produsCpSuprafataAri, produsCpSuprafataHa,  produsCpProductieEstimata, produsCpProductieComercializata FROM            produsCp WHERE        (unitateId = " + Session["SESunitateId"].ToString() + ") AND (certProdId = " + lblCertProdId.Text + ") AND (produsCpIndex = " + i.ToString() + ") AND (produsCpTip = 0)";
                vSdR = vCmd.ExecuteReader();
                if (vSdR.Read())
                {
                    if (tbProdus.Text.Length == 0)
                        tbProdus.Text = vSdR["produsCpDenumire"].ToString();
                    if (Convert.ToDecimal(vSdR["produsCpSuprafataAri"].ToString()) != 0)
                        tbAri.Text = Convert.ToDecimal(vSdR["produsCpSuprafataAri"].ToString()).ToString("N");
                    if (Convert.ToDecimal(vSdR["produsCpSuprafataHa"].ToString()) != 0)
                        tbHa.Text = Convert.ToDecimal(vSdR["produsCpSuprafataHa"].ToString()).ToString("N");
                    if (Convert.ToDecimal(vSdR["produsCpProductieEstimata"].ToString()) != 0)
                        tbProductieEstimata.Text = Convert.ToDecimal(vSdR["produsCpProductieEstimata"].ToString()).ToString("N");
                    if (Convert.ToDecimal(vSdR["produsCpProductieComercializata"].ToString()) != 0)
                        tbProductieComercializata.Text = Convert.ToDecimal(vSdR["produsCpProductieComercializata"].ToString()).ToString("N");
                }
                vSdR.Close();
            }
            for (int i = 0; i < gvProduseA.Rows.Count; i++)
            {
                TextBox tbProdus = (TextBox)gvProduseA.Rows[i].FindControl("tbProdus");
                TextBox tbNr = (TextBox)gvProduseA.Rows[i].FindControl("tbNr");
                TextBox tbProductieEstimata = (TextBox)gvProduseA.Rows[i].FindControl("tbProductieEstimata");
                TextBox tbProductieComercializata = (TextBox)gvProduseA.Rows[i].FindControl("tbProductieComercializata");

                vCmd.CommandText = "SELECT        produsCpDenumire, produsCpSuprafataAri,produsCpNr,produsCpProductieEstimata, produsCpProductieComercializata FROM            produsCp WHERE        (unitateId = " + Session["SESunitateId"].ToString() + ") AND (certProdId = " + lblCertProdId.Text + ") AND (produsCpIndex = " + i.ToString() + ") AND (produsCpTip = 1)";

                vSdR = vCmd.ExecuteReader();
                if (vSdR.Read())
                {
                    if (tbProdus.Text.Length == 0)
                        tbProdus.Text = vSdR["produsCpDenumire"].ToString();
                    if (Convert.ToDecimal(vSdR["produsCpNr"].ToString()) != 0)
                        tbNr.Text = Convert.ToDecimal(vSdR["produsCpNr"].ToString()).ToString();
                    if (Convert.ToDecimal(vSdR["produsCpProductieEstimata"].ToString()) != 0)
                        tbProductieEstimata.Text = Convert.ToDecimal(vSdR["produsCpProductieEstimata"].ToString()).ToString("N");
                    if (Convert.ToDecimal(vSdR["produsCpProductieComercializata"].ToString()) != 0)
                        tbProductieComercializata.Text = Convert.ToDecimal(vSdR["produsCpProductieComercializata"].ToString()).ToString("N");
                }
                vSdR.Close();
            }
        }
        ManipuleazaBD.InchideConexiune(vCon);

    }

    string valideazaSalveaza()
    {
        string vError = "";
        try { Convert.ToDateTime(tbData.Text); }
        catch { vError = "Data este invalida !"; }

        if (tbSerieCP.Text.Length == 0)
            vError += "Serie certificat de producător nu trebuie sa fie gol ! ";
        if (tbNrCp.Text.Length == 0)
            vError += "Nr. nu trebuie sa fie gol ! ";
        if (tbNrCertificat.Text.Length == 0)
            vError += "Număr  nu trebuie sa fie gol ! ";
        if (numarAvizTextBox.Text.Length == 0)
            vError += "Completati numar aviz";
        if (numarAtestatTextBox.Text.Length == 0)
            vError += "Completati numar atestat";
        return vError;
    }

    protected void btSalveaza_Click(object sender, EventArgs e)
    {
        lblEroare1.Visible = false;
        lblEroare1.Text = "";
        string vEroare = valideazaSalveaza();

        if (vEroare.Length == 0)
        {
            if (gvMembrii.Rows.Count == 0)
            {
                lblEroare1.Visible = true;
                lblEroare1.Text = "Trebuie sa ai adaugat cel puţin un membru ca titular !";
            }
            if (!lblEroare1.Visible)
            {
                VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
                vVerificaSesiuni.VerificaSesiuniCookie();

                SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
                SqlCommand vCmd = new SqlCommand();
                vCmd.Connection = vCon;
                SqlTransaction vTrans = ManipuleazaBD.DeschideTranzactie(vCon);
                vCmd.Transaction = vTrans;

                try
                {
                    vCmd.CommandText = @"UPDATE       certProd SET  certProdNr ='" + tbNrCertificat.Text + "', certProdData =convert(datetime,'" + tbData.Text + "',104), certificatProducatorSerie ='" + tbSerieCP.Text + "', certificatProducatorNr ='" + tbNrCp.Text + "',nrAviz = '" + numarAvizTextBox.Text + @"',dataAviz = convert(datetime,'" + dataAvizTextBox.Text + "',104), nrAtestat = '" + numarAtestatTextBox.Text + "',serieAtestat = '"+serieAtestatTextBox.Text+"',dataAtestat = convert(datetime,'" + dataAtestatTextBox.Text + @"',104)
                    WHERE        (certProdId = '" + lblCertProdId.Text + "') AND (unitateId = '" + Session["SESunitateId"].ToString() + "') AND (an = '" + Session["SESan"].ToString() + "') and  gospodarieId =" + Session["SESgospodarieId"].ToString() + "";
                    vCmd.CommandText = vCmd.CommandText.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
                    vCmd.ExecuteNonQuery();

                    //prima data sterg produsele adaufate doar daca este pe modifica
                    if (ViewState["tip"].Equals("m"))
                    {
                        vCmd.CommandText = "DELETE FROM produsCp WHERE        (unitateId = '" + Session["SESunitateId"].ToString() + "') AND (certProdId = " + lblCertProdId.Text + ")";
                        vCmd.ExecuteNonQuery();
                    }

                    //inserez produsele adagate

                    for (int i = 0; i < gvProduseV.Rows.Count; i++)
                    {
                        TextBox tbProdus = (TextBox)gvProduseV.Rows[i].FindControl("tbProdus");
                        TextBox tbHa = (TextBox)gvProduseV.Rows[i].FindControl("tbHa");
                        TextBox tbAri = (TextBox)gvProduseV.Rows[i].FindControl("tbAri");
                        TextBox tbProductieEstimata = (TextBox)gvProduseV.Rows[i].FindControl("tbProductieEstimata");
                        TextBox tbProductieComercializata = (TextBox)gvProduseV.Rows[i].FindControl("tbProductieComercializata");
                        //doar daca a introdus ceva atunci sa salvez in tabele
                        if (tbProdus.Text.Length > 0)
                        {
                            decimal vHa = 0, vAri = 0;

                            try { vHa = Convert.ToDecimal(tbHa.Text); }
                            catch { }
                            try { vAri = Convert.ToDecimal(tbAri.Text); }
                            catch { }
                            decimal vEstimare = 0, vCome = 0;
                            try { vEstimare = Convert.ToDecimal(tbProductieEstimata.Text); }
                            catch { }
                            try { vCome = Convert.ToDecimal(tbProductieComercializata.Text); }
                            catch { }
                            //&& (vEstimare != 0 || vCome != 0)
                            if ((vHa != 0 || vAri != 0))
                            {
                                vCmd.CommandText = "INSERT INTO produsCp (unitateId, certProdId, produsCpDenumire, produsCpSuprafataAri, produsCpSuprafataHa, produsCpNr, produsCpProductieEstimata, produsCpProductieComercializata,produsCpIndex,produsCpTip) VALUES        ('" + Session["SESunitateId"].ToString() + "','" + lblCertProdId.Text + "','" + tbProdus.Text + "','" + vAri.ToString().Replace(",", ".") + "','" + vHa.ToString().Replace(",", ".") + "','0','" + vEstimare.ToString().Replace(",", ".") + "','" + vCome.ToString().Replace(",", ".") + "','" + i.ToString() + "','0')";
                                vCmd.CommandText = vCmd.CommandText.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
                                vCmd.ExecuteNonQuery();
                            }
                        }
                    }
                    for (int i = 0; i < gvProduseA.Rows.Count; i++)
                    {
                        TextBox tbProdus = (TextBox)gvProduseA.Rows[i].FindControl("tbProdus");
                        TextBox tbNr = (TextBox)gvProduseA.Rows[i].FindControl("tbNr");
                        TextBox tbProductieEstimata = (TextBox)gvProduseA.Rows[i].FindControl("tbProductieEstimata");
                        TextBox tbProductieComercializata = (TextBox)gvProduseA.Rows[i].FindControl("tbProductieComercializata");
                        //doar daca a introdus ceva atunci sa salvez in tabele
                        if (tbProdus.Text.Length > 0)
                        {
                            decimal vEstimare = 0, vCome = 0;
                            try { vEstimare = Convert.ToDecimal(tbProductieEstimata.Text); }
                            catch { }
                            try { vCome = Convert.ToDecimal(tbProductieComercializata.Text); }
                            catch { }
                            Int64 vNr = 0;
                            try { vNr = Convert.ToInt64(tbNr.Text); }
                            catch { }
                            if (vNr != 0)// && (vEstimare != 0 || vCome != 0)
                            {
                                vCmd.CommandText = "INSERT INTO produsCp (unitateId, certProdId, produsCpDenumire, produsCpSuprafataAri, produsCpSuprafataHa, produsCpNr, produsCpProductieEstimata, produsCpProductieComercializata,produsCpIndex,produsCpTip) VALUES        ('" + Session["SESunitateId"].ToString() + "','" + lblCertProdId.Text + "','" + tbProdus.Text + "','0','0','" + tbNr.Text + "','" + vEstimare.ToString().Replace(",", ".") + "','" + vCome.ToString().Replace(",", ".") + "','" + i.ToString() + "','1')";
                                vCmd.CommandText = vCmd.CommandText.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
                                vCmd.ExecuteNonQuery();
                            }
                        }
                    }


                    vTrans.Commit();
                }
                catch { vTrans.Rollback(); }
                ManipuleazaBD.InchideConexiune(vCon);
            }
            if (!lblEroare1.Visible)
            {
                pnlPopupAdaugaProduseComercializate.Visible = true;
            }
        }
        else
        {
            lblEroare1.Visible = true;
            lblEroare1.Text = vEroare;
            tbNr.Focus();
        }
        Response.Redirect("~/capitol12.aspx");
    }
    protected void btInapoi_Click(object sender, EventArgs e)
    {
        if (Session["actiune"] == "adauga")
        {
            SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
            SqlCommand vCmd = new SqlCommand();
            SqlTransaction vTrans = ManipuleazaBD.DeschideTranzactie(vCon);
            vCmd.Connection = vCon;
            vCmd.Transaction = vTrans;

            try
            {
                vCmd.CommandText = "DELETE FROM membriCp WHERE        (unitateId = " + Session["SESunitateId"].ToString() + ") AND (certProdId = " + lblCertProdId.Text + ")";
                vCmd.ExecuteNonQuery();
                vCmd.CommandText = "DELETE FROM certProd WHERE        (unitateId = " + Session["SESunitateId"].ToString() + ") AND (certProdId = " + lblCertProdId.Text + ")";
                vCmd.ExecuteNonQuery();
                vTrans.Commit();
            }
            catch { vTrans.Rollback(); }
            ManipuleazaBD.InchideConexiune(vCon);
            pnlAfis.Visible = true;
            pnlAdaugaModifica.Visible = false;
        }
        Response.Redirect("~/capitol12.aspx");
    }
}