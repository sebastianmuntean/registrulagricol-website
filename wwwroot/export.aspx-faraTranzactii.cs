﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Configuration;
using System.Data.SqlClient;

public partial class export : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    public static string XMLCitesteDataAnterioruluiExport()
    {
        string vDataAnterioruluiExport = ManipuleazaBD.fRezultaUnString("SELECT MAX(exportData) as DataAnterioruluiExport  FROM exporturi", "DataAnterioruluiExport");
        return vDataAnterioruluiExport;
    }
    public static List<string> XMLListaGospodariiDeScris(string pDataAnterioruluiExport) 
    { 
        // gasim toate gospodariile care au fost actualizate dupa data ultimului export
        string vInterogare = "SELECT gospodarieId FROM gospodarii WHERE dataModificare >= CONVERT(DATETIME, '"+pDataAnterioruluiExport+"', 104)";
        List<string> vListaGospodarii = ManipuleazaBD.fRezultaListaStringuri(vInterogare, "gospodarieId");
        return vListaGospodarii;
    }
    public void XMLScrieDataUltimuluiExport()
    {
        string vInterogare = "INSERT INTO exporturi (exportData, exportOra, unitateId, utilizatorId) VALUES (CONVERT(DATETIME, '" + DateTime.Now.Date.ToString() + "', 104), '" + DateTime.Now.Hour.ToString()+ "','" + Session["SESunitateId"] + "','" + Session["SESutilizatorId"] + "')";
        ManipuleazaBD.fManipuleazaBD(vInterogare);
    }
    public static void CreeazaXML(string pUnitate, string pUtilizator, string pDataInceput, string pGospodarie, XmlTextWriter writer)
    {


        //deschidem gospodarii
        writer.WriteStartElement("gospodarii");
        // scriem atribute gospodarie
        XMLScrieElement(writer, "gospodarii","gospodarieId",pGospodarie);

        // scriem capitolul 1 - tabela membri
        string strConn = ConfigurationManager.ConnectionStrings["TNTRegistruAgricolConnectionString"].ConnectionString;
        SqlConnection con = new SqlConnection(strConn);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = "SELECT capitolId FROM membri WHERE gospodarieId='"+pGospodarie+"' AND unitateId = '"+pUnitate+"'";
        con.Open();
        List<string> vListaCapitoleIdMembri = new List<string> { };
        try
        {
            SqlDataReader vRezultat = cmd.ExecuteReader();
            while (vRezultat.Read())
            {
                vListaCapitoleIdMembri.Add(vRezultat["capitolId"].ToString());
            }
        }
        catch { }
        con.Close();
        // scriem atribute capitole - tabela membri
        foreach (string vCapitolId in vListaCapitoleIdMembri)
        {
            List<string> vCampuri = new List<string> { "gospodarieId", "unitateId", "capitolId" };
            List<string> vValori = new List<string> { pGospodarie, pUnitate, vCapitolId };
            // deschidem linia capitol
            writer.WriteStartElement("membri");
            XMLScrieElement(writer, "membri", vCampuri, vValori);
            // inchidem linia capitol
            writer.WriteEndElement();
        }

        // scriem Capitolele care intra in tabela parcele
        string[] vRezCapitolParcele = { "parcelaId" };
        cmd.CommandText = "SELECT parcelaId FROM parcele WHERE gospodarieId='"+pGospodarie+"' AND unitateaId = '"+pUnitate+"'";
        con.Open();
        List<string> vListaCapitoleIdParcele = new List<string> { };
        try
        {
            SqlDataReader vRezultat = cmd.ExecuteReader();
            while (vRezultat.Read())
            {
                vListaCapitoleIdParcele.Add(vRezultat["parcelaId"].ToString());
            }
        }
        catch { }
        con.Close();
        // scriem atribute capitole - tabela capitole
        foreach (string vParcelaId in vListaCapitoleIdParcele)
        {
            List<string> vCampuri = new List<string> { "gospodarieId", "unitateaId", "parcelaId" };
            List<string> vValori = new List<string> { pGospodarie, pUnitate, vParcelaId };
            // deschidem linia capitol
            writer.WriteStartElement("parcele");
            XMLScrieElement(writer, "parcele", vCampuri, vValori);
            // inchidem linia capitol
            writer.WriteEndElement();
        }

        // scriem Capitolele care intra in tabela paduri
        string[] vRezCapitolPaduri = { "paduriId" };
        cmd.CommandText = "SELECT paduriId FROM paduri WHERE gospodarieId='"+pGospodarie+"' AND unitateId = '"+pUnitate+"'";
        con.Open();
        List<string> vListaCapitoleIdPaduri = new List<string> { };
        try
        {
            SqlDataReader vRezultat = cmd.ExecuteReader();
            while (vRezultat.Read())
            {
                vListaCapitoleIdPaduri.Add(vRezultat["paduriId"].ToString());
            }
        }
        catch { }
        con.Close();
        // scriem atribute capitole - tabela capitole
        foreach (string vPaduriId in vListaCapitoleIdPaduri)
        {
            List<string> vCampuri = new List<string> { "gospodarieId", "unitateId", "paduriId" };
            List<string> vValori = new List<string> { pGospodarie, pUnitate, vPaduriId };
            // deschidem linia capitol
            writer.WriteStartElement("paduri");
            XMLScrieElement(writer, "paduri", vCampuri, vValori);
            // inchidem linia capitol
            writer.WriteEndElement();
        }
        


        // scriem Capitolele care intra in tabela capitole
        string[] vRezCapitol2 = { "capitolId" };
        cmd.CommandText = "SELECT capitolId FROM capitole WHERE gospodarieId='"+pGospodarie+"' AND unitateId = '"+pUnitate+"'";
        con.Open();
        List<string> vListaCapitoleId2 = new List<string>{};
        try
        {
            SqlDataReader vRezultat = cmd.ExecuteReader();
            while (vRezultat.Read())
            {
                vListaCapitoleId2.Add(vRezultat["capitolId"].ToString());
            }
        }
        catch { }
        con.Close();
        // scriem atribute capitole - tabela capitole
        foreach (string vCapitolId in vListaCapitoleId2)
        {
            List<string> vCampuri = new List<string> { "gospodarieId", "unitateId", "capitolId" };
            List<string> vValori = new List<string> { pGospodarie, pUnitate, vCapitolId };
            // deschidem linia capitol
            writer.WriteStartElement("capitole");
            XMLScrieElement(writer, "capitole", vCampuri, vValori);
            // inchidem linia capitol
            writer.WriteEndElement();
        }
        // inchidem gospodarie
        writer.WriteEndElement();


    }    

    public static void XMLScrieElement(XmlTextWriter pFisier, string pElement, string[] pAtribute, string[] pValori)
    {
        pFisier.WriteStartElement(pElement);
        int vContor = 0;
        foreach (string vAtribut in pAtribute)
        {
            pFisier.WriteAttributeString(vAtribut, pValori[vContor]);
            vContor++;
        }
        pFisier.WriteEndElement();
    }
    public static void XMLScrieElement(XmlTextWriter pFisier, string pTabela, string pWhereCamp, string pWhereValoare)
    {
       // scriem toate atributele pentru o tabela - luam numele coloanelor si le scriem ca atribute
        int vContor = 0;
        List<string> vListaNumeColoane = ManipuleazaBD.NumeleColoanelor(pTabela);
        // citim valorile pentru coloanele extrase
        string vInterogare="SELECT ";
        foreach (string vNumeColoana in vListaNumeColoane)
        {
            if (vContor==0){vInterogare += vNumeColoana;}
            else {vInterogare+=", " + vNumeColoana;}
            vContor++;
        }
        vInterogare += " FROM " + pTabela + " WHERE " + pWhereCamp + "='" + pWhereValoare + "'"; 
        List<string> vListaRezultate = ManipuleazaBD.fRezultaUnString(vInterogare, vListaNumeColoane);
        // scriem atributele
        vContor = 0;
        foreach (string vAtribut in vListaNumeColoane)
        {
            pFisier.WriteAttributeString(vAtribut, vListaRezultate[vContor]);
            vContor++;
        }

    }
    public static void XMLScrieElement(XmlTextWriter pFisier, string pTabela, List<string> pWhereCampuri, List<string> pWhereValori)
    {
        // scriem toate atributele pentru o tabela - luam numele coloanelor si le scriem ca atribute
        int vContor = 0;
        List<string> vListaNumeColoane = ManipuleazaBD.NumeleColoanelor(pTabela);
        // citim valorile pentru coloanele extrase
        string vInterogare = "SELECT ";
        foreach (string vNumeColoana in vListaNumeColoane)
        {
            if (vContor == 0) { vInterogare += vNumeColoana; }
            else { vInterogare += ", " + vNumeColoana; }
            vContor++;
        }
        vInterogare += " FROM " + pTabela + " WHERE ";
        vContor = 0;
        foreach (string vCamp in pWhereCampuri)
        {
            if (vContor == 0) { vInterogare += vCamp + "='" + pWhereValori[vContor] + "'"; }
            else { vInterogare += "AND " + vCamp + "='" + pWhereValori[vContor] + "'"; }
            vContor++;
        }
        List<string> vListaRezultate = ManipuleazaBD.fRezultaUnString(vInterogare, vListaNumeColoane);
        // scriem atributele
        vContor = 0;
        foreach (string vAtribut in vListaNumeColoane)
        {
            pFisier.WriteAttributeString(vAtribut, vListaRezultate[vContor]);
            vContor++;
        }

    }
    public static void XMLScrieElement(XmlTextWriter pFisier, string pElement, int p)
    {
        pFisier.WriteStartElement(pElement);
        pFisier.WriteEndElement();
    }
    public static void XMLScrieElement(XmlTextWriter pFisier,string pParinte, string pElement, string[] pAtribute, string[] pValori)
    {
        pFisier.WriteStartElement(pElement);
        pFisier.WriteAttributeString(pAtribute[1], pValori[1]);
        pFisier.WriteEndElement();
    }

    protected void XMLIncepeDocumentul(string pUnitate, string pUtilizator, string pDataInceput, XmlTextWriter writer)
    {
        writer.WriteStartDocument();
        writer.WriteComment("Fisier ce contine datele de export pentru Registrul Agricol - TNT Computers SRL Sibiu office@tntcomputers.ro");
        writer.WriteStartElement("Export");
        writer.WriteAttributeString("unitateId", pUnitate);
        writer.WriteAttributeString("unitateCodFiscal", ManipuleazaBD.fRezultaUnString("SELECT TOP(1) unitateCodFiscal FROM unitati WHERE unitateId='" + pUnitate + "'", "unitateCodFiscal"));
        writer.WriteAttributeString("utilizatorId", pUtilizator);
        writer.WriteAttributeString("utilizatorCNP", ManipuleazaBD.fRezultaUnString("SELECT TOP(1) utilizatorCNP FROM utilizatori WHERE unitateId='" + pUnitate + "' AND utilizatorId='" + pUtilizator + "'", "utilizatorCNP"));
        writer.WriteAttributeString("dataAnterioruluiExport", pDataInceput);
        writer.WriteAttributeString("dataActualuluiExport", DateTime.Now.Date.ToString());
    }
    protected void XMLInchideDocumentul(XmlTextWriter writer)
    {
        writer.WriteEndElement();
        writer.WriteEndDocument();
        writer.Close();
    }
    protected void btExporta_Click(object sender, EventArgs e)
    {
        // Creates an XML file is not exist
        XmlTextWriter writer = new XmlTextWriter("D:\\test.xml", null);
        XmlTextReader reader = new XmlTextReader("D:\\test.xml");
        XMLIncepeDocumentul(Session["SESunitateId"].ToString(), Session["SESutilizatorId"].ToString(), XMLCitesteDataAnterioruluiExport(), writer);
        // citim gospodariile actualizate de la data anteriorului export
        foreach(string vGospodarieId in XMLListaGospodariiDeScris(XMLCitesteDataAnterioruluiExport()))
        {
            CreeazaXML(Session["SESunitateId"].ToString(), Session["SESutilizatorId"].ToString(), DateTime.Now.ToString(), vGospodarieId, writer);    
        }
        // Ends the document
        XMLInchideDocumentul(writer);
        // scriem data exportului tocmai incheiat
        XMLScrieDataUltimuluiExport();
    }
    protected void btImporta_Click(object sender, EventArgs e)
    {
        XmlTextReader reader = new XmlTextReader("D:\\test.xml");
        List<string> vListaElemente = new List<string> { };
        // citim elementul Export
        List<string> vCampuri = new List<string> { }; 
        List<string> vValori = new List<string> { }; 
        List<string> vCampuriId = new List<string> { };
        List<string> vValoriId = new List<string> { };
        int vContor = 0;
        int vContorGospodarii = 0;
        string vNume = "";
        while (reader.Read())
        {
            XMLCitesteElement(reader, "Export");
            vContor++;
            break;
        }
        // ne rezulta vListaValoriAtribute si vListaNumeAtribute pe care vom introduce in bd     
        string vNumeElementPrincipal = "";
        while (reader.Read())
        {
            if (reader.Name == "gospodarii")
            {
                XMLSterge(reader);
                vNumeElementPrincipal = "gospodarii";
                // stergem capitolele din gospodarii, membrii, parcelele si padurile si facem update la gospodarii 
            }
            else
            {
                vNumeElementPrincipal = "";
            }
            // initializam listele
            vListaNumeAtribute.Clear();
            vListaValoriAtribute.Clear();
            vCampuri.Clear();
            vValori.Clear();
            vValoriId.Clear();
            vCampuriId.Clear();
            vContorGospodarii = 0;
            vNume = reader.Name; // de aici ne da tabela unde inseram
            while (vContorGospodarii < reader.AttributeCount)
            {
                // citim atributele pentru Element
                reader.MoveToNextAttribute();
                vListaValoriAtribute.Add(reader.GetAttribute(reader.Name));
                vListaNumeAtribute.Add(reader.Name);
                vContorGospodarii++;
            }
             // daca e tip gospodarii : facem update
            if (vNumeElementPrincipal == "gospodarii")
            {
              try
                {
                    // facem update la gospodarii
                    int vContorX = 0;
                    foreach (string vCampGospodarii in vListaNumeAtribute)
                    {
                        if (vContorX > 0)
                        {
                            vCampuri.Add(vCampGospodarii);
                            // daca e tip data convertim
                            if (vCampGospodarii == "dataModificare")
                            {
                                vValori.Add("###" + vListaValoriAtribute[vContorX]);
                            }
                            else
                            {
                                vValori.Add(vListaValoriAtribute[vContorX]);
                            }
                        }
                        vContorX++;
                    }
                    vCampuriId.Add("unitateId");
                    vCampuriId.Add("gospodarieId");
                    vValoriId.Add(vListaValoriAtribute[vListaNumeAtribute.IndexOf("unitateId")]);
                    vValoriId.Add(vListaValoriAtribute[vListaNumeAtribute.IndexOf("gospodarieId")]);
                    ManipuleazaBD.fManipuleazaBDArray(1, "gospodarii", vCampuri, vValori, vCampuriId, vValoriId);
                    // am finalizat update la gospodarii
                }
                catch { }
            }
            // daca nu sunt gospodarii DELETE + INSERT
            else
            { 
                // am sters mai sus TOT ce a tinut de gospodarie, acum inseram pe rand
                try
                {
                    // facem INSERT la celelalte elemente altele decat GOSPODARII
                    int vContorY = 0;
                    foreach (string vCampAltele in vListaNumeAtribute)
                    {
                        if (vContorY > 0) // primul element e id-ul, nu il inseram - altfel da eroare
                        {
                            vCampuri.Add(vCampAltele);
                            // daca e tip data convertim; avem cazul dataNasterii la membri
                            switch (vCampAltele)
                            {
                                case "dataNasterii":
                                    vValori.Add("###" + vListaValoriAtribute[vContorY]); 
                                    break;
                                case "parcelaSuprafataIntravilanAri":
                                    vValori.Add(vListaValoriAtribute[vContorY].Replace(',', '.'));
                                    break;
                                case "parcelaSuprafataExtravilanAri":
                                    vValori.Add(vListaValoriAtribute[vContorY].Replace(',', '.'));
                                    break;
                                case "paduriSuprafataAri":
                                    vValori.Add(vListaValoriAtribute[vContorY].Replace(',', '.'));
                                    break;
                                case "paduriSuprafataGrupa2Ari":
                                    vValori.Add(vListaValoriAtribute[vContorY].Replace(',', '.'));
                                    break;
                                case "col1":
                                    vValori.Add(vListaValoriAtribute[vContorY].Replace(',', '.'));
                                    break;
                                case "col2":
                                    vValori.Add(vListaValoriAtribute[vContorY].Replace(',', '.'));
                                    break;
                                case "col3":
                                    vValori.Add(vListaValoriAtribute[vContorY].Replace(',', '.'));
                                    break;
                                case "col4":
                                    vValori.Add(vListaValoriAtribute[vContorY].Replace(',', '.'));
                                    break;
                                case "col5":
                                    vValori.Add(vListaValoriAtribute[vContorY].Replace(',', '.'));
                                    break;
                                case "col6":
                                    vValori.Add(vListaValoriAtribute[vContorY].Replace(',', '.'));
                                    break;
                                case "col7":
                                    vValori.Add(vListaValoriAtribute[vContorY].Replace(',', '.'));
                                    break;
                                case "col8":
                                    vValori.Add(vListaValoriAtribute[vContorY].Replace(',', '.'));
                                    break;
                                default: 
                                    vValori.Add(vListaValoriAtribute[vContorY]);
                                    break;
                            }
                        }
                        vContorY++;
                    }
                    ManipuleazaBD.fManipuleazaBDArray(0, vNume, vCampuri, vValori, vCampuriId, vValoriId);
                    // am finalizat update la altele decat gospodarii
                }
                catch { }
            }
            reader.MoveToElement();
            vContor++;
        }
    }
    public void XMLSterge(XmlTextReader reader)
    { 
        // citim in continuare sa luam atributele gospodariei
        string vGospodarieId = reader.GetAttribute("gospodarieId");
        string vUnitateId = reader.GetAttribute("unitateId");
        string vAn = DateTime.Now.Year.ToString();
        if (vGospodarieId != "" && vGospodarieId != null)
        {
            string vInterogareCapitole = "DELETE FROM capitole WHERE gospodarieId='" + vGospodarieId + "' AND unitateId='" + vUnitateId + "' AND an='" + vAn + "'";
            string vInterogareParcele = "DELETE FROM parcele WHERE gospodarieId='" + vGospodarieId + "' AND unitateaId='" + vUnitateId + "' AND an='" + vAn + "'";
            string vInterogarePaduri = "DELETE FROM paduri WHERE gospodarieId='" + vGospodarieId + "' AND unitateId='" + vUnitateId + "' AND an='" + vAn + "'";
            string vInterogareMembri = "DELETE FROM membri WHERE gospodarieId='" + vGospodarieId + "' AND unitateId='" + vUnitateId + "' AND an='" + vAn + "'";
            
            ManipuleazaBD.fManipuleazaBD(vInterogareCapitole); 
            ManipuleazaBD.fManipuleazaBD(vInterogareParcele); 
            ManipuleazaBD.fManipuleazaBD(vInterogarePaduri); 
            ManipuleazaBD.fManipuleazaBD(vInterogareMembri);
             
        }
        reader.MoveToElement();

    }
    public void XMLCitesteCopil(XmlTextReader reader, string pElement)
    {
        // ne intoarcem la capatul elementului gospodarii si sarim la descendentul pElement 
 
        reader.ReadToDescendant(pElement);
        vListaNumeAtribute.Clear();
        vListaValoriAtribute.Clear();
        int vContor = 0;
        while (!reader.EOF)
        {
            vContor = 0;
            while (vContor < reader.AttributeCount)
            {
                // citim atributele pentru membri
                reader.MoveToNextAttribute();
                vListaValoriAtribute.Add(reader.GetAttribute(reader.Name));
                vListaNumeAtribute.Add(reader.Name);
                vContor++;
                // si aici le introducem in baza de date .....
                // ...
                // ...
            }
            // mergem la urmatorul frate; daca nu e valid sarim din ciclu
            if (!reader.ReadToNextSibling(pElement))
            { reader.ReadEndElement();break; }
            
        }
        //reader.Read();
        

    }

    List<string> vListaValoriAtribute = new List<string> { };
    List<string> vListaNumeAtribute = new List<string> { };
    public void XMLCitesteElement(XmlTextReader pReader, string pElement)
    {
 
            pReader.ReadToFollowing(pElement);
            int vContor = 0;
            while (vContor < pReader.AttributeCount)
            {
                pReader.MoveToNextAttribute();
                vListaValoriAtribute.Add(pReader.GetAttribute(pReader.Name));
                vListaNumeAtribute.Add(pReader.Name);
                vContor++;
            }
 
    }
}
