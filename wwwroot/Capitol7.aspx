﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Capitol7.aspx.cs" Inherits="Capitol7" Culture="ro-RO" UICulture="ro-RO" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Capitole / CAPITOLUL VII: Animale domestice şi/sau animale sălbatice crescute în captivitate, în condiţiile legii - Situaţia la începutul semestrului" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="upUpdatePanel" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnAfiseazaMesaje" runat="server">
                <asp:ValidationSummary ID="valCapitole" runat="server" DisplayMode="SingleParagraph"
                    Visible="true" CssClass="validator" ForeColor="" />
            </asp:Panel>
            <asp:Panel ID="Panel5" CssClass="ascunsa" runat="server">
                <!-- //////////// VALIDATOARE  \\\\\\\\\\\\\\\\\\ -->
                <asp:CustomValidator ID="valCapitol" runat="server" ErrorMessage=""></asp:CustomValidator>
            </asp:Panel>
            <asp:Panel ID="pnTabel" runat="server" CssClass="panel_general" Visible="true">
                <asp:GridView ID="gvCapitol" CssClass="tabela_capitole" runat="server" AutoGenerateColumns="False"
                    DataKeyNames="capitolId" DataSourceID="sdsGvCapitol" OnPreRender="gvCapitol_PreRender"
                    OnRowDataBound="gvCapitol_RowDataBound" OnDataBound="gvCapitol_DataBound">
                    <Columns>
                        <asp:BoundField DataField="denumire1" SortExpression="denumire1" HeaderText="Specie/categorie de animale" />
                        <asp:TemplateField HeaderText="Cod rand" SortExpression="codRand">
                            <EditItemTemplate>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblCodRand" runat="server" Text='<%# Bind("codRand") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Sem. I" SortExpression="col1">
                            <ItemTemplate>
                                <asp:TextBox ID="tbCol1" AutoPostBack="true" onfocus="vFocus(this)" runat="server"
                                    Text='<%# Bind("col1", "{0}") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Sem. II" SortExpression="col2">
                            <ItemTemplate>
                                <asp:TextBox ID="tbCol2" AutoPostBack="true" onfocus="vFocus(this)" runat="server"
                                    Text='<%# Bind("col2", "{0}") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="formula" SortExpression="formula" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblFormula" runat="server" Text='<%# Bind("formula") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="capitolId" InsertVisible="False" SortExpression="capitolId"
                            Visible="False">
                            <EditItemTemplate>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblId" runat="server" Text='<%# Bind("capitolId") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:SqlDataSource ID="sdsGvCapitol" runat="server" 
        SelectCommand="SELECT capitole.capitolId, capitole.unitateId, capitole.gospodarieId, capitole.an, capitole.codCapitol, capitole.codRand, CONVERT (int, capitole.col1) AS col1, CONVERT (int, capitole.col2) as col2, CONVERT (int, capitole.col3) AS col3, capitole.col4, CONVERT (int, capitole.col5) AS col5, capitole.col6, capitole.col7, capitole.col8, sabloaneCapitole.formula, sabloaneCapitole.denumire1, sabloaneCapitole.denumire2, sabloaneCapitole.denumire3, sabloaneCapitole.denumire4, sabloaneCapitole.denumire5 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = @unitateId) AND (capitole.an = @an) AND (capitole.gospodarieId = @gospodarieId) AND (sabloaneCapitole.capitol = @capitol)  AND (sabloaneCapitole.an = @an) ORDER BY capitole.codRand">
        <SelectParameters>
            <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" DefaultValue="" />
            <asp:SessionParameter Name="an" SessionField="SESan" DefaultValue="" />
            <asp:SessionParameter DefaultValue="" Name="gospodarieId" SessionField="SESgospodarieId" />
            <asp:Parameter DefaultValue="7" Name="capitol" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:Panel ID="pnListaButoanePrincipale" CssClass="butoane" runat="server">
        <asp:Button CssClass="buton" ID="btAdauga" runat="server" Text="salvează capitol"
            OnClick="btSalveaza_Click" />
        <asp:Button CssClass="buton" ID="btAdaugaAnimale" runat="server" Text="operează intrari/ieşiri animale"
           PostBackUrl="~/animale.aspx" />
        <asp:Button CssClass="buton" ID="btListaGospodarii" runat="server" Text="listă gospodării"
            OnClick="btListaGospodarii_Click" />
        <asp:Button CssClass="buton" ID="btTiparire" runat="server" Text="tipărire capitol"
            OnClick="btTiparire_Click" />
            <asp:Button CssClass="buton" ID="btCorelatii" runat="server" 
            Text="verifică corelaţii" onclick="btCorelatii_Click" Visible="false" />
    </asp:Panel>
</asp:Content>
