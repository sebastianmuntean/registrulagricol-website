﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CentralizatorCapitol12f.aspx.cs" Inherits="CentralizatorCapitol12f" Culture="ro-RO" UICulture="ro-RO" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Capitole / CAPITOLUL XII: f) Producţia de struguri obţinută de gospodăriile/exploataţiile agricole fără personalitate juridică cu domeniul/sediul în localitate şi/sau unităţile cu personalitate juridică care au activitate pe raza localităţii" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="upUpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnTabel" runat="server" CssClass="panel_general" Visible="true">
                <asp:GridView ID="gvCapitol" CssClass="tabela_capitole" runat="server" AutoGenerateColumns="False"
                    DataKeyNames="capitolId" DataSourceID="sdsGvCapitol" OnPreRender="gvCapitol_PreRender"
                    OnRowDataBound="gvCapitol_RowDataBound" ondatabound="gvCapitol_DataBound">
                    <Columns>
                        <asp:BoundField DataField="denumire1" SortExpression="denumire1" HeaderText="Denumire cultură" />
                        <asp:TemplateField HeaderText="Cod rand" SortExpression="codRand">
                            <EditItemTemplate>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblCodRand" runat="server" Text='<%# Bind("codRand") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Suprafaţa ha" SortExpression="col1">
                            <ItemTemplate>
                                <asp:TextBox ID="tbCol1" AutoPostBack="true" onfocus="vFocus(this)"
                                    runat="server" Text='<%# Bind("col1", "{0:N}") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Producţie medie kg/ha" SortExpression="col2">
                            <ItemTemplate>
                                <asp:TextBox ID="tbCol2" AutoPostBack="true" onfocus="vFocus(this)"
                                    runat="server" Text='<%# Bind("col2", "{0:N}") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Producţia totală tone" SortExpression="col3">
                            <ItemTemplate>
                                <asp:TextBox ID="tbCol3" AutoPostBack="true" onfocus="vFocus(this)" runat="server"
                                    Text='<%# Bind("col3", "{0}") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="formula" SortExpression="formula" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblFormula" runat="server" Text='<%# Bind("formula") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="capitolId" InsertVisible="False" SortExpression="capitolId"
                            Visible="False">
                            <EditItemTemplate>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblId" runat="server" Text='<%# Bind("capitolId") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:SqlDataSource ID="sdsGvCapitol" runat="server" 
        SelectCommand="SELECT capitole.capitolId, capitole.unitateId, capitole.gospodarieId, capitole.an, capitole.codCapitol, capitole.codRand, capitole.col1, capitole.col2, capitole.col3, capitole.col4, CONVERT (int, capitole.col5) AS col5, capitole.col6, capitole.col7, capitole.col8, sabloaneCapitole.formula, sabloaneCapitole.denumire1, sabloaneCapitole.denumire2, sabloaneCapitole.denumire3, sabloaneCapitole.denumire4, sabloaneCapitole.denumire5 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE (capitole.unitateId = @unitateId) AND (capitole.an = @an) AND (sabloaneCapitole.an = @an)  AND (capitole.gospodarieId = @gospodarieId) AND (sabloaneCapitole.capitol = @capitol) ORDER BY capitole.codRand">
        <SelectParameters>
            <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" 
                DefaultValue="" />
            <asp:SessionParameter Name="an" SessionField="SESan" DefaultValue="" />
            <asp:Parameter DefaultValue="-1" Name="gospodarieId" />
            <asp:Parameter DefaultValue="c12f" Name="capitol" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:Panel ID="pnListaButoanePrincipale" CssClass="butoane" runat="server">
        <asp:Button CssClass="buton" ID="btAdauga" runat="server" Text="salvează capitol"
            OnClick="btSalveaza_Click" />
        <asp:Button CssClass="buton" ID="btListaGospodarii" runat="server" Text="listă gospodării"
            OnClick="btListaGospodarii_Click" />
        <asp:Button CssClass="buton" ID="btTiparire" runat="server" 
            Text="tipărire capitol" onclick="btTiparire_Click" />
    </asp:Panel>
</asp:Content>
