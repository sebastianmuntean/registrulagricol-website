﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class printRaportCladiri : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataTable reportDataTable = GetRaport();
            DataTable unitatiDataTable = GetUnitateDenumire();
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource ds = new ReportDataSource("dsRapoarte_raportCapitole", reportDataTable);
            ReportDataSource ds2 = new ReportDataSource("dsRapoarte_dtUnitati", unitatiDataTable);
            ReportViewer1.LocalReport.DataSources.Add(ds);
            ReportViewer1.LocalReport.DataSources.Add(ds2);
            ReportViewer1.LocalReport.Refresh();
            ReportViewer1.Visible = true;
        }
    }

    private DataTable GetUnitateDenumire()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        string interogare = @"SELECT        unitati.unitateId, unitati.unitateDenumire, unitati.unitateCodSiruta, unitati.judetId, unitati.localitateId, unitati.unitateStrada, unitati.unitateNr, unitati.unitateAp, 
                         unitati.unitateCodPostal, unitati.localitateComponentaId, unitati.unitateCodFiscal, unitati.unitateActiva, unitati.unitatePrincipala, judete.judetDenumire, 
                         localitati.localitateDenumire
FROM            unitati INNER JOIN
                         judete ON unitati.judetId = judete.judetId INNER JOIN
                         localitati ON unitati.localitateId = localitati.localitateId
WHERE        (unitati.unitateId = " + Session["SESunitateId"].ToString() + ")";
        SqlDataAdapter adapter = new SqlDataAdapter(interogare, connection);
        DataTable table = new DataTable();
        adapter.Fill(table);
        ManipuleazaBD.InchideConexiune(connection);
        return table;
    }

    private DataTable GetRaport()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        string interogare = @"SELECT VOLUM,NRPOZITIE, (case persJuridica when 1 THEN jUnitate WHEN 0 THEN membruNume END) as membruNume , (case persJuridica when 1 THEN jCodFiscal WHEN 0 THEN membruCnp END) as membruCnp ,codCapitol FROM capitole
                                    JOIN gospodarii ON gospodarii.gospodarieId = capitole.gospodarieId
                            WHERE (COL1+COL2+COL3+COL4+COL5+COL6+COL7+COL8 > 0)
                            AND gospodarii.unitateId = " + Session["SESUnitateId"] + " AND codCapitol = '" + Request.QueryString["capitol"] + "' and gospodarii.an = " + Session["SESAn"] + @"
                            GROUP BY VOLUM,NRPOZITIE,membruNume,membruCnp,codCapitol,persJuridica,jUnitate,persJuridica,jCodFiscal
                            order by volum,nrPozitie";
        SqlDataAdapter adapter = new SqlDataAdapter(interogare, connection);
        DataTable table = new DataTable();
        adapter.SelectCommand.CommandTimeout = 0;
        adapter.Fill(table);
        ManipuleazaBD.InchideConexiune(connection);
        return table;
    }

    protected void Page_SaveStateComplete(object sender, EventArgs e)
    {
        Warning[] warnings;
        string[] streamids;
        string mimeType;
        string encoding;
        string fileNameExtension;
        byte[] bytes = ReportViewer1.LocalReport.Render("PDF", null, out mimeType, out encoding, out fileNameExtension, out streamids, out warnings);
        Response.Buffer = true;
        Response.Clear();
        Response.ContentType = mimeType;
        Response.AddHeader("content-disposition", "inline; filename=Nume" + fileNameExtension);
        Response.BinaryWrite(bytes);
        Response.Flush();
        Response.End();
        Response.Close();
    }
}