﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class reparaAdaugaRanduriCapitole : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            tbAn.Text = Session["SESan"].ToString();
    }
    protected void btAdauga_Click(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd1 = new SqlCommand();
        SqlCommand vCmd2 = new SqlCommand();
        SqlCommand vCmd3 = new SqlCommand();
        vCmd1.Connection = vCon;
        vCmd1.CommandTimeout = 0;
        vCmd2.Connection = vCon;
        vCmd2.CommandTimeout = 0;
        vCmd3.Connection = vCon;
        vCmd3.CommandTimeout = 0;
        vCmd3.CommandText = "SELECT gospodarieId, codCapitol, unitateId FROM capitole WHERE unitateId = " + ddlUnitate.SelectedValue + " group by gospodarieId, codCapitol, unitateId";
        SqlDataReader vTabel1 = vCmd3.ExecuteReader();
        while (vTabel1.Read())
        {
            vCmd2.CommandText = "SELECT codRand FROM sabloaneCapitole WHERE (an = '" + tbAn.Text + "') AND (capitol = '" + vTabel1["codCapitol"].ToString() + "')";
            SqlDataReader vTabel2 = vCmd2.ExecuteReader();
            while (vTabel2.Read())
            {
                vCmd1.CommandText = "select count(*) from capitole where gospodarieId='" + vTabel1["gospodarieId"].ToString() + "' and an='" + tbAn.Text + "' and codCapitol='" + vTabel1["codCapitol"].ToString() + "' and codRand='" + vTabel2["codRand"].ToString() + "'";
                if (Convert.ToInt32(vCmd1.ExecuteScalar()) == 0)
                {
                    vCmd1.CommandText = "INSERT INTO capitole (unitateId, gospodarieId, an, codCapitol, codRand, col1, col2, col3, col4, col5, col6, col7, col8) VALUES ('" + vTabel1["unitateId"].ToString() + "', '" + vTabel1["gospodarieId"].ToString() + "', '" + tbAn.Text + "', '" + vTabel1["codCapitol"].ToString() + "', '" + vTabel2["codRand"].ToString() + "', 0, 0, 0, 0, 0, 0, 0, 0)";
                    vCmd1.ExecuteNonQuery();
                }
            }
            vTabel2.Close();
        }
        vTabel1.Close();
        ManipuleazaBD.InchideConexiune(vCon);
    }
}
