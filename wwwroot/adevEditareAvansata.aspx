﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="adevEditareAvansata.aspx.cs" Inherits="adevEditareAvansata" ValidateRequest="false"
    EnableEventValidation="false" UICulture="ro-Ro" Culture="ro-Ro" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

                    <asp:Panel ID="Panel2" runat="server">
                        <h2>
                            ADAUGĂ / MODIFICĂ O ADEVERINŢĂ</h2>
                    </asp:Panel>
    <asp:Panel ID="pnAvansata" runat="server" CssClass="panel_general">
        <asp:Panel ID="pnEditareAvansata" CssClass="adauga" runat="server" Visible="true">

                <asp:Panel ID="Panel8" runat="server">
                    <h2>
                        EDITARE AVANSATĂ A TEXTULUI ADEVERINŢEI</h2>
                </asp:Panel>
                <asp:Panel ID="Panel9" runat="server">
                    <asp:TextBox ID="tbTextAvansat" runat="server" TextMode="MultiLine" Rows="20" />

                    <script type="text/javascript">
                        //<![CDATA[

                        CKEDITOR.replace('ctl00_ContentPlaceHolder1_tbTextAvansat',
					{
					    contentsCss: ['/css/master.css', CKEDITOR.basePath + 'mo.css'],
					    fullPage: true,
					    height: 600


					});

					//]]>
                    </script>

                </asp:Panel>
            <asp:Panel ID="Panel6" runat="server" CssClass="">
                <asp:RadioButtonList ID="rbPePagina" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="1">Una pe pagină</asp:ListItem>
                    <asp:ListItem Selected="True" Value="2">Două pe pagină</asp:ListItem>
                </asp:RadioButtonList>
            </asp:Panel>
            <asp:Panel ID="Panel14" runat="server" CssClass="butoane">
                <asp:Button ID="btSalveazaAvansata" runat="server" CssClass="buton" Text="salvează şi tipăreşte textul adeverinţei"
                    OnClick="btSalveazaAvansata_Click" />
                <asp:Button ID="btAnuleazaSalvareaAvansata" runat="server" CssClass="buton" Text="listă adeverinţe"
                    OnClick="btAnuleazaSalvareaAvansata_Click" />
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
</asp:Content>
