﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="raportListaStrainasi.aspx.cs" Inherits="raportListaStrainasi" Culture="ro-RO" UICulture="ro-RO"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" Runat="Server">
<asp:Label ID="url" runat="server" Text="Rapoarte / Listă străinași" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Panel ID="pnGeneral" runat="server" CssClass="panel_general">
    <asp:Panel ID="pnFiltrare" runat="server" CssClass="cauta">
       <asp:Label ID="lblFiltrareDupa" runat="server" Text="Caută după:"></asp:Label>
       <asp:Label ID="lblNume" runat="server" Text="nume"></asp:Label>
       <asp:TextBox ID="tbNume" runat="server" AutoPostBack="False" Width="150px"></asp:TextBox>
       <asp:Label ID="lblFJudet" runat="server" Text="Judeţ"></asp:Label>
            <asp:DropDownList ID="ddlFJudet" AutoPostBack="true" runat="server" OnPreRender="ddlFJudet_PreRender"
                OnSelectedIndexChanged="ddlFJudet_SelectedIndexChanged">
            </asp:DropDownList>
        <asp:Label ID="lblUnitate" runat="server" Text="unitate:"></asp:Label>
        <asp:DropDownList ID="ddlUnitate" runat="server" oninit="ddlUnitate_Init">
        </asp:DropDownList>
     </asp:Panel>
        <asp:Panel ID="pnButoane" runat="server" CssClass="butoane">
        <asp:Button ID="btTiparire" runat="server" Text="tipărește"  CssClass="buton" 
                onclick="btnFiltrare_Click"/>
        </asp:Panel>
    <asp:Panel ID="pnRaport" runat="server" CssClass="panel_general">
    </asp:Panel>
    </asp:Panel>

</asp:Content>


