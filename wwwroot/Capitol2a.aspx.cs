﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

/// <summary>
/// Adaugare capitol
/// Creata la:                  14.02.2011
/// Autor:                      Laza Tudor Mihai
/// Ultima                      actualizare: 01.04.2011
/// Autor:                      SM - adaugare log tiparire
/// </summary> 
public partial class Capitol2a : System.Web.UI.Page
{
    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion
    protected void Page_Init(object sender, EventArgs e)
    {
        CalculCapitole.PopuleazaTabelaCapitole(Convert.ToInt32(Session["SESan"]), Convert.ToInt64(Session["SESgospodarieId"]), Convert.ToInt64(Session["SESunitateId"]), "2a");
        if (Session["SESgospodarieId"] == null)
        {
            HttpCookie vCookie = Request.Cookies["COOKTNT"];
            vCookie = CriptareCookie.DecodeCookie(vCookie);
            if (vCookie != null)
            {
                if (vCookie["COOKgospodarieId"] != null)
                {
                    Session["SESgospodarieId"] = vCookie["COOKgospodarieId"].ToString();
                }
                else if (Request.Url.AbsolutePath.IndexOf("Gospodarii.aspx") == -1 && Session["SESgospodarieId"] == null)
                    Response.Redirect("~/Gospodarii.aspx");
            }
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        DBConnections connection = new DBConnections(Convert.ToInt16(Session["SESan"]));

        sdsGvCapitol.ConnectionString = connection.Create();

        ViewState["TabIndex"] = 30;
        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "Cap.2a", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
        }
    }
    protected void gvCapitol_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (ViewState["Capitole"] != null)
        {
            List<Capitole> vCapitole = (List<Capitole>)ViewState["Capitole"];
            ViewState["TabIndex"] = CalculCapitole.PopulareGridView(vCapitole, this, gvCapitol, e, Convert.ToInt16(ViewState["TabIndex"]), 1);
        }
        if (e.Row.DataItemIndex == 0)
        {
            TextBox tbCol = (TextBox)e.Row.FindControl("tbCol3");
            tbCol.Focus();
        }
    }
    protected void gvCapitol_PreRender(object sender, EventArgs e)
    {
        List<Capitole> vCapitole = new List<Capitole>();
        vCapitole.Clear();
        vCapitole = CalculCapitole.PopuleazaViewState(gvCapitol, 1, 1, 1, 0, 1);
        ViewState.Add("Capitole", vCapitole);
        gvCapitol.DataBind();
    }
    protected void btListaGospodarii_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Gospodarii.aspx");
    }
    protected void btSalveaza_Click(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();

        // salvam valorile initiale in lista
        List<List<string>> vListaValoriInitiale = CalculCapitole.vListaValoriInitiale(Convert.ToString(Session["SESgospodarieId"]), Convert.ToString(Session["SESunitateId"]), "2a", Convert.ToInt16(Session["SESan"]));

        List<Capitole> vCapitole = new List<Capitole>();
        vCapitole.Clear();
        vCapitole = CalculCapitole.PopuleazaViewState(gvCapitol, 1, 1, 1, 0, 1);
        ViewState.Add("Capitole", vCapitole);
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlTransaction vTranz = ManipuleazaBD.DeschideTranzactie(vCon);
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.Transaction = vTranz;
        try
        {
            vCapitole = (List<Capitole>)ViewState["Capitole"];
            for (int i = 0; i < vCapitole.Count; i++)
            {
                vCmd.CommandText = @"UPDATE capitole SET col1 = '" + vCapitole[i].Col1.Replace(',', '.') + "', col2 = '" + vCapitole[i].Col2.Replace(',', '.') + "', col3 = '" + vCapitole[i].Col3.Replace(',', '.') + "', col4 = '" + vCapitole[i].Col4.Replace(',', '.') + "', col5 = '" + vCapitole[i].Col5.Replace(',', '.') + "', col6 = '" + vCapitole[i].Col6.Replace(',', '.') + "', col7 = '" + vCapitole[i].Col7.Replace(',', '.') + "', col8 = '" + vCapitole[i].Col8.Replace(',', '.') + "' WHERE (capitolId = '" + vCapitole[i].Id.ToString() + "')";
                vCmd.ExecuteNonQuery();
            }

            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "capitole", "salvare capitol 2a", "", vCmd, Convert.ToInt64(Session["SESgospodarieId"]), 2);
            ClassLog.UpdateDataUltimeiModificari(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESgospodarieId"]), vCmd);
            vTranz.Commit();
        }
        catch { vTranz.Rollback(); }
        finally
        {
            ManipuleazaBD.InchideConexiune(vCon);        
            clsCorelatii.VerificaCorelatie(Convert.ToInt32(Session["SESunitateId"]), Convert.ToInt32(Session["SESgospodarieId"]), Convert.ToInt32(Session["SESan"]), "2a");
            ((MasterPage)this.Page.Master).VerificaCorelatii();
        }
        valCapitol.IsValid = false;
        // scriem si in capitoleCentralizate
        valCapitol.ErrorMessage = "Capitolul 2A a fost salvat cu succes ! " + CalculCapitole.vInsertCapitoleCentralizate(Convert.ToString(Session["SESgospodarieId"]), Convert.ToString(Session["SESunitateId"]), "2a", Convert.ToString(Session["SESan"]), vListaValoriInitiale); 

    }
    protected void btTiparire_Click(object sender, EventArgs e)
    {
        clsTiparireCapitole.UmpleRapCapitole(Session["SESutilizatorId"].ToString(), Session["SESan"].ToString(), Session["SESgospodarieId"].ToString(), "2a");
        ResponseHelper.Redirect("~/printCapitolul2a.aspx?codCapitol=2a", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
        ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "tiparire", "tiparire capitol 2a", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
    }
    protected void gvCapitol_DataBound(object sender, EventArgs e)
    {
        SqlConnection vCon =  ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select top(1) coalesce(unitateFocusLaEnter,'False') from unitati where unitateId='" + Session["SESunitateId"].ToString() + "'";
        Int16 vTabIndex = Convert.ToInt16(((TextBox)(this.Master.FindControl("tbTabIndex"))).Text);
        if (Convert.ToBoolean(vCmd.ExecuteScalar()))
            vTabIndex = Convert.ToInt16("0");
        ControlTabIndex.SetTabIndex(this, gvCapitol, vTabIndex);
        ManipuleazaBD.InchideConexiune(vCon);
    }
    protected void btCorelatii_Click(object sender, EventArgs e)
    {
        clsCorelatii.VerificaCorelatie(Convert.ToInt32(Session["SESunitateId"]), Convert.ToInt32(Session["SESgospodarieId"]), Convert.ToInt32(Session["SESan"]), "2a");
    }
    protected void btAdauga_Init(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        // daca am bifa pe unitati adaug in C2A din C2B
        SqlConnection vCon =  ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select top(1) unitateAdaugaDinC2B from unitati where unitateId='" + Session["SESunitateId"].ToString() + "'";
        bool vAdaugaDinC2b = Convert.ToBoolean(vCmd.ExecuteScalar());
        ManipuleazaBD.InchideConexiune(vCon);
        if (vAdaugaDinC2b)
            btAdauga.Visible = false;
    }
}
