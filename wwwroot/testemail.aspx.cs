﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Net;

public partial class testemail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CreateCopyMessage();
    }
    public static void CreateCopyMessage()
    {
        MailAddress from = new MailAddress("sebastianmuntean@yahoo.com", "Yahoo");
        MailAddress to = new MailAddress("sebastian@simpludesign.com", "Simplu");
        MailMessage message = new MailMessage(from, to);
        // message.Subject = "Using the SmtpClient class.";
        message.Subject = "Using the SmtpClient class.";
        message.Body = @"Using this feature, you can send an e-mail message from an application very easily.";
        // Add a carbon copy recipient.
        MailAddress copy = new MailAddress("sebastian.muntean@tntcomputers.ro");
        message.CC.Add(copy);
        SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
        // Include credentials if the server requires them.
        client.Credentials = CredentialCache.DefaultNetworkCredentials;
        
        Console.WriteLine("Sending an e-mail message to {0} by using the SMTP host {1}.",
             to.Address, client.Host);

        try
        {
            client.Send(message);
        }
        catch (Exception ex)
        {
            Console.WriteLine("Exception caught in CreateCopyMessage(): {0}",
                        ex.ToString());
        }
    }


}
