﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Text.RegularExpressions;
using System.Collections;
/// <summary>
/// Adaugare capitol
/// Creata la:                  ??.02.2011
/// Autor:                      SM
/// Ultima                      actualizare: 01.04.2011
/// Autor:                      SM - adaugare in log
/// </summary> 
public partial class export : System.Web.UI.Page
{

    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "Export", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
        }
    }
    public string XMLCitesteDataAnterioruluiExport()
    {
        string vDataAnterioruluiExport = ManipuleazaBD.fRezultaUnString("SELECT MAX(exportData) as DataAnterioruluiExport  FROM exporturi WHERE exportTip = '0' and unitateId='" + Session["SESunitateId"].ToString() + "'", "DataAnterioruluiExport");
        return vDataAnterioruluiExport;
    }
    public List<string> XMLListaGospodariiDeScris(string pDataAnterioruluiExport)
    {
        // gasim toate gospodariile care au fost actualizate dupa data ultimului export
        string vInterogare = "SELECT gospodarieId FROM gospodarii WHERE unitateId = '" + Session["SESunitateId"] + "' AND  dataModificare >= CONVERT(DATETIME, '" + pDataAnterioruluiExport + "', 104)";
        //  string vInterogare = "SELECT gospodarieId FROM gospodarii WHERE dataModificare >= '" + pDataAnterioruluiExport + "'";
        List<string> vListaGospodarii = ManipuleazaBD.fRezultaListaStringuri(vInterogare, "gospodarieId");
        return vListaGospodarii;
    }
    public void XMLScrieDataUltimuluiExport(string pCale)
    {
        string vInterogare = "INSERT INTO exporturi (exportData, exportOra, exportUnitateCodFiscal, utilizatorCNP, exportTip, exportCale, unitateId) VALUES (CONVERT(DATETIME,'" + DateTime.Now.Date.ToString() + "',104), '" + DateTime.Now.Hour.ToString() + "','" + ManipuleazaBD.fRezultaUnString("SELECT TOP(1) unitateCodFiscal FROM unitati WHERE unitateId='" + Session["SESunitateID"] + "'", "unitateCodFiscal") + "','" + ManipuleazaBD.fRezultaUnString("SELECT TOP(1) utilizatorCNP FROM utilizatori WHERE unitateId='" + Session["SESunitateID"] + "' AND utilizatorId='" + Session["SESutilizatorID"] + "'", "utilizatorCNP") + "', '0','" + pCale + "', '" + Session["SESunitateId"].ToString() + "')"; // 0 export, 1-import
        ManipuleazaBD.fManipuleazaBD(vInterogare);
    }
    public void XMLScrieDataUltimuluiImport(string pCale)
    {
        string vInterogare = @"INSERT INTO exporturi (exportData, exportOra, unitateId, utilizatorId, exportTip, exportCale) VALUES (CONVERT(DATETIME, '" + DateTime.Now.Date.ToString() + "', 104), '" + DateTime.Now.Hour.ToString() + "','" + Session["SESunitateId"] + "','" + Session["SESutilizatorId"] + "', '0','" + pCale + "')"; // 0 export, 1-import
        ManipuleazaBD.fManipuleazaBD(vInterogare);

    }
    public static void CreeazaXML(string pUnitate, string pUtilizator, string pDataInceput, string pGospodarie, XmlTextWriter writer)
    {


        //deschidem gospodarii
        writer.WriteStartElement("gospodarii");
        // scriem atribute gospodarie
        XMLScrieElement(writer, "gospodarii", "gospodarieId", pGospodarie);

        // scriem capitolul 1 - tabela membri
        string strConn = ConfigurationManager.ConnectionStrings["TNTRegistruAgricolConnectionString"].ConnectionString;
        SqlConnection con = new SqlConnection(strConn);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = "SELECT capitolId FROM membri WHERE gospodarieId='" + pGospodarie + "' AND unitateId = '" + pUnitate + "'";
        con.Open();
        List<string> vListaCapitoleIdMembri = new List<string> { };
        try
        {
            SqlDataReader vRezultat = cmd.ExecuteReader();
            while (vRezultat.Read())
            {
                vListaCapitoleIdMembri.Add(vRezultat["capitolId"].ToString());
            }
        }
        catch { }
        con.Close();
        // scriem atribute capitole - tabela membri
        foreach (string vCapitolId in vListaCapitoleIdMembri)
        {
            List<string> vCampuri = new List<string> { "gospodarieId", "unitateId", "capitolId" };
            List<string> vValori = new List<string> { pGospodarie, pUnitate, vCapitolId };
            // deschidem linia capitol
            writer.WriteStartElement("membri");
            XMLScrieElement(writer, "membri", vCampuri, vValori);
            // inchidem linia capitol
            writer.WriteEndElement();
        }

        // scriem Capitolele care intra in tabela parcele
        string[] vRezCapitolParcele = { "parcelaId" };
        cmd.CommandText = "SELECT parcelaId FROM parcele WHERE gospodarieId='" + pGospodarie + "' AND unitateaId = '" + pUnitate + "'";
        con.Open();
        List<string> vListaCapitoleIdParcele = new List<string> { };
        try
        {
            SqlDataReader vRezultat = cmd.ExecuteReader();
            while (vRezultat.Read())
            {
                vListaCapitoleIdParcele.Add(vRezultat["parcelaId"].ToString());
            }
        }
        catch { }
        con.Close();
        // scriem atribute capitole - tabela capitole
        foreach (string vParcelaId in vListaCapitoleIdParcele)
        {
            List<string> vCampuri = new List<string> { "gospodarieId", "unitateaId", "parcelaId" };
            List<string> vValori = new List<string> { pGospodarie, pUnitate, vParcelaId };
            // deschidem linia capitol
            writer.WriteStartElement("parcele");
            XMLScrieElement(writer, "parcele", vCampuri, vValori);
            // inchidem linia capitol
            writer.WriteEndElement();
        }

        // coordonatele 
        string[] vRezCoordonate = { "coordonataId" };
        cmd.CommandText = "SELECT coordonataId FROM coordonate WHERE gospodarieId='" + pGospodarie + "' AND unitateId = '" + pUnitate + "'";
        con.Open();
        List<string> vListaIdCoordonate = new List<string> { };
        try
        {
            SqlDataReader vRezultat = cmd.ExecuteReader();
            while (vRezultat.Read())
            {
                vListaIdCoordonate.Add(vRezultat["coordonataId"].ToString());
            }
        }
        catch { }
        con.Close();
        // scriem atribute coordonate parcele
        foreach (string vCoordonataId in vListaIdCoordonate)
        {
            List<string> vCampuri = new List<string> { "gospodarieId", "unitateId", "coordonataId" };
            List<string> vValori = new List<string> { pGospodarie, pUnitate, vCoordonataId };
            // deschidem linia capitol
            writer.WriteStartElement("coordonate");
            XMLScrieElement(writer, "coordonate", vCampuri, vValori);
            // inchidem linia capitol
            writer.WriteEndElement();
        }

        // scriem Capitolele care intra in tabela paduri
        string[] vRezCapitolPaduri = { "paduriId" };
        cmd.CommandText = "SELECT paduriId FROM paduri WHERE gospodarieId='" + pGospodarie + "' AND unitateId = '" + pUnitate + "'";
        con.Open();
        List<string> vListaCapitoleIdPaduri = new List<string> { };
        try
        {
            SqlDataReader vRezultat = cmd.ExecuteReader();
            while (vRezultat.Read())
            {
                vListaCapitoleIdPaduri.Add(vRezultat["paduriId"].ToString());
            }
        }
        catch { }
        con.Close();
        // scriem atribute capitole - tabela capitole
        foreach (string vPaduriId in vListaCapitoleIdPaduri)
        {
            List<string> vCampuri = new List<string> { "gospodarieId", "unitateId", "paduriId" };
            List<string> vValori = new List<string> { pGospodarie, pUnitate, vPaduriId };
            // deschidem linia capitol
            writer.WriteStartElement("paduri");
            XMLScrieElement(writer, "paduri", vCampuri, vValori);
            // inchidem linia capitol
            writer.WriteEndElement();
        }



        // scriem Capitolele care intra in tabela capitole
        string[] vRezCapitol2 = { "capitolId" };
        cmd.CommandText = "SELECT capitolId FROM capitole WHERE gospodarieId='" + pGospodarie + "' AND unitateId = '" + pUnitate + "'";
        con.Open();
        List<string> vListaCapitoleId2 = new List<string> { };
        try
        {
            SqlDataReader vRezultat = cmd.ExecuteReader();
            while (vRezultat.Read())
            {
                vListaCapitoleId2.Add(vRezultat["capitolId"].ToString());
            }
        }
        catch { }
        con.Close();
        // scriem atribute capitole - tabela capitole
        foreach (string vCapitolId in vListaCapitoleId2)
        {
            List<string> vCampuri = new List<string> { "gospodarieId", "unitateId", "capitolId" };
            List<string> vValori = new List<string> { pGospodarie, pUnitate, vCapitolId };
            // deschidem linia capitol
            writer.WriteStartElement("capitole");
            XMLScrieElement(writer, "capitole", vCampuri, vValori);
            // inchidem linia capitol
            writer.WriteEndElement();
        }
        // inchidem gospodarie
        writer.WriteEndElement();


    }

    public static void XMLScrieElement(XmlTextWriter pFisier, string pElement, string[] pAtribute, string[] pValori)
    {
        pFisier.WriteStartElement(pElement);
        int vContor = 0;
        foreach (string vAtribut in pAtribute)
        {
            pFisier.WriteAttributeString(vAtribut, pValori[vContor]);
            vContor++;
        }
        pFisier.WriteEndElement();
    }
    public static void XMLScrieElement(XmlTextWriter pFisier, string pTabela, string pWhereCamp, string pWhereValoare)
    {
        // scriem toate atributele pentru o tabela - luam numele coloanelor si le scriem ca atribute
        int vContor = 0;
        List<string> vListaNumeColoane = ManipuleazaBD.NumeleColoanelor(pTabela);
        // citim valorile pentru coloanele extrase
        string vInterogare = "SELECT ";
        foreach (string vNumeColoana in vListaNumeColoane)
        {
            if (vContor == 0) { vInterogare += vNumeColoana; }
            else { vInterogare += ", " + vNumeColoana; }
            vContor++;
        }
        vInterogare += " FROM " + pTabela + " WHERE " + pWhereCamp + "='" + pWhereValoare + "'";
        List<string> vListaRezultate = ManipuleazaBD.fRezultaUnString(vInterogare, vListaNumeColoane);
        // scriem atributele
        vContor = 0;
        foreach (string vAtribut in vListaNumeColoane)
        {
            pFisier.WriteAttributeString(vAtribut, vListaRezultate[vContor]);
            vContor++;
        }

    }
    public static void XMLScrieElement(XmlTextWriter pFisier, string pTabela, List<string> pWhereCampuri, List<string> pWhereValori)
    {
        // scriem toate atributele pentru o tabela - luam numele coloanelor si le scriem ca atribute
        int vContor = 0;
        List<string> vListaNumeColoane = ManipuleazaBD.NumeleColoanelor(pTabela);
        // citim valorile pentru coloanele extrase
        string vInterogare = "SELECT ";
        foreach (string vNumeColoana in vListaNumeColoane)
        {
            if (vContor == 0) { vInterogare += vNumeColoana; }
            else { vInterogare += ", " + vNumeColoana; }
            vContor++;
        }
        vInterogare += " FROM " + pTabela + " WHERE ";
        vContor = 0;
        foreach (string vCamp in pWhereCampuri)
        {
            if (vContor == 0) { vInterogare += vCamp + "='" + pWhereValori[vContor] + "'"; }
            else { vInterogare += "AND " + vCamp + "='" + pWhereValori[vContor] + "'"; }
            vContor++;
        }
        List<string> vListaRezultate = ManipuleazaBD.fRezultaUnString(vInterogare, vListaNumeColoane);
        // scriem atributele
        vContor = 0;
        foreach (string vAtribut in vListaNumeColoane)
        {
            pFisier.WriteAttributeString(vAtribut, vListaRezultate[vContor]);
            vContor++;
        }

    }
    public static void XMLScrieElement(XmlTextWriter pFisier, string pElement, int p)
    {
        pFisier.WriteStartElement(pElement);
        pFisier.WriteEndElement();
    }
    public static void XMLScrieElement(XmlTextWriter pFisier, string pParinte, string pElement, string[] pAtribute, string[] pValori)
    {
        pFisier.WriteStartElement(pElement);
        pFisier.WriteAttributeString(pAtribute[1], pValori[1]);
        pFisier.WriteEndElement();
    }

    protected void XMLIncepeDocumentul(string pUnitate, string pUtilizator, string pDataInceput, XmlTextWriter writer)
    {
        writer.WriteStartDocument();
        writer.WriteComment("Fisier ce contine datele de export pentru Registrul Agricol - TNT Computers SRL Sibiu office@tntcomputers.ro");
        writer.WriteStartElement("Export");
        writer.WriteAttributeString("unitateId", pUnitate);

        writer.WriteAttributeString("unitateCodFiscal", ManipuleazaBD.fRezultaUnString("SELECT TOP(1) unitateCodFiscal FROM unitati WHERE unitateId='" + pUnitate + "'", "unitateCodFiscal"));
        writer.WriteAttributeString("utilizatorId", pUtilizator);
        writer.WriteAttributeString("utilizatorCNP", ManipuleazaBD.fRezultaUnString("SELECT TOP(1) utilizatorCNP FROM utilizatori WHERE unitateId='" + pUnitate + "' AND utilizatorId='" + pUtilizator + "'", "utilizatorCNP"));
        writer.WriteAttributeString("dataAnterioruluiExport", pDataInceput);
        writer.WriteAttributeString("dataActualuluiExport", DateTime.Now.Date.ToString());
        writer.WriteAttributeString("an", Session["SESan"].ToString());
    }
    protected void XMLInchideDocumentul(XmlTextWriter writer)
    {
        writer.WriteEndElement();
        writer.WriteEndDocument();
        writer.Close();
    }
    protected string XMLCreeazaDenumireFisier()
    {
        string vDenumire = "";
        string vInterogare = "SELECT unitateCodFiscal FROM unitati WHERE unitateId='" + Session["SESunitateId"].ToString() + "'";
        vDenumire += ManipuleazaBD.fRezultaUnString(vInterogare, "unitateCodFiscal") + "-" + DateTime.Now.Day.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + "-" + DateTime.Now.Minute.ToString() + ".xml";
        return vDenumire;
    }
    protected string XMLCaleFisier()
    {
        string vDenumire = System.AppDomain.CurrentDomain.BaseDirectory.ToString() + "Exporturi\\" + XMLCreeazaDenumireFisier();
        return vDenumire;
    }
    protected void btExporta_Click(object sender, EventArgs e)
    {

        string vCale = XMLCaleFisier();
        string vNumeFisier = XMLCreeazaDenumireFisier();
        ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "EXPORT", "incepe exportul" + "| nume fisier: " + vCale, "", Convert.ToInt64(Session["SESgospodarieId"]), 6);
        Label1.Text = vNumeFisier;
        try
        {
            XmlTextWriter writer = new XmlTextWriter(vCale, null);
            XMLIncepeDocumentul(Session["SESunitateId"].ToString(), Session["SESutilizatorId"].ToString(), XMLCitesteDataAnterioruluiExport(), writer);
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "EXPORT", "a inceput documentul | va avea de scris un nr. de " + XMLListaGospodariiDeScris(XMLCitesteDataAnterioruluiExport()).Count().ToString() + " gospodarii", "", Convert.ToInt64(Session["SESgospodarieId"]), 6);
            // citim gospodariile actualizate de la data anteriorului export
            foreach (string vGospodarieId in XMLListaGospodariiDeScris(XMLCitesteDataAnterioruluiExport()))
            {
                CreeazaXML(Session["SESunitateId"].ToString(), Session["SESutilizatorId"].ToString(), DateTime.Now.ToString(), vGospodarieId, writer);
            }
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "EXPORT", "a terminat de scris gospodariile", "", Convert.ToInt64(Session["SESgospodarieId"]), 6);
            // Ends the document
            XMLInchideDocumentul(writer);
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "EXPORT", "a inchis documentul", "", Convert.ToInt64(Session["SESgospodarieId"]), 6);

            // scriem data exportului tocmai incheiat
            //facem xml -> zip
            XMLZip(vCale);

            XMLScrieDataUltimuluiExport(vNumeFisier);
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "EXPORT", "a arhivat documentul si a scris in Export data ultimului export", "", Convert.ToInt64(Session["SESgospodarieId"]), 6);

        }
        catch
        {
            Label1.Text = "Eroare la exportarea datelor!"; Label1.Visible = true;
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "EXPORT", "a dat o eroare la export!!!", "", Convert.ToInt64(Session["SESgospodarieId"]), 6);
        }
        gvExporturi.DataBind();
    }

    protected void XMLUnZip(string pCale)
    {
        // dezarhivam zipul de importat
        Stream inFile = new FileStream(pCale, FileMode.Open, FileAccess.Read, FileShare.Read);
        Stream decodedStream = new MemoryStream();
        byte[] buffer = new byte[1];
        string vCaleFinala = pCale.Remove(pCale.IndexOf(".zip"));
        //   (Label)(((Repeater).FindControl[""]).Text = "Se dezarhivează fişierul " + pCale;
        //   upStare.Update();
        try
        {
            FileStream fs = File.Create(vCaleFinala);
            using (Stream inGzipStream = new GZipStream(inFile, CompressionMode.Decompress))
            {
                int bytesRead;
                while ((bytesRead = inGzipStream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    // decodedStream.Write(buffer, 0, bytesRead);
                    fs.Write(buffer, 0, buffer.Length);
                }
            }
            fs.Close();
            inFile.Close();
            //      lblMesaj1.Text = "S-a dezarhivat fişierul " + pCale;
            //    upStare.Update();
        }
        catch
        {
            //           lblMesaj1.Text = "A intervenit o eroare la dezarhivarea fişierului " + pCale;
            //      upStare.Update();
            inFile.Close();
        }

    }

    protected void XMLZip(string pCale)
    {
        FileStream sourceFile = File.OpenRead(pCale);
        FileStream destFile = File.Create(pCale + ".zip");
        // facem zip?
        GZipStream compStream = new GZipStream(destFile, CompressionMode.Compress);

        try
        {
            int theByte = sourceFile.ReadByte();
            while (theByte != -1)
            {
                compStream.WriteByte((byte)theByte);
                theByte = sourceFile.ReadByte();
            }
        }
        finally
        {
            compStream.Dispose();
        }
        sourceFile.Close();
        destFile.Close();
    }
    public static string vFisier = "";
    protected string XMLAlegeFisier(object sender, EventArgs e)
    {
        string filepath = "";
        if (fuImporta.HasFile)
        {
            // alegem fisierul de importat
            filepath = fuImporta.PostedFile.FileName;
            vFisier = filepath;
            if (filepath.IndexOf(".zip") != (filepath.Length - 4) && filepath.IndexOf(".xml") != (filepath.Length - 4))
            {
                valCustom.IsValid = false;
                valCustom.ErrorMessage = "Fișierul pentru importul datelor trebuie sa fie de tip .zip sau .xml!";

                //       lblMesaj1.Text = "S-a ales fişierul " + filepath;
                //       upStare.Update();
                filepath = ""; return filepath;
            }
            // il copiem in directorul de importuri
            //      GridView1.Rows[0].Cells[1].Text = "sadgfdsgsg";
            //lblMesaj1.Text = "Se copiaza pe server fişierul " + filepath;
            //      GridView1.DataBind();
            //      upStare.Update();
            try
            {
                fuImporta.PostedFile.SaveAs(Server.MapPath(".\\Exporturi\\Importuri\\") + filepath);
                //           lblMesaj1.Text = "S-a copiat pe server fişierul " + filepath;
            }
            catch
            {
                //            lblMesaj1.Text = "A intervenit o eroare la copierea pe server a fisierului " + filepath;
            }
            //      upStare.Update();
            if (filepath.IndexOf(".zip") == (filepath.Length - 4))
            {
                XMLUnZip(Server.MapPath(".\\Exporturi\\Importuri\\") + filepath);
                Label1.Text = "Fisier salvat în: " + Server.MapPath(".\\Exporturi\\Importuri\\") + filepath;
                filepath = Server.MapPath(".\\Exporturi\\Importuri\\") + filepath.Remove(filepath.IndexOf(".zip"));
            }
            else
            {
                filepath = Server.MapPath(".\\Exporturi\\Importuri\\") + filepath;
            }
            //     upStare.Update();

        }
        return filepath;
    }
    protected void btImporta_Click(object sender, EventArgs e)
    {
        // deschidem fiserul recent copiat si dezarhivat cu XMLAlegefisier
        ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "IMPORT", "incepem importul", "", Convert.ToInt64(Session["SESgospodarieId"]), 7);
        string vCaleImportZip = XMLAlegeFisier(sender, e);

        //     string vCaleImportZip = Server.MapPath(".\\Exporturi\\Importuri\\") + "exportDate-4241222-15.03.2011.xml";
        if (vCaleImportZip == "")
        {
            valCustom.IsValid = false;
            valCustom.ErrorMessage = "Alegeți un fișier .zip sau .xml pentru importul datelor!";
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "IMPORT", "a ales un fisier invalid: " + fuImporta.FileName, "", Convert.ToInt64(Session["SESgospodarieId"]), 7);
            return;
        }
        ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "IMPORT", "1.fisierul de import este " + vCaleImportZip, "", Convert.ToInt64(Session["SESgospodarieId"]), 7);
        //    XmlTextReader reader = new XmlTextReader(vCaleImportZip.Remove(vCaleImportZip.IndexOf(".zip")));
        XmlTextReader reader = new XmlTextReader(vCaleImportZip);

        List<string> vListaElemente = new List<string> { };
        // citim elementul Export
        List<string> vCampuri = new List<string> { };
        List<string> vValori = new List<string> { };
        List<string> vCampuriId = new List<string> { };
        List<string> vValoriId = new List<string> { };
        int vContor = 0;
        int vContorGospodarii = 0;
        string vNume = "";
        string vUnitate = "";
        while (reader.Read())
        {
            string vMesaj = "";
            XMLCitesteElement(reader, "Export", vCaleImportZip, out vUnitate, out vMesaj);

            if (vUnitate == "")
            {
                valCustom.IsValid = false;
                valCustom.ErrorMessage = vMesaj;
                ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "IMPORT", "2. eroare la citirea headerului: " + vMesaj, "", Convert.ToInt64(Session["SESgospodarieId"]), 7);
                return;
            }
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "IMPORT", "2. a citit headerul: unitate = " + vUnitate, "", Convert.ToInt64(Session["SESgospodarieId"]), 7);
            vContor++;
            break;
        }
        //       lblMesaj1.Text = "S-au citit si scris datele datele Exportului... ";
        //    upStare.Update();

        //   .FindControl["lblNrGospodariiRezolvate"]).Text = 
        //******************************************************************
        // id de unitate il luam pe cel din baza de date, nu pe cel din xml!
        //*******************************************************************
        string vUnitateId = vUnitate;
        //*******************************************************************
        // luam si id-ul de an
        string vAn = reader.GetAttribute("an");

        if (vAn == "" || vAn == null)
        {
            vAn = DateTime.Now.Year.ToString();
        }
        // ne rezulta vListaValoriAtribute si vListaNumeAtribute pe care vom introduce in bd
        // definim tranzactia
        ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "IMPORT", "3. anul =  " + vAn + " | incepem citirea gospodariilor", "", Convert.ToInt64(Session["SESgospodarieId"]), 7);
        int numarGospodarii = 0;
        string vGospodarieId = "";
        try
        {
            string vNumeElementPrincipal = "";
            while (reader.Read())
            {
                SqlConnection vCon = ManipuleazaBD.CreareConexiune();
                SqlTransaction vTranz = ManipuleazaBD.DeschideTranzactieFaraRestrictii(vCon, System.Data.IsolationLevel.ReadUncommitted);
                SqlCommand vCmd = new SqlCommand();
                vCmd.Connection = vCon;
                vCmd.Transaction = vTranz;
                try
                {
                    if (reader.Name == "gospodarii")
                    {
                        numarGospodarii++;

                        // citim in continuare sa luam atributele gospodariei
                        //*******************************************************************************
                        // nu tinem seama de id-ul din xml ci citim volum si pozitie (pe langa idunitate)
                        // cu acestea citim din bd id-ul gospodariei; daca nu exista cream o gospodarie noua
                        //*******************************************************************************
                        vGospodarieId = ManipuleazaBD.fRezultaUnString("SELECT gospodarieId FROM gospodarii WHERE volum ='" + reader.GetAttribute("volum") + "' AND nrPozitie ='" + reader.GetAttribute("nrPozitie") + "' AND unitateId='" + vUnitateId + "' AND an = '" + vAn + "'", "gospodarieId");
                        ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "IMPORT", "4. " + numarGospodarii.ToString() + ". incepe pt gospodaria: " + vGospodarieId + "(volum = " + reader.GetAttribute("volum").ToString() + ", nrPozitie =" + reader.GetAttribute("nrPozitie").ToString() + ")", "", Convert.ToInt64(Session["SESgospodarieId"]), 7);
                        //??????????????????
                        // care este anul??? cel din xml sau cel curent la care se face importul??? 
                        //string  vAn = reader.GetAttribute("an");
                        //??????????????????
                        //string vAn = DateTime.Now.Year.ToString();
                        if (vGospodarieId != "" && vGospodarieId != null)
                        {
                            string vInterogareCapitole = "DELETE FROM capitole WHERE gospodarieId='" + vGospodarieId + "' AND unitateId='" + vUnitateId + "' AND an='" + vAn + "'";
                            vCmd.CommandText = vInterogareCapitole;
                            vCmd.ExecuteNonQuery();
                            string vInterogareParcele = "DELETE FROM parcele WHERE gospodarieId='" + vGospodarieId + "' AND unitateaId='" + vUnitateId + "' AND an='" + vAn + "'";
                            vCmd.CommandText = vInterogareParcele;
                            vCmd.ExecuteNonQuery();
                            string vInterogareCoordonate = "DELETE FROM coordonate WHERE gospodarieId='" + vGospodarieId + "' AND unitateId='" + vUnitateId + "' AND an='" + vAn + "'";
                            vCmd.CommandText = vInterogareCoordonate;
                            vCmd.ExecuteNonQuery();
                            string vInterogarePaduri = "DELETE FROM paduri WHERE gospodarieId='" + vGospodarieId + "' AND unitateId='" + vUnitateId + "' AND an='" + vAn + "'";
                            vCmd.CommandText = vInterogarePaduri;
                            vCmd.ExecuteNonQuery();
                            string vInterogareMembri = "DELETE FROM membri WHERE gospodarieId='" + vGospodarieId + "' AND unitateId='" + vUnitateId + "'";// AND an='" + vAn + "'";
                            vCmd.CommandText = vInterogareMembri;
                            vCmd.ExecuteNonQuery();
                            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "IMPORT", "4. " + numarGospodarii.ToString() + ". sterge din tabelele de capitole toate inregistrarile despre gospodarie", "", Convert.ToInt64(Session["SESgospodarieId"]), 7);
                        }
                        reader.MoveToElement();
                        vNumeElementPrincipal = "gospodarii";
                        // stergem capitolele din gospodarii, membri, parcelele si padurile si facem update la gospodarii 
                    }
                    else
                    {
                        vNumeElementPrincipal = "";
                    }
                    // initializam listele
                    vListaNumeAtribute.Clear();
                    vListaValoriAtribute.Clear();
                    vCampuri.Clear();
                    vValori.Clear();
                    vValoriId.Clear();
                    vCampuriId.Clear();
                    vContorGospodarii = 0;
                    vNume = reader.Name; // de aici ne da tabela unde inseram
                    // daca e inceputul de tag; daca e final atunci nu avem atribute
                    while (vContorGospodarii < reader.AttributeCount)
                    {
                        // citim atributele pentru Element
                        reader.MoveToNextAttribute();
                        vListaNumeAtribute.Add(reader.Name);
                        if (reader.Name == "unitateId")
                        {
                            vListaValoriAtribute.Add(vUnitateId);
                        }
                        else
                        {
                            vListaValoriAtribute.Add(reader.GetAttribute(reader.Name));
                        }
                        vContorGospodarii++;
                    }
                    // daca e tip gospodarii : facem update daca exista si insert una noua daca nu exista gospodaria in bd
                    if (vNumeElementPrincipal == "gospodarii")
                    {
                        // try
                        //  {
                        // facem update la gospodarii
                        int vContorX = 0;
                        foreach (string vCampGospodarii in vListaNumeAtribute)
                        {
                            if (vContorX > 0)
                            {
                                vCampuri.Add(vCampGospodarii);
                                // daca e tip data convertim
                                if (vCampGospodarii == "dataModificare")
                                {
                                    vValori.Add("###" + vListaValoriAtribute[vContorX]);
                                }
                                else
                                {
                                    // daca e nrInt scoatem numarul din el
                                    if (vCampGospodarii == "nrInt")
                                        vValori.Add(ScoateNumere(vListaValoriAtribute[vContorX]));
                                    else
                                        vValori.Add(vListaValoriAtribute[vContorX]);
                                }
                            }
                            vContorX++;
                        }
                        if (vListaNumeAtribute.Count() > 1)
                        {
                            vCampuriId.Add("unitateId");
                            vCampuriId.Add("gospodarieId");
                            //vValoriId.Add(vListaValoriAtribute[vListaNumeAtribute.IndexOf("unitateId")]);
                            vValoriId.Add(vUnitateId);
                            // vValoriId.Add(vListaValoriAtribute[vListaNumeAtribute.IndexOf("gospodarieId")]);
                            // cautam sa vedem daca exista gospodaria
                            // dupa Unitate, Volum si Pozitie
                            string[] vNumeCampuriGospodarie = { "an", "volum", "nrPozitie", "unitateId" };
                            string[] vValoriCampuriGospodarie = { vAn, vListaValoriAtribute[vListaNumeAtribute.IndexOf("volum")], vListaValoriAtribute[vListaNumeAtribute.IndexOf("nrPozitie")], vUnitateId };
                            int vExistaGosp = ManipuleazaBD.fVerificaExistentaArray("gospodarii", vNumeCampuriGospodarie, vValoriCampuriGospodarie, " AND ");
                            if (vExistaGosp == 1)
                            {
                                //     vGospodarieId = ManipuleazaBD.fRezultaUnString("SELECT gospodarieId FROM gospodarii WHERE volum ='" + vListaValoriAtribute[vListaNumeAtribute.IndexOf("volum")] + "' AND nrPozitie ='" + vListaValoriAtribute[vListaNumeAtribute.IndexOf("nrPozitie")] + "' AND unitateId='" + vUnitateId + "'", "gospodarieId");
                                vValoriId.Add(vGospodarieId);
                                vCmd.CommandText = ManipuleazaBD.CreeazaComanda(1, "gospodarii", vCampuri, vValori, vCampuriId, vValoriId);
                                vCmd.ExecuteNonQuery();
                            }
                            else
                            {
                                int vGospodarieNoua = 0;
                                ManipuleazaBD.fManipuleazaBDArray(out vGospodarieNoua, "gospodarii", vCampuri, vValori);
                                vGospodarieId = vGospodarieNoua.ToString();
                                // scriem gospodarieidinitial
                                ManipuleazaBD.fManipuleazaBD("UPDATE gospodarii SET gospodarieIdInitial = '" + vGospodarieId + "' WHERE gospodarieId = '" + vGospodarieId + "'; UPDATE gospodarii SET an = '" + vAn + "' WHERE  gospodarieId = '" + vGospodarieId + "';");
                                //vCmd.CommandText = ManipuleazaBD.CreeazaComanda(0, "gospodarii", vCampuri, vValori, vCampuriId, vValoriId);
                            }
                            //vCmd.CommandText = ManipuleazaBD.CreeazaComanda(1, "gospodarii", vCampuri, vValori, vCampuriId, vValoriId);
                            // try
                            // {
                            //     vCmd.ExecuteNonQuery();
                            // }
                            // catch { }
                            // am finalizat update la gospodarii
                        }
                        //  }
                        //  catch { }
                    }
                    // daca nu sunt gospodarii DELETE + INSERT
                    else //if (vNume == "parcele") //if (vUnitateId=="11111111")
                    {

                        // am sters mai sus TOT ce a tinut de gospodarie, acum inseram pe rand
                        if (reader.AttributeCount > 0)
                        {
                            // facem INSERT la celelalte elemente altele decat GOSPODARII
                            int vContorY = 0;
                            foreach (string vCampAltele in vListaNumeAtribute)
                            {
                                if (vContorY > 0) // primul element e id-ul, nu il inseram - altfel da eroare
                                {
                                    vCampuri.Add(vCampAltele);
                                    // daca e tip data convertim; avem cazul dataNasterii la membri
                                    switch (vCampAltele)
                                    {

                                        case "unitateId":
                                            vValori.Add(vUnitateId);
                                            break;
                                        case "gospodarieId":
                                            vValori.Add(vGospodarieId);
                                            break;
                                        case "parcelaSuprafataIntravilanHa":
                                            if (vListaValoriAtribute[vContorY] == "" || vListaValoriAtribute[vContorY] == null || vListaValoriAtribute[vContorY] == "0,000000")
                                            {
                                                vValori.Add("0");
                                            }
                                            else
                                            {
                                                if (vListaValoriAtribute[vContorY].IndexOf('.') > 0)
                                                {
                                                    vValori.Add(vListaValoriAtribute[vContorY].Remove(vListaValoriAtribute[vContorY].IndexOf('.')));
                                                }
                                                else
                                                {
                                                    vValori.Add(vListaValoriAtribute[vContorY]);
                                                }

                                            }
                                            break;
                                        case "parcelaSuprafataExtravilanHa":
                                            if (vListaValoriAtribute[vContorY] == "" || vListaValoriAtribute[vContorY] == null || vListaValoriAtribute[vContorY] == "0,000000")
                                            {
                                                vValori.Add("0");
                                            }
                                            else
                                            {
                                                if (vListaValoriAtribute[vContorY].IndexOf('.') > 0)
                                                {
                                                    vValori.Add(vListaValoriAtribute[vContorY].Remove(vListaValoriAtribute[vContorY].IndexOf('.')));
                                                }
                                                else
                                                {
                                                    vValori.Add(vListaValoriAtribute[vContorY]);
                                                }
                                            }
                                            break;
                                        case "paduriSuprafataHa":
                                            if (vListaValoriAtribute[vContorY] == "" || vListaValoriAtribute[vContorY] == null || vListaValoriAtribute[vContorY] == "0,000000")
                                            {
                                                vValori.Add("0");
                                            }
                                            else
                                            {
                                                if (vListaValoriAtribute[vContorY].IndexOf('.') > 0)
                                                {
                                                    vValori.Add(vListaValoriAtribute[vContorY].Remove(vListaValoriAtribute[vContorY].IndexOf('.')));
                                                }
                                                else
                                                {
                                                    vValori.Add(vListaValoriAtribute[vContorY]);
                                                }
                                            }
                                            break;
                                        case "paduriSuprafataGrupa2Ha":
                                            if (vListaValoriAtribute[vContorY] == "" || vListaValoriAtribute[vContorY] == null || vListaValoriAtribute[vContorY] == "0,000000")
                                            {
                                                vValori.Add("0");
                                            }
                                            else
                                            {
                                                if (vListaValoriAtribute[vContorY].IndexOf('.') > 0)
                                                {
                                                    vValori.Add(vListaValoriAtribute[vContorY].Remove(vListaValoriAtribute[vContorY].IndexOf('.')));
                                                }
                                                else
                                                {
                                                    vValori.Add(vListaValoriAtribute[vContorY]);
                                                }
                                            }
                                            break;
                                        case "dataNasterii":
                                            vValori.Add("###" + vListaValoriAtribute[vContorY]);
                                            break;
                                        case "parcelaSuprafataIntravilanAri":
                                            if (vListaValoriAtribute[vContorY] == "" || vListaValoriAtribute[vContorY] == null || vListaValoriAtribute[vContorY] == "0,000000")
                                            {
                                                vValori.Add("0");
                                            }
                                            else
                                            {
                                                vValori.Add(vListaValoriAtribute[vContorY].Replace(',', '.'));
                                            }
                                            break;
                                        case "parcelaSuprafataExtravilanAri":
                                            if (vListaValoriAtribute[vContorY] == "" || vListaValoriAtribute[vContorY] == null || vListaValoriAtribute[vContorY] == "0,000000")
                                            {
                                                vValori.Add("0");
                                            }
                                            else
                                            {
                                                vValori.Add(vListaValoriAtribute[vContorY].Replace(',', '.'));
                                            }
                                            break;
                                        case "paduriSuprafataAri":
                                            if (vListaValoriAtribute[vContorY] == "" || vListaValoriAtribute[vContorY] == null || vListaValoriAtribute[vContorY] == "0,000000")
                                            {
                                                vValori.Add("0");
                                            }
                                            else
                                            {
                                                vValori.Add(vListaValoriAtribute[vContorY].Replace(',', '.'));
                                            }
                                            break;
                                        case "paduriSuprafataGrupa2Ari":
                                            if (vListaValoriAtribute[vContorY] == "" || vListaValoriAtribute[vContorY] == null || vListaValoriAtribute[vContorY] == "0,000000")
                                            {
                                                vValori.Add("0");
                                            }
                                            else
                                            {
                                                vValori.Add(vListaValoriAtribute[vContorY].Replace(',', '.'));
                                            }
                                            break;
                                        case "col1":
                                            if (vListaValoriAtribute[vContorY] == "" || vListaValoriAtribute[vContorY] == null)
                                            {
                                                vValori.Add("0");
                                            }
                                            else
                                            {
                                                vValori.Add(vListaValoriAtribute[vContorY].Replace(',', '.'));
                                            }
                                            break;
                                        case "col2":
                                            if (vListaValoriAtribute[vContorY] == "" || vListaValoriAtribute[vContorY] == null)
                                            {
                                                vValori.Add("0");
                                            }
                                            else
                                            {
                                                vValori.Add(vListaValoriAtribute[vContorY].Replace(',', '.'));
                                            }
                                            break;
                                        case "col3":
                                            if (vListaValoriAtribute[vContorY] == "" || vListaValoriAtribute[vContorY] == null)
                                            {
                                                vValori.Add("0");
                                            }
                                            else
                                            {
                                                vValori.Add(vListaValoriAtribute[vContorY].Replace(',', '.'));
                                            }
                                            break;
                                        case "col4":
                                            if (vListaValoriAtribute[vContorY] == "" || vListaValoriAtribute[vContorY] == null)
                                            {
                                                vValori.Add("0");
                                            }
                                            else
                                            {
                                                vValori.Add(vListaValoriAtribute[vContorY].Replace(',', '.'));
                                            }
                                            break;
                                        case "col5":
                                            if (vListaValoriAtribute[vContorY] == "" || vListaValoriAtribute[vContorY] == null)
                                            {
                                                vValori.Add("0");
                                            }
                                            else
                                            {
                                                vValori.Add(vListaValoriAtribute[vContorY].Replace(',', '.'));
                                            }
                                            break;
                                        case "col6":
                                            if (vListaValoriAtribute[vContorY] == "")
                                            {
                                                vValori.Add("0");
                                            }
                                            else
                                            {
                                                vValori.Add(vListaValoriAtribute[vContorY].Replace(',', '.'));
                                            }
                                            break;
                                        case "col7":
                                            if (vListaValoriAtribute[vContorY] == "" || vListaValoriAtribute[vContorY] == null)
                                            {
                                                vValori.Add("0");
                                            }
                                            else
                                            {
                                                vValori.Add(vListaValoriAtribute[vContorY].Replace(',', '.'));
                                            }
                                            break;
                                        case "col8":
                                            if (vListaValoriAtribute[vContorY] == "" || vListaValoriAtribute[vContorY] == null)
                                            {
                                                vValori.Add("0");
                                            }
                                            else
                                            {
                                                vValori.Add(vListaValoriAtribute[vContorY].Replace(',', '.'));
                                            }
                                            break;
                                        case "coordonataX":
                                            if (vListaValoriAtribute[vContorY] == "" || vListaValoriAtribute[vContorY] == null)
                                            {
                                                vValori.Add("0");
                                            }
                                            else
                                            {
                                                vValori.Add(vListaValoriAtribute[vContorY].Replace(',', '.'));
                                            }
                                            break;
                                        case "coordonataY":
                                            if (vListaValoriAtribute[vContorY] == "" || vListaValoriAtribute[vContorY] == null)
                                            {
                                                vValori.Add("0");
                                            }
                                            else
                                            {
                                                vValori.Add(vListaValoriAtribute[vContorY].Replace(',', '.'));
                                            }
                                            break;
                                        case "coordonataZ":
                                            if (vListaValoriAtribute[vContorY] == "" || vListaValoriAtribute[vContorY] == null)
                                            {
                                                vValori.Add("0");
                                            }
                                            else
                                            {
                                                vValori.Add(vListaValoriAtribute[vContorY].Replace(',', '.'));
                                            }
                                            break;
                                        case "parcelaNrCadastru":
                                            if (vListaValoriAtribute[vContorY] == "" || vListaValoriAtribute[vContorY] == null)
                                            {
                                                vValori.Add("0");
                                            }
                                            else
                                            {
                                                vValori.Add(vListaValoriAtribute[vContorY]);
                                            }
                                            break;
                                        case "parcelaNrCadastruProvizoriu":
                                            if (vListaValoriAtribute[vContorY] == "" || vListaValoriAtribute[vContorY] == null)
                                            {
                                                vValori.Add("0");
                                            }
                                            else
                                            {
                                                vValori.Add(vListaValoriAtribute[vContorY]);
                                            }
                                            break;
                                        case "parcelaLocalitate":
                                            if (vListaValoriAtribute[vContorY] == "" || vListaValoriAtribute[vContorY] == null)
                                            {
                                                vValori.Add("-");
                                            }
                                            else
                                            {
                                                vValori.Add(vListaValoriAtribute[vContorY]);
                                            }
                                            break;
                                        case "parcelaAdresa":
                                            if (vListaValoriAtribute[vContorY] == "" || vListaValoriAtribute[vContorY] == null)
                                            {
                                                vValori.Add("-");
                                            }
                                            else
                                            {
                                                vValori.Add(vListaValoriAtribute[vContorY]);
                                            }
                                            break;
                                        case "importIdssghdshsghjkshgsdhgkdhsgkhskgjhsgkjhsk":
                                            {
                                                vValori.Add(DateTime.Now.Day.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString());
                                            }
                                            break;
                                        default:
                                            vValori.Add(vListaValoriAtribute[vContorY]);
                                            break;
                                    }
                                }
                                vContorY++;
                            }
                            // special pt parcele unde avem unitateaId nu unitateId
                            if (vNume == "parcele")
                            {
                                try
                                {
                                    vCampuri[vCampuri.IndexOf("unitateId")] = "unitateaId";
                                }
                                catch { }
                            }
                            if (vNume == "membri")
                            { }
                            vCmd.CommandText = ManipuleazaBD.CreeazaComanda(0, vNume, vCampuri, vValori, vCampuriId, vValoriId);
                            vCmd.ExecuteNonQuery();
                            // am finalizat insert la altele decat gospodarii
                        }
                    }
                    reader.MoveToElement(); vContor++;
                    vTranz.Commit();
                }
                catch
                {
                    if (numarGospodarii % 2 != 0)
                    {
                        try
                        {
                            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "IMPORT", "4. " + numarGospodarii.ToString() + ". EROARE la gospodaria: " + vGospodarieId + "(volum = " + reader.GetAttribute("volum").ToString() + ", nrPozitie =" + reader.GetAttribute("nrPozitie").ToString() + ")", "", Convert.ToInt64(Session["SESgospodarieId"]), 7);
                        }
                        catch
                        { }
                    }
                    vTranz.Rollback();
                }
                finally
                {
                    ManipuleazaBD.InchideConexiune(vCon);
                    if (numarGospodarii % 2 != 0)
                    {
                        try
                        {
                            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "IMPORT", "4. " + numarGospodarii.ToString() + ". a terminat pt gospodaria: " + vGospodarieId + "(A parcurs " + numarGospodarii.ToString() + "/2)(volum = " + reader.GetAttribute("volum").ToString() + ", nrPozitie =" + reader.GetAttribute("nrPozitie").ToString() + ")", "", Convert.ToInt64(Session["SESgospodarieId"]), 7);
                        }
                        catch
                        {

                        }
                    }
                }
            }
            reader.Close();
            if (numarGospodarii % 2 != 0)
            {
                try
                {
                    ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "IMPORT", "5.  a terminat importul. A parcurs " + (numarGospodarii / 2).ToString() + " gospodarii", "", Convert.ToInt64(Session["SESgospodarieId"]), 7);
                }
                catch
                { }
            }
        }
        catch { }
        XMLCompleteazaNull(vUnitate, vAn);

        gvImporturi.DataBind();
        // final tranzactie
    }
    public void XMLCompleteazaNull(string vUnitate, string vAn)
    {
        ManipuleazaBD.fManipuleazaBD("UPDATE parcele SET parcelaNrCadastral ='0' WHERE parcelaNrCadastral IS NULL  AND unitateaId = '" + vUnitate + "'");
        ManipuleazaBD.fManipuleazaBD("UPDATE parcele SET parcelaNrCadastralProvizoriu ='0' WHERE parcelaNrCadastralProvizoriu IS NULL  AND unitateaId = '" + vUnitate + "'");
        ManipuleazaBD.fManipuleazaBD("UPDATE parcele SET parcelaLocalitate ='-' WHERE parcelaLocalitate IS NULL  AND unitateaId = '" + vUnitate + "'");
        ManipuleazaBD.fManipuleazaBD("UPDATE parcele SET parcelaAdresa ='-' WHERE parcelaAdresa IS NULL  AND unitateaId = '" + vUnitate + "'");
        ManipuleazaBD.fManipuleazaBD("UPDATE parcele SET parcelaIdInitial = parcelaId WHERE parcelaIdInitial IS NULL  AND unitateaId = '" + vUnitate + "'");
        ManipuleazaBD.fManipuleazaBD("UPDATE gospodarii SET gospodarieCui ='-' WHERE gospodarieCui IS NULL  AND unitateId = '" + vUnitate + "'");

        //ManipuleazaBD.fManipuleazaBD("UPDATE parcele SET parcelaNrCadastral ='' WHERE parcelaNrCadastralProvizoriu = NULL ");
        // scriem si volum si pozitie int
        List<string> vValoare = new List<string> { };
        string strConn = ConfigurationManager.ConnectionStrings["TNTRegistruAgricolConnectionString"].ConnectionString;
        SqlConnection con = new SqlConnection(strConn);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = "SELECT volum, nrPozitie, gospodarieId FROM gospodarii WHERE (volumInt IS NULL) OR (nrPozitieInt IS NULL)";
        con.Open();
        int cont = 0;
        string[] vVolPoz = new string[] { "", "" };
        try
        {
            SqlDataReader vRezultat = cmd.ExecuteReader();
            while (vRezultat.Read())
            {
                vVolPoz = new string[] { "", "" };
                vVolPoz[0] = ScoateNumere(Convert.ToString(vRezultat["volum"]));
                vVolPoz[1] = ScoateNumere(Convert.ToString(vRezultat["nrPozitie"]));
                ManipuleazaBD.fManipuleazaBD("UPDATE gospodarii SET volumInt = '" + vVolPoz[0] + "', nrPozitieInt = '" + vVolPoz[1] + "' WHERE gospodarieId = '" + vRezultat["gospodarieId"] + "' ");
                cont++;
            }
        }
        catch { }
        con.Close();

    }
    public void XMLSterge(XmlTextReader reader)
    {
        // .... nu mai folosim...
        // citim in continuare sa luam atributele gospodariei
        string vGospodarieId = reader.GetAttribute("gospodarieId");
        string vUnitateId = reader.GetAttribute("unitateId");
        string vAn = DateTime.Now.Year.ToString();
        if (vGospodarieId != "" && vGospodarieId != null)
        {
            string vInterogareCapitole = "DELETE FROM capitole WHERE gospodarieId='" + vGospodarieId + "' AND unitateId='" + vUnitateId + "' AND an='" + vAn + "'";
            string vInterogareParcele = "DELETE FROM parcele WHERE gospodarieId='" + vGospodarieId + "' AND unitateaId='" + vUnitateId + "' AND an='" + vAn + "'";
            string vInterogarePaduri = "DELETE FROM paduri WHERE gospodarieId='" + vGospodarieId + "' AND unitateId='" + vUnitateId + "' AND an='" + vAn + "'";
            string vInterogareMembri = "DELETE FROM membri WHERE gospodarieId='" + vGospodarieId + "' AND unitateId='" + vUnitateId + "' AND an='" + vAn + "'";

            ManipuleazaBD.fManipuleazaBD(vInterogareCapitole);
            ManipuleazaBD.fManipuleazaBD(vInterogareParcele);
            ManipuleazaBD.fManipuleazaBD(vInterogarePaduri);
            ManipuleazaBD.fManipuleazaBD(vInterogareMembri);

        }
        reader.MoveToElement();

    }
    public void XMLCitesteCopil(XmlTextReader reader, string pElement)
    {
        // ne intoarcem la capatul elementului gospodarii si sarim la descendentul pElement 

        reader.ReadToDescendant(pElement);
        vListaNumeAtribute.Clear();
        vListaValoriAtribute.Clear();
        int vContor = 0;
        while (!reader.EOF)
        {
            vContor = 0;
            while (vContor < reader.AttributeCount)
            {
                // citim atributele pentru membri
                reader.MoveToNextAttribute();
                vListaValoriAtribute.Add(reader.GetAttribute(reader.Name));
                vListaNumeAtribute.Add(reader.Name);
                vContor++;
                // si aici le introducem in baza de date .....
                // ...
                // ...
            }
            // mergem la urmatorul frate; daca nu e valid sarim din ciclu
            if (!reader.ReadToNextSibling(pElement))
            { reader.ReadEndElement(); break; }

        }
        //reader.Read();


    }

    List<string> vListaValoriAtribute = new List<string> { };
    List<string> vListaNumeAtribute = new List<string> { };
    public void XMLCitesteElement(XmlTextReader pReader, string pElement, string pCale, out string pUnitate, out string pMesaj)
    {
        pReader.ReadToFollowing(pElement);
        int vContor = 0;
        while (vContor < pReader.AttributeCount)
        {
            pReader.MoveToNextAttribute();
            vListaValoriAtribute.Add(pReader.GetAttribute(pReader.Name));
            vListaNumeAtribute.Add(pReader.Name);
            vContor++;
        }
        // verificam daca exista unitatea sau e XML-ul e valid

        if (ManipuleazaBD.fVerificaExistenta("unitati", "unitateCodFiscal", vListaValoriAtribute[vListaNumeAtribute.IndexOf("unitateCodFiscal")]) == 0)
        {
            pUnitate = "";
            pMesaj = "Unitatea pe care doriți să o actualizați nu există. Vă rugăm să introduceți această unitate în Administrare / Unități / Adaugă.";
            return;
        }
        else { pUnitate = ManipuleazaBD.fRezultaUnString("SELECT unitateId FROM unitati WHERE unitateCodFiscal ='" + vListaValoriAtribute[vListaNumeAtribute.IndexOf("unitateCodFiscal")] + "'", "unitateId"); pMesaj = ""; }
        // scriem datele in exporturi
        string vInterogare = "INSERT INTO exporturi (exportData, unitateId, exportUnitateCodFiscal, utilizatorCNP, exportTip, importData, importUnitateId,importUtilizatorId ) VALUES (CONVERT(DATETIME, '" + vListaValoriAtribute[vListaNumeAtribute.IndexOf("dataActualuluiExport")] + "', 104),'" + ManipuleazaBD.fRezultaUnString("SELECT unitateId FROM unitati WHERE unitateCodFiscal='" + vListaValoriAtribute[vListaNumeAtribute.IndexOf("unitateCodFiscal")] + "'", "unitateId") + "', '" + vListaValoriAtribute[vListaNumeAtribute.IndexOf("unitateCodFiscal")] + "','" + vListaValoriAtribute[vListaNumeAtribute.IndexOf("utilizatorCNP")] + "', '1',CONVERT(DATETIME, '" + DateTime.Now.Date.ToString() + "', 104), '" + Session["SESunitateId"] + "','" + Session["SESutilizatorId"] + "' )"; // 0 export, 1-import
        //   string vInterogare = "INSERT INTO exporturi (exportData, unitateId, exportUnitateCodFiscal, utilizatorCNP, exportTip, importData, importUnitateId,importUtilizatorId, exportCale ) VALUES ('" + vListaValoriAtribute[vListaNumeAtribute.IndexOf("dataActualuluiExport")] + "','" + ManipuleazaBD.fRezultaUnString("SELECT unitateId FROM unitati WHERE unitateCodFiscal='" + vListaValoriAtribute[vListaNumeAtribute.IndexOf("unitateCodFiscal")] + "'", "unitateId") + "', '" + vListaValoriAtribute[vListaNumeAtribute.IndexOf("unitateCodFiscal")] + "','" + vListaValoriAtribute[vListaNumeAtribute.IndexOf("utilizatorCNP")] + "', '1',CONVERT(DATETIME,'" + DateTime.Now.Date.ToString() + "',104), '" + Session["SESunitateId"] + "','" + Session["SESutilizatorId"] + "','" + vFisier + "' )"; // 0 export, 1-import
        ManipuleazaBD.fManipuleazaBD(vInterogare);

    }
    protected void gvExporturi_OnDataBound(object sender, EventArgs e)
    {
        for (int i = 0; i < gvExporturi.Rows.Count; i++)
        {
            if (gvExporturi.Rows[i].RowType == DataControlRowType.DataRow)
            {
                try
                {
                    gvExporturi.Rows[i].Cells[4].Text = "<a href=\"/Exporturi/" + gvExporturi.Rows[i].Cells[4].Text + ".zip\" target=\"_blank\">descarcă fisierul XML</a>";
                }
                catch { }
            }
        }
    }
    protected void gvImporturi_OnDataBound(object sender, EventArgs e)
    {
        for (int i = 0; i < gvImporturi.Rows.Count; i++)
        {
            if (gvImporturi.Rows[i].RowType == DataControlRowType.DataRow)
            {
                try
                {
                    gvImporturi.Rows[i].Cells[3].Text = "<a href=\"/Exporturi/Importuri/" + gvImporturi.Rows[i].Cells[3].Text + "\" target=\"_blank\">descarcă fisierul XML</a>";
                }
                catch { }
            }
        }
    }
    protected void fuImporta_Load(object sender, EventArgs e)
    {
        //  XMLAlegeFisier(sender, e);
    }
    protected void gvStare_DataBound(object sender, EventArgs e)
    {

    }
    protected string ScoateNumere(string pScoateNumar)
    {
        switch (pScoateNumar)
        {
            default:
                break;
            case "I":
                pScoateNumar = "1";
                break;
            case "II":
                pScoateNumar = "2";
                break;
            case "III":
                pScoateNumar = "3";
                break;
            case "IV":
                pScoateNumar = "4";
                break;
            case "V":
                pScoateNumar = "5";
                break;
            case "VI":
                pScoateNumar = "6";
                break;
            case "VII":
                pScoateNumar = "7";
                break;
            case "VIII":
                pScoateNumar = "8";
                break;
            case "IX":
                pScoateNumar = "9";
                break;
            case "X":
                pScoateNumar = "10";
                break;
            case "XI":
                pScoateNumar = "11";
                break;
            case "XII":
                pScoateNumar = "12";
                break;
            case "XIII":
                pScoateNumar = "13";
                break;
            case "XIV":
                pScoateNumar = "14";
                break;
            case "XV":
                pScoateNumar = "15";
                break;
            case "XVI":
                pScoateNumar = "16";
                break;
            case "XVII":
                pScoateNumar = "17";
                break;
            case "XVIII":
                pScoateNumar = "18";
                break;
            case "XIX":
                pScoateNumar = "19";
                break;
            case "XX":
                pScoateNumar = "20";
                break;
            case "XXI":
                pScoateNumar = "21";
                break;
            case "XXII":
                pScoateNumar = "22";
                break;
            case "XXIII":
                pScoateNumar = "23";
                break;
            case "XXIV":
                pScoateNumar = "24";
                break;
            case "XXV":
                pScoateNumar = "25";
                break;
            case "XXVI":
                pScoateNumar = "26";
                break;
            case "XXVII":
                pScoateNumar = "27";
                break;
            case "XXVIII":
                pScoateNumar = "28";
                break;
            case "XXIX":
                pScoateNumar = "29";
                break;
            case "XXX":
                pScoateNumar = "30";
                break;
            case "XXXI":
                pScoateNumar = "31";
                break;
            case "XXXII":
                pScoateNumar = "32";
                break;
            case "XXXIII":
                pScoateNumar = "33";
                break;
            case "XXXIV":
                pScoateNumar = "34";
                break;
            case "XXXV":
                pScoateNumar = "35";
                break;
            case "XXXVI":
                pScoateNumar = "36";
                break;
            case "XXXVII":
                pScoateNumar = "37";
                break;
            case "XXXVIII":
                pScoateNumar = "38";
                break;
            case "XXXIX":
                pScoateNumar = "39";
                break;
            case "XL":
                pScoateNumar = "40";
                break;
            case "XLI":
                pScoateNumar = "41";
                break;
            case "XLII":
                pScoateNumar = "42";
                break;
            case "XLIII":
                pScoateNumar = "43";
                break;
            case "XLIV":
                pScoateNumar = "44";
                break;
            case "XLV":
                pScoateNumar = "45";
                break;
            case "XLVI":
                pScoateNumar = "46";
                break;
            case "XLVII":
                pScoateNumar = "47";
                break;
            case "XLVIII":
                pScoateNumar = "48";
                break;
            case "XLIX":
                pScoateNumar = "49";
                break;
            case "L":
                pScoateNumar = "50";
                break;
            case "LI":
                pScoateNumar = "51";
                break;
            case "LII":
                pScoateNumar = "52";
                break;
            case "LIII":
                pScoateNumar = "53";
                break;
            case "LIV":
                pScoateNumar = "54";
                break;
            case "LV":
                pScoateNumar = "55";
                break;
            case "LVI":
                pScoateNumar = "56";
                break;
            case "LVII":
                pScoateNumar = "57";
                break;
            case "LVIII":
                pScoateNumar = "58";
                break;
            case "LIX":
                pScoateNumar = "59";
                break;
            case "LX":
                pScoateNumar = "60";
                break;
            case "LXI":
                pScoateNumar = "61";
                break;
            case "LXII":
                pScoateNumar = "62";
                break;
            case "LXIII":
                pScoateNumar = "63";
                break;
            case "LXIV":
                pScoateNumar = "64";
                break;
            case "LXV":
                pScoateNumar = "65";
                break;
            case "LXVI":
                pScoateNumar = "66";
                break;
            case "LXVII":
                pScoateNumar = "67";
                break;
            case "LXVIII":
                pScoateNumar = "68";
                break;
            case "LXIX":
                pScoateNumar = "69";
                break;
            case "LXX":
                pScoateNumar = "70";
                break;
            case "LXXI":
                pScoateNumar = "71";
                break;
            case "LXXII":
                pScoateNumar = "72";
                break;
            case "LXXIII":
                pScoateNumar = "73";
                break;
            case "LXXIV":
                pScoateNumar = "74";
                break;
            case "LXXV":
                pScoateNumar = "75";
                break;
            case "LXXVI":
                pScoateNumar = "76";
                break;
            case "LXXVII":
                pScoateNumar = "77";
                break;
            case "LXXVIII":
                pScoateNumar = "78";
                break;
            case "LXXIX":
                pScoateNumar = "79";
                break;
            case "LXXX":
                pScoateNumar = "80";
                break;
            case "LXXXI":
                pScoateNumar = "81";
                break;
            case "LXXXII":
                pScoateNumar = "82";
                break;
            case "LXXXIII":
                pScoateNumar = "83";
                break;
            case "LXXXIV":
                pScoateNumar = "84";
                break;
            case "LXXXV":
                pScoateNumar = "85";
                break;
            case "LXXXVI":
                pScoateNumar = "86";
                break;
            case "LXXXVII":
                pScoateNumar = "87";
                break;
            case "LXXXVIII":
                pScoateNumar = "88";
                break;
            case "LXXXIX":
                pScoateNumar = "89";
                break;
            case "XC":
                pScoateNumar = "90";
                break;
            case "XCI":
                pScoateNumar = "91";
                break;
            case "XCII":
                pScoateNumar = "92";
                break;
            case "XCIII":
                pScoateNumar = "93";
                break;
            case "XCIV":
                pScoateNumar = "94";
                break;
            case "XCV":
                pScoateNumar = "95";
                break;
            case "XCVI":
                pScoateNumar = "96";
                break;
            case "XCVII":
                pScoateNumar = "97";
                break;
            case "XCVIII":
                pScoateNumar = "98";
                break;
            case "XCIX":
                pScoateNumar = "99";
                break;
            case "C":
                pScoateNumar = "100";
                break;
        }
        int vContor = 0;
        int vLungime = pScoateNumar.Length;
        string vNumar = "";
        while (vContor < vLungime)
        {
            string vCaracter = pScoateNumar[vContor].ToString();
            try
            {
                vNumar += Convert.ToInt16(vCaracter).ToString();
            }
            catch { }
            vContor++;
        }
        try
        {
            vNumar = Convert.ToInt16(vNumar).ToString();
        }
        catch { vNumar = "0"; }
        return vNumar; ;
    }
}

