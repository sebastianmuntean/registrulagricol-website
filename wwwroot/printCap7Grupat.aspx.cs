﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data.SqlClient;

public partial class printCap7Grupat : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataTable reportDataTable = GetDataTAbleForReport();
            DataTable unitatiDataTable = GetUnitateDenumire();
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource ds = new ReportDataSource("dsRapoarte_Cap7GrupatDataTable", reportDataTable);
            ReportDataSource ds2 = new ReportDataSource("dsRapoarte_dtUnitati", unitatiDataTable);
            ReportViewer1.LocalReport.DataSources.Add(ds);
            ReportViewer1.LocalReport.DataSources.Add(ds2);
            ReportViewer1.LocalReport.Refresh();
            ReportViewer1.Visible = true;
        }
    }
    protected void button_Click(object sender, EventArgs e)
    {
        
    }

    private DataTable GetDataTAbleForReport()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        string interogare = @"select denumire1,sabloanecapitole.codrand,gospodarii.localitate,CASE gospodarii.persJuridica WHEN 1 THEN 'Persoana juridica' else 'Persoana fizica' end as 'Tip persoana', CASE gospodarii.strainas WHEN 1 THEN 'Strainas' else 'Localnic' end as 'Tip gospodarie',
                                    CAST(sum(capitole.col2) as int) as 'Sem1',CAST(sum(capitole.col1) as int) as 'Sem2',CAST(sum(capitole.col1 + capitole.col2) as int) as 'Total'
                             FROM capitole
                             join sabloaneCapitole on sabloaneCapitole.capitol = capitole.codCapitol and sabloaneCapitole.codRand = capitole.codRand and sabloaneCapitole.an = " +Session["SESan"].ToString()+@"
                             join gospodarii on capitole.gospodarieId = gospodarii.gospodarieId
                             join unitati on unitati.unitateId = gospodarii.unitateId
                             join localitati
                             on localitati.localitateId = unitati.localitateId
                             WHERE (codCapitol = '7' and capitol = '7' and capitole.an = " + Session["SESan"].ToString() + @" and capitole.unitateId = " + Session["SESunitateId"].ToString() + @" )
                             group by gospodarii.persJuridica,gospodarii.strainas,gospodarii.localitate,denumire1,capitole.codRand,sabloanecapitole.codrand
                             order by gospodarii.persJuridica,gospodarii.strainas,gospodarii.localitate,capitole.codRand";
         SqlDataAdapter adapter= new SqlDataAdapter(interogare, connection);
         DataTable table = new DataTable();
         adapter.Fill(table);
         ManipuleazaBD.InchideConexiune(connection);
         return table;
    }

    private DataTable GetUnitateDenumire()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        string interogare = @"SELECT        unitati.unitateId, unitati.unitateDenumire, unitati.unitateCodSiruta, unitati.judetId, unitati.localitateId, unitati.unitateStrada, unitati.unitateNr, unitati.unitateAp, 
                         unitati.unitateCodPostal, unitati.localitateComponentaId, unitati.unitateCodFiscal, unitati.unitateActiva, unitati.unitatePrincipala, judete.judetDenumire, 
                         localitati.localitateDenumire
FROM            unitati INNER JOIN
                         judete ON unitati.judetId = judete.judetId INNER JOIN
                         localitati ON unitati.localitateId = localitati.localitateId
WHERE        (unitati.unitateId = " + Session["SESunitateId"].ToString() + ")";
        SqlDataAdapter adapter = new SqlDataAdapter(interogare, connection);
        DataTable table = new DataTable();
        adapter.Fill(table);
        ManipuleazaBD.InchideConexiune(connection);
        return table;
    }
}