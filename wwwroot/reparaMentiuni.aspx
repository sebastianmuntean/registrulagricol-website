﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="reparaMentiuni.aspx.cs" Inherits="reparaMentiuni" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="pnAfiseazaMesaje" runat="server">
        <asp:ValidationSummary ID="valSumUtilizatori" runat="server" DisplayMode="SingleParagraph"
            Visible="true" ValidationGroup="GrupValidareExport" CssClass="validator" ForeColor="" />
        <asp:CustomValidator ID="valCustom" runat="server" ErrorMessage="" ValidationGroup="GrupValidareExport"></asp:CustomValidator>
    </asp:Panel>
    <asp:Panel ID="pnListaLocalitati" runat="server" CssClass="panel_general" Visible="true">
        <asp:Panel ID="pnGrid" runat="server" Visible="true">
            <h1>
                Repara localitati
            </h1>
            <p>
                <asp:FileUpload ID="fuFisier" runat="server" />
            </p>
            <asp:Panel ID="pnButoane" CssClass="butoane" runat="server">
                <asp:Button CssClass="buton" ID="btAdauga" runat="server" Text="salveaza menţiuni"
                    OnClick="btAdauga_Click" />
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
</asp:Content>
