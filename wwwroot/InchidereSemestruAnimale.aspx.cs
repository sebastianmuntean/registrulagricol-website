﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class InchidereSemestruAnimale : System.Web.UI.Page
{
    short an;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["SESutilizatorId"].ToString() == "52") pnInchideMaiMulteDeLaPanaLa.Visible = true;
        else pnInchideMaiMulteDeLaPanaLa.Visible = false;
        if (!IsPostBack)
        {
            if (Session["SESgospodarieId"] == null)
            {
                VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
                vVerificaSesiuni.VerificaSesiuniCookie();
            }
            corelatiiGridView.DataSource = GetCorelatiiInvalide();
            corelatiiGridView.DataBind();

            if (corelatiiGridView.Rows.Count > 0)
            {
                corelatiiLabel.Text = "Exista corelatii invalide la capitolul 7 pe semestrul 2 la urmatoarele gospodarii";
            }
            else
            {
                exportToExcelButton.Visible = false;
                exportToWordButton.Visible = false;
            }
        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        an = Convert.ToInt16(HttpContext.Current.Session["SESan"]);
    }
    protected void inchidereSemestruAnimaleButton_Click(object sender, EventArgs e)
    {
        StergeInvalidariAnimale();
        InchidereSemestru(Convert.ToInt16(Session["SESunitateId"]));
    }
    protected void btInchideMaiMulteDeLaPanaLa_Click(object sender, EventArgs e)
    {
        short deLa = 0;
        short panaLa = 0;
        try
        {
            deLa = Convert.ToInt16(tbDeLa.Text);
        }
        catch { }
        try
        {
            panaLa = Convert.ToInt16(tbPanaLa.Text);
        }
        catch { }

        for (short unitate = deLa; unitate <= panaLa; unitate++)
        {
            if (VerificaAnDeschis(unitate, Convert.ToInt16(an))) InchidereSemestru(unitate);
        }
    }
    private bool VerificaAnDeschis(short unitate, short an)
    {
        if (ManipuleazaBD.fRezultaUnString("SELECT [unitateAnDeschis] FROM [tntcomputers].[unitati] where unitateId = " + unitate.ToString(), "unitateAnDeschis", 2016) == an.ToString()) return true;
        else   
        return false;
    }
    /// <summary>
    /// Verifica si Completeza randurile dintr-un capitol. Returneaza o lista de INSERT-uri ce urmeaza a fi folosite ulterior.
    /// </summary>
    public static List<string> CompleteazaRanduriInCapitolePentruOGospodarie(string unitate, string gospodarie, string an, List<AnimaleServices.RandSablon> sablonCapitol, DataTable dataTableRanduriExistente, string mod)
    {
        List<string> listaInserturi = new List<string>();
        //List<string> listaRanduriExistente = dataTableRanduriExistente.Rows.T
        // parcurgem sablonul si verificam 
        List<string> listaCoduriDeLucru = new List<string>();
        bool amGasitCodRand = false;
        foreach (AnimaleServices.RandSablon randSablon in sablonCapitol)
        {
            foreach (DataRow randExistent in dataTableRanduriExistente.Rows)
            {
                if (randExistent["codRand"].ToString() == randSablon.CodRand)
                {
                    listaCoduriDeLucru.Add(randExistent["codRand"].ToString());
                    amGasitCodRand = true;
                    break;
                }
            }
            if (!amGasitCodRand)
            {
                // nu am gasit codul asa ca il inseram cu zero
                listaCoduriDeLucru.Add(randSablon.CodRand);
                if (mod == "tot") listaInserturi.Add(@"INSERT INTO [capitole] ([unitateId], [gospodarieId], [an], [codCapitol], [codRand], [col1], [col2], [col3], [col4], [col5], [col6], [col7], [col8]) VALUES ('" + unitate + "','" + gospodarie + "','" + an + "','" + randSablon.Capitol + "', '" + randSablon.CodRand + @"', '0', '0', '0', '0', '0', '0', '0', '0'); 
                    ");
                // completam si datatable primita
                DataRow randNou = dataTableRanduriExistente.NewRow();
                if (dataTableRanduriExistente.Rows.Count == 0)
                {
                    DataColumn coloana;
                    coloana = new DataColumn();

                    coloana.DataType = System.Type.GetType("System.Int32");
                    coloana.ColumnName = "col1";
                    dataTableRanduriExistente.Columns.Add(coloana);
                    coloana = new DataColumn();
                    coloana.DataType = System.Type.GetType("System.Int32");
                    coloana.ColumnName = "col2";
                    dataTableRanduriExistente.Columns.Add(coloana);
                    coloana = new DataColumn();
                    coloana.DataType = System.Type.GetType("System.String");
                    coloana.ColumnName = "codRand";
                    dataTableRanduriExistente.Columns.Add(coloana);
                    coloana = new DataColumn();
                    coloana.DataType = System.Type.GetType("System.String");
                    coloana.ColumnName = "gospodarieId";
                    dataTableRanduriExistente.Columns.Add(coloana);
                }
                randNou["col1"] = "0";
                randNou["col2"] = "0";
                randNou["codRand"] = randSablon.CodRand;
                randNou["gospodarieId"] = gospodarie;
                dataTableRanduriExistente.Rows.Add(randNou);
            }
            amGasitCodRand = false;
        }
        return listaInserturi;
    }
    private void InchidereSemestru(Int16 unitate)
    {
        if (ManipuleazaBD.fVerificaExistenta("unitati", "unitateid", unitate.ToString(), 2015) == 0) return;
        StergeInvalidariAnimale(unitate.ToString());
        // luam sabloanele pentru a fi siguri ulterior ca toate randurile sunt complete
        List<AnimaleServices.RandSablon> sablonCapitol7 = AnimaleServices.CitesteRanduriSablon(an, "7");
        List<AnimaleServices.RandSablon> sablonCapitol8 = AnimaleServices.CitesteRanduriSablon(an, "8");
        List<AnimaleServices.RandSablon> sablonCapitol8_2 = AnimaleServices.CitesteRanduriSablon(an, "8", " AND [codRand] in (9, 17, 26, 35, 44, 53, 62, 71, 79, 85)  ");

        DataTable raportTable = CreateRaport();

        DataTable gospodariiTable;
        SqlConnection connection;
        ClassLog.fLog(1, Convert.ToInt32(HttpContext.Current.Session["SESutilizatorId"]), DateTime.Now, "transfer animale " + an, "unitatea:" + unitate, "inceput inchiderea animale", 0, 0);

        // citim doar gospodariile care au animale, in 7 sau 8
        FillGospodariiTable(out gospodariiTable, out connection, unitate.ToString());
        ClassLog.fLog(1, Convert.ToInt32(HttpContext.Current.Session["SESutilizatorId"]), DateTime.Now, "transfer animale " + an, "unitatea:" + unitate, "numar gospodarii cu animale:" + gospodariiTable.Rows.Count, 0, 0);


        //StringBuilder animal = new StringBuilder(); // pentru update la toate gospodariile
        List<StringBuilder> listaAnimal = new List<StringBuilder>(); // pentru update la toate gospodariile deodata - in pachete de x updateturi

        // lista stringuri - fiecare string pt un update/insert

        List<string> listaStringTransferuri = new List<string>();


        int i = 0;
        int nrGospodariiParcurse = 0;

        // pe unitate
        // luam datele din cap 7 si 8 si le sortam dupa gospodarie si codrand
        DataTable UevolutiaAnimalelorPeSem1 = new DataTable();
        UevolutiaAnimalelorPeSem1 = AnimaleServices.GetEvolutiaAnimalelorInCursulAnuluiByUnitateAndByAn(Convert.ToInt16(an), unitate);
        List<DataTable> listaEvolutiaAnimalelorPeSem1 = UevolutiaAnimalelorPeSem1.AsEnumerable()
            .GroupBy(row => row.Field<int>("gospodarieId"))
            .Select(g => g.CopyToDataTable())
            .ToList();

        DataTable UevolutiaAnimalelorPeSem2 = new DataTable();
        UevolutiaAnimalelorPeSem2 = AnimaleServices.GetEvolutiaAnimalelorInCursulAnuluiByUnitateAndByAndPeSem2(Convert.ToInt16(an), unitate);
        List<DataTable> listaEvolutiaAnimalelorPeSem2 = UevolutiaAnimalelorPeSem2.AsEnumerable()
            .GroupBy(row => row.Field<int>("gospodarieId"))
            .Select(g => g.CopyToDataTable())
            .ToList();

        DataTable UsituatiaAnimalelorLaInceputulSemestruluiDataTable = new DataTable();
        UsituatiaAnimalelorLaInceputulSemestruluiDataTable = AnimaleServices.GetSituatiaAnimalelorLaInceputulSemestruluiByUnitateAndByAn(Convert.ToInt16(an), unitate);
        List<DataTable> listaSituatiaAnimalelorLaInceputulSemestruluiDataTable = UsituatiaAnimalelorLaInceputulSemestruluiDataTable.AsEnumerable()
            .GroupBy(row => row.Field<int>("gospodarieId"))
            .Select(g => g.CopyToDataTable())
            .ToList();

        foreach (DataRow gospodariiRow in gospodariiTable.Rows)
        {
            i++;


            //  StringBuilder animal = new StringBuilder(); // pentru update la o singura gospodarie

            SqlConnection.ClearPool(connection);

            int sumaBovine = 0;
            int sumaVaci = 0;
            int sumaOvine = 0;
            int sumaCaprine = 0;
            int sumaPorcine = 0;
            int sumaCabaline = 0;
            int sumaPasari = 0;
            int sumaAlteAnimale = 0;
            int sumaAlbine = 0;
            int sumaPesti = 0;
            int sumaBovine7 = 0;
            int sumaVaci7 = 0;
            int sumaOvine7 = 0;
            int sumaCaprine7 = 0;
            int sumaPorcine7 = 0;
            int sumaCabaline7 = 0;
            int sumaPasari7 = 0;
            int sumaAlteAnimale7 = 0;
            int sumaAlbine7 = 0;
            int sumaPesti7 = 0;

            int idGospodarie = Convert.ToInt32(gospodariiRow["gospodarieId"]);

            DataTable evolutiaAnimalelorPeSem1 = new DataTable();
            DataTable evolutiaAnimalelorPeSem2 = new DataTable();
            DataTable situatiaAnimalelorLaInceputulSemestruluiDataTable = new DataTable();
            // pentru comparare finala:
            DataTable evolutiaAnimalelorPeSem1Initial = new DataTable();
            DataTable evolutiaAnimalelorPeSem2Initial = new DataTable();
            DataTable situatiaAnimalelorLaInceputulSemestruluiDataTableInitial = new DataTable();

            AnimaleServices.ConnectionYear = Convert.ToInt16(an);
            // pe gospodarie


            //evolutiaAnimalelorPeSem1 = AnimaleServices.GetEvolutiaAnimalelorInCursulAnuluiByGospodarieAndByAnAndByUnitate(idGospodarie, Convert.ToInt16(an), unitate);
            //evolutiaAnimalelorPeSem2 = AnimaleServices.GetEvolutiaAnimalelorInCursulAnuluiByGospodarieAndByAnAndByUnitatePeSem2(idGospodarie, Convert.ToInt16(an), unitate);
            try
            {
                evolutiaAnimalelorPeSem1 = evolutiaAnimalelorPeSem1Initial = UevolutiaAnimalelorPeSem1.AsEnumerable().Where(row => row.Field<int>("gospodarieId") == Convert.ToInt32(gospodariiRow["gospodarieId"])).CopyToDataTable();
            }
            catch { }
            try
            {
                evolutiaAnimalelorPeSem2 = evolutiaAnimalelorPeSem2Initial = UevolutiaAnimalelorPeSem2.AsEnumerable().Where(row => row.Field<int>("gospodarieId") == Convert.ToInt32(gospodariiRow["gospodarieId"])).CopyToDataTable();
            }
            catch { }
            //situatiaAnimalelorLaInceputulSemestruluiDataTable = AnimaleServices.GetSituatiaAnimalelorLaInceputulSemestruluiByGospodarieAndByAnAndByUnitate(idGospodarie, Convert.ToInt16(an), unitate);
            try
            {
                situatiaAnimalelorLaInceputulSemestruluiDataTable = situatiaAnimalelorLaInceputulSemestruluiDataTableInitial = UsituatiaAnimalelorLaInceputulSemestruluiDataTable.AsEnumerable().Where(row => row.Field<int>("gospodarieId") == Convert.ToInt32(gospodariiRow["gospodarieId"])).CopyToDataTable();
            }
            catch { }

            // verificam daca are randuri complete pe cap 7 si 8
            listaStringTransferuri.AddRange(CompleteazaRanduriInCapitolePentruOGospodarie(unitate.ToString(), idGospodarie.ToString(), an.ToString(), sablonCapitol7, situatiaAnimalelorLaInceputulSemestruluiDataTable, "tot"));
            listaStringTransferuri.AddRange(CompleteazaRanduriInCapitolePentruOGospodarie(unitate.ToString(), idGospodarie.ToString(), an.ToString(), sablonCapitol8, evolutiaAnimalelorPeSem1, "tot"));
            listaStringTransferuri.AddRange(CompleteazaRanduriInCapitolePentruOGospodarie(unitate.ToString(), idGospodarie.ToString(), an.ToString(), sablonCapitol8_2, evolutiaAnimalelorPeSem2, "partial"));

            // daca nu are date in cap8, dar are in 7
            //if (evolutiaAnimalelorPeSem1.Rows.Count == 0 && situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows.Count > 0)
            //{
            //    CalculCapitole.PopuleazaTabelaCapitole(Convert.ToInt32(an), idGospodarie, unitate, "8");
            //    evolutiaAnimalelorPeSem1 = AnimaleServices.GetEvolutiaAnimalelorInCursulAnuluiByGospodarieAndByAnAndByUnitate(idGospodarie, Convert.ToInt16(an), unitate);
            //    evolutiaAnimalelorPeSem2 = AnimaleServices.GetEvolutiaAnimalelorInCursulAnuluiByGospodarieAndByAnAndByUnitatePeSem2(idGospodarie, Convert.ToInt16(an), unitate);
            //}
            //else
            //{ }
            // daca are date in 7 sem1 
            #region FAZA 1  - completam 8col2 inceputul si finalul de semestru cu datele luate din 7col1
            DataTable randuriCompletatePe8Col1;
            int areRanduriCompletatePe8Col1 = 0;
            try
            {
                randuriCompletatePe8Col1 = evolutiaAnimalelorPeSem1.AsEnumerable().Where(row => row.Field<decimal>("col1") != 0).CopyToDataTable();
                areRanduriCompletatePe8Col1 = randuriCompletatePe8Col1.Rows.Count;
            }
            catch { areRanduriCompletatePe8Col1 = 0; }

            // completam daca are doar valori zero peste tot
            // daca totalul in 7col1 != 0 si nu este egal cu totalul din 8 sem1
            if (situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows.Count > 0 && areRanduriCompletatePe8Col1 == 0)
            {
                // 7: Bovine - total cod (02+08+13+26)
                // 8: Bovine total existente la sfârşitul semestrului cod (01+02+03+04-05-06-07-08)
                if (Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[0]["col1"]) != 0 && Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[0]["col1"]) == 0)
                {
                    evolutiaAnimalelorPeSem1.Rows[0]["col1"] = situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[0]["col1"];
                    evolutiaAnimalelorPeSem1.Rows[8]["col1"] = Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[0]["col1"]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[1]["col1"]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[2]["col1"]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[3]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[4]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[5]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[6]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[7]["col1"]);

                    listaStringTransferuri.Add(@"UPDATE capitole 
                                            SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[0]["col1"] +
                                               @"' WHERE codCapitol = '8' 
                                                    AND codRand = 1 
                                                    AND gospodarieId = " + idGospodarie + @" 
                                                    AND an = " + an + "; ");
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[8]["col1"] +
                                  "' WHERE codCapitol = '8' AND codRand = 9 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                }

                if (Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[22]["col1"]) != 0 && Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[9]["col1"]) == 0)
                {
                    evolutiaAnimalelorPeSem1.Rows[9]["col1"] = situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[22]["col1"];
                    evolutiaAnimalelorPeSem1.Rows[16]["col1"] = Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[9]["col1"]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[10]["col1"]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[11]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[12]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[13]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[14]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[15]["col1"]);

                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[9]["col1"] +
                                  "' WHERE codCapitol = '8' AND codRand = 10 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[16]["col1"] +
                                  "' WHERE codCapitol = '8' AND codRand = 17 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                }

                if (Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[29]["col1"]) != 0 && Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[17]["col1"]) == 0)
                {
                    evolutiaAnimalelorPeSem1.Rows[17]["col1"] = situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[29]["col1"];
                    evolutiaAnimalelorPeSem1.Rows[25]["col1"] = Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[17]["col1"]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[18]["col1"]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[19]["col1"]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[20]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[21]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[22]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[23]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[24]["col1"]);
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[17]["col1"] +
                                  "' WHERE codCapitol = '8' AND codRand = 18 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[25]["col1"] +
                                  "' WHERE codCapitol = '8' AND codRand = 26 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                }

                if (Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[35]["col1"]) != 0 && Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[26]["col1"]) == 0)
                {
                    evolutiaAnimalelorPeSem1.Rows[26]["col1"] = situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[35]["col1"];
                    evolutiaAnimalelorPeSem1.Rows[34]["col1"] = Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[26]["col1"]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[27]["col1"]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[28]["col1"]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[29]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[30]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[31]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[32]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[33]["col1"]);
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[26]["col1"] +
                                  "' WHERE codCapitol = '8' AND codRand = 27 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[34]["col1"] +
                                  "' WHERE codCapitol = '8' AND codRand = 35 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                }

                if (Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[40]["col1"]) != 0 && Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[35]["col1"]) == 0)
                {
                    evolutiaAnimalelorPeSem1.Rows[35]["col1"] = situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[40]["col1"];
                    evolutiaAnimalelorPeSem1.Rows[43]["col1"] = Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[35]["col1"]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[36]["col1"]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[37]["col1"]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[38]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[39]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[40]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[41]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[42]["col1"]);
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[35]["col1"] +
                                  "' WHERE codCapitol = '8' AND codRand = 36 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[43]["col1"] +
                                  "' WHERE codCapitol = '8' AND codRand = 44 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                }
                // cabaline
                if (Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[55]["col1"]) != 0 && Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[44]["col1"]) == 0)
                {
                    evolutiaAnimalelorPeSem1.Rows[44]["col1"] = situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[55]["col1"];
                    evolutiaAnimalelorPeSem1.Rows[52]["col1"] = Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[44]["col1"]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[45]["col1"]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[46]["col1"]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[47]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[48]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[49]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[50]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[51]["col1"]);
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[44]["col1"] +
                                  "' WHERE codCapitol = '8' AND codRand = 45 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[52]["col1"] +
                                  "' WHERE codCapitol = '8' AND codRand = 53 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");

                }
                // alte animale, in cap 7 iepuri cod rand 61 +  animale de blana 63 + alte animale 77
                // in cap 8 - cod rand 63 inceput, 71 final (63+64+65+66-67-68-69-70)
                if (
                    (Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[60]["col1"]) + Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[62]["col1"])) != 0 && Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[62]["col1"]) == 0)
                {
                    evolutiaAnimalelorPeSem1.Rows[62]["col1"] = Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[60]["col1"]) + Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[62]["col1"]);
                    evolutiaAnimalelorPeSem1.Rows[70]["col1"] = Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[62]["col1"]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[63]["col1"]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[64]["col1"]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[65]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[66]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[67]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[68]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[69]["col1"]);
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[62]["col1"] +
                                  "' WHERE codCapitol = '8' AND codRand = 63 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[70]["col1"] +
                                  "' WHERE codCapitol = '8' AND codRand = 71 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                }
                // pasari
                if (Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[67]["col1"]) != 0 && Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[53]["col1"]) == 0)
                {
                    evolutiaAnimalelorPeSem1.Rows[53]["col1"] = situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[67]["col1"];
                    evolutiaAnimalelorPeSem1.Rows[61]["col1"] = Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[53]["col1"]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[54]["col1"]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[55]["col1"]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[56]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[57]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[58]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[59]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[60]["col1"]);
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[53]["col1"] +
                                  "' WHERE codCapitol = '8' AND codRand = 54 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[61]["col1"] +
                                  "' WHERE codCapitol = '8' AND codRand = 62 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                }

                if (Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[76]["col1"]) != 0 && Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[62]["col1"]) == 0)
                {
                    evolutiaAnimalelorPeSem1.Rows[62]["col1"] = situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[76]["col1"];
                    evolutiaAnimalelorPeSem1.Rows[70]["col1"] = Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[62]["col1"]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[63]["col1"]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[64]["col1"]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[65]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[66]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[67]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[68]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[69]["col1"]);
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[62]["col1"] +
                                  "' WHERE codCapitol = '8' AND codRand = 63 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[70]["col1"] +
                                  "' WHERE codCapitol = '8' AND codRand = 71 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                }
                // albine
                if (Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[77]["col1"]) != 0 && Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[71]["col1"]) == 0)
                {
                    evolutiaAnimalelorPeSem1.Rows[71]["col1"] = situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[77]["col1"];
                    evolutiaAnimalelorPeSem1.Rows[78]["col1"] = Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[71]["col1"]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[72]["col1"]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[73]["col1"]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[74]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[75]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[76]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[77]["col1"]);
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[71]["col1"] +
                                  "' WHERE codCapitol = '8' AND codRand = 72 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[78]["col1"] +
                                  "' WHERE codCapitol = '8' AND codRand = 79 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                }
                // pesti
                if (evolutiaAnimalelorPeSem1.Rows.Count > 79 && situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows.Count > 78)
                {
                    if (Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[78]["col1"]) != 0 && Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[79]["col1"]) == 0)
                    {
                        evolutiaAnimalelorPeSem1.Rows[79]["col1"] = situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[78]["col1"];
                        evolutiaAnimalelorPeSem1.Rows[84]["col1"] = Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[79]["col1"]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[80]["col1"]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[81]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[82]["col1"]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[83]["col1"]);
                        listaStringTransferuri.Add(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[79]["col1"] +
                                      "' WHERE codCapitol = '8' AND codRand = 80 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                        listaStringTransferuri.Add(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[84]["col1"] +
                                      "' WHERE codCapitol = '8' AND codRand = 85 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    }
                }
            }
            #endregion
            #region FAZA 2
            // capitolul 8, sem 1
            DataTable randuriCompletatePe8Col2;
            int areRanduriCompletatePe8Col2 = 0;
            try
            {
                randuriCompletatePe8Col2 = evolutiaAnimalelorPeSem2.AsEnumerable().Where(row => row.Field<decimal>("col2") != 0).CopyToDataTable();
                areRanduriCompletatePe8Col2 = randuriCompletatePe8Col2.Rows.Count;
            }
            catch { areRanduriCompletatePe8Col2 = 0; }
            // completam daca are doar valori zero peste tot
            if (areRanduriCompletatePe8Col2 == 0)
            {
                foreach (DataRow rowAnimal in evolutiaAnimalelorPeSem1.Rows)
                {
                    if (Convert.ToInt32(rowAnimal["col2"]) != 0)
                        continue;
                    Tuple<short, short> bovine = new Tuple<short, short>(9, 1);
                    Tuple<short, short> vaci = new Tuple<short, short>(17, 10);
                    Tuple<short, short> ovine = new Tuple<short, short>(26, 18);
                    Tuple<short, short> caprine = new Tuple<short, short>(35, 27);
                    Tuple<short, short> porcine = new Tuple<short, short>(44, 36);
                    Tuple<short, short> cabaline = new Tuple<short, short>(53, 45);
                    Tuple<short, short> pasari = new Tuple<short, short>(62, 54);
                    Tuple<short, short> alteAnimale = new Tuple<short, short>(71, 63);
                    Tuple<short, short> albine = new Tuple<short, short>(79, 72);
                    Tuple<short, short> pesti = new Tuple<short, short>(85, 80);

                    if (Convert.ToInt32(evolutiaAnimalelorPeSem2.Rows[0]["col2"]) == 0)
                    {
                        //    UpdateRandAnimal(rowAnimal, idGospodarie, animal, bovine);
                        UpdateRandAnimal(rowAnimal, idGospodarie, listaStringTransferuri, bovine);

                        sumaBovine += GetSuma(rowAnimal, bovine);
                    }
                    if (Convert.ToInt32(evolutiaAnimalelorPeSem2.Rows[1]["col2"]) == 0)
                    {
                        //  UpdateRandAnimal(rowAnimal, idGospodarie, animal, vaci);
                        UpdateRandAnimal(rowAnimal, idGospodarie, listaStringTransferuri, vaci);
                        sumaVaci += GetSuma(rowAnimal, vaci);
                    }
                    if (Convert.ToInt32(evolutiaAnimalelorPeSem2.Rows[2]["col2"]) == 0)
                    {
                        //  UpdateRandAnimal(rowAnimal, idGospodarie, animal, ovine);
                        UpdateRandAnimal(rowAnimal, idGospodarie, listaStringTransferuri, ovine);
                        sumaOvine += GetSuma(rowAnimal, ovine);
                    }
                    if (Convert.ToInt32(evolutiaAnimalelorPeSem2.Rows[3]["col2"]) == 0)
                    {
                        // UpdateRandAnimal(rowAnimal, idGospodarie, animal, caprine);
                        UpdateRandAnimal(rowAnimal, idGospodarie, listaStringTransferuri, caprine);
                        sumaCaprine += GetSuma(rowAnimal, caprine);
                    }
                    if (Convert.ToInt32(evolutiaAnimalelorPeSem2.Rows[4]["col2"]) == 0)
                    {
                        // UpdateRandAnimal(rowAnimal, idGospodarie, animal, porcine);
                        UpdateRandAnimal(rowAnimal, idGospodarie, listaStringTransferuri, porcine);
                        sumaPorcine += GetSuma(rowAnimal, porcine);
                    }
                    if (Convert.ToInt32(evolutiaAnimalelorPeSem2.Rows[5]["col2"]) == 0)
                    {
                        //  UpdateRandAnimal(rowAnimal, idGospodarie, animal, cabaline);
                        UpdateRandAnimal(rowAnimal, idGospodarie, listaStringTransferuri, cabaline);
                        sumaCabaline += GetSuma(rowAnimal, cabaline);
                    }
                    if (Convert.ToInt32(evolutiaAnimalelorPeSem2.Rows[6]["col2"]) == 0)
                    {
                        //   UpdateRandAnimal(rowAnimal, idGospodarie, animal, pasari);
                        UpdateRandAnimal(rowAnimal, idGospodarie, listaStringTransferuri, pasari);
                        sumaPasari += GetSuma(rowAnimal, pasari);
                    }
                    if (Convert.ToInt32(evolutiaAnimalelorPeSem2.Rows[7]["col2"]) == 0)
                    {
                        //   UpdateRandAnimal(rowAnimal, idGospodarie, animal, alteAnimale);
                        UpdateRandAnimal(rowAnimal, idGospodarie, listaStringTransferuri, alteAnimale);
                        sumaAlteAnimale += GetSuma(rowAnimal, alteAnimale);
                    }
                    if (Convert.ToInt32(evolutiaAnimalelorPeSem2.Rows[8]["col2"]) == 0)
                    {
                        //   UpdateRandAnimal(rowAnimal, idGospodarie, animal, albine);
                        UpdateRandAnimal(rowAnimal, idGospodarie, listaStringTransferuri, albine);
                        sumaAlbine += GetSuma(rowAnimal, albine);
                    }

                    if (evolutiaAnimalelorPeSem1.Rows.Count > 79)
                    {
                        if (Convert.ToInt32(evolutiaAnimalelorPeSem2.Rows[9]["col2"]) == 0)
                        {
                            //     UpdateRandAnimal(rowAnimal, idGospodarie, animal, pesti);
                            UpdateRandAnimal(rowAnimal, idGospodarie, listaStringTransferuri, pesti);
                            sumaPesti += GetSuma(rowAnimal, pesti);
                        }
                    }
                }
            }
            #endregion
            #region FAZA 3
            // capitolul 7, sem 2
            DataTable randuriCompletatePe7Col2;
            int areRanduriCompletatePe7Col2 = 0;
            try
            {
                randuriCompletatePe7Col2 = situatiaAnimalelorLaInceputulSemestruluiDataTable.AsEnumerable().Where(row => row.Field<decimal>("col2") != 0).CopyToDataTable();
                areRanduriCompletatePe7Col2 = randuriCompletatePe7Col2.Rows.Count;
            }
            catch { areRanduriCompletatePe7Col2 = 0; }
            if (situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows.Count > 0 && areRanduriCompletatePe7Col2 == 0)
            {
                //if (Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[0]["col2"]) != 0 ||
                //    Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[1]["col2"]) != 0 ||
                //    Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[2]["col2"]) != 0 ||
                //    Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[3]["col2"]) != 0 ||
                //    Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[4]["col2"]) != 0 ||
                //    Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[5]["col2"]) != 0 ||
                //    Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[6]["col2"]) != 0 ||
                //    Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[7]["col2"]) != 0 ||
                //    Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[8]["col2"]) != 0 ||
                //    Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[9]["col2"]) != 0 ||
                //    Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[10]["col2"]) != 0 ||
                //    Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[11]["col2"]) != 0 ||
                //    Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[12]["col2"]) != 0 ||
                //    Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[13]["col2"]) != 0 ||
                //    Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[14]["col2"]) != 0 ||
                //    Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[15]["col2"]) != 0 ||
                //    Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[16]["col2"]) != 0 ||
                //    Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[17]["col2"]) != 0 ||
                //    Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[18]["col2"]) != 0 ||
                //    Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[19]["col2"]) != 0 ||
                //    Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[20]["col2"]) != 0 ||
                //    Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[21]["col2"]) != 0 ||
                //    Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[22]["col2"]) != 0 ||
                //    Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[23]["col2"]) != 0 ||
                //    Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[24]["col2"]) != 0 ||
                //    Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[25]["col2"]) != 0 ||
                //    Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[26]["col2"]) != 0)
                //{ }
                //else // if (sumaBovine7 == 1)
                //{
                    //if (sumaBovine > 0)
                    //{
                    //update bovine femele de la sub 1 an la 1-2 ani
                    int femeleIntreUnuSiDoiAniExistente = Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[10]["col1"]);
                    int femeleIntreUnuSiDoiAniActualizate = femeleIntreUnuSiDoiAniExistente + Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[2]["col1"]) - Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[3]["col1"]);

                    //animal.Append(@"UPDATE capitole
                    //            SET col2 = '" + femeleIntreUnuSiDoiAniActualizate +
                    //              "' WHERE codCapitol = '7' AND codRand = 11 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col2 = '" + femeleIntreUnuSiDoiAniActualizate +
                                  "' WHERE codCapitol = '7' AND codRand = 11 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    //update bovine femele de la sub 6 luni la sub 1 an
                    //animal.Append(@"UPDATE capitole
                    //            SET col2 ='" + situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[3]["col1"].ToString().Replace(",", ".") +
                    //              "' WHERE codCapitol = '7' AND codRand = 3 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col2 ='" + situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[3]["col1"].ToString().Replace(",", ".") +
                                  "' WHERE codCapitol = '7' AND codRand = 3 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");

                    //update bovine masculi de la sub 1 an la 1-2 ani 
                    int masculiIntreUnuSiDoiAniExistenti = Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[11]["col1"]);
                    int masculiIntreUnuSiDoiAniActualizate = masculiIntreUnuSiDoiAniExistenti + Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[4]["col1"]) - Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[5]["col1"]);

                    //animal.Append(@"UPDATE capitole
                    //            SET col2 = '" + masculiIntreUnuSiDoiAniActualizate +
                    //              "' WHERE codCapitol = '7' AND codRand = 12 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col2 = '" + masculiIntreUnuSiDoiAniActualizate +
                                  "' WHERE codCapitol = '7' AND codRand = 12 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");

                    //update bovine masculi de la sub 6 luni la sub 1 an
                    //animal.Append(@"UPDATE capitole
                    //            SET col2 = '" + situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[5]["col1"].ToString().Replace(",", ".") +
                    //              "' WHERE codCapitol = '7' AND codRand = 5 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col2 = '" + situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[5]["col1"].ToString().Replace(",", ".") +
                                  "' WHERE codCapitol = '7' AND codRand = 5 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");

                    int totalBovineFemeleIntreUnuSiDoiAni = femeleIntreUnuSiDoiAniActualizate + Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[9]["col1"]);

                    //animal.Append(@"UPDATE capitole
                    //            SET col2 = '" + totalBovineFemeleIntreUnuSiDoiAni +
                    //              "' WHERE codCapitol = '7' AND codRand = 9 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col2 = '" + totalBovineFemeleIntreUnuSiDoiAni +
                                  "' WHERE codCapitol = '7' AND codRand = 9 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");

                    int totalBovineIntreUnuSiDoiAni = totalBovineFemeleIntreUnuSiDoiAni + masculiIntreUnuSiDoiAniActualizate;

                    //animal.Append(@"UPDATE capitole
                    //            SET col2 = '" + totalBovineIntreUnuSiDoiAni +
                    //              "' WHERE codCapitol = '7' AND codRand = 8 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col2 = '" + totalBovineIntreUnuSiDoiAni +
                                  "' WHERE codCapitol = '7' AND codRand = 8 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");

                    int totalBovineSubUnAn = Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[5]["col1"]) + Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[3]["col1"]);

                    //animal.Append(@"UPDATE capitole
                    //            SET col2 = '" + totalBovineSubUnAn +
                    //              "' WHERE codCapitol = '7' AND codRand = 2 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col2 = '" + totalBovineSubUnAn +
                                  "' WHERE codCapitol = '7' AND codRand = 2 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");

                    int totalBovine = totalBovineSubUnAn + totalBovineIntreUnuSiDoiAni + Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[12]["col1"]) + Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[25]["col1"]);

                    //animal.Append(@"UPDATE capitole
                    //            SET col2 = '" + totalBovine +
                    //              "' WHERE codCapitol = '7' AND codRand = 1 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col2 = '" + totalBovine +
                                  "' WHERE codCapitol = '7' AND codRand = 1 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");

                    //animal.Append(@"UPDATE capitole
                    //            SET col2 =  0
                    //            WHERE codCapitol = '7' AND codRand = 4 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col2 =  0
                                WHERE codCapitol = '7' AND codRand = 4 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");

                    //animal.Append(@"UPDATE capitole
                    //            SET col2 =  0
                    //            WHERE codCapitol = '7' AND codRand = 6 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col2 =  0
                                WHERE codCapitol = '7' AND codRand = 6 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");

                    //animal.Append(@"UPDATE capitole
                    //            SET col2 =  col1
                    //            WHERE codCapitol = '7' AND codRand = 7 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col2 =  col1
                                WHERE codCapitol = '7' AND codRand = 7 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");

                    //animal.Append(@"UPDATE capitole
                    //            SET col2 =  col1
                    //            WHERE codCapitol = '7' AND codRand = 10 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col2 =  col1
                                WHERE codCapitol = '7' AND codRand = 10 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");

                    //animal.Append(@"UPDATE capitole
                    //            SET col2 = col1 
                    //            WHERE codCapitol = '7' AND codRand >= 13 AND codRand <= 22 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col2 = col1 
                                WHERE codCapitol = '7' AND codRand >= 13 AND codRand <= 22 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");

                    //animal.Append(@"UPDATE capitole
                    //            SET col2 = col1 
                    //            WHERE codCapitol = '7' AND codRand >= 26 AND codRand <= 29 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col2 = col1 
                                WHERE codCapitol = '7' AND codRand >= 26 AND codRand <= 29 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    //}

                    //if (sumaVaci > 0)
                    //{
                    //animal.Append(@"UPDATE capitole
                    //            SET col2 = col1 
                    //            WHERE codCapitol = '7' AND codRand >= 23 AND codRand <= 25 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col2 = col1 
                                WHERE codCapitol = '7' AND codRand >= 23 AND codRand <= 25 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    //}
                //}
                //if (Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[29]["col2"]) == 0 &&
                //   Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[30]["col2"]) == 0 &&
                //   Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[31]["col2"]) == 0 &&
                //   Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[32]["col2"]) == 0 &&
                //   Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[33]["col2"]) == 0 &&
                //   Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[34]["col2"]) == 0 //&& sumaOvine7 == 1
                //   )
                //{
                    //if (sumaOvine > 0)
                    //{
                    //animal.Append(@"UPDATE capitole
                    //            SET col2 = col1 
                    //            WHERE codCapitol = '7' AND codRand >= 30 AND codRand <= 35 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col2 = col1 
                                WHERE codCapitol = '7' AND codRand >= 30 AND codRand <= 35 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    //}
   //             }
   //             if (Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[35]["col2"]) == 0 &&
   //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[36]["col2"]) == 0 &&
   //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[37]["col2"]) == 0 &&
   //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[38]["col2"]) == 0 &&
   //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[39]["col2"]) == 0 //&& sumaCaprine7 == 1
   //)
   //             {
                    //if (sumaCaprine > 0)
                    //{
                    //animal.Append(@"UPDATE capitole
                    //            SET col2 = col1 
                    //            WHERE codCapitol = '7' AND codRand >= 36 AND codRand <= 40 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col2 = col1 
                                WHERE codCapitol = '7' AND codRand >= 36 AND codRand <= 40 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    //}
//                }
//                if (Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[40]["col2"]) == 0 &&
//Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[41]["col2"]) == 0 &&
//Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[42]["col2"]) == 0 &&
//Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[43]["col2"]) == 0 &&
//Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[44]["col2"]) == 0 &&
//Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[45]["col2"]) == 0 &&
//Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[46]["col2"]) == 0 &&
//Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[47]["col2"]) == 0 &&
//Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[48]["col2"]) == 0 &&
//Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[49]["col2"]) == 0 &&
//Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[50]["col2"]) == 0 &&
//Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[51]["col2"]) == 0 &&
//Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[52]["col2"]) == 0 &&
//Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[53]["col2"]) == 0 &&
//Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[54]["col2"]) == 0// && sumaPorcine7 == 1
//)
//                {
                    //if (sumaPorcine > 0)
                    //{
                    //animal.Append(@"UPDATE capitole
                    //            SET col2 = col1 
                    //            WHERE codCapitol = '7' AND codRand >= 41 AND codRand <= 55 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col2 = col1 
                                WHERE codCapitol = '7' AND codRand >= 41 AND codRand <= 55 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    //}
//                }
//                if (Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[55]["col2"]) == 0 &&
//Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[56]["col2"]) == 0 &&
//Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[57]["col2"]) == 0 &&
//Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[58]["col2"]) == 0 &&
//Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[59]["col2"]) == 0// && sumaCabaline7 == 1
//)
//                {
                    //if (sumaCabaline > 0)
                    //{
                    //animal.Append(@"UPDATE capitole
                    //            SET col2 = col1 
                    //            WHERE codCapitol = '7' AND codRand >= 56 AND codRand <= 60 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col2 = col1 
                                WHERE codCapitol = '7' AND codRand >= 56 AND codRand <= 60 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    //}
//                }
//                //if (sumaAlteAnimale > 0)
//                //{
//                if (Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[60]["col2"]) == 0 &&
//Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[61]["col2"]) == 0// && sumaAlteAnimale7 == 1
//)
//                {

                    //animal.Append(@"UPDATE capitole
                    //        SET col2 = col1 
                    //        WHERE codCapitol = '7' AND codRand >= 61 AND codRand <= 62 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    listaStringTransferuri.Add(@"UPDATE capitole
                            SET col2 = col1 
                            WHERE codCapitol = '7' AND codRand >= 61 AND codRand <= 62 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
//                }
//                if (Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[62]["col2"]) == 0 &&
//Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[63]["col2"]) == 0 &&
//Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[64]["col2"]) == 0 &&
//Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[65]["col2"]) == 0 &&
//Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[66]["col2"]) == 0// && sumaAlteAnimale7 == 1
//)
//                {

                    //animal.Append(@"UPDATE capitole
                    //        SET col2 = col1 
                    //        WHERE codCapitol = '7' AND codRand >= 63 AND codRand <= 67 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    listaStringTransferuri.Add(@"UPDATE capitole
                            SET col2 = col1 
                            WHERE codCapitol = '7' AND codRand >= 63 AND codRand <= 67 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
    //            }
    //            //}

    //            if (Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[67]["col2"]) == 0 &&
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[68]["col2"]) == 0 &&
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[69]["col2"]) == 0 &&
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[70]["col2"]) == 0 &&
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[71]["col2"]) == 0 &&
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[72]["col2"]) == 0 &&
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[73]["col2"]) == 0 &&
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[74]["col2"]) == 0 &&
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[75]["col2"]) == 0// && sumaPasari7 == 1
    //)
    //            {
                    //if (sumaPasari > 0)
                    //{
                    //animal.Append(@"UPDATE capitole
                    //            SET col2 = col1 
                    //            WHERE codCapitol = '7' AND codRand >= 68 AND codRand <= 74 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col2 = col1 
                                WHERE codCapitol = '7' AND codRand >= 68 AND codRand <= 74 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");

                    //animal.Append(@"UPDATE capitole
                    //            SET col2 = col1 
                    //             WHERE codCapitol = '7' AND codRand >= 75 AND codRand <= 76 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col2 = col1 
                                 WHERE codCapitol = '7' AND codRand >= 75 AND codRand <= 76 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    //}
                //}
                //if (Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[76]["col2"]) == 0// && sumaAlteAnimale7 == 1
                //    )
                //{
                    //if (sumaAlteAnimale > 0)
                    //{
                    //animal.Append(@"UPDATE capitole
                    //            SET col2 = col1 
                    //            WHERE codCapitol = '7' AND codRand = 77 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col2 = col1 
                                WHERE codCapitol = '7' AND codRand = 77 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    //}
                //}
                //if (Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[77]["col2"]) == 0// && sumaAlbine7 == 1
                //    )
                //{
                    //    if (sumaAlbine > 0)
                    //{
                    //animal.Append(@"UPDATE capitole
                    //            SET col2 = col1 
                    //            WHERE codCapitol = '7' AND codRand = 78 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    listaStringTransferuri.Add(@"UPDATE capitole
                                SET col2 = col1 
                                WHERE codCapitol = '7' AND codRand = 78 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                    //}
    //            }

    //            if (situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows.Count > 78)

    //                if (Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[78]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[79]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[80]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[81]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[82]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[83]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[84]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[85]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[86]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[87]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[88]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[89]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[90]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[91]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[92]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[93]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[94]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[95]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[96]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[97]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[98]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[99]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[100]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[101]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[102]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[103]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[104]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[105]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[106]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[107]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[108]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[109]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[110]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[111]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[112]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[113]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[114]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[115]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[116]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[117]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[118]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[119]["col2"]) != 0 ||
    //Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[120]["col2"]) != 0// && sumaPesti7 == 1
    //)
    //                {
                        //if (sumaPesti > 0)
                        //{
                        //animal.Append(@"UPDATE capitole
                        //        SET col2 = col1 
                        //        WHERE codCapitol = '7' AND codRand >= 79 AND codRand <= 121 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                        listaStringTransferuri.Add(@"UPDATE capitole
                                SET col2 = col1 
                                WHERE codCapitol = '7' AND codRand >= 79 AND codRand <= 121 AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
                        //}
                    //}
            }
            #endregion
            #region FAZA 4 invalidari
            if (sumaBovine + sumaCabaline + sumaCaprine + sumaOvine + sumaPasari + sumaPorcine + sumaVaci + sumaAlteAnimale + sumaAlbine < 0)
            {
                PopulateDataTable(raportTable, gospodariiRow);
                string insert = @"INSERT INTO invalidarisemestruanimale (idGospodarie,idUnitate,semestru,an)
                                    VALUES ('" + idGospodarie + "','" + unitate.ToString() + "','2','" + an + "')";
                SqlCommand insertCommand = new SqlCommand(insert, connection);
                try
                {
                    insertCommand.ExecuteNonQuery();
                    ClassLog.fLog(1, Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "capitole", "transfer animale, unit." + unitate + ", gosp " + idGospodarie.ToString(), "scris invalidarisemestruanimale", idGospodarie, 0);
                }
                catch
                {
                    ClassLog.fLog(1, Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "capitole", "transfer animale, unit." + unitate + ", gosp " + idGospodarie.ToString(), "esuat scris invalidarisemestruanimale", idGospodarie, 0);
                }
            }
            #endregion
            nrGospodariiParcurse++;
        }   // inchidem ciclul, face insert pe TOATE gospodariile deodata 
        #region FAZA 5 insert


        ClassLog.fLog(1, Convert.ToInt32(HttpContext.Current.Session["SESutilizatorId"]), DateTime.Now, "transfer animale " + an, "unitatea:" + unitate, "incheiat crearea string update pt." + nrGospodariiParcurse + " gospodarii parcurse; nr.linii: " + listaStringTransferuri.Count.ToString(), 0, 0);
        //            SqlCommand insertToDBCommand = new SqlCommand(animal.ToString().Replace(",", "."), connection);

        // le impartim pe pachete de 2500 linii
        List<string> listaStringTransferuriPachete = new List<string>();
        int contor = 0;
        int contorTot = 0;
        int inserturi = 0;
        int updateuri = 0;
        string stringTransferuriPachete = "";
        foreach (string stringTransfer in listaStringTransferuri)
        {
            string stringTransferFinal = stringTransfer;
            if (stringTransferFinal.IndexOf("UPDATE") != -1)
            {
                stringTransferFinal = stringTransferFinal.Replace(",", ".");
                updateuri++;
            }
            else if (stringTransferFinal.IndexOf("INSERT") != -1)
            {
                inserturi++;
            }
            stringTransferuriPachete += stringTransferFinal;
            contor++; contorTot++;
            if (contor > 2500 || contorTot == listaStringTransferuri.Count)
            {
                listaStringTransferuriPachete.Add(stringTransferuriPachete);
                stringTransferuriPachete = "";
                contor = 0;
            }
        }
        // return;
        //SqlCommand insertToDBCommand = new SqlCommand(animal.ToString().Replace(",", "."), connection);
        int contorPachete = 0;
        foreach (string pachet in listaStringTransferuriPachete)
        {
            contorPachete++;
            SqlCommand insertToDBCommand = new SqlCommand(pachet, connection);
            insertToDBCommand.CommandTimeout = 100000;
            try
            {
                insertToDBCommand.ExecuteNonQuery();
                //ClassLog.fLog(Convert.ToInt32(HttpContext.Current.Session["SESunitateId"]), Convert.ToInt32(HttpContext.Current.Session["SESutilizatorId"]), DateTime.Now, "transfer animale " + an, "unitatea:" + unitate, "terminat faza 4;  insertToDBCommand reusit", idGospodarie, 0);
                ClassLog.fLog(1, Convert.ToInt32(HttpContext.Current.Session["SESutilizatorId"]), DateTime.Now, "transfer animale " + an, "unitatea:" + unitate + "; nr linii total:" + contorTot, "terminat pachet nr." + contorPachete + ";  insertToDBCommand reusit;", 0, 0);
            }
            catch (Exception x)
            {
                ClassLog.fLog(1, Convert.ToInt32(HttpContext.Current.Session["SESutilizatorId"]), DateTime.Now, "transfer animale " + an, "unitatea:" + unitate, "pachet nr." + contorPachete + "; insertToDBCommand ESUAT; " + pachet, 0, 0);
            }
        }
        #endregion
        ClassLog.fLog(1, Convert.ToInt32(HttpContext.Current.Session["SESutilizatorId"]), DateTime.Now, "transfer animale " + an, "unitatea:" + unitate + "; nr linii total:" + contorTot, "terminat " + contorPachete + " pachete; inserturi:" + inserturi + "; updateuri: " + updateuri, 0, 0);
        corelatiiGridView.DataSource = raportTable;
        corelatiiGridView.DataBind();

        if (corelatiiGridView.Rows.Count > 0)
        {
            corelatiiLabel.Text = "Exista corelatii invalide la capitolul 7 pe semestrul 2 la urmatoarele gospodarii";
            inchidereSemestruAnimaleButton.Visible = false;
            exportToExcelButton.Visible = true;
            exportToWordButton.Visible = true;

        }
        else
        {
            corelatiiLabel.Text = "Închidere de semestru animale realizată cu succes";
            inchidereSemestruAnimaleButton.Visible = false;
        }



        ManipuleazaBD.InchideConexiune(connection);
        ClassLog.fLog(1, Convert.ToInt32(HttpContext.Current.Session["SESutilizatorId"]), DateTime.Now, "transfer animale " + an, "unitatea:" + unitate, "terminat transfer; nrGospodariiParcurse: " + nrGospodariiParcurse.ToString(), 0, 0);
    } // inchidem ciclul, face insert pe FIECARE gospodarie in parte
      //}

    private int GetSuma(DataRow rowAnimal, Tuple<short, short> tuple)
    {
        return Convert.ToInt32(rowAnimal["codRand"]) == tuple.Item1 ? Convert.ToInt32(rowAnimal["col1"]) : 0;
    }

    private void UpdateRandAnimal(DataRow rowAnimal, int idGospodarie, StringBuilder animal, Tuple<short, short> coduriAnimal)
    {
        if (Convert.ToInt32(rowAnimal["codRand"]) == coduriAnimal.Item1)
        {
            animal.Append(@"UPDATE capitole SET col2 = " + rowAnimal["col1"].ToString().Replace(",", ".") + @" 
                                        WHERE codCapitol = '8' AND codRand = " + coduriAnimal.Item2 + " AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
            animal.Append(@"UPDATE capitole SET col2 = " + rowAnimal["col1"].ToString().Replace(",", ".") + @" 
                                        WHERE codCapitol = '8' AND codRand = " + coduriAnimal.Item1 + " AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
        }
    }
    private void UpdateRandAnimal(DataRow rowAnimal, int idGospodarie, List<string> listaStringTransferuri, Tuple<short, short> coduriAnimal)
    {
        if (Convert.ToInt32(rowAnimal["codRand"]) == coduriAnimal.Item1)
        {
            listaStringTransferuri.Add(@"
UPDATE capitole SET col2 = " + rowAnimal["col1"].ToString().Replace(",", ".") + @" 
                                        WHERE codCapitol = '8' AND codRand = " + coduriAnimal.Item2 + " AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
            listaStringTransferuri.Add(@"
UPDATE capitole SET col2 = " + rowAnimal["col1"].ToString().Replace(",", ".") + @" 
                                        WHERE codCapitol = '8' AND codRand = " + coduriAnimal.Item1 + " AND gospodarieId = " + idGospodarie + " AND an = " + an + "; ");
        }
    }
    private void FillGospodariiTable(out DataTable gospodariiTable, out SqlConnection connection)
    {

        // luam doar gospodariile care au date pe cap 7 sau cap 8, suma lor fiind > 0
        string command = @"SELECT [gospodarii].[gospodarieId], [gospodarii].[volum], [gospodarii].[nrPozitie],[gospodarii].[membruNume],[gospodarii].[membruCnp],[unitati].[unitateDenumire]
                                FROM [gospodarii]
								inner join 
								(
                                    SELECT [gospodarieId], sum([col1]) as suma
                                    FROM capitole 
                                    WHERE [codCapitol] in ('7','8')  AND [an] = " + an + " AND [unitateId] = " + Session["SESunitateId"] + @" 
                                    group by [gospodarieId] having sum([col1])>0
                                ) as gospodariiCuValoriAnimale
				                        on gospodarii.[gospodarieId] = gospodariiCuValoriAnimale.[gospodarieId]

                                JOIN unitati ON unitati.unitateId = gospodarii.unitateId
                             WHERE gospodarii.unitateId = " + Session["SESunitateId"] + " AND an = " + an + "";

        connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(an));
        SqlDataAdapter gospodariiAdapter = new SqlDataAdapter(command, connection);

        gospodariiTable = new DataTable();
        gospodariiTable.Clear();

        gospodariiAdapter.Fill(gospodariiTable);
    }
    private void FillGospodariiTable(out DataTable gospodariiTable, out SqlConnection connection, string unitate)
    {

        // luam doar gospodariile care au date pe cap 7 sau cap 8, suma lor fiind > 0
        string command = @"SELECT [gospodarii].[gospodarieId], [gospodarii].[volum], [gospodarii].[nrPozitie],[gospodarii].[membruNume],[gospodarii].[membruCnp],[unitati].[unitateDenumire]
                                FROM [gospodarii]
								inner join 
								(
                                    SELECT [gospodarieId], sum([col1]) as suma
                                    FROM capitole 
                                    WHERE [codCapitol] in ('7','8')  AND [an] = " + an + " AND [unitateId] = " + unitate + @" 
                                    group by [gospodarieId] having sum([col1])>0
                                ) as gospodariiCuValoriAnimale
				                        on gospodarii.[gospodarieId] = gospodariiCuValoriAnimale.[gospodarieId]

                                JOIN unitati ON unitati.unitateId = gospodarii.unitateId
                             WHERE gospodarii.unitateId = " + unitate + " AND an = " + an + "";

        connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(an));
        SqlDataAdapter gospodariiAdapter = new SqlDataAdapter(command, connection);

        gospodariiTable = new DataTable();
        gospodariiTable.Clear();

        gospodariiAdapter.Fill(gospodariiTable);
    }

    private void StergeInvalidariAnimale()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(an));
        string delete = @"DELETE
                            FROM InvalidariSemestruAnimale where idUnitate = " + Session["SESunitateId"] + "";
        SqlCommand deleteCommand = new SqlCommand(delete, connection);
        deleteCommand.ExecuteNonQuery();
        ManipuleazaBD.InchideConexiune(connection);
    }
    private void StergeInvalidariAnimale(string unitate)
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(an));
        string delete = @"DELETE
                            FROM InvalidariSemestruAnimale where idUnitate = " + unitate + "";
        SqlCommand deleteCommand = new SqlCommand(delete, connection);
        deleteCommand.ExecuteNonQuery();
        ManipuleazaBD.InchideConexiune(connection);
    }

    private DataTable GetCorelatiiInvalide()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(an));
        string interogare = @"SELECT DISTINCT volum as 'Volum', nrPozitie as 'Poziie',membruNume as 'Cap de gospodarie', membruCnp as 'CNP' 
                                 FROM gospodarii
                               JOIN InvalidariSemestruAnimale 
                               ON InvalidariSemestruAnimale.idGospodarie = gospodarii.gospodarieId
                            WHERE gospodarii.unitateId = " + Session["SESunitateId"] + " AND InvalidariSemestruAnimale.an = " + an + " AND semestru = '2'";
        DataTable corelatiiTable = new DataTable();
        SqlDataAdapter corelatiiAdapter = new SqlDataAdapter(interogare, connection);
        corelatiiAdapter.Fill(corelatiiTable);
        ManipuleazaBD.InchideConexiune(connection);
        return corelatiiTable;

    }

    public void PopulateDataTable(DataTable raportTable, DataRow row)
    {
        DataRow corelatiiRow = raportTable.NewRow();
        corelatiiRow["volum"] = row["volum"];
        corelatiiRow["pozitie"] = row["nrPozitie"];
        corelatiiRow["Cap de gospodarie"] = row["membruNume"];
        corelatiiRow["CNP"] = row["membruCNP"];
        raportTable.Rows.Add(corelatiiRow);
    }

    private DataTable CreateRaport()
    {
        DataTable table = new DataTable();
        table.Columns.Add("Volum");
        table.Columns.Add("Pozitie");
        table.Columns.Add("Cap de gospodarie");
        table.Columns.Add("CNP");
        return table;
    }

    private void ExportGridToExcel()
    {
        Response.Clear();
        Response.Buffer = true;
        string FileName = "Inchidere semestru capitol 7 " + DateTime.Now + ".xls";
        Response.AddHeader("content-disposition",
        "attachment;filename=" + FileName);
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        FormatGrid(sw);
        string style = @"<style> .textmode { mso-number-format:\@; } </style>";
        Response.Write(style);
        Response.Output.Write(sw.ToString());
        Response.Flush();
        Response.End();
    }

    private void ExportGridToWord()
    {
        Response.Clear();
        Response.Buffer = true;
        string FileName = "Inchidere semestru capitol 7 " + DateTime.Now + ".doc";
        Response.AddHeader("content-disposition",
        "attachment;filename=" + FileName);
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-word ";
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        FormatGrid(sw);
        Response.Output.Write(sw.ToString());
        Response.Flush();
        Response.End();
    }

    private void FormatGrid(StringWriter sw)
    {
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        corelatiiGridView.AllowPaging = false;
        corelatiiGridView.HeaderRow.Style.Add("background-color", "#FFFFFF");
        corelatiiGridView.HeaderRow.Cells[0].Style.Add("background-color", "green");
        corelatiiGridView.HeaderRow.Cells[1].Style.Add("background-color", "green");
        corelatiiGridView.HeaderRow.Cells[2].Style.Add("background-color", "green");
        corelatiiGridView.HeaderRow.Cells[3].Style.Add("background-color", "green");
        corelatiiGridView.HeaderRow.Cells[0].Width = 100;
        corelatiiGridView.HeaderRow.Cells[1].Width = 100;
        corelatiiGridView.HeaderRow.Cells[2].Width = 200;
        corelatiiGridView.HeaderRow.Cells[3].Width = 200;

        for (int i = 0; i < corelatiiGridView.Rows.Count; i++)
        {
            GridViewRow row = corelatiiGridView.Rows[i];
            row.BackColor = System.Drawing.Color.White;
            row.Attributes.Add("class", "textmode");
            if (i % 2 != 0)
            {
                row.Cells[0].Style.Add("background-color", "#C2D69B");
                row.Cells[1].Style.Add("background-color", "#C2D69B");
                row.Cells[2].Style.Add("background-color", "#C2D69B");
                row.Cells[3].Style.Add("background-color", "#C2D69B");
            }
        }
        corelatiiGridView.RenderControl(hw);
    }
    protected void exportToExcelButton_Click(object sender, EventArgs e)
    {
        ExportGridToExcel();
    }
    protected void exportToWordButton_Click(object sender, EventArgs e)
    {
        ExportGridToWord();
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
    }


    protected void teste(object sender, EventArgs e)
    {


        DataTable d = AnimaleServices.GetEvolutiaAnimalelorInCursulAnuluiByUnitateAndByAndPeSem2(2015, 39);
        d.DefaultView.Sort = "gospodarieId";
        DataTable dsort = new DataTable();
        dsort = d.DefaultView.ToTable();
        //List<Capitole> lista = new List<Capitole>();
        //Capitole capitol = new Capitole();
        //capitol.Col1 = "1";
        //capitol.Col2 = "50";
        //lista.Add(capitol);
        //capitol = new Capitole();
        //capitol.Col1 = "3";
        //capitol.Col2 = "10";
        //lista.Add(capitol);
        //capitol = new Capitole();
        //capitol.Col1 = "4";
        //capitol.Col2 = "40";
        //lista.Add(capitol);
        //capitol = new Capitole();
        //capitol.Col1 = "1";
        //capitol.Col2 = "30";
        //lista.Add(capitol);
        //var lista1 = lista.OrderBy(a => a.Col1).ThenBy(a => a.Col2).ToList();
    }


}