﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="contulMeu.aspx.cs" Inherits="contulMeu" Culture="ro-RO" UICulture="ro-RO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Configurări / Contul meu" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="upUpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnValSum" runat="server">
                <asp:ValidationSummary ID="valSum" runat="server" DisplayMode="SingleParagraph" Visible="true"
                    ValidationGroup="GrupValidare" CssClass="validator" ForeColor="" />
            </asp:Panel>
            <asp:Panel ID="pnPanelGeneral" runat="server">
                <asp:Panel ID="pnAdaugaUtilizator" CssClass="panel_general" runat="server">
                    <asp:Panel ID="pnAfiseazaMesaje" runat="server">
                        <asp:Label ID="lblEroare" runat="server" CssClass="validator" Text="Eroare" Visible="false" />
                    </asp:Panel>
                    <asp:Panel ID="pnMaster" CssClass="adauga" runat="server">
                        <asp:Panel ID="pnDetalii" runat="server">
                            <div>
                                Conform regulilor impuse de securitatea datelor incluse în Registrul Agricol, <strong>
                                    vă rugăm să vă schimbaţi parola cel puţin o dată la 3 luni de zile</strong>.
                                Parola va trebui să fie diferită de ultimele 10 parole folosite de Dvs. în tntregistrulagricol.ro.
                                De asemenea, sunt obligatorii următorele date: <strong>NUME</strong>, <strong>PRENUME</strong>
                                şi <strong>ADRESA DE EMAIL</strong> (o adresă reală, validă, la care puteţi fi contactat).
                            </div>
                            <hr />
                            <p>
                                <asp:Label ID="lblNume" runat="server" Text="Nume" CssClass="indent"></asp:Label>
                                <asp:TextBox ID="tbNume" runat="server"></asp:TextBox>
                                <asp:Label ID="lblPrenume" runat="server" Text="Prenume" CssClass="indent"></asp:Label>
                                <asp:TextBox ID="tbPrenume" runat="server"></asp:TextBox>
                            </p>
                            <p>
                                <asp:Label ID="lblCNP" runat="server" Text="CNP" CssClass="indent"></asp:Label>
                                <asp:TextBox ID="tbCNP" runat="server"></asp:TextBox>
                                <asp:Label ID="lblFunctia" runat="server" Text="Funcţia" CssClass="indent"></asp:Label>
                                <asp:TextBox ID="tbFunctia" runat="server"></asp:TextBox>
                            </p>
                            <p>
                                <asp:Label ID="lblAdresa" runat="server" Text="Adresa" CssClass="indent"></asp:Label>
                                <asp:TextBox ID="tbAdresa" TextMode="MultiLine" Rows="3" Width="500px" runat="server"></asp:TextBox>
                            </p>
                            <p>
                                <asp:Label ID="lblTelefon" runat="server" Text="Telefon fix" CssClass="indent"></asp:Label>
                                <asp:TextBox ID="tbTelefon" runat="server"></asp:TextBox>
                                <asp:Label ID="lblMobil" runat="server" Text="Telefon mobil" CssClass="indent"></asp:Label>
                                <asp:TextBox ID="tbMobil" runat="server"></asp:TextBox>
                            </p>
                            <p>
                                <asp:Label ID="lblEmail" runat="server" Text="E-mail" CssClass="indent"></asp:Label>
                                <asp:TextBox ID="tbEmail" Width="300px" runat="server"></asp:TextBox>
                            </p>
                            <p>
                                <asp:Label ID="lblDurataSesiuneLucru" runat="server" Text="Durată sesiune lucru"></asp:Label>
                                <asp:TextBox ID="tbDurataSesiuneLucru" Width="50px" MaxLength="2" runat="server"></asp:TextBox>
                            </p>
                            <asp:Panel ID="Panel1" runat="server">
                                <h2>
                                    Schimbare parolă</h2>
                                <p>
                                    <asp:Label ID="lblParolaVeche" runat="server" Text="Parola veche" CssClass="indent"></asp:Label>
                                    <asp:TextBox ID="tbParolaVeche" TextMode="Password" runat="server"></asp:TextBox>
                                </p>
                                <p>
                                    <asp:Label ID="lblParolaNoua" runat="server" Text="Parola nouă" CssClass="indent"></asp:Label>
                                    <asp:TextBox ID="tbParolaNoua" TextMode="Password" runat="server"></asp:TextBox>
                                </p>
                                <p>
                                    <asp:Label ID="lblParolaConfirm" runat="server" Text="Confirmare parolă" CssClass="indent"></asp:Label>
                                    <asp:TextBox ID="tbParolaConfirm" TextMode="Password" runat="server"></asp:TextBox>
                                </p>
                                <p style="font-size: 12px; color: Gray;">
                                    Parola trebuie să conţină:
                                    <br />
                                    - minim 8 caractere;
                                    <br />
                                    - cel puţin o literă mare şi una mică
                                    <br />
                                    - cel puţin o cifră
                                    <br />
                                    - cel puţin un caracter special dintre următoarele: @ # $ % ^ & + = ? ! > < . *
                                    ~ |
                                </p>
                            </asp:Panel>
                        </asp:Panel>
                        <asp:Panel ID="pnButoanePrincipale" runat="server" CssClass="butoane">
                            <asp:Button ID="btModificaCont" runat="server" CssClass="buton" Text="modifica utilizatorul"
                                ValidationGroup="GrupValidare" OnClick="btModificaCont_Click" />
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel ID="validatoare" CssClass="ascunsa" runat="server">
                    <!-- //////////// VALIDATOARE  \\\\\\\\\\\\\\\\\\ -->
                    <asp:CustomValidator ID="valCustom" runat="server" ErrorMessage="" ValidationGroup="GrupValidare"></asp:CustomValidator>
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
