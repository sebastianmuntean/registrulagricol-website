﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;

public partial class CentralizatorDosare : System.Web.UI.Page
{
    SqlCommand command = new SqlCommand();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "Cap.2b", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
        }
        if (deLaTextBox.Text == "")
            deLaTextBox.Text = "01.01.1900";
        if (panaLaTextBox.Text == "")
            panaLaTextBox.Text = "12.12.2099";
        RefreshListaDosareGridView();
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        
    }

    protected void printCentralizatorDosareButton_OnClick(Object sender, EventArgs e)
    {
        if (deLaTextBox.Text == "")
            deLaTextBox.Text = "01.01.1900";
        if (panaLaTextBox.Text == "")
            panaLaTextBox.Text = "12.12.2099";
        string data1 = Convert.ToDateTime(deLaTextBox.Text).ToString("yyyy-MM-dd");
        string data2 = Convert.ToDateTime(panaLaTextBox.Text).ToString("yyyy-MM-dd");

        string redirectString = @"~/printCentralizatorDosare.aspx?&nrDosar=" + nrDosarTextBox.Text + "&nrRegistruGeneral=" + nrRegistruGeneralTextBox.Text + "&volumInt=" + volumTextBox.Text + "&nrPozitieInt=" + pozitieTextBox.Text + "&deLa=" + data1 + "&panaLa=" +data2+ "&membruNume=" + vanzatorTextBox.Text;

        ResponseHelper.Redirect(redirectString, "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
        ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "centralizatorDosare", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
    }

    protected void dosareGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(dosareGridView, e, this);
        e.Row.Cells[0].Visible = false;
    }

    protected void dosareGridView_SelectedIndexChanged(object sender, EventArgs e)
    {
    }

    protected void PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        dosareGridView.PageIndex = e.NewPageIndex;
        RefreshListaDosareGridView();
    }

    private DataTable GetListaOferte()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        DataTable table = CreateCentralizatorOferteTableStructure();
        command.Connection = connection;
        command.CommandText = @"select * from OferteVanzare join parcele on OferteVanzare.idParcela = parcele.parcelaId  INNER JOIN sabloaneCapitole ON parcele.parcelaCategorie = sabloaneCapitole.codRand Inner join gospodarii on gospodarii.gospodarieId = OferteVanzare.idGospodarie WHERE (sabloaneCapitole.capitol = '2a')  AND (parcele.an = " + Session["SESan"] + ") AND (sabloaneCapitole.an = " + Session["SESan"] + ") AND (OferteVanzare.unitateId = " + Session["SESunitateId"] + ") AND OferteVanzare.nrDosar LIKE '%" + nrDosarTextBox.Text + "%' AND OferteVanzare.nrRegistruGeneral LIKE '%" + nrRegistruGeneralTextBox.Text + "%'   and (gospodarii.membruNume LIKE '%" + vanzatorTextBox.Text + "%') and (volumInt LIKE  '%" + volumTextBox.Text + "%' ) and (nrPozitieInt LIKE  '%" + pozitieTextBox.Text + "%') and (dataInregistrarii between convert(datetime,'" + deLaTextBox.Text + "',104) and convert(datetime,'" + panaLaTextBox.Text + "',104)) order by nrDosar,nrRegistruGeneral";
        SqlDataReader dataReader = command.ExecuteReader();
        while (dataReader.Read())
        {
            PopulateListaCentralizatorOferteTable(table, dataReader);
        }
        ManipuleazaBD.InchideConexiune(connection);
        return table;
    }

    private static void PopulateListaCentralizatorOferteTable(DataTable table, SqlDataReader dataReader)
    {
        DataRow row = table.NewRow();
        row["idOferta"] = dataReader["idOferta"];
        row["Nr. Cerere"] = dataReader["nrDosar"];
        row["Nr. Registru general"] = dataReader["nrRegistruGeneral"];
        row["Data inregistrarii"] = Convert.ToDateTime(dataReader["dataInregistrarii"]).ToShortDateString();
        row["Denumire parcela"] = dataReader["parcelaDenumire"];
        row["Nr TOPO"] = dataReader["parcelaNrTopo"];
        row["CF"] = dataReader["parcelaCF"];
        row["Nr. Cadastral"] = dataReader["parcelaNrCadastral"];
        row["Nr. Cadastral provizioriu"] = dataReader["parcelaNrCadastralProvizoriu"];
        row["Bloc fizic"] = dataReader["parcelaNrBloc"];
        row["Adresa"] = dataReader["parcelaAdresa"];
        row["Suprafata oferita Ha"] = dataReader["suprafataOferitaHa"];
        row["Suprafata oferita Ari"] = dataReader["suprafataOferitaAri"];
        row["Suma"] = dataReader["valoareParcela"];
        row["Categorie"] = dataReader["denumire4"];
        row["Volum"] = dataReader["volumInt"];
        row["Nr.pozitie"] = dataReader["nrPozitieInt"];
        row["Vanzator"] = dataReader["membruNume"];
        table.Rows.Add(row);
    }

    private static DataTable CreateCentralizatorOferteTableStructure()
    {
        DataTable table = new DataTable();
        table.Columns.Add("idOferta");
        table.Columns.Add("Nr. Cerere");
        table.Columns.Add("Nr. Registru general");
        table.Columns.Add("Data inregistrarii");
        table.Columns.Add("Vanzator");
        table.Columns.Add("Volum");
        table.Columns.Add("Nr.pozitie");
        table.Columns.Add("Denumire parcela");
        table.Columns.Add("Categorie");
        table.Columns.Add("Nr TOPO");
        table.Columns.Add("CF");
        table.Columns.Add("Nr. Cadastral");
        table.Columns.Add("Nr. Cadastral provizioriu");
        table.Columns.Add("Bloc Fizic");
        table.Columns.Add("Adresa");
        table.Columns.Add("Suprafata oferita Ha");
        table.Columns.Add("Suprafata oferita Ari");
        table.Columns.Add("Suma");
        return table;
    }

    private void RefreshListaDosareGridView()
    {
        dosareGridView.DataSource = GetListaOferte();
        dosareGridView.DataBind();
    }
}