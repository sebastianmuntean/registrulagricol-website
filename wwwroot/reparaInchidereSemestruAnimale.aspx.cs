﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class reparaInchidereSemestruAnimale : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            tbAn.Text = Session["SESan"].ToString();
        btAdauga.Visible = false;
    }
    protected void btAdauga_Click(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "UPDATE capitol[1]e SET col2 = col1 WHERE (unitateId = '" + ddlUnitate.SelectedValue + "') AND (codCapitol IN ('7')) AND (an = '" + tbAn.Text + "')";
        vCmd.ExecuteNonQuery();
        ManipuleazaBD.InchideConexiune(vCon);
    }
    protected List<List<string>> NumarRanduriInCapitole()
    {
        List<List<string>> numarRanduriInCapitole = new List<List<string>> { };
        List<string> rezultate = new List<string> { "numarRanduriInCapitol", "capitol" };
        numarRanduriInCapitole = ManipuleazaBD.fRezultaListaStringuri(@"
            SELECT count(codrand) as numarRanduriInCapitol, capitol FROM[TNTRegistruAgricol3].[tntcomputers].[sabloaneCapitole] where an = 2015 and capitol not like 'c%'
  group by capitol order by capitol ", rezultate, 2016);
        return numarRanduriInCapitole;
    }
    protected void btCurataCapitole_Click(object sender, EventArgs e)
    {
        // numarul de randuri din fiecare capitol - folosim pentru ca sa optimizam cate inregistrari sa extragem o data
        List<List<string>> numarRanduriInCapitole = NumarRanduriInCapitole();
        lblTotalSterse.Text = "<br />Unitatea: " + ddlUnitate.SelectedValue + " - " + ddlUnitate.SelectedItem.Text + " | start: " + DateTime.Now;
        string unitate = ddlUnitate.SelectedValue;
        ClassLog.fLog(Convert.ToInt32(Session["SESunitateId"]), Convert.ToInt32(Session["SESutilizatorId"]), DateTime.Now, "capitole", "curata gunoi unit. " + unitate.ToString(), "Unitatea: " + ddlUnitate.SelectedValue + " - " + ddlUnitate.SelectedItem.Text, 0, 0);
        List<string> ani = new List<string> { "2015" };
        //List<string> ani = new List<string> { "2011", "2012", "2013", "2014", "2015", "2016" };

        List<string> capitole = new List<string> { "2a", "3", "4a", "4a1", "4b1", "4b2", "4c", "5a", "5b", "5c", "5d", "6", "7", "8", "9", "10a", "10b" };

        string delete = "";
        int numarRanduriAfectate = 0;
        int totalGeneralSterse = 0;
        foreach (string an in ani)
        {
            foreach (List<string> capitol in numarRanduriInCapitole)
            {
                ClassLog.fLog(Convert.ToInt32(Session["SESunitateId"]), Convert.ToInt32(Session["SESutilizatorId"]), DateTime.Now, "capitole", "curata gunoi unit. " + unitate.ToString(), "cap: " + capitol[1], 0, 0);
                lblTotalSterse.Text += "<br />inceput sterge cap: " + capitol[1] + " - ";
                lblTotalSterse.Text += "<br />inceput sterge cap: " + capitol[1] + " - ";
                delete = @"DELETE  from capitole where
    gospodarieId in (
				  SELECT top (" + Convert.ToInt32(2500 / Convert.ToInt32(capitol[0])) + @") [gospodarieId]
				  FROM capitole 
                  WHERE [codCapitol] =  '" + capitol[1] + @"'   AND [an] = " + an.ToString() + @"  AND [unitateId] = " + unitate + @"
				  group by [gospodarieId] having sum([col1])=0 and sum([col2])=0 and sum([col3])=0 and sum([col4])=0 and sum([col5])=0 and sum([col6])=0 and sum([col7])=0 and sum([col8])=0 ) 
			  AND [codCapitol] =   '" + capitol[1] + @"'   
			  AND [an] = " + an.ToString() + @" 
			  AND [unitateId] =  " + unitate + @"";
                numarRanduriAfectate = 1;
                while (numarRanduriAfectate > 0)
                {
                    ManipuleazaBD.fManipuleazaBD(delete, 2016, out numarRanduriAfectate);
                    lblTotalSterse.Text += numarRanduriAfectate.ToString() + " + ";
                    totalGeneralSterse += numarRanduriAfectate;
                    ClassLog.fLog(Convert.ToInt32(Session["SESunitateId"]), Convert.ToInt32(Session["SESutilizatorId"]), DateTime.Now, "capitole", "curata gunoi unit. " + unitate.ToString(), "cap. " + capitol[1] + "randuri afectate: " + numarRanduriAfectate.ToString(), 0, 0);
                }

                delete = "";
            }


        }
        ClassLog.fLog(Convert.ToInt32(Session["SESunitateId"]), Convert.ToInt32(Session["SESutilizatorId"]), DateTime.Now, "capitole", "curata gunoi unit. " + unitate.ToString(), "finalizata la: " + DateTime.Now + " | TOTAL RANDURI STERSE:" + totalGeneralSterse, 0, 0);
        lblTotalSterse.Text += "<br />finalizata la: " + DateTime.Now;
        lblTotalSterse.Text += "<h3>TOTAL RANDURI STERSE: " + totalGeneralSterse + "</h3>";

        //lblTotalInregistrariInaintePeCapitol.Text = delete;
    }
    protected void btCurataMaiMulte_Click(object sender, EventArgs e)
    {
        // numarul de randuri din fiecare capitol - folosim pentru ca sa optimizam cate inregistrari sa extragem o data
        List<List<string>> numarRanduriInCapitole = NumarRanduriInCapitole();
        List<string> unitati = new List<string> {
         TextBox1.Text, TextBox2.Text, TextBox3.Text, TextBox4.Text, TextBox5.Text, TextBox6.Text, TextBox7.Text, TextBox8.Text, TextBox9.Text, TextBox10.Text,
         TextBox11.Text, TextBox12.Text, TextBox13.Text, TextBox14.Text, TextBox15.Text, TextBox16.Text, TextBox17.Text, TextBox18.Text, TextBox19.Text, TextBox20.Text,
         TextBox21.Text, TextBox22.Text, TextBox23.Text, TextBox24.Text, TextBox25.Text, TextBox26.Text, TextBox27.Text, TextBox28.Text, TextBox29.Text, TextBox30.Text,
         TextBox31.Text, TextBox32.Text, TextBox33.Text, TextBox34.Text, TextBox35.Text, TextBox36.Text, TextBox37.Text, TextBox38.Text, TextBox39.Text, TextBox40.Text
        };
        List<string> ani = new List<string> { "2013" };
        //List<string> ani = new List<string> { "2011", "2012", "2013", "2014", "2015", "2016" };
        List<string> capitole = new List<string> { "2a", "3", "4a", "4a1", "4b1", "4b2", "4c", "5a", "5b", "5c", "5d", "6", "7", "8", "9", "10a", "10b" };

        /// string interogare = "Select count(capitolid) as tot from capitole WHERE [codCapitol] = '5b'  AND[an] = " + an.ToString() + @"  AND[unitateId] =" + ddlUnitate.SelectedValue;
        //  lblTotalInregistrariInaintePeCapitol.Text=ManipuleazaBD.fRezultaUnString(interogare, "tot", an);
        string delete = "";
        int numarRanduriAfectate = 0;
        int totalGeneralSterse = 0;
        foreach (string unitate in unitati)
        {
            ClassLog.fLog(Convert.ToInt32(Session["SESunitateId"]), Convert.ToInt32(Session["SESutilizatorId"]), DateTime.Now, "capitole", "curata gunoi unit. " + unitate.ToString(), "Unitatea: " + ddlUnitate.SelectedValue + " - " + ddlUnitate.SelectedItem.Text, 0, 0);
            lblTotalSterse.Text = "<h2>Unitatea: " + ddlUnitate.SelectedValue + " - " + ddlUnitate.SelectedItem.Text + " | start: " + DateTime.Now + "</h2>";
            foreach (string an in ani)
            {

                foreach (List<string> capitol in numarRanduriInCapitole)
                {
                    ClassLog.fLog(Convert.ToInt32(Session["SESunitateId"]), Convert.ToInt32(Session["SESutilizatorId"]), DateTime.Now, "capitole", "curata gunoi unit. " + unitate.ToString(), "cap: " + capitol[1], 0, 0);
                    lblTotalSterse.Text += "<br />inceput sterge cap: " + capitol[1] + " - ";
                    delete = @"DELETE  from capitole where
    gospodarieId in (
				  SELECT top (" + Convert.ToInt32(2500 / Convert.ToInt32(capitol[0])) + @") [gospodarieId]
				  FROM capitole 
                  WHERE [codCapitol] =  '" + capitol[1] + @"'   AND [an] = " + an.ToString() + @"  AND [unitateId] = " + unitate + @"
				  group by [gospodarieId] having sum([col1])=0 and sum([col2])=0 and sum([col3])=0 and sum([col4])=0 and sum([col5])=0 and sum([col6])=0 and sum([col7])=0 and sum([col8])=0 ) 
			  AND [codCapitol] =   '" + capitol[1] + @"'   
			  AND [an] = " + an.ToString() + @" 
			  AND [unitateId] =  " + unitate + @"";
                    numarRanduriAfectate = 1;
                    while (numarRanduriAfectate > 0)
                    {
                        ManipuleazaBD.fManipuleazaBD(delete, 2016, out numarRanduriAfectate);
                        lblTotalSterse.Text += numarRanduriAfectate.ToString() + " + ";
                        ClassLog.fLog(Convert.ToInt32(Session["SESunitateId"]), Convert.ToInt32(Session["SESutilizatorId"]), DateTime.Now, "capitole", "curata gunoi unit. " + unitate.ToString() + "  an:" + an.ToString(), "cap. " + capitol[1] + "randuri afectate: " + numarRanduriAfectate.ToString(), 0, 0);
                        totalGeneralSterse += numarRanduriAfectate;
                    }

                    delete = "";
                }


            }
            ClassLog.fLog(Convert.ToInt32(Session["SESunitateId"]), Convert.ToInt32(Session["SESutilizatorId"]), DateTime.Now, "capitole", "curata gunoi unit. " + unitate.ToString(), "finalizata la: " + DateTime.Now + " | TOTAL RANDURI STERSE:" + totalGeneralSterse, 0, 0);
            lblTotalSterse.Text += "<br />finalizata la: " + DateTime.Now;
            lblTotalSterse.Text += "<h3>TOTAL RANDURI STERSE: " + totalGeneralSterse + "</h3>";
        }
        //lblTotalInregistrariInaintePeCapitol.Text = delete;
    }
    protected void btCurataMaiMulteDeLaPanaLa_Click(object sender, EventArgs e)
    {
        // numarul de randuri din fiecare capitol - folosim pentru ca sa optimizam cate inregistrari sa extragem o data
        List<List<string>> numarRanduriInCapitole = NumarRanduriInCapitole();
        List<string> unitati = new List<string> {
         TextBox1.Text, TextBox2.Text, TextBox3.Text, TextBox4.Text, TextBox5.Text, TextBox6.Text, TextBox7.Text, TextBox8.Text, TextBox9.Text, TextBox10.Text,
         TextBox11.Text, TextBox12.Text, TextBox13.Text, TextBox14.Text, TextBox15.Text, TextBox16.Text, TextBox17.Text, TextBox18.Text, TextBox19.Text, TextBox20.Text,
         TextBox21.Text, TextBox22.Text, TextBox23.Text, TextBox24.Text, TextBox25.Text, TextBox26.Text, TextBox27.Text, TextBox28.Text, TextBox29.Text, TextBox30.Text,
         TextBox31.Text, TextBox32.Text, TextBox33.Text, TextBox34.Text, TextBox35.Text, TextBox36.Text, TextBox37.Text, TextBox38.Text, TextBox39.Text, TextBox40.Text
        };
        List<string> ani = new List<string> { "2010" };
        //List<string> ani = new List<string> { "2011", "2012", "2013", "2014", "2015", "2016" };
        List<string> capitole = new List<string> { "2a", "3", "4a", "4a1", "4b1", "4b2", "4c", "5a", "5b", "5c", "5d", "6", "7", "8", "9", "10a", "10b" };

        /// string interogare = "Select count(capitolid) as tot from capitole WHERE [codCapitol] = '5b'  AND[an] = " + an.ToString() + @"  AND[unitateId] =" + ddlUnitate.SelectedValue;
        //  lblTotalInregistrariInaintePeCapitol.Text=ManipuleazaBD.fRezultaUnString(interogare, "tot", an);
        string delete = "";
        int numarRanduriAfectate = 0;
        int totalGeneralSterse = 0;
        for (int i = Convert.ToInt32(TextBox1.Text); i <= Convert.ToInt32(TextBox2.Text); i++)
        {
            string unitate = i.ToString();
            ClassLog.fLog(Convert.ToInt32(Session["SESunitateId"]), Convert.ToInt32(Session["SESutilizatorId"]), DateTime.Now, "capitole", "curata gunoi unit. " + unitate.ToString(), "Unitatea: " + ddlUnitate.SelectedValue + " - " + ddlUnitate.SelectedItem.Text, 0, 0);
            lblTotalSterse.Text = "<h2>Unitatea: " + ddlUnitate.SelectedValue + " - " + ddlUnitate.SelectedItem.Text + " | start: " + DateTime.Now + "</h2>";
            foreach (string an in ani)
            {

                foreach (List<string> capitol in numarRanduriInCapitole)
                {
                    ClassLog.fLog(Convert.ToInt32(Session["SESunitateId"]), Convert.ToInt32(Session["SESutilizatorId"]), DateTime.Now, "capitole", "curata gunoi unit. " + unitate.ToString(), "cap: " + capitol[1], 0, 0);
                    lblTotalSterse.Text += "<br />inceput sterge cap: " + capitol[1] + " - ";
                    delete = @"DELETE  from capitole where
    gospodarieId in (
				  SELECT top (" + Convert.ToInt32(2500 / Convert.ToInt32(capitol[0])) + @") [gospodarieId]
				  FROM capitole 
                  WHERE [codCapitol] =  '" + capitol[1] + @"'   AND [an] = " + an.ToString() + @"  AND [unitateId] = " + unitate + @"
				  group by [gospodarieId] having sum([col1])=0 and sum([col2])=0 and sum([col3])=0 and sum([col4])=0 and sum([col5])=0 and sum([col6])=0 and sum([col7])=0 and sum([col8])=0 ) 
			  AND [codCapitol] =   '" + capitol[1] + @"'   
			  AND [an] = " + an.ToString() + @" 
			  AND [unitateId] =  " + unitate + @"";
                    numarRanduriAfectate = 1;
                    while (numarRanduriAfectate > 0)
                    {
                        ManipuleazaBD.fManipuleazaBD(delete, 2016, out numarRanduriAfectate);
                        lblTotalSterse.Text += numarRanduriAfectate.ToString() + " + ";
                        ClassLog.fLog(Convert.ToInt32(Session["SESunitateId"]), Convert.ToInt32(Session["SESutilizatorId"]), DateTime.Now, "capitole", "curata gunoi unit. " + unitate.ToString() + "  an:" + an.ToString(), "cap. " + capitol[1] + "randuri afectate: " + numarRanduriAfectate.ToString(), 0, 0);
                        totalGeneralSterse += numarRanduriAfectate;
                    }

                    delete = "";
                }


            }
            ClassLog.fLog(Convert.ToInt32(Session["SESunitateId"]), Convert.ToInt32(Session["SESutilizatorId"]), DateTime.Now, "capitole", "curata gunoi unit. " + unitate.ToString(), "finalizata la: " + DateTime.Now + " | TOTAL RANDURI STERSE:" + totalGeneralSterse, 0, 0);
            lblTotalSterse.Text += "<br />finalizata la: " + DateTime.Now;
            lblTotalSterse.Text += "<h3>TOTAL RANDURI STERSE: " + totalGeneralSterse + "</h3>";
        }
        //lblTotalInregistrariInaintePeCapitol.Text = delete;
    }
    protected void btVerifica_Click(object sender, EventArgs e)
    {

        lblColoane.Text = "";
        // List<string> ani = new List<string> { "2011", "2012", "2013", "2014", "2015", "2016" };
        List<string> ani = new List<string> { "2015" };
        string unitate = ddlUnitate.SelectedValue;
        foreach (string an in ani)
        {
            string interogare =
                @"

SELECT * FROM 
(
 /* cap 2a */
 select count(capitolid) as _2a from capitole where
   gospodarieId in (SELECT top  10000 [gospodarieId]
          FROM capitole 
                  WHERE [codCapitol] = '2a'  AND [an] = " + an.ToString() + @"  AND [unitateId] = " + unitate + @"
				  group by [gospodarieId] having 
sum([col1])=0 and sum([col2])=0 and sum([col3])=0 and sum([col4])=0 and sum([col5])=0 and sum([col6])=0 and sum([col7])=0 and sum([col8])=0 ) and
  [codCapitol] = '2a'  AND [an] = " + an.ToString() + @"  AND [unitateId] = " + unitate + @" ) as t2a, (
 
 /* cap 3*/
 select count(capitolid) as _3 from capitole where
   gospodarieId in (SELECT top  10000 [gospodarieId]
          FROM capitole 
                  WHERE [codCapitol] = '3'  AND [an] = " + an.ToString() + @"  AND [unitateId] = " + unitate + @"
				  group by [gospodarieId] having 
sum([col1])=0 and sum([col2])=0 and sum([col3])=0 and sum([col4])=0 and sum([col5])=0 and sum([col6])=0 and sum([col7])=0 and sum([col8])=0 ) and
  [codCapitol] = '3'  AND [an] = " + an.ToString() + @"  AND [unitateId] = " + unitate + @") as t3, (

 /* cap 4a*/
 select count(capitolid) as _4a from capitole where
   gospodarieId in (SELECT top  10000 [gospodarieId]
          FROM capitole 
                  WHERE [codCapitol] = '4a'  AND [an] = " + an.ToString() + @"  AND [unitateId] = " + unitate + @"
				  group by [gospodarieId] having 
sum([col1])=0 and sum([col2])=0 and sum([col3])=0 and sum([col4])=0 and sum([col5])=0 and sum([col6])=0 and sum([col7])=0 and sum([col8])=0 ) and
  [codCapitol] = '4a'  AND [an] = " + an.ToString() + @"  AND [unitateId] = " + unitate + @" ) as t4a, (

 /* cap 4a1*/
 select count(capitolid) _4a1  from capitole where
   gospodarieId in (SELECT top  10000 [gospodarieId]
          FROM capitole 
                  WHERE [codCapitol] = '4a1'  AND [an] = " + an.ToString() + @"  AND [unitateId] = " + unitate + @"
				  group by [gospodarieId] having 
sum([col1])=0 and sum([col2])=0 and sum([col3])=0 and sum([col4])=0 and sum([col5])=0 and sum([col6])=0 and sum([col7])=0 and sum([col8])=0 ) and
  [codCapitol] = '4a1'  AND [an] = " + an.ToString() + @"  AND [unitateId] = " + unitate + @" ) as t4a1, (

 /* cap 4b1*/
 select count(capitolid) _4b1 from capitole where
   gospodarieId in (SELECT top  10000 [gospodarieId]
          FROM capitole 
                  WHERE [codCapitol] = '4b1'  AND [an] = " + an.ToString() + @"  AND [unitateId] = " + unitate + @"
				  group by [gospodarieId] having 
sum([col1])=0 and sum([col2])=0 and sum([col3])=0 and sum([col4])=0 and sum([col5])=0 and sum([col6])=0 and sum([col7])=0 and sum([col8])=0 ) and
  [codCapitol] = '4b1'  AND [an] = " + an.ToString() + @"  AND [unitateId] = " + unitate + @" ) as t4b1, (

 /* cap 4b2*/
 select count(capitolid) _4b2 from capitole where
   gospodarieId in (SELECT top  10000 [gospodarieId]
          FROM capitole 
                  WHERE [codCapitol] = '4b2'  AND [an] = " + an.ToString() + @"  AND [unitateId] = " + unitate + @"
				  group by [gospodarieId] having 
sum([col1])=0 and sum([col2])=0 and sum([col3])=0 and sum([col4])=0 and sum([col5])=0 and sum([col6])=0 and sum([col7])=0 and sum([col8])=0 ) and
  [codCapitol] = '4b2'  AND [an] = " + an.ToString() + @"  AND [unitateId] = " + unitate + @" ) as t4b2, (

 /* cap 4c*/
 select count(capitolid) _4c from capitole where
   gospodarieId in (SELECT top  10000 [gospodarieId]
          FROM capitole 
                  WHERE [codCapitol] = '4c'  AND [an] = " + an.ToString() + @"  AND [unitateId] = " + unitate + @"
				  group by [gospodarieId] having 
sum([col1])=0 and sum([col2])=0 and sum([col3])=0 and sum([col4])=0 and sum([col5])=0 and sum([col6])=0 and sum([col7])=0 and sum([col8])=0 ) and
  [codCapitol] = '4c'  AND [an] = " + an.ToString() + @"  AND [unitateId] = " + unitate + @" ) as t4c, (

 /* cap 5a*/
 select count(capitolid) as _5a from capitole where
   gospodarieId in (SELECT top  10000 [gospodarieId]
          FROM capitole 
                  WHERE [codCapitol] = '5a'  AND [an] = " + an.ToString() + @"  AND [unitateId] = " + unitate + @"
				  group by [gospodarieId] having 
sum([col1])=0 and sum([col2])=0 and sum([col3])=0 and sum([col4])=0 and sum([col5])=0 and sum([col6])=0 and sum([col7])=0 and sum([col8])=0 ) and
  [codCapitol] = '5a'  AND [an] = " + an.ToString() + @"  AND [unitateId] = " + unitate + @" ) as t5a, (

 /* cap 5b*/
 select count(capitolid) as _5b from capitole where
   gospodarieId in (SELECT top  10000 [gospodarieId]
          FROM capitole 
                  WHERE [codCapitol] = '5b'  AND [an] = " + an.ToString() + @"  AND [unitateId] = " + unitate + @"
				  group by [gospodarieId] having 
sum([col1])=0 and sum([col2])=0 and sum([col3])=0 and sum([col4])=0 and sum([col5])=0 and sum([col6])=0 and sum([col7])=0 and sum([col8])=0 ) and
  [codCapitol] = '5b'  AND [an] = " + an.ToString() + @"  AND [unitateId] = " + unitate + @" ) as t5b, (

 /* cap 5c*/
 select count(capitolid) as _5c  from capitole where
   gospodarieId in (SELECT top  10000 [gospodarieId]
          FROM capitole 
                  WHERE [codCapitol] = '5c'  AND [an] = " + an.ToString() + @"  AND [unitateId] = " + unitate + @"
				  group by [gospodarieId] having 
sum([col1])=0 and sum([col2])=0 and sum([col3])=0 and sum([col4])=0 and sum([col5])=0 and sum([col6])=0 and sum([col7])=0 and sum([col8])=0 ) and
  [codCapitol] = '5c'  AND [an] = " + an.ToString() + @"  AND [unitateId] = " + unitate + @" ) as t5c, (

 /* cap 5d*/
 select count(capitolid) _5d from capitole where
   gospodarieId in (SELECT top  10000 [gospodarieId]
          FROM capitole 
                  WHERE [codCapitol] = '5d'  AND [an] = " + an.ToString() + @"  AND [unitateId] = " + unitate + @"
				  group by [gospodarieId] having 
sum([col1])=0 and sum([col2])=0 and sum([col3])=0 and sum([col4])=0 and sum([col5])=0 and sum([col6])=0 and sum([col7])=0 and sum([col8])=0 ) and
  [codCapitol] = '5d'  AND [an] = " + an.ToString() + @"  AND [unitateId] = " + unitate + @" ) as t5d, (

 /* cap 6*/
 select count(capitolid) as _6 from capitole where
   gospodarieId in (SELECT top  10000 [gospodarieId]
          FROM capitole 
                  WHERE [codCapitol] = '6'  AND [an] = " + an.ToString() + @"  AND [unitateId] = " + unitate + @"
				  group by [gospodarieId] having 
sum([col1])=0 and sum([col2])=0 and sum([col3])=0 and sum([col4])=0 and sum([col5])=0 and sum([col6])=0 and sum([col7])=0 and sum([col8])=0 ) and
  [codCapitol] = '6'  AND [an] = " + an.ToString() + @"  AND [unitateId] = " + unitate + @" ) as t6, (



 /* cap 7*/
 select count(capitolid) as _7 from capitole where
   gospodarieId in (SELECT top  10000 [gospodarieId]
          FROM capitole 
                  WHERE [codCapitol] = '7'  AND [an] = " + an.ToString() + @"  AND [unitateId] = " + unitate + @"
				  group by [gospodarieId] having 
sum([col1])=0 and sum([col2])=0 and sum([col3])=0 and sum([col4])=0 and sum([col5])=0 and sum([col6])=0 and sum([col7])=0 and sum([col8])=0 ) and
  [codCapitol] = '7'  AND [an] = " + an.ToString() + @"  AND [unitateId] = " + unitate + @" ) as t7, (



 /* cap 8*/
 select count(capitolid) as _8 from capitole where
   gospodarieId in (SELECT top  10000 [gospodarieId]
          FROM capitole 
                  WHERE [codCapitol] = '8'  AND [an] = " + an.ToString() + @"  AND [unitateId] = " + unitate + @"
				  group by [gospodarieId] having 
sum([col1])=0 and sum([col2])=0 and sum([col3])=0 and sum([col4])=0 and sum([col5])=0 and sum([col6])=0 and sum([col7])=0 and sum([col8])=0 ) and
  [codCapitol] = '8'  AND [an] = " + an.ToString() + @"  AND [unitateId] = " + unitate + @" ) as t8, (



 /* cap 9*/
 select count(capitolid) as _9 from capitole where
   gospodarieId in (SELECT top  10000 [gospodarieId]
          FROM capitole 
                  WHERE [codCapitol] = '9'  AND [an] = " + an.ToString() + @"  AND [unitateId] = " + unitate + @"
				  group by [gospodarieId] having 
sum([col1])=0 and sum([col2])=0 and sum([col3])=0 and sum([col4])=0 and sum([col5])=0 and sum([col6])=0 and sum([col7])=0 and sum([col8])=0 ) and
  [codCapitol] = '9'  AND [an] = " + an.ToString() + @"  AND [unitateId] = " + unitate + @" ) as t9, (



 /* cap 10a*/
 select count(capitolid) as _10a from capitole where
   gospodarieId in (SELECT top  10000 [gospodarieId]
          FROM capitole 
                  WHERE [codCapitol] = '10a'  AND [an] = " + an.ToString() + @"  AND [unitateId] = " + unitate + @"
				  group by [gospodarieId] having 
sum([col1])=0 and sum([col2])=0 and sum([col3])=0 and sum([col4])=0 and sum([col5])=0 and sum([col6])=0 and sum([col7])=0 and sum([col8])=0 ) and
  [codCapitol] = '10a'  AND [an] = " + an.ToString() + @"  AND [unitateId] = " + unitate + @" ) as t10a, (



 /* cap 10b*/
 select count(capitolid) as _10b from capitole where
   gospodarieId in (SELECT top  10000 [gospodarieId]
          FROM capitole 
                  WHERE [codCapitol] = '10b'  AND [an] = " + an.ToString() + @"  AND [unitateId] = " + unitate + @"
				  group by [gospodarieId] having 
sum([col1])=0 and sum([col2])=0 and sum([col3])=0 and sum([col4])=0 and sum([col5])=0 and sum([col6])=0 and sum([col7])=0 and sum([col8])=0 ) and
  [codCapitol] = '10b'  AND [an] = " + an.ToString() + @"  AND [unitateId] = " + unitate + @" ) as t10b

";
            lblColoane.Text += "<h3>Anul " + an + ":</h3> ";
            List<string> capitole = new List<string> { "2a", "3", "4", "4a", "4a1", "4b1", "4b2", "4c", "5a", "5b", "5c", "5d", "6", "7", "8", "9", "10a", "10b", "11" };

            List<string> coloane = new List<string> { "_2a", "_3", "_4", "_4a", "_4a1", "_4b1", "_4b2", "_4c", "_5a", "_5b", "_5c", "_5d", "_6", "_7", "_8", "_9", "_10a", "_10b", "_11" };
            int contor = 0;
            int total = 0;
            int contorColoana = 0;
            ClassLog.fLog(Convert.ToInt32(Session["SESunitateId"]), Convert.ToInt32(Session["SESutilizatorId"]), DateTime.Now, "capitole", "verifica gunoi unit. " + unitate.ToString(), " | inceput verificare la " + DateTime.Now + " | anul de verificat: " + an, 0, 0);
            foreach (string capitol in capitole)
            {
                string select = @"select count(capitolid) as " + coloane[contorColoana].ToString() + @" from capitole where
   gospodarieId in (SELECT [gospodarieId]
          FROM capitole
                  WHERE[codCapitol] = '" + capitol.ToString() + "'  AND[an] = " + an.ToString() + @"  AND[unitateId] = " + unitate.ToString() + @"

                  group by[gospodarieId] having
sum([col1])= 0 and sum([col2]) = 0 and sum([col3])= 0 and sum([col4]) = 0 and sum([col5])= 0 and sum([col6]) = 0 and sum([col7])= 0 and sum([col8]) = 0 ) and
              [codCapitol] = '" + capitol.ToString() + "'  AND[an] = " + an.ToString() + @"  AND[unitateId] = " + unitate.ToString() + @"";
                string rezultat = "";
                try
                {
                    rezultat = ManipuleazaBD.fRezultaUnString(select, coloane[contorColoana], 2016);
                    ClassLog.fLog(Convert.ToInt32(Session["SESunitateId"]), Convert.ToInt32(Session["SESutilizatorId"]), DateTime.Now, "capitole", "verifica gunoi unit. " + unitate.ToString(), "cap. " + capitol + ": " + rezultat.ToString() + " inregistrari", 0, 0);
                }
                catch (Exception x)
                {
                    ClassLog.fLog(Convert.ToInt32(Session["SESunitateId"]), Convert.ToInt32(Session["SESutilizatorId"]), DateTime.Now, "capitoleEROARE", "verifica gunoi unit. " + unitate.ToString() + " cap. " + capitol, "EROARE: " + rezultat.ToString() + " X = " + x, 0, 0);
                }
                //foreach (string coloana in coloane)
                //{

                lblColoane.Text += coloane[contorColoana] + ": <span style='color:red'>" + rezultat.ToString() + "</span> | ";
                total += Convert.ToInt32(rezultat);
                contor++;
                //}

                contorColoana++;
            }
            lblColoane.Text += "------  <h3 style = 'color:red' >TOTAL: " + total + "</h3><br />";
            ClassLog.fLog(Convert.ToInt32(Session["SESunitateId"]), Convert.ToInt32(Session["SESutilizatorId"]), DateTime.Now, "capitole", "verifica gunoi unit. " + unitate.ToString(), "terminat verificare la " + DateTime.Now.ToString() + " | TOTAL: " + total.ToString(), 0, 0);

        }
    }
}
