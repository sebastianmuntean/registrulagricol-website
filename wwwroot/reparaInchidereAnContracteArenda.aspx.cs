﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class reparaInchidereAnContracteArenda : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            tbAn.Text = DateTime.Now.AddYears(-1).Year.ToString();
        }
    }
    protected void btAdauga_Click(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd1 = new SqlCommand();
        SqlCommand vCmd2 = new SqlCommand();
        vCmd1.Connection = vCon;
        vCmd2.Connection = vCon;
        // fac lista de unitati
        List<string> vListaUnitati = new List<string>();
        vCmd1.CommandText = "select unitateId from unitati where convert(nvarchar,unitateId) like '" + ddlUnitate.SelectedValue + "'";
        SqlDataReader vTabel = vCmd1.ExecuteReader();
        while (vTabel.Read())
            vListaUnitati.Add(vTabel["unitateId"].ToString());
        vTabel.Close();
        foreach (string vUnitateId in vListaUnitati)
        {
            // parcurg contracte
            vCmd2.CommandText = "SELECT parceleAtribuiriContracte.contractId, parceleAtribuiriContracte.unitateId, parceleAtribuiriContracte.contractGospodarieIdDa, parceleAtribuiriContracte.contractGospodarieIdPrimeste, parceleAtribuiriContracte.contractDataInceput, parceleAtribuiriContracte.contractDataFinal, parceleAtribuiriContracte.contractDetalii, parceleAtribuiriContracte.contractTip, parceleAtribuiriContracte.contractDataSemnarii, parceleAtribuiriContracte.contractNr, parceleAtribuiriContracte.contractActiv, parceleAtribuiriContracte.contractIdImport, parceleAtribuiriContracte.contractSub, COALESCE (gospodarii.gospodarieIdInitial, 0) AS gospodarieIdInitialDa, COALESCE (gospodarii_1.gospodarieIdInitial, 0) AS gospodarieIdInitialPrimeste FROM parceleAtribuiriContracte INNER JOIN gospodarii ON parceleAtribuiriContracte.contractGospodarieIdDa = gospodarii.gospodarieId INNER JOIN gospodarii AS gospodarii_1 ON parceleAtribuiriContracte.contractGospodarieIdPrimeste = gospodarii_1.gospodarieId WHERE (gospodarii.an = '" + tbAn.Text + "') AND (gospodarii.unitateId = '" + vUnitateId + "') AND (YEAR(parceleAtribuiriContracte.contractDataFinal) > " + tbAn.Text + ")";
            vTabel = vCmd2.ExecuteReader();
            List<ListaSelect> vListaContracte = new List<ListaSelect>();
            while (vTabel.Read())
            {
                ListaSelect vElementLista = new ListaSelect()
                {
                    Col1 = vTabel["contractId"].ToString(),
                    Col2 = vTabel["contractGospodarieIdDa"].ToString(),
                    Col3 = vTabel["contractGospodarieIdPrimeste"].ToString(),
                    Col4 = vTabel["contractDataInceput"].ToString(),
                    Col5 = vTabel["contractDataFinal"].ToString(),
                    Col6 = vTabel["contractDetalii"].ToString(),
                    Col7 = vTabel["contractTip"].ToString(),
                    Col8 = vTabel["contractDataSemnarii"].ToString(),
                    Col9 = vTabel["contractNr"].ToString(),
                    Col10 = vTabel["contractActiv"].ToString(),
                    Col11 = vTabel["contractIdImport"].ToString(),
                    Col12 = vTabel["contractSub"].ToString(),
                    Col13 = vTabel["gospodarieIdInitialDa"].ToString(),
                    Col14 = vTabel["gospodarieIdInitialPrimeste"].ToString()
                };
                vListaContracte.Add(vElementLista);
            }
            vTabel.Close();
            foreach (ListaSelect vContract in vListaContracte)
            {
                // iau id gospodarie da nou
                vCmd1.CommandText = "select gospodarieId from gospodarii where unitateId='" + vUnitateId + "' and an='" + (Convert.ToInt32(tbAn.Text) + 1).ToString() + "' and gospodarieIdInitial in ('" + vContract.Col13 + "', '" + vContract.Col2 + "')";
                string vGospodarieIdDaNou = vCmd1.ExecuteScalar().ToString();
                // iau id gospodarie primeste nou
                vCmd1.CommandText = "select gospodarieId from gospodarii where unitateId='" + vUnitateId + "' and an='" + (Convert.ToInt32(tbAn.Text) + 1).ToString() + "' and gospodarieIdInitial in ('" + vContract.Col14 + "', '" + vContract.Col3 + "')";
                string vGospodarieIdPrimesteNou = vCmd1.ExecuteScalar().ToString();
                // adaug contract si iau id nou
                vCmd1.CommandText = "INSERT INTO parceleAtribuiriContracte (unitateId, contractGospodarieIdDa, contractGospodarieIdPrimeste, contractDataInceput, contractDataFinal, contractDetalii, contractTip, contractDataSemnarii, contractNr, contractActiv, contractIdImport, contractSub) VALUES (0, '" + vGospodarieIdDaNou + "', '" + vGospodarieIdPrimesteNou + "', convert(datetime,'" + vContract.Col4 + "',104), convert(datetime,'" + vContract.Col5 + "',104), N'" + vContract.Col6 + "', '" + vContract.Col7 + "', convert(datetime,'" + vContract.Col8 + "',104), '" + vContract.Col9 + "', '" + Convert.ToBoolean(vContract.Col10) + "', 0, '" + vContract.Col12 + "'); select scope_identity() as [scope_identity]";
                string vContractIdNou = vCmd1.ExecuteScalar().ToString();
                // extrag parcele pentru contract curent
                vCmd1.CommandText = "SELECT parceleModUtilizare.modUtilizareId, parceleModUtilizare.parcelaId, parceleModUtilizare.c3Rand, parceleModUtilizare.c3Ha, parceleModUtilizare.c3Ari, parceleModUtilizare.anul, parceleModUtilizare.unitateId, parceleModUtilizare.gospodarieId, parceleModUtilizare.catreGospodarieId, parceleModUtilizare.catreC3Rand, parceleModUtilizare.contractId, parceleModUtilizare.modUtilizareIdImport, parcele.parcelaIdInitial FROM parceleModUtilizare INNER JOIN parcele ON parceleModUtilizare.parcelaId = parcele.parcelaId WHERE (parceleModUtilizare.contractId = '" + vContract.Col1 + "')";
                vTabel = vCmd1.ExecuteReader();
                List<ListaSelect> vListaParcele = new List<ListaSelect>();
                while (vTabel.Read())
                {
                    ListaSelect vElementLista = new ListaSelect()
                    {
                        Col1 = vTabel["parcelaId"].ToString(),
                        Col2 = vTabel["c3Rand"].ToString(),
                        Col3 = vTabel["c3Ha"].ToString(),
                        Col4 = vTabel["c3Ari"].ToString(),
                        Col5 = vTabel["anul"].ToString(),
                        Col6 = vTabel["unitateId"].ToString(),
                        Col7 = vTabel["gospodarieId"].ToString(),
                        Col8 = vTabel["catreGospodarieId"].ToString(),
                        Col9 = vTabel["catreC3Rand"].ToString(),
                        Col10 = vTabel["contractId"].ToString(),
                        Col11 = vTabel["modUtilizareIdImport"].ToString(),
                        Col12 = vTabel["parcelaIdInitial"].ToString()
                    };
                    vListaParcele.Add(vElementLista);
                }
                vTabel.Close();
                string vInterogare = "";
                // parcurg parcele contract si adaug cu contractId nou
                foreach (ListaSelect vParcela in vListaParcele)
                {
                    // iau id parcela nou
                    vCmd1.CommandText = "select parcelaId from parcele where unitateaId='" + vUnitateId + "' and an='" + (Convert.ToInt32(tbAn.Text) + 1).ToString() + "' and parcelaIdInitial in ('" + vParcela.Col12 + "', '" + vParcela.Col1 + "')";                    
                    string vParcelaIdNou ="0";
                    try{
                    vParcelaIdNou = vCmd1.ExecuteScalar().ToString();}
                    catch{}
                    if (vParcelaIdNou != "0")
                    {
                        vInterogare += "INSERT INTO parceleModUtilizare (parcelaId, c3Rand, c3Ha, c3Ari, anul, unitateId, gospodarieId, catreGospodarieId, catreC3Rand, contractId, modUtilizareIdImport) VALUES ('" + vParcelaIdNou + "', '" + vParcela.Col2 + "', '" + Convert.ToDecimal(vParcela.Col3).ToString().Replace(",", ".") + "', '" + Convert.ToDecimal(vParcela.Col4).ToString().Replace(",", ".") + "', '" + (Convert.ToInt32(tbAn.Text) + 1).ToString() + "', '" + vParcela.Col6 + "', '" + vGospodarieIdDaNou + "', '" + vGospodarieIdPrimesteNou + "', '" + vParcela.Col9 + "', '" + vContractIdNou + "', 0); ";
                    }
                }
                if (vInterogare != "")
                {
                    vCmd1.CommandText = vInterogare;
                    vCmd1.ExecuteNonQuery();
                    vInterogare = "";
                }
            }
        }
        ManipuleazaBD.InchideConexiune(vCon);
        valCustom.IsValid = false;
        valCustom.ErrorMessage = "Închiderea de an pentru contracte arendă a fost realizată cu succes!";
    }
}