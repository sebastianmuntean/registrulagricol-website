﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.IO;
using System.Data;
using System.Data.SqlClient;

public partial class printCentralizatorDosare : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!IsPostBack)
            {
                DataTable reportDataTable = GetDataTAbleForReport();
                DataTable unitatiDataTable = GetUnitateDenumire();
                ReportViewer1.LocalReport.DataSources.Clear();
                ReportDataSource ds = new ReportDataSource("rapoarteDosareVanzareDataSet_dtCentralizatorDosare", reportDataTable);
                ReportDataSource ds2 = new ReportDataSource("dsRapoarte_dtUnitati", unitatiDataTable);
                ReportViewer1.LocalReport.DataSources.Add(ds);
                ReportViewer1.LocalReport.DataSources.Add(ds2);
                ReportViewer1.LocalReport.Refresh();
                ReportViewer1.Visible = true;
            }
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "printCerereVanzare", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
        }
    }

    private DataTable GetUnitateDenumire()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        string interogare = @"SELECT        unitati.unitateId, unitati.unitateDenumire, unitati.unitateCodSiruta, unitati.judetId, unitati.localitateId, unitati.unitateStrada, unitati.unitateNr, unitati.unitateAp, 
                         unitati.unitateCodPostal, unitati.localitateComponentaId, unitati.unitateCodFiscal, unitati.unitateActiva, unitati.unitatePrincipala, judete.judetDenumire, 
                         localitati.localitateDenumire
FROM            unitati INNER JOIN
                         judete ON unitati.judetId = judete.judetId INNER JOIN
                         localitati ON unitati.localitateId = localitati.localitateId
WHERE        (unitati.unitateId = " + Session["SESunitateId"].ToString() + ")";
        SqlDataAdapter adapter = new SqlDataAdapter(interogare, connection);
        DataTable table = new DataTable();
        adapter.Fill(table);
        ManipuleazaBD.InchideConexiune(connection);
        return table;
    }

    private DataTable GetDataTAbleForReport()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        string interogare = @"SELECT        OferteVanzare.nrDosar, OferteVanzare.nrRegistruGeneral, OferteVanzare.suprafataOferitaAri, OferteVanzare.suprafataOferitaHa, parcele.parcelaDenumire, parcele.parcelaNrTopo, parcele.parcelaCF, 
                         parcele.parcelaNrBloc, parcele.parcelaNrCadastral, parcele.parcelaNrCadastralProvizoriu, OferteVanzare.valoareParcela, gospodarii.judet, gospodarii.localitate, unitati.unitateDenumire, 
                         OferteVanzare.dataInregistrarii, gospodarii.membruNume, gospodarii.volum, gospodarii.nrPozitie, parcele.parcelaAdresa
FROM            OferteVanzare INNER JOIN
                         parcele ON parcele.parcelaId = OferteVanzare.idParcela INNER JOIN
                         gospodarii ON gospodarii.gospodarieId = OferteVanzare.idGospodarie INNER JOIN
                         unitati ON unitati.unitateId = OferteVanzare.unitateId
WHERE        (OferteVanzare.unitateId = "+Session["SESUnitateId"]+@") 
ORDER BY OferteVanzare.nrDosar, OferteVanzare.nrRegistruGeneral";
        SqlDataAdapter adapter = new SqlDataAdapter(interogare, connection);
        DataTable table = new DataTable();
        adapter.Fill(table);
        ManipuleazaBD.InchideConexiune(connection);
        return table;
    }

    //protected void Page_SaveStateComplete(object sender, EventArgs e)
    //{
    //    Warning[] warnings;
    //    string[] streamids;
    //    string mimeType;
    //    string encoding;
    //    string fileNameExtension;
    //    byte[] bytes = ReportViewer1.LocalReport.Render("PDF", null, out mimeType, out encoding, out fileNameExtension, out streamids, out warnings);
    //    Response.Buffer = true;
    //    Response.Clear();
    //    Response.ContentType = mimeType;
    //    Response.AddHeader("content-disposition", "inline; filename=Nume" + fileNameExtension);
    //    Response.BinaryWrite(bytes);
    //    Response.Flush();
    //    Response.End();
    //    Response.Close();
    //}

}