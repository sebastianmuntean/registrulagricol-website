﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="printListaAdeverinte.aspx.cs" Inherits="printListaAdeverinte" Culture="ro-RO"
    UICulture="ro-RO" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt"
        Height="400px" Width="400px">
        <LocalReport ReportPath="rapoarte\raportListaAdeverinte.rdlc">
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="dsRapoarte_dtUnitati" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource2" Name="dsRapoarte_dtListaAdeverinte" />
            </DataSources>
        </LocalReport>
    </rsweb:ReportViewer>
    <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetData" TypeName="dsRapoarteTableAdapters.dtListaAdeverinteTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="%" Name="volum" QueryStringField="vol" Type="String" />
            <asp:QueryStringParameter DefaultValue="" Name="nrint1" QueryStringField="nr1" Type="Int32" />
            <asp:QueryStringParameter DefaultValue="" Name="nrint2" QueryStringField="nr2" Type="Int32" />
            <asp:QueryStringParameter DefaultValue="%" Name="strainas" QueryStringField="strainas"
                Type="String" />
            <asp:QueryStringParameter DefaultValue="%" Name="strada" QueryStringField="str" Type="String" />
            <asp:SessionParameter Name="an" SessionField="SESan" Type="Int32" />
            <asp:QueryStringParameter DefaultValue="%" Name="unitateid" QueryStringField="unit"
                Type="String" />
            <asp:QueryStringParameter DefaultValue="%" Name="localitate" QueryStringField="loc"
                Type="String" />
            <asp:QueryStringParameter DefaultValue="" Name="codSex" QueryStringField="sex" Type="String" />
            <asp:QueryStringParameter Name="varsta1" QueryStringField="varsta1" Type="Decimal" />
            <asp:QueryStringParameter Name="varsta2" QueryStringField="varsta2" Type="Decimal" />
            <asp:QueryStringParameter DefaultValue="" Name="judetId" QueryStringField="judet"
                Type="String" />
            <asp:QueryStringParameter DefaultValue="" Name="nume" QueryStringField="nume" Type="String" />
            <asp:QueryStringParameter Name="decedat" QueryStringField="decedat" Type="String" />
            <asp:QueryStringParameter Name="tipSablon" QueryStringField="tipSablon" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetData" TypeName="dsRapoarteTableAdapters.dtUnitatiTableAdapter">
        <SelectParameters>
            <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
