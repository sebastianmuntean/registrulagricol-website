﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class reparaCapitoleCentralizate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["SESutilizatorId"].ToString() != "52")
            Button1.Visible = false;
    }
    protected void btAdaugaUnitatiNoi_Click(object sender, EventArgs e)
    {
        // cautam unitati noi care nu au inregistrari
        List<string> vListaUnitati = ManipuleazaBD.fRezultaListaStringuri("SELECT * FROM unitati ORDER BY unitateId", "unitateId", Convert.ToInt16(Session["SESan"]));
        List<string> vUnitati = new List<string>();
        foreach (string vUnitate in vListaUnitati)
        {
            string vExista = ManipuleazaBD.fRezultaUnString("SELECT coalesce( count(*), '0') as exista FROM capitoleCentralizate WHERE unitateId = '" + vUnitate + "'", "exista", Convert.ToInt16(Session["SESan"]));
            if (Convert.ToInt16(vExista) == 0)
                vUnitati.Add(vUnitate);

        }
        //return;
        // luam lista de ani din ciclu
        //iclul curent 
        string vListaAniDinCiclu = ManipuleazaBD.fRezultaUnString("SELECT * FROM cicluri WHERE cicluAni like '%" + Session["SESan"] + "%'", "cicluAni", Convert.ToInt16(Session["SESan"]));
        char[] vCar = new char[] { '#' };
        string[] vListaAni = vListaAniDinCiclu.Split(vCar, StringSplitOptions.RemoveEmptyEntries);
        string vInsert = "";
        string vAnul = "2012";
        foreach (string vAn in vListaAni)
        {
            if (vAn == "2010")
                vAnul = "2012";
            else
                vAnul = vAn;
            // din sabloaneCapitole luam fiecare rand si introducem in centralizatoare
            List<string> vCampuri = new List<string> { "capitol", "codRand" };
            List<List<string>> vListaSabloane = ManipuleazaBD.fRezultaListaStringuri("SELECT * from sabloaneCapitole WHERE an = " + vAnul + " ORDER BY capitol, codRand", vCampuri, Convert.ToInt16(Session["SESan"]));
            foreach (string vUnitateId in vUnitati)
            {
                for (int i = 0; i <= 3; i++)
                {
                    if (vListaSabloane.Count > 0)
                    {
                        foreach (List<string> vSablon in vListaSabloane)
                        {
                            vInsert += "INSERT INTO capitoleCentralizate (unitateId, gospodarieId, an, codCapitol, codRand, col1, col2, col3, col4, col5, col6, col7, col8, gospodarieTip) VALUES ('" + vUnitateId + "', '0', '" + vAn + "','" + vSablon[0] + "', '" + vSablon[1] + "', '0', '0', '0', '0', '0', '0', '0', '0', '" + i.ToString() + "'); ";
                        }
                    }
                    ManipuleazaBD.fManipuleazaBD(vInsert, Convert.ToInt16(Session["SESan"]));
                    vInsert = ""; //return;
                }
            }
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        return;
        ManipuleazaBD.fManipuleazaBD("TRUNCATE TABLE capitoleCentralizate", Convert.ToInt16(Session["SESan"]));
        // luam lista de unitati
        List<string> vListaUnitati = ManipuleazaBD.fRezultaListaStringuri("SELECT * FROM unitati ORDER BY unitateId", "unitateId", Convert.ToInt16(Session["SESan"]));
        // luam lista de ani din ciclu
        //iclul curent 
        string vListaAniDinCiclu = ManipuleazaBD.fRezultaUnString("SELECT * FROM cicluri WHERE cicluAni like '%" + Session["SESan"] + "%'", "cicluAni", Convert.ToInt16(Session["SESan"]));
        char[] vCar = new char[] { '#' };
        string[] vListaAni = vListaAniDinCiclu.Split(vCar, StringSplitOptions.RemoveEmptyEntries);
        string vInsert = ""; 
        string vAnul = "2012";
        foreach (string vAn in vListaAni)
        {
            if (vAn == "2010")
                vAnul = "2012";
            else
                vAnul = vAn;
            // din sabloaneCapitole luam fiecare rand si introducem in centralizatoare
            List<string> vCampuri = new List<string> { "capitol", "codRand" };
            List<List<string>> vListaSabloane = ManipuleazaBD.fRezultaListaStringuri("SELECT * from sabloaneCapitole WHERE an = " + vAnul + " ORDER BY capitol, codRand", vCampuri, Convert.ToInt16(Session["SESan"]));
            foreach (string vUnitateId in vListaUnitati)
            {
                for (int i = 0; i <= 3; i++)
                {
                    if (vListaSabloane.Count > 0)
                    {
                        foreach (List<string> vSablon in vListaSabloane)
                        {
                            vInsert += "INSERT INTO capitoleCentralizate (unitateId, gospodarieId, an, codCapitol, codRand, col1, col2, col3, col4, col5, col6, col7, col8, gospodarieTip) VALUES ('" + vUnitateId + "', '0', '" + vAn + "','" + vSablon[0] + "', '" + vSablon[1] + "', '0', '0', '0', '0', '0', '0', '0', '0', '" + i.ToString() + "'); ";
                        }
                    }
                    ManipuleazaBD.fManipuleazaBD(vInsert, Convert.ToInt16(Session["SESan"]));
                    vInsert = ""; //return;
                }
            }
        }
    }
    protected void UmpleCentralizate(string pCapitol, string pUnitate, string pAn)
    { 
        // citim din capitol pt unitatea respectiva
        // gospodariitip : localnic fizic 0+0 - 0 ; localnic juridic  - 0+1 1; strainas fizic 1+0 -2; strainas juridic 1+1 - 3
        List<string> vCampuri = new List<string> { "codRand", "sumcol1", "sumcol2", "sumcol3", "sumcol4", "sumcol5", "sumcol6", "sumcol7", "sumcol8" };
        string vClauzaTip = "";
        string vGospodarieTip = "0";
        List<List<string>> vListaValori = new List<List<string>> { };
        for (int i = 0; i <= 3; i++ )
        {
            
            // stabilim tipul de gospodarie si clauza dupa care se face cautarea
            switch (i)
            {
                case 0: // localnic fizic 0+0 0 
                    vClauzaTip = " AND (gospodarii.strainas = 0) AND (gospodarii.persJuridica = 0) ";
                    vGospodarieTip = "0";
                    break;
                case 1: //localnic juridic  - 0+1 1;
                    vClauzaTip = " AND (gospodarii.strainas = 0) AND (gospodarii.persJuridica = 1) ";
                    vGospodarieTip = "1";
                    break;
                case 2: //strainas fizic 1+0 -2;
                    vClauzaTip = " AND (gospodarii.strainas = 1) AND (gospodarii.persJuridica = 0) ";
                    vGospodarieTip = "2";
                    break;
                case 3://strainas juridic 1+1 - 3
                    vClauzaTip = " AND (gospodarii.strainas = 1) AND (gospodarii.persJuridica = 1) ";
                    vGospodarieTip = "3";
                    break;
            
            }
            // scoatem valorile pt fiecare tip de gospodarie in parte, din capitole
            string vMesajExceptie = "";
            vListaValori = ManipuleazaBD.fRezultaListaStringuri("SELECT codRand, SUM(COALESCE (col1, 0)) AS sumcol1, SUM(COALESCE (col2, 0)) AS sumcol2, SUM(COALESCE (col3, 0)) AS sumcol3, SUM(COALESCE (col4, 0)) AS sumcol4, SUM(COALESCE (col5, 0)) AS sumcol5, SUM(COALESCE (col6, 0)) AS sumcol6, SUM(COALESCE (col7, 0)) AS sumcol7, SUM(COALESCE (col8, 0)) AS sumcol8  FROM capitole INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId WHERE capitole.unitateId =" + pUnitate + " AND codCapitol ='" + pCapitol + "' AND capitole.an=" + pAn + vClauzaTip + " GROUP BY codRand ORDER BY codRand", vCampuri, out vMesajExceptie, Convert.ToInt16(Session["SESan"]));
            if (vListaValori.Count > 0)
            {
                // scriem valorile  pt fiecare tip de gospodarie in parte, din capitoleCentralizate
                string vInsert = "";
                foreach (List<string> vValori in vListaValori)
                {
                    vInsert += "UPDATE capitoleCentralizate SET col1 ='" + vValori[1].Replace(',', '.') + "', col2 ='" + vValori[2].Replace(',', '.') + "', col3 ='" + vValori[3].Replace(',', '.') + "', col4 ='" + vValori[4].Replace(',', '.') + "', col5 ='" + vValori[5].Replace(',', '.') + "', col6 ='" + vValori[6].Replace(',', '.') + "', col7 ='" + vValori[7].Replace(',', '.') + "', col8 ='" + vValori[8].Replace(',', '.') + "' WHERE codRand='" + vValori[0] + "' AND unitateId =" + pUnitate + " AND codCapitol ='" + pCapitol + "' AND an=" + pAn + " AND gospodarieTip = '" + vGospodarieTip + "'; ";
                }

                //ManipuleazaBD.fManipuleazaBD(vInsert);
                int vCateRanduri = -1;
                ManipuleazaBD.fManipuleazaBD(vInsert, Convert.ToInt16(Session["SESan"]));
                ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "Centralizeaza", "unit:" + pUnitate + "; an:" + pAn + "; cap:" + pCapitol + "; tipgos:" + vGospodarieTip + "; randuri:" + vCateRanduri.ToString(), "SUCCES", Convert.ToInt64(Session["SESgospodarieId"]), 1);
                lblRezultate.Text += "<br /><span style='font-size:8px;'>unitatea:" + pUnitate + "; an:" + pAn + "; cap:" + pCapitol + "; tipgos:" + vGospodarieTip + " ! SUCCES !" + vMesajExceptie + "</span>";
            }
            else
            {
                ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "Centralizeaza", "unit:" + pUnitate + "; an:" + pAn + "; cap:" + pCapitol + "; tipgos:" + vGospodarieTip, "nu sunt gospodarii de acest tip; " + vMesajExceptie, Convert.ToInt64(Session["SESgospodarieId"]), 1);
                if (vMesajExceptie != "reusit")
                {
                    lblRezultate.Text += "<br /><span style='color:red;'>unitatea:" + pUnitate + "; an:" + pAn + "; cap:" + pCapitol + "; tipgos:" + vGospodarieTip + " ----- nu sunt gospodarii de acest tip;" + vMesajExceptie + "</span>";
                }
                else
                {
                    lblRezultate.Text += "<br /><span style='color:Green;'>unitatea:" + pUnitate + "; an:" + pAn + "; cap:" + pCapitol + "; tipgos:" + vGospodarieTip + " ----- nu sunt gospodarii de acest tip;" + vMesajExceptie + "</span>";
                }
            }
        }
     }
    protected void UmpleCentralizate(string pCapitol, string pUnitate, string pAn, string pCapitoleCentralizate)
    {
        // citim din capitol pt unitatea respectiva
        // gospodariitip : localnic fizic 0+0 - 0 ; localnic juridic  - 0+1 1; strainas fizic 1+0 -2; strainas juridic 1+1 - 3
        List<string> vCampuri = new List<string> { "codRand", "sumcol1", "sumcol2", "sumcol3", "sumcol4", "sumcol5", "sumcol6", "sumcol7", "sumcol8" };
        string vClauzaTip = "";
        string vGospodarieTip = "0";
        List<List<string>> vListaValori = new List<List<string>> { };
        for (int i = 0; i <= 3; i++)
        {

            // stabilim tipul de gospodarie si clauza dupa care se face cautarea
            switch (i)
            {
                case 0: // localnic fizic 0+0 0 
                    vClauzaTip = " AND (gospodarii.strainas = 0) AND (gospodarii.persJuridica = 0) ";
                    vGospodarieTip = "0";
                    break;
                case 1: //localnic juridic  - 0+1 1;
                    vClauzaTip = " AND (gospodarii.strainas = 0) AND (gospodarii.persJuridica = 1) ";
                    vGospodarieTip = "1";
                    break;
                case 2: //strainas fizic 1+0 -2;
                    vClauzaTip = " AND (gospodarii.strainas = 1) AND (gospodarii.persJuridica = 0) ";
                    vGospodarieTip = "2";
                    break;
                case 3://strainas juridic 1+1 - 3
                    vClauzaTip = " AND (gospodarii.strainas = 1) AND (gospodarii.persJuridica = 1) ";
                    vGospodarieTip = "3";
                    break;

            }
            // scoatem valorile pt fiecare tip de gospodarie in parte, din capitole
            string vMesajExceptie = "";
            vListaValori = ManipuleazaBD.fRezultaListaStringuri("SELECT codRand, sumcol1, sumcol2, sumcol3, sumcol4,sumcol5, sumcol6, sumcol7, sumcol8  FROM " + pCapitoleCentralizate + " ORDER BY codRand", vCampuri, out vMesajExceptie, Convert.ToInt16(Session["SESan"]));
            if (vListaValori.Count > 0)
            {
                // scriem valorile  pt fiecare tip de gospodarie in parte, din capitoleCentralizate
                string vInsert = "";
                foreach (List<string> vValori in vListaValori)
                {
                    vInsert += "UPDATE capitoleCentralizate SET col1 ='" + vValori[1].Replace(',', '.') + "', col2 ='" + vValori[2].Replace(',', '.') + "', col3 ='" + vValori[3].Replace(',', '.') + "', col4 ='" + vValori[4].Replace(',', '.') + "', col5 ='" + vValori[5].Replace(',', '.') + "', col6 ='" + vValori[6].Replace(',', '.') + "', col7 ='" + vValori[7].Replace(',', '.') + "', col8 ='" + vValori[8].Replace(',', '.') + "' WHERE codRand='" + vValori[0] + "' AND unitateId =" + pUnitate + " AND codCapitol ='" + pCapitol + "' AND an=" + pAn + " AND gospodarieTip = '" + vGospodarieTip + "'; ";
                }

                //ManipuleazaBD.fManipuleazaBD(vInsert);
                int vCateRanduri = -1;
                ManipuleazaBD.fManipuleazaBD(vInsert, Convert.ToInt16(Session["SESan"]));
                ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "Centralizeaza", "unit:" + pUnitate + "; an:" + pAn + "; cap:" + pCapitol + "; tipgos:" + vGospodarieTip + "; randuri:" + vCateRanduri.ToString(), "SUCCES", Convert.ToInt64(Session["SESgospodarieId"]), 1);
                lblRezultate.Text += "<br /><span style='font-size:8px;'>unitatea:" + pUnitate + "; an:" + pAn + "; cap:" + pCapitol + "; tipgos:" + vGospodarieTip + " ! SUCCES !" + vMesajExceptie + "</span>";
            }
            else
            {
                ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "Centralizeaza", "unit:" + pUnitate + "; an:" + pAn + "; cap:" + pCapitol + "; tipgos:" + vGospodarieTip, "nu sunt gospodarii de acest tip; " + vMesajExceptie, Convert.ToInt64(Session["SESgospodarieId"]), 1);
                if (vMesajExceptie != "reusit")
                {
                    lblRezultate.Text += "<br /><span style='color:red;'>unitatea:" + pUnitate + "; an:" + pAn + "; cap:" + pCapitol + "; tipgos:" + vGospodarieTip + " ----- nu sunt gospodarii de acest tip;" + vMesajExceptie + "</span>";
                }
                else
                {
                    lblRezultate.Text += "<br /><span style='color:Green;'>unitatea:" + pUnitate + "; an:" + pAn + "; cap:" + pCapitol + "; tipgos:" + vGospodarieTip + " ----- nu sunt gospodarii de acest tip;" + vMesajExceptie + "</span>";
                }
            }
        }
    }

    protected void UmpleCentralizatePasCuPas(string pCapitol, string pUnitate, string pAn)
    {
        // citim din capitol pt unitatea respectiva
        // gospodariitip : localnic fizic 0+0 - 0 ; localnic juridic  - 0+1 1; strainas fizic 1+0 -2; strainas juridic 1+1 - 3
        List<string> vCampuri = new List<string> { "codRand", "sumcol1", "sumcol2", "sumcol3", "sumcol4", "sumcol5", "sumcol6", "sumcol7", "sumcol8" };
        string vClauzaTip = "";
        string vGospodarieTip = "0";
        List<List<string>> vListaValori = new List<List<string>> { };
        for (int i = 0; i <= 3; i++)
        {

            // stabilim tipul de gospodarie si clauza dupa care se face cautarea
            switch (i)
            {
                case 0: // localnic fizic 0+0 0 
                    vClauzaTip = " AND (gospodarii.strainas = 0) AND (gospodarii.persJuridica = 0) ";
                    vGospodarieTip = "0";
                    break;
                case 1: //localnic juridic  - 0+1 1;
                    vClauzaTip = " AND (gospodarii.strainas = 0) AND (gospodarii.persJuridica = 1) ";
                    vGospodarieTip = "1";
                    break;
                case 2: //strainas fizic 1+0 -2;
                    vClauzaTip = " AND (gospodarii.strainas = 1) AND (gospodarii.persJuridica = 0) ";
                    vGospodarieTip = "2";
                    break;
                case 3://strainas juridic 1+1 - 3
                    vClauzaTip = " AND (gospodarii.strainas = 1) AND (gospodarii.persJuridica = 1) ";
                    vGospodarieTip = "3";
                    break;

            }
            // scoatem valorile pt fiecare tip de gospodarie in parte, din capitole
            string vMesajExceptie = "";
            vListaValori = ManipuleazaBD.fRezultaListaStringuri("SELECT codRand, SUM(COALESCE (col1, 0)) AS sumcol1, SUM(COALESCE (col2, 0)) AS sumcol2, SUM(COALESCE (col3, 0)) AS sumcol3, SUM(COALESCE (col4, 0)) AS sumcol4, SUM(COALESCE (col5, 0)) AS sumcol5, SUM(COALESCE (col6, 0)) AS sumcol6, SUM(COALESCE (col7, 0)) AS sumcol7, SUM(COALESCE (col8, 0)) AS sumcol8  FROM capitole INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId WHERE capitole.unitateId =" + pUnitate + " AND codCapitol ='" + pCapitol + "' AND capitole.an=" + pAn + vClauzaTip + " GROUP BY codRand ORDER BY codRand", vCampuri, out vMesajExceptie, Convert.ToInt16(Session["SESan"]));
            if (vListaValori.Count > 0)
            {
                // scriem valorile  pt fiecare tip de gospodarie in parte, din capitoleCentralizate
                string vInsert = "";
                foreach (List<string> vValori in vListaValori)
                {
                    vInsert += "UPDATE capitoleCentralizate SET col1 ='" + vValori[1].Replace(',', '.') + "', col2 ='" + vValori[2].Replace(',', '.') + "', col3 ='" + vValori[3].Replace(',', '.') + "', col4 ='" + vValori[4].Replace(',', '.') + "', col5 ='" + vValori[5].Replace(',', '.') + "', col6 ='" + vValori[6].Replace(',', '.') + "', col7 ='" + vValori[7].Replace(',', '.') + "', col8 ='" + vValori[8].Replace(',', '.') + "' WHERE codRand='" + vValori[0] + "' AND unitateId =" + pUnitate + " AND codCapitol ='" + pCapitol + "' AND an=" + pAn + " AND gospodarieTip = '" + vGospodarieTip + "'; ";
                }

                //ManipuleazaBD.fManipuleazaBD(vInsert);
                int vCateRanduri = -1;
                ManipuleazaBD.fManipuleazaBD(vInsert, Convert.ToInt16(Session["SESan"]));
                ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "Centralizeaza", "unit:" + pUnitate + "; an:" + pAn + "; cap:" + pCapitol + "; tipgos:" + vGospodarieTip + "; randuri:" + vCateRanduri.ToString(), "SUCCES", Convert.ToInt64(Session["SESgospodarieId"]), 1);
                lblRezultate.Text += "<br /><span style='font-size:8px;'>unitatea:" + pUnitate + "; an:" + pAn + "; cap:" + pCapitol + "; tipgos:" + vGospodarieTip + " ! SUCCES !" + vMesajExceptie + "</span>";
            }
            else
            {
                ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "Centralizeaza", "unit:" + pUnitate + "; an:" + pAn + "; cap:" + pCapitol + "; tipgos:" + vGospodarieTip, "nu sunt gospodarii de acest tip; " + vMesajExceptie, Convert.ToInt64(Session["SESgospodarieId"]), 1);
                if (vMesajExceptie != "reusit")
                {
                    lblRezultate.Text += "<br /><span style='color:red;'>unitatea:" + pUnitate + "; an:" + pAn + "; cap:" + pCapitol + "; tipgos:" + vGospodarieTip + " ----- nu sunt gospodarii de acest tip;" + vMesajExceptie + "</span>";
                }
                else
                {
                    lblRezultate.Text += "<br /><span style='color:Green;'>unitatea:" + pUnitate + "; an:" + pAn + "; cap:" + pCapitol + "; tipgos:" + vGospodarieTip + " ----- nu sunt gospodarii de acest tip;" + vMesajExceptie + "</span>";
                }
            }
        }
    }
    protected void Button2x_Click(object sender, EventArgs e)
    {
        // luam lista de unitati
        List<string> vListaUnitati = ManipuleazaBD.fRezultaListaStringuri("SELECT * FROM unitati ORDER BY unitateId", "unitateId", Convert.ToInt16(Session["SESan"]));
        List<string> vListaCodCapitol = ManipuleazaBD.fRezultaListaStringuri("SELECT DISTINCT capitol FROM sabloaneCapitole WHERE (capitol NOT IN ('parcele', 'paduri')) ORDER BY capitol", "capitol", Convert.ToInt16(Session["SESan"]));
        for (int vAn = 2012; vAn <=2013; vAn++)
        {
            foreach (string vUnitate in vListaUnitati)
            {
                foreach (string vCapitol in vListaCodCapitol)
                {
                  // if (vUnitate=="1")  
                    UmpleCentralizate(vCapitol, vUnitate, vAn.ToString());        
                }
            }
        }
        
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        // luam lista de unitati
       // List<string> vListaUnitati = ManipuleazaBD.fRezultaListaStringuri("SELECT * FROM unitati ORDER BY unitateId ", "unitateId");
        List<string> vListaCodCapitol = ManipuleazaBD.fRezultaListaStringuri("SELECT DISTINCT capitol FROM sabloaneCapitole WHERE (capitol NOT IN ('parcele', 'paduri')) ORDER BY capitol", "capitol", Convert.ToInt16(Session["SESan"]));
      //  List<string> vListaUnitati = new List<string> {  };
        char[] vDelim = new char[] {'#' };
        string[] vListaUnitati = tbUnitate.Text.Split(vDelim);
        //vListaUnitati = tbUnitate.Text.Split(vDelim);
       // vListaCodCapitol = new List<string> { "7" };
        for (int vAn = 2012; vAn <= 2014; vAn++)
        {

            foreach (string vUnitate in vListaUnitati)
           {
                foreach (string vCapitol in vListaCodCapitol)
                {
                    // if (vUnitate=="1")  
                    UmpleCentralizate(vCapitol, vUnitate, vAn.ToString());
                }
           }
        }

    }
    protected void toate_Click(object sender, EventArgs e)
    {
        // luam lista de unitati
        List<string> vListaUnitati = ManipuleazaBD.fRezultaListaStringuri("SELECT * FROM unitati WHERE CONVERT(nvarchar,unitateId) LIKE '" + ddlUnitate.SelectedValue + "' ORDER BY unitateId ", "unitateId", Convert.ToInt16(Session["SESan"]));
        List<string> vListaCodCapitol = ManipuleazaBD.fRezultaListaStringuri("SELECT DISTINCT capitol FROM sabloaneCapitole WHERE (capitol NOT IN ('parcele', 'paduri')) ORDER BY capitol", "capitol", Convert.ToInt16(Session["SESan"]));
        if (Session["SESutilizatorId"].ToString() != "52") return;
        for (int vAn = 2012; vAn <= 2014; vAn++)
        {
            foreach (string vUnitate in vListaUnitati)
            {
                foreach (string vCapitol in vListaCodCapitol)
                {
                    // if (vUnitate=="1")  
                    UmpleCentralizate(vCapitol, vUnitate, vAn.ToString());
                }
            }
        }
    }
    protected void Button2tabele_Click(object sender, EventArgs e)
    {
        // luam lista de unitati
        UmpleCentralizate(tbCapitol.Text, tbUnitate.Text, tbAn.Text, tbTabela.Text);
    }
}
