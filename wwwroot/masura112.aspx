﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="masura112.aspx.cs" Inherits="masura112" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="<%$ Resources:Resursa, raMasura112Titlu%>" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="pnMasura112" runat="server" CssClass="panel_general" Visible="true">
        <!-- lista Membri -->
        <asp:Panel ID="pnListaMembri" runat="server" CssClass="panel_general" Visible="true">
            <asp:Panel ID="pnMembri" runat="server">
                <asp:GridView ID="gvMembri" AllowPaging="True" AllowSorting="True" CssClass="tabela"
                    runat="server" AutoGenerateColumns="False" DataKeyNames="capitolId" DataSourceID="SqlMembri"
                    OnDataBound="gvMembri_DataBound">
                    <Columns>
                        <asp:BoundField DataField="capitolId" HeaderText="Nr." InsertVisible="False" ReadOnly="True"
                            SortExpression="capitolId" Visible="false" />
                        <asp:BoundField DataField="codRand" HeaderText="rand" SortExpression="codRand" Visible="false" />
                        <asp:BoundField DataField="nume" HeaderText="nume şi prenume" SortExpression="nume" />
                        <asp:BoundField DataField="denumireRudenie" HeaderText="grad de rudenie" SortExpression="denumireRudenie" />
                        <asp:BoundField DataField="codSex" HeaderText="sex" SortExpression="codSex" />
                        <asp:BoundField DataField="mentiuni" HeaderText="menţiuni" SortExpression="mentiuni" />
                        <asp:BoundField DataField="dataNasterii" HeaderText="data naşterii" SortExpression="dataNasterii"
                            DataFormatString="{0:d}" HtmlEncode="False" />
                        <asp:BoundField DataField="cnp" HeaderText="CNP" SortExpression="cnp" />
                    </Columns>
                    <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                </asp:GridView>
                <asp:SqlDataSource ID="SqlMembri" runat="server" 
                    SelectCommand="SELECT [codSex], [codRand], [mentiuni], [dataNasterii], [denumireRudenie], [cnp], [nume], [capitolId] FROM [membri] WHERE ([gospodarieId] = @gospodarieId)">
                    <SelectParameters>
                        <asp:SessionParameter DefaultValue="0" Name="gospodarieId" SessionField="SESgospodarieId"
                            Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </asp:Panel>
        </asp:Panel>
        <asp:Panel ID="Panel1" runat="server" CssClass="adauga">
        <div>
            <asp:Label ID="lblMembriEligibili" runat="server" Text=""></asp:Label></div>
             <div><asp:Label ID="lblValoareTotalaUDE" runat="server" Text=""></asp:Label></div>

            <div class="citat">
            <span  class="citat">Din Ghidul Solicitantului pentru Măsura 112 - <a href="http://www.apdrp.ro/" target="_blank">www.apdrp.ro</a>:</span><br />
            <span class="citat">
            2.1 Cine poate beneficia de fonduri nerambursabile <br />Beneficiarii eligibili pentru sprijinul financiar nerambursabil acordat prin Măsura 112 sunt fermierii în vârstă de până la 40 de ani (neîmpliniţi la data depunerii Cererii de finanţare), persoane fizice sau juridice care practică în principal activităţi agricole şi a căror exploataţie agricolă:<br /> - are o dimensiune economică cuprinsă între 6 şi 40 UDE;<br /> - este situată pe teritoriul ţării;<br /> - este înregistrată în Registrul unic de identificare – APIA şi Registrul agricol.
            </span>
            </div>
        </asp:Panel>
        <asp:Panel ID="Panel2" runat="server">
            <asp:Label ID="lblTabel" runat="server" Text=""></asp:Label>
        </asp:Panel>
    </asp:Panel>
</asp:Content>
