﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="raportContracteArenda.aspx.cs" Inherits="raportContracteArenda" Culture="ro-RO" UICulture="ro-RO" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Rapoarte / Listă contracte arendă" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="pnGeneral" runat="server" CssClass="panel_general">
        <asp:Panel ID="pnFiltrareUnitati" runat="server" CssClass="cauta">
            <asp:Label ID="lblFJudet" runat="server" Text="Judeţ"></asp:Label>
            <asp:DropDownList ID="ddlFJudet" AutoPostBack="true" runat="server" OnPreRender="ddlFJudet_PreRender"
                OnSelectedIndexChanged="ddlFJudet_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:Label ID="lblUnitate" runat="server" Text="Unitatea:"></asp:Label>
            <asp:DropDownList ID="ddlUnitate" runat="server" OnInit="ddlUnitate_Init">
            </asp:DropDownList>
        </asp:Panel>
        <asp:Panel ID="pnFiltrare" runat="server" CssClass="cauta">
            <asp:Label ID="lblFiltrareDupa" runat="server" Text="Caută după:"></asp:Label>
            <asp:Label ID="lblVolum" runat="server" Text="vol."></asp:Label>
            <asp:TextBox ID="tbVolum" runat="server" AutoPostBack="False" Width="30px"></asp:TextBox>
            <asp:Label ID="lblNrPozitie" runat="server" Text="poz."></asp:Label>
            <asp:TextBox ID="tbNrPozitie" runat="server" AutoPostBack="False" Width="30px"></asp:TextBox>
            <asp:Label ID="lblDeLaNr" runat="server" Text="de la nr."></asp:Label>
            <asp:TextBox ID="tbDeLaNr" runat="server" AutoPostBack="False" Width="40px"></asp:TextBox>
            <asp:Label ID="lblLaNr" runat="server" Text="la nr.:"></asp:Label>
            <asp:TextBox ID="tbLaNr" runat="server" AutoPostBack="False" Width="40px"></asp:TextBox>
            <asp:Label ID="lblStrainas" runat="server" Text="strainaș"></asp:Label>
            <asp:DropDownList ID="ddlStrainas" runat="server" AutoPostBack="False">
                <asp:ListItem Value="%">toți</asp:ListItem>
                <asp:ListItem Value="1">da</asp:ListItem>
                <asp:ListItem Value="0">nu</asp:ListItem>
            </asp:DropDownList>
            <asp:Label ID="lblStrada" runat="server" Text="str."></asp:Label>
            <asp:TextBox ID="tbStrada" runat="server" AutoPostBack="False" Width="100px"></asp:TextBox>
            <asp:Label ID="lblLocalitate" runat="server" Text="loc."></asp:Label>
            <asp:TextBox ID="tbLocalitate" runat="server" AutoPostBack="False" Width="100px"></asp:TextBox>
        </asp:Panel>
        <asp:Panel ID="Panel7" CssClass="" runat="server" Visible="true">
            <asp:Panel ID="Panel6" runat="server" CssClass="cauta">
                <asp:Label ID="lblCInceput" runat="server" Text="Derulate în perioada" />
                <asp:TextBox ID="tbCInceput" runat="server" Width="80" Format="dd.MM.yyyy" onchange="this.value=ValidareData(this.value, 'dd.MM.yyyy')"></asp:TextBox>
                <asp:CalendarExtender ID="caltbCInceput" runat="server" Enabled="True" Format="dd.MM.yyyy"
                    PopupButtonID="tbCInceput" TargetControlID="tbCInceput">
                </asp:CalendarExtender>
                <asp:Label ID="lblCFinal" runat="server" Text="-" />
                <asp:TextBox ID="tbCFinal" runat="server" Width="80" Format="dd.MM.yyyy" onchange="this.value=ValidareData(this.value, 'dd.MM.yyyy')"></asp:TextBox>
                <asp:CalendarExtender ID="caltbCFinal" runat="server" Enabled="True" Format="dd.MM.yyyy"
                    PopupButtonID="tbCFinal" TargetControlID="tbCFinal">
                </asp:CalendarExtender>

                <asp:Label ID="Label3" runat="server" Text="Tip contract"></asp:Label>
                <asp:DropDownList ID="ddlTipContract" runat="server">
                    <asp:ListItem Text="toate" Value="%"></asp:ListItem>
                    <asp:ListItem Text="a) în arendă" Value="3"></asp:ListItem>
                    <asp:ListItem Text="b) în parte" Value="4"></asp:ListItem>
                    <asp:ListItem Text="c) cu titlu gratuit" Value="5"></asp:ListItem>
                    <asp:ListItem Text="d) în concesiune" Value="6"></asp:ListItem>
                    <asp:ListItem Text="e) în asociere" Value="7"></asp:ListItem>
                    <asp:ListItem Text="f) sub alte forme" Value="8"></asp:ListItem>
                </asp:DropDownList>
                <asp:Label ID="Label1" runat="server" Text="Ordonare"></asp:Label>
                <asp:DropDownList ID="ddlOrdonare" runat="server">
                    <asp:ListItem Text="volum, număr poziție ale celui care dă" Value="1"></asp:ListItem>
                    <asp:ListItem Text="volum, număr poziție ale celui care primește" Value="2"></asp:ListItem>
                    <asp:ListItem Text="numelui celui care dă" Value="3"></asp:ListItem>
                    <asp:ListItem Text="numelui celui care primește" Value="4"></asp:ListItem>
                    <asp:ListItem Text="data semnării contractului" Value="5"></asp:ListItem>
                    <asp:ListItem Text="numărul contractului" Value="6"></asp:ListItem>
                    <asp:ListItem Text="tipul contractului" Value="7"></asp:ListItem>
                </asp:DropDownList>

            </asp:Panel>
        </asp:Panel>
        <asp:Panel ID="pnButoane" runat="server" CssClass="butoane">
            <asp:Button ID="btTiparire" runat="server" Text="tipărește" CssClass="buton" OnClick="btTiparire_Click" />
            <asp:Button ID="btTiparireXls" runat="server" Text="salveaza in Excel" CssClass="buton" OnClick="btTiparireXls_Click" />
        </asp:Panel>
    </asp:Panel>
</asp:Content>
