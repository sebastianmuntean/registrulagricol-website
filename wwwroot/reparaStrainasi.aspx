﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="reparaStrainasi.aspx.cs" Inherits="reparaStrainasi" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    repara strainasi
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="upLocalitati" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnAfiseazaMesaje" runat="server">
                <asp:ValidationSummary ID="valCapitole" runat="server" DisplayMode="SingleParagraph"
                    Visible="true" CssClass="validator" ForeColor="" />
            </asp:Panel>
            <asp:Panel ID="Panel5" CssClass="ascunsa" runat="server">
                <!-- //////////// VALIDATOARE  \\\\\\\\\\\\\\\\\\ -->
                <asp:CustomValidator ID="valCapitol" runat="server" ErrorMessage=""></asp:CustomValidator>
            </asp:Panel>
            <asp:Panel ID="pnListaLocalitati" runat="server" CssClass="panel_general" Visible="true">
                <asp:Panel ID="pnCautare" runat="server" CssClass="cauta">
                    <asp:Label ID="lblUnitati" runat="server" Text="Unitatea"></asp:Label>
                    <asp:DropDownList ID="ddlUnitati" runat="server" DataSourceID="SqlUnitati" DataTextField="unitateDenumire"
                        DataValueField="unitateId" AutoPostBack="true" OnSelectedIndexChanged="ddlUnitati_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlUnitati" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString %>"
                        SelectCommand="SELECT [unitateId], [unitateDenumire] FROM [unitati] ORDER BY [unitateDenumire]">
                    </asp:SqlDataSource>
                </asp:Panel>
                <asp:Panel ID="pnDenumireLocalitate" CssClass="panel_general" runat="server">
                    <p>
                        <asp:Label ID="lblJudet" runat="server" Text="judet"></asp:Label>
                        <asp:TextBox ID="tbJudet" Text="SIBIU" runat="server"></asp:TextBox>
                        <asp:Label ID="lblLocalitate" runat="server" Text="localitate"></asp:Label>
                        <asp:DropDownList ID="ddlLocalitate" runat="server" DataSourceID="SqlLocalitati"
                            DataTextField="localitateDenumire" DataValueField="localitateDenumire">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="SqlLocalitati" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString %>"
                            SelectCommand="SELECT [localitateDenumire] FROM [localitatiComuna] WHERE ([unitateId] = @unitateId)">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="ddlUnitati" Name="unitateId" PropertyName="SelectedValue"
                                    Type="Int32" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </p>
                </asp:Panel>
                <asp:Panel ID="pnButoane" CssClass="butoane" runat="server">
                    <asp:Button CssClass="buton" ToolTip="Daca este PF si nu este strainas tip=1, daca este PF strainas tip=2, daca este PJ si nu este strainas tip=3 iar daca este PJ strainas tip=4"
                        ID="btReparaTipGospodarie" runat="server" Text="repara tip gospodarie" OnClick="btReparaTipGospodarie_Click" />
                    <asp:Button ID="btReparaAdresaStrainasi" CssClass="buton" ToolTip="daca are bifa de strainas si localitatea de la strinas nu este completata, iau adresa de la gospodarie si o pun la adresa strainas. La localitatea de la gospodarie pun localitatea selectata mai sus"
                        runat="server" Text="repara adresa strainas" OnClick="btReparaAdresaStrainasi_Click" />
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
