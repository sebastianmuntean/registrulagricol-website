﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class raportStatisticUtilizatori : System.Web.UI.Page
{
    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion
    protected void Page_Init(object sender, EventArgs e)
    {
        tbDataDin.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToShortDateString();
        tbDataLa.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).AddDays(-1).ToShortDateString();
    }
    protected void Page_Load(object sender, EventArgs e)
    {

        DBConnections connection = new DBConnections(Convert.ToInt16(Session["SESan"]));

        SqlUtilizatori.ConnectionString = connection.Create();

        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "raportStatisticUtilizatori", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
        }
    }
    protected void ddlUnitate_Init(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select top(1) tipUtilizatorId from utilizatori where utilizatorId='" + Session["SESutilizatorId"].ToString() + "'";
        int vTipUtilizator = Convert.ToInt32(vCmd.ExecuteScalar());
        if (vTipUtilizator == 2 || vTipUtilizator == 3 || vTipUtilizator == 5)
        {
            vCmd.CommandText = "SELECT TOP (1) unitati.judetId FROM utilizatori INNER JOIN unitati ON utilizatori.unitateId = unitati.unitateId WHERE (utilizatori.utilizatorId = '" + Session["SESutilizatorId"].ToString() + "')";
            string vJudetId = vCmd.ExecuteScalar().ToString();
            ddlFJudet.Items.Clear();
            ddlFJudet.Items.Add(new ListItem("", vJudetId));
            vCmd.CommandText = "select unitateDenumire,unitateId from unitati where judetId='" + vJudetId + "' order by unitateDenumire";
            ddlUnitate.Items.Clear();
            ListItem vItem = new ListItem("-toate-", "%");
            ddlUnitate.Items.Add(vItem);
            SqlDataReader vTabel = vCmd.ExecuteReader();
            while (vTabel.Read())
            {
                vItem = new ListItem(vTabel["unitateDenumire"].ToString(), vTabel["unitateId"].ToString());
                ddlUnitate.Items.Add(vItem);
            }
            vTabel.Close();
            if (ViewState["unitateId"] != null)
                ddlUnitate.SelectedValue = ViewState["unitateId"].ToString();
            //((MasterPage)this.Page.Master).SchimbaGospodaria();
        }
        ManipuleazaBD.InchideConexiune(vCon);
    }
    protected void ddlFJudet_PreRender(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select judetDenumire,judetId from judete order by judetDenumire";
        ddlFJudet.Items.Clear();
        ListItem vItem = new ListItem("-toate-", "%");
        ddlFJudet.Items.Add(vItem);
        SqlDataReader vTabel = vCmd.ExecuteReader();
        while (vTabel.Read())
        {
            vItem = new ListItem(vTabel["judetDenumire"].ToString(), vTabel["judetId"].ToString());
            ddlFJudet.Items.Add(vItem);
        }
        vTabel.Close();
        vCmd.CommandText = "select top(1) coalesce(judetId,0) from unitati where convert(nvarchar,unitateId)='" + Session["SESunitateId"].ToString() + "'";
        if (ViewState["judetId"] == null)
            try { ViewState["judetId"] = vCmd.ExecuteScalar().ToString(); }
            catch { ViewState["judetId"] = "%"; }
        vCmd.CommandText = "select unitateDenumire,unitateId from unitati where convert(nvarchar,judetId) like '" + ViewState["judetId"].ToString() + "' order by unitateDenumire";
        ddlUnitate.Items.Clear();
        vItem = new ListItem("-toate-", "%");
        ddlUnitate.Items.Add(vItem);
        vTabel = vCmd.ExecuteReader();
        while (vTabel.Read())
        {
            vItem = new ListItem(vTabel["unitateDenumire"].ToString(), vTabel["unitateId"].ToString());
            ddlUnitate.Items.Add(vItem);
        }
        vTabel.Close();
        ManipuleazaBD.InchideConexiune(vCon);
        try
        {
            ddlUnitate.SelectedValue = ViewState["unitateId"].ToString();
        }
        catch
        {
            ddlUnitate.SelectedValue = "%";
        }
        ddlFJudet.SelectedValue = ViewState["judetId"].ToString();
    }
    protected void ddlFJudet_SelectedIndexChanged(object sender, EventArgs e)
    {
        ViewState["judetId"] = ddlFJudet.SelectedValue;
    }
    protected void ddlUnitate_SelectedIndexChanged(object sender, EventArgs e)
    {
        /*HttpCookie vCook = Request.Cookies["COOKTNT"];
        string v1 = "";
        string v2 = "";
        string v3 = "";

        if (vCook != null)
        {
            vCook = CriptareCookie.DecodeCookie(vCook);
            v1 = vCook["COOKan"];
            v2 = vCook["COOKutilizatorId"];
            v3 = vCook["COOKunitateId"];
            vCook = CriptareCookie.EncodeCookie(vCook);
            Response.Cookies.Add(vCook);
            Response.Cookies.Remove("COOKTNT");
        }
        HttpCookie vCook1 = new HttpCookie("COOKTNT");
        vCook1["COOKan"] = v1;
        vCook1["COOKutilizatorId"] = v2;
        vCook1["COOKunitateId"] = v3;
        vCook1 = CriptareCookie.EncodeCookie(vCook1);
        Response.Cookies.Add(vCook1);*/
        if (ddlUnitate.SelectedValue == "%")
            ViewState["unitateId"] = 0;
        else ViewState["unitateId"] = ddlUnitate.SelectedValue;
        //   Session["SESgospodarieId"] = "NULA";
        Session["SESgospodarieId"] = null;

        //((MasterPage)this.Page.Master).SchimbaGospodaria();
    }
    protected void lbSelectatiTot_Click(object sender, EventArgs e)
    {
        if (ViewState["selectat"] == null)
            ViewState["selectat"] = "0";
        for (int a = 0; a < cblUtilizatori.Items.Count; a++)
            if (ViewState["selectat"].ToString() == "0")
                cblUtilizatori.Items[a].Selected = true;
            else if (ViewState["selectat"].ToString() == "1")
                cblUtilizatori.Items[a].Selected = false;
        if (ViewState["selectat"].ToString() == "0")
            ViewState["selectat"] = "1";
        else if (ViewState["selectat"].ToString() == "1")
            ViewState["selectat"] = "0";
    }
    protected void btTiparire_Click(object sender, EventArgs e)
    {
        // verificare sesiuni
        VerificareSesiuni vVerificareSesiuni = new VerificareSesiuni();
        vVerificareSesiuni.VerificaSesiuniCookie();
        // calculez raportul
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        // iau denumirea unitatii selectate, a judetului si a utilizatorilor
        string vUnitatea = "Toate";
        if (ddlUnitate.SelectedValue != "%")
        {
            vCmd.CommandText = "select top(1) unitateDenumire from unitati where unitateId = '" + ddlUnitate.SelectedValue + "'";
            vUnitatea = vCmd.ExecuteScalar().ToString();
        }
        string vJudet = "Toate";
        if (ddlFJudet.SelectedValue != "%")
        {
            vCmd.CommandText = "select top(1) judetDenumire from judete where judetId = '" + ddlFJudet.SelectedValue + "'";
            vJudet = vCmd.ExecuteScalar().ToString();
        }
        string vUtilizatoriIduri = "";
        foreach (ListItem a in cblUtilizatori.Items)
        {
            vUtilizatoriIduri += a.Value + ",";
        }
        if (vUtilizatoriIduri == "")
        {
            valCustom.IsValid = false;
            valCustom.ErrorMessage = "Alegeţi cel puţin un utilizator !";
            ManipuleazaBD.InchideConexiune(vCon);
            return;
        }
        else
        {
            vUtilizatoriIduri = vUtilizatoriIduri.Remove(vUtilizatoriIduri.Length - 1, 1);
        }
        vCmd.CommandText = "select utilizatorNume, utilizatorPrenume from utilizatori where utilizatorId in (" + vUtilizatoriIduri + ")";
        SqlDataReader vTabel = vCmd.ExecuteReader();
        string vUtilizatoriNume = "";
        while (vTabel.Read())
        {
            vUtilizatoriNume += vTabel["utilizatorNume"].ToString() + " " + vTabel["utilizatorPrenume"].ToString() + ";";
        }
        vTabel.Close();
        if (vUtilizatoriNume != "")
        {
            vUtilizatoriNume = vUtilizatoriNume.Remove(vUtilizatoriNume.Length - 1, 1);
        }
        // merg pe fiecare zi din luna si anul ales si calculez totalurile
        DateTime vDataStart = Convert.ToDateTime(tbDataDin.Text).Date;
        DateTime vDataFinal = Convert.ToDateTime(tbDataLa.Text).Date;
        // sterg datele din raport pentru utilizatorul curent
        vCmd.CommandText = "delete from rapStatisticUtilizatori where utilizatorId='" + Session["SESutilizatorId"].ToString() + "'";
        vCmd.ExecuteNonQuery();
        while (vDataStart <= vDataFinal)
        {
            if (vDataStart.Date == DateTime.Now.Date)
            { }
            // intrari in sistem
            vCmd.CommandText = "SELECT COUNT(*) AS Expr1 FROM logOperatii INNER JOIN utilizatori ON logOperatii.utilizatorId = utilizatori.utilizatorId INNER JOIN unitati ON utilizatori.unitateId = unitati.unitateId WHERE (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (CONVERT(nvarchar, unitati.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (logOperatii.utilizatorId IN (" + vUtilizatoriIduri + ")) AND (logOperatii.data = convert(datetime,'" + vDataStart.Date.ToShortDateString() + "',104)) AND (logOperatii.operatieId = '8')";
            int vLogin = Convert.ToInt32(vCmd.ExecuteScalar());
            // gospodarii adaugate
            vCmd.CommandText = "SELECT COUNT(*) AS Expr1 FROM logOperatii INNER JOIN utilizatori ON logOperatii.utilizatorId = utilizatori.utilizatorId INNER JOIN unitati ON utilizatori.unitateId = unitati.unitateId WHERE (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (CONVERT(nvarchar, unitati.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (logOperatii.utilizatorId IN (" + vUtilizatoriIduri + ")) AND (logOperatii.data = convert(datetime,'" + vDataStart.Date.ToShortDateString() + "',104)) AND (logOperatii.tabela like '%gospodarii%') AND (logOperatii.operatieId = '2')";
            int vGospodariiAdaugate = Convert.ToInt32(vCmd.ExecuteScalar());
            // capitole modificate
            vCmd.CommandText = "SELECT COUNT(*) AS Expr1 FROM logOperatii INNER JOIN utilizatori ON logOperatii.utilizatorId = utilizatori.utilizatorId INNER JOIN unitati ON utilizatori.unitateId = unitati.unitateId WHERE (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (CONVERT(nvarchar, unitati.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (logOperatii.utilizatorId IN (" + vUtilizatoriIduri + ")) AND (logOperatii.data = convert(datetime,'" + vDataStart.Date.ToShortDateString() + "',104)) AND (logOperatii.tabela = 'capitole') AND (logOperatii.operatieId in ('2', '3'))";
            int vCapitoleModificate = Convert.ToInt32(vCmd.ExecuteScalar());
            // erori semnalate
            vCmd.CommandText = "SELECT COUNT(*) AS Expr1 FROM logOperatii INNER JOIN utilizatori ON logOperatii.utilizatorId = utilizatori.utilizatorId INNER JOIN unitati ON utilizatori.unitateId = unitati.unitateId WHERE (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (CONVERT(nvarchar, unitati.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (logOperatii.utilizatorId IN (" + vUtilizatoriIduri + ")) AND (logOperatii.data = convert(datetime,'" + vDataStart.Date.ToShortDateString() + "',104)) AND (logOperatii.tabela = 'semnalareEroare') AND (logOperatii.operatieId = '2')";
            int vEroriSemnalate = Convert.ToInt32(vCmd.ExecuteScalar());
            // operatiuni de adaugare
            vCmd.CommandText = "SELECT COUNT(*) AS Expr1 FROM logOperatii INNER JOIN utilizatori ON logOperatii.utilizatorId = utilizatori.utilizatorId INNER JOIN unitati ON utilizatori.unitateId = unitati.unitateId WHERE (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (CONVERT(nvarchar, unitati.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (logOperatii.utilizatorId IN (" + vUtilizatoriIduri + ")) AND (logOperatii.data = convert(datetime,'" + vDataStart.Date.ToShortDateString() + "',104)) AND (logOperatii.operatieId = '2')";
            int vAdaugari = Convert.ToInt32(vCmd.ExecuteScalar());
            // operatiuni de modificare
            vCmd.CommandText = "SELECT COUNT(*) AS Expr1 FROM logOperatii INNER JOIN utilizatori ON logOperatii.utilizatorId = utilizatori.utilizatorId INNER JOIN unitati ON utilizatori.unitateId = unitati.unitateId WHERE (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (CONVERT(nvarchar, unitati.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (logOperatii.utilizatorId IN (" + vUtilizatoriIduri + ")) AND (logOperatii.data = convert(datetime,'" + vDataStart.Date.ToShortDateString() + "',104)) AND (logOperatii.operatieId = '3')";
            int vModificari = Convert.ToInt32(vCmd.ExecuteScalar());
            // operatiuni de stergere
            vCmd.CommandText = "SELECT COUNT(*) AS Expr1 FROM logOperatii INNER JOIN utilizatori ON logOperatii.utilizatorId = utilizatori.utilizatorId INNER JOIN unitati ON utilizatori.unitateId = unitati.unitateId WHERE (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (CONVERT(nvarchar, unitati.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (logOperatii.utilizatorId IN (" + vUtilizatoriIduri + ")) AND (logOperatii.data = convert(datetime,'" + vDataStart.Date.ToShortDateString() + "',104)) AND (logOperatii.operatieId = '4')";
            int vStergeri = Convert.ToInt32(vCmd.ExecuteScalar());
            // operatiuni de tiparire
            vCmd.CommandText = "SELECT COUNT(*) AS Expr1 FROM logOperatii INNER JOIN utilizatori ON logOperatii.utilizatorId = utilizatori.utilizatorId INNER JOIN unitati ON utilizatori.unitateId = unitati.unitateId WHERE (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (CONVERT(nvarchar, unitati.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (logOperatii.utilizatorId IN (" + vUtilizatoriIduri + ")) AND (logOperatii.data = convert(datetime,'" + vDataStart.Date.ToShortDateString() + "',104)) AND (logOperatii.operatieId = '5')";
            int vTipariri = Convert.ToInt32(vCmd.ExecuteScalar());
            // vizualizari
            vCmd.CommandText = "SELECT COUNT(*) AS Expr1 FROM logOperatii INNER JOIN utilizatori ON logOperatii.utilizatorId = utilizatori.utilizatorId INNER JOIN unitati ON utilizatori.unitateId = unitati.unitateId WHERE (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (CONVERT(nvarchar, unitati.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (logOperatii.utilizatorId IN (" + vUtilizatoriIduri + ")) AND (logOperatii.data = convert(datetime,'" + vDataStart.Date.ToShortDateString() + "',104)) AND (logOperatii.operatieId = '1')";
            int vVizualizari = Convert.ToInt32(vCmd.ExecuteScalar());
            vCmd.CommandText = "INSERT INTO rapStatisticUtilizatori (utilizatorId, an, luna, lunaText, data, unitateDenumire, judetDenumire, utilizatoriNumePrenume, gospodariiAdaugate, capitoleModificate, adaugari, modificari, stergeri, vizualizari, tipariri, eroriSemnalate, login, perioada) VALUES ('" + Session["SESutilizatorId"].ToString() + "', '" + Convert.ToDateTime(tbDataDin.Text).Year.ToString() + "', '" + Convert.ToDateTime(tbDataDin.Text).Month.ToString() + "', '" + Convert.ToDateTime(tbDataDin.Text).ToString("MMMM") + "', convert(datetime,'" + vDataStart.Date.ToShortDateString() + "',104), '" + vUnitatea + "', '" + vJudet + "', '" + vUtilizatoriNume + "', '" + vGospodariiAdaugate.ToString() + "', '" + vCapitoleModificate.ToString() + "', '" + vAdaugari.ToString() + "', '" + vModificari + "', '" + vStergeri + "', '" + vVizualizari.ToString() + "', '" + vTipariri.ToString() + "', '" + vEroriSemnalate.ToString() + "', '" + vLogin.ToString() + "', '" + tbDataDin.Text + " - " + tbDataLa.Text + "')";
            vCmd.ExecuteNonQuery();
            vDataStart = vDataStart.AddDays(1);
        }
        ManipuleazaBD.InchideConexiune(vCon);
        ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "raportStatisticUtilizatori", "tiparire raport statistic utilizatori", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
        ResponseHelper.Redirect("~/printRaportStatisticUtilizatori.aspx", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
    }
    protected void SqlUtilizatori_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        // extrag tip de utilizator din utilizatori
        if (ViewState["tipUtilizator"] == null)
        {
            SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
            SqlCommand vCmd = new SqlCommand();
            vCmd.Connection = vCon;
            vCmd.CommandText = "select top(1) tipUtilizatorId from utilizatori where utilizatorId='" + Session["SESutilizatorId"].ToString() + "'";
            ViewState["tipUtilizator"] = vCmd.ExecuteScalar().ToString();
            ManipuleazaBD.InchideConexiune(vCon);
        }
        // daca este primar afisez doar utilizatorii din unitatea curenta
        if (ViewState["tipUtilizator"].ToString() == "5")
            e.Command.CommandText += " AND (unitati.unitateId = '" + Session["SESunitateId"].ToString() + "')";
        e.Command.CommandText += " ORDER BY unitati.unitateDenumire, utilizatori.utilizatorNume, utilizatori.utilizatorPrenume";
    }
}
