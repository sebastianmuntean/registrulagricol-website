﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class reparaLocalitati : System.Web.UI.Page
{
    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void lbSalveaza_Click(object sender, EventArgs e)
    {
        Label lblDenumireVeche = (Label)((LinkButton)sender).Parent.FindControl("lblDenumireVeche");
        TextBox tbDenumireNoua = (TextBox)((LinkButton)sender).Parent.FindControl("tbDenumireNoua");
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "update gospodarii set localitate=N'" + tbDenumireNoua.Text + "' where unitateId='" + ddlUnitati.SelectedValue.ToString() + "' and localitate=N'" + lblDenumireVeche.Text + "'";
        vCmd.ExecuteNonQuery();
        ManipuleazaBD.InchideConexiune(vCon);
        gvLocalitatiGospodarii.DataBind();
    }
    protected void lbSalveaza_Click1(object sender, EventArgs e)
    {
        Label lblDenumireVeche = (Label)((LinkButton)sender).Parent.FindControl("lblDenumireVeche");
        TextBox tbDenumireNoua = (TextBox)((LinkButton)sender).Parent.FindControl("tbDenumireNoua");
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "update parcele set parcelaLocalitate=N'" + tbDenumireNoua.Text + "' where unitateaId='" + ddlUnitati.SelectedValue.ToString() + "' and parcelaLocalitate=N'" + lblDenumireVeche.Text + "'";
        vCmd.ExecuteNonQuery();
        ManipuleazaBD.InchideConexiune(vCon);
        gvLocalitatiParcele.DataBind();
    }
}
