﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Data.SqlClient;

/// <summary>
/// autor           ?
/// data            ?
///     ultima actualizare          20.01.2012
///     utilzator                   alex    
///     modificare                  adaugare taguri [bovine] bovine  |  [pasari] pasari  , [livada] livada, sesizare 491
/// </summary>

public partial class adevSabloane : System.Web.UI.Page
{

    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion
    private void populareCreatPentru()
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "SELECT judete.judetDenumire + ' - ' + unitati.unitateDenumire AS unitate, unitati.unitateId FROM unitati INNER JOIN judete ON unitati.judetId = judete.judetId ORDER BY unitate";
        ddlCCreatPentru.Items.Clear();
        ddlCCreatPentru.Items.Add(new ListItem(" ", "%"));
        ddlCCreatPentru.Items.Add(new ListItem("-toate", "0"));
        SqlDataReader vTabel = vCmd.ExecuteReader();
        while (vTabel.Read())
            ddlCCreatPentru.Items.Add(new ListItem(vTabel["unitate"].ToString(), vTabel["unitateId"].ToString()));
        vTabel.Close();
        ManipuleazaBD.InchideConexiune(vCon);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            populareCreatPentru();
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "adevSabloane", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
        }

        lblLegenda.Text =
  @"Legenda: <br />(textul scris între [...] se va completa automat la emiterea adeverinţei cu valorile corespunzătoare din datele înregistrate la gospodăria din care face parte beneficiarul adeverinţei)<br />

*************************************** <br />
ARANJARE TEXT<br />
***************************************
[paragraf]  (paragraf nou) <br />
!!!Atentie!!! Pentru paragraf nou folositi tasta ENTER sau 
SHIFT+ENTER (pentru ruperea randului care sa pastreze aceeasi distanta intre randuri)<br />
<br />
*************************************** <br />
DETALII UNITATE<br />
***************************************
[unitate]  denumirea unităţii         
[judet]  judeţul 
[localitate-rang] orașului, municipiului, comunei, satului
[adresa-unitate] adresa unitatii      
[telefon-unitate] telefon unitate
[fax-unitate] fax       
[email-unitate] sau [email] email unitate
[web-unitate] pagina internet a unitatii
[cod-postal-unitate] codul postal al unitatii
[primar]  primar
[viceprimar]  vice-primar 
[secretar]  secretar
[contabil]  contabil 
[sefserviciuagricol] sef serviciu agricol  
[inspector] [inspector1] [inspector2] [inspector3] inspectori ([inspector] preia datele din inspector1) 
[utilizator] numele si prenumele utilizatorului care emite adeverinta
<br /><br />
*************************************** <br />
DATE GOSPODARIE<br />
*************************************** 

[localitate] localitatea de domiciliu 
[localitate-proprietate] localitate proprietate strainas
[localitate-unitate]  localitatea unităţii
[volum]  (volumul în care este înscrisă gospodăria) 
[pozitia]  (poziţia în volum) 
[parcele-bloc-fizic] (lista parcelelor + blocul fizic)
[adresa]  (adresa gospodariei, str+nr+bl+sc+et )
[adresa-domiciliu] (adresa de strainaş jud+loc+str+bl+sc+et) 

[nr-persoane]  numărul de persoane ce locuiesc în gospodărie 
[copii-in-intretinere]  numărul de copii(fiu/fiică) cu vârsta < 18 ani 
[cnp]  (cod numeric personal) 
[seria-ci]  (seria carte de identitate) 
[numar-ci]  (număr carte de identitate) 
[tip-gospodarie] tipul gospodariei (localnic, strainas, firma pe raza localitatii, firma strainasa)
<br /><br />
***************<br />
MEMBRI<br />
***************
[cap-gospodarie] nume si prenume cap de gospodarie
[nume]  numele şi prenumele beneficiarului 
[data-nasterii] data nasterii membrului selectat

[membru] afiseaza o listă cu numele + prenumele membrilor gospodăriei
[membru-data-nasterii] afişează o listă cu membrii gospodpăriei + data naşterii

[lista-membri]
[lista-membri-rudenie]
[lista-membri-rudenie-data-nasterii]
[lista-membri-rudenie-data-nasterii-cnp]

<br /><br />
***************<br />
CONSTRUCTII cap.11<br />
***************
[suprafata-cladire1] afiseaza suprafata trecuta prima cladire (capitol 11 rand 1)
[suprafata-cladiri] afiseaza suprafata cladirilor de locuit (capitol 11 rand 1+4+7+10)
<br /><br />
***************<br />
ANIMALE cap.7<br />
***************
[animale]  efectivele de animale totale  

[bovine] 
[vaci-lapte]  
[bovine-tineret-ingrasat]
[bovine-tineret-prasala]
[juninci]

[ovine]     total ovine
[ovine-femele-reproductie]
[ovine-mioare-montate]
[ovine-tineret]
[berbeci]
[alte-ovine]
[tapi]

[caprine]   total caprine  
[capre]
[tapi]
[caprine-tineret]
[alte-caprine]

[ovine/caprine] ovine + caprine

[porcine]    total porcine
[purcei]
[porcine-ingrasat]
[scroafe]
[vieri]
[scroafe-montate]
[scroafe-nemontate]
[scrofite]
[scrofite-tineret-femel-prasila]

[albine] familii de albine 

[cabaline]  
[magari]   
[catari]  

[iepuri]

[pasari]  
[pui-carne]
[gaini-ouatoare] <br /><br />

************<br />
TERENURI - doar valori / din contracte + parcele 2b<br />
[teren-intravilan-ha] terenuri in intravilan in ha (toate categoriile)
[teren-intravilan-mp] terenuri in intravilan in metri patrati (toate categoriile)
[teren-extravilan-ha] terenuri in extravilan in ha (toate categoriile)
[teren-extravilan-mp] terenuri in extravilan in metri patrati (toate categoriile)
[teren-arabil-extravilan-ha] terenuri arabile in extravilan in ha
[teren-arabil-extravilan-mp] terenuri arabile in extravilan in metri patrati
[teren-arabil-intravilan-ha] terenuri arabile in intravilan in ha
[teren-arabil-intravilan-mp] terenuri arabile in intravilan in metri patrati
************
<br />*** arabil ***
[teren-arabil-proprietate] total arabil din parcele fara cele date in arenda - cf. contracte
[teren-arabil-total] total arabil proprietate + cele primite cf contracte
[teren-arabil-arenda] [teren-arabil-parte][teren-arabil-gratuit] [teren-arabil-concesiune] [teren-arabil-asociere][teren-arabil-alte-forme]
<br />*** pasuni ***
[teren-pasuni-proprietate] total pasuni din parcele fara cele date in arenda - cf. contracte
[teren-pasuni-total] total pasuni proprietate + cele primite cf contracte
[teren-pasuni-arenda] [teren-pasuni-parte][teren-pasuni-gratuit] [teren-pasuni-concesiune] [teren-pasuni-asociere][teren-pasuni-alte-forme]
<br />*** fanete ***
[teren-fanete-proprietate] total fanete din parcele fara cele date in arenda - cf. contracte
[teren-fanete-total] total fanete proprietate + cele primite cf contracte
[teren-fanete-arenda] [teren-fanete-parte][teren-fanete-gratuit] [teren-fanete-concesiune] [teren-fanete-asociere][teren-fanete-alte-forme]
<br />*** vii ***
[teren-vii-proprietate] total vii din parcele fara cele date in arenda - cf. contracte
[teren-vii-total] total vii proprietate + cele primite cf contracte<br />
<br />*** paduri ***
[teren-paduri-proprietate] total paduri din parcele fara cele date in arenda - cf. contracte
[teren-paduri-total] total paduri proprietate + cele primite cf contracte
<br /><br />
************<br />
CULTURI - doar valori / din 4a<br />
************
[culturi] - toate culturile din cap 4a
[grau/secara]
[orz]
[orzoaica]
[cartofi]
[porumb]
[sfecla-de-zahar]
[floarea-soarelui]
[plante-de-nutret]
[ovaz]
[legume]
<br /><br />
************<br />
TERENURI - cu text / din cap.3<br />
************
[terenuri]  terenuri 
[terenuri-minim30ari]  terenuri cu suprafata minim 30 de ari
[teren-proprietate]  teren total din care arabil 
[teren-arenda-dat]  terenuri date în arendă 
[teren-arenda-primit]  terenuri primite în arendă 
[teren-parte-dat]  terenuri date în parte 
[teren-parte-primit]  terenuri primite în parte 
[teren-gratuit-dat]  terenuri date cu titlu gratuit 
[teren-gratuit-primit]  terenuri primite cu titltu gratuit 
[teren-concesiune-dat]  terenuri date în concesiune 
[teren-concesiune-dat]  terenuri date în concesiune 
[teren-asociere-dat]  terenuri date în asociere 
[teren-asociere-primit]  terenuri primite în asociere 
[teren-alte-forme-primit]  terenuri primite sub alte forme 
[terenuri-paduri-total] total paduri
[livada] livada
<br /><br />
**********<br />
UTILAJE<br />
**********
[utilaje]  utilajele 
[tractoare]  tractoare 1+2+3+4+5+6+7
[motocultoare] Motocultoare 8
[motocositoare] Motocositoare 9	
[pluguri]  toate plugurile (randurile 10 +11+ 12 + 13)
[pluguri-mecanica] Pluguri pentru tractor cu două trupiţe 10 + Pluguri pentru tractor cu trei trupiţe 11 + Pluguri pentru tractor cu mai mult de trei trupiţe 12
[pluguri-animala] Pluguri cu tracţiune animală 13
[cultivatoare] Cultivatoare	14 	
[grape] toate grapele 15+16
[grape-mecanica] Grape cu tracţiune mecanică (toate tipurile)	15 	
[grape-animala] Grape cu tracţiune animală	16 	
[combinatoare] Combinatoare	17 	
[semanatori] toate semanatorile 18+19+20+21+22+23+24
[semanatori-mecanica]Semănători cu tracţiune mecanică pentru păioase - simple 18 + Semănători cu tracţiune mecanică pentru păioase - multifuncţionale 19 +	
Semănători cu tracţiune mecanică pentru păioase - semănat în teren nelucrat	20 + Semănători cu tracţiune mecanică pentru prăşitoare - simple	21 	+ Semănători cu tracţiune mecanică pentru prăşitoare - multifuncţionale 22 	+ Semănători cu tracţiune mecanică pentru prăşitoare - semănat în teren nelucrat 23 	
[semanatori-animala] Semănători cu tracţiune animala 24
[masini-plantat] 25+26
[masini-plantat-cartofi] Maşini pentru plantat cartofi 25 	
[masini-plantat-rasaduri] Maşini pentru plantat răsaduri 26 	
[masini-imprastiat] 27+28
[masini-imprastiat-chimice] Maşini pentru împrăştiat îngrăşăminte chimice 27 	
[masini-imprastiat-organice] Maşini pentru împrăştiat îngrăşăminte organice	28 	
[masini-stropit] 29+30
[masini-stropit-purtate] Maşini de stropit şi prăfuit cu tracţiune mecanică - purtate	29 	
[masini-stropit-tractate] Maşini de stropit şi prăfuit cu tracţiune mecanică - tractate	30 	
[masini-erbicidat] Maşini pentru erbicidat	31 	
[combine-paioase]Combine autopropulsate pentru recoltat cereale păioase	32 	
[combine-porumb]Combine autopropulsate pentru recoltat pentru recoltat porumb	33 	
[combine-furaje]Combine autopropulsate pentru recoltat pentru recoltat furaje	34 	
[batoze] Batoze pentru cereale păioase	35 	
[combine-sfecla] Combine pentru recoltat sfeclă de zahăr	36 	
[dislocatoare-sfecla] Dislocatoare pentru sfeclă de zahăr	37 	
[decoletat-sfecla] Maşini de decoletat sfeclă de zahăr	38 	
[recoltat-cartofi] Combine şi maşini pentru recoltat cartofi	39 	
[cositori] Cositori cu tracţiune mecanică	40 	
[vindrovere] Vindrovere autopropulsate pentru recoltat furaje	41 	
[prese-balotat] 42+43
[prese-balotat-paralelipipedici] Prese pentru balotat paie şi fân - baloţi paralelipipedici	42 	
[prese-balotat-cilindrici] Prese pentru balotat paie şi fân - baloţi cilindrici	43 	
[remorci] Remorci pentru tractor	44 	
[auto-transport-marfa] 45+46
[auto-transport-marfa-1,5tone] Autovehicule pentru transport mărfuri cu capacitate până la 1,5 tone	45 	
[auto-transport-marfa+1,5tone] Autovehicule pentru transport mărfuri cu capacitate peste 1,5 tone	46 	
[care-carute] Care şi căruţe	47 	
[incarcatoare-hidraulice] Încărcătoare hidraulice	48 	
[motopompe-irigat] Motopompe pentru irigat	49 	
[aripi-ploaie-deplasare-manuala] Aripi de ploaie cu deplasare manuală	50 	
[instalatii-irigat] 51+52+53
[instalatii-irigat-tambur] Instalaţii de irigat autodeplasabile cu tambur şi furtun	51 	
[instalatii-irigat-deplasare-liniara] Instalaţii de irigat cu deplasare liniară	52 	
[instalatii-irigat-pivot] Instalaţii de irigat cu pivot	53 	
[instalatii-muls] Instalaţii pentru muls mecanic	54 	
[instalatii-preparare-furaje] Instalaţii pentru prepararea furajelor	55 	
[instalatii-dejectii] Instalaţii pentru evacuarea dejecţiilor	56 	
[instalatii-tuica] Instalaţii/cazan pentru fabricat ţuică/rachiu	57 	
[alte-utilaje] Alte utilaje, instalaţii pentru agricultură şi silvicultură, mijloace de transport cu tracţiune animală şi mecanică	58 	
[mori] 59+60+61
[mori-ciocanele] Mori cu ciocănele	59 	
[mori-valturi] Mori cu valţuri	60 	
 ";
        /*
         [paragraf]Se adevereşte prin prezenta că numitul [nume] domiciliat/ă în [localitate], CNP [CNP], BI/CI seria [seriaCI] nr.[numarCI] figurează în Registrul Agricol al comunei [localitate], vol.[volum], poz.[poziţia], unde locuieşte împreună cu familia.
[paragraf]Menţiuni: familia nu deţine teren agricol şi nu realizează venituri impozabile.
[paragraf]Eliberăm prezenta adeverinţă spre a-i servi sus-numitului/ei la [  ]. 
         
         */
        lblLegenda.Text = InlocuiesteTagLegenda(lblLegenda.Text);
    }
    protected string InlocuiesteTagLegenda(string pText)
    {
        int vInceput = 0;
        int vFinal = 0;
        string pTextFinal = pText;
        while (vInceput != -1 && vFinal != -1)
        {
            vInceput = pText.IndexOf('[', vFinal);
            if (vInceput > 0) vFinal = pText.IndexOf(']', vInceput);
            else break;
            string vTag = pText.Substring(vInceput, vFinal - vInceput + 1);
            pTextFinal = pTextFinal.Replace(vTag, "<br /><span style='color:red; font-weight:bold; padding-right:30px;'>" + vTag + "</span>");
            vInceput = vFinal;
        }
        return pTextFinal;
    }

    protected void AdaugaModificaAdevSablon(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        string vInterogare = "";
        int vValoareSelectata = Convert.ToInt32(gvListaAdevSabloane.SelectedValue);
        if (((Button)sender).ID.ToString() == "btAdaugaAdevSablon")
        {
            vInterogare = "INSERT INTO adeverinteSabloane (unitateId, adevTip, adevSablonTip, adevSablonTitlu, adevSablonText, adevSubsolId, adevAntetId, adevSablonDenumire) VALUES ('" + Session["SESunitateId"] + "','" + ddAdevTip.SelectedValue.ToString() + "','0', N'',N'" + tbText.Text + "', '" + ddSubsol.SelectedValue + "', '" + ddAntetId.SelectedValue + "', N'" + tbDenumireSablon.Text + "') ";
            ManipuleazaBD.fManipuleazaBD(vInterogare, Convert.ToInt16(Session["SESan"]));
            gvListaAdevSabloane.SelectedIndex = -1;
            pnAdaugaAdevSablon.Visible = false;
            pnListaAdevSabloane.Visible = true;
            btModificaAdevSablon.Visible = false;
            btStergeAdevSablon.Visible = false;
        }
        else
        {
            vInterogare = @"UPDATE adeverinteSabloane SET  unitateId='" + Session["SESunitateId"] + "', adevTip='" + ddAdevTip.SelectedValue + "', adevSablonTip='0', adevSablonTitlu =N'', adevSablonText=N'" + tbText.Text + "', adevSubsolId='" + ddSubsol.SelectedValue + "', adevAntetId='" + ddAntetId.SelectedValue + "', adevSablonDenumire=N'" + tbDenumireSablon.Text + "' WHERE adevSablonId = '" + vValoareSelectata.ToString() + "'";
            ManipuleazaBD.fManipuleazaBD(vInterogare, Convert.ToInt16(Session["SESan"]));
            pnAdaugaAdevSablon.Visible = false;
            pnListaAdevSabloane.Visible = true;
            btModificaAdevSablon.Visible = true;
            btStergeAdevSablon.Visible = true;
            gvListaAdevSabloane.SelectedIndex = GasesteRandul(vValoareSelectata);
        }
        gvListaAdevSabloane.DataBind();
    }
    protected int GasesteRandul(int pValoare)
    {
        int vRand = -1;
        for (int i = 0; i < gvListaAdevSabloane.Rows.Count; i++)
        {
            if (gvListaAdevSabloane.Rows[i].Cells[0].Text == pValoare.ToString())
            {
                vRand = i;
                break;
            }
        }
        return vRand;
    }
    protected void ModificaArataListaAdevSablon(object sender, EventArgs e)
    {
        pnAdaugaAdevSablon.Visible = false;
        pnListaAdevSabloane.Visible = true;
        btModificaAdevSablon.Visible = true;
        btStergeAdevSablon.Visible = true;
    }
    protected void AdaugaArataListaAdevSablon(object sender, EventArgs e)
    {
        pnAdaugaAdevSablon.Visible = false;
        pnListaAdevSabloane.Visible = true;
        btModificaAdevSablon.Visible = false;
        btStergeAdevSablon.Visible = false;
    }
    protected void InitializareCampuri(int pTip, string pAdevSablonId)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        // adaugare
        if (pTip == 0)
        {
            // daca nu e utilizator administrator = 0, nu afisam tipul de adeverinta si selectam tipul de adeverinta cu unitatea utilizatorului
            if (ManipuleazaBD.fRezultaUnString("SELECT TOP(1) tipUtilizatorId FROM utilizatori WHERE utilizatorId ='" + Session["SESutilizatorId"].ToString() + "'", "tipUtilizatorId", Convert.ToInt16(Session["SESan"])) == "1")
            {
                lblAdevTip.Visible = true;
                ddAdevTip.Visible = true;
            }
            else
            {
                lblAdevTip.Visible = false;
                ddAdevTip.Visible = false;
                ddAdevTip.DataBind();
                ddAdevTip.SelectedValue = ManipuleazaBD.fRezultaUnString("SELECT TOP(1) unitateId FROM utilizatori WHERE utilizatorId ='" + Session["SESutilizatorId"].ToString() + "'", "unitateId", Convert.ToInt16(Session["SESan"])).ToString();
            }
            //tbTitlu.Text = "";
            tbText.Text = "";
            tbDenumireSablon.Text = "";
            try
            {
                ddAntetId.DataBind();
                // antet
                string vRez = ManipuleazaBD.fRezultaUnString("SELECT MIN(adevSubsolId) as minim FROM adeverinteSabloane WHERE unitateId='" + Session["SESunitateId"] + "' AND adevSubsolTip = '0'", "minim", Convert.ToInt16(Session["SESan"]));
                if (vRez != "") { ddAntetId.SelectedValue = vRez; }
            }
            catch { }

            try
            {
                ddSubsol.DataBind();
                // subsol
                string vRez = ManipuleazaBD.fRezultaUnString("SELECT MIN(adevSubsolId) as minim FROM adeverinteSubsoluri WHERE unitateId='" + Session["SESunitateId"] + "' AND adevSubsolTip = '1'", "minim", Convert.ToInt16(Session["SESan"]));
                if (vRez != "") { ddSubsol.SelectedValue = vRez; }
            }
            catch
            { }
        }
        // modificare
        else
        {
            // daca nu e utilizator administrator = 0, nu afisam tipul de adeverinta si selectam tipul de adeverinta existent
            ddAdevTip.DataBind();
            ddAdevTip.SelectedValue = ManipuleazaBD.fRezultaUnString("SELECT adevTip FROM adeverinteSabloane WHERE adevSablonId ='" + gvListaAdevSabloane.SelectedValue + "'", "adevTip", Convert.ToInt16(Session["SESan"]));
            if (ManipuleazaBD.fRezultaUnString("SELECT TOP(1) tipUtilizatorId FROM utilizatori WHERE utilizatorId ='" + Session["SESutilizatorId"].ToString() + "'", "tipUtilizatorId", Convert.ToInt16(Session["SESan"])) == "1")
            {
                lblAdevTip.Visible = true;
                ddAdevTip.Visible = true;
            }
            else
            {
                lblAdevTip.Visible = false;
                ddAdevTip.Visible = false;
            }
            List<string> vListaElemente = new List<string> { "adevSablonTitlu", "adevSablonText", "adevSubsolId", "adevAntetId", "adevSablonDenumire" };
            List<string> vListaRezultate = ManipuleazaBD.fRezultaUnString("SELECT adevSablonTitlu, adevSablonText, adevSubsolId, adevAntetId, adevSablonDenumire FROM adeverinteSabloane WHERE adevSablonId ='" + gvListaAdevSabloane.SelectedValue + "'", vListaElemente, Convert.ToInt16(Session["SESan"]));
            try
            {
                ddAntetId.SelectedValue = vListaRezultate[3];
                ddSubsol.SelectedValue = vListaRezultate[2];
            }
            catch { }
            tbText.Text = vListaRezultate[1];
            //tbTitlu.Text = vListaRezultate[0];
            tbDenumireSablon.Text = vListaRezultate[4];
        }
    }
    protected void ArataAdaugaAdevSablon(object sender, EventArgs e)
    {
        int vValoareSelectata = Convert.ToInt32(gvListaAdevSabloane.SelectedValue);
        ddSubsol.DataBind();
        ddAntetId.DataBind();
        pnAdaugaAdevSablon.Visible = true;
        pnListaAdevSabloane.Visible = false;
        if (((Button)sender).ID.ToString() == "btListaAdaugaAdevSablon")
        {
            InitializareCampuri(0, "");
            //gvListaAdevSabloane.SelectedIndex = -1;
            // butoane adauga si lista-adauga active
            btAdaugaAdevSablon.Visible = true;
            btAdaugaArataListaAdevSablon.Visible = true;
            btAdaugaModificaAdevSablon.Visible = false;
            btModificaArataListaAdevSablon.Visible = false;
        }
        else
        {
            InitializareCampuri(1, vValoareSelectata.ToString());
            btAdaugaAdevSablon.Visible = false;
            btAdaugaArataListaAdevSablon.Visible = false;
            btAdaugaModificaAdevSablon.Visible = true;
            btModificaArataListaAdevSablon.Visible = true;
        }
    }
    protected void StergeSablonul(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        string vAdevSablonId = gvListaAdevSabloane.SelectedValue.ToString();
        ManipuleazaBD.fManipuleazaBD("DELETE FROM [adeverinteSabloane] WHERE [adevSablonId] = '" + vAdevSablonId + "'", Convert.ToInt16(Session["SESan"]));
        gvListaAdevSabloane.DataBind();
        gvListaAdevSabloane.SelectedIndex = -1;
        btModificaAdevSablon.Visible = false;
        btStergeAdevSablon.Visible = false;
    }
    protected void gvListaAdevSabloane_SelectedIndexChanged(object sender, EventArgs e)
    {
        // actiuni la selectare rand; aratam butoanele de sterge si modifica
        btAdaugaAdevSablon.Visible = true;
        btModificaAdevSablon.Visible = true;
        btStergeAdevSablon.Visible = true;
        gvListaAdevSabloane.DataBind();
    }
    protected void gvListaAdevSabloane_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(gvListaAdevSabloane, e, this);
    }
    protected void gvListaAdevSabloane_DataBound(object sender, EventArgs e)
    {
        for (int i = 0; i < gvListaAdevSabloane.Rows.Count; i++)
        {
            if (gvListaAdevSabloane.Rows[i].RowType == DataControlRowType.DataRow)
            {
                try
                {


                    gvListaAdevSabloane.Rows[i].Cells[3].Text = ManipuleazaBD.fRezultaUnString("SELECT adevSubsolDenumire FROM adeverinteSubsoluri WHERE adevSubsolId ='" + gvListaAdevSabloane.Rows[i].Cells[3].Text + "'", "adevSubsolDenumire", Convert.ToInt16(Session["SESan"]));
                    gvListaAdevSabloane.Rows[i].Cells[4].Text = ManipuleazaBD.fRezultaUnString("SELECT adevSubsolDenumire FROM adeverinteSubsoluri WHERE adevSubsolId ='" + gvListaAdevSabloane.Rows[i].Cells[4].Text + "'", "adevSubsolDenumire", Convert.ToInt16(Session["SESan"]));
                    if (gvListaAdevSabloane.Rows[i].Cells[6].Text == "0")
                    {
                        gvListaAdevSabloane.Rows[i].Cells[6].Text = "toate";
                    }
                    else
                    {
                        gvListaAdevSabloane.Rows[i].Cells[6].Text = ManipuleazaBD.fRezultaUnString("SELECT unitateDenumire FROM unitati WHERE unitateId ='" + gvListaAdevSabloane.Rows[i].Cells[6].Text + "'", "unitateDenumire", Convert.ToInt16(Session["SESan"]));
                    }
                    ((Label)gvListaAdevSabloane.Rows[i].FindControl("lblText")).ToolTip = Regex.Replace(((Label)gvListaAdevSabloane.Rows[i].FindControl("lblText")).Text, @"<(.|\n)*?>", string.Empty);
                    try
                    {
                        ((Label)gvListaAdevSabloane.Rows[i].FindControl("lblText")).Text =
                            Regex.Replace(((Label)gvListaAdevSabloane.Rows[i].FindControl("lblText")).Text, @"<(.|\n)*?>", string.Empty).Remove(250) + "...";

                    }
                    catch { }
                }
                catch { }
            }
        }
    }

    protected void ddAdevTip_DataBound(object sender, EventArgs e)
    {
        ListItem liAdevTip = new ListItem();
        liAdevTip.Value = "0";
        liAdevTip.Text = " vizibilă pentru toate unitățile";
        liAdevTip.Selected = true;
        ddAdevTip.Items.Add(liAdevTip);
    }
    protected void gvListaAdevSabloane_Init(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        List<string> vCampuri = new List<string> { "tipUtilizatorId", "unitateId" };
        List<string> vRezultate = new List<string> { };
        vRezultate.Clear();
        vRezultate = ManipuleazaBD.fRezultaUnString("SELECT TOP(1) tipUtilizatorId, unitateId FROM utilizatori WHERE utilizatorId ='" + Session["SESutilizatorId"].ToString() + "'", vCampuri, Convert.ToInt16(Session["SESan"]));
        if (vRezultate[0] != "1")
        {
            SqlListaAdevSabloane.SelectCommand = "SELECT * FROM [adeverinteSabloane] LEFT OUTER JOIN unitati ON unitati.unitateId = adeverinteSabloane.unitateID WHERE [adeverinteSabloane].[adevTip] = '" + vRezultate[1] + "'";
        }
    }
    protected void ddAntetId_Init(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        List<string> vCampuri = new List<string> { "tipUtilizatorId", "unitateId" };
        List<string> vRezultate = new List<string> { };
        vRezultate.Clear();
        vRezultate = ManipuleazaBD.fRezultaUnString("SELECT TOP(1) tipUtilizatorId, unitateId FROM utilizatori WHERE utilizatorId ='" + Session["SESutilizatorId"].ToString() + "'", vCampuri, Convert.ToInt16(Session["SESan"]));
        if (vRezultate[0] != "1")
        {
            SqlAnteturi.SelectCommand = "SELECT [adevSubsolId], [adevSubsolDenumire] FROM [adeverinteSubsoluri] WHERE ( adevSubsolTip = '0') AND ((adevTip = '" + vRezultate[1] + "') OR (adevTip ='0')) ORDER BY [adevSubsolDenumire]";
        }
    }
    protected void ddSubsol_Init(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        List<string> vCampuri = new List<string> { "tipUtilizatorId", "unitateId" };
        List<string> vRezultate = new List<string> { };
        vRezultate.Clear();
        vRezultate = ManipuleazaBD.fRezultaUnString("SELECT TOP(1) tipUtilizatorId, unitateId FROM utilizatori WHERE utilizatorId ='" + Session["SESutilizatorId"].ToString() + "'", vCampuri, Convert.ToInt16(Session["SESan"]));
        if (vRezultate[0] != "1")
        {
            SqlSubsoluri.SelectCommand = "SELECT [adevSubsolId], [adevSubsolDenumire] FROM [adeverinteSubsoluri] WHERE ( adevSubsolTip = '1') AND ((adevTip = '" + vRezultate[1] + "') OR (adevTip ='0')) ORDER BY [adevSubsolDenumire]";
        }
    }
    protected void gvListaAdevSabloane_PreRender(object sender, EventArgs e)
    {
        for (int i = 0; i < gvListaAdevSabloane.Rows.Count; i++)
        {
            if (gvListaAdevSabloane.Rows[i].RowType == DataControlRowType.DataRow)
            {
                ((Label)gvListaAdevSabloane.Rows[i].FindControl("lblText")).Text = ((Label)gvListaAdevSabloane.Rows[i].FindControl("lblText")).Text.Replace("adeverinta_antet", "adev_antet_tabel").Replace("adev_general", "adev_antet_tabel").Replace("css/master.css", String.Empty).Replace("font-size:", "x:").Replace("width:", "x:").Replace("height:", "x:").Replace("<br />", String.Empty).Replace("<p", "<x").Replace("</p>", "</x>");
            }
        }
    }
    protected void bt1_Click(object sender, EventArgs e)
    {
        tbText.Text = "<link href=\"css/master.css\" rel=\"stylesheet\" type=\"text/css\"><div class=\"adev_general\"><p style=\"font-size:27px; font-weight:bold; text-align:center;\">" + ManipuleazaBD.fRezultaUnString("SELECT adevSablonTitlu FROM adeverinteSabloane WHERE adevSablonId = '" + gvListaAdevSabloane.SelectedValue + "' ", "adevSablonTitlu", Convert.ToInt16(Session["SESan"])) + "</p>" + tbText.Text + "</div>";
        // tbText.DataBind();
    }
}
