﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Utilizatori.aspx.cs" Inherits="Utilizatori" UICulture="ro-RO" Culture="ro-RO"
    EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Administrare / Utilizatori" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="upUtilizatori" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnAfiseazaMesaje" runat="server">
                <asp:ValidationSummary ID="valSumUtilizatori" runat="server" DisplayMode="SingleParagraph"
                    Visible="true" ValidationGroup="GrupValidareUtilizatori" CssClass="validator"
                    ForeColor="" />
            </asp:Panel>
            <!-- lista utilizatori -->
            <asp:Panel ID="pnListaUtilizatori" runat="server" CssClass="panel_general" Visible="true">
                <asp:Panel ID="pnCautare" runat="server" CssClass="cauta">
                    <asp:Label ID="lblCauta" runat="server" Text="Cauta dupa:" />
                    <!-- autocomplete unitate -->
                    <asp:Label ID="lblCUnitate" runat="server" Text="Unitatea" />
                    <asp:TextBox ID="tbCUnitate" runat="server" autocomplete="off" AutoPostBack="True" />
                    <ajaxToolkit:AutoCompleteExtender ID="tbCLocalitate_AutoCompleteExtender" runat="server"
                        Enabled="True" ServiceMethod="GetCompletionList" ServicePath="AutoComplete.asmx"
                        TargetControlID="tbCUnitate" BehaviorID="AutoCompleteEx1" MinimumPrefixLength="1"
                        CompletionInterval="10" EnableCaching="True" CompletionSetCount="0" CompletionListCssClass="autocomplete_completionListElement"
                        CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                        DelimiterCharacters="" ShowOnlyCurrentWordInCompletionListItem="false" FirstRowSelected="False"
                        UseContextKey="True" ContextKey="Unitate">
                    </ajaxToolkit:AutoCompleteExtender>
                    <!-- autocomplete login -->
                    <asp:Label ID="lblCLogin" runat="server" Text="Nume de utilizator" />
                    <asp:TextBox ID="tbCLogin" runat="server" autocomplete="off" AutoPostBack="True" />
                    <ajaxToolkit:AutoCompleteExtender ID="tbCLogin_AutoCompleteExtender" runat="server"
                        Enabled="True" ServiceMethod="GetCompletionList" ServicePath="AutoComplete.asmx"
                        TargetControlID="tbCLogin" BehaviorID="AutoCompleteEx2" MinimumPrefixLength="1"
                        CompletionInterval="10" EnableCaching="True" CompletionSetCount="5" CompletionListCssClass="autocomplete_completionListElement"
                        CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                        DelimiterCharacters="" ShowOnlyCurrentWordInCompletionListItem="false" FirstRowSelected="False"
                        UseContextKey="True" ContextKey="Login">
                    </ajaxToolkit:AutoCompleteExtender>
                    <!-- autocomplete nume / prenume -->
                    <asp:Label ID="lblCNume" runat="server" Text="Nume / Prenume" />
                    <asp:TextBox ID="tbCNume" runat="server" autocomplete="off" AutoPostBack="True" />
                    <ajaxToolkit:AutoCompleteExtender ID="tbCNume_AutoCompleteExtender" runat="server"
                        Enabled="True" ServiceMethod="GetCompletionList" ServicePath="AutoComplete.asmx"
                        TargetControlID="tbCNume" BehaviorID="AutoCompleteEx3" MinimumPrefixLength="1"
                        CompletionInterval="10" EnableCaching="True" CompletionSetCount="5" CompletionListCssClass="autocomplete_completionListElement"
                        CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                        DelimiterCharacters="" ShowOnlyCurrentWordInCompletionListItem="false" FirstRowSelected="False"
                        UseContextKey="True" ContextKey="Nume">
                    </ajaxToolkit:AutoCompleteExtender>
                </asp:Panel>
                <asp:Panel runat="server">
                    <asp:GridView ID="gvUtilizatori" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnDataBound="gvUtilizatori_DataBound" OnRowDataBound="gvUtilizatori_RowDataBound"
                        BorderStyle="None" CellPadding="3" DataKeyNames="utilizatorId" DataSourceID="SqlUnitati"
                        GridLines="Vertical" CssClass="tabela" AllowPaging="True" OnSelectedIndexChanged="gvUtilizatori_SelectedIndexChanged">
                        <Columns>
                            <asp:BoundField DataField="unitateDenumire" HeaderText="Unitatea" SortExpression="unitateDenumire" />
                            <asp:BoundField DataField="utilizatorLogin" HeaderText="Login" SortExpression="utilizatorLogin" />
                            <asp:BoundField DataField="utilizatorId" HeaderText="nr." SortExpression="utilizatorId"
                                InsertVisible="False" ReadOnly="True" />
                            <asp:BoundField DataField="utilizatorNume" HeaderText="Nume" SortExpression="utilizatorNume" />
                            <asp:BoundField DataField="utilizatorPrenume" HeaderText="Prenume" SortExpression="utilizatorPrenume" />
                            <asp:BoundField DataField="utilizatorFunctia" HeaderText="Functia" SortExpression="utilizatorFunctia" />
                            <asp:BoundField DataField="utilizatorMobil" HeaderText="Mobil" SortExpression="utilizatorMobil" />
                            <asp:BoundField DataField="utilizatorTelefon" HeaderText="Telefon" SortExpression="utilizatorTelefon" />
                            <asp:BoundField DataField="utilizatorCNP" HeaderText="CNP" SortExpression="utilizatorCNP" />
                            <asp:BoundField DataField="utilizatorEmail" HeaderText="Email" SortExpression="utilizatorEmail" />
                            <asp:BoundField DataField="tipUtilizatorDenumire" HeaderText="Tip" SortExpression="tipUtilizatorDenumire" />
                            <asp:BoundField DataField="utilizatorActiv" HeaderText="Activ" SortExpression="utilizatorActiv" />
                        </Columns>
                        <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                        <HeaderStyle Font-Bold="True" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlUnitati" runat="server" 
                        SelectCommand="SELECT [utilizatori].[utilizatorLogin], [utilizatori].[utilizatorId], [utilizatori].[utilizatorNume], [utilizatori].[utilizatorPrenume], [utilizatori].[utilizatorFunctia], [utilizatori].[utilizatorMobil], [utilizatori].[utilizatorTelefon], [utilizatori].[utilizatorCNP], [utilizatori].[utilizatorEmail], [utilizatori].[utilizatorActiv], [utilizatori].[unitateId], [unitati].[unitateDenumire], [tipuriUtilizatori].[tipUtilizatorDenumire] FROM [utilizatori] LEFT OUTER JOIN [unitati] ON [utilizatori].[unitateId] = [unitati].[unitateId] LEFT OUTER JOIN [tipuriUtilizatori] ON [utilizatori].[tipUtilizatorId]=[tipuriUtilizatori].[tipUtilizatorId] WHERE ([unitati].[unitateDenumire] like ('%' + @pDenUnitate + '%')) AND ([utilizatori].[utilizatorNume] like ('%' + @pUtilizatorNume + '%')) AND ([utilizatori].[utilizatorLogin] like ('%' + @pUtilizatorLogin + '%'))"
                        OnInit="SqlUnitati_Init" OnSelecting="SqlUnitati_Selecting">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="tbCUnitate" Name="pDenUnitate" DefaultValue="%"
                                PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbCNume" Name="pUtilizatorNume" DefaultValue="%"
                                PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbCLogin" Name="pUtilizatorLogin" DefaultValue="%"
                                PropertyName="Text" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </asp:Panel>
                <asp:Panel ID="pnListaButoanePrincipale" CssClass="butoane" runat="server">
                    <asp:Button CssClass="buton" ID="btModificaUtilizator" runat="server" Text="modifica utilizatorul"
                        OnClick="ArataPanelModificaUtilizator" Visible="false" />
                    <asp:Button CssClass="buton" ID="btArataModificaDrepturiUtilizator" runat="server"
                        Text="modifica drepturi utilizator" OnClick="ArataPanelModificaDrepturiUtilizator"
                        Visible="false" />
                    <asp:Button CssClass="buton" ID="btStergeUtilizator" runat="server" Text="sterge utilizatorul"
                        OnClick="StergeUtilizator" ValidationGroup="GrupValidareUtilizatori" OnClientClick="return confirm(&quot;Sunteți sigur că doriți să ștergeți utilizatorul?&quot;)"
                        Visible="false" />
                    <asp:Button CssClass="buton" ID="btListaAdaugaUtilizator" runat="server" Text="adaugă un utilizator"
                        OnClick="ArataAdaugaUtilizatori" />
                    <asp:Button CssClass="buton" ID="btStergeCoduri" runat="server" Text="resetează coduri drepturi conectare"
                        OnClick="btStergeCoduri_Click" Visible="false" />
                    <asp:Button CssClass="buton" ID="btStergeIp" runat="server" Text="deblochează IP"
                        Visible="true" OnClick="btStergeIp_Click" />
                       
                </asp:Panel>
            </asp:Panel>
            <!-- adaugam utilizatori -->
            <asp:Panel ID="pnAdaugaUtilizator" CssClass="panel_general" runat="server" Visible="false">
                <asp:Panel CssClass="adauga" runat="server">
                    <asp:Panel runat="server">
                        <h2>
                            ADAUGA UN UTILIZATOR</h2>
                    </asp:Panel>
                    <asp:Panel runat="server">
                        <p>
                            <asp:Label ID="lblJudet" runat="server" Text="Judeţ"></asp:Label>
                            <asp:DropDownList ID="ddlJudet" AppendDataBoundItems="True" AutoPostBack="True" runat="server"
                                DataSourceID="SqlJudete" DataTextField="judetDenumire" DataValueField="judetId"
                                OnSelectedIndexChanged="ddlJudet_SelectedIndexChanged">
                                <asp:ListItem Text="-toate-" Value="%"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="SqlJudete" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016 %>"
                                SelectCommand="SELECT [judetId], [judetDenumire] FROM [judete] ORDER BY [judetDenumire]">
                            </asp:SqlDataSource>
                            <asp:Label ID="lblUnitate" runat="server" Text="Unitatea" />
                            <asp:DropDownList ID="ddUnitate" runat="server" DataSourceID="SqlDdUnitate" DataTextField="unitateDenumire"
                                DataValueField="unitateId">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="SqlDdUnitate" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016 %>"
                                SelectCommand="SELECT unitateId, unitateDenumire FROM unitati WHERE (CONVERT(nvarchar,judetId) like @judetId)"
                                OnInit="SqlDdUnitate_Init" OnSelecting="SqlDdUnitate_Selecting">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="ddlJudet" Name="judetId" PropertyName="SelectedValue" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </p>
                        <p>
                            <asp:Label ID="lblLoginOriginal" runat="server" Text="" Visible="false" />
                            <asp:Label ID="lblLogin" runat="server" Text="Nume de conectare (username)" />
                            <asp:TextBox ID="tbLogin" runat="server"></asp:TextBox>
                            <asp:Label ID="lblParola" runat="server" Text="Parola" />
                            <asp:TextBox TextMode="Password" ID="tbParola" runat="server" />
                            <asp:Label ID="lblConfirmaParola" runat="server" Text="Confirma parola" />
                            <asp:TextBox TextMode="Password" ID="tbConfirmaParola" runat="server" />
                            <asp:Label ID="lblTipUtilizator" runat="server" Text="Tip utilizator" />
                            <asp:DropDownList ID="ddTipUtilizator" runat="server" DataSourceID="SqlTipuriUtilizatori"
                                DataTextField="tipUtilizatorDenumire" DataValueField="tipUtilizatorId">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="SqlTipuriUtilizatori" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016 %>"
                                SelectCommand="SELECT tipUtilizatorId, tipUtilizatorDenumire FROM [tipuriUtilizatori] ORDER BY tipUtilizatorId DESC ">
                            </asp:SqlDataSource>
                        </p>
                        <p>
                            <asp:Label ID="lblUtilizatorEmail" runat="server" Text="Email" />
                            <asp:TextBox ID="tbUtilizatorEmail" runat="server" />
                            <asp:Label ID="lblUtilizatorNume" runat="server" Text="Nume" />
                            <asp:TextBox ID="tbUtilizatorNume" runat="server" />
                            <asp:Label ID="lblUtilizatorPrenume" runat="server" Text="Prenume" />
                            <asp:TextBox ID="tbUtilizatorPrenume" runat="server" />
                            <asp:Label ID="lblUtilizatorAdresa" runat="server" Text="Adresa" />
                            <asp:TextBox ID="tbUtilizatorAdresa" runat="server" Width="350px" />
                            <asp:Label ID="lblDurataSesiune" runat="server" Text="Durată sesiune lucru"></asp:Label>
                            <asp:TextBox ID="tbDurataSesiuneLucru" Width="50px" MaxLength="2" runat="server"></asp:TextBox>
                        </p>
                        <p>
                            <asp:Label ID="lblUtilizatorFunctia" runat="server" Text="Functia" />
                            <asp:TextBox ID="tbUtilizatorFunctia" runat="server" />
                            <asp:Label ID="lblUtilizatorCNP" runat="server" Text="CNP" />
                            <asp:TextBox ID="tbUtilizatorCNP" runat="server" Width="150px" />
                            <asp:Label ID="lblUtilizatorTelefon" runat="server" Text="Telefon" />
                            <asp:TextBox ID="tbUtilizatorTelefon" runat="server" Width="100px" />
                            <asp:Label ID="lblUtilizatorMobil" runat="server" Text="Mobil" />
                            <asp:TextBox ID="tbUtilizatorMobil" runat="server" Width="100px" />
                            <asp:Label ID="lblActiv" runat="server" Text="Activati?" />
                            <asp:DropDownList ID="ddUtilizatorActiv" runat="server">
                                <asp:ListItem Text="da" Selected="True" Value="1">       
                                </asp:ListItem>
                                <asp:ListItem Text="nu" Value="0">       
                                </asp:ListItem>
                            </asp:DropDownList>
                        </p>
                    </asp:Panel>
                    <asp:Panel ID="pnButoanePrincipale" runat="server" CssClass="butoane">
                        <asp:Button CssClass="buton" ID="btAdaugaUtilizator" runat="server" Text="adauga un utilizator"
                            OnClick="AdaugaModificaUtilizator" ValidationGroup="GrupValidareUtilizatori"
                            Visible="false" />
                 
                        <asp:Button ID="btAdaugaModificaUtilizator" runat="server" CssClass="buton" OnClick="AdaugaModificaUtilizator"
                            Text="modifica utilizatorul"  Visible="false" />
                        <asp:Button ID="btModificaArataListaUtilizatori" runat="server" CssClass="buton"
                            OnClick="ModificaArataListaUtilizatori" Text="lista utilizatorilor" Visible="false" />
                        <asp:Button ID="btAdaugaArataListaUtilizatori" runat="server" CssClass="buton" OnClick="AdaugaArataListaUtilizatori"
                            Text="lista utilizatorilor" Visible="false" />
                        
                    </asp:Panel>
                    <asp:Panel ID="Panel1" CssClass="ascunsa" runat="server">
                        <!-- //////////// VALIDATOARE  \\\\\\\\\\\\\\\\\\ -->
                        <asp:RequiredFieldValidator ID="valLogin" ControlToValidate="tbLogin" ValidationGroup="GrupValidareUtilizatori"
                            runat="server" ErrorMessage="Numele de utilizator este obligatoriu! (minim 8 caractere)"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="valParola" ControlToValidate="tbParola" ValidationGroup="GrupValidareUtilizatori"
                            runat="server" ErrorMessage="Parola este obligatorie! (minim 8 caractere)"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="valConfirmareParola" ControlToValidate="tbConfirmaParola"
                            ValidationGroup="GrupValidareUtilizatori" runat="server" ErrorMessage="Confirmati parola!"></asp:RequiredFieldValidator>
                        <asp:CustomValidator ID="valCustom" runat="server" ErrorMessage="" ValidationGroup="GrupValidareUtilizatori"></asp:CustomValidator>
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
            <!-- adaugam utilizatori -->
            <asp:Panel ID="pnModificaDrepturiUtilizator" CssClass="panel_general" runat="server"
                Visible="false">
                <asp:Panel CssClass="adauga" runat="server">
                    <asp:Panel runat="server">
                        <h2>
                            <asp:Label ID="Label1" runat="server" Text="Label">MODIFICĂ DREPTURILE UTILIZATORULUI</asp:Label>
                            <asp:Label ID="lblNumeUtilizator" runat="server" Text="nume itilizator"></asp:Label></h2>
                    </asp:Panel>
                    <asp:Panel ID="pnGridView" runat="server">
                        <asp:GridView ID="gvDrepturiUtilizatori" runat="server" CssClass="tabela" AutoGenerateColumns="False"
                            DataSourceID="SqlListaPagini" PageSize="100">
                            <Columns>
                                <asp:TemplateField HeaderText="pagina">
                                    <ItemTemplate>
                                        <asp:Label ID="paginaDenumire" runat="server" Text='<%# Bind("paginaDenumire") %>'
                                            Visible="true"></asp:Label>
                                        <asp:Label ID="paginaId" runat="server" Text='<%# Bind("paginaId") %>' Visible="true"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Citire">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="cbCitire" runat="server" Enabled="True" Checked="true" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Stergere">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="cbStergere" runat="server" Enabled="True" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Creare">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="cbCreare" runat="server" Enabled="True" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Modificare">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="cbModificare" runat="server" Enabled="True" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Tiparire">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="cbTiparire" runat="server" Enabled="True" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlListaPagini" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016 %>"
                            SelectCommand="SELECT  [paginaId], [paginaDenumire] FROM [pagini]"></asp:SqlDataSource>
                    </asp:Panel>
                    <asp:Panel ID="Panel6" runat="server" CssClass="butoane">
                        <asp:Button ID="btModificaDrepturiUtilizator" runat="server" CssClass="buton" OnClick="ModificaDrepturiUtilizator"
                            Text="modifică drepturi utilizator" Visible="true" />
                        <asp:Button ID="btModificaDrepturiArataListaUtilizatori" runat="server" CssClass="buton"
                            OnClick="ModificaArataListaUtilizatori" Text="lista utilizatorilor" Visible="true" />
                    </asp:Panel>
                    <asp:Panel ID="Panel7" CssClass="ascunsa" runat="server">
                        <!-- //////////// VALIDATOARE  \\\\\\\\\\\\\\\\\\ -->
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="tbLogin"
                            ValidationGroup="GrupValidareUtilizatori" runat="server" ErrorMessage="Numele de utilizator este obligatoriu! (minim 8 caractere)"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="tbParola"
                            ValidationGroup="GrupValidareUtilizatori" runat="server" ErrorMessage="Parola este obligatorie! (minim 8 caractere)"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="tbConfirmaParola"
                            ValidationGroup="GrupValidareUtilizatori" runat="server" ErrorMessage="Confirmati parola!"></asp:RequiredFieldValidator>
                        <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="" ValidationGroup="GrupValidareUtilizatori"></asp:CustomValidator>
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="popup" runat="server" Visible="false">
                <asp:Panel ID="pnlPopUpGospodarii" runat="server" CssClass="popupBack" Visible="true">
                </asp:Panel>
                <asp:Panel ID="pnIp" runat="server" CssClass="panel_general popupPanel" Visible="true">
                    <asp:Panel ID="pnlIp1" runat="server" CssClass="cauta">
                        <asp:Label ID="lblIp" runat="server" Text="IP:"></asp:Label>
                        <asp:TextBox ID="tbIp" Width="200px" runat="server"></asp:TextBox>
                    </asp:Panel>
                    <asp:Panel ID="Panel2" CssClass="butoane" runat="server">
                        <asp:Button CssClass="buton" ID="btStergeDeblocheaza" runat="server" Text="deblochează"
                            Visible="true" OnClick="btStergeDeblocheaza_Click" />
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
