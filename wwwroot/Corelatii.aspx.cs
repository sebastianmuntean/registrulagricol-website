﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
/// <summary>
/// Adaugare capitol
/// Creata la:                  ??.02.2011
/// Autor:                      SM
/// Ultima                      actualizare: 01.04.2011
/// Autor:                      SM - adaugare in log
/// </summary> 
public partial class Corelatii : System.Web.UI.Page
{
    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
         DBConnections connection = new DBConnections(Convert.ToInt16(Session["SESan"]));

         SqlCorelatii.ConnectionString = connection.Create();
        SqlCapitole.ConnectionString = connection.Create();
        SqlDataSource1.ConnectionString = connection.Create();
        SqlRanduri.ConnectionString = connection.Create();

        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "corelatii", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
        }
    }
    protected void SqlCorelatii_Init(object sender, EventArgs e)
    {
 
    }
    protected void gvCorelatii_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(gvCorelatii, e, this);
    }
    protected void gvCorelatii_DataBound(object sender, EventArgs e)
    {
        // luam denumirea unitatii
        SqlCorelatii.SelectCommand = @"SELECT * FROM [corelatii] WHERE (" +
                  "( ([camp1] LIKE '*" + tbCCapitol.Text + "%') OR ([camp2] LIKE '%*" + tbCCapitol.Text + "%') ) AND " +
                  "( ([camp1] LIKE '%#" + tbCRand.Text + "%') OR ([camp2] LIKE '%#" + tbCRand.Text + "%') ) AND " +
                  "( ([camp1] LIKE '%;" + tbCColoana.Text + "%') OR ([camp2] LIKE '%;" + tbCColoana.Text + "%') )" +
                  ") ORDER BY camp1";   
        for (int i = 0; i < gvCorelatii.Rows.Count; i++)
        {
            char[] vDelimitatoare = {'^','*','#',';' };
            if (gvCorelatii.Rows[i].RowType == DataControlRowType.DataRow)
            {
                try
                {
                    gvCorelatii.Rows[i].Cells[1].Text = clsCorelatii.AfiseazaStringCorelatie(gvCorelatii.Rows[i].Cells[1].Text, Convert.ToInt16(Session["SESan"]));
                    gvCorelatii.Rows[i].Cells[2].Text = clsCorelatii.AfiseazaSemnCorelatie(gvCorelatii.Rows[i].Cells[2].Text);
                    gvCorelatii.Rows[i].Cells[3].Text = clsCorelatii.AfiseazaStringCorelatie(gvCorelatii.Rows[i].Cells[3].Text, Convert.ToInt16(Session["SESan"]));
                }
                catch { }
            }
        }
    }

    protected void ModificaArataListaCorelatii(object sender, EventArgs e)
    {
        pnAdaugaCorelatie.Visible = false;
        pnListaCorelatii.Visible = true;
        btStergeCorelatie.Visible = true;
        btModificaCorelatie.Visible = true;
    }
    protected void AdaugaArataListaCorelatii(object sender, EventArgs e)
    {
        pnAdaugaCorelatie.Visible = false;
        pnListaCorelatii.Visible = true;
        btStergeCorelatie.Visible = false;
        btModificaCorelatie.Visible = false;
        gvCorelatii.SelectedIndex = -1;
    }
    protected void StergeCorelația(object sender, EventArgs e)
    {
        string vCorelatieId = gvCorelatii.SelectedValue.ToString();
        string[] vCampuri = {"camp1","camp2","semn" };
        List<string> vValori = ManipuleazaBD.fRezultaUnString("SELECT * FROM corelatii WHERE corelatieId ='" + vCorelatieId + "'", vCampuri, Convert.ToInt16(Session["SESan"]));
        ManipuleazaBD.fManipuleazaBD("DELETE FROM [corelatii] WHERE [corelatieId] = '" + vCorelatieId + "'", Convert.ToInt16(Session["SESan"]));
        ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "corelatii", "stergere corelatia: " + vValori[0] + vValori[1] + vValori[2], "id: " + vCorelatieId, Convert.ToInt64(Session["SESgospodarieId"]), 4);
        gvCorelatii.DataBind();
        gvCorelatii.SelectedIndex = -1;
        btStergeCorelatie.Visible = false;
        btModificaCorelatie.Visible = false;
    }
    protected int GasesteRandul(int pValoare)
    { 
        int vRand = -1;
        for (int i = 0; i < gvCorelatii.Rows.Count; i++)
        { 
            if (gvCorelatii.Rows[i].Cells[0].Text == pValoare.ToString())
            {
                vRand = i;
                break;
            }
        }
        return vRand;
    }
    protected void ArataAdaugaCorelatie(object sender, EventArgs e)
    {
        pnAdaugaCorelatie.Visible = true;
        pnListaCorelatii.Visible = false;
        // daca e adaugare
        if (((Button)sender).ID.ToString() == "btListaAdaugaCorelatie")
        {
            ddCapitolCamp1.SelectedValue = ManipuleazaBD.fRezultaUnString("SELECT TOP(1) capitol FROM sabloaneCapitole ORDER BY capitol", "capitol", Convert.ToInt16(Session["SESan"]));
            ddSemn.SelectedValue = ddSemn.Items[0].Value;
            tbRandCamp1.Text = "";
            tbColoanaCamp1.Text = "";
            btAdaugaCorelatie.Visible = true;
            btAdaugaModificaCorelatie.Visible = false;
            btModificaArataListaCorelatii.Visible = false;
            btAdaugaArataListaCorelatii.Visible = true;
            for (int i = 0; i < gvDupaEgal.Rows.Count; i++)
            {
                if (gvDupaEgal.Rows[i].RowType == DataControlRowType.DataRow)
                {
                    ((TextBox)gvDupaEgal.Rows[i].Cells[3].FindControl("tbRandCamp2")).Text = "";
                    ((TextBox)gvDupaEgal.Rows[i].Cells[4].FindControl("tbColoanaCamp2")).Text = "";
                    ((DropDownList)gvDupaEgal.Rows[i].Cells[1].FindControl("ddOperatie")).SelectedValue = "+";
                    try { ((DropDownList)gvDupaEgal.Rows[i].Cells[2].FindControl("ddCapitolCamp2")).SelectedValue = ManipuleazaBD.fRezultaUnString("SELECT TOP(1) capitol FROM sabloaneCapitole ORDER BY capitol", "capitol", Convert.ToInt16(Session["SESan"])); }
                    catch { }
                }
            }
            for (int i = 0; i < gvInainteEgal.Rows.Count; i++)
            {
                if (gvInainteEgal.Rows[i].RowType == DataControlRowType.DataRow)
                {
                    ((TextBox)gvInainteEgal.Rows[i].Cells[3].FindControl("tbRandCamp2")).Text = "";
                    ((TextBox)gvInainteEgal.Rows[i].Cells[4].FindControl("tbColoanaCamp2")).Text = "";
                    ((DropDownList)gvInainteEgal.Rows[i].Cells[1].FindControl("ddOperatie")).SelectedValue = "+";
                    try { ((DropDownList)gvInainteEgal.Rows[i].Cells[2].FindControl("ddCapitolCamp2")).SelectedValue = ManipuleazaBD.fRezultaUnString("SELECT TOP(1) capitol FROM sabloaneCapitole ORDER BY capitol", "capitol", Convert.ToInt16(Session["SESan"])); }
                    catch { }
                }
            }
        }
            // daca e modificare
        else
        {
            btAdaugaCorelatie.Visible = false;
            btAdaugaModificaCorelatie.Visible = true;
            btModificaArataListaCorelatii.Visible = true;
            btAdaugaArataListaCorelatii.Visible = false;
            // luam valoarea camp2 
            string vCorelatieId = gvCorelatii.SelectedValue.ToString();
            string vCamp1 = ManipuleazaBD.fRezultaUnString("SELECT camp1 FROM corelatii WHERE corelatieId = '" + vCorelatieId.ToString() + "'", "camp1", Convert.ToInt16(Session["SESan"]));
            string vCamp2 = ManipuleazaBD.fRezultaUnString("SELECT camp2 FROM corelatii WHERE corelatieId = '" + vCorelatieId.ToString() + "'", "camp2", Convert.ToInt16(Session["SESan"]));
            string vCamp3 = ManipuleazaBD.fRezultaUnString("SELECT semn FROM corelatii WHERE corelatieId = '" + vCorelatieId.ToString() + "'", "semn", Convert.ToInt16(Session["SESan"]));
            ddCapitolCamp1.SelectedValue = clsCorelatii.DesfaCampuriInElemente(vCamp1)[2];
            ddSemn.SelectedValue = vCamp3;
            tbRandCamp1.Text = clsCorelatii.DesfaCampuriInElemente(vCamp1)[3];
            tbColoanaCamp1.Text = clsCorelatii.DesfaCampuriInElemente(vCamp1)[4];


            string[] vElemente = {};
            for (int i = 0; i < gvInainteEgal.Rows.Count; i++)
            {
                // randuri populate cu date existente
                if (i < clsCorelatii.DesfaCorelatieInCampuri(vCamp1).Length)
                {
                    if (clsCorelatii.DesfaCorelatieInCampuri(vCamp1)[i] != "")
                    {
                        vElemente = clsCorelatii.DesfaCampuriInElemente(clsCorelatii.DesfaCorelatieInCampuri(vCamp1)[i]);
                        ((TextBox)gvInainteEgal.Rows[i].Cells[3].FindControl("tbRandCamp2")).Text = vElemente[3];
                        ((TextBox)gvInainteEgal.Rows[i].Cells[4].FindControl("tbColoanaCamp2")).Text = vElemente[4];
                        ((DropDownList)gvInainteEgal.Rows[i].Cells[1].FindControl("ddOperatie")).SelectedValue = vElemente[1];
                        try { ((DropDownList)gvInainteEgal.Rows[i].Cells[2].FindControl("ddCapitolCamp2")).SelectedValue = vElemente[2]; }
                        catch { }
                    }
                }
                    // restul randurilor goale
                else if (gvInainteEgal.Rows[i].RowType == DataControlRowType.DataRow)
                {
                    ((TextBox)gvInainteEgal.Rows[i].Cells[3].FindControl("tbRandCamp2")).Text = "";
                    ((TextBox)gvInainteEgal.Rows[i].Cells[4].FindControl("tbColoanaCamp2")).Text = "";
                    ((DropDownList)gvInainteEgal.Rows[i].Cells[1].FindControl("ddOperatie")).SelectedValue = "+";
                    try { ((DropDownList)gvInainteEgal.Rows[i].Cells[2].FindControl("ddCapitolCamp2")).SelectedValue = ManipuleazaBD.fRezultaUnString("SELECT TOP(1) capitol FROM sabloaneCapitole ORDER BY capitol", "capitol", Convert.ToInt16(Session["SESan"])); }
                    catch { }
                }
            }
            for (int i = 0; i < gvDupaEgal.Rows.Count; i++)
            {
                // randuri populate cu date existente
                if (i < clsCorelatii.DesfaCorelatieInCampuri(vCamp2).Length)
                {
                    if (clsCorelatii.DesfaCorelatieInCampuri(vCamp2)[i] != "")
                    {
                        vElemente = clsCorelatii.DesfaCampuriInElemente(clsCorelatii.DesfaCorelatieInCampuri(vCamp2)[i]);
                        ((TextBox)gvDupaEgal.Rows[i].Cells[3].FindControl("tbRandCamp2")).Text = vElemente[3];
                        ((TextBox)gvDupaEgal.Rows[i].Cells[4].FindControl("tbColoanaCamp2")).Text = vElemente[4];
                        ((DropDownList)gvDupaEgal.Rows[i].Cells[1].FindControl("ddOperatie")).SelectedValue = vElemente[1];
                        try { ((DropDownList)gvDupaEgal.Rows[i].Cells[2].FindControl("ddCapitolCamp2")).SelectedValue = vElemente[2]; }
                        catch { }
                    }
                }
                // restul randurilor goale
                else if (gvDupaEgal.Rows[i].RowType == DataControlRowType.DataRow)
                {
                    ((TextBox)gvDupaEgal.Rows[i].Cells[3].FindControl("tbRandCamp2")).Text = "";
                    ((TextBox)gvDupaEgal.Rows[i].Cells[4].FindControl("tbColoanaCamp2")).Text = "";
                    ((DropDownList)gvDupaEgal.Rows[i].Cells[1].FindControl("ddOperatie")).SelectedValue = "+";
                    try { ((DropDownList)gvDupaEgal.Rows[i].Cells[2].FindControl("ddCapitolCamp2")).SelectedValue = ManipuleazaBD.fRezultaUnString("SELECT TOP(1) capitol FROM sabloaneCapitole ORDER BY capitol", "capitol", Convert.ToInt16(Session["SESan"])); }
                    catch { }
                }
            }
        }

    }
    protected string CreeazaStringCorelatieCamp2()
    {
        // formam stringul campului doi
        // cautam pana gasim o Operatie = final
        string vStringCorelatie = "";
        for (int i = 0; i < gvDupaEgal.Rows.Count; i++)
        {
            if (gvDupaEgal.Rows[i].RowType == DataControlRowType.DataRow)
            {
                try
                {
                    if (((DropDownList)gvDupaEgal.Rows[i].Cells[1].FindControl("ddOperatie")).SelectedValue != "final" &&  ((TextBox)gvDupaEgal.Rows[i].Cells[3].FindControl("tbRandCamp2")).Text != "" && ((TextBox)gvDupaEgal.Rows[i].Cells[4].FindControl("tbColoanaCamp2")).Text != "")
                    {
                        vStringCorelatie += "^" + ((DropDownList)gvDupaEgal.Rows[i].Cells[1].FindControl("ddOperatie")).SelectedValue; vStringCorelatie += "*" + ((DropDownList)gvDupaEgal.Rows[i].Cells[2].FindControl("ddCapitolCamp2")).SelectedValue + "#" + ((TextBox)gvDupaEgal.Rows[i].Cells[3].FindControl("tbRandCamp2")).Text + ";" + ((TextBox)gvDupaEgal.Rows[i].Cells[4].FindControl("tbColoanaCamp2")).Text + "!" + ((DropDownList)gvDupaEgal.Rows[i].Cells[2].FindControl("ddUMCamp2")).SelectedValue + ".";      
                    }
                    else { break; }
                        
                }
                catch { }
            }
        }
        return vStringCorelatie;
    }
    protected string CreeazaStringCorelatieCamp2(int vTipCamp)
    {
        // formam stringul campului doi
        // cautam pana gasim o Operatie = final
        GridView vGv = new GridView();
        if (vTipCamp == 2)  vGv = gvDupaEgal;
        else vGv = gvInainteEgal;

        string vStringCorelatie = "";
        for (int i = 0; i < vGv.Rows.Count; i++)
        {
            if (vGv.Rows[i].RowType == DataControlRowType.DataRow)
            {
                try
                {
                    if (((DropDownList)vGv.Rows[i].Cells[1].FindControl("ddOperatie")).SelectedValue != "final" && ((TextBox)vGv.Rows[i].Cells[3].FindControl("tbRandCamp2")).Text != "" && ((TextBox)gvDupaEgal.Rows[i].Cells[4].FindControl("tbColoanaCamp2")).Text != "")
                    {
                        vStringCorelatie += "^" + ((DropDownList)vGv.Rows[i].Cells[1].FindControl("ddOperatie")).SelectedValue; vStringCorelatie += "*" + ((DropDownList)vGv.Rows[i].Cells[2].FindControl("ddCapitolCamp2")).SelectedValue + "#" + ((TextBox)vGv.Rows[i].Cells[3].FindControl("tbRandCamp2")).Text + ";" + ((TextBox)vGv.Rows[i].Cells[4].FindControl("tbColoanaCamp2")).Text + "!" + ((DropDownList)vGv.Rows[i].Cells[2].FindControl("ddUMCamp2")).SelectedValue + ".";
                    }
                    else { break; }
                }
                catch { }
            }
        }
        return vStringCorelatie;
    }
    protected string[] CreeazaStringCorelatie()
    {
       // string[] vStringCorelatie = { "^+" + "*" + ddCapitolCamp1.SelectedValue + "#" + tbRandCamp1.Text + ";" + tbColoanaCamp1.Text + "!"+ddUMCamp1.SelectedValue + ".",   CreeazaStringCorelatieCamp2() };
        string[] vStringCorelatie = { CreeazaStringCorelatieCamp2(1), CreeazaStringCorelatieCamp2(2) };
        return vStringCorelatie;
    }
    protected void AdaugaModificaCorelatie(object sender, EventArgs e)
    {
        string vInterogare = "";
        // extragem din stringurile create camp1 si camp2
        string[] vCampuriTablou = CreeazaStringCorelatie();
        string vCamp1 = vCampuriTablou[0];
        string vCamp2 = vCampuriTablou[1];
        int vValoareSelectata = Convert.ToInt32(gvCorelatii.SelectedValue);
        if (((Button)sender).ID.ToString() == "btAdaugaCorelatie")
            {
                vInterogare = "INSERT INTO corelatii (camp1,camp2, semn) VALUES ('" + vCamp1 + "','" + vCamp2 + "', '"+ddSemn.SelectedValue+"')";
                ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "corelatii", "adauga corelatia: " + vCamp1 + ddSemn.SelectedValue + vCamp2, "", Convert.ToInt64(Session["SESgospodarieId"]), 2);
                int vUtilizatorId=-1;
                ManipuleazaBD.fManipuleazaBDCuId(vInterogare, out vUtilizatorId, Convert.ToInt16(Session["SESan"]));
                gvCorelatii.SelectedIndex = -1;
                pnAdaugaCorelatie.Visible = false;
                pnListaCorelatii.Visible = true;
                btModificaCorelatie.Visible = false;
                btStergeCorelatie.Visible = false;
            }
            else
            {
                vInterogare = @"UPDATE corelatii SET camp1 ='" + vCamp1 + "', camp2 ='" + vCamp2 + "', semn ='"+ddSemn.SelectedValue+"'  WHERE corelatieId ='" + gvCorelatii.SelectedValue.ToString() + "'" ;
                ManipuleazaBD.fManipuleazaBD(vInterogare, Convert.ToInt16(Session["SESan"]));
                ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "corelatii", "modificare corelatia: " + vCamp1 + ddSemn.SelectedValue + vCamp2 + ", id: " + gvCorelatii.SelectedValue.ToString(), "", Convert.ToInt64(Session["SESgospodarieId"]), 3);
                pnAdaugaCorelatie.Visible = false;
                pnListaCorelatii.Visible = true;
                btModificaCorelatie.Visible = true;
                btStergeCorelatie.Visible = true;
                gvCorelatii.DataBind();
            }
        gvCorelatii.DataBind();
        if (((Button)sender).ID.ToString() == "btAdaugaCorelatie") { gvCorelatii.SelectedIndex = -1; }
        else
        {
            gvCorelatii.SelectedIndex = GasesteRandul(vValoareSelectata);
        }
    }
    protected void AdaugaModificaCorelatie2(object sender, EventArgs e)
    {
        string vInterogare = "";
        // extragem din stringurile create camp1 si camp2
        string[] vCampuriTablou = CreeazaStringCorelatie();
        string vCamp1 = vCampuriTablou[0];
        string vCamp2 = vCampuriTablou[1];
        int vValoareSelectata = Convert.ToInt32(gvCorelatii.SelectedValue);
        if (((Button)sender).ID.ToString() == "btAdaugaCorelatie")
        {
            vInterogare = "INSERT INTO corelatii (camp1,camp2, semn) VALUES ('" + vCamp1 + "','" + vCamp2 + "', '" + ddSemn.SelectedValue + "')";
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "corelatii", "adauga corelatia: " + vCamp1 + ddSemn.SelectedValue + vCamp2, "", Convert.ToInt64(Session["SESgospodarieId"]), 2);
            int vUtilizatorId = -1;
            ManipuleazaBD.fManipuleazaBDCuId(vInterogare, out vUtilizatorId, Convert.ToInt16(Session["SESan"]));
            gvCorelatii.SelectedIndex = -1;
            pnAdaugaCorelatie.Visible = false;
            pnListaCorelatii.Visible = true;
            btModificaCorelatie.Visible = false;
            btStergeCorelatie.Visible = false;
        }
        else
        {
            vInterogare = @"UPDATE corelatii SET camp1 ='" + vCamp1 + "', camp2 ='" + vCamp2 + "', semn ='" + ddSemn.SelectedValue + "'  WHERE corelatieId ='" + gvCorelatii.SelectedValue.ToString() + "'";
            ManipuleazaBD.fManipuleazaBD(vInterogare, Convert.ToInt16(Session["SESan"]));
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "corelatii", "modificare corelatia: " + vCamp1 + ddSemn.SelectedValue + vCamp2 + ", id: " + gvCorelatii.SelectedValue.ToString(), "", Convert.ToInt64(Session["SESgospodarieId"]), 3);
            pnAdaugaCorelatie.Visible = false;
            pnListaCorelatii.Visible = true;
            btModificaCorelatie.Visible = true;
            btStergeCorelatie.Visible = true;
            gvCorelatii.DataBind();
        }
        gvCorelatii.DataBind();
        if (((Button)sender).ID.ToString() == "btAdaugaCorelatie") { gvCorelatii.SelectedIndex = -1; }
        else
        {
            gvCorelatii.SelectedIndex = GasesteRandul(vValoareSelectata);
        }
    }
    protected void gvCorelatii_SelectedIndexChanged(object sender, EventArgs e)
    {
        // actiuni la selectare rand; aratam butoanele de sterge si modifica
        btAdaugaCorelatie.Visible = true;
        btModificaCorelatie.Visible = true;
        btStergeCorelatie.Visible = true;
        gvCorelatii.DataBind();
    }
    protected void ddOperatie_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gvDupaEgal_OnInit(object sender, EventArgs e)
    {
        for (int i = 0; i < 10; i++)
        {
           
        }
    }
    protected void gvDupaEgal_DataBound(object sender, EventArgs e)
    {
    }
    protected void AdaugaParcele(object sender, EventArgs e)
    {
        ManipuleazaBD.fManipuleazaBD("INSERT INTO sabloaneCapitole (capitol, an) VALUES ('paduri','1970')", Convert.ToInt16(Session["SESan"])); ManipuleazaBD.fManipuleazaBD("INSERT INTO sabloaneCapitole (capitol, an) VALUES ('parcele','1970')", Convert.ToInt16(Session["SESan"]));
    }
}
