﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adevEditareAvansata : System.Web.UI.Page
{
    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        // umplem textboxul de text

        if (!IsPostBack)
        {
            tbTextAvansat.Text = "<link href=\"css/master.css\" rel=\"stylesheet\" type=\"text/css\">";
            tbTextAvansat.Text += ManipuleazaBD.fRezultaUnString("SELECT textTiparit FROM adeverinte WHERE adeverintaId='" + Request.QueryString["id"].ToString() + "'", "textTiparit", Convert.ToInt16(Session["SESan"]));
        }
        HttpCookie vCookie = Request.Cookies["COOKTNTExpira"];
        if (vCookie == null)
        {
          //  Response.Redirect("Login.aspx");
        }
        else
        {
                vCookie["COOKexpira"] = DateTime.Now.AddMinutes(130).ToString();
                vCookie.Expires = DateTime.Now.AddMinutes(2005);
        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {

    }
    protected void btAnuleazaSalvareaAvansata_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/adevAdaugaAdeverinte.aspx?");
    }
    protected void btSalveazaAvansata_Click(object sender, EventArgs e)
    {
        ManipuleazaBD.fManipuleazaBD("UPDATE [adeverinte] SET [textTiparit] = N'" + tbTextAvansat.Text.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş") + "'  WHERE [adeverintaId] = '" + Request.QueryString["id"].ToString() + "'", Convert.ToInt16(Session["SESan"]));
        ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "adeverinte", "editarea avansata adeverinta id:" + Request.QueryString["id"].ToString(), "", Convert.ToInt64(Session["SESgospodarieId"]), 3);
        ResponseHelper.Redirect("~/adevTiparesteAdeverinta.aspx?id=" + Request.QueryString["id"].ToString() + "&p=" + rbPePagina.SelectedValue, "_blank", "height=600,width=900,status=no,toolbar=no,menubar=no,location=no, scrollbars=1");
    }
}
