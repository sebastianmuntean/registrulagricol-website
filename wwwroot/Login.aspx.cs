using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class _Default : System.Web.UI.Page
{
    protected void verificare_licenta()
    {
        string calea = Server.MapPath("temp");
        System.IO.DirectoryInfo inf = System.IO.Directory.GetParent(calea);
        calea = inf.FullName;
        //WebCamService.FtpClient ftpClient = null;
        FTPS.Client.FTPSClient ftpClient = new FTPS.Client.FTPSClient();
        try
        {

            ftpClient.Connect("ftp.tntcomputers.ro", new System.Net.NetworkCredential("sediu", "Tartareanu13?"), FTPS.Client.ESSLSupportMode.ControlAndDataChannelsRequested);
            ftpClient.SetCurrentDirectory("Protectie");
            //ftpClient = new WebCamService.FtpClient("ftp.tntcomputers.ro", "sediu", "Tartareanu13?", 600, 21);
            //ftpClient.Login();
            //ftpClient.ChangeDir("Protectie");
            //string[] lista = ftpClient.GetFileList();
            //string[] lista=ftpClient.getf
            /*foreach (string fisier in lista)
            {
                if (fisier != "")
                    ftpClient.Download(fisier, calea + "\\" + fisier, false);
            }*/
            ftpClient.GetFile("update.tnt", calea + "\\" + "update.tnt");
            ftpClient.Close();

            //System.IO.FileStream fs = new System.IO.FileStream(calea + "\\activare_taxe.txt", System.IO.FileAccess.Read, System.IO.FileMode.Open);
            System.IO.StreamReader sr = null;
            DateTime data = Convert.ToDateTime("01.01.2009");
            string linie = "", mesaj = "";
            int perioada = 1;
            try
            {
                sr = new System.IO.StreamReader(calea + "\\update.tnt");

                string cui = tbUnitateId.Text;

                while (!sr.EndOfStream)
                {
                    linie = sr.ReadLine();
                    string[] cuvinte = linie.Split(';');
                    mesaj = "";
                    if (cuvinte[0] == cui)
                    {
                        linie = linie.Replace(cui, "");
                        //linie = linie.Replace(cuvinte[cuvinte.Length - 2], "");
                        mesaj = cuvinte[2];
                        data = DateTime.Now.Date;
                        if (cuvinte[1] == "1")
                            data = new DateTime(DateTime.Now.Year, 1, 1).Date;

                        break;
                    }
                }
                sr.Close();
                System.IO.File.Delete(calea + "\\update.tnt");
                System.IO.File.Delete(calea + "\\update1.tnt");
            }
            catch
            {
                sr.Close();
                System.IO.File.Delete(calea + "\\update.tnt");
                System.IO.File.Delete(calea + "\\update1.tnt");
            }
            Session["SESmesaj"] = mesaj;
        }
        catch
        {
        }
    }

    //protected override void OnPreInit(EventArgs e)
    //{
    //    base.OnPreInit(e);

    //    if (!IsPostBack)
    //    {
    //        if (!Request.IsSecureConnection)
    //        {
    //            string redirectUrl = Request.Url.ToString().Replace("http:", "https:");
    //            Response.Redirect(redirectUrl, false);
    //            HttpContext.Current.ApplicationInstance.CompleteRequest();
    //        }
    //    }
    //}

    protected void Page_Load(object sender, EventArgs e)
    {
        HttpCookie vCookie = Request.Cookies["COOKTNT"];
        vCookie = CriptareCookie.DecodeCookie(vCookie);
        if (vCookie != null)
        {
            if (Convert.ToDateTime(vCookie["COOKexpira"]) < DateTime.Now)
            {
                valCustom.IsValid = false;
                valCustom.ErrorMessage = "Aţi avut mai mult de 10 minute de inactivitate! Vă invităm să vă conectaţi din nou. ";
            }
        }
    }

    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    protected void lkbRo_Click(object sender, EventArgs e)
    {
        Session["SESlimba"] = "ro-RO";
        HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        vCookie["COOKlimba"] = "ro-RO";
        Response.Cookies.Add(vCookie);

        //Response.Redirect(Request.Url.AbsolutePath);
        Response.Redirect("~/login.aspx");
    }
    protected void lbkHu_Click(object sender, EventArgs e)
    {
        Session["SESlimba"] = "hu-HU";
        HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        vCookie["COOKlimba"] = "hu-HU";
        Response.Cookies.Add(vCookie);

        //Response.Redirect(Request.Url.AbsolutePath);
        Response.Redirect("~/login.aspx");
    }
    protected DateTime UltimaReusita()
    {
        DateTime vUltima = DateTime.Now.AddMinutes(-60);
        string vUtilizatorIp = HttpContext.Current.Request.UserHostAddress;
        try
        {
            vUltima = Convert.ToDateTime(ManipuleazaBD.fRezultaUnString("SELECT MAX(CONVERT(DATETIME, CONVERT(NVARCHAR, data) + ' ' + CONVERT(NVARCHAR,ora) + ':' + CONVERT(NVARCHAR,min) + ':00')) as vUltima FROM logOperatii WHERE valoareVeche = 'REUSIT' AND coalesce(utilizatorIp,'') = '" + vUtilizatorIp + "'", "vUltima", Convert.ToInt16(DateTime.Now.Year)));
        }
        catch
        {
            vUltima = DateTime.Now.AddDays(-10);
        }
        return vUltima;
    }

    protected bool VerificareFursec()
    {
        // verificam fursecul; daca are mai mult de cinci coduri si nu corespunde niciunul ii afisam mesaj sa sune 
        HttpCookie vCookFursec = Request.Cookies["COOKfursec"];
        vCookFursec = CriptareCookie.DecodeCookie(vCookFursec);
        // daca nu exista cautam in tabela utilizatori codurile
        string vUnitateIdFursec = ManipuleazaBD.fRezultaUnString("SELECT unitateId FROM unitati WHERE unitateCodFiscal ='" + tbUnitateId.Text + "'", "unitateId", Convert.ToInt16(DateTime.Now.Year));
        string[] vSeparatoare = { "############" };
        string vCoduriFursecIntreg = ManipuleazaBD.fRezultaUnString("SELECT utilizatorFursec FROM utilizatori WHERE utilizatorLogin = '" + tbUtilizatorNume.Text + "' AND unitateId = '" + vUnitateIdFursec + "'", "utilizatorFursec", Convert.ToInt16(DateTime.Now.Year));
        string[] vCoduriFursec = vCoduriFursecIntreg.Split(vSeparatoare, System.StringSplitOptions.RemoveEmptyEntries);
        string vUtilizatorIdFursec = ManipuleazaBD.fRezultaUnString("SELECT utilizatorId FROM utilizatori WHERE utilizatorLogin = '" + tbUtilizatorNume.Text + "' AND unitateId = '" + vUnitateIdFursec + "'", "utilizatorId", Convert.ToInt16(DateTime.Now.Year));
        // daca e administratorul tnt nu tinem cont
        if (vUtilizatorIdFursec == "52") return true;
        string vFursec = "";
        int vContorFursec = 0;
        if (vCookFursec != null)
        {
            // daca are ceva coduri, verificam coduri sa corespunda cu cookiul
            for (int i = 0; i < vCoduriFursec.Length; i++)
            {
                vFursec = CriptareCookie.DeCripteazaFursec(vCookFursec["COOKcod"]);
                // daca vreunul corespunde il lasam sa treaca mai departe
                if (vFursec == vCoduriFursec[i])
                {
                    vContorFursec++;
                    break;
                }
            }
        }
        // daca nu corespunde niciun cod sau nu are cookie
        if (vContorFursec == 0 || vCookFursec == null)
        {
            if (vCoduriFursec.Length < 5)
            // daca are mai putin de cinci coduri, mai adaugam un cod si rescriem cookie-ul
            {
                vCookFursec = new HttpCookie("COOKfursec");
                vFursec = CriptareCookie.GenereazaFursec();
                // scriem si in tabela codul generat
                ManipuleazaBD.fManipuleazaBD("UPDATE utilizatori SET utilizatorFursec = '############" + vFursec + vCoduriFursecIntreg + "' WHERE utilizatorId ='" + vUtilizatorIdFursec + "' ", Convert.ToInt16(DateTime.Now.Year));
                vFursec = CriptareCookie.CripteazaFursec(vFursec);
                //HttpCookie vCookFursec = new HttpCookie("COOKfursec");
                vCookFursec["COOKcod"] = vFursec;
                vCookFursec = CriptareCookie.EncodeCookie(vCookFursec);
                vCookFursec.Expires = DateTime.Now.AddYears(1);
                Response.Cookies.Add(vCookFursec);
            }
            else if (vCoduriFursec.Length >= 5)
            {
                // are deja 5 coduri si nu corespunde niciunul--> mesaj eroare, sa sune
                valCustom.ErrorMessage = "Pentru utilizatorul " + tbUtilizatorNume.Text + " au fost constatate probleme la drepturile de utilizare. Va rugăm să contactaţi administratorul sistemului la tel. 0369 101 101.";
                valCustom.IsValid = false;
                return false;
            }
        }
        return true;
    }
    protected void btAutentificare_Click(object sender, EventArgs e)
    {
        string vUtilizatorIp = HttpContext.Current.Request.UserHostAddress;
        // blocam daca are trei incercari nereusite pentru un utilizator de pe un anumit ip intr-o ora
        string vUnitateCIF = tbUnitateId.Text;
        string vParola = Criptare.Encrypt(tbParola.Text);
        string vLogin = tbUtilizatorNume.Text;
        int vUtilizatorId = 0;
        // verificam daca e blocat ip-ul in ultima ora, daca e blocat nu mai verificam altceva
        string vBlocat = ManipuleazaBD.fRezultaUnString("SELECT COUNT(*) as blocat FROM logOperatii WHERE CONVERT(DATETIME, CONVERT(NVARCHAR, data) + ' ' + CONVERT(NVARCHAR,ora) + ':' + CONVERT(NVARCHAR,min) + ':00') <= CONVERT(datetime, '" + DateTime.Now + "',104) AND CONVERT(DATETIME, CONVERT(NVARCHAR, data) + ' ' + CONVERT(NVARCHAR,ora) + ':' + CONVERT(NVARCHAR,min) + ':00') >= CONVERT(datetime, '" + DateTime.Now.AddHours(-1) + "',104) AND CONVERT(DATETIME, CONVERT(NVARCHAR, data) + ' ' + CONVERT(NVARCHAR,ora) + ':' + CONVERT(NVARCHAR,min) + ':00') >= CONVERT(DATETIME,'" + UltimaReusita() + "',104) AND valoareVeche = 'BLOCAT' AND tabela = 'LOGIN' AND coalesce(utilizatorIP,'')  = '" + vUtilizatorIp + "'", "blocat", Convert.ToInt16(DateTime.Now.Year));
        // if (Convert.ToInt32(vBlocat) == 0)
        {
            // daca nu e blocat ip-ul verificam daca avem trei esuari in ultima ora
            string vEsuariAnterioare = ManipuleazaBD.fRezultaUnString("SELECT COUNT(*) as numarIncercari FROM logOperatii WHERE CONVERT(DATETIME, CONVERT(NVARCHAR, data) + ' ' + CONVERT(NVARCHAR,ora) + ':' + CONVERT(NVARCHAR,min) + ':00') <= CONVERT(datetime, '" + DateTime.Now + "',104) AND CONVERT(DATETIME, CONVERT(NVARCHAR, data) + ' ' + CONVERT(NVARCHAR,ora) + ':' + CONVERT(NVARCHAR,min) + ':00') >= CONVERT(datetime, '" + DateTime.Now.AddMinutes(-60) + "',104) AND CONVERT(DATETIME, CONVERT(NVARCHAR, data) + ' ' + CONVERT(NVARCHAR,ora) + ':' + CONVERT(NVARCHAR,min) + ':00') >= CONVERT(DATETIME,'" + UltimaReusita() + "',104) AND valoareVeche = 'ESUAT' AND tabela = 'LOGIN' AND coalesce(utilizatorIP,'')  = '" + vUtilizatorIp + "'", "numarIncercari", Convert.ToInt16(DateTime.Now.Year));
            //  if (Convert.ToInt32(vEsuariAnterioare) < 3)
            {
                int vIese = 0;
                while (vIese == 0)
                {
                    // verificam utilizatorul
                    string vUnitateId = "0";
                    vUnitateId = ManipuleazaBD.fRezultaUnString("SELECT unitateId FROM unitati WHERE unitateCodFiscal ='" + vUnitateCIF + "'", "unitateId", Convert.ToInt16(DateTime.Now.Year));
                    string[] vCampuriLogin = { "utilizatorLogin", "utilizatorParola", "unitateId", "utilizatorActiv" };
                    string[] vValoriLogin = { vLogin, vParola, vUnitateId, "1" };
                    if (ManipuleazaBD.fVerificaExistentaArray("utilizatori", vCampuriLogin, vValoriLogin, " AND ", Convert.ToInt16(Session["SESan"])) == 1)
                    {
                        try
                        {
                            //   vUtilizatorId = Convert.ToInt32(ManipuleazaBD.fRezultaUnString("SELECT utilizatorId FROM utilizatori WHERE utilizatorLogin = '" + vLogin + "' AND utilizatorIp LIKE '%" + vUtilizatorIp + "%'", "utilizatorId"));
                        }
                        catch
                        {
                            valCustom.IsValid = false;
                            if (Convert.ToInt32(vEsuariAnterioare) < 2)
                            {
                                valCustom.ErrorMessage = "IP-ul " + vUtilizatorIp + " nu corespunde utilizatorului. Va rugăm să contactaţi administratorul sistemului pentru a vă da drepturi de conectare de pe acest IP (0369 101 101).";
                                ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), vUtilizatorId, DateTime.Now, "LOGIN", vUtilizatorIp + "|" + vLogin + "|" + tbUnitateId.Text, "ESUAT", Convert.ToInt64(Session["SESgospodarieId"]), 8, vUtilizatorIp);
                            }
                            else
                            {
                                valCustom.ErrorMessage = "Probleme la numele utilizator/IP...";//asta stergem
                                //  valCustom.ErrorMessage = "Ați depășit 3 încercări! IP-ul " + vUtilizatorIp + " va fi blocat până în " + DateTime.Now.AddHours(24);
                                ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), vUtilizatorId, DateTime.Now, "LOGIN", vUtilizatorIp + "|" + vLogin + "|" + tbUnitateId.Text, "BLOCAT", Convert.ToInt64(Session["SESgospodarieId"]), 8, vUtilizatorIp);
                            }
                            break;
                        }
                        vUtilizatorId = Convert.ToInt32(ManipuleazaBD.fRezultaUnString("SELECT utilizatorId FROM utilizatori WHERE utilizatorLogin = '" + vLogin + "' AND unitateId = '" + vUnitateId + "'", "utilizatorId", Convert.ToInt16(DateTime.Now.Year)));
                    }
                    else
                    {
                        valCustom.IsValid = false;
                        if (Convert.ToInt32(vEsuariAnterioare) < 2)
                        {
                            valCustom.ErrorMessage = "Probleme la combinaţia numele utilizator/parola/unitate...";
                            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), vUtilizatorId, DateTime.Now, "LOGIN", vUtilizatorIp + "|" + vLogin + "|" + tbUnitateId.Text, "ESUAT", Convert.ToInt64(Session["SESgospodarieId"]), 8, vUtilizatorIp);
                        }
                        else
                        {
                            valCustom.ErrorMessage = "Ați depășit 3 încercări de conectare în ultima oră! IP-ul " + vUtilizatorIp + " va fi blocat până în " + DateTime.Now.AddHours(1);
                            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), vUtilizatorId, DateTime.Now, "LOGIN", vUtilizatorIp + "|" + vLogin + "|" + tbUnitateId.Text, "BLOCAT", Convert.ToInt64(Session["SESgospodarieId"]), 8, vUtilizatorIp);
                        }
                        break;
                    }
                    vUnitateId = ManipuleazaBD.fRezultaUnString("SELECT unitateId FROM unitati WHERE unitateCodFiscal ='" + vUnitateCIF + "'", "unitateId", Convert.ToInt16(DateTime.Now.Year));
                    string[] vCampuri = { "utilizatorLogin", "utilizatorParola", "unitateId" };
                    string[] vValori = { vLogin, vParola, vUnitateId };
                    if (ManipuleazaBD.fVerificaExistentaArray("utilizatori", vCampuri, vValori, " AND ", Convert.ToInt16(Session["SESan"])) == 1)
                    {
                        int vDurataSesiuneLucru = Convert.ToInt32(ManipuleazaBD.fRezultaUnString("select top(1) coalesce(utilizatorDurataSesiune,30) as utilizatorDurataSesiune from utilizatori where utilizatorId='" + vUtilizatorId + "'", "utilizatorDurataSesiune", Convert.ToInt16(DateTime.Now.Year)));
                        if (!VerificareFursec()) return;
                        Session["SESunitateId"] = vUnitateId;
                        Session["SESutilizatorId"] = Convert.ToInt32(ManipuleazaBD.fRezultaUnString("SELECT utilizatorId FROM utilizatori WHERE utilizatorLogin = '" + vLogin + "' AND unitateId = '" + vUnitateId + "'", "utilizatorId", Convert.ToInt16(DateTime.Now.Year)));
                        Session["SESjudetId"] = Convert.ToInt32(ManipuleazaBD.fRezultaUnString("SELECT TOP (1) unitati.judetId FROM utilizatori INNER JOIN unitati ON utilizatori.unitateId = unitati.unitateId where utilizatorId='" + Session["SESutilizatorId"].ToString() + "'", "judetId", Convert.ToInt16(DateTime.Now.Year)));
                        //Session["SESan"] = DateTime.Now.Year.ToString();
                        Session["SESan"] = ManipuleazaBD.fRezultaUnString("SELECT unitateAnDeschis FROM unitati WHERE unitateID = '" + Session["SESunitateID"].ToString() + "'", "unitateAnDeschis", Convert.ToInt16(DateTime.Now.Year));
                        // Session["SESan"] = "2011";
                        //Session["SESan"] = DateTime.Now.Year.ToString();
                        verificare_licenta();
                        HttpCookie vCook = new HttpCookie("COOKTNT");
                        vCook["COOKunitateId"] = vUnitateId;
                        vCook["COOKutilizatorId"] = vUtilizatorId.ToString();
                        //  vCook["COOKan"] = DateTime.Now.Year.ToString();
                        vCook["COOKan"] = ManipuleazaBD.fRezultaUnString("SELECT unitateAnDeschis FROM unitati WHERE unitateID = '" + Session["SESunitateID"].ToString() + "'", "unitateAnDeschis", Convert.ToInt16(DateTime.Now.Year));
                        //vCook["COOKan"] = "2011";
                        vCook["COOKjudetId"] = Session["SESjudetId"].ToString();
                        if (Session["SESmesaj"] != null)
                            vCook["COOKmesaj"] = Session["SESmesaj"].ToString();
                        vCook = CriptareCookie.EncodeCookie(vCook);
                        vCook.Expires = DateTime.Now.AddMinutes(2005);
                        Response.Cookies.Add(vCook);

                        vCook = new HttpCookie("COOKTNTExpira");
                        vCook["COOKexpira"] = DateTime.Now.AddMinutes(vDurataSesiuneLucru).ToString();
                        Response.Cookies.Add(vCook);
                        //vCook.Expires = DateTime.Now.AddHours(8);

                        // daca e prima logare facem update la ip-ul de unde se conecteaza in utilizatori
                        // aflam IP-ul din utilizatori
                        string vUtilizatorIpDinTabela = ManipuleazaBD.fRezultaUnString("SELECT utilizatorIp FROM utilizatori WHERE utilizatorId = '" + Session["SESutilizatorId"].ToString() + "'", "utilizatorIp", Convert.ToInt16(Session["SESan"]));
                        if (vUtilizatorIpDinTabela == "758.492.820.741")
                        {
                            ManipuleazaBD.fManipuleazaBD("UPDATE utilizatori SET utilizatorIp = '" + vUtilizatorIp + "' WHERE utilizatorId='" + Session["SESutilizatorId"].ToString() + "'", Convert.ToInt16(DateTime.Now.Year));
                            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "LOGIN", vUtilizatorIp + "|" + vLogin + "|" + tbUnitateId.Text, "1-LOG-SCHIMB-IP", Convert.ToInt64(Session["SESgospodarieId"]), 8, vUtilizatorIp);
                        }
                        try
                        {
                            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "LOGIN", vUtilizatorIp + "|" + vLogin + "|" + tbUnitateId.Text, "REUSIT", Convert.ToInt64(Session["SESgospodarieId"]), 8, vUtilizatorIp);
                        }
                        catch { }
                        Response.Redirect("Gospodarii.aspx");
                    }
                    else
                    {
                        valCustom.IsValid = false;

                        if (Convert.ToInt32(vEsuariAnterioare) < 20000)
                        {
                            valCustom.ErrorMessage = "Combinația nume utilizator - parolă nu este corectă";
                            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), vUtilizatorId, DateTime.Now, "LOGIN", vUtilizatorIp + "|" + vLogin + "|" + tbUnitateId.Text, "ESUAT", Convert.ToInt64(Session["SESgospodarieId"]), 8, vUtilizatorIp);
                        }
                        else
                        {
                            valCustom.ErrorMessage = "Combinația nume utilizator - parolă nu este corectă"; // asta stergem
                            //  valCustom.ErrorMessage = "Ați depășit 3 încercări! IP-ul " + vUtilizatorIp + " va fi blocat până în " + DateTime.Now.AddHours(24);
                            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), vUtilizatorId, DateTime.Now, "LOGIN", vUtilizatorIp + "|" + vLogin + "|" + tbUnitateId.Text, "BLOCAT", Convert.ToInt64(Session["SESgospodarieId"]), 8, vUtilizatorIp);
                        }
                        break;
                    }
                }
            }
            //else
            //{
            //    valCustom.IsValid = false;
            //    valCustom.ErrorMessage = "Aţi depăşit numărul de încercări de conectare! Acest IP (" + vUtilizatorIp.ToString() + ") vă este blocat pentru o oră!"; // asta stergem
            //    //  valCustom.ErrorMessage = "Ați depășit 3 încercări!Acest IP este blocat până în " + DateTime.Now.AddDays(1);
            //    ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), vUtilizatorId, DateTime.Now, "LOGIN", vUtilizatorIp + "|" + vLogin + "|" + tbUnitateId.Text, "BLOCAT", Convert.ToInt64(Session["SESgospodarieId"]), 8);

            //}
        }
        //else // vBlocat
        {
            valCustom.IsValid = false;
            try
            {
                valCustom.ErrorMessage = "Accesul de pe acest IP (" + vUtilizatorIp.ToString() + ") vă este blocat până în: " + Convert.ToDateTime(ManipuleazaBD.fRezultaUnString("SELECT MAX(CONVERT(DATETIME, CONVERT(NVARCHAR, data) + ' ' + CONVERT(NVARCHAR,ora) + ':' + CONVERT(NVARCHAR,min) + ':00')) as dataBlocat FROM logOperatii WHERE CONVERT(DATETIME, CONVERT(NVARCHAR, data) + ' ' + CONVERT(NVARCHAR,ora) + ':' + CONVERT(NVARCHAR,min) + ':00') <= CONVERT(datetime, '" + DateTime.Now + "',104) AND CONVERT(DATETIME, CONVERT(NVARCHAR, data) + ' ' + CONVERT(NVARCHAR,ora) + ':' + CONVERT(NVARCHAR,min) + ':00') >= CONVERT(datetime, '" + DateTime.Now.AddHours(-24) + "',104) AND CONVERT(DATETIME, CONVERT(NVARCHAR, data) + ' ' + CONVERT(NVARCHAR,ora) + ':' + CONVERT(NVARCHAR,min) + ':00') >= CONVERT(DATETIME,'" + UltimaReusita() + "',104) AND valoareVeche = 'BLOCAT'  AND tabela = 'LOGIN' AND coalesce(utilizatorIP,'') = '" + vUtilizatorIp + "'", "dataBlocat", Convert.ToInt16(DateTime.Now.Year))).AddHours(1) + ". Pentru deblocare imediată sunaţi la tel. 0369 101 101.";
            }
            catch
            {

            }
        }

    }
}
