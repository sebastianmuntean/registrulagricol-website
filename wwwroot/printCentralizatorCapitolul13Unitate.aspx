﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="printCentralizatorCapitolul13Unitate.aspx.cs" Inherits="printCentralizatorCapitolul13Unitate" Culture="ro-RO" uiCulture="ro-RO" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" Runat="Server">
<asp:Label ID="url" runat="server" Text="Rapoarte / Centralizator CAPITOLUL  XIII: Producţia animală obţinută de gospodăriile/exploataţiile agricole fără personalitate juridică cu domiciliul/sediul în localitate şi/sau unităţile cu personalitate juridică care au activitate pe raza localităţii" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Panel ID="pnLista" runat="server" CssClass="panel_general" Visible="true">
    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" 
        Font-Size="8pt" Height="600px" Width="100%">
        <LocalReport ReportPath="rapoarte\raportCentralizatorCapitolul13Unitate.rdlc">
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" 
                    Name="dsRapoarte_dtCapitole" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource2" 
                    Name="dsRapoarte_dtUnitati" />
            </DataSources>
        </LocalReport>
    </rsweb:ReportViewer>
    <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="dsRapoarteTableAdapters.dtUnitatiTableAdapter">
        <SelectParameters>
            <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" 
                Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="dsRapoarteTableAdapters.dtCapitoleTableAdapter">
        <SelectParameters>
            <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" 
                Type="Int32" />
            <asp:Parameter DefaultValue="-1" Name="gopodarieId" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="an" SessionField="SESan" 
                Type="Int32" />
            <asp:QueryStringParameter Name="codCapitol" QueryStringField="codCapitol" 
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    </asp:Panel>
</asp:Content>

