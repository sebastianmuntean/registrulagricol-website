﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="reparaVolumPozitie.aspx.cs" Inherits="reparaVolumPozitie" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    Modificare volum şi poziţie
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<asp:UpdatePanel ID="upPostBack" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnListaGospodarii" runat="server" CssClass="panel_general" Visible="true">
                <asp:Panel ID="Panel4" runat="server">
                    <asp:Label ID="lblEroare1" runat="server" CssClass="validator" Font-Size="15px" Text="Eroare" Visible="false" />
                </asp:Panel>
                <asp:Panel ID="pnCautare" runat="server" CssClass="cauta">
                    <asp:Label ID="lbCautaDupa" runat="server" Text="Caută:"></asp:Label>
                    <asp:Label ID="lblUnitate" runat="server" Text=""></asp:Label>
                    <asp:DropDownList ID="ddlUnitate" Width="177px" AutoPostBack="True" runat="server"
                        OnInit="ddlfUnitati_Init" OnPreRender="ddlUnitate_PreRender" OnSelectedIndexChanged="ddlUnitate_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:Label ID="lbfVolum" runat="server" Text="Vol."></asp:Label>
                    <asp:TextBox ID="tbfVolum" Width="27px" runat="server" AutoPostBack="True"></asp:TextBox>
                    <asp:Label ID="lbfNrPoz" runat="server" Text="Poz."></asp:Label>
                    <asp:TextBox ID="tbfNrPoz" Width="27px" runat="server" AutoPostBack="True"></asp:TextBox>
                    <asp:Label ID="ldfNume" runat="server" Text="Nume"></asp:Label>
                    <asp:TextBox ID="tbfNume" Width="70px" runat="server" AutoPostBack="True"></asp:TextBox>
                    <asp:Label ID="lbfLoc" runat="server" Text="Loc."></asp:Label>
                    <asp:TextBox ID="tbfLoc" Width="70px" runat="server" AutoPostBack="True"></asp:TextBox>
                    <asp:Label ID="lbfStrada" runat="server" Text="Str."></asp:Label>
                    <asp:TextBox ID="tbfStrada" Width="70px" runat="server" AutoPostBack="True"></asp:TextBox>
                    <asp:Label ID="lbfNr" runat="server" Text="Nr."></asp:Label>
                    <asp:TextBox ID="tbfNr" Width="20px" runat="server" AutoPostBack="True"></asp:TextBox>
                </asp:Panel>
                <asp:Panel ID="Panel5" runat="server" CssClass="cauta">
                    <asp:Label ID="lblFCautaDupa1" runat="server" Text="Caută:"></asp:Label>
                    <asp:Label ID="lblFTip" runat="server" Text="Tip:"></asp:Label>
                    <asp:DropDownList ID="ddlFTip" AutoPostBack="true" runat="server" Width="110px">
                        <asp:ListItem Value="%">Toate</asp:ListItem>
                        <asp:ListItem Value="1">Localnic</asp:ListItem>
                        <asp:ListItem Value="2">Străinaş</asp:ListItem>
                        <asp:ListItem Value="3">Firmă pe raza localităţii</asp:ListItem>
                        <asp:ListItem Value="4">Firmă străinaşă</asp:ListItem>
                    </asp:DropDownList>
                    <asp:Label ID="lblOrdonareDupa" runat="server" Text="Ordonare după:"></asp:Label>
                    <asp:DropDownList AutoPostBack="true" ID="ddlFOrdonareDupa" runat="server" OnSelectedIndexChanged="ddlFOrdonareDupa_SelectedIndexChanged"
                        OnInit="ddlFOrdonareDupa_Init">
                        <asp:ListItem Text="Volum, nr. poziţie" Value="0"></asp:ListItem>
                        <asp:ListItem Text="Localitate, stradă, nr." Value="1"></asp:ListItem>
                        <asp:ListItem Text="Nume" Value="2"></asp:ListItem>
                        <asp:ListItem Text="ID crescător" Value="3"></asp:ListItem>
                        <asp:ListItem Text="ID descrescător" Value="4"></asp:ListItem>
                    </asp:DropDownList>
                </asp:Panel>
                <asp:Panel ID="pnGrid" runat="server" Visible="true">
                    <asp:GridView ID="gvGospodarii" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        BorderStyle="None" CellPadding="3" GridLines="Vertical" ShowFooter="True" CssClass="tabela"
                        DataKeyNames="gospodarieId" DataSourceID="SqlGospodarii" EmptyDataText="Nu sunt adaugate inregistrari"
                        AllowPaging="True">
                        <Columns>
                            <asp:TemplateField HeaderText="Volum" SortExpression="volumInt">
                                <EditItemTemplate>
                                    
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblGospodarieId" Visible="false" runat="server" Text='<%# Bind("gospodarieId") %>'></asp:Label>
                                    <asp:TextBox ID="tbVolum" AutoPostBack="true" Width="50px" runat="server" 
                                        Text='<%# Bind("volum") %>' onfocus="this.select()" OnTextChanged="tbVolum_TextChanged" ToolTip='<%# Bind("volumInt") %>'></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Număr poziţie" SortExpression="nrPozitieInt">
                                <EditItemTemplate>
                                    
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:TextBox ID="tbPozitie" AutoPostBack="true" Width="50px" runat="server" 
                                        Text='<%# Bind("nrPozitie") %>' onfocus="this.select()" OnTextChanged="tbPozitie_TextChanged" ToolTip='<%# Bind("nrPozitieInt") %>'></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="nume" HeaderText="Nume" SortExpression="nume" />
                            <asp:BoundField DataField="codSiruta" HeaderText="Cod şiruta" SortExpression="codSiruta" />
                            <asp:BoundField DataField="tip" HeaderText="Tip" SortExpression="tip" />
                            <asp:BoundField DataField="strada" HeaderText="Strada" SortExpression="strada" />
                            <asp:BoundField DataField="nr" HeaderText="Număr" SortExpression="nr" />
                            <asp:BoundField DataField="nrInt" HeaderText="Număr întreg" SortExpression="nrInt" />
                            <asp:BoundField DataField="codExploatatie" HeaderText="Cod exploataţie" SortExpression="codExploatatie" />
                            <asp:BoundField DataField="codUnic" HeaderText="Cod unic" SortExpression="codUnic" />
                            <asp:BoundField DataField="judet" HeaderText="Judeţ" SortExpression="judet" />
                            <asp:BoundField DataField="localitate" HeaderText="Localitate" SortExpression="localitate" />
                        </Columns>
                        <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                        <HeaderStyle Font-Bold="True" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlGospodarii" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString %>"
                        SelectCommand="SELECT gospodarieId, unitateId, volum, nrPozitie, volumInt, nrPozitieInt, codSiruta, CASE gospodarii.tip WHEN '1' THEN 'Localnic' WHEN '2' THEN 'Străinaş' WHEN '3' THEN 'Firmă pe raza localităţii' ELSE 'Firmă străinaşă' END AS tip, CASE strainas WHEN 1 THEN sstrada ELSE strada END AS strada, CASE strainas WHEN 1 THEN snr ELSE nr END AS nr, nrInt, bl, sc, et, ap, codExploatatie, codUnic, CASE strainas WHEN 1 THEN sjudet ELSE judet END AS judet, CASE strainas WHEN 1 THEN slocalitate ELSE localitate END AS localitate, persJuridica, jUnitate, jSubunitate, jCodFiscal, jNumeReprez, strainas, sStrada, sNr, sBl, sSc, sEtj, sAp, sJudet, sLocalitate, CASE persjuridica WHEN 0 THEN (SELECT TOP (1) nume FROM membri WHERE an = @an AND gospodarieId = gospodarii.gospodarieid AND codrudenie = 1) ELSE junitate END AS nume FROM gospodarii WHERE (COALESCE (volum, N'') LIKE @volum) AND (COALESCE (nrPozitie, N'') LIKE @nrPozitie) AND (CONVERT (nvarchar, unitateId) LIKE @unitateId) AND (COALESCE (CASE persjuridica WHEN 0 THEN (SELECT TOP (1) nume FROM membri WHERE an = @an AND gospodarieId = gospodarii.gospodarieid AND codrudenie = 1) ELSE junitate END, N'') LIKE '%' + @nume + '%') AND (tip LIKE @tip) AND (COALESCE (CASE strainas WHEN 1 THEN slocalitate ELSE localitate END, N'') LIKE '%' + @localitate + '%') AND (COALESCE (CASE strainas WHEN 1 THEN sstrada ELSE strada END, N'') LIKE '%' + @strada + '%') AND (COALESCE (CASE strainas WHEN 1 THEN snr ELSE nr END, N'') LIKE @nr)  AND an = @an ORDER BY volumInt, nrPozitieInt">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="ddlUnitate" Name="unitateId" PropertyName="SelectedValue" />
                            <asp:ControlParameter ControlID="tbfVolum" DefaultValue="%" Name="volum" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfNrPoz" DefaultValue="%" Name="nrPozitie" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfLoc" DefaultValue="%" Name="localitate" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfStrada" DefaultValue="%" Name="strada" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfNr" DefaultValue="%" Name="nr" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfNume" DefaultValue="%" Name="nume" PropertyName="Text" />
                            <asp:SessionParameter Name="an" SessionField="SESan" />
                            <asp:ControlParameter ControlID="ddlFTip" Name="tip" PropertyName="SelectedValue" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlGospodariiLocStrNr" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString %>"
                        SelectCommand="SELECT gospodarieId, unitateId, volum, nrPozitie, volumInt, nrPozitieInt, codSiruta, CASE gospodarii.tip WHEN '1' THEN 'Localnic' WHEN '2' THEN 'Străinaş' WHEN '3' THEN 'Firmă pe raza localităţii' ELSE 'Firmă străinaşă' END AS tip, CASE strainas WHEN 1 THEN sstrada ELSE strada END AS strada, CASE strainas WHEN 1 THEN snr ELSE nr END AS nr, nrInt, bl, sc, et, ap, codExploatatie, codUnic, CASE strainas WHEN 1 THEN sjudet ELSE judet END AS judet, CASE strainas WHEN 1 THEN slocalitate ELSE localitate END AS localitate, persJuridica, jUnitate, jSubunitate, jCodFiscal, jNumeReprez, strainas, sStrada, sNr, sBl, sSc, sEtj, sAp, sJudet, sLocalitate, CASE persjuridica WHEN 0 THEN (SELECT TOP (1) nume FROM membri WHERE an = @an AND gospodarieId = gospodarii.gospodarieid AND codrudenie = 1) ELSE junitate END AS nume FROM gospodarii WHERE (COALESCE (volum, N'') LIKE @volum) AND (COALESCE (nrPozitie, N'') LIKE @nrPozitie) AND (CONVERT (nvarchar, unitateId) LIKE @unitateId) AND (COALESCE (CASE persjuridica WHEN 0 THEN (SELECT TOP (1) nume FROM membri WHERE an = @an AND gospodarieId = gospodarii.gospodarieid AND codrudenie = 1) ELSE junitate END, N'') LIKE '%' + @nume + '%') AND (tip LIKE @tip) AND (COALESCE (CASE strainas WHEN 1 THEN slocalitate ELSE localitate END, N'') LIKE '%' + @localitate + '%') AND (COALESCE (CASE strainas WHEN 1 THEN sstrada ELSE strada END, N'') LIKE '%' + @strada + '%') AND (COALESCE (CASE strainas WHEN 1 THEN snr ELSE nr END, N'') LIKE @nr) AND gospodarii.an = @an order by localitate, strada, nr">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="ddlUnitate" Name="unitateId" PropertyName="SelectedValue" />
                            <asp:ControlParameter ControlID="tbfVolum" DefaultValue="%" Name="volum" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfNrPoz" DefaultValue="%" Name="nrPozitie" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfLoc" DefaultValue="%" Name="localitate" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfStrada" DefaultValue="%" Name="strada" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfNr" DefaultValue="%" Name="nr" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfNume" DefaultValue="%" Name="nume" PropertyName="Text" />
                            <asp:SessionParameter Name="an" SessionField="SESan" />
                            <asp:ControlParameter ControlID="ddlFTip" Name="tip" PropertyName="SelectedValue" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlGospodariiNume" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString %>"
                        SelectCommand="SELECT gospodarieId, unitateId, volum, nrPozitie, volumInt, nrPozitieInt, codSiruta, CASE gospodarii.tip WHEN '1' THEN 'Localnic' WHEN '2' THEN 'Străinaş' WHEN '3' THEN 'Firmă pe raza localităţii' ELSE 'Firmă străinaşă' END AS tip, CASE strainas WHEN 1 THEN sstrada ELSE strada END AS strada, CASE strainas WHEN 1 THEN snr ELSE nr END AS nr, nrInt, bl, sc, et, ap, codExploatatie, codUnic, CASE strainas WHEN 1 THEN sjudet ELSE judet END AS judet, CASE strainas WHEN 1 THEN slocalitate ELSE localitate END AS localitate, persJuridica, jUnitate, jSubunitate, jCodFiscal, jNumeReprez, strainas, sStrada, sNr, sBl, sSc, sEtj, sAp, sJudet, sLocalitate, CASE persjuridica WHEN 0 THEN (SELECT TOP (1) nume FROM membri WHERE an = @an AND gospodarieId = gospodarii.gospodarieid AND codrudenie = 1) ELSE junitate END AS nume FROM gospodarii WHERE (COALESCE (volum, N'') LIKE @volum) AND (COALESCE (nrPozitie, N'') LIKE @nrPozitie) AND (CONVERT (nvarchar, unitateId) LIKE @unitateId) AND (COALESCE (CASE persjuridica WHEN 0 THEN (SELECT TOP (1) nume FROM membri WHERE an = @an AND gospodarieId = gospodarii.gospodarieid AND codrudenie = 1) ELSE junitate END, N'') LIKE '%' + @nume + '%') AND (tip LIKE @tip) AND (COALESCE (CASE strainas WHEN 1 THEN slocalitate ELSE localitate END, N'') LIKE '%' + @localitate + '%') AND (COALESCE (CASE strainas WHEN 1 THEN sstrada ELSE strada END, N'') LIKE '%' + @strada + '%') AND (COALESCE (CASE strainas WHEN 1 THEN snr ELSE nr END, N'') LIKE @nr)  AND gospodarii.an = @an order by nume">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="ddlUnitate" Name="unitateId" PropertyName="SelectedValue" />
                            <asp:ControlParameter ControlID="tbfVolum" DefaultValue="%" Name="volum" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfNrPoz" DefaultValue="%" Name="nrPozitie" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfLoc" DefaultValue="%" Name="localitate" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfStrada" DefaultValue="%" Name="strada" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfNr" DefaultValue="%" Name="nr" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfNume" DefaultValue="%" Name="nume" PropertyName="Text" />
                            <asp:SessionParameter Name="an" SessionField="SESan" />
                            <asp:ControlParameter ControlID="ddlFTip" Name="tip" PropertyName="SelectedValue" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlGospodariiIdCresc" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString %>"
                        SelectCommand="SELECT gospodarieId, unitateId, volum, nrPozitie, volumInt, nrPozitieInt, codSiruta, CASE gospodarii.tip WHEN '1' THEN 'Localnic' WHEN '2' THEN 'Străinaş' WHEN '3' THEN 'Firmă pe raza localităţii' ELSE 'Firmă străinaşă' END AS tip, CASE strainas WHEN 1 THEN sstrada ELSE strada END AS strada, CASE strainas WHEN 1 THEN snr ELSE nr END AS nr, nrInt, bl, sc, et, ap, codExploatatie, codUnic, CASE strainas WHEN 1 THEN sjudet ELSE judet END AS judet, CASE strainas WHEN 1 THEN slocalitate ELSE localitate END AS localitate, persJuridica, jUnitate, jSubunitate, jCodFiscal, jNumeReprez, strainas, sStrada, sNr, sBl, sSc, sEtj, sAp, sJudet, sLocalitate, CASE persjuridica WHEN 0 THEN (SELECT TOP (1) nume FROM membri WHERE an = @an AND gospodarieId = gospodarii.gospodarieid AND codrudenie = 1) ELSE junitate END AS nume FROM gospodarii WHERE (COALESCE (volum, N'') LIKE @volum) AND (COALESCE (nrPozitie, N'') LIKE @nrPozitie) AND (CONVERT (nvarchar, unitateId) LIKE @unitateId) AND (COALESCE (CASE persjuridica WHEN 0 THEN (SELECT TOP (1) nume FROM membri WHERE an = @an AND gospodarieId = gospodarii.gospodarieid AND codrudenie = 1) ELSE junitate END, N'') LIKE '%' + @nume + '%') AND (tip LIKE @tip) AND (COALESCE (CASE strainas WHEN 1 THEN slocalitate ELSE localitate END, N'') LIKE '%' + @localitate + '%') AND (COALESCE (CASE strainas WHEN 1 THEN sstrada ELSE strada END, N'') LIKE '%' + @strada + '%') AND (COALESCE (CASE strainas WHEN 1 THEN snr ELSE nr END, N'') LIKE @nr)  AND gospodarii.an = @an order by gospodarieId ASC">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="ddlUnitate" Name="unitateId" PropertyName="SelectedValue" />
                            <asp:ControlParameter ControlID="tbfVolum" DefaultValue="%" Name="volum" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfNrPoz" DefaultValue="%" Name="nrPozitie" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfLoc" DefaultValue="%" Name="localitate" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfStrada" DefaultValue="%" Name="strada" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfNr" DefaultValue="%" Name="nr" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfNume" DefaultValue="%" Name="nume" PropertyName="Text" />
                            <asp:SessionParameter Name="an" SessionField="SESan" />
                            <asp:ControlParameter ControlID="ddlFTip" Name="tip" PropertyName="SelectedValue" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlGospodariiIdDesc" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString %>"
                        SelectCommand="SELECT gospodarieId, unitateId, volum, nrPozitie, volumInt, nrPozitieInt, codSiruta, CASE gospodarii.tip WHEN '1' THEN 'Localnic' WHEN '2' THEN 'Străinaş' WHEN '3' THEN 'Firmă pe raza localităţii' ELSE 'Firmă străinaşă' END AS tip, CASE strainas WHEN 1 THEN sstrada ELSE strada END AS strada, CASE strainas WHEN 1 THEN snr ELSE nr END AS nr, nrInt, bl, sc, et, ap, codExploatatie, codUnic, CASE strainas WHEN 1 THEN sjudet ELSE judet END AS judet, CASE strainas WHEN 1 THEN slocalitate ELSE localitate END AS localitate, persJuridica, jUnitate, jSubunitate, jCodFiscal, jNumeReprez, strainas, sStrada, sNr, sBl, sSc, sEtj, sAp, sJudet, sLocalitate, CASE persjuridica WHEN 0 THEN (SELECT TOP (1) nume FROM membri WHERE an = @an AND gospodarieId = gospodarii.gospodarieid AND codrudenie = 1) ELSE junitate END AS nume FROM gospodarii WHERE (COALESCE (volum, N'') LIKE @volum) AND (COALESCE (nrPozitie, N'') LIKE @nrPozitie) AND (CONVERT (nvarchar, unitateId) LIKE @unitateId) AND (COALESCE (CASE persjuridica WHEN 0 THEN (SELECT TOP (1) nume FROM membri WHERE an = @an AND gospodarieId = gospodarii.gospodarieid AND codrudenie = 1) ELSE junitate END, N'') LIKE '%' + @nume + '%') AND (tip LIKE @tip) AND (COALESCE (CASE strainas WHEN 1 THEN slocalitate ELSE localitate END, N'') LIKE '%' + @localitate + '%') AND (COALESCE (CASE strainas WHEN 1 THEN sstrada ELSE strada END, N'') LIKE '%' + @strada + '%') AND (COALESCE (CASE strainas WHEN 1 THEN snr ELSE nr END, N'') LIKE @nr)  AND gospodarii.an = @an order by gospodarieId DESC">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="ddlUnitate" Name="unitateId" PropertyName="SelectedValue" />
                            <asp:ControlParameter ControlID="tbfVolum" DefaultValue="%" Name="volum" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfNrPoz" DefaultValue="%" Name="nrPozitie" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfLoc" DefaultValue="%" Name="localitate" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfStrada" DefaultValue="%" Name="strada" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfNr" DefaultValue="%" Name="nr" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfNume" DefaultValue="%" Name="nume" PropertyName="Text" />
                            <asp:SessionParameter Name="an" SessionField="SESan" />
                            <asp:ControlParameter ControlID="ddlFTip" Name="tip" PropertyName="SelectedValue" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </asp:Panel>
                <asp:Panel ID="pnButoanePrincipale" CssClass="butoane" runat="server">
                    
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
