﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="detaliiUnitate.aspx.cs" Inherits="detaliiUnitate" Culture="ro-RO" UICulture="ro-RO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Configurări / Detalii Unitate" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="pnValSum" runat="server">
        <asp:ValidationSummary ID="valSum" runat="server" DisplayMode="SingleParagraph" Visible="true"
            ValidationGroup="GrupValidare" CssClass="validator" ForeColor="" />
    </asp:Panel>
    <asp:Panel ID="pnPanelGeneral" runat="server">
        <asp:Panel ID="pnAdaugaUtilizator" CssClass="panel_general" runat="server">
            <asp:Panel ID="pnMaster" CssClass="adauga" runat="server">
                <asp:Panel ID="pnDetalii" runat="server">
                    <p>
                        <asp:Label ID="lbl" runat="server" Text="Completați numele întreg așa cum doriți să apară în anteturile / subsolurile de adeverințe."></asp:Label>
                    </p>
                    <p>
                        <asp:Label ID="lblAdresa" runat="server" Text="Adresa unitatii:" />
                        <asp:Label ID="lblStrada" runat="server" Text="Strada" />
                        <asp:TextBox ID="tbStrada" runat="server" />
                        <asp:Label ID="lblNumar" runat="server" Text="Nr." />
                        <asp:TextBox ID="tbNumar" runat="server" Width="30px" />
                        <asp:Label ID="lblAp" runat="server" Text="Ap." />
                        <asp:TextBox ID="tbAp" runat="server" Width="30px" />
                        <asp:Label ID="lblCodPostal" runat="server" Text="Cod poștal" />
                        <asp:TextBox ID="tbCodPostal" runat="server" Width="100px" />
                    </p>
                    <p>
                        <asp:Label ID="lblTelefon" runat="server" Text="Telefon:" Width="130px" />
                        <asp:TextBox ID="tbTelefon" runat="server" Width="200px" />
                        <asp:Label ID="lblFax" runat="server" Text="Fax" Width="130px" />
                        <asp:TextBox ID="tbFax" runat="server" Width="200px" />
                    </p>
                    <p>
                        <asp:Label ID="lblEmail" runat="server" Text="Adresa Email:" Width="130px" />
                        <asp:TextBox ID="tbEmail" runat="server" Width="200px" />
                        <asp:Label ID="lblWeb" runat="server" Text="Pagina internet" Width="130px" />
                        <asp:TextBox ID="tbWeb" runat="server" Width="200px" />
                    </p>
                    <p>
                        <asp:Label ID="lblPrimar" runat="server" Text="Primar" Width="130px"></asp:Label>
                        <asp:TextBox ID="tbPrimar" runat="server" Width="200px"></asp:TextBox>
                        <asp:Label ID="lblVicePrimar" runat="server" Text="Vice-Primar" Width="130px"></asp:Label>
                        <asp:TextBox ID="tbVicePrimar" runat="server" Width="200px"></asp:TextBox>
                    </p>
                    <p>
                        <asp:Label ID="lblSecretar" runat="server" Text="Secretar" Width="130px"></asp:Label>
                        <asp:TextBox ID="tbSecretar" runat="server" Width="200px"></asp:TextBox>
                        <asp:Label ID="lblContabil" runat="server" Text="Contabil" Width="130px"></asp:Label>
                        <asp:TextBox ID="tbContabil" runat="server" Width="200px"></asp:TextBox>
                    </p>
                    <p>
                        <asp:Label ID="lblSefServiciuAgricol" runat="server" Text="Șef Serviciu Agricol"
                            Width="130px"></asp:Label>
                        <asp:TextBox ID="tbSefServiciuAgricol" runat="server" Width="200px"></asp:TextBox>
                        <asp:Label ID="lblInspector1" runat="server" Text="Inspector (1)" Width="130px"></asp:Label>
                        <asp:TextBox ID="tbInspector1" runat="server" Width="200px"></asp:TextBox>
                    </p>
                    <p>
                        <asp:Label ID="lblInspector2" runat="server" Text="Inspector (2)" Width="130px"></asp:Label>
                        <asp:TextBox ID="tbInspector2" runat="server" Width="200px"></asp:TextBox>
                        <asp:Label ID="lblInspector3" runat="server" Text="Inspector (3)" Width="130px"></asp:Label>
                        <asp:TextBox ID="tbInspector3" runat="server" Width="200px"></asp:TextBox>
                    </p>
                    <p>
                        <asp:Label ID="lblTipAfisareStrainas" runat="server" Text="Tip afisare adresa strainas"></asp:Label>
                        <asp:DropDownList ID="ddlTipAfisareStrainas" runat="server">
                            <asp:ListItem Text="adresa de domiciliu" Value="1"></asp:ListItem>
                            <asp:ListItem Text="adresa din localitate" Value="2"></asp:ListItem>
                        </asp:DropDownList>
                    </p>
                    <p>
                        <asp:Label ID="lblEmblema" runat="server" Text="Alegeti o imagine (stemă, emblemă, ştampilă, semnătură etc.):"></asp:Label>
                    </p>
                    <p>
                        <asp:Button ID="btEmblema" runat="server" Text="Incarca emblema" OnClick="btEmblema_Click"
                            CssClass="buton" Visible="false" />
                        <asp:FileUpload ID="fuEmblema" runat="server" OnLoad="fuEmblema_Load" /><br />
                        <br />
                        <asp:Label ID="lblDescriereEmblema" runat="server" Text="Descriere imagine: "></asp:Label><asp:TextBox
                            ID="tbEmblemaDescriere" runat="server" Width="600px"></asp:TextBox>
                        <asp:GridView ID="gvEmbleme" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            DataKeyNames="fisierId" DataSourceID="SqlEmbleme" OnPreRender="gvEmbleme_PreRender"
                            CssClass="tabela">
                            <Columns>
                                <asp:TemplateField ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lkbSterge" runat="server" CommandName="Delete" OnClientClick="return confirm('Eşti sigur că doreşti să renunţi la imagine?')"
                                            Text="şterge"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="fisierId" HeaderText="fisierId" InsertVisible="False"
                                    ReadOnly="True" SortExpression="fisierId" Visible="false" />
                                <asp:BoundField DataField="fisierDescriere" HeaderText="descriere" SortExpression="fisierDescriere" />
                                <asp:TemplateField HeaderText="fişier" SortExpression="fisierNume">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGvNumeFisier" runat="server" Text='<%# Bind("fisierNume") %>'></asp:Label><br />
                                        <asp:Image ID="imgGvEmblema" runat="server" BorderStyle="Solid" BorderWidth="1px"
                                            BorderColor="Beige" Height="100px" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="fisierDataIncarcare" HeaderText="data încărcării" SortExpression="fisierDataIncarcare" />
                            </Columns>
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlEmbleme" runat="server" 
                            SelectCommand="SELECT [fisierId], [fisierDescriere], [fisierNume], [fisierDataIncarcare] FROM [fisiere] WHERE (([unitateCodFiscal] = @unitateCodFiscal) AND ([fisierStatus] = @fisierStatus)) ORDER BY [fisierDataIncarcare] DESC, [fisierDescriere] DESC"
                            DeleteCommand="UPDATE fisiere SET fisierStatus='sters' WHERE (fisierId = @fisierId)"
                            OnInit="SqlEmbleme_Init">
                            <SelectParameters>
                                <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" Type="Int32" />
                                <asp:Parameter DefaultValue="activ" Name="fisierStatus" Type="String" />
                                <asp:Parameter DefaultValue="activ" Name="unitateCodFiscal" Type="String" />
                            </SelectParameters>
                            <DeleteParameters>
                                <asp:ControlParameter ControlID="gvEmbleme" Name="fisierId" PropertyName="SelectedValue" />
                            </DeleteParameters>
                        </asp:SqlDataSource>
                    </p>
                </asp:Panel>
                <asp:Panel ID="pnButoanePrincipale" runat="server" CssClass="butoane">
                    <asp:Button ID="btModificaCont" runat="server" CssClass="buton" Text="modifică datele unității"
                        ValidationGroup="GrupValidare" OnClick="btModificaCont_Click" />
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
        <asp:Panel ID="validatoare" CssClass="ascunsa" runat="server">
            <!-- //////////// VALIDATOARE  \\\\\\\\\\\\\\\\\\ -->
            <asp:CustomValidator ID="valCustom" runat="server" ErrorMessage="" ValidationGroup="GrupValidare"></asp:CustomValidator>
        </asp:Panel>
    </asp:Panel>
</asp:Content>
