﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class reparaDiacriticeInchidereAn : System.Web.UI.Page
{
    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            tbAn.Text = Session["SESan"].ToString();
    }
    protected void btAdauga_Click(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd1 = new SqlCommand();
        SqlCommand vCmd2 = new SqlCommand();
        SqlCommand vCmd3 = new SqlCommand();
        vCmd1.Connection = vCon;
        vCmd2.Connection = vCon;
        vCmd3.Connection = vCon;
        vCmd1.CommandTimeout = 0;
        vCmd2.CommandTimeout = 0;
        vCmd3.CommandTimeout = 0;
        
        vCmd2.CommandText = "select unitateId from unitati where convert(nvarchar,unitateId) like '" + ddlUnitate.SelectedValue + "'";
        List<string> vListaUnitati = new List<string>();
        SqlDataReader vTabelUnitati = vCmd2.ExecuteReader();
        while (vTabelUnitati.Read())
        {
            // nu repar unitatile deja reparate
            vCmd1.CommandText = "select count(*) from tempMihaiUnitati where unitateId='" + vTabelUnitati["unitateId"].ToString() + "'";
            if (Convert.ToInt32(vCmd1.ExecuteScalar()) == 0)
                vListaUnitati.Add(vTabelUnitati["unitateId"].ToString());
        }
        vTabelUnitati.Close();
        
        foreach (string vUnitate in vListaUnitati)
        {
            // inceput de unitate
            vCmd1.CommandText = "select top(1) unitateDenumire from unitati where unitateId='" + vUnitate + "'";
            string vUnitateDenumire = vCmd1.ExecuteScalar().ToString();
            vCmd1.CommandText = "INSERT INTO tempMihaiUnitati (unitateId, unitateDenumire, tip, data) VALUES ('" + vUnitate + "', N'" + vUnitateDenumire + "', 'START', convert(datetime,'" + DateTime.Now + "',104))";
            vCmd1.ExecuteNonQuery();
            // gospodarii
            vCmd2.CommandText = "SELECT a.gospodarieId, a.strada AS strada1, a.localitate AS localitate1, a.judet AS judet1, a.sStrada AS sStrada1, a.sLocalitate AS sLocalitate1, a.sJudet AS sJudet1, a.observatii AS observatii1, a.membruNume AS membruNume1, b.strada AS strada2, b.judet AS judet2, b.localitate AS localitate2, b.sStrada AS sStrada2, b.sJudet AS sJudet2, b.sLocalitate AS sLocalitate2, b.observatii AS observatii2, b.membruNume AS membruNume2 FROM gospodarii AS a INNER JOIN gospodarii AS b ON a.gospodarieIdInitial = b.gospodarieIdInitial AND b.an = " + tbAn.Text + " - 1 WHERE (a.unitateId = '" + vUnitate + "') AND (a.an = '" + tbAn.Text + "')";
            SqlDataReader vTabel = vCmd2.ExecuteReader();
            string vUpdate = "";
            string vInterogare = "";
            int vCount = 0;
            while (vTabel.Read())
            {
                List<string> vListaUpdate = new List<string>();
                if (contineDiacritice(vTabel, "strada2") && !contineDiacritice(vTabel, "strada1") && egalitateCampuri(vTabel, "strada1", "strada2"))
                    vListaUpdate.Add("strada");
                if (contineDiacritice(vTabel, "localitate2") && !contineDiacritice(vTabel, "localitate1") && egalitateCampuri(vTabel, "localitate1", "localitate2"))
                    vListaUpdate.Add("localitate");
                if (contineDiacritice(vTabel, "judet2") && !contineDiacritice(vTabel, "judet1") && egalitateCampuri(vTabel, "judet1", "judet2"))
                    vListaUpdate.Add("judet");
                if (contineDiacritice(vTabel, "sStrada2") && !contineDiacritice(vTabel, "sStrada1") && egalitateCampuri(vTabel, "sStrada1", "sStrada2"))
                    vListaUpdate.Add("sStrada");
                if (contineDiacritice(vTabel, "sLocalitate2") && !contineDiacritice(vTabel, "sLocalitate1") && egalitateCampuri(vTabel, "sLocalitate1", "sLocalitate2"))
                    vListaUpdate.Add("sLocalitate");
                if (contineDiacritice(vTabel, "sJudet2") && !contineDiacritice(vTabel, "sJudet1") && egalitateCampuri(vTabel, "sJudet1", "sJudet2"))
                    vListaUpdate.Add("sJudet");
                if (contineDiacritice(vTabel, "observatii2") && !contineDiacritice(vTabel, "observatii1") && egalitateCampuri(vTabel, "observatii1", "observatii2"))
                    vListaUpdate.Add("observatii");
                if (contineDiacritice(vTabel, "membruNume2") && !contineDiacritice(vTabel, "membruNume1") && egalitateCampuri(vTabel, "membruNume1", "membruNume2"))
                    vListaUpdate.Add("membruNume");
                if (vListaUpdate.Count > 0)
                {
                    vUpdate = "update gospodarii set ";
                    foreach (string a in vListaUpdate)
                        vUpdate += a + " = N'" + vTabel[a + "2"].ToString() + "', ";
                    vUpdate = vUpdate.Remove(vUpdate.LastIndexOf(", "), vUpdate.Length - vUpdate.LastIndexOf(", "));
                    vUpdate += " where gospodarieId=" + vTabel["gospodarieId"].ToString();
                    vInterogare += vUpdate + "; ";
                    vCount++;
                }
                //if (vCount >= 500)
                //{
                //    vCmd1.CommandText = vInterogare;
                //    vCmd1.ExecuteNonQuery();
                //    vInterogare = "";
                //    vCount = 0;
                //}
            }
            vTabel.Close();
            vCount = 0;
            if (vInterogare != "")
            {
                vCmd1.CommandText = vInterogare;
                vCmd1.ExecuteNonQuery();
                vInterogare = "";
            }
            // membri
            vCmd2.CommandText = "SELECT membri.capitolId, membri.gospodarieId, membri.cnp, membri.nume, membri.denumireRudenie, membri.mentiuni, gospodarii.gospodarieIdInitial FROM membri INNER JOIN gospodarii ON membri.gospodarieId = gospodarii.gospodarieId WHERE (membri.unitateId = '" + vUnitate + "') AND (membri.an = '" + tbAn.Text + "')";
            vTabel = vCmd2.ExecuteReader();

            while (vTabel.Read())
            {
                string vGospodarieId = "0";
                try
                {
                    vCmd1.CommandText = "select top(1) gospodarieId from gospodarii where gospodarieIdInitial='" + vTabel["gospodarieIdInitial"].ToString() + "' and an='" + (Convert.ToInt32(tbAn.Text) - 1) + "'";
                    vGospodarieId = vCmd1.ExecuteScalar().ToString();
                }
                catch { vGospodarieId = vTabel["gospodarieIdInitial"].ToString(); }
                CNP vCnp = new CNP(vTabel["cnp"].ToString().Replace(" ",""));
                if (vCnp.IsValidB())
                {
                    string vNume = vTabel["nume"].ToString(), vDenumireRudenie = vTabel["denumireRudenie"].ToString(), vMentiuni = vTabel["mentiuni"].ToString();
                    // extrag datele despre membru in anul precedent
                    vCmd3.CommandText = "SELECT membri.capitolId, membri.gospodarieId, membri.cnp, membri.nume, membri.denumireRudenie, membri.mentiuni, gospodarii.gospodarieIdInitial FROM membri INNER JOIN gospodarii ON membri.gospodarieId = gospodarii.gospodarieId WHERE (membri.gospodarieId = '" + vGospodarieId + "') AND (membri.an = '" + (Convert.ToInt32(tbAn.Text) - 1) + "') AND (membri.cnp = '" + vTabel["cnp"].ToString() + "')";
                    SqlDataReader vTabel3 = vCmd3.ExecuteReader();
                    List<string> vListaUpdate = new List<string>();
                    List<string> vListaUpdateValori = new List<string>();
                    if (vTabel3.Read())
                    {
                        vNume = vTabel3["nume"].ToString();
                        if (contineDiacritice(vTabel3, "nume") && !contineDiacritice(vTabel, "nume") && egalitateCampuri(vTabel["nume"].ToString(), vNume))
                        {
                            vListaUpdate.Add("nume");
                            vListaUpdateValori.Add(vNume);
                        }
                        vDenumireRudenie = vTabel3["denumireRudenie"].ToString();
                        if (contineDiacritice(vTabel3, "denumireRudenie") && !contineDiacritice(vTabel, "denumireRudenie") && egalitateCampuri(vTabel["denumireRudenie"].ToString(), vDenumireRudenie))
                        {
                            vListaUpdate.Add("denumireRudenie");
                            vListaUpdateValori.Add(vDenumireRudenie);
                        }
                        vMentiuni = vTabel3["mentiuni"].ToString();
                        if (contineDiacritice(vTabel3, "mentiuni") && !contineDiacritice(vTabel, "mentiuni") && egalitateCampuri(vTabel["mentiuni"].ToString(), vMentiuni))
                        {
                            vListaUpdate.Add("mentiuni");
                            vListaUpdateValori.Add(vMentiuni);
                        }
                    }
                    vTabel3.Close();
                    if (vListaUpdate.Count > 0)
                    {
                        vUpdate = "update membri set ";
                        for (int a = 0; a < vListaUpdate.Count; a++)
                            vUpdate += vListaUpdate[a] + " = N'" + vListaUpdateValori[a] + "', ";
                        vUpdate = vUpdate.Remove(vUpdate.LastIndexOf(", "), vUpdate.Length - vUpdate.LastIndexOf(", "));
                        vUpdate += " where capitolId=" + vTabel["capitolId"].ToString();
                        vInterogare += vUpdate + "; ";
                        vCount++;
                        //vCmd4.CommandText = vInterogare;
                        //vCmd4.ExecuteScalar();
                        //vCmd4.ExecuteNonQuery();
                    }
                    //if (vCount >= 500)
                    //{
                    //    vCmd1.CommandText = vInterogare;
                    //    vCmd1.ExecuteNonQuery();
                    //    vInterogare = "";
                    //    vCount = 0;
                    //}
                }
            }
            vTabel.Close();
            vCount = 0;
            if (vInterogare != "")
            {
                vCmd1.CommandText = vInterogare;
                vCmd1.ExecuteNonQuery();
                vInterogare = "";
            }
            /*if (vInterogare != "")
            {
                vCmd1.CommandText = vInterogare;
                vCmd1.ExecuteNonQuery();
                vInterogare = "";
            }*/
            // parcele
            vCmd2.CommandText = "SELECT a.parcelaDenumire AS parcelaDenumire1, a.parcelaMentiuni AS parcelaMentiuni1, a.parcelaAdresa AS parcelaAdresa1, a.parcelaLocalitate AS parcelaLocalitate1, a.parcelaTarla AS parcelaTarla1, a.parcelaTitluProprietate AS parcelaTitluProprietate1, b.parcelaDenumire AS parcelaDenumire2, b.parcelaMentiuni AS parcelaMentiuni2, b.parcelaAdresa AS parcelaAdresa2, b.parcelaLocalitate AS parcelaLocalitate2, b.parcelaTarla AS parcelaTarla2, b.parcelaTitluProprietate AS parcelaTitluProprietate2, a.parcelaId FROM parcele AS a INNER JOIN parcele AS b ON a.parcelaIdInitial = b.parcelaIdInitial AND b.an = " + tbAn.Text + " - 1 WHERE (a.unitateaId = '" + vUnitate + "') AND (a.an = '" + tbAn.Text + "')";
            vTabel = vCmd2.ExecuteReader();
            while (vTabel.Read())
            {
                List<string> vListaUpdate = new List<string>();
                if (contineDiacritice(vTabel, "parcelaDenumire2") && !contineDiacritice(vTabel, "parcelaDenumire1") && egalitateCampuri(vTabel, "parcelaDenumire1", "parcelaDenumire2"))
                    vListaUpdate.Add("parcelaDenumire");
                if (contineDiacritice(vTabel, "parcelaMentiuni2") && !contineDiacritice(vTabel, "parcelaMentiuni1") && egalitateCampuri(vTabel, "parcelaMentiuni1", "parcelaMentiuni2"))
                    vListaUpdate.Add("parcelaMentiuni");
                if (contineDiacritice(vTabel, "parcelaAdresa2") && !contineDiacritice(vTabel, "parcelaAdresa1") && egalitateCampuri(vTabel, "parcelaAdresa1", "parcelaAdresa2"))
                    vListaUpdate.Add("parcelaAdresa");
                if (contineDiacritice(vTabel, "parcelaLocalitate2") && !contineDiacritice(vTabel, "parcelaLocalitate1") && egalitateCampuri(vTabel, "parcelaLocalitate1", "parcelaLocalitate2"))
                    vListaUpdate.Add("parcelaLocalitate");
                if (contineDiacritice(vTabel, "parcelaTarla2") && !contineDiacritice(vTabel, "parcelaTarla1") && egalitateCampuri(vTabel, "parcelaTarla1", "parcelaTarla2"))
                    vListaUpdate.Add("parcelaTarla");
                if (contineDiacritice(vTabel, "parcelaTitluProprietate2") && !contineDiacritice(vTabel, "parcelaTitluProprietate1") && egalitateCampuri(vTabel, "parcelaTitluProprietate1", "parcelaTitluProprietate2"))
                    vListaUpdate.Add("parcelaTitluProprietate");
                if (vListaUpdate.Count > 0)
                {
                    vUpdate = "update parcele set ";
                    foreach (string a in vListaUpdate)
                        vUpdate += a + " = N'" + vTabel[a + "2"].ToString() + "', ";
                    vUpdate = vUpdate.Remove(vUpdate.LastIndexOf(", "), vUpdate.Length - vUpdate.LastIndexOf(", "));
                    vUpdate += " where parcelaId=" + vTabel["parcelaId"].ToString();
                    vInterogare += vUpdate + "; ";
                    vCount++;
                }
                //if (vCount >= 500)
                //{
                //    vCmd1.CommandText = vInterogare;
                //    vCmd1.ExecuteNonQuery();
                //    vInterogare = "";
                //    vCount = 0;
                //}
            }
            vTabel.Close();
            if (vInterogare != "")
            {
                vCmd1.CommandText = vInterogare;
                vCmd1.ExecuteNonQuery();
                vInterogare = "";
            }
            vCmd1.CommandText = "INSERT INTO tempMihaiUnitati (unitateId, unitateDenumire, tip, data) VALUES ('" + vUnitate + "', N'" + vUnitateDenumire + "', 'FINAL', convert(datetime,'" + DateTime.Now + "',104))";
            vCmd1.ExecuteNonQuery();
        }
        ManipuleazaBD.InchideConexiune(vCon);
        gvUnitatiFacute.DataBind();
        valCustom.IsValid = false;
        valCustom.ErrorMessage = "Am terminat repararea de diacritice";
    }
    private bool egalitateCampuri(SqlDataReader pTabel, string pCamp1, string pCamp2)
    {
        bool vRezultat = false;
        List<ListItem> vListaDiacritice = new List<ListItem>();
        vListaDiacritice.Add(new ListItem("Ş", "S"));
        vListaDiacritice.Add(new ListItem("ş", "s"));
        vListaDiacritice.Add(new ListItem("Ţ", "T"));
        vListaDiacritice.Add(new ListItem("ţ", "t"));
        vListaDiacritice.Add(new ListItem("Â", "A"));
        vListaDiacritice.Add(new ListItem("â", "a"));
        vListaDiacritice.Add(new ListItem("Ă", "A"));
        vListaDiacritice.Add(new ListItem("ă", "a"));
        vListaDiacritice.Add(new ListItem("Î", "I"));
        vListaDiacritice.Add(new ListItem("î", "i"));
        string vCamp1 = pTabel[pCamp1].ToString(), vCamp2 = pTabel[pCamp2].ToString();
        foreach (ListItem a in vListaDiacritice)
        {
            vCamp1 = vCamp1.Replace(a.Text, a.Value);
            vCamp2 = vCamp2.Replace(a.Text, a.Value);
        }
        if (vCamp1 == vCamp2)
            vRezultat = true;
        return vRezultat;
    }
    private bool egalitateCampuri(string pCamp1, string pCamp2)
    {
        bool vRezultat = false;
        List<ListItem> vListaDiacritice = new List<ListItem>();
        vListaDiacritice.Add(new ListItem("Ş", "S"));
        vListaDiacritice.Add(new ListItem("ş", "s"));
        vListaDiacritice.Add(new ListItem("Ţ", "T"));
        vListaDiacritice.Add(new ListItem("ţ", "t"));
        vListaDiacritice.Add(new ListItem("Â", "A"));
        vListaDiacritice.Add(new ListItem("â", "a"));
        vListaDiacritice.Add(new ListItem("Ă", "A"));
        vListaDiacritice.Add(new ListItem("ă", "a"));
        vListaDiacritice.Add(new ListItem("Î", "I"));
        vListaDiacritice.Add(new ListItem("î", "i"));
        foreach (ListItem a in vListaDiacritice)
        {
            pCamp1 = pCamp1.Replace(a.Text, a.Value);
            pCamp2 = pCamp2.Replace(a.Text, a.Value);
        }
        if (pCamp1 == pCamp2)
            vRezultat = true;
        return vRezultat;
    }
    private bool contineDiacritice(SqlDataReader pTabel, string pCamp)
    {
        bool vRezultat = false;
        List<string> vListaDiacritice = new List<string>();
        vListaDiacritice.Add("Ş");
        vListaDiacritice.Add("ş");
        vListaDiacritice.Add("Ţ");
        vListaDiacritice.Add("ţ");
        vListaDiacritice.Add("Â");
        vListaDiacritice.Add("â");
        vListaDiacritice.Add("Ă");
        vListaDiacritice.Add("ă");
        vListaDiacritice.Add("Î");
        vListaDiacritice.Add("î");
        foreach (string a in vListaDiacritice)
        {
            if (pTabel[pCamp].ToString().Contains(a))
                vRezultat = true;
        }
        return vRezultat;
    }
}
