﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class raportCapitole : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            capitoleDropDown.DataSource = GetCapitole();
            capitoleDropDown.DataValueField = "capitol";
            capitoleDropDown.DataBind();
        }
    }

    public DataTable GetCapitole()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        string interogare = @"select distinct(capitol) from sabloaneCapitole 
WHERE        (an= " + Session["SESAn"].ToString() + @")
            order by capitol";
        SqlDataAdapter adapter = new SqlDataAdapter(interogare, connection);
        DataTable table = new DataTable();
        adapter.Fill(table);
        ManipuleazaBD.InchideConexiune(connection);
        return table;
    }

    protected void btTiparire_Click(object sender, EventArgs e)
    {
        ResponseHelper.Redirect("~/printRaportCapitole.aspx?capitol=" + capitoleDropDown.SelectedValue + "","_new","");
    }
}