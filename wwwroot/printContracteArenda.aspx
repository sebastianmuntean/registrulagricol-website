﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="printContracteArenda.aspx.cs" Inherits="printContracteArenda" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" 
    Font-Size="8pt" Height="400px" Width="400px">
        <LocalReport ReportPath="rapoarte\raportContracteArenda.rdlc">
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" 
                    Name="dsRapoarte_dtUnitati" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource2" 
                    Name="dsRapoarte_dtRapContracteArenda" />
            </DataSources>
        </LocalReport>
    </rsweb:ReportViewer>
<asp:ObjectDataSource ID="ObjectDataSource2" runat="server" 
    InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" 
    SelectMethod="GetData" 
    TypeName="dsRapoarteTableAdapters.dtRapContracteArendaTableAdapter">
    <SelectParameters>
        <asp:SessionParameter Name="utilizatorId" SessionField="SESutilizatorId" 
            Type="Int32" />
        <asp:SessionParameter Name="ordonare" SessionField="rcaOrdonare" 
            Type="String" />
    </SelectParameters>
    <InsertParameters>
        <asp:Parameter Name="unitateId" Type="Int32" />
        <asp:Parameter Name="utilizatorId" Type="Int32" />
        <asp:Parameter Name="nrContract" Type="String" />
        <asp:Parameter Name="dataContract" Type="DateTime" />
        <asp:Parameter Name="gospodarieIdDa" Type="Int64" />
        <asp:Parameter Name="gospodarieIdPrimeste" Type="Int64" />
        <asp:Parameter Name="detaliiContract" Type="String" />
        <asp:Parameter Name="volumDa" Type="String" />
        <asp:Parameter Name="nrPozitieDa" Type="String" />
        <asp:Parameter Name="numeDa" Type="String" />
        <asp:Parameter Name="volumPrimeste" Type="String" />
        <asp:Parameter Name="nrPozitiePrimeste" Type="String" />
        <asp:Parameter Name="numePrimeste" Type="String" />
        <asp:Parameter Name="detaliiParcela" Type="String" />
        <asp:Parameter Name="ha" Type="Int32" />
        <asp:Parameter Name="ari" Type="Decimal" />
        <asp:Parameter Name="contractId" Type="Int64" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
    OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
    TypeName="dsRapoarteTableAdapters.dtUnitatiTableAdapter">
    <SelectParameters>
        <asp:SessionParameter Name="unitateId" SessionField="rcaUnitateId" 
            Type="Int32" />
    </SelectParameters>
</asp:ObjectDataSource>
</asp:Content>

