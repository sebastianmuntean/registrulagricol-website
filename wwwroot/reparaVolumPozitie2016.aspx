﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="reparaVolumPozitie2016.aspx.cs" Inherits="reparaVolumPozitie2016" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link type="text/css" href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <script type="text/jscript" src="bootstrap/js/jquery.min.js"></script>
    <script type="text/jscript" src="bootstrap/js/bootstrap.min.js"></script>
    <style type="text/css">
        body {
            font-family: unset;
            font-size: unset;
            line-height: 1.42857143;
            color: black;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    Modificare volum şi poziţie
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <%--    <asp:UpdatePanel ID="upPostBack" runat="server">
        <ContentTemplate>--%>
    <asp:Panel ID="pnListaGospodarii" runat="server" CssClass="panel_general" Visible="true">
        <asp:Panel ID="Panel4" runat="server">
            <asp:Label ID="lblEroare1" runat="server" CssClass="validator" Font-Size="15px" Text="Eroare" Visible="false" />
        </asp:Panel>
        <asp:Panel ID="pnCautare" runat="server" CssClass="cauta">
            <asp:Label ID="lbCautaDupa" runat="server" Text="Caută după:"></asp:Label>
            <%--  <asp:Label ID="lblUnitate" runat="server" Text=""></asp:Label>
            <asp:DropDownList ID="ddlUnitate" Width="177px" AutoPostBack="True" runat="server"
                OnInit="ddlfUnitati_Init" OnPreRender="ddlUnitate_PreRender" OnSelectedIndexChanged="ddlUnitate_SelectedIndexChanged">
            </asp:DropDownList>--%>
            <asp:Label ID="lbfVolum" runat="server" Text="Vol."></asp:Label>
            <asp:TextBox ID="tbfVolum" Width="47px" runat="server" AutoPostBack="True" OnTextChanged="ReIncarcaGrid" Style="padding-right: 0; margin-right: 0"></asp:TextBox><asp:DropDownList ID="ddlModSelectVol" runat="server" AutoPostBack="true" Style="padding-left: 0; margin-left: 0; height: 1.6em;">
                <asp:ListItem Text="conține" Value="%X%"></asp:ListItem>
                <asp:ListItem Text="exact" Value="X"></asp:ListItem>
                <asp:ListItem Text="începe cu" Value="X%"></asp:ListItem>
                <asp:ListItem Text="se termină cu" Value="%X"></asp:ListItem>
            </asp:DropDownList>
            <asp:Label ID="lbfNrPoz" runat="server" Text="Poz."></asp:Label>
            <asp:TextBox ID="tbfNrPoz" Width="47px" runat="server" AutoPostBack="True" OnTextChanged="ReIncarcaGrid"></asp:TextBox>
            <asp:Label ID="ldfNume" runat="server" Text="Nume"></asp:Label>
            <asp:TextBox ID="tbfNume" Width="100px" runat="server" AutoPostBack="True" OnTextChanged="ReIncarcaGrid"></asp:TextBox>
            <asp:Label ID="lbfLoc" runat="server" Text="Loc."></asp:Label>
            <asp:TextBox ID="tbfLoc" Width="110px" runat="server" AutoPostBack="True" OnTextChanged="ReIncarcaGrid"></asp:TextBox>
        </asp:Panel>
        <asp:Panel ID="Panel5" runat="server" CssClass="cauta">
            <asp:Label ID="lblFTip" runat="server" Text="Tip"></asp:Label>
            <asp:DropDownList ID="ddlFTip" AutoPostBack="true" runat="server" Width="110px" OnSelectedIndexChanged="ReIncarcaGrid">
                <asp:ListItem Value="%">Toate</asp:ListItem>
                <asp:ListItem Value="1">Localnic</asp:ListItem>
                <asp:ListItem Value="2">Străinaş</asp:ListItem>
                <asp:ListItem Value="3">Firmă pe raza localităţii</asp:ListItem>
                <asp:ListItem Value="4">Firmă străinaşă</asp:ListItem>
            </asp:DropDownList>
            <asp:Label ID="lblOrdonareDupa" runat="server" Text="Ordonare"></asp:Label>
            <asp:DropDownList AutoPostBack="true" ID="ddlFOrdonareDupa" runat="server" OnSelectedIndexChanged="ReIncarcaGrid">
                <asp:ListItem Text="Volum, nr. poziţie" Value="0"></asp:ListItem>
                <asp:ListItem Text="Localitate, stradă, nr." Value="1"></asp:ListItem>
                <asp:ListItem Text="Nume" Value="2"></asp:ListItem>
                <asp:ListItem Text="ID crescător" Value="3"></asp:ListItem>
                <asp:ListItem Text="ID descrescător" Value="4"></asp:ListItem>
            </asp:DropDownList>
            <asp:Label ID="lbNrLinii" runat="server" Text="Nr. linii"></asp:Label>
            <asp:DropDownList ID="ddNrLinii" AutoPostBack="True" runat="server" OnSelectedIndexChanged="ReIncarcaGrid">
                <asp:ListItem Selected="True">10</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>50</asp:ListItem>
                <asp:ListItem>100</asp:ListItem>
                <asp:ListItem Value="999999">Toate</asp:ListItem>
            </asp:DropDownList>
            <asp:Label ID="lbfStrada" runat="server" Text="Str."></asp:Label>
            <asp:TextBox ID="tbfStrada" Width="100px" runat="server" AutoPostBack="True" OnTextChanged="ReIncarcaGrid"></asp:TextBox>
            <asp:Label ID="lbfNr" runat="server" Text="Nr."></asp:Label>
            <asp:TextBox ID="tbfNr" Width="30px" Style="padding-right: 0; margin-right: 0" runat="server" AutoPostBack="True" OnTextChanged="ReIncarcaGrid"></asp:TextBox><asp:DropDownList ID="ddlModSelectNr" runat="server" AutoPostBack="true" Style="padding-left: 0; margin-left: 0; height: 1.6em;">
                <asp:ListItem Text="conține" Value="%X%"></asp:ListItem>
                <asp:ListItem Text="exact" Value="X"></asp:ListItem>
                <asp:ListItem Text="începe cu" Value="X%"></asp:ListItem>
                <asp:ListItem Text="se termină cu" Value="%X"></asp:ListItem>
            </asp:DropDownList>
        </asp:Panel>

        <asp:Panel ID="pnButoanePrincipale" CssClass="butoane" runat="server">
        </asp:Panel>

        <%--  --%>
        <asp:Panel ID="pnLBMesajAtentionareModificari" runat="server" Visible="true">
            <asp:Label ID="lbMesaj" runat="server" Text="Atentie! Modificarile vor fi salvate in baza de date doar la apasarea butonului Salveaza Modificari!" ForeColor="Red" Font-Size="Medium"></asp:Label>
        </asp:Panel>
        <asp:Panel ID="pnGVGospodarii" runat="server" Visible="true">

            <asp:GridView ID="gvRVP" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                BorderStyle="None" CellPadding="3" GridLines="Vertical" ShowFooter="True" CssClass="tabela"
                DataKeyNames="gospodarieId" EmptyDataText="Nu sunt adaugate inregistrari"
                AllowPaging="True" OnPageIndexChanging="gvGospodariiPaginare" OnRowDataBound="gvRVP_RowDataBound">
                <Columns>
                    <%--<asp:CommandField ShowSelectButton="true" SelectText="selecteaza" ButtonType="Button" />--%>
                    <asp:TemplateField HeaderText="Vol. / Vol.inițial" SortExpression="volumInt">
                        <ItemTemplate>
                            <asp:Label ID="lblGospodarieId" Visible="false" runat="server" Text='<%# Bind("gospodarieId") %>'></asp:Label>
                            <asp:Label ID="lblNeregularitate" Visible="false" runat="server" Text='<%# Bind("Neregularitate") %>'></asp:Label>
                            <asp:TextBox ID="tbVolum" OnTextChanged="tbChangeVerifica" AutoPostBack="true" Width="50px" runat="server" Text='<%# Bind("volum") %>' ToolTip='<%# Bind("volum") %>'></asp:TextBox>
                            /
                            <asp:Label ID="lblVolumVechi" runat="server" Text='<%# Bind("volumLaIncarcareGrid") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Nr.poz. / Nr.poz. inițial" SortExpression="nrPozitieInt">
                        <EditItemTemplate>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:TextBox ID="tbNrPozitie" runat="server" AutoPostBack="true" Text='<%# Bind("nrPozitie") %>' ToolTip='<%# Bind("nrPozitie") %>' Width="50px" OnTextChanged="tbChangeVerifica"></asp:TextBox>
                            /
                            <asp:Label ID="lblNrPozitieVechi" runat="server" Text='<%# Bind("nrPozitieLaIncarcareGrid") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="membrunume" HeaderText="Nume" SortExpression="membrunume" />
                    <asp:BoundField DataField="codSiruta" HeaderText="Cod şiruta" SortExpression="codSiruta" />
                    <asp:BoundField DataField="tip" HeaderText="Tip" SortExpression="tip" />
                    <asp:BoundField DataField="strada" HeaderText="Strada" SortExpression="strada" />
                    <asp:BoundField DataField="nr" HeaderText="Număr" SortExpression="nr" />
                    <asp:BoundField DataField="nrInt" HeaderText="Număr întreg" SortExpression="nrInt" />
                    <asp:BoundField DataField="codExploatatie" HeaderText="Cod exploataţie" SortExpression="codExploatatie" />
                    <asp:BoundField DataField="codUnic" HeaderText="Cod unic" SortExpression="codUnic" />
                    <asp:BoundField DataField="judet" HeaderText="Judeţ" SortExpression="judet" />
                    <asp:BoundField DataField="localitate" HeaderText="Localitate" SortExpression="localitate" />
                </Columns>
            </asp:GridView>

            <asp:Button ID="bt_SalveazaModificari" runat="server" Text="Salveaza modificarile in baza de date" CssClass="btn btn-success" OnClick="bt_SalveazaModificari_Click" />
            <asp:Button ID="bt_RenuntaModificari" runat="server" Text="Renunta la toate modificarile" CssClass="btn btn-danger" OnClick="bt_RenuntaModificari_Click" OnClientClick="if ( ! RenuntaModoficari()) return false;" meta:resourcekey="bt_RenuntaModificariResource1" />
        </asp:Panel>

    </asp:Panel>
    <%--        </ContentTemplate>
    </asp:UpdatePanel>--%>
    <script type="text/javascript">
        function openModal() {
            $('#ctl00_ContentPlaceHolder1_ModalVolumPoz').modal('show');
        }
        function hideModal() {
            $('#ctl00_ContentPlaceHolder1_ModalVolumPoz').modal('hide');
            $(this).data('#ctl00_ContentPlaceHolder1_ModalVolumPoz', null);
        }

    </script>
    <!-- Modal -->

    <div class="modal fade in" runat="server" id="ModalVolumPoz" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document" runat="server" style="width: 80%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Următoarele gospodării sunt în conflict:</h4>
                </div>
                <div class="modal-body">
                    <%--  --%>
                    <%--                    <asp:UpdatePanel ID="UPModalGV" runat="server" UpdateMode="Always">
                        <ContentTemplate>--%>
                    <asp:Panel runat="server" CssClass="table-responsive">
                        <asp:GridView ID="gvModal" runat="server" AutoGenerateColumns="False"
                            BorderStyle="None" CellPadding="3" GridLines="Vertical" ShowFooter="True" CssClass="tabela table"
                            DataKeyNames="gospodarieId" EmptyDataText="Nu sunt adaugate inregistrari" OnRowDataBound="gvModal_DataBound">
                            <Columns>
                                <%--<asp:CommandField ShowSelectButton="true" SelectText="selecteaza" ButtonType="Button" />--%>

                                <asp:TemplateField HeaderText="Volum" SortExpression="volumInt">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGospodarieId" Visible="false" runat="server" Text='<%# Bind("gospodarieId") %>'></asp:Label>
                                        <asp:Label ID="lblNeregularitate" Visible="true" runat="server" Text='<%# Bind("Neregularitate") %>'></asp:Label>
                                        <asp:TextBox ID="tbVolum" AutoPostBack="true" Width="50px" runat="server" Text='<%# Bind("volum") %>' ToolTip='<%# Bind("volum") %>' OnTextChanged="tbChangeModalVerifica"> </asp:TextBox>
                                        /
                                                <asp:Label ID="lblVolumVechi" runat="server" Text='<%# Bind("volumLaIncarcareGrid") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Număr poziţie" SortExpression="nrPozitieInt">
                                    <EditItemTemplate>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:TextBox ID="tbNrPozitie" runat="server" AutoPostBack="true" Text='<%# Bind("nrPozitie") %>' ToolTip='<%# Bind("nrPozitie") %>' Width="50px" OnTextChanged="tbChangeModalVerifica"></asp:TextBox>
                                        /
                            <asp:Label ID="lblNrPozitieVechi" runat="server" Text='<%# Bind("nrPozitieLaIncarcareGrid") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="membrunume" HeaderText="Nume" SortExpression="membrunume" />
                                <asp:BoundField DataField="codSiruta" HeaderText="Cod şiruta" SortExpression="codSiruta" />
                                <asp:BoundField DataField="tip" HeaderText="Tip" SortExpression="tip" />
                                <asp:BoundField DataField="strada" HeaderText="Strada" SortExpression="strada" />
                                <asp:BoundField DataField="nr" HeaderText="Număr" SortExpression="nr" />
                                <asp:BoundField DataField="nrInt" HeaderText="Număr întreg" SortExpression="nrInt" />
                                <asp:BoundField DataField="codExploatatie" HeaderText="Cod exploataţie" SortExpression="codExploatatie" />
                                <asp:BoundField DataField="codUnic" HeaderText="Cod unic" SortExpression="codUnic" />
                                <asp:BoundField DataField="judet" HeaderText="Judeţ" SortExpression="judet" />
                                <asp:BoundField DataField="localitate" HeaderText="Localitate" SortExpression="localitate" />
                            </Columns>

                        </asp:GridView>
                    </asp:Panel>
                    <%--                        </ContentTemplate>
                    </asp:UpdatePanel>--%>
                </div>
                <asp:UpdatePanel ID="UPModalFooter" runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <div class="modal-footer">
                            <asp:Panel ID="pnModalButoanePrincipale" runat="server" CssClass="block">
                            </asp:Panel>
                            <asp:Panel ID="pnModalButoaneMesaj" runat="server" CssClass="block">
                            </asp:Panel>
                            <asp:Button ID="bt_OKModal" runat="server" Text="OK" CssClass="btn btn-succes" OnClick="bt_ModalOK" />
                            <asp:Button ID="bt_ValidezMaiTarziuModal" runat="server" Text="Validez mai tarziu - pastrez modificarile" CssClass="btn btn-danger" OnClick="bt_ValidezMaiTarziuModal_Click" />
                            <asp:Button ID="bt_RenuntaModal" runat="server" Visible="false" Text="Renunta la ultimele modificari" CssClass="btn btn-danger" OnClick="TerminaLucruCuModal" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

    <asp:Button ID="bt_ShowModal" runat="server" Text="Button" OnClick="bt_ShowModal_Click" Style="display: none" />
    <script type="text/javascript">
        function clickme() {
            $('#<%= bt_ShowModal.ClientID %>').click();
        }
        function RenuntaModoficari() {
            return confirm("Ultimele modificari efectuate nu au fost salvate in baza de date si vor fi pierdute la apasarea butonului OK. Sigur doriti sa renuntati?");
        }
    </script>
</asp:Content>
