﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class printCapitolul15 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataTable reportDataTable = GetContracte();
            DataTable unitatiDataTable = GetUnitateDenumire();
            DataTable gospodariiDataTable = GetGospodarie();
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource ds = new ReportDataSource("dsRapoarte_Cap15DataTable", reportDataTable);
            ReportDataSource ds2 = new ReportDataSource("dsRapoarte_dtUnitati", unitatiDataTable);
            ReportDataSource ds3 = new ReportDataSource("dsRapoarte_dtGospodarii", gospodariiDataTable);
            ReportViewer1.LocalReport.DataSources.Add(ds);
            ReportViewer1.LocalReport.DataSources.Add(ds2);
            ReportViewer1.LocalReport.DataSources.Add(ds3);
            ReportViewer1.LocalReport.Refresh();
            ReportViewer1.Visible = true;
        }
    }
    protected void Page_SaveStateComplete(object sender, EventArgs e)
    {
        Warning[] warnings;
        string[] streamids;
        string mimeType;
        string encoding;
        string fileNameExtension;
        byte[] bytes = ReportViewer1.LocalReport.Render("PDF", null, out mimeType, out encoding, out fileNameExtension, out streamids, out warnings);
        Response.Buffer = true;
        Response.Clear();
        Response.ContentType = mimeType;
        Response.AddHeader("content-disposition", "inline; filename=Nume" + fileNameExtension);
        Response.BinaryWrite(bytes);
        Response.Flush();
        Response.End();
        Response.Close();
    }
    public DataTable GetContracte()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        string interogare = @"SELECT gospodarii_1.gospodarieId AS idDa, gospodarii.gospodarieId AS idPrimeste, parceleAtribuiriContracte.contractId, parceleAtribuiriContracte.contractNr, parceleAtribuiriContracte.contractDataSemnarii, parceleAtribuiriContracte.contractGospodarieIdDa, parceleAtribuiriContracte.contractGospodarieIdPrimeste, parceleAtribuiriContracte.contractDataInceput, parceleAtribuiriContracte.contractDataFinal, 
CASE gospodarii_1.persJuridica WHEN 0 THEN gospodarii_1.membruNume ELSE gospodarii_1.jUnitate END  AS gospodarie1, 
CASE gospodarii.persJuridica WHEN 0 THEN gospodarii.membruNume ELSE gospodarii.jUnitate END   AS gospodarie, sabloaneCapitole.denumire1,
                        Convert(nvarchar(20),Convert(decimal(18,4),(SUM(parceleModUtilizare.c3Ha) + ROUND(SUM(parceleModUtilizare.c3Ari) / 100, 0, 1) +( SUM(parceleModUtilizare.c3Ari) - ROUND(SUM(parceleModUtilizare.c3Ari) / 100, 0, 1) * 100 )/ 100)))
+ ' / ' +
(CASE parcele.parcelaCategorie
   WHEN 1 THEN 'Teren arabil (inclusiv sere si solarii)'
   WHEN 2 THEN 'Pasuni naturale' 
   WHEN 3 THEN 'Fânete naturale'
   WHEN 4 THEN 'Vii, pepiniere viticole si hameisti'
   WHEN 7 THEN 'Livezi de pomi, pepiniere, arbusti fructiferi'
   WHEN 9 THEN 'Gradini familiale'
   WHEN 11 THEN 'Paduri si alte terenuri cu vegetatie forestiera'
   WHEN 13 THEN 'Drumuri si cai ferate'
   WHEN 14 THEN 'Constructii'
   WHEN 15 THEN 'Terenuri degradate si neproductive'
   WHEN 16 THEN 'Ape si balti' END)
AS ha, 
                     redeventa,parcele.parcelaNrBloc
                     FROM sabloaneCapitole INNER JOIN
                          parceleModUtilizare ON sabloaneCapitole.codRand = parceleModUtilizare.c3Rand 
                    INNER JOIN parcele on parceleModUtilizare.parcelaId = parcele.parcelaid
                    INNER JOIN gospodarii 
                    INNER JOIN parceleAtribuiriContracte ON gospodarii.gospodarieId = parceleAtribuiriContracte.contractGospodarieIdPrimeste 
                    INNER JOIN gospodarii AS gospodarii_1 ON parceleAtribuiriContracte.contractGospodarieIdDa = gospodarii_1.gospodarieId ON parceleModUtilizare.contractId = parceleAtribuiriContracte.contractId WHERE (sabloaneCapitole.capitol = '3') AND (gospodarii.gospodarieId = " + Session["SESGospodarieId"] + " or gospodarii_1.gospodarieId = " + Session["SESGospodarieId"] + @") AND (sabloaneCapitole.an = " + Session["SESAn"] + ")  AND (sabloaneCapitole.an =" + Session["SESAn"] + ")  AND  YEAR(parceleAtribuiriContracte.contractDataFinal) >= " + HttpContext.Current.Session["SESAn"] + @"  and contractTip in (3,10)
        GROUP BY gospodarii_1.gospodarieId,parcele.parcelaNrBloc, gospodarii.gospodarieId, parceleAtribuiriContracte.contractId, parceleAtribuiriContracte.contractNr, parceleAtribuiriContracte.contractDataSemnarii, parceleAtribuiriContracte.contractGospodarieIdDa, parceleAtribuiriContracte.contractGospodarieIdPrimeste, parceleAtribuiriContracte.contractDataInceput, parceleAtribuiriContracte.contractDataFinal, gospodarii_1.membruNume,  gospodarii.persJuridica,gospodarii.jUnitate,gospodarii_1.persJuridica,gospodarii_1.jUnitate,gospodarii.membruNume,parcele.parcelaCategorie, sabloaneCapitole.denumire1,redeventa 
ORDER BY sabloaneCapitole.denumire1";
        SqlDataAdapter adapter = new SqlDataAdapter(interogare, connection);
        DataTable table = new DataTable();
        adapter.Fill(table);
        ManipuleazaBD.InchideConexiune(connection);
        return table;

    }

    private DataTable GetGospodarie()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        string interogare = @"SELECT        gospodarieId, unitateId, volum, nrPozitie, codSiruta, tip, COALESCE (strada, '') AS strada, COALESCE (nr, '') AS nr, nrInt, COALESCE (bl, '') AS bl, 
                         COALESCE (sc, '') AS sc, COALESCE (et, '') AS et, COALESCE (ap, '') AS ap, codExploatatie, codUnic, judet, localitate, persJuridica, jUnitate, jSubunitate, 
                         jCodFiscal, jNumeReprez, strainas, sStrada, sNr, sBl, sSc, sEtj, sAp, sJudet, sLocalitate,
                             (SELECT        TOP (1) nume
                               FROM            membri
                               WHERE        (gospodarieId = gospodarii.gospodarieId)
                               ORDER BY codRudenie) AS Membru,
                             (SELECT        TOP (1) cnp
                               FROM            membri AS membri_1
                               WHERE        (gospodarieId = gospodarii.gospodarieId)
                               ORDER BY codRudenie) AS cnpMembru
FROM            gospodarii
where an = " + Session["SESAn"] + " and gospodarieId = " + Session["SESGospodarieid"] + "";
        SqlDataAdapter adapter = new SqlDataAdapter(interogare, connection);
        DataTable table = new DataTable();
        adapter.Fill(table);
        ManipuleazaBD.InchideConexiune(connection);
        return table;
    }

    private DataTable GetUnitateDenumire()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        string interogare = @"SELECT        unitati.unitateId, unitati.unitateDenumire, unitati.unitateCodSiruta, unitati.judetId, unitati.localitateId, unitati.unitateStrada, unitati.unitateNr, unitati.unitateAp, 
                         unitati.unitateCodPostal, unitati.localitateComponentaId, unitati.unitateCodFiscal, unitati.unitateActiva, unitati.unitatePrincipala, judete.judetDenumire, 
                         localitati.localitateDenumire
FROM            unitati INNER JOIN
                         judete ON unitati.judetId = judete.judetId INNER JOIN
                         localitati ON unitati.localitateId = localitati.localitateId
WHERE        (unitati.unitateId = " + Session["SESunitateId"].ToString() + ")";
        SqlDataAdapter adapter = new SqlDataAdapter(interogare, connection);
        DataTable table = new DataTable();
        adapter.Fill(table);
        ManipuleazaBD.InchideConexiune(connection);
        return table;
    }
}