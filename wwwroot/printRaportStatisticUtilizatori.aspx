﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="printRaportStatisticUtilizatori.aspx.cs" Inherits="printRaportStatisticUtilizatori" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <rsweb:ReportViewer ID="rvRaportStatisitcUtilizatori" runat="server" 
        Font-Names="Verdana" Font-Size="8pt" Height="400px" Width="400px">
        <LocalReport ReportPath="rapoarte\raportStatisticUtilizatori.rdlc">
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" 
                    Name="dsRapoarte_dtRapStatisticUtilizatori" />
            </DataSources>
        </LocalReport>
    </rsweb:ReportViewer>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
        DeleteMethod="Delete" InsertMethod="Insert" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="dsRapoarteTableAdapters.dtRapStatisticUtilizatoriTableAdapter" 
        UpdateMethod="Update">
        <DeleteParameters>
            <asp:Parameter Name="Original_rapStatisticId" Type="Int64" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="utilizatorId" Type="Int32" />
            <asp:Parameter Name="an" Type="Int32" />
            <asp:Parameter Name="luna" Type="Int32" />
            <asp:Parameter Name="lunaText" Type="String" />
            <asp:Parameter Name="data" Type="DateTime" />
            <asp:Parameter Name="unitateDenumire" Type="String" />
            <asp:Parameter Name="judetDenumire" Type="String" />
            <asp:Parameter Name="utilizatoriNumePrenume" Type="String" />
            <asp:Parameter Name="gospodariiAdaugate" Type="Int32" />
            <asp:Parameter Name="capitoleModificate" Type="Int32" />
            <asp:Parameter Name="adaugari" Type="Int32" />
            <asp:Parameter Name="modificari" Type="Int32" />
            <asp:Parameter Name="stergeri" Type="Int32" />
            <asp:Parameter Name="vizualizari" Type="Int32" />
            <asp:Parameter Name="tipariri" Type="Int32" />
            <asp:Parameter Name="eroriSemnalate" Type="Int32" />
            <asp:Parameter Name="login" Type="Int32" />
            <asp:Parameter Name="Original_rapStatisticId" Type="Int64" />
        </UpdateParameters>
        <SelectParameters>
            <asp:SessionParameter Name="utilizatorId" SessionField="SESutilizatorId" 
                Type="Int32" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="utilizatorId" Type="Int32" />
            <asp:Parameter Name="an" Type="Int32" />
            <asp:Parameter Name="luna" Type="Int32" />
            <asp:Parameter Name="lunaText" Type="String" />
            <asp:Parameter Name="data" Type="DateTime" />
            <asp:Parameter Name="unitateDenumire" Type="String" />
            <asp:Parameter Name="judetDenumire" Type="String" />
            <asp:Parameter Name="utilizatoriNumePrenume" Type="String" />
            <asp:Parameter Name="gospodariiAdaugate" Type="Int32" />
            <asp:Parameter Name="capitoleModificate" Type="Int32" />
            <asp:Parameter Name="adaugari" Type="Int32" />
            <asp:Parameter Name="modificari" Type="Int32" />
            <asp:Parameter Name="stergeri" Type="Int32" />
            <asp:Parameter Name="vizualizari" Type="Int32" />
            <asp:Parameter Name="tipariri" Type="Int32" />
            <asp:Parameter Name="eroriSemnalate" Type="Int32" />
            <asp:Parameter Name="login" Type="Int32" />
        </InsertParameters>
    </asp:ObjectDataSource>
</asp:Content>

