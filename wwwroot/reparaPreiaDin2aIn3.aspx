﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="reparaPreiaDin2aIn3.aspx.cs" Inherits="reparaPreiaDin2aIn3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnAfiseazaMesaje" runat="server">
                <asp:ValidationSummary ID="valSumUtilizatori" runat="server" DisplayMode="SingleParagraph"
                    Visible="true" ValidationGroup="GrupValidareExport" CssClass="validator" ForeColor="" />
                <asp:CustomValidator ID="valCustom" runat="server" ErrorMessage="" ValidationGroup="GrupValidareExport"></asp:CustomValidator>
            </asp:Panel>
            <asp:Panel ID="pnListaLocalitati" runat="server" CssClass="panel_general" Visible="true">
                <asp:Panel ID="pnGrid" runat="server" Visible="true">
                    <h1>
                        Preia date din capitolul 2a in capitolul 3
                    </h1>
                    <p>
                        Din capitolul 2a se ia randul 10 si se pune in capitolul 3 la randurile 1 si 17
                    </p>
                    <p>
                        <asp:Label ID="lblUnitatea" runat="server" Text="Unitatea"></asp:Label>
                        <asp:DropDownList ID="ddlUnitate" runat="server" DataSourceID="SqlUnitati" DataTextField="denumire"
                            DataValueField="unitateId">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="SqlUnitati" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString %>"
                            SelectCommand="SELECT judete.judetDenumire + ' - ' + unitati.unitateDenumire AS denumire, unitati.unitateId FROM unitati INNER JOIN judete ON unitati.judetId = judete.judetId ORDER BY judete.judetDenumire, unitati.unitateDenumire">
                        </asp:SqlDataSource>
                    </p>
                    <p>
                        <asp:CheckBox ID="cbPreiaDoar0" Text="preia date doar daca in capitolul 3 nu sunt date"
                            runat="server" />
                    </p>
                    <asp:Panel ID="pnButoane" CssClass="butoane" runat="server">
                        <asp:Button CssClass="buton" ID="btAdauga" runat="server" Text="salveaza" OnClick="btAdauga_Click" />
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
