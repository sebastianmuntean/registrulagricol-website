﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

public partial class intretinere : System.Web.UI.Page
{
    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["SESutilizatorId"].ToString() != "52")
            Response.Redirect("https://tntregistruagricol.ro/Gospodarii.aspx");
        if (!IsPostBack)
        {
            tbAn.Text = Session["SESan"].ToString();
            tbAnCorecteazaCapitoleLiniiDublateSauLipsa.Text = Session["SESan"].ToString();
        }
    }
    protected void btAdaugaUmpleNuluri_Click(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandTimeout = 100000;
        int randuriAfectate = 0;
        using (vCon)
        {
            vCmd.CommandText = @"UPDATE  [tntcomputers].[capitole] SET 
            [col1] = CASE[col1] WHEN null THEN 0 ELSE[col1] END
            ,[col2] = CASE WHEN[col2] is null THEN 0 ELSE[col2] END
            ,[col3] = CASE WHEN[col3] is null THEN 0 ELSE[col3] END
            ,[col4] = CASE WHEN[col4] is null THEN 0 ELSE[col4] END
            ,[col5] = CASE WHEN[col5] is null THEN 0 ELSE[col5] END
            ,[col6] = CASE WHEN[col6] is null THEN 0 ELSE[col6] END
            ,[col7] = CASE WHEN[col7] is null THEN 0 ELSE[col7] END
            ,[col8] = CASE WHEN[col8] is null THEN 0 ELSE[col8] END
            ,[col9] = CASE WHEN[col9] is null THEN '' ELSE[col9] END
            ,[col10] = CASE WHEN[col10] is null THEN '' ELSE[col10] END

             WHERE
                an = " + tbAn.Text +
                    @"and unitateid = " + ddlUnitate.SelectedValue +
                    @" and(col1 is null or col2 is null or col3 is null or col4 is null or col5 is null or col6 is null or col7 is null or col8 is null or col9 is null or col10 is null) ";


            randuriAfectate = vCmd.ExecuteNonQuery();

        }
        valCustom.IsValid = false;
        valCustom.ErrorMessage = "S-au actualizat datele pentru un numar de " + randuriAfectate.ToString() + " randuri afectate in tabela capitole, pentru unitatea " + ddlUnitate.SelectedItem.Text + ", anul " + tbAn.Text;
        ClassLog.fLog(1, 52, DateTime.Now, "capitole", "umple nulluri", "S-au actualizat datele pentru un numar de " + randuriAfectate.ToString() + " randuri afectate in tabela capitole, pentru unitatea " + ddlUnitate.SelectedItem.Text + ", anul " + tbAn.Text, 0, 0);
    }
    protected void btCorecteazaMembri_Click(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandTimeout = 100000;
        int randuriAfectate = 0;
        string insert = "";
        List<List<string>> listaMembriFinala = new List<List<string>>();
        using (vCon)
        {
            // luam gospodariile din 2015 din care trebuie sa luam membri lipsa 
            string select = @"select g2015.gospodarieId, g2015.gospodarieIdInitial,
 (SELECT count(*) from [tntcomputers].membri where membri.gospodarieId = g2015.gospodarieId) as x2015
 ,
 (SELECT count(*) from [tntcomputers].membri where membri.gospodarieId  =(SELECT g2016.gospodarieId from tntcomputers.gospodarii as g2016 
                               where an = 2016 and g2016.gospodarieIdInitial = g2015.gospodarieIdInitial)) as x2016


				  from tntcomputers.gospodarii as g2015 where an = 2015 and unitateId = " + ddlUnitate.SelectedValue + @" and g2015.gospodarieIdInitial in 
                 (select g2016.gospodarieIdInitial
                  from tntcomputers.gospodarii as g2016 where an = 2016 and unitateId = " + ddlUnitate.SelectedValue + @" and 
                  (SELECT count(*) from [tntcomputers].membri where membri.gospodarieId = g2016.gospodarieId)   
                    !=
                  (SELECT count(*) from [tntcomputers].membri where coalesce(membri.decedat,0) <> 1 and membri.gospodarieId = (
                        SELECT g2015.gospodarieId from tntcomputers.gospodarii as g2015 
                               where an = 2015 and g2015.gospodarieIdInitial = g2016.gospodarieIdInitial
                    )))
	                
	                ";
            List<List<string>> listaGospodariiDin2015 = ManipuleazaBD.fRezultaListaStringuri(select, new List<string>() { "gospodarieId", "gospodarieIdInitial" }, 2016);
            ClassLog.fLog(1, 52, DateTime.Now, "membri", "unit. " + ddlUnitate.SelectedValue + ";  repara din 2015 in 2016 pt" + listaGospodariiDin2015.Count + " gospodarii. ", "incepe cautarea membrilor lipsa", 0, 0);
            foreach (List<string> gospodarie in listaGospodariiDin2015)
            {
                string gospodarie2016 = ManipuleazaBD.fRezultaUnString("SELECT gospodarieID FROM tntcomputers.gospodarii WHERE an = 2016 and gospodarieIdInitial = " + gospodarie[1], "gospodarieId", 2016);
                List<List<string>> listaMembri2015 = ManipuleazaBD.fRezultaListaStringuri(@"
                    SELECT  [cnp], [nume], COALESCE([decedat],0) as decedat,[capitolId],[unitateId],[gospodarieId],[an],COALESCE([codRand],'0') as codRand,COALESCE([codSex],1) as codSex,COALESCE([codRudenie],1) as codRudenie,COALESCE([denumireRudenie], 'cap de gospodarie') as denumireRudenie,COALESCE([dataNasterii], '01.01.1900') as dataNasterii,COALESCE([mentiuni], '') as mentiuni FROM membri 
                        WHERE an = 2015 AND coalesce(decedat,0) <> 1 
                            AND unitateId = " + ddlUnitate.SelectedValue + @" 
                            AND gospodarieId = " + gospodarie[0],
                       new List<string>() {
                           "cnp"
                          ,"nume"
                          ,"decedat"
                          ,"capitolId"
                          ,"unitateId"
                          ,"gospodarieId"
                          ,"an"
                          ,"codRand"
                          ,"codSex"
                          ,"codRudenie"
                          ,"denumireRudenie"
                          ,"dataNasterii"
                          ,"mentiuni"
                           }, 2016);
                List<List<string>> listaMembri2016 = ManipuleazaBD.fRezultaListaStringuri(@"
                    SELECT [cnp], [nume], COALESCE([decedat],0) as decedat,[capitolId],[unitateId],[gospodarieId],[an],COALESCE([codRand],'0') as codRand,COALESCE([codSex],1) as codSex,COALESCE([codRudenie],1) as codRudenie,COALESCE([denumireRudenie], 'cap de gospodarie') as denumireRudenie,COALESCE([dataNasterii], '01.01.1900') as dataNasterii,COALESCE([mentiuni], '') as mentiuni  FROM tntcomputers.membri 
                        WHERE an = 2016 
                            AND unitateId = " + ddlUnitate.SelectedValue + @"  
                            AND gospodarieId = (
                                                SELECT gospodarieID FROM tntcomputers.gospodarii 
                                                WHERE an = 2016 and gospodarieIdInitial = " + gospodarie[1] +
                                                ")",
                      new List<string>() {
                           "cnp"
                          ,"nume"
                          ,"decedat"
                          ,"capitolId"
                          ,"unitateId"
                          ,"gospodarieId"
                          ,"an"
                          ,"codRand"
                          ,"codSex"
                          ,"codRudenie"
                          ,"denumireRudenie"
                          ,"dataNasterii"
                          ,"mentiuni"
                           }, 2016);
                bool exista = false;
                foreach (List<string> membru2015 in listaMembri2015)
                {

                    foreach (List<string> membru2016 in listaMembri2016)
                    {
                        if (membru2015[0] == membru2016[0]
                            &&
                            (membru2015[1] == membru2016[1] || membru2015[1].Replace("ţ", "t").Replace("ş", "s").Replace("Ţ", "T").Replace("Ş", "S").Replace("ș", "s").Replace("Ș", "S").Replace("ț", "t").Replace("Ț", "T").Replace("ă", "a").Replace("Ă", "A") == membru2016[1]))
                        {
                            exista = true;
                            break;
                        }
                        else { }
                    }
                    if (!exista)
                    {
                        insert += @"INSERT INTO [tntcomputers].[membri]
                                       ([unitateId]
                                       ,[gospodarieId]
                                       ,[an]
                                       ,[nume]
                                       ,[codRand]
                                       ,[cnp]
                                       ,[codSex]
                                       ,[codRudenie]
                                       ,[denumireRudenie]
                                       ,[dataNasterii]
                                       ,[mentiuni]
                                       ,[decedat]
                                       ,[decedatData]
                                       ,[decedatSerie]
                                       ,[decedatNumar])
                                 VALUES
                                       (
                                        '" + ddlUnitate.SelectedValue + @"'
                                       ,'" + gospodarie2016 + @"'
                                       , '2016'
                                       ,N'" + membru2015[1] + @"'
                                       ,N'" + membru2015[7] + @"'
                                       ,N'" + membru2015[0] + @"'
                                       ,N'" + membru2015[8] + @"'
                                       ,N'" + membru2015[9] + @"'
                                       ,N'" + membru2015[10] + @"'
                                       ,Convert(datetime,'" + membru2015[11] + @"',104)
                                       ,N'" + membru2015[12] + @"'
                                        ,'0'
                                        ,Convert(datetime,'01.01.1900',104)
                                        ,''
                                        ,'')
                                        ; ";
                        listaMembriFinala.Add(membru2015);
                    }
                }
            }
            ManipuleazaBD.fManipuleazaBD(insert, 2016);
            ClassLog.fLog(1, 52, DateTime.Now, "membri", "unit. " + ddlUnitate.SelectedValue + ";  repara din 2015 in 2016 pt" + listaGospodariiDin2015.Count + " gospodarii, " + listaMembriFinala.Count + " membri introdusi", insert.Replace("'", "_"), 0, 0);
        }
        valCustom.IsValid = false;
        valCustom.ErrorMessage = "S-au introdus un numar de " + listaMembriFinala.Count + " membri, pentru unitatea " + ddlUnitate.SelectedItem.Text;
    }

    protected void btCorecteazaDiacriticeMembri_Click(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandTimeout = 100000;
        int randuriAfectate = 0;
        string update = "";
        List<List<string>> listaMembriFinala = new List<List<string>>();
        using (vCon)
        {
            // luam membri din 2015 care trebuie actualizati
            string select = @"
                        select  m2015.nume as numeCuCareSeInlocuieste, m2016.nume as numeDeInlocuit,  
                        m2015.capitolid as capitolid2015,m2016.capitolid as capitolid2016
                        from tntcomputers.gospodarii as g2015 
                        inner join tntcomputers.membri as m2015 on m2015.gospodarieId = g2015.gospodarieId 
                        inner join tntcomputers.membri as m2016 on m2016.gospodarieId = (

	                        SELECT gospodarieId FROM tntcomputers.gospodarii as g2016 WHERE an = 2016  and g2016.gospodarieIdInitial = g2015.gospodarieIdInitial)
	                        WHERE 
			                        g2015.an = 2015 
		                        and g2015.unitateId = " + ddlUnitate.SelectedValue + @"
		                        and len(m2015.nume) = len(m2016.nume)
		                        and len(m2015.nume) > 0
		                        and replace(replace(replace(replace(m2015.nume, N'Ă', 'A'), N'Â', 'A'), N'Ş', 'S'), N'Ţ', 'T') =  m2016.nume
                                and (charindex(N'Ă', m2016.nume)  > 0 OR charindex(N'Â', m2016.nume)  > 0 OR charindex(N'Ş', m2016.nume)  > 0 OR charindex(N'Ţ', m2016.nume)  > 0 )
	                ";
            List<List<string>> listaMembri = ManipuleazaBD.fRezultaListaStringuri(select, new List<string>() { "numeCuCareSeInlocuieste", "numeDeInlocuit", "capitolid2015", "capitolid2016", }, 2016);
            ClassLog.fLog(1, 52, DateTime.Now, "membri", "unit. " + ddlUnitate.SelectedValue + ";  repara diacritice din 2015 in 2016 pt. " + listaMembri.Count + " membri. ", "", 0, 0);
            foreach (List<string> membru in listaMembri)
            {
                update += @"UPDATE [tntcomputers].[membri] SET [nume] = N'" + membru[0] + "' WHERE capitolid = " + membru[3] + "; ";
                listaMembriFinala.Add(membru);
            }
        }
        ManipuleazaBD.fManipuleazaBD(update, 2016);
        ClassLog.fLog(1, 52, DateTime.Now, "membri", "unit. " + ddlUnitate.SelectedValue + ";  repara diacritice din 2015 in 2016 pt. " + listaMembriFinala.Count + " membri actualizati", update.Replace("'", "_"), 0, 0);
        valCustom.IsValid = false;
        valCustom.ErrorMessage = "S-au actualizat un numar de " + listaMembriFinala.Count + " membri, pentru unitatea " + ddlUnitate.SelectedItem.Text;

        // actualizam si in gospodarii


        randuriAfectate = 0;
        update = "";
        listaMembriFinala = new List<List<string>>();
        using (vCon)
        {
            // luam membri din 2015 care trebuie actualizati
            string select = @"
                        select m2016.nume as numeCuCareSeInlocuieste, g2016.membruNume as numeDeInlocuit,  
                        g2016.gospodarieId as gospodarieId2016
                        from tntcomputers.membri as m2016 
                        inner join tntcomputers.gospodarii as g2016 on m2016.gospodarieId = g2016.gospodarieId

						where   g2016.an = 2016 
		                        and g2016.unitateId = " + ddlUnitate.SelectedValue + @"
		                        and len(g2016.membruNume) = len(m2016.nume)
		                        and len(g2016.membruNume) > 0
								and (replace(replace(replace(replace(m2016.nume, N'Ă', 'A'), N'Â', 'A'), N'Ş', 'S'), N'Ţ', 'T') =  g2016.membruNume)
								and (charindex(N'Ă', m2016.nume)  > 0 OR charindex(N'Â', m2016.nume)  > 0 OR charindex(N'Ş', m2016.nume)  > 0 OR charindex(N'Ţ', m2016.nume)  > 0 )
								order by g2016.membruNume	                ";
            List<List<string>> listaMembri = ManipuleazaBD.fRezultaListaStringuri(select, new List<string>() { "numeCuCareSeInlocuieste", "numeDeInlocuit", "gospodarieId2016", }, 2016);
            ClassLog.fLog(1, 52, DateTime.Now, "membri", "unit. " + ddlUnitate.SelectedValue + ";  repara diacritice in gospodarii din 2015 in 2016 pt. " + listaMembri.Count + " gospodarii. ", "", 0, 0);
            foreach (List<string> membru in listaMembri)
            {
                update += @"UPDATE [tntcomputers].[gospodarii] SET [membruNume] = N'" + membru[0] + "' WHERE gospodarieId = " + membru[2] + "; ";
                listaMembriFinala.Add(membru);
            }
        }
        ManipuleazaBD.fManipuleazaBD(update, 2016);
        ClassLog.fLog(1, 52, DateTime.Now, "gospodarii", "unit. " + ddlUnitate.SelectedValue + ";  repara diacritice din 2015 in 2016 pt. " + listaMembriFinala.Count + " gospodarii actualizate", update.Replace("'", "_"), 0, 0);




        valCustom.IsValid = false;
        valCustom.ErrorMessage += "<br/> S-au actualizat un numar de " + listaMembriFinala.Count + " gospodarii, pentru unitatea " + ddlUnitate.SelectedItem.Text;
    }

    protected void btCorecteazaCapitoleLiniiDublate_Click(object sender, EventArgs e)
    {

        // curata dubluri
        StergeLiniiCapitoleDubluri(tbAnCorecteazaCapitoleLiniiDublateSauLipsa.Text, ddlUnitate.SelectedValue);
        // verificam daca exista

        List<string> listaStringTransferuri = new List<string>();
        List<AnimaleServices.RandSablon> sablonCapitol = AnimaleServices.CitesteRanduriSablon(Convert.ToInt16(tbAnCorecteazaCapitoleLiniiDublateSauLipsa.Text), ddlCapitoleCorecteazaCapitoleLiniiDublateSauLipsa.SelectedValue);
        // verificam daca are randuri complete pe cap 7 si 8
        //   listaStringTransferuri.AddRange(CompleteazaRanduriInCapitolePentruOGospodarie(ddlUnitate.SelectedValue, idGospodarie.ToString(), tbAnCorecteazaCapitoleLiniiDublateSauLipsa.Text, sablonCapitol, situatiaAnimalelorLaInceputulSemestruluiDataTable);


    }
    public void StergeLiniiCapitoleDubluri(string an, string unitate)
    {
        List<List<string>> listaLinii = CitesteListaLiniiCapitolDublate(an, unitate);
        string delete = "";
        foreach (List<string> linie in listaLinii)
        {
            delete += @"DELETE from [tntcomputers].capitole 
                            WHERE 
                                codRand = " + linie[0] + @" AND gospodarieid = " + linie[1] + @" AND codCapitol = '" + linie[2] + @"'
                                AND capitolid <> (
                                  SELECT max(capitolid) FROM [tntcomputers].capitole 
                                    WHERE 
                                       codRand = " + linie[0] + @" AND gospodarieid = " + linie[1] + @" AND codCapitol = '" + linie[2] + @"'
                                        );";
        }
        ManipuleazaBD.fManipuleazaBD(delete, 2016);
        ClassLog.fLog(1, 52, DateTime.Now, "capitole", "unit. " + unitate + ";  sterge " + listaLinii.Count + "coduri rand dublate", delete.Replace("'", "_"), 0, 0);
        valCustom.IsValid = false;
        valCustom.ErrorMessage = "S-au sterse dublurile pentru un numar de " + listaLinii.Count + " linii, pentru unitatea " + unitate;
    }
    public static List<List<string>> CitesteListaLiniiCapitolDublate(string an, string unitate)
    {
        string select = @"SELECT  
                            COALESCE(c.codRand,0) as codRand
                            ,gospodarieId
                            ,codCapitol
                          FROM [tntcomputers].[capitole] c
                          WHERE 
                                c.an = " + an + @" 
                                and unitateId = " + unitate + @"
                          GROUP BY
                                c.codRand
                                ,gospodarieid
                                ,codCapitol
                              HAVING COUNT(c.codRand)> 1
                              ORDER BY gospodarieid,c.codRand 
                            ";
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(2016);
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandTimeout = 100000;
        int randuriAfectate = 0;
        string update = "";
        List<List<string>> listaLiniiCapitoleDublate = new List<List<string>>();
        using (vCon)
        {
            listaLiniiCapitoleDublate = ManipuleazaBD.fRezultaListaStringuri(select, new List<string>() { "codRand", "gospodarieId", "codCapitol" }, 2016);
        }
        return listaLiniiCapitoleDublate;
    }
    public void CompleteazaRanduriInCapitolePentruOGospodarie(string unitate, string an, List<AnimaleServices.RandSablon> sablonCapitol)
    {
        string insert = "";
        int contor = 0;
        string selectExistente = @"
                                    SELECT an, codRand, codcapitol, gospodarieId FROM [tntcomputers].capitole where gospodarieid in
                                    (SELECT    gospodarieId
		                                      FROM [tntcomputers].[capitole] c
		                                      WHERE unitateid= " + unitate + @" and c.an = " + an + @" and codcapitol = '" + ddlCapitoleCorecteazaCapitoleLiniiDublateSauLipsa.SelectedValue + @"' 
                                              GROUP BY gospodarieId
                                              HAVING  count(c.codRand) <> (SELECT count(s.codRand)
                                                                                FROM tntcomputers.sabloaneCapitole s
                                                                                WHERE s.an =  " + an + @" 
                                                                                        and s.capitol = '" + ddlCapitoleCorecteazaCapitoleLiniiDublateSauLipsa.SelectedValue + @"'
                                                                                GROUP BY s.an )
                                    )
                                    AND unitateid =  " + unitate + @"  and an =  " + an + @"  and codcapitol = '" + ddlCapitoleCorecteazaCapitoleLiniiDublateSauLipsa.SelectedValue + @"' 
                                    ORDER BY gospodarieId, codCapitol, codRand";
        List<List<string>> listaExistenteToateGospodariile = ManipuleazaBD.fRezultaListaStringuri(selectExistente, new List<string>() { "an", "codRand", "codcapitol", "gospodarieId" }, 2016);
        var listaGrupatePeGospodarii = listaExistenteToateGospodariile
        .GroupBy(x => x[3])
        .ToList();

        List<string> listaInserturi = new List<string>();
        //List<string> listaRanduriExistente = dataTableRanduriExistente.Rows.T
        // parcurgem sablonul si verificam 
        List<string> listaCoduriDeLucru = new List<string>();
        bool amGasitCodRand = false;
        foreach (AnimaleServices.RandSablon randSablon in sablonCapitol)
        {
            string gospodarieId = "0";
            foreach (var randExistent in listaGrupatePeGospodarii)
            {
                List<List<string>> x = randExistent.ToList();
                gospodarieId = x[0][3].ToString();
                foreach (List<string> linieCapitol in x)
                {
                    if (linieCapitol[1].ToString() == randSablon.CodRand)
                    {
                        listaCoduriDeLucru.Add(linieCapitol[1].ToString());
                        amGasitCodRand = true;
                        break;
                    }
                }
                if (!amGasitCodRand)
                {
                    // nu am gasit codul asa ca il inseram cu zero
                    listaCoduriDeLucru.Add(randSablon.CodRand);
                    insert += @"INSERT INTO [capitole] ([unitateId], [gospodarieId], [an], [codCapitol], [codRand], [col1], [col2], [col3], [col4], [col5], [col6], [col7], [col8]) VALUES ('" + unitate + "','" + gospodarieId + "','" + an + "','" + randSablon.Capitol + "', '" + randSablon.CodRand + @"', '0', '0', '0', '0', '0', '0', '0', '0'); 
                    ";
                    contor++;

                }
                amGasitCodRand = false;
            }
        }
        ManipuleazaBD.fManipuleazaBD(insert, 2016);
        ClassLog.fLog(1, 52, DateTime.Now, "capitole", "unit. " + unitate + ";  insereaza goluri " + contor + "linii lipsa", insert.Replace("'", "_"), 0, 0);
        valCustom.IsValid = false;
        valCustom.ErrorMessage = "S-au inserat un numar de " + contor + " linii, pentru unitatea " + unitate;
    }

    protected void btCorecteazaCapitoleLiniiLipsa_Click(object sender, EventArgs e)
    {
        List<AnimaleServices.RandSablon> sablonCapitol = AnimaleServices.CitesteRanduriSablon(Convert.ToInt16(tbAnCorecteazaCapitoleLiniiDublateSauLipsa.Text), ddlCapitoleCorecteazaCapitoleLiniiDublateSauLipsa.SelectedValue);
        CompleteazaRanduriInCapitolePentruOGospodarie(ddlUnitate.SelectedValue, tbAnCorecteazaCapitoleLiniiLipsa.Text, sablonCapitol);
    }

    protected void btStabilesteProprietariiIn2B_Click(object sender, EventArgs e)
    {
        int nrRanduriAfectate = 0;
        string update = @"UPDATE[parcele] SET proprietarId = (CASE WHEN(proprietarId is null or proprietarid = 0) THEN (SELECT TOP(1) m.capitolId FROM membri m WHERE m.gospodarieId = parcele.gospodarieId AND m.codRudenie = 1) ELSE[proprietarId] END)
  WHERE (proprietarId is null or proprietarid = 0) AND unitateaid = " + ddlUnitate.SelectedValue + "  and an = " + tbAn.Text;
        ManipuleazaBD.fManipuleazaBD(update, 2016, out nrRanduriAfectate);
        ClassLog.fLog(1, 52, DateTime.Now, "parcele", "unit. " + ddlUnitate.SelectedValue + ";  update " + nrRanduriAfectate + " parcele cu proprietarId nul sau 0", update.Replace("'", "_"), 0, 0);
        valCustom.IsValid = false;
        valCustom.ErrorMessage = "S-au actualizat un numar de " + nrRanduriAfectate + " parcele, pentru unitatea " + ddlUnitate.SelectedValue;
    }

    protected void btCopiazaCulturiDinAnPrecedent_Click(object sender, EventArgs e)
    {
        List<Modele.Capitole> capitoleDeInserat = new List<Modele.Capitole>();
        capitoleDeInserat.AddRange(CopiazaCulturiDinAnPrecedent(DateTime.Now.Year.ToString(), "4a"));
        capitoleDeInserat.AddRange(CopiazaCulturiDinAnPrecedent(DateTime.Now.Year.ToString(), "4a1"));
        capitoleDeInserat.AddRange(CopiazaCulturiDinAnPrecedent(DateTime.Now.Year.ToString(), "4b1"));
        capitoleDeInserat.AddRange(CopiazaCulturiDinAnPrecedent(DateTime.Now.Year.ToString(), "4b2"));
        capitoleDeInserat.AddRange(CopiazaCulturiDinAnPrecedent(DateTime.Now.Year.ToString(), "4c"));
        Services.CapitoleServices.Save(capitoleDeInserat);
    }
    protected List<Modele.Capitole> CopiazaCulturiDinAnPrecedent(string an, string codCapitol)
    {
        // luam lista din anul precedent
        List<List<string>> listaDinAnPrecedent = new List<List<string>>();
        List<string> listaInregistrare = new List<string>();
        string select = @"
                            SELECT  c.[capitolId]
                                  ,c.[gospodarieId]
	                              ,g.gospodarieIdInitial as gospodarieAnVechi
                                  ,g2016.gospodarieId as gospodarieAnNou
                                  ,g.[unitateId]
                                  ,g.[an]
                                  ,c.[codCapitol]
                                  ,c.[codRand]
                                  ,c.[col1]
                                  ,c.[col2]
                             FROM[TNTRegistruAgricol3].[tntcomputers].[capitole] c
                             INNER JOIN gospodarii g ON g.gospodarieId = c.gospodarieId
                             INNER JOIN gospodarii g2016 ON g2016.gospodarieIdInitial = g.gospodarieIdInitial

                            WHERE g.unitateid = " + ddlUnitate.SelectedValue
                            + @" AND g.an = " + tbCopiazaCulturiDinAnPrecedentAn.Text +
                            @" AND g2016.an = " + DateTime.Now.Year.ToString() +
                            @" AND c.codCapitol = '" + codCapitol + @"'
                               ORDER BY c.gospodarieId, c.codCapitol, c.codRand";
        listaDinAnPrecedent = ManipuleazaBD.fRezultaListaStringuri(select, new List<string>() { "gospodarieAnVechi", "gospodarieAnNou", "codRand", "col1", "col2", "codCapitol" }, 2016);
        var listaGrupatePeGospodarii = listaDinAnPrecedent
        .GroupBy(x => x[1])
        .ToList();

        // parcurgem lista de gospodarii de modificat si facem delete si insert daca nu au date, fara inregistrari sau suma pe capitol 0
        List<Modele.Capitole> listaCapitole = new List<Modele.Capitole>();
        foreach (var dateGospodarie in listaGrupatePeGospodarii)
        {
            List<List<string>> inregistrari = dateGospodarie.ToList();
            if (!ManipuleazaBD.AreDateInCapitolePeAnulRespectiv(inregistrari[0][1], an, codCapitol))
            {
                // sterge ce exista - cele cu zero eventual
                ManipuleazaBD.StergeDateInCapitolePeAnulRespectiv(inregistrari[0][1], an, codCapitol);
                // adauga cele noi
                foreach (List<string> inregistrare in inregistrari)
                {
                    Modele.Capitole capitol = new Modele.Capitole();
                    capitol.GospodarieId = Convert.ToInt64(inregistrare[1]);
                    capitol.CodRand = Convert.ToInt32(inregistrare[2]);
                    capitol.Col1 = Convert.ToDecimal(inregistrare[3]);
                    capitol.Col2 = Convert.ToDecimal(inregistrare[4]);
                    capitol.An = Convert.ToInt16(DateTime.Now.Year);
                    capitol.Cod = inregistrare[5];
                    capitol.UnitateId = Convert.ToInt32(ddlUnitate.SelectedValue);
                    listaCapitole.Add(capitol);
                }
            }
        }
        return listaCapitole;
    }
}