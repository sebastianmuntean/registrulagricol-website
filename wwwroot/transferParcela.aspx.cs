﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

/// <summary>
/// Pagina transfer parcela
/// Creata la:                  23.03.2011
/// Autor:                      Laza Tudor Mihai
/// Ultima                      actualizare: 23.03.2011
/// Autor:                      Laza Tudor Mihai
/// </summary> 
public partial class transferParcela : System.Web.UI.Page
{
    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        DBConnections connection = new DBConnections(Convert.ToInt16(Session["SESan"]));

        SqlSabloaneCapitole.ConnectionString = connection.Create();
        SqlTipTransfer.ConnectionString = connection.Create();
        
        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "transferParcela", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
        }
    }
    protected void gvGospodarii_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(gvGospodarii, e, this);
    }
    protected void tbData_Init(object sender, EventArgs e)
    {
        tbData.Text = DateTime.Now.Date.ToString("dd.MM.yyyy");
    }
    protected void btAnuleazaSalvarea_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Capitol2b.aspx");
    }
    protected void btSalveazaSablon_Click(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlTransaction vTranz = ManipuleazaBD.DeschideTranzactie(vCon);
        SqlCommand vCmd = new SqlCommand();
        SqlCommand vCmd1 = new SqlCommand();
        vCmd1.Connection = vCon;
        vCmd1.Transaction = vTranz;
        vCmd.Connection = vCon;
        vCmd.Transaction = vTranz;
        int vOK = 0;
        try
        {
            string[] vParcele = Session["SESparcelaId"].ToString().Split(';');
            foreach (string a in vParcele)
            {
                // modifica id-ul gospodariei
                vCmd.CommandText = "update parcele set gospodarieId='" + gvGospodarii.SelectedValue.ToString() + "' where parcelaId='" + a + "'";
                vCmd.ExecuteNonQuery();
                // adaug un rand in istoric parcela
                vCmd.CommandText = "INSERT INTO istoricParcela (parcelaId, data, motiv, unitateId, deLa, la, documentAtesta, observatii) VALUES ('" + a + "', convert(datetime,'" + tbData.Text + "',104), '" + ddlTipTransfer.SelectedValue.ToString() + "', '" + Session["SESunitateId"].ToString() + "', '" + Session["SESgospodarieId"].ToString() + "', '" + gvGospodarii.SelectedValue.ToString() + "', N'" + tbDocument.Text + "', N'" + tbObservatii.Text + "')";
                vCmd.CommandText = vCmd.CommandText.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
                vCmd.ExecuteNonQuery();
                ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "parcele", "transfer parcela", "", Convert.ToInt64(Session["SESgospodarieId"]), 3);
                // iau suprafetele pentru parcela selectata
                /*vCmd1.CommandText = "select * from parcele where parcelaId='" + a + "'";
                SqlDataReader vTabel = vCmd1.ExecuteReader();
                vTabel.Read();
                int vHaIntra = 0, vHaExtra = 0;
                decimal vAriIntra = 0, vAriExtra = 0;
                vHaIntra = Convert.ToInt32(vTabel["parcelaSuprafataIntravilanHa"].ToString());
                vHaExtra = Convert.ToInt32(vTabel["parcelaSuprafataExtravilanHa"].ToString());
                vAriIntra = Convert.ToDecimal(vTabel["parcelaSuprafataIntravilanAri"].ToString());
                vAriExtra = Convert.ToDecimal(vTabel["parcelaSuprafataExtravilanAri"].ToString());
                vTabel.Close();*/
                // scad suprafetele din capitolul 2a

                
            }
            vTranz.Commit();
            vOK = 1;
        }
        catch { vTranz.Rollback(); }
        finally { ManipuleazaBD.InchideConexiune(vCon); }
        if (vOK == 1)
        {
            // daca am bifa pe unitati adaug in C2A din C2B
            vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
            vCmd = new SqlCommand();
            vCmd.Connection = vCon;
            vCmd.CommandText = "select top(1) unitateAdaugaDinC2B from unitati where unitateId='" + Session["SESunitateId"].ToString() + "'";
            bool vAdaugaDinC2b = Convert.ToBoolean(vCmd.ExecuteScalar());
            ManipuleazaBD.InchideConexiune(vCon);
            if (vAdaugaDinC2b)
            {
                CalculCapitole.AdaugaCapitol2ADin2B(Session["SESunitateId"].ToString(), Session["SESan"].ToString(), Session["SESgospodarieId"].ToString(), "0");
                CalculCapitole.AdaugaCapitol2ADin2B(Session["SESunitateId"].ToString(), Session["SESan"].ToString(), gvGospodarii.SelectedValue.ToString(), "0");
            }
            Response.Redirect("~/Capitol2b.aspx");
        }
    }
}
