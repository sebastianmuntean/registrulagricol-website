﻿// this.value=ValidareData(this.value, 'dd.MM.yyyy')
function ValidareData(val, frmt) {
    var car_desp = "", format = "", numere = "0123456789", fin = "", car_spec = "";
    var ok = 0;
    var zi = "", luna = "", an = "";
    var DataCurenta = new Date();
    var luna_crt = DataCurenta.getMonth() + 1;
    var zi_crt = DataCurenta.getDate();
    var an_crt = DataCurenta.getFullYear().toString();
    if (val.length > 10) {
        val = val.toString().substr(0, 9);
    }
    if (luna_crt.toString().length == 1)
        luna_crt = "0" + luna_crt.toString();
    if (zi_crt.toString().length == 1)
        zi_crt = "0" + zi_crt.toString();
    for (var i = 0; i < numere.length; i++) {
        if (val.toString().indexOf(numere.charAt(i)) != -1) {
            ok = 1;
        }
        else {
        }
    }
    if (val.toString().indexOf(".") != -1) {
        car_desp = ".";
    }
    else if (val.toString().indexOf("/") != -1) {
        car_desp = "/";
    }
    else if (val.toString().indexOf("-") != -1) {
        car_desp = "-";
    }
    else {
        for (var i = 0; i < frmt.toString().length; i++) {
            if (frmt.toString().toLowerCase().charAt(i) != "d" && frmt.toString().toLowerCase().charAt(i) != "m" && frmt.toString().toLowerCase().charAt(i) != "y") {
                car_desp = frmt.toString().charAt(i);
            }
        }
        for (var i = 0; i < val.toString().length; i++) {
            if (numere.indexOf(val.toString().charAt(i)) == -1) {
                car_spec = val.toString().charAt(i);
                val = val.toString().replace(car_spec, car_desp);
            }
        }
    }
    if (frmt.toString().toLowerCase().charAt(0) == "d") {
        format = "dmy";
    }
    else if (frmt.toString().toLowerCase().charAt(0) == "m") {
        format = "mdy";
    }
    else if (frmt.toString().toLowerCase().charAt(0) == "y") {
        format = "ymd";
    }
    if (ok == 1) {
        if (val.toString().indexOf(car_desp) != -1) {
            // avem caracter de despartire
            if (format == "dmy") {
                var tmp = "";
                var check = 0;
                for (var i = 0; i < val.toString().length; i++) {
                    if (val.toString().charAt(i) != car_desp) {
                        // pregatesc valoarea pentru zi luna sau an
                        tmp += val.toString().charAt(i);
                        if (val.toString().charAt(i + 1) == car_desp || i + 1 == val.toString().length) {
                            // dau valoare la zi
                            if (check == 0) {
                                if (tmp.length == 1) {
                                    zi = "0" + tmp.toString();
                                }
                                else if (tmp.length == 2) {
                                    zi = tmp.toString();
                                }
                            }
                            // dau valoare la luna
                            else if (check == 1) {
                                if (tmp.length == 1) {
                                    luna = "0" + tmp.toString();
                                }
                                else if (tmp.length == 2) {
                                    luna = tmp.toString();
                                }
                            }
                            // dau valoare la an
                            else if (check == 2) {
                                if (tmp.length == 1) {
                                    an = "200" + tmp.toString();
                                }
                                else if (tmp.length == 2) {
                                    if (tmp <= 30) {
                                        an = "20" + tmp.toString();
                                    }
                                    else if (tmp > 30) {
                                        an = "19" + tmp.toString();
                                    }
                                }
                                else if (tmp.length == 3) {
                                    if (tmp <= 30) {
                                        an = "2" + tmp.toString();
                                    }
                                    else if (tmp > 30) {
                                        an = "1" + tmp.toString();
                                    }
                                }
                                else {
                                    an = tmp.toString();
                                }
                            }
                            tmp = "";
                            check++;
                        }
                    }
                }
                if (zi == "") {
                    zi = zi_crt;
                }
                if (luna == "") {
                    luna = luna_crt;
                }
                if (an == "") {
                    an = an_crt;
                }
            }
            else if (format == "mdy") {
                // format luna zi an
                var tmp = "";
                var check = 0;
                for (var i = 0; i < val.toString().length; i++) {
                    if (val.toString().charAt(i) != car_desp) {
                        // pregatesc valoarea pentru zi luna sau an
                        tmp += val.toString().charAt(i);
                        if (val.toString().charAt(i + 1) == car_desp || i + 1 == val.toString().length) {
                            // dau valoare la luna
                            if (check == 0) {
                                if (tmp.length == 1) {
                                    luna = "0" + tmp.toString();
                                }
                                else if (tmp.length == 2) {
                                    luna = tmp.toString();
                                }
                            }
                            // dau valoare la zi
                            else if (check == 1) {
                                if (tmp.length == 1) {
                                    zi = "0" + tmp.toString();
                                }
                                else if (tmp.length == 2) {
                                    zi = tmp.toString();
                                }
                            }
                            // dau valoare la an
                            else if (check == 2) {
                                if (tmp.length == 1) {
                                    an = "200" + tmp.toString();
                                }
                                else if (tmp.length == 2) {
                                    if (tmp <= 30) {
                                        an = "20" + tmp.toString();
                                    }
                                    else if (tmp > 30) {
                                        an = "19" + tmp.toString();
                                    }
                                }
                                else if (tmp.length == 3) {
                                    if (tmp <= 30) {
                                        an = "2" + tmp.toString();
                                    }
                                    else if (tmp > 30) {
                                        an = "1" + tmp.toString();
                                    }
                                }
                                else {
                                    an = tmp.toString();
                                }
                            }
                            tmp = "";
                            check++;
                        }
                    }
                }
                if (zi == "") {
                    zi = zi_crt;
                }
                if (luna == "") {
                    luna = luna_crt;
                }
                if (an == "") {
                    an = an_crt;
                }
            }
            else if (format == "ymd") {
                // format an luna zi
                var tmp = "";
                var check = 0;
                for (var i = 0; i < val.toString().length; i++) {
                    if (val.toString().charAt(i) != car_desp) {
                        // pregatesc valoarea pentru zi luna sau an
                        tmp += val.toString().charAt(i);
                        if (val.toString().charAt(i + 1) == car_desp || i + 1 == val.toString().length) {
                            // dau valoare la an
                            if (check == 0) {
                                if (tmp.length == 1) {
                                    an = "200" + tmp.toString();
                                }
                                else if (tmp.length == 2) {
                                    if (tmp <= 30) {
                                        an = "20" + tmp.toString();
                                    }
                                    else if (tmp > 30) {
                                        an = "19" + tmp.toString();
                                    }
                                }
                                else if (tmp.length == 3) {
                                    if (tmp <= 30) {
                                        an = "2" + tmp.toString();
                                    }
                                    else if (tmp > 30) {
                                        an = "1" + tmp.toString();
                                    }
                                }
                                else {
                                    an = tmp.toString();
                                }
                            }
                            // dau valoare la luna
                            else if (check == 1) {
                                if (tmp.length == 1) {
                                    luna = "0" + tmp.toString();
                                }
                                else if (tmp.length == 2) {
                                    luna = tmp.toString();
                                }
                            }
                            // dau valoare la zi
                            if (check == 2) {
                                if (tmp.length == 1) {
                                    zi = "0" + tmp.toString();
                                }
                                else if (tmp.length == 2) {
                                    zi = tmp.toString();
                                }
                            }
                            tmp = "";
                            check++;
                        }
                    }
                }
                if (zi == "") {
                    zi = zi_crt;
                }
                if (luna == "") {
                    luna = luna_crt;
                }
                if (an == "") {
                    an = an_crt;
                }
            }
        }
        // nu avem caracter de despartire
        else {
            if (val.length > 8) {
                val = val.toString().substr(0, 7);
            }
            if (format == "dmy") {
                if (val.toString().length == 1 || val.toString().length == 2) {
                    if (val.toString().length == 1)
                        zi = "0" + val.toString();
                    else if (val.toString().length == 2)
                        zi = val.toString();
                    luna = luna_crt;
                    an = an_crt;
                }
                else if (val.toString().length == 3) {
                    zi = "0" + val.toString().charAt(0);
                    luna = "0" + val.toString().charAt(1);
                    an = "200" + val.toString().charAt(2);
                }
                else if (val.toString().length == 4) {
                    zi = val.toString().charAt(0) + val.toString().charAt(1);
                    luna = val.toString().charAt(2) + val.toString().charAt(3);
                    an = an_crt;
                }
                else if (val.toString().length == 5) {
                    zi = val.toString().charAt(0) + val.toString().charAt(1);
                    luna = val.toString().charAt(2) + val.toString().charAt(3);
                    an = "200" + val.toString().charAt(4);
                }
                else if (val.toString().length == 6) {
                    zi = val.toString().charAt(0) + val.toString().charAt(1);
                    luna = val.toString().charAt(2) + val.toString().charAt(3);
                    if (val.toString().charAt(4).toString() + val.toString().charAt(5).toString() > 30) {
                        an = "19" + val.toString().charAt(4).toString() + val.toString().charAt(5).toString();
                    }
                    else {
                        an = "20" + val.toString().charAt(4).toString() + val.toString().charAt(5).toString();
                    }
                }
                else if (val.toString().length == 7) {
                    zi = val.toString().charAt(0) + val.toString().charAt(1);
                    luna = val.toString().charAt(2) + val.toString().charAt(3);
                    if (val.toString().charAt(4).toString() + val.toString().charAt(5).toString() + val.toString().charAt(6).toString() > 30) {
                        an = "1" + val.toString().charAt(4).toString() + val.toString().charAt(5).toString() + val.toString().charAt(6);
                    }
                    else {
                        an = "2" + val.toString().charAt(4).toString() + val.toString().charAt(5).toString() + val.toString().charAt(6);
                    }
                }
                else if (val.toString().length == 8) {
                    zi = val.toString().charAt(0) + val.toString().charAt(1);
                    luna = val.toString().charAt(2) + val.toString().charAt(3);
                    an = val.toString().charAt(4).toString() + val.toString().charAt(5).toString() + val.toString().charAt(6) + val.toString().charAt(7);
                }
                else {
                    zi = "";
                    luna = "";
                    an = "";
                }
            }
            else if (format == "mdy") {
                if (val.toString().length == 1 || val.toString().length == 2) {
                    if (val.toString().length == 1)
                        luna = "0" + val.toString();
                    else if (val.toString().length == 2)
                        luna = val.toString();
                    zi = zi_crt;
                    an = an_crt;
                }
                else if (val.toString().length == 3) {
                    luna = "0" + val.toString().charAt(0);
                    zi = "0" + val.toString().charAt(1);
                    an = "200" + val.toString().charAt(2);
                }
                else if (val.toString().length == 4) {
                    luna = val.toString().charAt(0) + val.toString().charAt(1);
                    zi = val.toString().charAt(2) + val.toString().charAt(3);
                    an = an_crt;
                }
                else if (val.toString().length == 5) {
                    luna = val.toString().charAt(0) + val.toString().charAt(1);
                    zi = val.toString().charAt(2) + val.toString().charAt(3);
                    an = "200" + val.toString().charAt(4);
                }
                else if (val.toString().length == 6) {
                    luna = val.toString().charAt(0) + val.toString().charAt(1);
                    zi = val.toString().charAt(2) + val.toString().charAt(3);
                    if (val.toString().charAt(4).toString() + val.toString().charAt(5).toString() > 30) {
                        an = "19" + val.toString().charAt(4).toString() + val.toString().charAt(5).toString();
                    }
                    else {
                        an = "20" + val.toString().charAt(4).toString() + val.toString().charAt(5).toString();
                    }
                }
                else if (val.toString().length == 7) {
                    luna = val.toString().charAt(0) + val.toString().charAt(1);
                    zi = val.toString().charAt(2) + val.toString().charAt(3);
                    if (val.toString().charAt(4).toString() + val.toString().charAt(5).toString() + val.toString().charAt(6).toString() > 30) {
                        an = "1" + val.toString().charAt(4).toString() + val.toString().charAt(5).toString() + val.toString().charAt(6);
                    }
                    else {
                        an = "2" + val.toString().charAt(4).toString() + val.toString().charAt(5).toString() + val.toString().charAt(6);
                    }
                }
                else if (val.toString().length == 8) {
                    luna = val.toString().charAt(0) + val.toString().charAt(1);
                    zi = val.toString().charAt(2) + val.toString().charAt(3);
                    an = val.toString().charAt(4).toString() + val.toString().charAt(5).toString() + val.toString().charAt(6) + val.toString().charAt(7);
                }
                else {
                    zi = "";
                    luna = "";
                    an = "";
                }
            }
            else if (format == "ymd") {
                if (val.toString().length == 1 || val.toString().length == 2 || val.toString().length == 3) {
                    if (val.toString().length == 1) {
                        an = "200" + val.toString();
                    }
                    if (val.toString().length == 2) {
                        if (val > 30)
                            an = "19" + val.toString();
                        else if (val <= 30)
                            an = "20" + val.toString();
                    }
                    if (val.toString().length == 3) {
                        if (val > 30)
                            an = "1" + val.toString();
                        else if (val <= 30)
                            an = "2" + val.toString();
                    }
                    luna = luna_crt;
                    zi = zi_crt;
                }
                else if (val.toString().length == 4) {
                    an = val.toString().charAt(0) + val.toString().charAt(1) + val.toString().charAt(2) + val.toString().charAt(3);
                    luna = luna_crt;
                    zi = zi_crt;
                }
                else if (val.toString().length == 5) {
                    an = val.toString().charAt(0) + val.toString().charAt(1) + val.toString().charAt(2) + val.toString().charAt(3);
                    luna = "0" + val.toString().charAt(4);
                    zi = zi_crt;
                }
                else if (val.toString().length == 6) {
                    if (val.toString().charAt(0).toString() + val.toString().charAt(1).toString() > 30) {
                        an = "19" + val.toString().charAt(0).toString() + val.toString().charAt(1).toString();
                    }
                    else {
                        an = "20" + val.toString().charAt(0).toString() + val.toString().charAt(1).toString();
                    }
                    luna = val.toString().charAt(2) + val.toString().charAt(3);
                    zi = val.toString().charAt(4) + val.toString().charAt(5);
                }
                else if (val.toString().length == 7) {
                    if (val.toString().charAt(0).toString() + val.toString().charAt(1).toString() + val.toString().charAt(2).toString() > 30) {
                        an = "1" + val.toString().charAt(0).toString() + val.toString().charAt(1).toString() + val.toString().charAt(2);
                    }
                    else {
                        an = "2" + val.toString().charAt(0).toString() + val.toString().charAt(1).toString() + val.toString().charAt(2);
                    }
                    luna = val.toString().charAt(2) + val.toString().charAt(3);
                    zi = val.toString().charAt(4) + val.toString().charAt(5);
                }
                else if (val.toString().length == 8) {
                    an = val.toString().charAt(0).toString() + val.toString().charAt(1).toString() + val.toString().charAt(2) + val.toString().charAt(3);
                    zi = val.toString().charAt(4) + val.toString().charAt(5);
                    luna = val.toString().charAt(6) + val.toString().charAt(7);

                }
                else {
                    zi = "";
                    luna = "";
                    an = "";
                }
            }
        }
        if (zi == "00")
            zi = zi_crt;
        if (luna == "00")
            luna = luna_crt;
        if (zi != "" && luna != "" && an != "") {
            if (luna > 12)
                luna = 12;
            if (luna == 1 || luna == 3 || luna == 5 || luna == 7 || luna == 8 || luna == 10 || luna == 12) {
                if (zi > 31)
                    zi = 31;
            }
            if (luna == 4 || luna == 6 || luna == 9 || luna == 11) {
                if (zi > 30)
                    zi = 30;
            }
            if (an % 400 == 0 && luna == 2 && zi > 29) {
                zi = 29;
            }
            if (an % 400 != 0 && luna == 2 && zi > 28) {
                zi = 28;
            }
        }
        if (format == "dmy") {
            fin = zi + car_desp + luna + car_desp + an;
        }
        if (format == "mdy") {
            fin = luna + car_desp + zi + car_desp + an;
        }
        if (format == "ymd") {
            fin = an + car_desp + luna + car_desp + zi;
        }
    }
    else {
        if (format == "dmy") {
            fin = zi_crt + car_desp + luna_crt + car_desp + an_crt;
        }
        if (format == "mdy") {
            fin = luna_crt + car_desp + zi_crt + car_desp + an_crt;
        }
        if (format == "ymd") {
            fin = an_crt + car_desp + luna_crt + car_desp + zi_crt;
        }
    }
    if (fin == car_desp + car_desp) {
        if (format == "dmy") {
            fin = zi_crt + car_desp + luna_crt + car_desp + an_crt;
        }
        if (format == "mdy") {
            fin = luna_crt + car_desp + zi_crt + car_desp + an_crt;
        }
        if (format == "ymd") {
            fin = an_crt + car_desp + luna_crt + car_desp + zi_crt;
        }
    }
    if (val == "")
        fin = "";
    return fin;
}