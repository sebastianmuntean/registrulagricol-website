﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cautaContract : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DBConnections connection = new DBConnections(Convert.ToInt16(Session["SESan"]));

        SqlModUtilizareContracteGrid.ConnectionString = connection.Create();

        if (!IsPostBack)
        {
            tbCInceput.Text = DateTime.Now.ToShortDateString();
            tbCFinal.Text = DateTime.Now.ToShortDateString();
        }
    }
    protected void tbCInceput_TextChanged(object sender, EventArgs e)
    {
        // inlucuim datele daca se introduc aiurea
        try
        {
            if (Convert.ToDateTime(tbCInceput.Text) > Convert.ToDateTime(tbCFinal.Text))
                tbCFinal.Text = tbCInceput.Text;
        }
        catch
        {
            try
            {
                tbCInceput.Text = Convert.ToDateTime(tbCFinal.Text).ToString();
            }
            catch
            {
                tbCInceput.Text = tbCFinal.Text = DateTime.Now.ToString().Remove(11);
            }
        }
    }
    protected void tbCFinal_TextChanged(object sender, EventArgs e)
    {
        // inlucuim datele daca se introduc aiurea
        try
        {
            if (Convert.ToDateTime(tbCFinal.Text) < Convert.ToDateTime(tbCInceput.Text))
                tbCInceput.Text = tbCFinal.Text;
        }
        catch
        {
            try
            {
                tbCFinal.Text = Convert.ToDateTime(tbCInceput.Text).ToString();
            }
            catch
            {
                tbCFinal.Text = tbCInceput.Text = DateTime.Now.ToString().Remove(11);
            }
        }
    }
    protected void tbDataInceputContract_TextChanged(object sender, EventArgs e)
    {
        // inlucuim datele daca se introduc aiurea
        /*     if (tbDataFinalContract.Text.Trim() != "")
             {
                 try
                 {
                     if (Convert.ToDateTime(tbDataInceputContract.Text) > Convert.ToDateTime(tbDataFinalContract.Text) && tbDataFinalContract.Text.Trim() != "")

                         tbDataFinalContract.Text = tbDataInceputContract.Text;

                 }
                 catch
                 {
                     try
                     {
                         tbDataInceputContract.Text = Convert.ToDateTime(tbDataFinalContract.Text).ToString();
                     }
                     catch
                     {
                         tbDataInceputContract.Text = tbDataFinalContract.Text = DateTime.Now.ToString().Remove(11);
                     }
                 }
             }
             // daca e data inceput din anul urmator punem ultima data din anul curent
             try
             {
                 if (Convert.ToDateTime(tbDataInceputContract.Text) > Convert.ToDateTime("31.12." + Session["SESan"].ToString()))
                 {
                     tbDataInceputContract.Text = "31.12." + Session["SESan"].ToString();
                     valCustom.IsValid = false;
                     valCustom.ErrorMessage = "Contractul trebuie sa cuprindă şi cel puţin o zi din anul " + Session["SESan"].ToString() + "!";
                 }
             }
             catch
             {
                tbDataInceputContract.Text = DateTime.Now.ToString().Remove(11);
             }
         * */
    }
    protected void tbDataFinalContract_TextChanged(object sender, EventArgs e)
    {
        // inlucuim datele daca se introduc aiurea
        /*  if (tbDataInceputContract.Text.Trim() != "")
          {
              try
              {
                  if (Convert.ToDateTime(tbDataFinalContract.Text) < Convert.ToDateTime(tbDataInceputContract.Text) && tbDataInceputContract.Text.Trim() != "")
                      tbDataInceputContract.Text = tbDataFinalContract.Text;
              }
              catch
              {
                  try
                  {
                      tbDataFinalContract.Text = Convert.ToDateTime(tbDataInceputContract.Text).ToString();
                  }
                  catch
                  {
                      tbDataFinalContract.Text = tbDataInceputContract.Text = DateTime.Now.ToString().Remove(11);
                  }
              }
          }
          // daca e data final din anul precedent punem prima zi din anul curent
          try
          {
              if (Convert.ToDateTime(tbDataFinalContract.Text) < Convert.ToDateTime("01.01." + Session["SESan"].ToString()))
              {
                  tbDataFinalContract.Text = "01.01." + Session["SESan"].ToString();
                  valCustom.IsValid = false;
                  valCustom.ErrorMessage = "Contractul trebuie sa cuprindă şi cel puţin o zi din anul " + Session["SESan"].ToString() + "!";
              }
          }
          catch
          {
              tbDataInceputContract.Text = DateTime.Now.ToString().Remove(11);
          }
         * */
    }
    protected void gvModUtilizareContracte_DataBound(object sender, EventArgs e)
    {
        // cautam numele capului de gospodarie
        for (int i = 0; i < gvModUtilizareContracte.Rows.Count; i++)
        {
            if (gvModUtilizareContracte.Rows[i].RowType == DataControlRowType.DataRow)
            {
                try
                {
                    ((Label)gvModUtilizareContracte.Rows[i].FindControl("lblNumeDa")).Text = GospodariaCurentaDetalii(((Label)gvModUtilizareContracte.Rows[i].FindControl("lblIdDa")).Text);
                    ((Label)gvModUtilizareContracte.Rows[i].FindControl("lblNumePrimeste")).Text = GospodariaCurentaDetalii(((Label)gvModUtilizareContracte.Rows[i].FindControl("lblIdPrimeste")).Text);
                    // scriem expirat sau nu 
                    if (Convert.ToDateTime(((Label)gvModUtilizareContracte.Rows[i].FindControl("lblDataFinal")).Text) < DateTime.Now.AddDays(-1))
                        ((Label)gvModUtilizareContracte.Rows[i].FindControl("lblExpirat")).Visible = true;
                    else ((Label)gvModUtilizareContracte.Rows[i].FindControl("lblExpirat")).Visible = false;
                    if (Convert.ToDateTime(((Label)gvModUtilizareContracte.Rows[i].FindControl("lblDataInceput")).Text) > DateTime.Now)
                        ((Label)gvModUtilizareContracte.Rows[i].FindControl("lblNeinceput")).Visible = true;
                    else if (Convert.ToDateTime(((Label)gvModUtilizareContracte.Rows[i].FindControl("lblDataFinal")).Text) > DateTime.Now.AddDays(-1))
                    {
                        ((Label)gvModUtilizareContracte.Rows[i].FindControl("lblNeinceput")).Visible = false;
                        ((Label)gvModUtilizareContracte.Rows[i].FindControl("lblInCurs")).Visible = true;
                    }
                    else
                    {
                        ((Label)gvModUtilizareContracte.Rows[i].FindControl("lblNeinceput")).Visible = false;

                    }
                }
                catch { }
            }
        }
    }
    protected void gvModUtilizareContracte_PreRender(object sender, EventArgs e)
    {

    }
    protected void gvModUtilizareContracte_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //ClasaGridView.SelectGrid(gvModUtilizareContracte, e, this);
        if (e.Row.RowType == DataControlRowType.Header)
        {
            ViewState["ha"] = 0;
            ViewState["ari"] = 0;
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label vLblHa = (Label)e.Row.FindControl("lblHa");
            Label vLblAri = (Label)e.Row.FindControl("lblAri");
            ViewState["ha"] = Convert.ToDecimal(ViewState["ha"].ToString()) + Convert.ToDecimal(vLblHa.Text);
            ViewState["ari"] = Convert.ToDecimal(ViewState["ari"].ToString()) + Convert.ToDecimal(vLblAri.Text);
        }
        else if (e.Row.RowType == DataControlRowType.Footer)
        {
            Label vLblHaT = (Label)e.Row.FindControl("lblHaT");
            Label vLblAriT = (Label)e.Row.FindControl("lblAriT");
            decimal vHa = Convert.ToDecimal(ViewState["ha"].ToString()), vAri = Convert.ToDecimal(ViewState["ari"].ToString());
            vHa = vHa + Math.Truncate(vAri / 100);
            vAri -= Math.Truncate(vAri / 100) * 100;
            vLblHaT.Text = vHa.ToString("F");
            vLblAriT.Text = vAri.ToString("F");
        }
    }
    protected string GospodariaCurentaDetalii(string pGospodarie)
    {
        // pTip = id-ul gospodariei
        string vDetalii = "";
        if (ManipuleazaBD.fVerificaExistentaExtins("gospodarii", "persJuridica", "True", " AND gospodarieId ='" + pGospodarie + "'", Convert.ToInt16(Session["SESan"])) == 1)
        {
            string[] vCampuriRezultate = { "volum", "nrPozitie", "jUnitate" };
            int vContor = 0;
            foreach (string vElement in ManipuleazaBD.fRezultaUnString("SELECT TOP(1) volum, nrPozitie, jUnitate FROM gospodarii WHERE unitateId = '" + Session["SESunitateId"].ToString() + "' AND gospodarieId='" + pGospodarie + "'", vCampuriRezultate, Convert.ToInt16(Session["SESan"])))
            {
                switch (vContor)
                {
                    case 0:
                        vDetalii += "Vol." + vElement.ToString();
                        break;
                    case 1:
                        vDetalii += " / Poz." + vElement.ToString();
                        break;
                    case 2:
                        vDetalii += " / " + vElement.ToString();
                        break;
                }
                vContor++;
            }
        }
        else
        {
            string[] vCampuriRezultate = { "volum", "nrPozitie", "nume" };
            int vContor = 0;
            foreach (string vElement in ManipuleazaBD.fRezultaUnString("SELECT TOP(1) volum, nrPozitie, nume FROM gospodarii INNER JOIN membri ON gospodarii.gospodarieId = membri.gospodarieId WHERE gospodarii.unitateId = '" + Session["SESunitateId"].ToString() + "' AND gospodarii.gospodarieId='" + pGospodarie + "' AND membri.codRudenie='1'", vCampuriRezultate, Convert.ToInt16(Session["SESan"])))
            {
                switch (vContor)
                {
                    case 0:
                        vDetalii += "Vol." + vElement.ToString();
                        break;
                    case 1:
                        vDetalii += " / Poz." + vElement.ToString();
                        break;
                    case 2:
                        vDetalii += " / " + vElement.ToString();
                        break;
                }
                vContor++;
            }

        }
        int vNumar = clsCorelatii.NumarCorelatiiInvalidate(Session["SESgospodarieId"].ToString(), Session["SESUnitateId"].ToString(), Session["SESan"].ToString());
        return vDetalii;
    }
}