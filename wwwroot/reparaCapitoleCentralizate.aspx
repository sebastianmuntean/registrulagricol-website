﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="reparaCapitoleCentralizate.aspx.cs" Inherits="reparaCapitoleCentralizate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Button ID="Button1" runat="server" Text="reface centralizatoare pentru toate capitolele !! atentie --- nu rulati in timpul programului!!!"
        OnClick="toate_Click" />
    <asp:Button ID="Button2" runat="server" Text="Actualizeaza o unitate" OnClick="Button2_Click" />
    <asp:Button ID="btAdaugaUnitatiNoi" runat="server" Text="Adauga unitati noi in centralizatoare"
        OnClick="btAdaugaUnitatiNoi_Click" />
    <br />
    <br />
    Unitate:<asp:TextBox ID="tbUnitate" runat="server"></asp:TextBox>
    <asp:DropDownList ID="ddlUnitate" runat="server" AppendDataBoundItems="True" 
        DataSourceID="SqlUnitati" DataTextField="unitateDenumire" 
        DataValueField="unitateId">
        <asp:ListItem Value="%">-toate-</asp:ListItem>
    </asp:DropDownList>
    <asp:SqlDataSource ID="SqlUnitati" runat="server" 
        ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016 %>" 
        SelectCommand="SELECT unitati.[unitateId], judetDenumire+' - '+unitati.[unitateDenumire] as unitateDenumire FROM [unitati] inner join judete on unitati.judetId=judete.judetId ORDER BY [unitateDenumire]">
    </asp:SqlDataSource>
    <br />
    An
    <asp:TextBox ID="tbAn" runat="server"></asp:TextBox>
    <br />
    Capitol:
    <asp:TextBox ID="tbCapitol" runat="server"></asp:TextBox>
    Tabela ajutatoare:
    <asp:TextBox ID="tbTabela" runat="server"></asp:TextBox>
    <asp:Label ID="lblRezultate" runat="server" Text="Rezultate:<br />"></asp:Label>
</asp:Content>
