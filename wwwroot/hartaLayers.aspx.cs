﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Data.SqlClient;

public partial class hartaLayers : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        DBConnections connection = new DBConnections(Convert.ToInt16(Session["SESan"]));

        SqlLayers.ConnectionString = connection.Create();

        afisareCuloriGrid();
    }
    private void afisareCuloriGrid()
    {
        for (int a = 0; a < gvLayere.Rows.Count; a++)
        {
            if (gvLayere.Rows[a].RowType == DataControlRowType.DataRow)
            {
                TextBox vTbCuloareGrid = (TextBox)gvLayere.Rows[a].FindControl("tbCuloareGrid");
                vTbCuloareGrid.ForeColor = Color.FromName("#" + vTbCuloareGrid.Text);
                vTbCuloareGrid.BackColor = Color.FromName("#" + vTbCuloareGrid.Text);
            }
        }
    }
    protected void gvLayere_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(gvLayere, e, this);
    }
    protected void gvLayere_SelectedIndexChanged(object sender, EventArgs e)
    {
        btModifica.Visible = true;
        btSterge.Visible = true;
    }
    protected void btAdauga_Click(object sender, EventArgs e)
    {
        ViewState["tip"] = "a";
        tbDenumire.Text = "";
        tbCuloare.Text = "";
        tbCuloare.ForeColor = Color.FromName("#000");
        tbCuloare.BackColor = Color.FromName("#FFF");
        pnListaLayere.Visible = false;
        pnAdaugaLayer.Visible = true;
    }
    protected void btModifica_Click(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "SELECT layerId, unitateId, layerDenumire, COALESCE (layerCuloare, '0000CC') AS layerCuloare FROM hartaLayer WHERE (layerId = '" + gvLayere.SelectedValue.ToString() + "')";
        SqlDataReader vTabel = vCmd.ExecuteReader();
        if (vTabel.Read())
        {
            ViewState["tip"] = "m";
            tbDenumire.Text = vTabel["layerDenumire"].ToString();
            tbCuloare.Text = vTabel["layerCuloare"].ToString();
            tbCuloare.ForeColor = Color.FromName("#" + tbCuloare.Text);
            tbCuloare.BackColor = Color.FromName("#" + tbCuloare.Text);
            pnListaLayere.Visible = false;
            pnAdaugaLayer.Visible = true;
        }
        vTabel.Close();
        ManipuleazaBD.InchideConexiune(vCon);
    }
    protected void btSterge_Click(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        // verifica daca layer-ul este deja folosit
        vCmd.CommandText = "select count(*) from hartaShapes where layerId='" + gvLayere.SelectedValue.ToString() + "'";
        int vVerificare = Convert.ToInt32(vCmd.ExecuteScalar());
        vCmd.CommandText = "select count(*) from hartaTexte where layerId='" + gvLayere.SelectedValue.ToString() + "'";
        vVerificare += Convert.ToInt32(vCmd.ExecuteScalar());
        vCmd.CommandText = "select count(*) from coordonate where layerId='" + gvLayere.SelectedValue.ToString() + "'";
        vVerificare += Convert.ToInt32(vCmd.ExecuteScalar());
        if (vVerificare == 0)
        {
            vCmd.CommandText = "delete from hartaLayer where layerId='" + gvLayere.SelectedValue.ToString() + "'";
            vCmd.ExecuteNonQuery();
            gvLayere.DataBind();
        }
        else
        {
            valCustom.IsValid = false;
            valCustom.ErrorMessage = "Layer-ul ales nu poate fi şters deoarece este deja folosit !";
        }
        ManipuleazaBD.InchideConexiune(vCon);
    }
    protected void btAnuleazaLocalitate_Click(object sender, EventArgs e)
    {
        pnListaLayere.Visible = true;
        pnAdaugaLayer.Visible = false;
        afisareCuloriGrid();
    }
    protected void btSalveazaLocalitate_Click(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        if (ViewState["tip"].ToString() == "a")
        {
            vCmd.CommandText = "INSERT INTO hartaLayer (unitateId, layerDenumire, layerCuloare) VALUES ('" + Session["SESunitateId"].ToString() + "', N'" + tbDenumire.Text + "', N'" + tbCuloare.Text + "')";
            vCmd.ExecuteNonQuery();
        }
        else if (ViewState["tip"].ToString() == "m")
        {
            vCmd.CommandText = "UPDATE hartaLayer SET layerDenumire = N'" + tbDenumire.Text + "', layerCuloare = N'" + tbCuloare.Text + "' WHERE (layerId = '" + gvLayere.SelectedValue.ToString() + "')";
            vCmd.ExecuteNonQuery();
        }
        ManipuleazaBD.InchideConexiune(vCon);
        gvLayere.DataBind();
        pnListaLayere.Visible = true;
        pnAdaugaLayer.Visible = false;
        afisareCuloriGrid();
    }
    protected void gvLayere_PageIndexChanged(object sender, EventArgs e)
    {
        afisareCuloriGrid();
    }
}
