﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="raportListaGospodariiCuMembri.aspx.cs" Inherits="raportListaGospodariiCuMembri"
    Culture="ro-RO" UICulture="ro-RO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Rapoarte / Lista gospodării cu membri" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="pnGeneral" runat="server" CssClass="panel_general">
        <asp:Panel ID="pnFiltrareUnitati" runat="server" CssClass="cauta">
            <asp:Label ID="lblFJudet" runat="server" Text="Judeţ"></asp:Label>
            <asp:DropDownList ID="ddlFJudet" AutoPostBack="true" runat="server" OnPreRender="ddlFJudet_PreRender"
                OnSelectedIndexChanged="ddlFJudet_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:Label ID="lblUnitate" runat="server" Text="Unitatea:"></asp:Label>
            <asp:DropDownList ID="ddlUnitate" runat="server" OnInit="ddlUnitate_Init">
            </asp:DropDownList>
        </asp:Panel>
        <asp:Panel ID="pnFiltrare" runat="server" CssClass="cauta">
            <asp:Label ID="lblFiltrareDupa" runat="server" Text="Caută după:"></asp:Label>
            <asp:Label ID="lblVolum" runat="server" Text="vol."></asp:Label>
            <asp:TextBox ID="tbVolum" runat="server" AutoPostBack="False" Width="30px"></asp:TextBox>
            <asp:Label ID="lblNrPozitie" runat="server" Text="nr. poz."></asp:Label>
            <asp:TextBox ID="tbNrPozitie" runat="server" AutoPostBack="False" Width="30px"></asp:TextBox>
            <asp:Label ID="lblDeLaNr" runat="server" Text="Nr. casa de la"></asp:Label>
            <asp:TextBox ID="tbDeLaNr" runat="server" AutoPostBack="False" Width="40px"></asp:TextBox>
            <asp:Label ID="lblLaNr" runat="server" Text="Nr. casa la:"></asp:Label>
            <asp:TextBox ID="tbLaNr" runat="server" AutoPostBack="False" Width="40px"></asp:TextBox>
        </asp:Panel>
        <asp:Panel ID="pnFiltrare1" runat="server" CssClass="cauta">
            <asp:Label ID="lblStrainas" runat="server" Text="strainaș"></asp:Label>
            <asp:DropDownList ID="ddlStrainas" runat="server" AutoPostBack="False">
                <asp:ListItem Value="%">toți</asp:ListItem>
                <asp:ListItem Value="1">da</asp:ListItem>
                <asp:ListItem Value="0">nu</asp:ListItem>
            </asp:DropDownList>
            <asp:Label ID="lblStrada" runat="server" Text="str."></asp:Label>
            <asp:TextBox ID="tbStrada" runat="server" AutoPostBack="False" Width="100px"></asp:TextBox>
            <asp:Label ID="lblLocalitate" runat="server" Text="loc."></asp:Label>
            <asp:TextBox ID="tbLocalitate" runat="server" AutoPostBack="False" Width="100px"></asp:TextBox>
            <asp:Label ID="lblDecedat" runat="server" Text="Decedat"></asp:Label>
            <asp:DropDownList ID="ddlFDecedat" runat="server">
                <asp:ListItem Text="-toti-" Value="%"></asp:ListItem>
                <asp:ListItem Text="DA" Value="1"></asp:ListItem>
                <asp:ListItem Text="NU" Value="0"></asp:ListItem>
            </asp:DropDownList>
             <asp:Label ID="Label1" runat="server" Text="Orodonare dupa"></asp:Label>
             <asp:DropDownList ID="orderDropDown" runat="server">
                <asp:ListItem Text="volum" Value="volum"></asp:ListItem>
                <asp:ListItem Text="pozitie" Value="nrPozitie"></asp:ListItem>
            </asp:DropDownList>
        </asp:Panel>
        <asp:Panel ID="pnFiltrareNrCasa" runat="server" CssClass="cauta" Visible="false">
            <asp:Label ID="lblNrCasaDeLa" runat="server" Text="Nr. casa de la"></asp:Label>
            <asp:TextBox ID="tbNrCasaDeLa" Width="50px" Text="0" runat="server"></asp:TextBox>
            <asp:Label ID="lblNrCasaLa" runat="server" Text="Nr. casa la"></asp:Label>
            <asp:TextBox ID="tbNrCasaLa" Width="50px" Text="9999" runat="server"></asp:TextBox>

        </asp:Panel>
        <asp:Panel ID="pnButoane" runat="server" CssClass="butoane">
            <asp:Button ID="btTiparire" runat="server" Text="tipărește" CssClass="buton" OnClick="btnFiltrare_Click" />
        </asp:Panel>
        <asp:Panel ID="pnRaport" runat="server" CssClass="panel_general">
        </asp:Panel>
    </asp:Panel>
</asp:Content>
