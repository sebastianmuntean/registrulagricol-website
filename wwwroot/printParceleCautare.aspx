﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="printParceleCautare.aspx.cs" Inherits="printParceleCautare" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Tiparire parcele" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <rsweb:ReportViewer ID="rvParceleCautare" runat="server" Font-Names="Verdana" Font-Size="8pt"
        Height="600px" Width="100%">
        <LocalReport ReportPath="rapoarte\raportParceleCautare.rdlc">
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="dsRapoarte_dtUnitati" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource2" Name="dsRapoarte_dtRapParceleCautare" />
            </DataSources>
        </LocalReport>
    </rsweb:ReportViewer>
    <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetData" TypeName="dsRapoarteTableAdapters.dtRapParceleCautareTableAdapter">
        <SelectParameters>
            <asp:SessionParameter Name="an" SessionField="SESan" Type="Int32" />
            <asp:SessionParameter Name="parcelaCategorie" SessionField="cpCategoria" Type="String" />
            <asp:SessionParameter Name="parcelaLocalitate" SessionField="parcelaLocalitate" Type="String" />
            <asp:SessionParameter Name="parcelaCF" SessionField="cpCF" Type="String" />
            <asp:SessionParameter Name="parcelaDenumire" SessionField="cpDenumire" Type="String" />
            <asp:SessionParameter Name="parcelaNrBloc" SessionField="cpBloc" Type="String" />
            <asp:SessionParameter Name="parcelaNrTopo" SessionField="cpTopo" Type="String" />
            <asp:SessionParameter Name="parcelaNrCadastral" SessionField="cpNrCadastral" Type="String" />
            <asp:SessionParameter Name="parcelaTitluProprietate" SessionField="cpTitluProprietate"
                Type="String" />
            <asp:SessionParameter Name="parcelaNrCadastralProvizoriu" SessionField="cpNrCadastralProvizoriu"
                Type="String" />
            <asp:SessionParameter Name="unitateId" SessionField="cpUnitate" Type="String" />
            <asp:SessionParameter Name="judetId" SessionField="cpJudet" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetData" TypeName="dsRapoarteTableAdapters.dtUnitatiTableAdapter">
        <SelectParameters>
            <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
