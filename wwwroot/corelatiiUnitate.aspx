﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="corelatiiUnitate.aspx.cs" Inherits="corelatiiUnitate" Culture="ro-RO" UICulture="ro-RO"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Configurări / Execută verificarea corelațiilor pentru toate gospodăriile din unitate" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="upUpdatePanel" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnAfiseazaMesaje" runat="server">
                <asp:ValidationSummary ID="valSum" runat="server" DisplayMode="SingleParagraph"
                    Visible="true" ValidationGroup="GrupValidare" CssClass="validator" ForeColor="" />
            </asp:Panel>
            <asp:Panel ID="pnl" CssClass="adauga" runat="server">
                <h2>
                    <asp:Label ID="lbl" runat="server" Text="Atenție! Această operație necesită un timp de așteptare destul de mare, în funcție de numărul de gospodării ale unității."></asp:Label>
                </h2>
            </asp:Panel>
            <asp:Panel ID="pnListaButoanePrincipale" CssClass="butoane" runat="server">
                <asp:Button CssClass="buton" ID="btCorelatii" runat="server" Text="verificarea corelaţiilor pentru toate gospodăriile"
                    OnClick="btCorelatii_Click" Visible="true"  ValidationGroup="GrupValidare"/>
                <asp:Button CssClass="buton" ID="btListaGospodarii" runat="server" Text="listă gospodării"
                    OnClick="btListaGospodarii_Click" />
            </asp:Panel>
            <asp:Panel ID="Panel1" CssClass="ascunsa" runat="server">
                <!-- //////////// VALIDATOARE  \\\\\\\\\\\\\\\\\\ -->
               <asp:CustomValidator ID="valCustom" runat="server" ErrorMessage="------------" ValidationGroup="GrupValidare"></asp:CustomValidator>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
