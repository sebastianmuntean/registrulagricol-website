﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class help : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "help", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
        }
    }
    protected void lblPaginaDenumire_Init(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select top(1) paginaDenumire from pagini where paginaId='" + Request.QueryString["pagina"].ToString() + "'";
        try { lblPaginaDenumire.Text = vCmd.ExecuteScalar().ToString(); }
        catch { }
        finally { ManipuleazaBD.InchideConexiune(vCon); }
    }
    protected void lblHelp_Init(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select top(1) helpText from help where paginaId='" + Request.QueryString["pagina"].ToString() + "'";
        try { lblHelp.Text = vCmd.ExecuteScalar().ToString(); }
        catch { }
        finally { ManipuleazaBD.InchideConexiune(vCon); }
    }
    protected void btInapoi_Init(object sender, EventArgs e)
    {
        btInapoi.PostBackUrl = Request.UrlReferrer.AbsolutePath;
    }
}
