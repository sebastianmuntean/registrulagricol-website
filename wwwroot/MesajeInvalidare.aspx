﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="MesajeInvalidare.aspx.cs" Inherits="MesajeInvalidare"  Culture="ro-RO" UICulture="ro-RO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Gospodării / Mesaje de invalidare" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="pnListaMesajeInvalidari" runat="server" CssClass="panel_general" Visible="true">
        <asp:Panel ID="pnGvMesajeInvalidari" runat="server">
            <asp:GridView ID="gvMesajeInvalidari" runat="server" 
                DataSourceID="SqlMesajeInvalidari" AllowPaging="True" AllowSorting="True" 
                AutoGenerateColumns="False" CssClass="tabela">
                <Columns>
                    <asp:TemplateField HeaderText="Mesaje de invalidare" 
                        SortExpression="mesajInvalidare">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("mesajInvalidare") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("mesajInvalidare") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <asp:SqlDataSource ID="SqlMesajeInvalidari" runat="server" 
                SelectCommand="SELECT [mesajInvalidare] FROM [mesajeInvalidari] WHERE (([unitateId] = @unitateId) AND ([gospodarieId] = @gospodarieId) AND ([an] = @an))">
                <SelectParameters>
                    <asp:SessionParameter DefaultValue="0" Name="unitateId" SessionField="SESunitateId"
                        Type="Int32" />
                    <asp:SessionParameter DefaultValue="0" Name="gospodarieId" SessionField="SESgospodarieId"
                        Type="Int32" />
                    <asp:SessionParameter DefaultValue="2011" Name="an" SessionField="SESan" Type="Int32" />
                </SelectParameters>
            </asp:SqlDataSource>
            <asp:Panel ID="pnListaButoanePrincipale" CssClass="butoane" runat="server">
                <asp:Button ID="btListaCapitole" runat="server" CssClass="buton" Text="listă gospodării"
                    Visible="true" PostBackUrl="~/Gospodarii.aspx" />
            </asp:Panel>
        </asp:Panel>
    </asp:Panel><!-- SELECT [mesajInvalidare] FROM [mesajeInvalidari] WHERE (([unitateId] = @unitateId) AND ([gospodarieId] = @gospodarieId) AND ([an] = @an))" -->
</asp:Content>
