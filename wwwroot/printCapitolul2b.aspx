﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="printCapitolul2b.aspx.cs" Inherits="printCapitolul2b" Culture="ro-RO"
    UICulture="ro-RO" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt"
        Height="400px" Width="400px">
        <LocalReport ReportPath="rapoarte\raportCapitolul2b.rdlc">
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="dsRapoarte_dtGospodarii" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource2" Name="dsRapoarte_dtUnitati" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource3" Name="dsRapoarte_dtParcele" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource4" 
                    Name="dsRapoarte_dtRapCapitoleAn1" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource5" 
                    Name="dsRapoarte_dtRapCapitoleAn2" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource6" 
                    Name="dsRapoarte_dtRapCapitoleAn3" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource7" 
                    Name="dsRapoarte_dtRapCapitoleAn4" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource8" Name="dsRapoarte_dtRapCapitoleAn5" />
            </DataSources>
        </LocalReport>
    </rsweb:ReportViewer>
    <asp:ObjectDataSource ID="ObjectDataSource4" runat="server" SelectMethod="GetData2b1"
        TypeName="dsRapoarteTableAdapters.dtRapCapitoleTableAdapter" InsertMethod="Insert"
        OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:SessionParameter Name="utilizatorId" SessionField="SESutilizatorId" Type="Int32" />
            <asp:QueryStringParameter Name="capitol" QueryStringField="codCapitol" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource5" runat="server" SelectMethod="GetData2b2"
        TypeName="dsRapoarteTableAdapters.dtRapCapitoleTableAdapter" InsertMethod="Insert"
        OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:SessionParameter Name="utilizatorId" SessionField="SESutilizatorId" Type="Int32" />
            <asp:QueryStringParameter Name="capitol" QueryStringField="codCapitol" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource6" runat="server" SelectMethod="GetData2b3"
        TypeName="dsRapoarteTableAdapters.dtRapCapitoleTableAdapter" InsertMethod="Insert"
        OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:SessionParameter Name="utilizatorId" SessionField="SESutilizatorId" Type="Int32" />
            <asp:QueryStringParameter Name="capitol" QueryStringField="codCapitol" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource7" runat="server" SelectMethod="GetData2b4"
        TypeName="dsRapoarteTableAdapters.dtRapCapitoleTableAdapter" InsertMethod="Insert"
        OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:SessionParameter Name="utilizatorId" SessionField="SESutilizatorId" Type="Int32" />
            <asp:QueryStringParameter Name="capitol" QueryStringField="codCapitol" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource8" runat="server" SelectMethod="GetData2b5"
        TypeName="dsRapoarteTableAdapters.dtRapCapitoleTableAdapter" InsertMethod="Insert"
        OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:SessionParameter Name="utilizatorId" SessionField="SESutilizatorId" Type="Int32" />
            <asp:QueryStringParameter Name="capitol" QueryStringField="codCapitol" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource3" runat="server" DeleteMethod="Delete"
        InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData"
        TypeName="dsRapoarteTableAdapters.dtParceleTableAdapter" UpdateMethod="Update">
        <SelectParameters>
            <asp:SessionParameter Name="an" SessionField="SESan" Type="Int32" />
            <asp:SessionParameter Name="gospodarieId" SessionField="SESgospodarieId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetData" TypeName="dsRapoarteTableAdapters.dtUnitatiTableAdapter">
        <SelectParameters>
            <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" DeleteMethod="Delete"
        InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData"
        TypeName="dsRapoarteTableAdapters.dtGospodariiTableAdapter" UpdateMethod="Update">
        <DeleteParameters>
            <asp:Parameter Name="Original_gospodarieId" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="unitateId" Type="Int32" />
            <asp:Parameter Name="volum" Type="String" />
            <asp:Parameter Name="nrPozitie" Type="String" />
            <asp:Parameter Name="codSiruta" Type="String" />
            <asp:Parameter Name="tip" Type="String" />
            <asp:Parameter Name="nrInt" Type="Int32" />
            <asp:Parameter Name="codExploatatie" Type="String" />
            <asp:Parameter Name="codUnic" Type="String" />
            <asp:Parameter Name="judet" Type="String" />
            <asp:Parameter Name="localitate" Type="String" />
            <asp:Parameter Name="persJuridica" Type="Boolean" />
            <asp:Parameter Name="jUnitate" Type="String" />
            <asp:Parameter Name="jSubunitate" Type="String" />
            <asp:Parameter Name="jCodFiscal" Type="String" />
            <asp:Parameter Name="jNumeReprez" Type="String" />
            <asp:Parameter Name="strainas" Type="Boolean" />
            <asp:Parameter Name="sStrada" Type="String" />
            <asp:Parameter Name="sNr" Type="String" />
            <asp:Parameter Name="sBl" Type="String" />
            <asp:Parameter Name="sSc" Type="String" />
            <asp:Parameter Name="sEtj" Type="String" />
            <asp:Parameter Name="sAp" Type="String" />
            <asp:Parameter Name="sJudet" Type="String" />
            <asp:Parameter Name="sLocalitate" Type="String" />
            <asp:Parameter Name="Original_gospodarieId" Type="Int32" />
        </UpdateParameters>
        <SelectParameters>
            <asp:SessionParameter Name="gospodarieId" SessionField="SESgospodarieId" Type="Int32" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="unitateId" Type="Int32" />
            <asp:Parameter Name="volum" Type="String" />
            <asp:Parameter Name="nrPozitie" Type="String" />
            <asp:Parameter Name="codSiruta" Type="String" />
            <asp:Parameter Name="tip" Type="String" />
            <asp:Parameter Name="nrInt" Type="Int32" />
            <asp:Parameter Name="codExploatatie" Type="String" />
            <asp:Parameter Name="codUnic" Type="String" />
            <asp:Parameter Name="judet" Type="String" />
            <asp:Parameter Name="localitate" Type="String" />
            <asp:Parameter Name="persJuridica" Type="Boolean" />
            <asp:Parameter Name="jUnitate" Type="String" />
            <asp:Parameter Name="jSubunitate" Type="String" />
            <asp:Parameter Name="jCodFiscal" Type="String" />
            <asp:Parameter Name="jNumeReprez" Type="String" />
            <asp:Parameter Name="strainas" Type="Boolean" />
            <asp:Parameter Name="sStrada" Type="String" />
            <asp:Parameter Name="sNr" Type="String" />
            <asp:Parameter Name="sBl" Type="String" />
            <asp:Parameter Name="sSc" Type="String" />
            <asp:Parameter Name="sEtj" Type="String" />
            <asp:Parameter Name="sAp" Type="String" />
            <asp:Parameter Name="sJudet" Type="String" />
            <asp:Parameter Name="sLocalitate" Type="String" />
        </InsertParameters>
    </asp:ObjectDataSource>
</asp:Content>
