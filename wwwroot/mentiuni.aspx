﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="mentiuni.aspx.cs" Inherits="mentiuni" EnableEventValidation="false" Culture="ro-RO" UICulture="ro-RO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Configurare / Mențiuni" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="upMentiuni" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnAfiseazaMesaje" runat="server">
                <asp:ValidationSummary ID="valCapitole" runat="server" DisplayMode="SingleParagraph"
                    Visible="true" CssClass="validator" ForeColor="" />
            </asp:Panel>
            <asp:Panel ID="Panel5" CssClass="ascunsa" runat="server">
                <!-- //////////// VALIDATOARE  \\\\\\\\\\\\\\\\\\ -->
                <asp:CustomValidator ID="valCapitol" runat="server" ErrorMessage=""></asp:CustomValidator>
            </asp:Panel>
            <asp:Panel ID="pnListaMentiuni" runat="server" CssClass="panel_general" Visible="true">
                <asp:Panel ID="pnCautare" runat="server" CssClass="cauta">
                </asp:Panel>
                <asp:Panel ID="pnGrid" runat="server" Visible="true">
                    <asp:GridView ID="gvMentiuni" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        BorderStyle="None" CellPadding="3" GridLines="Vertical" ShowFooter="True" CssClass="tabela"
                        EmptyDataText="Nu sunt adaugate inregistrari" AllowPaging="True" DataKeyNames="mentiuneId"
                        DataSourceID="SqlMentiuni" OnRowDataBound="gvMentiuni_RowDataBound" OnSelectedIndexChanged="gvMentiuni_SelectedIndexChanged">
                        <Columns>
                            <asp:BoundField DataField="mentiuneText" HeaderText="Text mentiune" SortExpression="mentiuneText" />
                        </Columns>
                        <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                        <HeaderStyle Font-Bold="True" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlMentiuni" runat="server" 
                        SelectCommand="SELECT [mentiuneId], [mentiuneText] FROM [mentiuni] WHERE ([gospodarieId] = @gospodarieId)">
                        <SelectParameters>
                            <asp:SessionParameter Name="gospodarieId" SessionField="SESgospodarieId" Type="Int64" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </asp:Panel>
                <asp:Panel ID="pnButoane" CssClass="butoane" runat="server">
                    <asp:Button CssClass="buton" ID="btModifica" Visible="false" runat="server" Text="modifică mențiune"
                        OnClick="btModifica_Click" />
                    <asp:Button CssClass="buton" ID="btSterge" Visible="false" runat="server" Text="șterge mențiune"
                        OnClientClick="return confirm (&quot;Sigur ștergeți ?&quot;)" OnClick="btSterge_Click" />
                    <asp:Button CssClass="buton" ID="btAdauga" runat="server" Text="adaugă mențiune"
                        OnClick="btAdauga_Click" />
                        <asp:Button CssClass="buton" ID="btTiparire" runat="server" 
                        Text="tipărire capitol" onclick="btTiparire_Click" />
                </asp:Panel>
                <!-- adaugam unitate -->
                <asp:Panel ID="pnAdaugaMentiuni" CssClass="panel_general" runat="server" Visible="false">
                    <asp:Panel ID="Panel1" CssClass="adauga" runat="server">
                        <asp:Panel ID="pnEroare" runat="server">
                            <h2>
                                <asp:Label ID="lblAdaugaMentiuni" runat="server" Text="ADAUGĂ MENȚIUNI"></asp:Label></h2>
                            <asp:Label ID="lblEroare" runat="server" CssClass="validator" Text="Eroare" Visible="false" />
                            <asp:ValidationSummary ID="valSumUnitati" runat="server" DisplayMode="SingleParagraph"
                                Visible="true" ValidationGroup="GrupValidareSabloaneCapitole" CssClass="validator"
                                ForeColor="" />
                        </asp:Panel>
                        <asp:Panel ID="Panel3" runat="server">
                            <p>
                                <asp:Label ID="lblText" runat="server" Text="Text mențiune"></asp:Label>
                                <asp:TextBox ID="tbText" TextMode="MultiLine" Width="800px" Rows="10" runat="server"></asp:TextBox>
                            </p>
                        </asp:Panel>
                        <asp:Panel ID="PanelValidatoareAscuns" CssClass="ascunsa" runat="server">
                            <!-- //////////// VALIDATOARE  \\\\\\\\\\\\\\\\\\ -->
                            <!-- //////////// VALIDATOARE  \\\\\\\\\\\\\\\\\\ -->
                        </asp:Panel>
                        <asp:Panel ID="pnButoaneAdaugare" runat="server" CssClass="butoane">
                            <asp:Button ID="btSalveazaMentiune" runat="server" CssClass="buton" Text="adauga mențiune"
                                ValidationGroup="GrupValidareSabloaneCapitole" OnClick="btSalveazaMentiune_Click" />
                            <asp:Button ID="btAnuleazaMentiune" runat="server" CssClass="buton" Text="listă mențiuni"
                                OnClick="btAnuleazaMentiune_Click" />
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
