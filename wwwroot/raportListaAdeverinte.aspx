﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="raportListaAdeverinte.aspx.cs" Inherits="raportListaAdeverinte" Culture="ro-RO"
    UICulture="ro-RO" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function AutoCompleteExtenderTipSablon_ItemSelected(source, eventArgs) {
            var vControl = document.getElementById("ctl00_ContentPlaceHolder1_tbTipSablonId");
            var vStr = eventArgs.get_value();
            vControl.value = vStr;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Rapoarte / Listă membri" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="pnGeneral" runat="server" CssClass="panel_general">
        <asp:Panel ID="pnFiltrareUnitate" runat="server" CssClass="cauta">
            <asp:Label ID="lblFJudet" runat="server" Text="Judeţ"></asp:Label>
            <asp:DropDownList ID="ddlFJudet" AutoPostBack="true" runat="server" OnPreRender="ddlFJudet_PreRender"
                OnSelectedIndexChanged="ddlFJudet_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:Label ID="lblUnitate" runat="server" Text="Unitatea:"></asp:Label>
            <asp:DropDownList ID="ddlUnitate" runat="server" OnInit="ddlUnitate_Init">
            </asp:DropDownList>
        </asp:Panel>
        <asp:Panel ID="pnFiltrare" runat="server" CssClass="cauta">
            <asp:Label ID="lblFiltrareDupa" runat="server" Text="Caută după:"></asp:Label>
            <asp:Label ID="lblVolum" runat="server" Text="vol."></asp:Label>
            <asp:TextBox ID="tbVolum" runat="server" AutoPostBack="False" Width="30px"></asp:TextBox>
            <asp:Label ID="lblDeLaNr" runat="server" Text="Nr. casa de la"></asp:Label>
            <asp:TextBox ID="tbfDeLaNr" runat="server" AutoPostBack="False" Width="40px"></asp:TextBox>
            <asp:Label ID="lblLaNr" runat="server" Text="Nr. casa la"></asp:Label>
            <asp:TextBox ID="tbfLaNr" runat="server" AutoPostBack="False" Width="40px"></asp:TextBox>
            <asp:Label ID="LabelStrainas" runat="server" Text="strainaș"></asp:Label>
            <asp:DropDownList ID="ddlStrainas" runat="server" AutoPostBack="False">
                <asp:ListItem Value="%">toți</asp:ListItem>
                <asp:ListItem Value="1">da</asp:ListItem>
                <asp:ListItem Value="0">nu</asp:ListItem>
            </asp:DropDownList>
            <asp:Label ID="lblStrada" runat="server" Text="str."></asp:Label>
            <asp:TextBox ID="tbStrada" runat="server" AutoPostBack="False" Width="100px"></asp:TextBox>
            <asp:Label ID="lblLocalitate" runat="server" Text="loc."></asp:Label>
            <asp:TextBox ID="tbLocalitate" runat="server" AutoPostBack="False" Width="100px"></asp:TextBox>
        </asp:Panel>
        <asp:Panel ID="pnFiltrare1" runat="server" CssClass="cauta">
            <asp:Label ID="lblFSex" runat="server" Text="Sex"></asp:Label>
            <asp:DropDownList ID="ddlFSex" runat="server">
                <asp:ListItem Value="%" Text="toţi"></asp:ListItem>
                <asp:ListItem Value="1" Text="masculin"></asp:ListItem>
                <asp:ListItem Value="2" Text="feminin"></asp:ListItem>
            </asp:DropDownList>
            <asp:Label ID="lblFDeLaVarsta" runat="server" Text="De la varsta"></asp:Label>
            <asp:TextBox ID="tbFDeLaVarsta" Text="0" runat="server"></asp:TextBox>
            <asp:Label ID="lblFLaVarsta" runat="server" Text="La varsta"></asp:Label>
            <asp:TextBox ID="tbFLaVarsta" Text="9999" runat="server"></asp:TextBox>
            <asp:Label ID="lblFNume" runat="server" Text="Nume"></asp:Label>
            <asp:TextBox ID="tbFNume" Width="100px" runat="server"></asp:TextBox>
            <asp:Label ID="lblDecedat" runat="server" Text="Decedat"></asp:Label>
            <asp:DropDownList ID="ddlFDecedat" runat="server">
                <asp:ListItem Text="-toti-" Value="%"></asp:ListItem>
                <asp:ListItem Text="DA" Value="1"></asp:ListItem>
                <asp:ListItem Text="NU" Value="0"></asp:ListItem>
            </asp:DropDownList>
        </asp:Panel>
        <asp:Panel ID="pnFiltrareNrCasa" runat="server" CssClass="cauta" Visible="false">
            <asp:Label ID="lblNrCasaDeLa" runat="server" Text="Nr. casa de la"></asp:Label>
            <asp:TextBox ID="tbNrCasaDeLa" Width="50px" Text="0" runat="server"></asp:TextBox>
            <asp:Label ID="lblNrCasaLa" runat="server" Text="Nr. casa la"></asp:Label>
            <asp:TextBox ID="tbNrCasaLa" Width="50px" Text="9999" runat="server"></asp:TextBox>
        </asp:Panel>
        <asp:Panel ID="pnFiltrare2" runat="server" CssClass="cauta">
            <asp:Label ID="lblTipSablon" runat="server" Text="Tip adeverinţă"></asp:Label>
            <asp:TextBox ID="tbTipSablon" runat="server"
                Width="400px" AutoPostBack="true" OnTextChanged="tbTipSablon_TextChanged"></asp:TextBox>
            <asp:TextBoxWatermarkExtender ID="tbTipSablon_TextBoxWatermarkExtender" runat="server"
                Enabled="True" TargetControlID="tbTipSablon" WatermarkCssClass="watermarkTextBox"
                WatermarkText="Cautare tip adeverinta">
            </asp:TextBoxWatermarkExtender>
            <asp:AutoCompleteExtender ID="AutoCompleteExtenderTipSablon" runat="server" BehaviorID="AutoCompleteEx3"
                CompletionInterval="10" CompletionListCssClass="autocomplete_completionListElement"
                CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem" CompletionListItemCssClass="autocomplete_listItem"
                CompletionSetCount="5" ContextKey="cont" DelimiterCharacters=";,:" EnableCaching="True"
                Enabled="True" FirstRowSelected="True" MinimumPrefixLength="1" OnClientItemSelected="AutoCompleteExtenderTipSablon_ItemSelected"
                ServiceMethod="GetCompletionList" ServicePath="~/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="false"
                TargetControlID="tbTipSablon" UseContextKey="True">
            </asp:AutoCompleteExtender>
            <asp:TextBox ID="tbTipSablonId" Style="display: none; visibility: hidden;" runat="server"></asp:TextBox>
        </asp:Panel>
        <asp:Panel ID="pnButoane" runat="server" CssClass="butoane">
            <asp:Button ID="btTiparire" runat="server" Text="tipărește" CssClass="buton" OnClick="btnFiltrare_Click" />
        </asp:Panel>
        <asp:Panel ID="pnRaport" runat="server" CssClass="panel_general">
        </asp:Panel>
    </asp:Panel>
</asp:Content>
