<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="printCapitoleToateCompact.aspx.cs" Inherits="printCapitoleToateCompact" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt"
        Height="400px" Width="400px">
        <LocalReport ReportPath="rapoarte\raportCapitoleToateCompact.rdlc">
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="dsRapoarte_dtUnitati" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource2" Name="dsRapoarte_dtMembri" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource3" Name="dsRapoarte_dtGospodarii" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource4" Name="dsRapoarte_dtMentiuni" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource5" Name="dsRapoarte_dtParcele" />
                <%--<rsweb:ReportDataSource DataSourceId="ObjectDataSource6" Name="dsRapoarte_dtPaduri" />--%>
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource7" Name="dsRapoarte_dtCapitole" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource8" Name="dsRapoarte_dtRapCapitole2a" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource9" Name="dsRapoarte_dtRapCapitole3" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource10" Name="dsRapoarte_dtRapCapitole4a" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource11" Name="dsRapoarte_dtRapCapitole4b" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource12" Name="dsRapoarte_dtRapCapitole4c" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource13" Name="dsRapoarte_dtRapCapitole5a" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource14" Name="dsRapoarte_dtRapCapitole5b" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource15" Name="dsRapoarte_dtRapCapitole5c" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource16" Name="dsRapoarte_dtRapCapitole5d" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource17" Name="dsRapoarte_dtRapCapitole6" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource18" Name="dsRapoarte_dtRapCapitole7" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource19" Name="dsRapoarte_dtRapCapitole8" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource20" Name="dsRapoarte_dtRapCapitole9" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource21" Name="dsRapoarte_dtRapCapitole10a" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource22" Name="dsRapoarte_dtRapCapitole10b" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource23" Name="dsRapoarte_dtRapCapitole11" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource24" 
                    Name="dsRapoarte_dtRapCapitoleAn1" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource25" 
                    Name="dsRapoarte_dtRapCapitoleAn2" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource26" 
                    Name="dsRapoarte_dtRapCapitoleAn3" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource27" 
                    Name="dsRapoarte_dtRapCapitoleAn4" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource28" Name="dsRapoarte_dtRapCapitoleAn5" />
            </DataSources>
        </LocalReport>
    </rsweb:ReportViewer>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetData"
        TypeName="dsRapoarteTableAdapters.dtUnitatiTableAdapter" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" SelectMethod="GetData"
        TypeName="dsRapoarteTableAdapters.dtMembriTableAdapter" DeleteMethod="Delete"
        InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" UpdateMethod="Update">
        <DeleteParameters>
            <asp:Parameter Name="Original_capitolId" Type="Int64" />
        </DeleteParameters>
        <SelectParameters>
            <asp:SessionParameter Name="gospodarieId" SessionField="SESgospodarieId" Type="Int32" />
            <asp:SessionParameter Name="an" SessionField="SESan" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource3" runat="server" SelectMethod="GetData"
        TypeName="dsRapoarteTableAdapters.dtGospodariiTableAdapter" DeleteMethod="Delete"
        InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" UpdateMethod="Update">
        <DeleteParameters>
            <asp:Parameter Name="Original_gospodarieId" Type="Int32" />
        </DeleteParameters>
        <SelectParameters>
            <asp:SessionParameter Name="gospodarieId" SessionField="SESgospodarieId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource4" runat="server" SelectMethod="GetData"
        TypeName="dsRapoarteTableAdapters.dtMentiuniTableAdapter" InsertMethod="Insert"
        OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:SessionParameter Name="gospodarieId" SessionField="SESgospodarieId" Type="Int64" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="mentiuneText" Type="String" />
        </InsertParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource5" runat="server" SelectMethod="GetData"
        TypeName="dsRapoarteTableAdapters.dtParceleTableAdapter" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:SessionParameter Name="an" SessionField="SESan" Type="Int32" />
            <asp:SessionParameter Name="gospodarieId" SessionField="SESgospodarieId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
   <%-- <asp:ObjectDataSource ID="ObjectDataSource6" runat="server" SelectMethod="GetData"
        TypeName="dsRapoarteTableAdapters.dtPaduriTableAdapter" DeleteMethod="Delete"
        InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" UpdateMethod="Update">
        <DeleteParameters>
            <asp:Parameter Name="Original_paduriId" Type="Int32" />
        </DeleteParameters>
        <SelectParameters>
            <asp:SessionParameter Name="an" SessionField="SESan" Type="Int32" />
            <asp:SessionParameter Name="gospodarieId" SessionField="SESgospodarieId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>--%>
    <asp:ObjectDataSource ID="ObjectDataSource7" runat="server" SelectMethod="GetData"
        TypeName="dsRapoarteTableAdapters.dtCapitoleTableAdapter" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" Type="Int32" />
            <asp:SessionParameter Name="gopodarieId" SessionField="SESgospodarieId" Type="Int32" />
            <asp:SessionParameter Name="an" SessionField="SESan" Type="Int32" />
            <asp:Parameter DefaultValue="2a" Name="codCapitol" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource8" runat="server" SelectMethod="GetData"
        TypeName="dsRapoarteTableAdapters.dtRapCapitoleTableAdapter" InsertMethod="Insert"
        OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:SessionParameter Name="utilizatorId" SessionField="SESutilizatorId" Type="Int32" />
            <asp:Parameter DefaultValue="2a" Name="capitol" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource9" runat="server" SelectMethod="GetData"
        TypeName="dsRapoarteTableAdapters.dtRapCapitoleTableAdapter" InsertMethod="Insert"
        OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:SessionParameter Name="utilizatorId" SessionField="SESutilizatorId" Type="Int32" />
            <asp:Parameter DefaultValue="3" Name="capitol" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource10" runat="server" SelectMethod="GetData"
        TypeName="dsRapoarteTableAdapters.dtRapCapitoleTableAdapter" InsertMethod="Insert"
        OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:SessionParameter Name="utilizatorId" SessionField="SESutilizatorId" Type="Int32" />
            <asp:Parameter DefaultValue="4a" Name="capitol" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource11" runat="server" SelectMethod="GetData"
        TypeName="dsRapoarteTableAdapters.dtRapCapitoleTableAdapter" InsertMethod="Insert"
        OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:SessionParameter Name="utilizatorId" SessionField="SESutilizatorId" Type="Int32" />
            <asp:Parameter DefaultValue="4b" Name="capitol" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource12" runat="server" SelectMethod="GetData"
        TypeName="dsRapoarteTableAdapters.dtRapCapitoleTableAdapter" InsertMethod="Insert"
        OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:SessionParameter Name="utilizatorId" SessionField="SESutilizatorId" Type="Int32" />
            <asp:Parameter DefaultValue="4c" Name="capitol" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource13" runat="server" SelectMethod="GetData"
        TypeName="dsRapoarteTableAdapters.dtRapCapitoleTableAdapter" InsertMethod="Insert"
        OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:SessionParameter Name="utilizatorId" SessionField="SESutilizatorId" Type="Int32" />
            <asp:Parameter DefaultValue="5a" Name="capitol" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource14" runat="server" SelectMethod="GetData"
        TypeName="dsRapoarteTableAdapters.dtRapCapitoleTableAdapter" InsertMethod="Insert"
        OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:SessionParameter Name="utilizatorId" SessionField="SESutilizatorId" Type="Int32" />
            <asp:Parameter DefaultValue="5b" Name="capitol" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource15" runat="server" SelectMethod="GetData"
        TypeName="dsRapoarteTableAdapters.dtRapCapitoleTableAdapter" InsertMethod="Insert"
        OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:SessionParameter Name="utilizatorId" SessionField="SESutilizatorId" Type="Int32" />
            <asp:Parameter DefaultValue="5c" Name="capitol" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource16" runat="server" SelectMethod="GetData"
        TypeName="dsRapoarteTableAdapters.dtRapCapitoleTableAdapter" InsertMethod="Insert"
        OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:SessionParameter Name="utilizatorId" SessionField="SESutilizatorId" Type="Int32" />
            <asp:Parameter DefaultValue="5d" Name="capitol" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource17" runat="server" SelectMethod="GetData"
        TypeName="dsRapoarteTableAdapters.dtRapCapitoleTableAdapter" InsertMethod="Insert"
        OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:SessionParameter Name="utilizatorId" SessionField="SESutilizatorId" Type="Int32" />
            <asp:Parameter DefaultValue="6" Name="capitol" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource18" runat="server" SelectMethod="GetData"
        TypeName="dsRapoarteTableAdapters.dtRapCapitoleTableAdapter" InsertMethod="Insert"
        OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:SessionParameter Name="utilizatorId" SessionField="SESutilizatorId" Type="Int32" />
            <asp:Parameter DefaultValue="7" Name="capitol" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource19" runat="server" SelectMethod="GetData"
        TypeName="dsRapoarteTableAdapters.dtRapCapitoleTableAdapter" InsertMethod="Insert"
        OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:SessionParameter Name="utilizatorId" SessionField="SESutilizatorId" Type="Int32" />
            <asp:Parameter DefaultValue="8" Name="capitol" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource20" runat="server" SelectMethod="GetData"
        TypeName="dsRapoarteTableAdapters.dtRapCapitoleTableAdapter" InsertMethod="Insert"
        OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:SessionParameter Name="utilizatorId" SessionField="SESutilizatorId" Type="Int32" />
            <asp:Parameter DefaultValue="9" Name="capitol" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource21" runat="server" SelectMethod="GetData"
        TypeName="dsRapoarteTableAdapters.dtRapCapitoleTableAdapter" InsertMethod="Insert"
        OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:SessionParameter Name="utilizatorId" SessionField="SESutilizatorId" Type="Int32" />
            <asp:Parameter DefaultValue="10a" Name="capitol" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource22" runat="server" SelectMethod="GetData"
        TypeName="dsRapoarteTableAdapters.dtRapCapitoleTableAdapter" InsertMethod="Insert"
        OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:SessionParameter Name="utilizatorId" SessionField="SESutilizatorId" Type="Int32" />
            <asp:SessionParameter Name="capitol" SessionField="10b" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource23" runat="server" SelectMethod="GetData"
        TypeName="dsRapoarteTableAdapters.dtRapCapitoleTableAdapter" InsertMethod="Insert"
        OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:SessionParameter Name="utilizatorId" SessionField="SESutilizatorId" Type="Int32" />
            <asp:Parameter DefaultValue="11" Name="capitol" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
        <asp:ObjectDataSource ID="ObjectDataSource24" runat="server" SelectMethod="GetData2b1"
        TypeName="dsRapoarteTableAdapters.dtRapCapitoleTableAdapter"
        OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:SessionParameter Name="utilizatorId" SessionField="SESutilizatorId" Type="Int32" />
            <asp:Parameter DefaultValue="2b" Name="capitol" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource25" runat="server" SelectMethod="GetData2b2"
        TypeName="dsRapoarteTableAdapters.dtRapCapitoleTableAdapter"
        OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:SessionParameter Name="utilizatorId" SessionField="SESutilizatorId" Type="Int32" />
            <asp:Parameter DefaultValue="2b" Name="capitol" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource26" runat="server" SelectMethod="GetData2b3"
        TypeName="dsRapoarteTableAdapters.dtRapCapitoleTableAdapter"
        OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:SessionParameter Name="utilizatorId" SessionField="SESutilizatorId" Type="Int32" />
            <asp:Parameter DefaultValue="2b" Name="capitol" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource27" runat="server" SelectMethod="GetData2b4"
        TypeName="dsRapoarteTableAdapters.dtRapCapitoleTableAdapter"
        OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:SessionParameter Name="utilizatorId" SessionField="SESutilizatorId" Type="Int32" />
            <asp:Parameter DefaultValue="2b" Name="capitol" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource28" runat="server" SelectMethod="GetData2b5"
        TypeName="dsRapoarteTableAdapters.dtRapCapitoleTableAdapter"
        OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:SessionParameter Name="utilizatorId" SessionField="SESutilizatorId" Type="Int32" />
            <asp:Parameter DefaultValue="2b" Name="capitol" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
