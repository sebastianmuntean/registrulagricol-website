﻿using System;
using System.Linq;
using Microsoft.Reporting.WebForms;
using System.Data;

public partial class printCapitoleToate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataTable reportDataTable = clsTiparireCapitole.UmpleRapCapitol11_CuSablon(Session["SESutilizatorId"].ToString(), Session["SESan"].ToString(), Session["SESgospodarieId"].ToString(), "11"); ;
          
            ReportDataSource ds = new ReportDataSource("dsRapoarte_sabloaneCapitole", reportDataTable);
            ReportViewer1.LocalReport.DataSources.Add(ds);

            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "printCapToate", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
        }
    }
    protected void Page_SaveStateComplete(object sender, EventArgs e)
    {
        Warning[] warnings;
        string[] streamids;
        string mimeType;
        string encoding;
        string fileNameExtension;
        byte[] bytes = ReportViewer1.LocalReport.Render("PDF", null, out mimeType, out encoding, out fileNameExtension, out streamids, out warnings);
        Response.Buffer = true;
        Response.Clear();
        Response.ContentType = mimeType;
        Response.AddHeader("content-disposition", "inline; filename=Nume" + fileNameExtension);
        Response.BinaryWrite(bytes);
        Response.Flush();
        Response.End();
        Response.Close();
    }
}
