﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="localitati.aspx.cs" Inherits="localitati" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Configurare / Localităţi" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="upLocalitati" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnAfiseazaMesaje" runat="server">
                <asp:ValidationSummary ID="valCapitole" runat="server" DisplayMode="SingleParagraph"
                    Visible="true" CssClass="validator" ForeColor="" />
            </asp:Panel>
            <asp:Panel ID="Panel5" CssClass="ascunsa" runat="server">
                <!-- //////////// VALIDATOARE  \\\\\\\\\\\\\\\\\\ -->
                <asp:CustomValidator ID="valCapitol" runat="server" ErrorMessage=""></asp:CustomValidator>
            </asp:Panel>
            <asp:Panel ID="pnListaLocalitati" runat="server" CssClass="panel_general" Visible="true">
                <asp:Panel ID="pnCautare" runat="server" CssClass="cauta">
                </asp:Panel>
                <asp:Panel ID="pnGrid" runat="server" Visible="true">
                    <asp:GridView ID="gvLocalitati" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        BorderStyle="None" CellPadding="3" GridLines="Vertical" ShowFooter="True" CssClass="tabela"
                        EmptyDataText="Nu sunt adaugate inregistrari" AllowPaging="True" DataKeyNames="localitateId"
                        DataSourceID="SqlLocalitati" OnRowDataBound="gvLocalitati_RowDataBound" OnSelectedIndexChanged="gvLocalitati_SelectedIndexChanged">
                        <Columns>
                            <asp:BoundField DataField="localitateDenumire" HeaderText="Denumire localitate" SortExpression="localitateDenumire" />
                        </Columns>
                        <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                        <HeaderStyle Font-Bold="True" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlLocalitati" runat="server" 
                        SelectCommand="SELECT * FROM [localitatiComuna] WHERE ([unitateId] = @unitateId)">
                        <SelectParameters>
                            <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </asp:Panel>
                <asp:Panel ID="pnButoane" CssClass="butoane" runat="server">
                    <asp:Button CssClass="buton" ID="btModifica" Visible="false" runat="server" Text="modifică localitate"
                        OnClick="btModifica_Click" />
                    <asp:Button CssClass="buton" ID="btSterge" Visible="false" runat="server" Text="șterge localitate"
                        OnClientClick="return confirm (&quot;Sigur stergeti ?&quot;)" OnClick="btSterge_Click" />
                    <asp:Button CssClass="buton" ID="btAdauga" runat="server" Text="adaugă localitate"
                        OnClick="btAdauga_Click" />
                </asp:Panel>
                <!-- adaugam unitate -->
                <asp:Panel ID="pnAdaugaLocalitate" CssClass="panel_general" runat="server" Visible="false">
                    <asp:Panel ID="Panel1" CssClass="adauga" runat="server">
                        <asp:Panel ID="pnEroare" runat="server">
                            <h2>
                                ADAUGĂ LOCALITĂŢI</h2>
                            <asp:Label ID="lblEroare" runat="server" CssClass="validator" Text="Eroare" Visible="false" />
                            <asp:ValidationSummary ID="valSumUnitati" runat="server" DisplayMode="SingleParagraph"
                                Visible="true" ValidationGroup="GrupValidareSabloaneCapitole" CssClass="validator"
                                ForeColor="" />
                        </asp:Panel>
                        <asp:Panel ID="Panel3" runat="server">
                            <p>
                                <asp:Label ID="lblDenumire" runat="server" Text="Denumire"></asp:Label>
                                <asp:TextBox ID="tbDenumire" runat="server"></asp:TextBox>
                            </p>
                            <p>
                                <asp:Label ID="lblCodSiRuta" runat="server" Text="Cod şiruta"></asp:Label>
                                <asp:TextBox ID="tbCodSiRuta" runat="server"></asp:TextBox>
                            </p>
                        </asp:Panel>
                        <asp:Panel ID="PanelValidatoareAscuns" CssClass="ascunsa" runat="server">
                            <!-- //////////// VALIDATOARE  \\\\\\\\\\\\\\\\\\ -->
                            <!-- //////////// VALIDATOARE  \\\\\\\\\\\\\\\\\\ -->
                        </asp:Panel>
                        <asp:Panel ID="pnButoaneAdaugare" runat="server" CssClass="butoane">
                            <asp:Button ID="btSalveazaLocalitate" runat="server" CssClass="buton" Text="adauga o localitate"
                                ValidationGroup="GrupValidareSabloaneCapitole" OnClick="btSalveazaLocalitate_Click" />
                            <asp:Button ID="btAnuleazaLocalitate" runat="server" CssClass="buton" Text="listă localităţi"
                                OnClick="btAnuleazaLocalitate_Click" />
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
