﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Capitol2c.aspx.cs" Inherits="Capitol2c" UICulture="ro-Ro" Culture="ro-Ro"
    EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Capitole / Capitolul II: c) Identificarea pădurilor proprietate privată în raport cu grupa funcțională și vârsta" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="upPaduri" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnAfiseazaMesaje" runat="server">
                <asp:ValidationSummary ID="valSumPaduri" runat="server" DisplayMode="SingleParagraph"
                    Visible="true" ValidationGroup="GrupValidarePaduri" CssClass="validator" ForeColor="" />
            </asp:Panel>
            <!-- lista Paduri -->
            <asp:Panel ID="pnListaPaduri" runat="server" CssClass="panel_general" Visible="true">
                <asp:Panel ID="pnCautare" runat="server" CssClass="cauta">
                    <asp:Label ID="lblCauta" runat="server" Text="Caută după:" />
                    <asp:Label ID="lblCProprietar" runat="server" Text="Proprietar" />
                    <asp:TextBox ID="tbCProprietar" runat="server" autocomplete="off" AutoPostBack="True"
                        Width="100px" />
                    <asp:Label ID="lblCUP" runat="server" Text="U.P." />
                    <asp:TextBox ID="tbCUP" runat="server" autocomplete="off" AutoPostBack="True" Width="170px" />
                    <asp:Label ID="lblCUA" runat="server" Text="U.A." />
                    <asp:TextBox ID="tbCUA" runat="server" autocomplete="off" AutoPostBack="True" Width="170px" />
                </asp:Panel>
                <asp:Panel ID="pnPaduri" runat="server">
                    <asp:GridView ID="gvPaduri" AllowPaging="True" DataKeyNames="paduriId" AllowSorting="True"
                        CssClass="tabela" runat="server" AutoGenerateColumns="False" OnDataBound="gvPaduri_DataBound"
                        DataSourceID="SqlPaduri" OnRowDataBound="gvPaduri_RowDataBound" OnSelectedIndexChanged="gvPaduri_SelectedIndexChanged">
                        <Columns>
                            <asp:BoundField DataField="paduriId" HeaderText="Nr." SortExpression="paduriId" Visible="false"
                                InsertVisible="False" ReadOnly="True" />
                            <asp:BoundField DataField="paduriProprietar" HeaderText="Proprietar" SortExpression="paduriProprietar" />
                            <asp:BoundField DataField="paduriCodRand" HeaderText="Cod rând" SortExpression="paduriCodRand" />
                            <asp:BoundField DataField="paduriUnitateDeProductie" HeaderText="Unitatea de producție (U.P.)"
                                SortExpression="paduriUnitateDeProductie" />
                            <asp:BoundField DataField="paduriUnitateaAmenajistica" HeaderText="Unitatea amenajistica (u.a.)"
                                SortExpression="paduriUnitateaAmenajistica" />
                            <asp:BoundField DataField="paduriSuprafataHa" HeaderText="Suprafața totală ha" SortExpression="paduriSuprafataHa" />
                            <asp:BoundField DataField="paduriSuprafataAri" HeaderText="ari" SortExpression="paduriSuprafataAri" />
                            <asp:BoundField DataField="paduriSuprafataGrupa2Ha" HeaderText="Suprafața Grupa II  - ha"
                                SortExpression="paduriSuprafataGrupa2Ha" />
                            <asp:BoundField DataField="paduriSuprafataGrupa2Ari" HeaderText="ari" SortExpression="paduriSuprafataGrupa2Ari" />
                            <asp:BoundField DataField="an" HeaderText="an" SortExpression="an" />
                        </Columns>
                        <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlPaduri" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString %>"
                        
                        SelectCommand="SELECT [paduriId], [paduriProprietar], [paduriCodRand], [paduriUnitateDeProductie], [paduriUnitateaAmenajistica], [paduriSuprafataHa], [an], [paduriSuprafataAri], [unitateId], [paduriSuprafataGrupa2Ha], [paduriSuprafataGrupa2Ari] FROM [paduri] WHERE (([an] = @an) AND ([gospodarieId] = @gospodarieId) AND ([unitateId] = @unitateId) AND ([paduriProprietar] LIKE '%' + @paduriProprietar + '%') AND ([paduriUnitateaAmenajistica] LIKE '%' + @paduriUnitateaAmenajistica + '%') AND ([paduriUnitateDeProductie] LIKE '%' +  @paduriUnitateDeProductie + '%'))">
                        <SelectParameters>
                            <asp:SessionParameter DefaultValue="2011" Name="an" SessionField="SESan"
                                Type="Int32" />
                            <asp:SessionParameter DefaultValue="0" Name="gospodarieId" SessionField="SESgospodarieId"
                                Type="Int32" />
                            <asp:SessionParameter DefaultValue="0" Name="unitateId" 
                                SessionField="SESunitateId" Type="Int32" />
                            <asp:ControlParameter ControlID="tbCProprietar" DefaultValue="%" Name="paduriProprietar"
                                PropertyName="Text" Type="String" />
                            <asp:ControlParameter ControlID="tbCUA" DefaultValue="%" 
                                Name="paduriUnitateaAmenajistica" PropertyName="Text"
                                Type="String" />
                            <asp:ControlParameter ControlID="tbCUP" DefaultValue="%" Name="paduriUnitateDeProductie"
                                PropertyName="Text" Type="String" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </asp:Panel>
                <asp:Panel ID="pnListaButoanePrincipale" CssClass="butoane" runat="server">
                    <asp:Button CssClass="buton" ID="btModificaPaduri" runat="server" Text="modificare pădure"
                        OnClick="ArataAdaugaPaduri" Visible="false" />
                    <asp:Button CssClass="buton" ID="btStergePaduri" runat="server" Text="şterge pădure"
                        OnClick="StergePaduri" ValidationGroup="GrupValidarePaduri" OnClientClick="return confirm(&quot;Sunteți sigur că doriți să ștergeți pădurea?&quot;)"
                        Visible="false" />
                    <asp:Button CssClass="buton" ID="btListaAdaugaPaduri" runat="server" Text="adaugă o pădure"
                        OnClick="ArataAdaugaPaduri" />
                    <asp:Button ID="btListaGospodarii" runat="server" CssClass="buton" Text="listă gospodării"
                        Visible="true" PostBackUrl="~/Gospodarii.aspx" />
                    <asp:Button ID="btTiparesteCapitol" runat="server" CssClass="buton" Text="tipărire capitol"
                        Visible="true"  onclick="btTiparesteCapitol_Click" />
                </asp:Panel>
            </asp:Panel>
            <!-- adaugam Paduri -->
            <asp:Panel ID="pnAdaugaPaduri" CssClass="panel_general" runat="server" Visible="false">
                <asp:Panel ID="pnAd" CssClass="adauga" runat="server">
                    <asp:Panel ID="pnAdTitlu" runat="server">
                        <h2>
                            ADAUGĂ / MODIFICĂ O PĂDURE</h2>
                    </asp:Panel>
                    <asp:Panel ID="pnAd1" runat="server">
                        <p>
                            <asp:Label ID="lblCodRand" runat="server" Visible="false" />
                            <asp:Label ID="lblProprietar" runat="server" Text="Proprietar" />
                            <asp:TextBox ID="tbProprietar" runat="server" Width="200px"></asp:TextBox>
                            <asp:Label ID="lblUP" runat="server" Text="Unitatea de producție" />
                            <asp:TextBox ID="tbUP" runat="server" Width="200px"></asp:TextBox>
                            <asp:Label ID="lblUA" runat="server" Text="Unitatea amenajistică" />
                            <asp:TextBox ID="tbUA" runat="server" Width="200px"></asp:TextBox>
                        </p>
                        <p>
                            
                            <asp:Label ID="lblSuprafata" runat="server" Text="Suprafaţa totală: " />
                            <asp:Label ID="lblHa" runat="server" Text=" ha" />
                            <asp:TextBox ID="tbHa" runat="server" Width="50px"></asp:TextBox>
                            <asp:Label ID="lblAri" runat="server" Text=" ari" />
                            <asp:TextBox ID="tbAri" runat="server" Width="50px"></asp:TextBox>
                        </p>
                        <p>
                            <asp:Label ID="lblSuprafataGr2" runat="server" Text="Din care în grupa II funcțională cu vârsta >20 de ani: " />
                        </p>
                        <p>
                            <asp:Label ID="lblSuprafataGr2Ha" runat="server" Text=" ha" />
                            <asp:TextBox ID="tbSuprafataGr2Ha" runat="server" Width="50px"></asp:TextBox>
                            <asp:Label ID="lblSuprafataGr2Ari" runat="server" Text=" ari" />
                            <asp:TextBox ID="tbSuprafataGr2Ari" runat="server" Width="50px"></asp:TextBox>
                        </p>
                    </asp:Panel>
                    <asp:Panel ID="pnButoanePrincipale" runat="server" CssClass="butoane">
                        <asp:Button CssClass="buton" ID="btAdaugaPaduri" runat="server" Text="adaugă o pădure"
                            OnClick="AdaugaModificaPaduri" ValidationGroup="GrupValidarePaduri" Visible="false" />
                        <asp:Button ID="btAdaugaModificaPaduri" runat="server" CssClass="buton" OnClick="AdaugaModificaPaduri"
                            Text="modificare pădure" ValidationGroup="GrupValidarePaduri" Visible="false" />
                        <asp:Button ID="btModificaArataListaPaduri" runat="server" CssClass="buton" OnClick="ModificaArataListaPaduri"
                            Text="lista pădurilor" Visible="false" />
                        <asp:Button ID="btAdaugaArataListaPaduri" runat="server" CssClass="buton" OnClick="AdaugaArataListaPaduri"
                            Text="lista pădurilor" Visible="false" />
                    </asp:Panel>
                    <asp:Panel ID="Panel5" CssClass="ascunsa" runat="server">
                        <!-- //////////// VALIDATOARE  \\\\\\\\\\\\\\\\\\ -->
                        <asp:RequiredFieldValidator ID="valProprietar" ControlToValidate="tbProprietar" ValidationGroup="GrupValidarePaduri"
                            runat="server" ErrorMessage=" Numele proprietarului este obligatoriu! "></asp:RequiredFieldValidator>
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
