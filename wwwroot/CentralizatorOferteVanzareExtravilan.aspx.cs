﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CentralizatorOferteVanzareExtravilan : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataTable reportDataTable = GetDataTAbleForReport();
            DataTable unitatiDataTable = GetUnitateDenumire();
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource ds = new ReportDataSource("rapoarteDosareVanzareDataSet_dtCentralizatorOferteVanzareExtravilan", reportDataTable);
            ReportDataSource ds2 = new ReportDataSource("dsRapoarte_dtUnitati", unitatiDataTable);
            ReportViewer1.LocalReport.DataSources.Add(ds);
            ReportViewer1.LocalReport.DataSources.Add(ds2);
            ReportViewer1.LocalReport.Refresh();
            ReportViewer1.Visible = true;
        }
    }

    private DataTable GetDataTAbleForReport()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        string interogare = @"select  membruNume,membruCnp,nrDosar,dataInregistrarii, sabloaneCapitole.denumire4 as categorieParcela,serieBuletin,nrBuletin,buletin,localitate,nr,strada,
                                    CONVERT(decimal(16,2),suprafataOferitaHa) as suprafataOferitaHa ,CONVERT(decimal(16,2),suprafataOferitaAri) as suprafataOferitaAri ,
                                    valoareParcela,parcelaTarla,parcelaCF,parcelaDenumire
                            from OferteVanzare
join gospodarii
on OferteVanzare.idGospodarie = gospodarii.gospodarieId
join OferteVanzareDetalii on OferteVanzareDetalii.idOferta = OferteVanzare.idOferta
join parcele on parcele.parcelaId = OferteVanzare.idParcela
join sabloaneCapitole on sabloaneCapitole.codRand = parcele.parcelaCodRand and sabloaneCapitole.an = " + Session["SESAn"]+@" and sabloaneCapitole.capitol = '2a'
                              WHERE OferteVanzare.an = "+Session["SESAn"]+" and OferteVanzare.unitateId = "+Session["SESUnitateId"]+" order by nrDosar";
        SqlDataAdapter adapter = new SqlDataAdapter(interogare, connection);
        DataTable table = new DataTable();
        adapter.Fill(table);
        ManipuleazaBD.InchideConexiune(connection);
        return table;
    }
    private DataTable GetUnitateDenumire()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        string interogare = @"SELECT    unitati.unitateId, unitati.unitateDenumire, unitati.unitateCodSiruta, unitati.judetId, unitati.localitateId, unitati.unitateStrada, unitati.unitateNr, unitati.unitateAp, 
                         unitati.unitateCodPostal, unitati.localitateComponentaId, unitati.unitateCodFiscal, unitati.unitateActiva, unitati.unitatePrincipala, judete.judetDenumire, 
                         localitati.localitateDenumire
FROM            unitati INNER JOIN
                         judete ON unitati.judetId = judete.judetId INNER JOIN
                         localitati ON unitati.localitateId = localitati.localitateId
WHERE        (unitati.unitateId = " + Session["SESunitateId"].ToString() + ")";
        SqlDataAdapter adapter = new SqlDataAdapter(interogare, connection);
        DataTable table = new DataTable();
        adapter.Fill(table);
        ManipuleazaBD.InchideConexiune(connection);
        return table;
    }
}