﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AdaugaSabloane.aspx.cs" Inherits="AdaugaSabloane"  Culture="ro-RO" UICulture="ro-RO"  %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
            DataKeyNames="paginaId" DataSourceID="SqlAdaugaSabloane">
            <Columns>
                <asp:BoundField DataField="paginaId" HeaderText="paginaId" 
                    InsertVisible="False" ReadOnly="True" SortExpression="paginaId" />
                <asp:BoundField DataField="paginaDenumire" HeaderText="paginaDenumire" 
                    SortExpression="paginaDenumire" />
                <asp:BoundField DataField="paginaActiva" HeaderText="paginaActiva" 
                    SortExpression="paginaActiva" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlAdaugaSabloane" runat="server"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
          
            SelectCommand="@SELECT [drepturi].[dreptCitire], [drepturi].[dreptModificare], [drepturi].[dreptCreare], [drepturi].[dreptTiparire] FROM [drepturi]LEFT JOIN [pagini] ON [pagini].[paginaId] = [drepturi].[paginaId]
            ">
            <SelectParameters>
                <asp:Parameter Name="SELECT" />
            </SelectParameters>
        </asp:SqlDataSource>
    </div>
    </form>
    
</body>
</html>
