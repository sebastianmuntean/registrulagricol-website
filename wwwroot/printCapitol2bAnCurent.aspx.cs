﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class printCapitol2bAnCurent : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataTable reportDataTable = GetDataTAbleForReport();
            DataTable unitatiDataTable = GetUnitateDenumire();
            DataTable gospodariiDataTable = GetGospodarie();
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource ds = new ReportDataSource("dsRapoarte_Cap2BAnCurentDataTable", reportDataTable);
            ReportDataSource ds2 = new ReportDataSource("dsRapoarte_dtUnitati", unitatiDataTable);
            ReportDataSource ds3 = new ReportDataSource("dsRapoarte_dtGospodarii", gospodariiDataTable);
            ReportViewer1.LocalReport.DataSources.Add(ds);
            ReportViewer1.LocalReport.DataSources.Add(ds2);
            ReportViewer1.LocalReport.DataSources.Add(ds3);
            ReportViewer1.LocalReport.Refresh();
            ReportViewer1.Visible = true;
        }
    }
    protected void Page_SaveStateComplete(object sender, EventArgs e)
    {
        Warning[] warnings;
        string[] streamids;
        string mimeType;
        string encoding;
        string fileNameExtension;
        byte[] bytes = ReportViewer1.LocalReport.Render("PDF", null, out mimeType, out encoding, out fileNameExtension, out streamids, out warnings);
        Response.Buffer = true;
        Response.Clear();
        Response.ContentType = mimeType;
        Response.AddHeader("content-disposition", "inline; filename=Nume" + fileNameExtension);
        Response.BinaryWrite(bytes);
        Response.Flush();
        Response.End();
        Response.Close();
    }
    private DataTable GetGospodarie()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        string interogare = @"SELECT   an,  gospodarieId, unitateId, volum, nrPozitie, codSiruta, tip, nrInt, codExploatatie, codUnic, judet, localitate, persJuridica, jUnitate, jSubunitate, 
                         jCodFiscal, jNumeReprez, strainas, sStrada, sNr, sBl, sSc, sEtj, sAp, sJudet, sLocalitate,
						 case strainas when 0 then strada  else sStrada  end as strada,
						 case strainas when 0 then nr  else sNr  end as nr,
						 case strainas when 0 then bl  else sBl  end as bl,
						 case strainas when 0 then sc  else sSc  end as sc,
						 case strainas when 0 then et  else sEtj  end as et,
						 case strainas when 0 then ap  else sAp  end as ap,
                             (SELECT        TOP (1) nume
                               FROM            membri
                               WHERE        (gospodarieId = gospodarii.gospodarieId)
                               ORDER BY codRudenie) AS Membru,
                             (SELECT        TOP (1) cnp
                               FROM           membri AS membri_1
                               WHERE        (gospodarieId = gospodarii.gospodarieId)
                               ORDER BY codRudenie) AS cnpMembru
FROM           gospodarii
where an = " + Session["SESAn"] + " and gospodarieId = " + Session["SESGospodarieid"] + "";
        SqlDataAdapter adapter = new SqlDataAdapter(interogare, connection);
        DataTable table = new DataTable();
        adapter.Fill(table);
        ManipuleazaBD.InchideConexiune(connection);
        return table;
    }

    private DataTable GetUnitateDenumire()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        string interogare = @"SELECT        unitati.unitateId, unitati.unitateDenumire, unitati.unitateCodSiruta, unitati.judetId, unitati.localitateId, unitati.unitateStrada, unitati.unitateNr, unitati.unitateAp, 
                         unitati.unitateCodPostal, unitati.localitateComponentaId, unitati.unitateCodFiscal, unitati.unitateActiva, unitati.unitatePrincipala, judete.judetDenumire, 
                         localitati.localitateDenumire
FROM            unitati INNER JOIN
                         judete ON unitati.judetId = judete.judetId INNER JOIN
                         localitati ON unitati.localitateId = localitati.localitateId
WHERE        (unitati.unitateId = " + Session["SESunitateId"].ToString() + ")";
        SqlDataAdapter adapter = new SqlDataAdapter(interogare, connection);
        DataTable table = new DataTable();
        adapter.Fill(table);
        ManipuleazaBD.InchideConexiune(connection);
        return table;
    }

    private DataTable GetDataTAbleForReport()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        string interogare = @"SELECT parcele.parcelaDenumire,
                                     parcele.parcelaCodRand,
                                     parcele.parcelaNrCadastral,
                                     parcele.parcelaSuprafataIntravilanHa AS parcelaSuprafataIntravilanHa, 
                                     parcele.parcelaSuprafataIntravilanAri AS parcelaSuprafataIntravilanAri, 
                                     parcele.parcelaSuprafataExtravilanHa AS parcelaSuprafataExtravilanHa, 
                                     parcele.parcelaSuprafataExtravilanAri AS parcelaSuprafataExtravilanAri ,
                                     parcele.parcelaNrTopo, 
                                     parcele.parcelaCF,
                                     CASE parcelaCategorie
                                     WHEN 1 THEN 'Teren arabil (inclusiv sere si solarii)'
                                     WHEN 2 THEN 'Pasuni naturale' 
                                     WHEN 3 THEN 'Fânete naturale'
                                     WHEN 4 THEN 'Vii, pepiniere viticole si hameisti'
                                     WHEN 7 THEN 'Livezi de pomi, pepiniere, arbusti fructiferi'
                                     WHEN 9 THEN 'Gradini familiale'
                                     WHEN 11 THEN 'Paduri si alte terenuri cu vegetatie forestiera'
                                     WHEN 13 THEN 'Drumuri si cai ferate'
                                     WHEN 14 THEN 'Constructii'
                                     WHEN 15 THEN 'Terenuri degradate si neproductive'
                                     WHEN 16 THEN 'Ape si balti'
	                                 end AS parcelaCategorie, 
                                     parcele.parcelaNrBloc, 
                                     parcele.parcelaAdresa + ' '+parcele.parcelaMentiuni as adresa
                            FROM parcele 
                            INNER JOIN unitati
                                ON parcele.unitateaId = unitati.unitateId
                            where parcele.unitateaId = " + Session["SESunitateId"] + " and parcele.gospodarieId = " + Session["SesGospodarieid"] + " and parcele.an = " + Session["SESAn"] + " ";
        SqlDataAdapter adapter = new SqlDataAdapter(interogare, connection);
        DataTable table = new DataTable();
        adapter.Fill(table);

        if(table.Rows.Count==0)
        {
            for ( int i = 0; i <= 10; i++)
            {
                table.Rows.Add("", i, "", 0, 0, 0, 0, "", "", "", "", "");
            }
        }

        ManipuleazaBD.InchideConexiune(connection);
        return table;
    }
}