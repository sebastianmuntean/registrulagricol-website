﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="raportCapitole.aspx.cs" Inherits="raportCapitole" Culture="ro-RO" uiCulture="ro-RO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" Runat="Server">
     <asp:Label ID="url" runat="server" Text="Rapoarte gospodarii cu valori in capitole" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Panel ID="pnGeneral" runat="server" CssClass="panel_general">
        <asp:Panel ID="pnFiltrareUnitati" runat="server" CssClass="cauta">
            <asp:Label ID="Label1" runat="server" Text="Capitol" />
            <asp:DropDownList runat ="server" Id="capitoleDropDown" width ="150px"></asp:DropDownList>
            </asp:Panel>
        </asp:Panel>
      <asp:Panel ID="pnButoane" runat="server" CssClass="butoane">
            <asp:Button ID="btTiparire" runat="server" Text="tipărește" CssClass="buton" OnClick="btTiparire_Click" />
        </asp:Panel>
</asp:Content>

