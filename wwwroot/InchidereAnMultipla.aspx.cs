﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

public partial class InchidereAnMultipla : System.Web.UI.Page
{
    string logFileName = string.Empty;
    InchidereAnRA.App_Code.Utils.FileExtender file = new InchidereAnRA.App_Code.Utils.FileExtender();

    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        inchideButton.Enabled = true;

        if (!IsPostBack)
        {
            try
            {
                ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "inchidere an", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
            }
            catch { }
        }
        HttpCookie vCookie = Request.Cookies["COOKTNTExpira"];

        if (vCookie == null)
        {
            Response.Redirect("Login.aspx");
        }
        else
        {
            if (Convert.ToDateTime(vCookie["COOKexpira"]) > DateTime.Now || (vCookie["COOKexpira"] == null))
            {
                vCookie["COOKexpira"] = DateTime.Now.AddMinutes(10000).ToString();
                vCookie.Expires = DateTime.Now.AddMinutes(2005);
                Response.Cookies.Add(vCookie);
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }

        tbAn.Text = ManipuleazaBD.fRezultaUnString("SELECT unitateAnDeschis FROM unitati WHERE unitateId = '" + Session["SESunitateId"].ToString() + "'", "unitateAnDeschis", Convert.ToInt16(DateTime.Now.Year));

        InchidereAnRA.App_Code.Utils.FileExtender.GlobalPath = MapPath("~/");
        // GetUnitate();


    }

    private void GetUnitate(int idunitate)
    {
        InchidereAnRA.App_Code.Models.Unitati unitate = new InchidereAnRA.App_Code.Models.Unitati();
        List<InchidereAnRA.App_Code.Models.Unitati> unitati = InchidereAnRA.App_Code.Services.UnitatiServices.GetUnitById(idunitate);//Convert.ToInt32(Session["SESunitateId"])

        Session["unitateDeInchis"] = unitati[0];
    }

    private void InchideAnul()
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(DateTime.Now.Year));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        int vAnNou = Convert.ToInt32(tbAn.Text) + 1;
        int vAnInchis = 0;
        int vGospodariiInchise = 0;

        try
        {
            {
                //GetUnitate(Convert.ToInt32(Session["SESunitateId"]));
                ClassLog.fLog(1, 52, DateTime.Now, "inchidere", "incepe", " inainte InchidereAnRA.App_Code.Models.Unitati  ", 0, 0);
                InchidereAnRA.App_Code.Models.Unitati unitate = (InchidereAnRA.App_Code.Models.Unitati)Session["unitateDeInchis"];
                ClassLog.fLog(1, 52, DateTime.Now, "inchidere", "incepe", " dupa InchidereAnRA.App_Code.Models.Unitati  ", 0, 0);
                // unitate.Id = Convert.ToInt32(Session["SESunitateId"]);
                //  unitate.AnDeschis = 2015;
                logFileName = "inchidereAn_" + unitate.Id + "_" + DateTime.Now.Date.ToString("ddMMyyyy") + ".txt";

                InchidereAnRA.App_Code.Controllers.ControllerInchidereDeAn controllerInchidereAn = new InchidereAnRA.App_Code.Controllers.ControllerInchidereDeAn(unitate);

                CreateLogFile();

                file.Append(InchidereAnRA.App_Code.Utils.FileExtender.GlobalPath + logFileName, "Am inceput procesul la: " + DateTime.Now.TimeOfDay);

                if (controllerInchidereAn.StergeDateleExistentePeAnulDeDeschis())
                {
                    file.Append(InchidereAnRA.App_Code.Utils.FileExtender.GlobalPath + logFileName, "S-au sters datele din sesiunea de inchidere precedenta, daca aceasta a existat!");

                    controllerInchidereAn.InchideUnitateaSelectata();
                    ClassLog.fLog(1, 52, DateTime.Now, "inchidere", "a terminat cu succes", " dupa controllerInchidereAn.InchideUnitateaSelectata();  ", 0, 0);
                }
                else
                {
                    file.Append(InchidereAnRA.App_Code.Utils.FileExtender.GlobalPath + logFileName, "Nu s-au sters datele din sesiunea de inchidere precedenta, daca aceasta a existat!");
                }

                file.Append(InchidereAnRA.App_Code.Utils.FileExtender.GlobalPath + logFileName, "Am terminat procesul la: " + DateTime.Now.TimeOfDay);
            }
            //    else
            //{
            //    lblEroare.Visible = true;
            //    lblEroare.Text = "Anul " + tbAn.Text + " este deja închis !";
            //}
        }
        catch (Exception x)
        {
            lblEroare.Visible = true;
            lblEroare.Text = "Anul " + tbAn.Text + " nu a putut fi inchis ! Vă rugăm să mai încercaţi o dată. Cod eroare: 001.";
            ClassLog.fLog(1, 52, DateTime.Now, "inchidere", "ESUATA!!!!", x.Message, 0, 0);
        }
        finally
        {
            ManipuleazaBD.InchideConexiune(vCon);
        }
        //}
        // daca anul nu era inchis copiem datele in anul urmator
        if (vAnInchis == 0)
        {
        }

    }
    //protected void btCorecteazaCentralizatoare_Click(object sender, EventArgs e)
    //{
    //    RefacereCentralizatoare.ReparaToateCapitoleleCentralizate();
    //}


    private void CreateLogFile()
    {
        if (file.Exists(InchidereAnRA.App_Code.Utils.FileExtender.GlobalPath + logFileName.Replace(".txt", "")))
        {
            file.Delete(InchidereAnRA.App_Code.Utils.FileExtender.GlobalPath + logFileName.Replace(".txt", ""));
        }

        if (file.Exists(InchidereAnRA.App_Code.Utils.FileExtender.GlobalPath + logFileName))
        {
            file.Delete(InchidereAnRA.App_Code.Utils.FileExtender.GlobalPath + logFileName);
        }

        file.Create(InchidereAnRA.App_Code.Utils.FileExtender.GlobalPath + logFileName);
    }

    protected void tbAn_PreRender(object sender, EventArgs e)
    {
        //tbAn.Text = ManipuleazaBD.fRezultaUnString("SELECT unitateAnDeschis FROM unitati WHERE unitateId = '" + Session["SESunitateId"].ToString() + "'", "unitateAnDeschis", Convert.ToInt16(DateTime.Now.Year));

        //if (Convert.ToDateTime("01.12." + ManipuleazaBD.fRezultaUnString("SELECT unitateAnDeschis FROM unitati WHERE unitateId = '" + Session["SESunitateId"].ToString() + "'", "unitateAnDeschis", Convert.ToInt16(DateTime.Now.Year))) >= DateTime.Now)
        //{
        //    tbAn.Text = "----";
        //    lblEroare.Visible = true;
        //    lblEroare.Text = "Este prea devreme pentru a se face inchiderea anului " + ManipuleazaBD.fRezultaUnString("SELECT unitateAnDeschis FROM unitati WHERE unitateId = '" + Session["SESunitateId"].ToString() + "'", "unitateAnDeschis", Convert.ToInt16(DateTime.Now.Year)).ToString() + " (anul deschis). Aceasta se poate face doar după 01.12." + ManipuleazaBD.fRezultaUnString("SELECT unitateAnDeschis FROM unitati WHERE unitateId = '" + Session["SESunitateId"].ToString() + "'", "unitateAnDeschis", Convert.ToInt16(DateTime.Now.Year)).ToString();
        //    //   btAdauga.Visible = false;
        //    inchideButton.Visible = false;
        //}
        //else
        //{
        //    lblEroare.Visible = false;
        //    inchideButton.Visible = true;
        //}

        //tbAn.Enabled = false;
    }
    protected void InchideButtonClick(object sender, EventArgs e)
    {
        inchideButton.Visible = false;
        try
        {
            List<int> unitati = new List<int>() { //143,
                                                  // 183, 
                                                  // 200, 
                                                  // 205, 
                206, 207, 208, 210, 211, 212, 213, 215, 216, 217, 221, 223 };
        
            foreach (int unitate in unitati)
            {

                try
                {
                    GetUnitate(unitate);
                    InchideAnul();
                }
                catch (Exception x) { ClassLog.fLog(1, 52, DateTime.Now, "inchidere " + unitati.ToString(), "ESUATA!!!!", x.Message, 0, 0); }

            }
           // InchideAnul();
            //lblEroare.Visible = true;
            //lblEroare.Text = "Anul " + tbAn.Text + " a fost închis !";

        }
        catch (Exception ex)
        {
            lblEroare.Visible = true;
            lblEroare.Text = ex.Message;
        }

    }

    private void InchidereSemestru(int idUnitate)
    {
        DataTable gospodariiTable;
        SqlConnection connection;
        FillGospodariiTable(out gospodariiTable, out connection, idUnitate);

        StringBuilder animal = new StringBuilder();
        int i = 0;
        foreach (DataRow gospodariiRow in gospodariiTable.Rows)
        {
            i++;

            SqlConnection.ClearPool(connection);

            int sumaBovine = 0;
            int sumaVaci = 0;
            int sumaOvine = 0;
            int sumaCaprine = 0;
            int sumaPorcine = 0;
            int sumaCabaline = 0;
            int sumaPasari = 0;
            int sumaAlteAnimale = 0;
            int sumaAlbine = 0;
            int sumaPesti = 0;

            int idGospodarie = Convert.ToInt32(gospodariiRow["gospodarieId"]);

            DataTable evolutiaAnimalelorPeSem1 = new DataTable();
            DataTable evolutiaAnimalelorPeSem2 = new DataTable();

            AnimaleServices.ConnectionYear = Convert.ToInt16(Session["SESan"]);

            evolutiaAnimalelorPeSem1 = AnimaleServices.GetEvolutiaAnimalelorInCursulAnuluiByGospodarieAndByAnAndByUnitate(idGospodarie, Convert.ToInt16(Session["SESan"]), Convert.ToInt16(idUnitate));
            evolutiaAnimalelorPeSem2 = AnimaleServices.GetEvolutiaAnimalelorInCursulAnuluiByGospodarieAndByAnAndByUnitatePeSem2(idGospodarie, Convert.ToInt16(Session["SESan"]), Convert.ToInt16(idUnitate));

            DataTable situatiaAnimalelorLaInceputulSemestruluiDataTable = new DataTable();
            situatiaAnimalelorLaInceputulSemestruluiDataTable = AnimaleServices.GetSituatiaAnimalelorLaInceputulSemestruluiByGospodarieAndByAnAndByUnitate(idGospodarie, Convert.ToInt16(Session["SESan"]), Convert.ToInt16(idUnitate));

            if (evolutiaAnimalelorPeSem1.Rows.Count == 0 && situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows.Count > 0)
            {
                CalculCapitole.PopuleazaTabelaCapitole(Convert.ToInt32(Session["SESan"]), idGospodarie, Convert.ToInt64(idUnitate), "8");
                evolutiaAnimalelorPeSem1 = AnimaleServices.GetEvolutiaAnimalelorInCursulAnuluiByGospodarieAndByAnAndByUnitate(idGospodarie, Convert.ToInt16(Session["SESan"]), Convert.ToInt16(idUnitate));
                evolutiaAnimalelorPeSem2 = AnimaleServices.GetEvolutiaAnimalelorInCursulAnuluiByGospodarieAndByAnAndByUnitatePeSem2(idGospodarie, Convert.ToInt16(Session["SESan"]), Convert.ToInt16(idUnitate));
            }

            if (situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows.Count > 0)
            {
                if (Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[0][0]) != 0 && Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[0][0]) != Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[0][0]))
                {
                    evolutiaAnimalelorPeSem1.Rows[0][0] = situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[0][0];
                    evolutiaAnimalelorPeSem1.Rows[8][0] = Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[0][0]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[1][0]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[2][0]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[3][0]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[4][0]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[5][0]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[6][0]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[7][0]);

                    animal.Append(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[0][0] +
                                  "' WHERE codCapitol = '8' AND codRand = 1 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");
                    animal.Append(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[8][0] +
                                  "' WHERE codCapitol = '8' AND codRand = 9 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");
                }

                if (Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[22][0]) != 0 && Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[9][0]) != Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[22][0]))
                {
                    evolutiaAnimalelorPeSem1.Rows[9][0] = situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[22][0];
                    evolutiaAnimalelorPeSem1.Rows[16][0] = Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[9][0]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[10][0]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[11][0]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[12][0]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[13][0]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[14][0]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[15][0]);

                    animal.Append(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[9][0] +
                                  "' WHERE codCapitol = '8' AND codRand = 10 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");
                    animal.Append(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[16][0] +
                                  "' WHERE codCapitol = '8' AND codRand = 17 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");
                }

                if (Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[29][0]) != 0 && Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[17][0]) != Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[29][0]))
                {
                    evolutiaAnimalelorPeSem1.Rows[17][0] = situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[29][0];
                    evolutiaAnimalelorPeSem1.Rows[25][0] = Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[17][0]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[18][0]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[19][0]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[20][0]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[21][0]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[22][0]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[23][0]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[24][0]);

                    animal.Append(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[17][0] +
                                  "' WHERE codCapitol = '8' AND codRand = 18 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");
                    animal.Append(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[25][0] +
                                  "' WHERE codCapitol = '8' AND codRand = 26 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");
                }

                if (Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[35][0]) != 0 && Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[26][0]) != Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[35][0]))
                {
                    evolutiaAnimalelorPeSem1.Rows[26][0] = situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[35][0];
                    evolutiaAnimalelorPeSem1.Rows[34][0] = Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[26][0]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[27][0]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[28][0]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[29][0]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[30][0]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[31][0]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[32][0]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[33][0]);

                    animal.Append(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[26][0] +
                                  "' WHERE codCapitol = '8' AND codRand = 27 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");
                    animal.Append(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[34][0] +
                                  "' WHERE codCapitol = '8' AND codRand = 35 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");
                }

                if (Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[40][0]) != 0 && Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[35][0]) != Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[40][0]))
                {
                    evolutiaAnimalelorPeSem1.Rows[35][0] = situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[40][0];
                    evolutiaAnimalelorPeSem1.Rows[43][0] = Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[35][0]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[36][0]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[37][0]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[38][0]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[39][0]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[40][0]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[41][0]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[42][0]);

                    animal.Append(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[35][0] +
                                  "' WHERE codCapitol = '8' AND codRand = 36 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");
                    animal.Append(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[43][0] +
                                  "' WHERE codCapitol = '8' AND codRand = 44 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");
                }

                if (Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[55][0]) != 0 && Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[44][0]) != Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[55][0]))
                {
                    evolutiaAnimalelorPeSem1.Rows[44][0] = situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[55][0];
                    evolutiaAnimalelorPeSem1.Rows[52][0] = Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[44][0]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[45][0]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[46][0]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[47][0]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[48][0]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[49][0]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[50][0]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[51][0]);

                    animal.Append(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[44][0] +
                                  "' WHERE codCapitol = '8' AND codRand = 45 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");
                    animal.Append(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[52][0] +
                                  "' WHERE codCapitol = '8' AND codRand = 53 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");
                }

                if (Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[67][0]) != 0 && Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[53][0]) != Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[67][0]))
                {
                    evolutiaAnimalelorPeSem1.Rows[53][0] = situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[67][0];
                    evolutiaAnimalelorPeSem1.Rows[61][0] = Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[53][0]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[54][0]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[55][0]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[56][0]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[57][0]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[58][0]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[59][0]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[60][0]);

                    animal.Append(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[53][0] +
                                  "' WHERE codCapitol = '8' AND codRand = 54 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");
                    animal.Append(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[61][0] +
                                  "' WHERE codCapitol = '8' AND codRand = 62 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");
                }

                if (Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[76][0]) != 0 && Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[62][0]) != Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[76][0]))
                {
                    evolutiaAnimalelorPeSem1.Rows[62][0] = situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[76][0];
                    evolutiaAnimalelorPeSem1.Rows[70][0] = Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[62][0]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[63][0]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[64][0]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[65][0]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[66][0]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[67][0]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[68][0]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[69][0]);

                    animal.Append(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[62][0] +
                                  "' WHERE codCapitol = '8' AND codRand = 63 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");
                    animal.Append(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[70][0] +
                                  "' WHERE codCapitol = '8' AND codRand = 71 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");
                }

                if (Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[77][0]) != 0 && Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[71][0]) != Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[77][0]))
                {
                    evolutiaAnimalelorPeSem1.Rows[71][0] = situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[77][0];
                    evolutiaAnimalelorPeSem1.Rows[78][0] = Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[71][0]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[72][0]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[73][0]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[74][0]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[75][0]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[76][0]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[77][0]);

                    animal.Append(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[71][0] +
                                  "' WHERE codCapitol = '8' AND codRand = 72 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");
                    animal.Append(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[78][0] +
                                  "' WHERE codCapitol = '8' AND codRand = 79 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");
                }

                if (evolutiaAnimalelorPeSem1.Rows.Count > 79 && situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows.Count > 78)
                {
                    if (Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[78][0]) != 0 && Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[79][0]) != Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[78][0]))
                    {
                        evolutiaAnimalelorPeSem1.Rows[79][0] = situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[78][0];
                        evolutiaAnimalelorPeSem1.Rows[84][0] = Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[79][0]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[80][0]) + Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[81][0]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[82][0]) - Convert.ToInt32(evolutiaAnimalelorPeSem1.Rows[83][0]);

                        animal.Append(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[79][0] +
                                      "' WHERE codCapitol = '8' AND codRand = 80 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");
                        animal.Append(@"UPDATE capitole
                                SET col1 = '" + evolutiaAnimalelorPeSem1.Rows[84][0] +
                                      "' WHERE codCapitol = '8' AND codRand = 85 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");
                    }
                }
            }
            foreach (DataRow rowAnimal in evolutiaAnimalelorPeSem1.Rows)
            {
                Tuple<short, short> bovine = new Tuple<short, short>(9, 1);
                Tuple<short, short> vaci = new Tuple<short, short>(17, 10);
                Tuple<short, short> ovine = new Tuple<short, short>(26, 18);
                Tuple<short, short> caprine = new Tuple<short, short>(35, 27);
                Tuple<short, short> porcine = new Tuple<short, short>(44, 36);
                Tuple<short, short> cabaline = new Tuple<short, short>(53, 45);
                Tuple<short, short> pasari = new Tuple<short, short>(62, 54);
                Tuple<short, short> alteAnimale = new Tuple<short, short>(71, 63);
                Tuple<short, short> albine = new Tuple<short, short>(79, 72);
                Tuple<short, short> pesti = new Tuple<short, short>(85, 80);

                if (Convert.ToInt32(evolutiaAnimalelorPeSem2.Rows[0][0]) == 0)
                {
                    UpdateRandAnimal(rowAnimal, idGospodarie, animal, bovine);
                    sumaBovine += GetSuma(rowAnimal, bovine);
                }
                if (Convert.ToInt32(evolutiaAnimalelorPeSem2.Rows[1][0]) == 0)
                {
                    UpdateRandAnimal(rowAnimal, idGospodarie, animal, vaci);
                    sumaVaci += GetSuma(rowAnimal, vaci);
                }
                if (Convert.ToInt32(evolutiaAnimalelorPeSem2.Rows[2][0]) == 0)
                {
                    UpdateRandAnimal(rowAnimal, idGospodarie, animal, ovine);
                    sumaOvine += GetSuma(rowAnimal, ovine);
                }
                if (Convert.ToInt32(evolutiaAnimalelorPeSem2.Rows[3][0]) == 0)
                {
                    UpdateRandAnimal(rowAnimal, idGospodarie, animal, caprine);
                    sumaCaprine += GetSuma(rowAnimal, caprine);
                }
                if (Convert.ToInt32(evolutiaAnimalelorPeSem2.Rows[4][0]) == 0)
                {
                    UpdateRandAnimal(rowAnimal, idGospodarie, animal, porcine);
                    sumaPorcine += GetSuma(rowAnimal, porcine);
                }
                if (Convert.ToInt32(evolutiaAnimalelorPeSem2.Rows[5][0]) == 0)
                {
                    UpdateRandAnimal(rowAnimal, idGospodarie, animal, cabaline);
                    sumaCabaline += GetSuma(rowAnimal, cabaline);
                }
                if (Convert.ToInt32(evolutiaAnimalelorPeSem2.Rows[6][0]) == 0)
                {
                    UpdateRandAnimal(rowAnimal, idGospodarie, animal, pasari);
                    sumaPasari += GetSuma(rowAnimal, pasari);
                }
                if (Convert.ToInt32(evolutiaAnimalelorPeSem2.Rows[7][0]) == 0)
                {
                    UpdateRandAnimal(rowAnimal, idGospodarie, animal, alteAnimale);
                    sumaAlteAnimale += GetSuma(rowAnimal, alteAnimale);
                }
                if (Convert.ToInt32(evolutiaAnimalelorPeSem2.Rows[8][0]) == 0)
                {
                    UpdateRandAnimal(rowAnimal, idGospodarie, animal, albine);
                    sumaAlbine += GetSuma(rowAnimal, albine);
                }

                if (evolutiaAnimalelorPeSem1.Rows.Count > 79)
                {
                    if (Convert.ToInt32(evolutiaAnimalelorPeSem2.Rows[9][0]) == 0)
                    {
                        UpdateRandAnimal(rowAnimal, idGospodarie, animal, pesti);
                        sumaPesti += GetSuma(rowAnimal, pesti);
                    }
                }
            }

            if (situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows.Count > 0)
            {
                if (sumaBovine > 0)
                {
                    //update bovine femele de la sub 1 an la 1-2 ani
                    int femeleIntreUnuSiDoiAniExistente = Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[10][0]);
                    int femeleIntreUnuSiDoiAniActualizate = femeleIntreUnuSiDoiAniExistente + Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[2][0]) - Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[3][0]);

                    animal.Append(@"UPDATE capitole
                                SET col2 = '" + femeleIntreUnuSiDoiAniActualizate +
                                  "' WHERE codCapitol = '7' AND codRand = 11 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");

                    //update bovine femele de la sub 6 luni la sub 1 an
                    animal.Append(@"UPDATE capitole
                                SET col2 ='" + situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[3][0].ToString().Replace(",", ".") +
                                  "' WHERE codCapitol = '7' AND codRand = 3 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");

                    //update bovine masculi de la sub 1 an la 1-2 ani 
                    int masculiIntreUnuSiDoiAniExistenti = Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[11][0]);
                    int masculiIntreUnuSiDoiAniActualizate = masculiIntreUnuSiDoiAniExistenti + Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[4][0]) - Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[5][0]);

                    animal.Append(@"UPDATE capitole
                                SET col2 = '" + masculiIntreUnuSiDoiAniActualizate +
                                  "' WHERE codCapitol = '7' AND codRand = 12 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");

                    //update bovine masculi de la sub 6 luni la sub 1 an
                    animal.Append(@"UPDATE capitole
                                SET col2 = '" + situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[5][0].ToString().Replace(",", ".") +
                                  "' WHERE codCapitol = '7' AND codRand = 5 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");

                    int totalBovineFemeleIntreUnuSiDoiAni = femeleIntreUnuSiDoiAniActualizate + Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[9][0]);

                    animal.Append(@"UPDATE capitole
                                SET col2 = '" + totalBovineFemeleIntreUnuSiDoiAni +
                                  "' WHERE codCapitol = '7' AND codRand = 9 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");

                    int totalBovineIntreUnuSiDoiAni = totalBovineFemeleIntreUnuSiDoiAni + masculiIntreUnuSiDoiAniActualizate;

                    animal.Append(@"UPDATE capitole
                                SET col2 = '" + totalBovineIntreUnuSiDoiAni +
                                  "' WHERE codCapitol = '7' AND codRand = 8 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");

                    int totalBovineSubUnAn = Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[5][0]) + Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[3][0]);

                    animal.Append(@"UPDATE capitole
                                SET col2 = '" + totalBovineSubUnAn +
                                  "' WHERE codCapitol = '7' AND codRand = 2 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");

                    int totalBovine = totalBovineSubUnAn + totalBovineIntreUnuSiDoiAni + Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[12][0]) + Convert.ToInt32(situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows[25][0]);

                    animal.Append(@"UPDATE capitole
                                SET col2 = '" + totalBovine +
                                  "' WHERE codCapitol = '7' AND codRand = 1 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");

                    animal.Append(@"UPDATE capitole
                                SET col2 =  0
                                WHERE codCapitol = '7' AND codRand = 4 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");

                    animal.Append(@"UPDATE capitole
                                SET col2 =  0
                                WHERE codCapitol = '7' AND codRand = 6 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");

                    animal.Append(@"UPDATE capitole
                                SET col2 =  col1
                                WHERE codCapitol = '7' AND codRand = 7 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");

                    animal.Append(@"UPDATE capitole
                                SET col2 =  col1
                                WHERE codCapitol = '7' AND codRand = 10 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");

                    animal.Append(@"UPDATE capitole
                                SET col2 = col1 
                                WHERE codCapitol = '7' AND codRand >= 13 AND codRand <= 22 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");

                    animal.Append(@"UPDATE capitole
                                SET col2 = col1 
                                WHERE codCapitol = '7' AND codRand >= 26 AND codRand <= 29 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");
                }

                if (sumaVaci > 0)
                {
                    animal.Append(@"UPDATE capitole
                                SET col2 = col1 
                                WHERE codCapitol = '7' AND codRand >= 23 AND codRand <= 25 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");
                }

                if (sumaOvine > 0)
                {
                    animal.Append(@"UPDATE capitole
                                SET col2 = col1 
                                WHERE codCapitol = '7' AND codRand >= 30 AND codRand <= 35 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");
                }

                if (sumaCaprine > 0)
                {
                    animal.Append(@"UPDATE capitole
                                SET col2 = col1 
                                WHERE codCapitol = '7' AND codRand >= 36 AND codRand <= 40 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");
                }

                if (sumaPorcine > 0)
                {
                    animal.Append(@"UPDATE capitole
                                SET col2 = col1 
                                WHERE codCapitol = '7' AND codRand >= 41 AND codRand <= 55 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");
                }

                if (sumaCabaline > 0)
                {
                    animal.Append(@"UPDATE capitole
                                SET col2 = col1 
                                WHERE codCapitol = '7' AND codRand >= 56 AND codRand <= 60 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");
                }

                animal.Append(@"UPDATE capitole
                            SET col2 = col1 
                            WHERE codCapitol = '7' AND codRand >= 61 AND codRand <= 62 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");

                animal.Append(@"UPDATE capitole
                            SET col2 = col1 
                            WHERE codCapitol = '7' AND codRand >= 63 AND codRand <= 67 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");

                if (sumaPasari > 0)
                {
                    animal.Append(@"UPDATE capitole
                                SET col2 = col1 
                                WHERE codCapitol = '7' AND codRand >= 68 AND codRand <= 74 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");
                }

                {
                    animal.Append(@"UPDATE capitole
                                SET col2 = col1 
                                 WHERE codCapitol = '7' AND codRand >= 75 AND codRand <= 76 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");
                }

                if (sumaAlteAnimale > 0)
                {
                    animal.Append(@"UPDATE capitole
                                SET col2 = col1 
                                WHERE codCapitol = '7' AND codRand = 77 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");
                }

                if (sumaAlbine > 0)
                {
                    animal.Append(@"UPDATE capitole
                                SET col2 = col1 
                                WHERE codCapitol = '7' AND codRand = 78 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");
                }

                if (situatiaAnimalelorLaInceputulSemestruluiDataTable.Rows.Count > 78)

                    if (sumaPesti > 0)
                    {
                        animal.Append(@"UPDATE capitole
                                SET col2 = col1 
                                WHERE codCapitol = '7' AND codRand >= 79 AND codRand <= 121 AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");
                    }
            }


        }

        SqlCommand insertToDBCommand = new SqlCommand(animal.ToString().Replace(",", "."), connection);
        insertToDBCommand.CommandTimeout = 100000;
        insertToDBCommand.ExecuteNonQuery();



        ManipuleazaBD.InchideConexiune(connection);
    }

    private int GetSuma(DataRow rowAnimal, Tuple<short, short> tuple)
    {
        return Convert.ToInt32(rowAnimal["codRand"]) == tuple.Item1 ? Convert.ToInt32(rowAnimal["col1"]) : 0;
    }

    private void UpdateRandAnimal(DataRow rowAnimal, int idGospodarie, StringBuilder animal, Tuple<short, short> coduriAnimal)
    {
        if (Convert.ToInt32(rowAnimal["codRand"]) == coduriAnimal.Item1)
        {
            animal.Append(@"UPDATE capitole SET col2 = " + rowAnimal["col1"].ToString().Replace(",", ".") + @" 
                                        WHERE codCapitol = '8' AND codRand = " + coduriAnimal.Item2 + " AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");
            animal.Append(@"UPDATE capitole SET col2 = " + rowAnimal["col1"].ToString().Replace(",", ".") + @" 
                                        WHERE codCapitol = '8' AND codRand = " + coduriAnimal.Item1 + " AND gospodarieId = " + idGospodarie + " AND an = " + Session["SESan"] + "; ");
        }
    }

    private void FillGospodariiTable(out DataTable gospodariiTable, out SqlConnection connection, int idUnitate)
    {
        string command = @"SELECT gospodarieId, volum, nrPozitie,membruNume,membruCnp,unitateDenumire
                                FROM gospodarii
                                JOIN unitati ON unitati.unitateId = gospodarii.unitateId
                             WHERE gospodarii.unitateId = " + idUnitate + " AND an = " + Session["SESan"] + "";

        connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlDataAdapter gospodariiAdapter = new SqlDataAdapter(command, connection);

        gospodariiTable = new DataTable();

        gospodariiAdapter.Fill(gospodariiTable);
    }

    private void StergeInvalidariAnimale(int idUnitate)
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        string delete = @"DELETE
                            FROM InvalidariSemestruAnimale where idUnitate = " + idUnitate + "";
        SqlCommand deleteCommand = new SqlCommand(delete, connection);
        deleteCommand.ExecuteNonQuery();
        ManipuleazaBD.InchideConexiune(connection);
    }


}
