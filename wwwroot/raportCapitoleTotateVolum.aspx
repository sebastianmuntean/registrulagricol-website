﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="raportCapitoleTotateVolum.aspx.cs" Inherits="raportCapitoleTotateVolum" UICulture="ro-RO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Tipărire registrul agricol pe volum" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="pnGeneral" runat="server" CssClass="panel_general">
        <asp:Panel ID="pnAfiseazaMesaje" runat="server">
            <asp:ValidationSummary ID="valSumParcele" runat="server" DisplayMode="SingleParagraph"
                Visible="true" ValidationGroup="GrupValidareParcele" CssClass="validator" ForeColor="" />
        </asp:Panel>
        <asp:Panel ID="Panel5" CssClass="ascunsa" runat="server">
            <!-- //////////// VALIDATOARE  \\\\\\\\\\\\\\\\\\ -->
            <asp:CustomValidator ID="valCustom" runat="server" ErrorMessage="x" ValidationGroup="GrupValidareParcele"></asp:CustomValidator>
        </asp:Panel>
        <asp:Panel ID="pnFiltrare" runat="server" CssClass="cauta">
            <asp:Label ID="lblFiltrareDupa" runat="server" Text="Caută după:"></asp:Label>
            <asp:Label ID="lblVolum" runat="server" Text="vol."></asp:Label>
            <asp:TextBox ID="tbVolum" runat="server" AutoPostBack="False" Width="30px"></asp:TextBox>
        </asp:Panel>
        <asp:Panel ID="pnButoane" runat="server" CssClass="butoane">
            <asp:Button ID="btTiparire" runat="server" Text="tipărește" CssClass="buton" OnClick="btTiparire_Click" />
        </asp:Panel>
    </asp:Panel>
</asp:Content>

