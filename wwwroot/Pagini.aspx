<%@ Page Title="Pagini" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Pagini.aspx.cs" Inherits="Pagini" UICulture="ro-RO" Culture="ro-RO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Administrare / Pagini" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="upPagini" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnAfiseazaMesaje" runat="server">
                <asp:ValidationSummary ID="valSumUnitati" runat="server" DisplayMode="SingleParagraph"
                    Visible="true" ValidationGroup="GrupValidarePagini" CssClass="validator" ForeColor="" />
            </asp:Panel>
            <asp:Panel ID="pnAdaugaPagini" CssClass="panel_general" runat="server">
                <asp:Panel ID="Panel1" CssClass="adauga" runat="server">
                    <asp:Panel ID="Panel2" runat="server">
                        <h2>
                            ADAUG� O PAGIN�</h2>
                    </asp:Panel>
                    <asp:Panel ID="adaugare" runat="server">
                        <p>
                            <asp:Label ID="lblPaginaDenumire" runat="server" Text="Denumire"></asp:Label>
                            <asp:TextBox ID="tbPaginaDenumire" runat="server" Width="200px"></asp:TextBox>
                            <asp:Label ID="lblPaginaUrl" runat="server" Text="URL"></asp:Label>
                            <asp:TextBox ID="tbPaginaUrl" runat="server" Width="200px"></asp:TextBox>
                        </p>
                        <p>
                            <asp:Label ID="lblPaginaDescriere" runat="server" Text="Descriere"></asp:Label>
                            <asp:TextBox ID="tbDescrierePagina" runat="server" Width="500px"></asp:TextBox>
                            <asp:Label ID="Activ�" runat="server" Text="Activ�"></asp:Label>
                            <asp:DropDownList ID="ddPaginaActiva" runat="server">
                                <asp:ListItem Text="da" Value="1">       
                                </asp:ListItem>
                                <asp:ListItem Text="nu" Value="0">       
                                </asp:ListItem>
                            </asp:DropDownList>
                        </p>
                    </asp:Panel>
                    <asp:Panel ID="pnButoane" runat="server" CssClass="butoane">
                        <asp:Button ID="btAdaugaPagina" runat="server" CssClass="buton" Text="adaug� o pagin�"
                            OnClick="AdaugaPagina" />
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="pnListaPagina" runat="server" CssClass="panel_general">
                <asp:GridView ID="gvPagini" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                    BorderStyle="None" CellPadding="3" DataKeyNames="paginaId" DataSourceID="SqlPagini"
                    GridLines="Vertical" ShowFooter="True" CssClass="tabela">
                    <Columns>
                        <asp:CommandField ShowEditButton="True" ShowDeleteButton="True" ShowSelectButton="True">
                        </asp:CommandField>
                        <asp:BoundField DataField="paginaId" HeaderText="paginaId" InsertVisible="False"
                            ReadOnly="True" SortExpression="paginaId" />
                        <asp:BoundField DataField="paginaDenumire" HeaderText="Denumire" SortExpression="paginaDenumire" />
                        <asp:BoundField DataField="paginaActiva" HeaderText="Activa" SortExpression="paginaActiva" />
                        <asp:BoundField DataField="paginaUrl" HeaderText="URL" SortExpression="paginaUrl" />
                        <asp:BoundField DataField="paginaDescriere" HeaderText="Descriere" SortExpression="paginaDescriere" />
                    </Columns>
                    <SelectedRowStyle Font-Bold="True" />
                    <HeaderStyle Font-Bold="True" />
                </asp:GridView>
                <asp:SqlDataSource ID="SqlPagini" runat="server" 
                    SelectCommand="SELECT * FROM [pagini]" DeleteCommand="DELETE FROM [pagini] WHERE [paginaId] = @paginaId"
                    InsertCommand="INSERT INTO [pagini] ([paginaDenumire], [paginaActiva], [paginaUrl], [paginaDescriere]) VALUES (@paginaDenumire, @paginaActiva, @paginaUrl, @paginaDescriere)"
                    UpdateCommand="UPDATE [pagini] SET [paginaDenumire] = @paginaDenumire, [paginaActiva] = @paginaActiva, [paginaUrl] = @paginaUrl, [paginaDescriere] = @paginaDescriere WHERE [paginaId] = @paginaId"
                    OnDeleted="SqlPagini_Deleted">
                    <DeleteParameters>
                        <asp:Parameter Name="paginaId" Type="Int32" />
                    </DeleteParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="paginaDenumire" Type="String" />
                        <asp:Parameter Name="paginaActiva" Type="Byte" />
                        <asp:Parameter Name="paginaUrl" Type="String" />
                        <asp:Parameter Name="paginaDescriere" Type="String" />
                        <asp:Parameter Name="paginaId" Type="Int32" />
                    </UpdateParameters>
                    <InsertParameters>
                        <asp:Parameter Name="paginaDenumire" Type="String" />
                        <asp:Parameter Name="paginaActiva" Type="Byte" />
                        <asp:Parameter Name="paginaUrl" Type="String" />
                        <asp:Parameter Name="paginaDescriere" Type="String" />
                    </InsertParameters>
                </asp:SqlDataSource>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
