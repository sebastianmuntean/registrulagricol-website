﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="masura112Lista.aspx.cs" Inherits="masura112" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="<%$ Resources:Resursa, raMasura112Titlu%>" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="pnMasura112" runat="server" CssClass="panel_general" Visible="true">
        <asp:Panel ID="Panel1" runat="server" CssClass="adauga">
        <div>
            <asp:Label ID="lblMembriEligibili" runat="server" Text=""></asp:Label></div>
             <div><asp:Label ID="lblValoareTotalaUDE" runat="server" Text=""></asp:Label></div>
             
            <asp:Button ID="btTipareste" runat="server" 
                Text="generează lista cu gospodăriile eligibile" onclick="btTipareste_Click" CssClass="buton" />
            <div class="citat">
            <span  class="citat">Din Ghidul Solicitantului pentru Măsura 112 - <a href="http://www.apdrp.ro/" target="_blank">www.apdrp.ro</a>:</span><br />
            <span class="citat">
            2.1 Cine poate beneficia de fonduri nerambursabile <br />Beneficiarii eligibili pentru sprijinul financiar nerambursabil acordat prin Măsura 112 sunt fermierii în vârstă de până la 40 de ani (neîmpliniţi la data depunerii Cererii de finanţare), persoane fizice sau juridice care practică în principal activităţi agricole şi a căror exploataţie agricolă:<br /> - are o dimensiune economică cuprinsă între 6 şi 40 UDE;<br /> - este situată pe teritoriul ţării;<br /> - este înregistrată în Registrul unic de identificare – APIA şi Registrul agricol.
            </span>
            </div>
        </asp:Panel>
        <asp:Panel ID="Panel2" runat="server">
            <asp:Label ID="lblTabel" runat="server" Text=""></asp:Label>
        </asp:Panel>
    </asp:Panel>
</asp:Content>
