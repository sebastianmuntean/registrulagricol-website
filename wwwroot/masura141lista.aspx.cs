﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class masura141 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    public static void StergeCapitolCentralizatorUtilizator(Int32 pAn, Int32 pUtilizatorId, String pCapitol)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(pAn));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = @"delete from rapCentralizatoare where utilizatorId='" + pUtilizatorId.ToString() + "' and an='" + pAn.ToString() + "' and capitol='" + pCapitol + "'";
        vCmd.ExecuteNonQuery();
        ManipuleazaBD.InchideConexiune(vCon);
    }
    public static void AdaugaCapitolCentralizator(string pInterogare, short an)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(an);
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = pInterogare;
        vCmd.ExecuteNonQuery();
        ManipuleazaBD.InchideConexiune(vCon);
    }
    protected void TabelMasura141()
    {
        StergeCapitolCentralizatorUtilizator(Convert.ToInt32(Session["SESan"]), Convert.ToInt32(Session["SESutilizatorId"]), "141");

        // luam lista cu gospodarii unde au membri ce sunt eligibili
        DateTime v18Ani = DateTime.Now.AddYears(-18);
        DateTime v62Ani = DateTime.Now.AddYears(-62);
        List<string> vCampuri = new List<string> { "gospodarieId", "volum", "nrPozitie", "adresa", "judet", "localitate", "nume", "cnp", "denumireRudenie", "dataNasterii" };
        List<List<string>> vListaGospodariiCuMembri = ManipuleazaBD.fRezultaListaStringuri(@"SELECT   gospodarii.gospodarieId,  gospodarii.volum, gospodarii.nrPozitie, 'str.' + CASE strainas WHEN 1 THEN sstrada ELSE strada END + ', nr. ' + CASE strainas WHEN 1 THEN snr ELSE nr END + ', bl.' + CASE strainas WHEN 1 THEN sBl ELSE bl END + ', sc.' + CASE strainas WHEN 1 THEN sSc ELSE sc END + ', et.' + CASE strainas WHEN 1 THEN sEtj ELSE et END + ', ap.' + CASE strainas WHEN 1 THEN sAp ELSE ap END AS adresa, CASE strainas WHEN 1 THEN sJudet ELSE judet END AS judet, CASE strainas WHEN 1 THEN sLocalitate ELSE localitate END AS localitate, membri.nume, membri.cnp, membri.denumireRudenie, membri.dataNasterii FROM  gospodarii INNER JOIN  membri ON gospodarii.gospodarieId = membri.gospodarieId WHERE     (CONVERT(datetime, membri.dataNasterii, 104) >= CONVERT(datetime, '" + v62Ani + "', 104)) AND (CONVERT(datetime, membri.dataNasterii, 104) <= CONVERT(datetime, '" + v18Ani + "', 104)) AND (gospodarii.unitateId = " + Session["SESunitateId"].ToString() + ") AND (membri.an = " + Session["SESan"].ToString() + ") AND (gospodarii.an = " + Session["SESan"].ToString() + ") AND (gospodarii.persJuridica = 0) ORDER BY judet, localitate, gospodarii.volumInt, gospodarii.nrPozitieInt, gospodarii.gospodarieId, membri.nume", vCampuri, Convert.ToInt16(Session["SESan"]));
        List<List<string>> vListaFinala = new List<List<string>> { };
        decimal vValoareUDE = 0;
        decimal vValoareUDEAnterioara = 0;
        string vInterogare = "";
        int vCodRand = 1;
        string vGospodarieAnterioara = "";
        List<List<string>> vListaPunctajAnterior = new List<List<string>> { };
        foreach (List<string> vGospodarie in vListaGospodariiCuMembri)
        {
            vValoareUDE =0;
            if (vGospodarie[1] == "1" && vGospodarie[2] == "17")
            {}
            // calculam punctajul pentru fiecare gospodarie din lista
            // daca e corespunzator o adaugam in listafinala
            // daca deja a fost gospodaria respectiva nu o mai verificam pentru ceilalti membri
            List<List<string>> vListaPunctaj = new List<List<string>> { };
            List<string> vGospodarieFinal = vGospodarie;
            // e o alta gospodarie
            if (vGospodarie[0] != vGospodarieAnterioara)
            {
                vListaPunctaj = clsAdeverinte.Masura112CalculTabel1(vGospodarie[0], Session["SESan"].ToString());
                vListaPunctajAnterior = vListaPunctaj;
                foreach (List<string> vRand in vListaPunctaj)
                {
                    if (vRand[0] == "total")
                    {
                        vValoareUDE = Convert.ToDecimal(vRand[7]);
                        vValoareUDEAnterioara = vValoareUDE;
                        break;
                    }
                }
                if (vValoareUDE < 40)
                {
                    vListaPunctaj = clsAdeverinte.Masura112CalculTabel2(vGospodarie[0], Session["SESan"].ToString());
                    foreach (List<string> vRand in vListaPunctaj)
                    {
                        if (vRand[0] == "total")
                        {
                            vValoareUDE = Convert.ToDecimal(vRand[7]);
                            vValoareUDEAnterioara = vValoareUDE;
                            break;
                        }
                    }
                    vValoareUDEAnterioara = vValoareUDE;
                    if (vValoareUDEAnterioara >= 2 && vValoareUDEAnterioara <= 8)
                    {
                        // e gospodarie nou si are punctaj bun
                        vGospodarieFinal.Add(vValoareUDE.ToString());
                        vInterogare += "INSERT INTO [rapCentralizatoare] ([utilizatorId], [an], [capitol], [codRand], [denumire1], [denumire2], [denumire3], [denumire4], [denumire5], [col1]) VALUES ('" + Convert.ToInt32(Session["SESutilizatorId"]) + "','" + Convert.ToInt32(Session["SESan"]) + "','141','" + vCodRand.ToString() + "',N'vol." + vGospodarieFinal[1] + "/poz." + vGospodarieFinal[2] + "',N'" + vGospodarieFinal[3] + "',N'" + vGospodarieFinal[4] + "',N'" + vGospodarieFinal[5] + "',N'" + vGospodarieFinal[6] + ", CNP " + vGospodarieFinal[7] + ",  " + vGospodarieFinal[8] + ", data naşterii " + vGospodarieFinal[9].Remove(10) + "','" + vValoareUDE.ToString().Replace(',', '.') + "');";
                         vCodRand++;
                    }
                }
            }
            // e aceeasi gospodarie cu punctaj bun
            else if (vValoareUDEAnterioara >= 2 && vValoareUDEAnterioara <= 8)
            {
                vGospodarieFinal.Add(vValoareUDE.ToString());
                vValoareUDE = vValoareUDEAnterioara;
                vInterogare += "INSERT INTO [rapCentralizatoare] ([utilizatorId], [an], [capitol], [codRand], [denumire1], [denumire2], [denumire3], [denumire4], [denumire5], [col1]) VALUES ('" + Convert.ToInt32(Session["SESutilizatorId"]) + "','" + Convert.ToInt32(Session["SESan"]) + "','141','" + vCodRand.ToString() + "','','','','',N'" + vGospodarieFinal[6] + ", CNP " + vGospodarieFinal[7] + ",  " + vGospodarieFinal[8] + ", data naşterii " + vGospodarieFinal[9].Remove(10) + "','" + vValoareUDE.ToString().Replace(',', '.') + "');";
                vCodRand++;
            }
            // pastram ultima gospodarie introdusa pentru a scrie diferit in insert
            vGospodarieAnterioara = vGospodarie[0];
        }
        AdaugaCapitolCentralizator(vInterogare, Convert.ToInt16(Session["SESan"]));
    }
    public void TimeSpanToDate(DateTime d1, DateTime d2, out int years, out int months, out int days)
    {
        // compute & return the difference of two dates,
        // returning years, months & days
        // d1 should be the larger (newest) of the two dates
        // we want d1 to be the larger (newest) date
        // flip if we need to
        if (d1 < d2)
        {
            DateTime d3 = d2;
            d2 = d1;
            d1 = d3;
        }

        // compute difference in total months
        months = 12 * (d1.Year - d2.Year) + (d1.Month - d2.Month);

        // based upon the 'days',
        // adjust months & compute actual days difference
        if (d1.Day < d2.Day)
        {
            months--;
            days = DateTime.DaysInMonth(d2.Year, d2.Month) - d2.Day + d1.Day;
        }
        else
        {
            days = d1.Day - d2.Day;
        }
        // compute years & actual months
        years = months / 12;
        months -= years * 12;
    }
    protected void btTipareste_Click(object sender, EventArgs e)
    {
        TabelMasura141();        
        ResponseHelper.Redirect("~/printMasura141.aspx?codCapitol=141", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");

    }
}
