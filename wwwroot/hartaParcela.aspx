﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="hartaParcela.aspx.cs" Inherits="hartaParcela" EnableEventValidation="false" %>

<%@ Register Assembly="WebEdition" Namespace="ThinkGeo.MapSuite.WebEdition" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

    <script type="text/javascript">
        window.onresize = function(event) {
            var pageHeight = jQuery(window).height();
            var navHeight = pageHeight - 82;
            $("#panelHarta").css({ "height": navHeight + 'px' });
        }

        function pageLoad() {
            $(document).ready(function() {
                var pageHeight = jQuery(window).height();
                var navHeight = pageHeight - 82;
                $("#panelHarta").css({ "height": navHeight + 'px' });
                ResizeMainPage();
                $("#panelHarta").slideDown();
                /*  $("#desenare").hide();
                $("#operatii").hide();
                $("#listaParcele").hide();
                $("#straturi").hide();
                */
                //  $("#ctl00_ContentPlaceHolder1_pnlPeste").hide();
                $("#lblDesene").mouseenter(function(event) {
                    $("#inchide").show();
                    $("#desenare").slideDown();
                    $("#operatii").hide();
                    $("#listaParcele").hide();
                    $("#straturi").hide();
                    $("#ctl00_ContentPlaceHolder1_pnlPeste").show();

                });
                $("#lblOperatii").mouseenter(function(event) {
                    $("#inchide").show();
                    $("#desenare").hide();
                    $("#operatii").slideDown();
                    $("#listaParcele").hide();
                    $("#straturi").hide();
                    $("#ctl00_ContentPlaceHolder1_pnlPeste").show();

                });
                $("#lblListaParcele").mouseenter(function(event) {
                    $("#inchide").show();
                    $("#desenare").hide();
                    $("#operatii").hide();
                    $("#listaParcele").slideDown();
                    $("#straturi").hide();
                    $("#ctl00_ContentPlaceHolder1_pnlPeste").show();

                });
                $("#lblStraturi").mouseenter(function(event) {
                    $("#inchide").show();
                    $("#desenare").hide();
                    $("#operatii").hide();
                    $("#listaParcele").hide();
                    $("#straturi").slideDown();
                    $("#ctl00_ContentPlaceHolder1_pnlPeste").show();

                });
                $("#ctl00_ContentPlaceHolder1_pnlPeste").click(function(event) {
                    $("#inchide").hide();
                    $("#desenare").hide();
                    $("#operatii").hide();
                    $("#listaParcele").hide();
                    $("#straturi").hide();
                    $("#ctl00_ContentPlaceHolder1_pnlPeste").hide();
                });
                $("#inchide").click(function(event) {
                    $("#inchide").hide();
                    $("#desenare").hide();
                    $("#operatii").hide();
                    $("#listaParcele").hide();
                    $("#straturi").hide();
                    $("#ctl00_ContentPlaceHolder1_pnlPeste").hide();
                });
                $("#tooltip_inchide").click(function(event) {
                    $("#ctl00_ContentPlaceHolder1_pnlToolTip").hide();
                    $("#ctl00_ContentPlaceHolder1_pnlToolTip").css('display', 'none');
                })


            });
        }
        ResizeMainPage = function() {
            var map = ctl00_ContentPlaceHolder1_harta.GetOpenLayersMap();
            map.updateSize();
        }
        function colorChanged(sender) {
            sender.get_element().style.color =
       "#" + sender.get_selectedColor();
            sender.get_element().style.backgroundColor = "#" + sender.get_selectedColor();
        }

        
    </script>

    <style type="text/css">
        #paneluri_container
        {
            position: relative;
            width: 100%;
        }
        .paneluri
        {
            margin: 0 auto;
            text-align: center;
            width: 100%;
            position: relative;
            display: block;
        }
        .paneluri_scule
        {
            position: relative;
            top: 0;
            left: 0;
            width: 95%;
            display: block;
            height: auto;
            margin: 40px auto 10px;
            text-align: center;
        }
        .paneluri_scule div.scula
        {
            display: inline-block;
            width: 100px;
            height: 130px;
            vertical-align: top;
            text-align: center;
            margin: 10px;
            position: relative;
        }
        .paneluri_scule div p
        {
            clear: both;
            display: inline-block;
            text-align: center;
            width: 100px;
            position: relative;
        }
        .paneluri_scule div.scula input
        {
            text-align: center;
            width: 64px;
            position: relative;
            margin: 0 auto;
            padding: 0;
        }
        .paneluri_container_scule
        {
            background: none repeat scroll 0 0 #FFFDDE;
            border-top: 1px solid black;
            border-bottom: 1px solid black;
            display: block;
            height: auto;
            left: 0;
            opacity: 0.9;
            overflow: visible;
            position: absolute;
            top: 0;
            width: 100%;
            z-index: 9997;
        }
        .paneluri_meniuri_scule
        {
            text-align: left;
            cursor: pointer;
            font-size: 12px;
            font-weight: bold;
            padding: 0px 5%;
            position: absolute;
            top: 0;
            width: 90%;
            display: block;
            height: auto;
            z-index: 9999;
            overflow: hidden;
        }
        .paneluri_meniuri_scule div
        {
            border-radius: 0em 0em 10px 10px;
            -moz-border-radius: 0px 0px 10px 10px;
            background: none repeat scroll 0 0 White;
            border-top: 1px solid black;
            display: inline-block;
            height: 1.4em;
            line-height: 1em;
            margin-right: 30px;
            opacity: 0.7;
            overflow: hidden;
            padding: 3px 7px;
            position: relative;
            text-align: center;
            width: 100px;
        }
        #inchide
        {
            position: absolute;
            top: -30px;
            right: 20px;
            text-align: right;
            z-index: 99999999;
        }
        .bara_scule
        {
            opacity: 1;
            width: 54px;
            font-size: 45px;
            height: 8em;
            overflow: visible;
            position: absolute;
            right: 3px;
            top: 0px;
            margin: 35px 0 0 0;
            padding: 5px;
            z-index: 9995;
        }
        .bara_scule_background
        {
            opacity: 0.3;
            background: white;
            -moz-border-radius: 10px;
            border-radius: 10px;
            height: 100%;
            z-index: 9993;
            position: absolute;
            top: 0;
            left: 0;
            width: 54px;
        }
        .bara_scule_butoane
        {
            position: absolute;
            top: 0;
            left: 0;
            z-index: 9994;
            padding: 2px 6px;
        }
        .bara_scule_butoane input
        {
            border: 0;
            height: 42px;
            width: 42px;
            margin: 1px 0;
            float: left;
            clear: both;
        }
        .bara_arata_parcele
        {
            background: url(Imagini/harti/arata_parcele42.png) no-repeat transparent;
        }
        .bara_arata_parcele:hover
        {
            background: url(Imagini/harti/arata_parcele42h.png) no-repeat transparent;
        }
        .bara_selecteaza_parcele
        {
            background: url(Imagini/harti/selecteaza_parcela42.png) no-repeat transparent;
        }
        .bara_selecteaza_parcele:hover
        {
            background: url(Imagini/harti/selecteaza_parcela42h.png) no-repeat transparent;
        }
        .bara_masoara
        {
            background: url(Imagini/harti/masoara42.png) no-repeat transparent;
        }
        .bara_masoara:hover
        {
            background: url(Imagini/harti/masoara42h.png) no-repeat transparent;
        }
        .bara_unire_linii
        {
            background: url(Imagini/harti/unire_linii42.png) no-repeat transparent;
        }
        .bara_unire_linii:hover
        {
            background: url(Imagini/harti/unire_linii42h.png) no-repeat transparent;
        }
        .bara_modificare
        {
            background: url(Imagini/harti/modificare42.png) no-repeat transparent;
        }
        .bara_modificare:hover
        {
            background: url(Imagini/harti/modificare42h.png) no-repeat transparent;
        }
        .bara_previzualizare
        {
            background: url(Imagini/harti/previzualizare42.png) no-repeat transparent;
        }
        .bara_previzualizare:hover
        {
            background: url(Imagini/harti/previzualizare42h.png) no-repeat transparent;
        }
        .bara_salvare_parcela
        {
            background: url(Imagini/harti/salvare_parcela42.png) no-repeat transparent;
        }
        .bara_salvare_parcela:hover
        {
            background: url(Imagini/harti/salvare_parcela42h.png) no-repeat transparent;
        }
        .bara_salvare
        {
            background: url(Imagini/harti/salvare42.png) no-repeat transparent;
        }
        .bara_salvare:hover
        {
            background: url(Imagini/harti/salvare42h.png) no-repeat transparent;
        }
        .tooltip
        {
            background: none repeat scroll 0 0 White;
            float: left;
            height: auto;
            left: -10100px;
            top:-100000px;
            line-height: 15px;
            min-height: 30px;
            overflow: inherit;
            position: absolute;
            width: 400px;
            z-index: 999999999;
            padding:3px 10px;
            -moz-border-radius: 10px;
            border-radius: 10px;
            opacity: 0.7;
display:none;
            
        }
        .panel_peste
        {
            width: 100%;
            position: fixed;
            background: white;
            opacity: 0.3;
            text-align: center;
            top: 0;
            left: 0;
            margin: 0 auto;
            height: 100%;
            z-index: 9996;
        }
        .upLista
        {
            position: relative;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
   
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <div style="position:relative; top:-10px; margin-bottom:-10px;">
            <asp:Panel ID="pnAfiseazaMesaje" runat="server">
                <asp:ValidationSummary ID="valEroareSummary" runat="server" DisplayMode="SingleParagraph"
                    Visible="true" CssClass="validatorCenter" ForeColor="" />
            </asp:Panel>
            <asp:Panel ID="Panel5" CssClass="ascunsa" runat="server">
                <!-- //////////// VALIDATOARE  \\\\\\\\\\\\\\\\\\ -->
                <asp:CustomValidator ID="valEroare" runat="server" ErrorMessage="Eroare"></asp:CustomValidator>
            </asp:Panel>

    <div id="paneluri_container">
        <asp:Panel ID="upLista" runat="server" CssClass="upLista">
            <asp:Panel ID="Panel2" CssClass="paneluri_meniuri_scule" runat="server">
                <div id="lblDesene">
                    <asp:Label ID="lblDesene1" runat="server" Text="Desenare"></asp:Label>
                </div>
                <div id="lblOperatii">
                    <asp:Label ID="lblOperatii1" runat="server" Text="Operaţii"></asp:Label>
                </div>
                <div id="lblListaParcele">
                    <asp:Label ID="lblListaParcele1" runat="server" Text="Listă parcele"></asp:Label>
                </div>
                <div id="lblStraturi">
                    <asp:Label ID="lblStraturi1" runat="server" Text="Straturi"></asp:Label>
                </div>
            </asp:Panel>
            <div id="pnlPeste" class="panel_peste" runat="server"  style="display: none;">
            </div>
            <div class="paneluri" id="panelHarta">
                <cc1:Map ID="harta" runat="server" Height="100%" Width="100%">
                </cc1:Map>
            </div>
            <div class="paneluri_scule" id="inchide" style="display: none;">
                       <asp:Image ID="imInchide" ToolTip="Închide" ImageUrl="~/Imagini/harti/inchide.png" Width="42" Height="42" runat="server" />
                    </div>
            <asp:Panel ID="Panel1" CssClass="paneluri_container_scule" runat="server" Visible="true">
                    
                    <div class="paneluri_scule" id="desenare" style="display: none;">
                    <asp:Panel ID="pnOptiuni" runat="server">
                        <asp:ImageButton ID="buttonNormal" runat="server" ToolTip="Normal Mode" ImageUrl="~/images/harta/Cursor.png"
                            OnClientClick="ctl00_ContentPlaceHolder1_harta.SetDrawMode('Normal');return false;" />
                        <asp:ImageButton ID="buttonDrawPoint" runat="server" ToolTip="Draw Point" ImageUrl="~/images/harta/point28.png"
                            OnClientClick="ctl00_ContentPlaceHolder1_harta.SetDrawMode('Point');return false;" />
                        <asp:ImageButton ID="buttonDrawLine" runat="server" ToolTip="Draw Line"  ImageUrl="~/images/harta/line28.png"
                            OnClientClick="ctl00_ContentPlaceHolder1_harta.SetDrawMode('Line');return false;" />
                        <asp:ImageButton ID="buttonDrawRectangle" runat="server" ToolTip="Draw Rectangle"
                            ImageUrl="~/images/harta/rectangle28.png" OnClientClick="ctl00_ContentPlaceHolder1_harta.SetDrawMode('Rectangle');return false;" />
                        <asp:ImageButton ID="buttonDrawSquare" runat="server" OnClientClick="ctl00_ContentPlaceHolder1_harta.SetDrawMode('Square');return false;"
                            ToolTip="Draw Square" ImageUrl="~/images/harta/square28.png" />
                        <asp:ImageButton ID="buttonDrawPolygon" runat="server" ToolTip="Draw Polygon" ImageUrl="~/images/harta/polygon28.png"
                            OnClientClick="ctl00_ContentPlaceHolder1_harta.SetDrawMode('Polygon');return false;" />
                        <asp:ImageButton ID="buttonDrawCircle" runat="server" ToolTip="Draw Circle" ImageUrl="~/images/harta/circle28.png"
                            OnClientClick="ctl00_ContentPlaceHolder1_harta.SetDrawMode('Circle');return false;" />
                        <asp:ImageButton ID="buttonDrawEllipse" runat="server" ToolTip="Draw Ellipse" ImageUrl="~/images/harta/ellipse28.png"
                            OnClientClick="ctl00_ContentPlaceHolder1_harta.SetDrawMode('Ellipse');return false;" />
                    </asp:Panel>
                </div>
                <div class="paneluri_scule" id="operatii" style="display: none;">
                    <div class="scula">
                        <asp:ImageButton ID="ImageButton1" ImageUrl="~/Imagini/harti/arata_parcele.png" OnClick="lbAfisareParcele_Click"
                            ToolTip="Afişează parcelă(e)" Width="64" Height="64" runat="server" />
                        <p>
                            Afişează parcelă(e)</p>
                    </div>
                    <div class="scula">
                        <asp:ImageButton ID="ImageButton2" ImageUrl="~/Imagini/harti/selecteaza_parcela.png"
                            OnClick="lbSelectareParcele_Click" Width="64" Height="64" runat="server" ToolTip="Selectare parcele" />
                        <p>
                            Selectare parcele</p>
                    </div>
                    <div class="scula">
                        <asp:ImageButton ID="ImageButton3" ImageUrl="~/Imagini/harti/masoara.png" OnClick="lbCalculDistanta_Click"
                            ToolTip="Calcul distanţă/suprafaţă" Width="64" Height="64" runat="server" /><p>
                                Calcul distanţă/suprafaţă</p>
                    </div>
                    <div class="scula">
                        <asp:ImageButton ID="ImageButton4" ImageUrl="~/Imagini/harti/unire_linii.png" OnClick="lbLeagaLinii_Click"
                            ToolTip="Unire linii parcelă" Width="64" Height="64" runat="server" /><p>
                                Unire linii parcelă</p>
                    </div>
                    <div class="scula">
                        <asp:ImageButton ID="ImageButton5" ImageUrl="~/Imagini/harti/modificare.png" OnClick="lbTransformaInPoligon_Click"
                            ToolTip="Transformă în poligon" Width="64" Height="64" runat="server" /><p>
                                Transformă în poligon</p>
                    </div>                    
                    <div class="scula">
                        <asp:ImageButton ID="ImageButton8" ImageUrl="~/Imagini/harti/previzualizare.png" OnClick="lbTransformaInPoligon_Click"
                            ToolTip="Previzualizare selecţie" Width="64" Height="64" runat="server" /><p>
                                Previzualizare selecţie</p>
                    </div>
                    <div class="scula">
                        <asp:ImageButton ID="ImageButton6" ImageUrl="~/Imagini/harti/salvare_parcela.png" runat="server"
                            OnClick="lbSalvareParcela_Click" OnClientClick='return confirm ("Coordonatele existente la parcela aleasă vor fi înlocuite de coordonatele noi. Sigur continuaţi ?")'
                            ToolTip="Salvare parcelă" Width="64" Height="64" /><p>
                                Salvare parcelă</p>
                    </div>
                    <div class="scula">
                        <asp:ImageButton ID="ImageButton7" ImageUrl="~/Imagini/harti/salvare.png" OnClick="lbSalvareModificari_Click"
                            ToolTip="Salvare modificări hartă" Width="64" Height="64" runat="server" />
                        <p>
                            Salvare modificări hartă</p>
                    </div>
                </div>
                <div class="paneluri_scule" id="listaParcele" style="display: none;">
                    <asp:UpdatePanel runat="server" ID="gospodariiParcele" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel ID="pnLista" runat="server" Visible="true">
                                <h1 style="text-align: center;">
                                    Alegeţi parcelele pentru afişare</h1>
                                <asp:Panel ID="pnCautare" runat="server" CssClass="cauta">
                                    <asp:Label ID="lbCautaDupa" runat="server" Text="Caută:"></asp:Label>
                                    <asp:Label ID="lbfVolum" runat="server" Text="Vol."></asp:Label>
                                    <asp:TextBox ID="tbfVolum" Width="27px" runat="server" AutoPostBack="True"></asp:TextBox>
                                    <asp:Label ID="lbfNrPoz" runat="server" Text="Poz."></asp:Label>
                                    <asp:TextBox ID="tbfNrPoz" Width="27px" runat="server" AutoPostBack="True"></asp:TextBox>
                                    <asp:Label ID="ldfNume" runat="server" Text="Nume"></asp:Label>
                                    <asp:TextBox ID="tbfNume" Width="70px" runat="server" AutoPostBack="True"></asp:TextBox>
                                    <asp:Label ID="lblFTopo" runat="server" Text="TOPO" />
                                    <asp:TextBox ID="tbFTopo" runat="server" autocomplete="off" AutoPostBack="True" Width="50px" />
                                    <asp:Label ID="lblFCF" runat="server" Text="C.F." />
                                    <asp:TextBox ID="tbFCF" runat="server" autocomplete="off" AutoPostBack="True" Width="80px" />
                                </asp:Panel>
                                <asp:GridView ID="gvParcele" AllowPaging="True" DataKeyNames="parcelaId" AllowSorting="True"
                                    CssClass="tabela" runat="server" AutoGenerateColumns="False" DataSourceID="SqlParcele"
                                    OnRowDataBound="gvParcele_RowDataBound" 
                                    onpageindexchanged="gvParcele_PageIndexChanged" 
                                    onprerender="gvParcele_PreRender">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lblParcelaId" Visible="false" runat="server" Text='<%# Eval("parcelaId") %>'></asp:Label>
                                                <asp:CheckBox ID="cbAfisare" AutoPostBack="true" Checked="true" runat="server" OnCheckedChanged="cbAfisare_CheckedChanged" />
                                            </ItemTemplate>
                                            <HeaderTemplate>
                                                <asp:LinkButton ID="lbSelecteazaTot" runat="server" OnClick="lbSelecteazaTot_Click">Alege tot</asp:LinkButton>
                                            </HeaderTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="volum" HeaderText="Volum" SortExpression="volum" />
                                        <asp:BoundField DataField="nrPozitie" HeaderText="Poziţie" SortExpression="nrPozitie" />
                                        <asp:BoundField DataField="nume" HeaderText="Nume" SortExpression="nume" />
                                        <asp:BoundField DataField="tip" HeaderText="Tip" SortExpression="tip" />
                                        <asp:BoundField DataField="localitate" HeaderText="Localitate" SortExpression="localitate" />
                                        <asp:BoundField DataField="strada" HeaderText="Strada" SortExpression="strada" />
                                        <asp:BoundField DataField="nr" HeaderText="Nr" SortExpression="nr" />
                                        <asp:BoundField DataField="parcelaDenumire" HeaderText="Denumire parcelă" SortExpression="parcelaDenumire" />
                                        <asp:BoundField DataField="parcelaNrTopo" HeaderText="Nr. topo" SortExpression="parcelaNrTopo" />
                                        <asp:BoundField DataField="parcelaCF" HeaderText="CF" SortExpression="parcelaCF" />
                                        <asp:BoundField DataField="parcelaNrCadastral" HeaderText="Nr. cad." SortExpression="parcelaNrCadastral" />
                                        <asp:BoundField DataField="parcelaSuprafataIntravilanHa" HeaderText="Ha intra" SortExpression="parcelaSuprafataIntravilanHa" />
                                        <asp:BoundField DataField="parcelaSuprafataIntravilanAri" HeaderText="Ari intra"
                                            SortExpression="parcelaSuprafataIntravilanAri" />
                                        <asp:BoundField DataField="parcelaSuprafataExtravilanHa" HeaderText="Ha extra" SortExpression="parcelaSuprafataExtravilanHa" />
                                        <asp:BoundField DataField="parcelaSuprafataExtravilanAri" HeaderText="Ari extra"
                                            SortExpression="parcelaSuprafataExtravilanAri" />
                                    </Columns>
                                    <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                                </asp:GridView>
                                <asp:SqlDataSource ID="SqlParcele" runat="server" 
                                    SelectCommand="SELECT (SELECT TOP (1) cnp FROM membri WHERE (unitateId = @unitateId) AND (an = @an) AND (gospodarieId = gospodarii.gospodarieId) AND (codRudenie = '1')) AS cnp, gospodarii.gospodarieId, gospodarii.unitateId, gospodarii.volum, gospodarii.nrPozitie, gospodarii.volumInt, gospodarii.nrPozitieInt, gospodarii.codSiruta, gospodarii.gospodarieCui, CASE gospodarii.tip WHEN '1' THEN 'Localnic' WHEN '2' THEN 'Străinaş' WHEN '3' THEN 'Firmă pe raza localităţii' ELSE 'Firmă străinaşă' END AS tip, CASE strainas WHEN 1 THEN sstrada ELSE strada END AS strada, CASE strainas WHEN 1 THEN snr ELSE nr END AS nr, gospodarii.nrInt, gospodarii.bl, gospodarii.sc, gospodarii.et, gospodarii.ap, gospodarii.codExploatatie, gospodarii.codUnic, CASE strainas WHEN 1 THEN sjudet ELSE judet END AS judet, CASE strainas WHEN 1 THEN slocalitate ELSE localitate END AS localitate, gospodarii.persJuridica, gospodarii.jUnitate, gospodarii.jSubunitate, gospodarii.jCodFiscal, gospodarii.jNumeReprez, gospodarii.strainas, gospodarii.sStrada, gospodarii.sNr, gospodarii.sBl, gospodarii.sSc, gospodarii.sEtj, gospodarii.sAp, gospodarii.sJudet, gospodarii.sLocalitate, CASE persjuridica WHEN 0 THEN (SELECT TOP (1) nume FROM membri WHERE an = @an AND gospodarieId = gospodarii.gospodarieid AND codrudenie = 1) ELSE junitate END AS nume, parcele.parcelaDenumire, parcele.parcelaCodRand, parcele.parcelaSuprafataIntravilanHa, parcele.parcelaSuprafataIntravilanAri, parcele.parcelaSuprafataExtravilanHa, parcele.parcelaSuprafataExtravilanAri, parcele.parcelaNrTopo, parcele.parcelaCF, parcele.parcelaCategorie, parcele.parcelaNrBloc, parcele.parcelaMentiuni, parcele.parcelaNrCadastral, parcele.parcelaNrCadastralProvizoriu, parcele.parcelaLocalitate, parcele.parcelaAdresa, parcele.parcelaTarla, parcele.parcelaTitluProprietate, parcele.parcelaClasaBonitate, parcele.parcelaPunctaj, parcele.parcelaId FROM gospodarii INNER JOIN parcele ON gospodarii.gospodarieId = parcele.gospodarieId AND gospodarii.an = parcele.an WHERE (COALESCE (gospodarii.volum, N'') LIKE @volum) AND (COALESCE (gospodarii.nrPozitie, N'') LIKE @nrPozitie) AND (COALESCE (CASE persjuridica WHEN 0 THEN (SELECT TOP (1) nume FROM membri WHERE an = @an AND gospodarieId = gospodarii.gospodarieid AND codrudenie = 1) ELSE junitate END, N'') LIKE '%' + @nume + '%') AND (gospodarii.unitateId = @unitateId) AND (gospodarii.an = @an) AND (parcele.parcelaNrTopo LIKE @nrTopo + '%') AND (parcele.parcelaCF LIKE @CF + '%') ORDER BY gospodarii.volumInt, gospodarii.nrPozitieInt, parcele.parcelaId, parcele.parcelaCategorie">
                                    <SelectParameters>
                                        <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" />
                                        <asp:SessionParameter Name="an" SessionField="SESan" />
                                        <asp:ControlParameter Name="volum" ControlID="tbfVolum" PropertyName="Text" DefaultValue="%" />
                                        <asp:ControlParameter Name="nrPozitie" ControlID="tbfNrPoz" PropertyName="Text" DefaultValue="%" />
                                        <asp:ControlParameter Name="nume" ControlID="tbfNume" PropertyName="Text" DefaultValue="%" />
                                        <asp:ControlParameter Name="nrTopo" ControlID="tbFTopo" PropertyName="Text" DefaultValue="%" />
                                        <asp:ControlParameter Name="CF" ControlID="tbFCF" PropertyName="Text" DefaultValue="%" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="gvParcele" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <div class="paneluri_scule" id="straturi" style="display: none;">
                    <asp:Panel ID="pnLayers" Visible="true" runat="server">
                        <h1 style="text-align: center; ">
                            Alegeţi straturi pentru afişare</h1>
                        <asp:DataList ID="dlLayers" runat="server" Width="100%" DataKeyField="layerId" DataSourceID="SqlLayers"
                            RepeatColumns="3" OnItemDataBound="dlLayers_ItemDataBound">
                            <ItemTemplate>
                                <table style="width: 100%; text-align: left;">
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="cbLayerAfisare" Checked="true" runat="server" />
                                            <asp:Label ID="lblDenumireLabel" runat="server" Text='<%# Eval("layerDenumire") %>' />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbCuloareLayer" onmouseover="this.style.cursor='pointer'" Width="50px"
                                                runat="server" Text='<%# Eval("layerCuloare") %>'></asp:TextBox>
                                            <asp:ColorPickerExtender ID="tbCuloareLayer_ColorPickerExtender" runat="server" Enabled="True"
                                                TargetControlID="tbCuloareLayer" OnClientColorSelectionChanged="colorChanged">
                                            </asp:ColorPickerExtender>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:DataList>
                        <asp:SqlDataSource ID="SqlLayers" runat="server" 
                            SelectCommand="SELECT layerId, unitateId, layerDenumire, COALESCE (layerCuloare, '0000CC') AS layerCuloare FROM hartaLayer WHERE (unitateId = @unitateId) ORDER BY layerDenumire">
                            <SelectParameters>
                                <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <br />
                        <asp:LinkButton ID="lbAfisareLayers" runat="server" OnClick="lbAfisareLayers_Click">Afişează straturile selectate pe hartă</asp:LinkButton>
                    </asp:Panel>
                </div>
            </asp:Panel>
            <div class="bara_scule">
                <div class="bara_scule_background">
                </div>
                <div class="bara_scule_butoane">
                    <asp:Button ID="btArataParcele" CssClass="bara_arata_parcele" OnClick="lbAfisareParcele_Click"
                            ToolTip="Afişează parcelă(e)" Width="42" Height="42" runat="server" />
                    <asp:Button ID="btSelecteazaParcela" CssClass="bara_selecteaza_parcele" OnClick="lbSelectareParcele_Click" Width="42" Height="42" runat="server" ToolTip="Selectare parcele" />
                    <asp:Button ID="btCalculDistanta" CssClass="bara_masoara" OnClick="lbCalculDistanta_Click" ToolTip="Calcul distanţă/suprafaţă" Width="42"  Height="42" runat="server" />
                    <asp:Button ID="btUnireLinii" CssClass="bara_unire_linii"   OnClick="lbLeagaLinii_Click" ToolTip="Unire linii parcelă" Width="42" Height="42"       runat="server" />
                    <asp:Button ID="btTransformaPoligon" CssClass="bara_modificare" OnClick="lbTransformaInPoligon_Click" ToolTip="Transformă în poligon" Width="42" Height="42" runat="server" />
                    <asp:Button ID="btPrevizualizare" CssClass="bara_previzualizare" OnClick="lbTransformaInPoligon_Click" ToolTip="Previzualizare selecţie" Width="42" Height="42" runat="server" />
                    <asp:Button ID="btSalveaza" CssClass="bara_salvare_parcela" runat="server" OnClick="lbSalvareParcela_Click" OnClientClick='return confirm ("Coordonatele existente la parcela aleasă vor fi înlocuite de coordonatele noi. Sigur continuaţi ?")' ToolTip="Salvare parcelă" Width="42" Height="42" />
                    <asp:Button ID="btSalveazaModificari" CssClass="bara_salvare" ImageUrl="~/Imagini/harti/salvare42.png" OnClick="lbSalvareModificari_Click" ToolTip="Salvare modificări hartă" Width="42" Height="42" runat="server" />
                </div>
            </div>
            <div id="pnlToolTip" runat="server" class="tooltip" style="display:none;">
                <div style="font-size: 9px; color: Red; position: relative; float: right; top: -20px;
                    right: 0; width: 30px; height: 13px;">
                    <u>închide</u></div>
            </div>
        </asp:Panel>
    </div>
    
    </div>
</asp:Content>
