﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="InchidereAnMultipla.aspx.cs" Inherits="InchidereAnMultipla" Culture="ro-RO" UICulture="ro-RO" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Administrare / Închidere an multipla" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel ID="upParcele" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel1" runat="server" CssClass="panel_general">
                <asp:Panel ID="Panel2" CssClass="adauga" runat="server">
                    <asp:Panel ID="pnEroare" runat="server" HorizontalAlign="Center">
                        <asp:Label ID="lblEroare" runat="server" CssClass="validator" Text="Eroare" Visible="false" />
                    </asp:Panel>
                    <p style="text-align:center; margin:10px auto;">
                        <asp:Label ID= "lblAn" runat="server" Text="An"></asp:Label>
                        <asp:TextBox ID="tbAn" runat="server" onprerender="tbAn_PreRender"></asp:TextBox>
                   
                    </p>
                    <asp:Panel ID="pnButoaneAdaugare" runat="server" CssClass="butoane" HorizontalAlign="Center">
                        <asp:Button ID="inchideButton" runat="server" CssClass="buton" Text="Porneşte închiderea multipla - schimba in spate unitatile care se vor inchide" OnClick="InchideButtonClick" OnClientClick="return confirm('Doriti sa incepeti inchiderea ?')" />
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>   
     
</asp:Content>
