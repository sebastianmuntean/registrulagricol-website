﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class semnalareEroare : System.Web.UI.Page
{
    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "semnalareEroare", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
        }
    }
    protected void btModificaCont_Click(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd1 = new SqlCommand();
        SqlCommand vCmd2 = new SqlCommand();
        vCmd1.Connection = vCon;
        vCmd2.Connection = vCon;
        // extrag judetId din unitati
        vCmd1.CommandText = "select top(1) judetId from unitati where unitateId='" + Session["SESunitateId"].ToString() + "'";
        string vJudetId = vCmd1.ExecuteScalar().ToString();
        if (tbMesaj.Text != "")
        {
            string vUnitateDenumire = "";
            vCmd2.CommandText = "SELECT utilizatori.utilizatorNume,utilizatori.utilizatorEmail, utilizatori.utilizatorPrenume, unitati.unitateDenumire FROM unitati INNER JOIN utilizatori ON unitati.unitateId = utilizatori.unitateId WHERE (utilizatori.utilizatorId = '" + Session["SESutilizatorId"].ToString() + "')";
            SqlDataReader vTabel = vCmd2.ExecuteReader();
            if (vTabel.Read())
            {
                vUnitateDenumire = vTabel["unitateDenumire"].ToString();
                try
                {
                    string vEmailUser = vTabel["utilizatorEmail"].ToString();
                    System.Net.Mail.MailAddress vAdresa = new System.Net.Mail.MailAddress("office@tntcomputers.ro");
                    System.Net.Mail.MailAddress vAdresa1 = new System.Net.Mail.MailAddress(vEmailUser);
                    System.Net.Mail.MailMessage vMsg = new System.Net.Mail.MailMessage(vAdresa1, vAdresa);

                    vMsg.Body = @"" + tbMesaj.Text + "<br/><br/> Acest e-mail a fost trimis de " + vTabel["utilizatorNume"].ToString() + " " + vTabel["utilizatorPrenume"].ToString() + " şi face parte din unitatea " + vTabel["unitateDenumire"].ToString();
                    vMsg.Subject = "Semnalare erori Registrul Agricol";
                    vMsg.IsBodyHtml = true;

                    System.Net.NetworkCredential vAutentificare = new System.Net.NetworkCredential("auto@tntcomputers.ro", "traxdata");
                    System.Net.Mail.SmtpClient vClient = new System.Net.Mail.SmtpClient("smtp.gmail.com", 587);

                    vClient.EnableSsl = true;
                    vClient.UseDefaultCredentials = false;
                    vClient.Credentials = vAutentificare;
                    vClient.Send(vMsg);
                    // trimit mesaj stefi si mihai
                    //vAdresa = new System.Net.Mail.MailAddress("tudor.laza@tntcomputers.ro");
                    //vAdresa1 = new System.Net.Mail.MailAddress(vEmailUser);
                    //vMsg = new System.Net.Mail.MailMessage(vAdresa1, vAdresa);

                    //vMsg.Body = @"" + tbMesaj.Text + "<br/><br/> Acest e-mail a fost trimis de " + vTabel["utilizatorNume"].ToString() + " " + vTabel["utilizatorPrenume"].ToString() + " şi face parte din unitatea " + vTabel["unitateDenumire"].ToString();
                    //vMsg.Subject = "Semnalare erori Registrul Agricol";
                    //vMsg.IsBodyHtml = true;
                    //vClient.Send(vMsg);
                    //vAdresa = new System.Net.Mail.MailAddress("stefania.spaiuc@tntcomputers.ro");
                    //vAdresa1 = new System.Net.Mail.MailAddress(vEmailUser);
                    //vMsg = new System.Net.Mail.MailMessage(vAdresa1, vAdresa);

                    //vMsg.Body = @"" + tbMesaj.Text + "<br/><br/> Acest e-mail a fost trimis de " + vTabel["utilizatorNume"].ToString() + " " + vTabel["utilizatorPrenume"].ToString() + " şi face parte din unitatea " + vTabel["unitateDenumire"].ToString();
                    //vMsg.Subject = "Semnalare erori Registrul Agricol";
                    //vMsg.IsBodyHtml = true;
                    //vClient.Send(vMsg);

                    //vAdresa = new System.Net.Mail.MailAddress("diana.plotuna@tntcomputers.ro");
                    //vAdresa1 = new System.Net.Mail.MailAddress(vEmailUser);
                    //vMsg = new System.Net.Mail.MailMessage(vAdresa1, vAdresa);

                    //vMsg.Body = @"" + tbMesaj.Text + "<br/><br/> Acest e-mail a fost trimis de " + vTabel["utilizatorNume"].ToString() + " " + vTabel["utilizatorPrenume"].ToString() + " şi face parte din unitatea " + vTabel["unitateDenumire"].ToString();
                    //vMsg.Subject = "Semnalare erori Registrul Agricol";
                    //vMsg.IsBodyHtml = true;
                    //vClient.Send(vMsg);
                    // trimit la roland
                    if (vJudetId == "27")
                    {
                        vAdresa = new System.Net.Mail.MailAddress("suport-harghita@tntcomputers.ro");
                        vAdresa1 = new System.Net.Mail.MailAddress(vEmailUser);
                        vMsg = new System.Net.Mail.MailMessage(vAdresa1, vAdresa);

                        vMsg.Body = @"" + tbMesaj.Text + "<br/><br/> Acest e-mail a fost trimis de " + vTabel["utilizatorNume"].ToString() + " " + vTabel["utilizatorPrenume"].ToString() + " şi face parte din unitatea " + vTabel["unitateDenumire"].ToString();
                        vMsg.Subject = "Semnalare erori Registrul Agricol - Harghita";
                        vMsg.IsBodyHtml = true;
                        vClient.Send(vMsg);
                    }
                    else
                    {
                        vAdresa = new System.Net.Mail.MailAddress("suport-tehnic@tntcomputers.ro");
                        vAdresa1 = new System.Net.Mail.MailAddress(vEmailUser);
                        vMsg = new System.Net.Mail.MailMessage(vAdresa1, vAdresa);

                        vMsg.Body = @"" + tbMesaj.Text + "<br/><br/> Acest e-mail a fost trimis de " + vTabel["utilizatorNume"].ToString() + " " + vTabel["utilizatorPrenume"].ToString() + " şi face parte din unitatea " + vTabel["unitateDenumire"].ToString();
                        vMsg.Subject = "Semnalare erori Registrul Agricol";
                        vMsg.IsBodyHtml = true;
                        vClient.Send(vMsg);
                    }

                    lblEroare.Visible = true;
                    lblEroare.Text = "Mesajul dumneavoastră a fost trimis cu succes";
                }
                catch 
                {
                    lblEroare.Visible = true;
                    lblEroare.Text = "Mesajul dumneavoastră nu a putut fi trimis. Vă rugăm să mai încercaţi odată";
                }
                ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "semnalareEroare", "semnalare eroare", "", Convert.ToInt64(Session["SESgospodarieId"]), 2);
            }
            vTabel.Close();
            // trimite mesaj catre administratori
            vCmd2.CommandText = "select utilizatorId, unitateId from utilizatori where tipUtilizatorId='1'";
            vTabel = vCmd2.ExecuteReader();
            while (vTabel.Read())
            {
                vCmd1.CommandText = "insert into mesaje (unitateId, utilizatorId, utilizatorIdDeLa, dataAdaugarii, subiect, mesaj) values ('" + Session["SESunitateId"].ToString() + "', '" + vTabel["utilizatorId"].ToString() + "', '" + Session["SESutilizatorId"].ToString() + "', convert(datetime,'" + DateTime.Now + "',104), N'Semnalare erori Registrul Agricol la unitatea " + vUnitateDenumire + "', N'" + tbMesaj.Text + "')";
                vCmd1.CommandText = vCmd1.CommandText.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
                vCmd1.ExecuteNonQuery();
            }
            vTabel.Close();
            tbMesaj.Text = "";
        }
        else
        {
            lblEroare.Visible = true;
            lblEroare.Text = "Vă rugăm sa completaţi câmpul de mai jos";
        }
        ManipuleazaBD.InchideConexiune(vCon);
    }
}
