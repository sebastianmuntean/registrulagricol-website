﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

public partial class adaugaPaginaDrepturi : System.Web.UI.Page
{
    
    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        DBConnections connection = new DBConnections(Convert.ToInt16(Session["SESan"]));

        SqlPagini.ConnectionString = connection.Create();
        SqlTipuriUtilizatori.ConnectionString = connection.Create();
        SqlListaPagini.ConnectionString = connection.Create();
    }
    private void PopuleazaSablon()
    {
        string vInterogare = "";
        int j = gvSabloane.Rows.Count;
        for (int i = 0; i < gvSabloane.Rows.Count; i++)
        {
            if (gvSabloane.Rows[i].RowType == DataControlRowType.DataRow)
            {
                // veificam daca avem inregistrare pentru pagina respectiva
                // si pt tipul de utilizator ales
                Label vPaginaId = (Label)gvSabloane.Rows[i].FindControl("paginaId");
                DropDownList vTipUtilizator = (DropDownList)((DropDownList)ddlTipUtilizator);
                vInterogare = "SELECT COUNT (*) as exista, tipUtilizatorId, paginaId, dreptCitire, dreptModificare, dreptTiparire, dreptCreare, dreptStergere FROM sabloaneDrepturi WHERE paginaId = '" + vPaginaId.Text + "' AND tipUtilizatorId='";
                try { vInterogare += vTipUtilizator.SelectedItem.Value + "' GROUP BY tipUtilizatorId, paginaId, dreptCitire, dreptModificare, dreptTiparire, dreptCreare, dreptStergere"; }
                catch { }
                SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
                SqlCommand vCmd = new SqlCommand();
                vCmd.Connection = vCon;
                vCmd.CommandType = System.Data.CommandType.Text;
                vCmd.CommandText = vInterogare;
                try
                {
                    CheckBox vCbCitire = (CheckBox)gvSabloane.Rows[i].FindControl("cbCitire");
                    CheckBox vCbModificare = (CheckBox)gvSabloane.Rows[i].FindControl("cbModificare");
                    CheckBox vCbTiparire = (CheckBox)gvSabloane.Rows[i].FindControl("cbTiparire");
                    CheckBox vCbCreare = (CheckBox)gvSabloane.Rows[i].FindControl("cbCreare");
                    CheckBox vCbStergere = (CheckBox)gvSabloane.Rows[i].FindControl("cbStergere");
                    SqlDataReader vLinieSabloane = vCmd.ExecuteReader();
                    if (vLinieSabloane.Read())
                    {
                        // daca exista in baza de date populam cu ce exista
                        // daca nu exista populam goale

                        if (Convert.ToInt32(vLinieSabloane["exista"]) > 0)
                        {
                            if (vLinieSabloane["dreptCitire"].ToString() == "0") { vCbCitire.Checked = false; } else { vCbCitire.Checked = true; }
                            if (vLinieSabloane["dreptModificare"].ToString() == "0") { vCbModificare.Checked = false; } else { vCbModificare.Checked = true; }
                            if (vLinieSabloane["dreptStergere"].ToString() == "0") { vCbStergere.Checked = false; } else { vCbStergere.Checked = true; }
                            if (vLinieSabloane["dreptCreare"].ToString() == "0") { vCbCreare.Checked = false; } else { vCbCreare.Checked = true; }
                            if (vLinieSabloane["dreptTiparire"].ToString() == "0") { vCbTiparire.Checked = false; } else { vCbTiparire.Checked = true; }
                        }
                    }
                    else
                    {
                        vCbCitire.Checked = true;
                        vCbModificare.Checked = false;
                        vCbStergere.Checked = false;
                        vCbCreare.Checked = false;
                        vCbTiparire.Checked = false;
                    }
                }
                catch
                {
                }
                vCon.Close();
            }
            else { }
        }
    }
    protected void btSabloane_Salveaza(object sender, EventArgs e)
    {
        string vInterogareUpdate = "";
        string vInterogareInsert = "";
        string vInterogareSelect = "";

        for (int i = 0; i < gvSabloane.Rows.Count; i++)
        {
            vInterogareUpdate = ""; vInterogareInsert = ""; vInterogareSelect = "";
            if (gvSabloane.Rows[i].RowType == DataControlRowType.DataRow)
            {
                // verificam daca exista deja linia
                // citim tipulutilizator si pagina id                    
                CheckBox vCbCitire = (CheckBox)gvSabloane.Rows[i].FindControl("cbCitire");
                CheckBox vCbModificare = (CheckBox)gvSabloane.Rows[i].FindControl("cbModificare");
                CheckBox vCbTiparire = (CheckBox)gvSabloane.Rows[i].FindControl("cbTiparire");
                CheckBox vCbCreare = (CheckBox)gvSabloane.Rows[i].FindControl("cbCreare");
                CheckBox vCbStergere = (CheckBox)gvSabloane.Rows[i].FindControl("cbStergere");
                DropDownList vTipUtilizator = (DropDownList)ddlTipUtilizator;
                Label vPaginaId = (Label)gvSabloane.Rows[i].FindControl("paginaId");
                vInterogareSelect = "SELECT COUNT(*) as exista FROM sabloaneDrepturi WHERE paginaId='" + vPaginaId.Text + "' AND tipUtilizatorId = '" + vTipUtilizator.Text + "'";

                if (clsSabloane.VerificaLinieSablon(vInterogareSelect) == 0)
                {
                    vInterogareInsert = "INSERT INTO sabloaneDrepturi (paginaId, dreptCitire, dreptModificare, dreptTiparire, dreptCreare, dreptStergere,tipUtilizatorId) VALUES ('";
                    vInterogareInsert += vPaginaId.Text.ToString() + "',";

                    if (vCbCitire.Checked == false) { vInterogareInsert += "'0',"; } else { vInterogareInsert += "'1',"; }
                    if (vCbModificare.Checked == false) { vInterogareInsert += "'0',"; } else { vInterogareInsert += "'1',"; }
                    if (vCbTiparire.Checked == false) { vInterogareInsert += "'0',"; } else { vInterogareInsert += "'1',"; }
                    if (vCbCreare.Checked == false) { vInterogareInsert += "'0',"; } else { vInterogareInsert += "'1',"; }
                    if (vCbStergere.Checked == false) { vInterogareInsert += "'0','"; } else { vInterogareInsert += "'1','"; }
                    vInterogareInsert += vTipUtilizator.SelectedItem.Value + "')";
                    ManipuleazaBD.fManipuleazaBD(vInterogareInsert, Convert.ToInt16(Session["SESan"]));
                }
                else
                {
                    vInterogareUpdate = "UPDATE sabloaneDrepturi SET ";
                    if (vCbCitire.Checked == false) { vInterogareUpdate += " dreptCitire = '0' "; } else { vInterogareUpdate += " dreptCitire = '1' "; }
                    if (vCbModificare.Checked == false) { vInterogareUpdate += ", dreptModificare = '0' "; } else { vInterogareUpdate += ", dreptModificare = '1' "; }
                    if (vCbTiparire.Checked == false) { vInterogareUpdate += ", dreptTiparire = '0' "; } else { vInterogareUpdate += ", dreptTiparire = '1' "; }
                    if (vCbCreare.Checked == false) { vInterogareUpdate += ", dreptCreare = '0' "; } else { vInterogareUpdate += ", dreptCreare = '1' "; }
                    if (vCbStergere.Checked == false) { vInterogareUpdate += ", dreptStergere = '0' "; } else { vInterogareUpdate += ", dreptStergere = '1' "; }
                    vInterogareUpdate += " WHERE tipUtilizatorId = '" + vTipUtilizator.SelectedItem.Value + "' AND paginaId = '" + vPaginaId.Text.ToString() + "' ";
                    ManipuleazaBD.fManipuleazaBD(vInterogareUpdate, Convert.ToInt16(Session["SESan"]));
                }
                // salvez sablonul de drepturi pentru toti utilizatorii cu tipul selectat
                SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
                SqlCommand vCmd1 = new SqlCommand();
                SqlCommand vCmd2 = new SqlCommand();
                vCmd1.Connection = vCon;
                vCmd2.Connection = vCon;
                vCmd2.CommandText = "select utilizatorId from utilizatori where tipUtilizatorId='" + ddlTipUtilizator.SelectedValue + "'";
                SqlDataReader vTabel = vCmd2.ExecuteReader();
                int vNr = 0;
                while (vTabel.Read())
                {
                    string vTipOperatie = "";
                    vCmd1.CommandText = "select count(*) from drepturiUtilizatori where utilizatorId='" + vTabel["utilizatorId"].ToString() + "' and paginaId='" + vPaginaId.Text + "'";
                    if (Convert.ToInt32(vCmd1.ExecuteScalar()) == 0)
                        vTipOperatie = "a";
                    else vTipOperatie = "m";
                    if (vTipOperatie == "a")
                    {
                        // adaug un rand cu drepturile pentru pagina curenta
                        vCmd1.CommandText = "INSERT INTO drepturiUtilizatori (utilizatorId, paginaId, dreptCreare, dreptCitire, dreptModificare, dreptStergere, dreptTiparire) VALUES ('" + vTabel["utilizatorId"].ToString() + "', '" + vPaginaId.Text + "', '" + ((vCbCreare.Checked) ? "1" : "0") + "', '" + ((vCbCitire.Checked) ? "1" : "0") + "', '" + ((vCbModificare.Checked) ? "1" : "0") + "', '" + ((vCbStergere.Checked) ? "1" : "0") + "', '" + ((vCbTiparire.Checked) ? "1" : "0") + "')";
                        vCmd1.ExecuteNonQuery();
                    }
                    else
                    {
                        // modific randul din drepturi utilizatori
                        vCmd1.CommandText = "UPDATE drepturiUtilizatori SET dreptCreare ='" + ((vCbCreare.Checked) ? "1" : "0") + "', dreptCitire ='" + ((vCbCitire.Checked) ? "1" : "0") + "', dreptModificare ='" + ((vCbModificare.Checked) ? "1" : "0") + "', dreptStergere ='" + ((vCbStergere.Checked) ? "1" : "0") + "', dreptTiparire ='" + ((vCbTiparire.Checked) ? "1" : "0") + "' WHERE utilizatorId= '" + vTabel["utilizatorId"].ToString() + "' and paginaId= '" + vPaginaId.Text + "'";
                        vCmd1.ExecuteNonQuery();
                    }
                    vNr++;
                }
                valCustom.IsValid = false;
                valCustom.ErrorMessage = "Am adaugat drepturi pentru " + vNr.ToString() + " utilizatori";
                vTabel.Close();
                ManipuleazaBD.InchideConexiune(vCon);
            }
            else { }
        }

    }
    protected void gvSabloane_DataBound(object sender, EventArgs e)
    {
        PopuleazaSablon();
    }
    protected void ddlTipUtilizator_SelectedIndexChanged(object sender, EventArgs e)
    {
        gvSabloane.DataBind();
    }
}
