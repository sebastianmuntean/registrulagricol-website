﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class reparaVolumPozitie : System.Web.UI.Page
{
    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void ddlfUnitati_Init(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select unitateDenumire,unitateId from unitati order by unitateDenumire";
        ddlUnitate.Items.Clear();
        ListItem vItem = new ListItem("-toate-", "%");
        ddlUnitate.Items.Add(vItem);
        SqlDataReader vTabel = vCmd.ExecuteReader();
        while (vTabel.Read())
        {
            vItem = new ListItem(vTabel["unitateDenumire"].ToString(), vTabel["unitateId"].ToString());
            ddlUnitate.Items.Add(vItem);
        }
        vTabel.Close();
        ManipuleazaBD.InchideConexiune(vCon);
        ddlUnitate.SelectedValue = Session["SESunitateId"].ToString();
        ((MasterPage)this.Page.Master).SchimbaGospodaria();
    }
    protected void ddlUnitate_PreRender(object sender, EventArgs e)
    {

    }
    protected void ddlUnitate_SelectedIndexChanged(object sender, EventArgs e)
    {
        HttpCookie vCook = Request.Cookies["COOKTNT"];
        string v1 = "";
        string v2 = "";
        string v3 = "";

        if (vCook != null)
        {
            vCook = CriptareCookie.DecodeCookie(vCook);
            v1 = vCook["COOKan"];
            v2 = vCook["COOKutilizatorId"];
            v3 = vCook["COOKunitateId"];
            vCook = CriptareCookie.EncodeCookie(vCook);
            Response.Cookies.Add(vCook);
            Response.Cookies.Remove("COOKTNT");
        }
        HttpCookie vCook1 = new HttpCookie("COOKTNT");
        vCook1["COOKan"] = v1;
        vCook1["COOKutilizatorId"] = v2;
        vCook1["COOKunitateId"] = v3;
        vCook1 = CriptareCookie.EncodeCookie(vCook1);
        Response.Cookies.Add(vCook1);

        Session["SESunitateId"] = ddlUnitate.SelectedValue;
        //   Session["SESgospodarieId"] = "NULA";
        Session["SESgospodarieId"] = null;

        ((MasterPage)this.Page.Master).SchimbaGospodaria();
        gvGospodarii.SelectedIndex = -1;
    }
    protected void ddlFOrdonareDupa_SelectedIndexChanged(object sender, EventArgs e)
    {
        HttpCookie vCook = Request.Cookies["COOKTNT"];

        if (vCook != null)
        {
            Session["SESordonareGospodarii"] = ddlFOrdonareDupa.SelectedValue;
            vCook = CriptareCookie.DecodeCookie(vCook);
            vCook["COOKordonareGospodarii"] = ddlFOrdonareDupa.SelectedValue;
            vCook = CriptareCookie.EncodeCookie(vCook);
            Response.Cookies.Add(vCook);
        }

        if (ddlFOrdonareDupa.SelectedValue == "0")
            gvGospodarii.DataSourceID = "SqlGospodarii";
        else if (ddlFOrdonareDupa.SelectedValue == "1")
            gvGospodarii.DataSourceID = "SqlGospodariiLocStrNr";
        else if (ddlFOrdonareDupa.SelectedValue == "2")
            gvGospodarii.DataSourceID = "SqlGospodariiNume";
        else if (ddlFOrdonareDupa.SelectedValue == "3")
            gvGospodarii.DataSourceID = "SqlGospodariiIdCresc";
        else if (ddlFOrdonareDupa.SelectedValue == "4")
            gvGospodarii.DataSourceID = "SqlGospodariiIdDesc";

        gvGospodarii.DataBind();
    }
    protected void ddlFOrdonareDupa_Init(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        ddlFOrdonareDupa.SelectedValue = Session["SESordonareGospodarii"].ToString();
    }

    protected void tbVolum_TextChanged(object sender, EventArgs e)
    {
        Label vLblGospodarieId = (Label)((TextBox)sender).Parent.FindControl("lblGospodarieId");
        TextBox vTbVolum = (TextBox)sender;
        TextBox vTbPozitie = (TextBox)((TextBox)sender).Parent.FindControl("tbPozitie");
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        // verific daca exista o gospodarie la volumul si numarul introdus
        if (clsUnitati.ValideazaVolumSiPozitie(Convert.ToInt32(Session["SESunitateId"]), Convert.ToInt16(Session["SESan"])))
        {
            vCmd.CommandText = "select count(*) from gospodarii where unitateId='" + Session["SESunitateId"].ToString() + "' and volum='" + vTbVolum.Text + "' and nrPozitie='" + vTbPozitie.Text + "' AND an='" + Session["SESan"].ToString() + "'";
            int vValidareVolum = Convert.ToInt32(vCmd.ExecuteScalar());
            if (vValidareVolum > 0)
            {
                lblEroare1.Visible = true; lblEroare1.Text = "La volumul " + vTbVolum.Text + ", poziţia " + vTbPozitie.Text + " există deja o gospodărie inregistrată";
                gvGospodarii.DataBind();
                return;
            }
            else
            {
                lblEroare1.Visible = false;
                string pozitieVeche = clsUnitati.GetPozitieVeche(Convert.ToInt64(vLblGospodarieId.Text), Convert.ToInt16(Session["SESan"]));
                string volumVechi = clsUnitati.GetVolumVechi(Convert.ToInt64(vLblGospodarieId.Text), Convert.ToInt16(Session["SESan"]));
                vCmd.CommandText = "update gospodarii set volum='" + vTbVolum.Text + "', volumInt ='" + ScoateNumere(vTbVolum.Text) + "',volumVechi = '" + volumVechi + "',pozitieVeche = '"+pozitieVeche+"' where gospodarieId='" + vLblGospodarieId.Text + "'";
                vCmd.ExecuteNonQuery();
                ManipuleazaBD.InchideConexiune(vCon);
                gvGospodarii.DataBind();
            }
        }
        else
        {
            string pozitieVeche = clsUnitati.GetPozitieVeche(Convert.ToInt64(vLblGospodarieId.Text), Convert.ToInt16(Session["SESan"]));
            string volumVechi = clsUnitati.GetVolumVechi(Convert.ToInt64(vLblGospodarieId.Text), Convert.ToInt16(Session["SESan"]));
            vCmd.CommandText = "update gospodarii set volum='" + vTbVolum.Text + "', volumInt ='" + ScoateNumere(vTbVolum.Text) + "',volumVechi = '" + volumVechi + "',pozitieVeche = '" + pozitieVeche + "' where gospodarieId='" + vLblGospodarieId.Text + "'";
            vCmd.ExecuteNonQuery();
            ManipuleazaBD.InchideConexiune(vCon);
            gvGospodarii.DataBind();
        }
    }
    protected void tbPozitie_TextChanged(object sender, EventArgs e)
    {
        Label vLblGospodarieId = (Label)((TextBox)sender).Parent.FindControl("lblGospodarieId");
        TextBox vTbPozitie = (TextBox)sender;
        TextBox vTbVolum = (TextBox)((TextBox)sender).Parent.FindControl("tbVolum");
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        // verific daca exista o gospodarie la volumul si numarul introdus
        if (clsUnitati.ValideazaVolumSiPozitie(Convert.ToInt32(Session["SESunitateId"]), Convert.ToInt16(Session["SESan"])))
        {
            vCmd.CommandText = "select count(*) from gospodarii where unitateId='" + Session["SESunitateId"].ToString() + "' and volum='" + vTbVolum.Text + "' and nrPozitie='" + vTbPozitie.Text + "' AND an='" + Session["SESan"].ToString() + "'";
            int vValidareVolum = Convert.ToInt32(vCmd.ExecuteScalar());
            if (vValidareVolum > 0)
            { lblEroare1.Visible = true; lblEroare1.Text = "La volumul " + vTbVolum.Text + ", poziţia " + vTbPozitie.Text + " există deja o gospodărie inregistrată"; gvGospodarii.DataBind(); return; }
            else
            {
                lblEroare1.Visible = false;
                string pozitieVeche = clsUnitati.GetPozitieVeche(Convert.ToInt64(vLblGospodarieId.Text), Convert.ToInt16(Session["SESan"]));
                string volumVechi = clsUnitati.GetVolumVechi(Convert.ToInt64(vLblGospodarieId.Text), Convert.ToInt16(Session["SESan"]));
                vCmd.CommandText = "update gospodarii set nrPozitie='" + vTbPozitie.Text + "', nrPozitieInt = '" + ScoateNumere(vTbPozitie.Text) + "',pozitieVeche='" + pozitieVeche + "',volumVechi = '" + volumVechi + "' where  gospodarieId='" + vLblGospodarieId.Text + "'";
                vCmd.ExecuteNonQuery();
                ManipuleazaBD.InchideConexiune(vCon);
                gvGospodarii.DataBind();
            }
        }
        else
        {
            string pozitieVeche = clsUnitati.GetPozitieVeche(Convert.ToInt64(vLblGospodarieId.Text), Convert.ToInt16(Session["SESan"]));
            string volumVechi = clsUnitati.GetVolumVechi(Convert.ToInt64(vLblGospodarieId.Text), Convert.ToInt16(Session["SESan"]));
            vCmd.CommandText = "update gospodarii set nrPozitie='" + vTbPozitie.Text + "', nrPozitieInt = '" + ScoateNumere(vTbPozitie.Text) + "',pozitieVeche='" + pozitieVeche + "',volumVechi = '"+volumVechi+"' where  gospodarieId='" + vLblGospodarieId.Text + "'";
            vCmd.ExecuteNonQuery();
            ManipuleazaBD.InchideConexiune(vCon);
            gvGospodarii.DataBind();
        }
    }
    protected string ScoateNumere(string pScoateNumar)
    {
        switch (pScoateNumar)
        {
            default:
                break;
            case "I":
                pScoateNumar = "1";
                break;
            case "II":
                pScoateNumar = "2";
                break;
            case "III":
                pScoateNumar = "3";
                break;
            case "IV":
                pScoateNumar = "4";
                break;
            case "V":
                pScoateNumar = "5";
                break;
            case "VI":
                pScoateNumar = "6";
                break;
            case "VII":
                pScoateNumar = "7";
                break;
            case "VIII":
                pScoateNumar = "8";
                break;
            case "IX":
                pScoateNumar = "9";
                break;
            case "X":
                pScoateNumar = "10";
                break;
            case "XI":
                pScoateNumar = "11";
                break;
            case "XII":
                pScoateNumar = "12";
                break;
            case "XIII":
                pScoateNumar = "13";
                break;
            case "XIV":
                pScoateNumar = "14";
                break;
            case "XV":
                pScoateNumar = "15";
                break;
            case "XVI":
                pScoateNumar = "16";
                break;
            case "XVII":
                pScoateNumar = "17";
                break;
            case "XVIII":
                pScoateNumar = "18";
                break;
            case "XIX":
                pScoateNumar = "19";
                break;
            case "XX":
                pScoateNumar = "20";
                break;
            case "XXI":
                pScoateNumar = "21";
                break;
            case "XXII":
                pScoateNumar = "22";
                break;
            case "XXIII":
                pScoateNumar = "23";
                break;
            case "XXIV":
                pScoateNumar = "24";
                break;
            case "XXV":
                pScoateNumar = "25";
                break;
            case "XXVI":
                pScoateNumar = "26";
                break;
            case "XXVII":
                pScoateNumar = "27";
                break;
            case "XXVIII":
                pScoateNumar = "28";
                break;
            case "XXIX":
                pScoateNumar = "29";
                break;
            case "XXX":
                pScoateNumar = "30";
                break;
            case "XXXI":
                pScoateNumar = "31";
                break;
            case "XXXII":
                pScoateNumar = "32";
                break;
            case "XXXIII":
                pScoateNumar = "33";
                break;
            case "XXXIV":
                pScoateNumar = "34";
                break;
            case "XXXV":
                pScoateNumar = "35";
                break;
            case "XXXVI":
                pScoateNumar = "36";
                break;
            case "XXXVII":
                pScoateNumar = "37";
                break;
            case "XXXVIII":
                pScoateNumar = "38";
                break;
            case "XXXIX":
                pScoateNumar = "39";
                break;
            case "XL":
                pScoateNumar = "40";
                break;
            case "XLI":
                pScoateNumar = "41";
                break;
            case "XLII":
                pScoateNumar = "42";
                break;
            case "XLIII":
                pScoateNumar = "43";
                break;
            case "XLIV":
                pScoateNumar = "44";
                break;
            case "XLV":
                pScoateNumar = "45";
                break;
            case "XLVI":
                pScoateNumar = "46";
                break;
            case "XLVII":
                pScoateNumar = "47";
                break;
            case "XLVIII":
                pScoateNumar = "48";
                break;
            case "XLIX":
                pScoateNumar = "49";
                break;
            case "L":
                pScoateNumar = "50";
                break;
            case "LI":
                pScoateNumar = "51";
                break;
            case "LII":
                pScoateNumar = "52";
                break;
            case "LIII":
                pScoateNumar = "53";
                break;
            case "LIV":
                pScoateNumar = "54";
                break;
            case "LV":
                pScoateNumar = "55";
                break;
            case "LVI":
                pScoateNumar = "56";
                break;
            case "LVII":
                pScoateNumar = "57";
                break;
            case "LVIII":
                pScoateNumar = "58";
                break;
            case "LIX":
                pScoateNumar = "59";
                break;
            case "LX":
                pScoateNumar = "60";
                break;
            case "LXI":
                pScoateNumar = "61";
                break;
            case "LXII":
                pScoateNumar = "62";
                break;
            case "LXIII":
                pScoateNumar = "63";
                break;
            case "LXIV":
                pScoateNumar = "64";
                break;
            case "LXV":
                pScoateNumar = "65";
                break;
            case "LXVI":
                pScoateNumar = "66";
                break;
            case "LXVII":
                pScoateNumar = "67";
                break;
            case "LXVIII":
                pScoateNumar = "68";
                break;
            case "LXIX":
                pScoateNumar = "69";
                break;
            case "LXX":
                pScoateNumar = "70";
                break;
            case "LXXI":
                pScoateNumar = "71";
                break;
            case "LXXII":
                pScoateNumar = "72";
                break;
            case "LXXIII":
                pScoateNumar = "73";
                break;
            case "LXXIV":
                pScoateNumar = "74";
                break;
            case "LXXV":
                pScoateNumar = "75";
                break;
            case "LXXVI":
                pScoateNumar = "76";
                break;
            case "LXXVII":
                pScoateNumar = "77";
                break;
            case "LXXVIII":
                pScoateNumar = "78";
                break;
            case "LXXIX":
                pScoateNumar = "79";
                break;
            case "LXXX":
                pScoateNumar = "80";
                break;
            case "LXXXI":
                pScoateNumar = "81";
                break;
            case "LXXXII":
                pScoateNumar = "82";
                break;
            case "LXXXIII":
                pScoateNumar = "83";
                break;
            case "LXXXIV":
                pScoateNumar = "84";
                break;
            case "LXXXV":
                pScoateNumar = "85";
                break;
            case "LXXXVI":
                pScoateNumar = "86";
                break;
            case "LXXXVII":
                pScoateNumar = "87";
                break;
            case "LXXXVIII":
                pScoateNumar = "88";
                break;
            case "LXXXIX":
                pScoateNumar = "89";
                break;
            case "XC":
                pScoateNumar = "90";
                break;
            case "XCI":
                pScoateNumar = "91";
                break;
            case "XCII":
                pScoateNumar = "92";
                break;
            case "XCIII":
                pScoateNumar = "93";
                break;
            case "XCIV":
                pScoateNumar = "94";
                break;
            case "XCV":
                pScoateNumar = "95";
                break;
            case "XCVI":
                pScoateNumar = "96";
                break;
            case "XCVII":
                pScoateNumar = "97";
                break;
            case "XCVIII":
                pScoateNumar = "98";
                break;
            case "XCIX":
                pScoateNumar = "99";
                break;
            case "C":
                pScoateNumar = "100";
                break;
        }
        int vContor = 0;
        int vLungime = pScoateNumar.Length;
        string vNumar = "";
        while (vContor < vLungime)
        {
            string vCaracter = pScoateNumar[vContor].ToString();
            try
            {
                vNumar += Convert.ToInt16(vCaracter).ToString();
            }
            catch { }
            vContor++;
        }
        try
        {
            vNumar = Convert.ToInt16(vNumar).ToString();
        }
        catch { vNumar = "0"; }
        return vNumar; ;
    }

  

}
