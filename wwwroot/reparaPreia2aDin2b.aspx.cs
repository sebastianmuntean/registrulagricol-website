﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class reparaPreia2aDin2b : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    protected void btAdauga_Click(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select * from gospodarii where unitateid='" + ddlUnitate.SelectedValue + "'";
        SqlDataReader vTabel = vCmd.ExecuteReader();
        while (vTabel.Read())
        {
            CalculCapitole.AdaugaCapitol2ADin2B(ddlUnitate.SelectedValue, Session["SESan"].ToString(), vTabel["gospodarieId"].ToString(), "1");
            clsCorelatii.VerificaCorelatie(Convert.ToInt32(ddlUnitate.SelectedValue), Convert.ToInt32(vTabel["gospodarieId"]), Convert.ToInt32(Session["SESan"]), "parcele");
        }
        vTabel.Close();
        ManipuleazaBD.InchideConexiune(vCon);
        valCustom.IsValid = false;
        valCustom.ErrorMessage = "Preluarea din capitolul 2b in capitolul 2a s-a finalizat cu succes !";
    }
}
