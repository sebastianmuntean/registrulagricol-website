﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="raportListeCentralizate.aspx.cs" Inherits="raportListeCentralizate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Rapoarte / Liste centralizate" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="pnGeneral" runat="server" CssClass="panel_general">
        <asp:Panel ID="pnFiltrareUnitati" runat="server" CssClass="cauta">
            <asp:Label ID="lblFJudet" runat="server" Text="Judeţ"></asp:Label>
            <asp:DropDownList ID="ddlFJudet" AutoPostBack="true" runat="server" OnPreRender="ddlFJudet_PreRender"
                OnSelectedIndexChanged="ddlFJudet_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:Label ID="lblUnitate" runat="server" Text="unitatea:"></asp:Label>
            <asp:DropDownList ID="ddlUnitate" runat="server" OnInit="ddlUnitate_Init">
            </asp:DropDownList>
        </asp:Panel>
        <asp:Panel ID="pnFiltrareTipRaport" runat="server" CssClass="cauta">
            <asp:Label ID="lblTipRaport" runat="server" Text="Tip raport"></asp:Label>
            <asp:DropDownList ID="ddlTipRaport" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlTipRaport_SelectedIndexChanged">
                <asp:ListItem Text="Listă animale" Value="7"></asp:ListItem>
                <asp:ListItem Text="Listă terenuri" Value="2a"></asp:ListItem>
                <asp:ListItem Text="Listă culturi" Value="4a"></asp:ListItem>
                <asp:ListItem Text="Listă utilaje" Value="9"></asp:ListItem>
            </asp:DropDownList>
            <asp:Label ID="lblCodRand" runat="server" Text=""></asp:Label>
            <asp:DropDownList ID="ddlCodRand" runat="server">
            </asp:DropDownList>
        </asp:Panel>
        <asp:Panel ID="pnInterval" runat="server" CssClass="cauta">
            <asp:DropDownList ID="ddlSemestru" runat="server">
                <asp:ListItem Text="Semestrul 1" Value="1"></asp:ListItem>
                <asp:ListItem Text="Semestrul 2" Value="2"></asp:ListItem>
            </asp:DropDownList>
            <asp:Label ID="lblBuc1" runat="server" Text="Minim (buc/ha)"></asp:Label>
            <asp:TextBox ID="tbBuc1" Width="50px" runat="server" Text="0"></asp:TextBox>
            <asp:Label ID="lblBuc2" runat="server" Text="Maxim (buc/ha)"></asp:Label>
            <asp:TextBox ID="tbBuc2" Width="50px" runat="server" Text="9999999"></asp:TextBox>
        </asp:Panel>
        <asp:Panel ID="pnFiltrare" runat="server" CssClass="cauta">
            <asp:Label ID="lblFiltrareDupa" runat="server" Text="Caută după:"></asp:Label>
            <asp:Label ID="lblVolum" runat="server" Text="vol."></asp:Label>
            <asp:TextBox ID="tbVolum" runat="server" AutoPostBack="False" Width="30px"></asp:TextBox>
            <asp:Label ID="lblNrPozitie" runat="server" Text="Nr. pozitie"></asp:Label>
            <asp:TextBox ID="tbNrPozitie" Width="30px" runat="server"></asp:TextBox>
            <asp:Label ID="lblDeLaNr" runat="server" Text="de la nr."></asp:Label>
            <asp:TextBox ID="tbDeLaNr" Text="0" runat="server" AutoPostBack="False" Width="40px"></asp:TextBox>
            <asp:Label ID="lblLaNr" runat="server" Text="la"></asp:Label>
            <asp:TextBox ID="tbLaNr" Text="9999999" runat="server" AutoPostBack="False" Width="40px"></asp:TextBox>
            <asp:Label ID="lblStrainas" runat="server" Text="strainaș:"></asp:Label>
            <asp:DropDownList ID="ddlStrainas" runat="server" AutoPostBack="False">
                <asp:ListItem Value="%">toți</asp:ListItem>
                <asp:ListItem Value="1">da</asp:ListItem>
                <asp:ListItem Value="0">nu</asp:ListItem>
            </asp:DropDownList>
            <asp:Label ID="lblStrada" runat="server" Text="str."></asp:Label>
            <asp:TextBox ID="tbStrada" runat="server" AutoPostBack="False" Width="100px"></asp:TextBox>
            <asp:Label ID="lblLocalitate" runat="server" Text="loc."></asp:Label>
            <asp:TextBox ID="tbLocalitate" runat="server" AutoPostBack="False" Width="100px"></asp:TextBox>
            <asp:Label ID="lblTipPers1" runat="server" Text="pers.jur."></asp:Label>
            <asp:DropDownList ID="ddlPersJuridica" runat="server" AutoPostBack="False">
                <asp:ListItem Value="%">toți</asp:ListItem>
                <asp:ListItem Value="1">da</asp:ListItem>
                <asp:ListItem Value="0">nu</asp:ListItem>
            </asp:DropDownList>
        </asp:Panel>
        <asp:Panel ID="pnOrdonare" runat="server" CssClass="cauta">
            <asp:Label ID="lblOrdonareDupa" runat="server" Text="Ordonează după:"></asp:Label>
            <asp:DropDownList ID="ddlOrdonareDupa" runat="server" AutoPostBack="False">
                <asp:ListItem Value="0">nume</asp:ListItem>
                <asp:ListItem Value="1">județ + localitate + strada + nr.</asp:ListItem>
                <asp:ListItem Value="2">volum + nr. poziție</asp:ListItem>
            </asp:DropDownList>
        </asp:Panel>
        <asp:Panel ID="pnButoane" runat="server" CssClass="butoane">
            <asp:Button ID="btTiparire" runat="server" Text="tipărește" CssClass="buton" OnClick="btnFiltrare_Click" />
        </asp:Panel>
        <asp:Panel ID="pnRaport" runat="server" CssClass="panel_general">
        </asp:Panel>
    </asp:Panel>
</asp:Content>
