﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="raportNomenclaturaStradala.aspx.cs" Inherits="raportNomenclaturaStradala" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Rapoarte / Lista gospodării cu membri" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="pnGeneral" runat="server" CssClass="panel_general">
        <asp:Panel ID="pnFiltrareUnitati" runat="server" CssClass="cauta">
            <asp:Label ID="lblFJudet" runat="server" Text="Judeţ"></asp:Label>
            <asp:DropDownList ID="ddlFJudet" AutoPostBack="true" runat="server" OnPreRender="ddlFJudet_PreRender"
                OnSelectedIndexChanged="ddlFJudet_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:Label ID="lblUnitate" runat="server" Text="Unitatea:"></asp:Label>
            <asp:DropDownList ID="ddlUnitate" runat="server" OnInit="ddlUnitate_Init">
            </asp:DropDownList>
        </asp:Panel>
        <asp:Panel ID="pnFiltrare" runat="server" CssClass="cauta">
            <asp:Label ID="lblFiltrareDupa" runat="server" Text="Caută după:"></asp:Label>
            <asp:Label ID="lblVolum" runat="server" Text="vol."></asp:Label>
            <asp:TextBox ID="tbVolum" runat="server" AutoPostBack="False" Width="30px"></asp:TextBox>
            <asp:Label ID="lblNrPozitie" runat="server" Text="vol."></asp:Label>
            <asp:TextBox ID="tbNrPozitie" runat="server" AutoPostBack="False" Width="30px"></asp:TextBox>
            <asp:Label ID="lblNr" runat="server" Text="nr."></asp:Label>
            <asp:TextBox ID="tbNr" runat="server" AutoPostBack="False" Width="40px"></asp:TextBox>
            <asp:Label ID="lblNume" runat="server" Text="Nume"></asp:Label>
            <asp:TextBox ID="tbNume" runat="server" AutoPostBack="False" Width="100px"></asp:TextBox>
            <asp:Label ID="lblTip" runat="server" Text="Tip"></asp:Label>
            <asp:DropDownList ID="ddlTip" runat="server" AutoPostBack="False">
                <asp:ListItem Value="%">toți</asp:ListItem>
                <asp:ListItem Value="1">Localnic</asp:ListItem>
                <asp:ListItem Value="2">Străinaş</asp:ListItem>
                <asp:ListItem Value="3">Firmă pe raza localităţii</asp:ListItem>
                <asp:ListItem Value="4">Firmă străinaşă</asp:ListItem>
            </asp:DropDownList>
        </asp:Panel>
        <asp:Panel ID="pnFiltrare1" runat="server" CssClass="cauta">
            <asp:Label ID="lblStrada" runat="server" Text="str."></asp:Label>
            <asp:TextBox ID="tbStrada" runat="server" AutoPostBack="False" Width="100px"></asp:TextBox>
            <asp:Label ID="lblLocalitate" runat="server" Text="loc."></asp:Label>
            <asp:TextBox ID="tbLocalitate" runat="server" AutoPostBack="False" Width="100px"></asp:TextBox>
            <asp:Label ID="lblOrdonare" runat="server" Text="Ordonare"></asp:Label>
            <asp:DropDownList ID="ddlOrdonare" runat="server">
                <asp:ListItem Value="1" Text="Volum, poziţie"></asp:ListItem>
                <asp:ListItem Value="2" Text="Loc., strada, nr."></asp:ListItem>
                <asp:ListItem Value="3" Text="Nume"></asp:ListItem>
                <asp:ListItem Value="4" Text="ID crescător"></asp:ListItem>
                <asp:ListItem Value="5" Text="ID descrescător"></asp:ListItem>
            </asp:DropDownList>
        </asp:Panel>
        <asp:Panel ID="pnButoane" runat="server" CssClass="butoane">
            <asp:Button ID="btTiparire" runat="server" Text="afişare" CssClass="buton" OnClick="btnFiltrare_Click" />
        </asp:Panel>
        <asp:Panel ID="pnRaport" runat="server" CssClass="panel_general">
            <rsweb:ReportViewer ID="rvNomenclaturaStradala" runat="server" Font-Names="Verdana"
                Font-Size="8pt" Height="600px" Width="100%">
                <LocalReport ReportPath="rapoarte\raportNomenclaturaStradala.rdlc">
                    <DataSources>
                        <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="dsRapoarte_dtUnitati" />
                        <rsweb:ReportDataSource DataSourceId="ObjectDataSource2" Name="dsRapoarte_dtListaGospodarii" />
                    </DataSources>
                </LocalReport>
            </rsweb:ReportViewer>
            <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetData" TypeName="dsRapoarteTableAdapters.dtListaGospodariiTableAdapter">
                <SelectParameters>
                    <asp:SessionParameter Name="an" SessionField="SESan" Type="Int32" />
                    <asp:ControlParameter ControlID="tbVolum" DefaultValue="%" Name="volum" PropertyName="Text"
                        Type="String" />
                    <asp:ControlParameter ControlID="tbNrPozitie" DefaultValue="%" Name="nrPozitie" PropertyName="Text"
                        Type="String" />
                    <asp:ControlParameter ControlID="ddlUnitate" DefaultValue="" Name="unitateId" PropertyName="SelectedValue"
                        Type="String" />
                    <asp:ControlParameter ControlID="ddlTip" Name="tip" PropertyName="SelectedValue"
                        Type="String" />
                    <asp:ControlParameter ControlID="tbLocalitate" DefaultValue="%" Name="localitate"
                        PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="tbStrada" DefaultValue="%" Name="strada" PropertyName="Text"
                        Type="String" />
                    <asp:ControlParameter ControlID="tbNr" DefaultValue="%" Name="nr" PropertyName="Text"
                        Type="String" />
                    <asp:ControlParameter ControlID="tbNume" DefaultValue="%" Name="nume" PropertyName="Text"
                        Type="String" />
                    <asp:ControlParameter ControlID="ddlFJudet" DefaultValue="" Name="judetId" PropertyName="SelectedValue"
                        Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetData" TypeName="dsRapoarteTableAdapters.dtUnitatiTableAdapter">
                <SelectParameters>
                    <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </asp:Panel>
    </asp:Panel>
</asp:Content>
