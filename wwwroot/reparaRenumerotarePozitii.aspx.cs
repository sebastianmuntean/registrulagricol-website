﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class reparaRenumerotarePozitii : System.Web.UI.Page
{
    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void ddlFJudet_PreRender(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select judetDenumire,judetId from judete order by judetDenumire";
        ddlFJudet.Items.Clear();
        ListItem vItem = new ListItem("-toate-", "%");
        ddlFJudet.Items.Add(vItem);
        SqlDataReader vTabel = vCmd.ExecuteReader();
        while (vTabel.Read())
        {
            vItem = new ListItem(vTabel["judetDenumire"].ToString(), vTabel["judetId"].ToString());
            ddlFJudet.Items.Add(vItem);
        }
        vTabel.Close();
        vCmd.CommandText = "select top(1) coalesce(judetId,0) from unitati where convert(nvarchar,unitateId)='" + Session["SESunitateId"].ToString() + "'";
        if (ViewState["judetId"] == null)
            try { ViewState["judetId"] = vCmd.ExecuteScalar().ToString(); }
            catch { ViewState["judetId"] = "%"; }
        vCmd.CommandText = "select unitateDenumire,unitateId from unitati where convert(nvarchar,judetId) like '" + ViewState["judetId"].ToString() + "' order by unitateDenumire";
        ddlUnitate.Items.Clear();
        vItem = new ListItem("-toate-", "%");
        ddlUnitate.Items.Add(vItem);
        vTabel = vCmd.ExecuteReader();
        while (vTabel.Read())
        {
            vItem = new ListItem(vTabel["unitateDenumire"].ToString(), vTabel["unitateId"].ToString());
            ddlUnitate.Items.Add(vItem);
        }
        vTabel.Close();
        ManipuleazaBD.InchideConexiune(vCon);
        try
        {
            ddlUnitate.SelectedValue = Session["SESunitateId"].ToString();
        }
        catch { ddlUnitate.SelectedValue = "%"; }
        ddlFJudet.SelectedValue = ViewState["judetId"].ToString();
        ((MasterPage)this.Page.Master).SchimbaGospodaria();
    }
    protected void ddlFJudet_SelectedIndexChanged(object sender, EventArgs e)
    {
        ViewState["judetId"] = ddlFJudet.SelectedValue;
        HttpCookie vCook = Request.Cookies["COOKTNT"];
        if (vCook != null)
        {
            Session["SESjudetId"] = ddlFJudet.SelectedValue; ;
            vCook = CriptareCookie.DecodeCookie(vCook);
            vCook["COOKjudetId"] = ddlFJudet.SelectedValue;
            vCook = CriptareCookie.EncodeCookie(vCook);
            Response.Cookies.Add(vCook);
        }
    }
    protected void ddlUnitate_PreRender(object sender, EventArgs e)
    {

    }
    protected void ddlUnitate_SelectedIndexChanged(object sender, EventArgs e)
    {
        HttpCookie vCook = Request.Cookies["COOKTNT"];
        string v1 = "";
        string v2 = "";
        string v3 = "";

        if (vCook != null)
        {
            vCook = CriptareCookie.DecodeCookie(vCook);
            v1 = vCook["COOKan"];
            v2 = vCook["COOKutilizatorId"];
            v3 = vCook["COOKunitateId"];
            vCook = CriptareCookie.EncodeCookie(vCook);
            Response.Cookies.Add(vCook);
            Response.Cookies.Remove("COOKTNT");
        }
        HttpCookie vCook1 = new HttpCookie("COOKTNT");
        vCook1["COOKan"] = v1;
        vCook1["COOKutilizatorId"] = v2;
        vCook1["COOKunitateId"] = v3;
        vCook1 = CriptareCookie.EncodeCookie(vCook1);
        Response.Cookies.Add(vCook1);
        if (ddlUnitate.SelectedValue == "%")
            Session["SESunitateId"] = 0;
        else Session["SESunitateId"] = ddlUnitate.SelectedValue;
        //   Session["SESgospodarieId"] = "NULA";
        Session["SESgospodarieId"] = null;
    }
    protected void ddlfUnitati_Init(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select top(1) tipUtilizatorId from utilizatori where utilizatorId='" + Session["SESutilizatorId"].ToString() + "'";
        int vTipUtilizator = Convert.ToInt32(vCmd.ExecuteScalar());
        if (vTipUtilizator == 2 || vTipUtilizator == 3)
        {
            vCmd.CommandText = "SELECT TOP (1) unitati.judetId FROM utilizatori INNER JOIN unitati ON utilizatori.unitateId = unitati.unitateId WHERE (utilizatori.utilizatorId = '" + Session["SESutilizatorId"].ToString() + "')";
            string vJudetId = vCmd.ExecuteScalar().ToString();
            ddlFJudet.Items.Clear();
            ddlFJudet.Items.Add(new ListItem("", vJudetId));
            vCmd.CommandText = "select unitateDenumire,unitateId from unitati where judetId='" + vJudetId + "' order by unitateDenumire";
            ddlUnitate.Items.Clear();
            ListItem vItem = new ListItem("-toate-", "%");
            ddlUnitate.Items.Add(vItem);
            SqlDataReader vTabel = vCmd.ExecuteReader();
            while (vTabel.Read())
            {
                vItem = new ListItem(vTabel["unitateDenumire"].ToString(), vTabel["unitateId"].ToString());
                ddlUnitate.Items.Add(vItem);
            }
            vTabel.Close();
            ddlUnitate.SelectedValue = Session["SESunitateId"].ToString();
            ((MasterPage)this.Page.Master).SchimbaGospodaria();
        }
        ManipuleazaBD.InchideConexiune(vCon);
    }
    protected void btRenumerotare_Click(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd1 = new SqlCommand();
        SqlCommand vCmd2 = new SqlCommand();
        vCmd1.Connection = vCon;
        vCmd2.Connection = vCon;
        int vVolum = Convert.ToInt32(tbVolumDeLa.Text), vPozitie = Convert.ToInt32(tbPozitieDeLa.Text);
        int vNr=0;
        for (int a = 0; a < gvLocalitati.Rows.Count; a++)
        {
            DropDownList vDdlLocalitate=(DropDownList)gvLocalitati.Rows[a].FindControl("ddlLocalitate");
            vCmd2.CommandText = "select * from gospodarii where unitateId='" + ddlUnitate.SelectedValue + "' and convert(nvarchar,tip) like '" + ddlTip.SelectedValue + "' AND an='" + Session["SESan"].ToString() + "' and localitate='" + vDdlLocalitate.SelectedValue + "'";
            SqlDataReader vTabel = vCmd2.ExecuteReader();
            while (vTabel.Read())
            {
                // modific volum si pozitie
                vCmd1.CommandText = "update gospodarii set volum='" + vVolum + "', nrPozitie='" + vPozitie + "' where gospodarieId='" + vTabel["gospodarieId"].ToString() + "'";
                vCmd1.ExecuteNonQuery();
                if (vPozitie == Convert.ToInt32(tbPozitiiVolum.Text))
                {
                    vVolum++;
                    vPozitie = 1;
                }
                else vPozitie++;
                vNr++;
            }
            vTabel.Close();
        }
        valCustom.IsValid = false;
        valCustom.ErrorMessage = "Am renumerotat " + vNr + " gospodarii. Ultima pozitie modificata: volum " + vVolum + " pozitia " + vPozitie;
        ManipuleazaBD.InchideConexiune(vCon);
    }
}
