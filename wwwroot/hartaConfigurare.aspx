﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="hartaConfigurare.aspx.cs" Inherits="hartaConfigurare" EnableEventValidation="false" %>

<%@ Register Assembly="WebEdition" Namespace="ThinkGeo.MapSuite.WebEdition" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div style="position: absolute; left: 150px; top: 85px; width: 800px; z-index: 1000;">
        <asp:UpdatePanel ID="upErori" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnAfiseazaMesaje" runat="server">
                    <asp:ValidationSummary ID="valEroareSummary" runat="server" DisplayMode="SingleParagraph"
                        Visible="true" CssClass="validator" ForeColor="" />
                </asp:Panel>
                <asp:Panel ID="Panel5" CssClass="ascunsa" runat="server">
                    <!-- //////////// VALIDATOARE  \\\\\\\\\\\\\\\\\\ -->
                    <asp:CustomValidator ID="valEroare" runat="server" ErrorMessage="Eroare"></asp:CustomValidator>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div style="position: absolute; left: 150px; top: 85px; width: 800px;">
        <asp:UpdatePanel ID="upHarta" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <cc1:Map ID="harta" runat="server" Height="600" Width="800">
                </cc1:Map>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="lbLeagaLinii" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="lbSalvareParcela" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="lbVizualizareSelectie" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <div style="position: absolute; left: 10px; top: 80px; width: 130px; z-index: 1000;">
        <h1 style="text-align: left">
            Opţiuni</h1>
        <p>
            <asp:ImageButton ID="buttonNormal" runat="server" ToolTip="Normal Mode" ImageUrl="~/images/harta/Cursor.png"
                OnClientClick="ctl00_ContentPlaceHolder1_harta.SetDrawMode('Normal');return false;" />
            <asp:ImageButton ID="buttonDrawPoint" runat="server" ToolTip="Draw Point" ImageUrl="~/images/harta/point28.png"
                OnClientClick="ctl00_ContentPlaceHolder1_harta.SetDrawMode('Point');return false;" />
            <asp:ImageButton ID="buttonDrawLine" runat="server" ToolTip="Draw Line" ImageUrl="~/images/harta/line28.png"
                OnClientClick="ctl00_ContentPlaceHolder1_harta.SetDrawMode('Line');return false;" />
            <asp:ImageButton ID="buttonDrawRectangle" runat="server" ToolTip="Draw Rectangle"
                ImageUrl="~/images/harta/rectangle28.png" OnClientClick="ctl00_ContentPlaceHolder1_harta.SetDrawMode('Rectangle');return false;" />
            <asp:ImageButton ID="buttonDrawSquare" runat="server" OnClientClick="ctl00_ContentPlaceHolder1_harta.SetDrawMode('Square');return false;"
                ToolTip="Draw Square" ImageUrl="~/images/harta/square28.png" />
            <asp:ImageButton ID="buttonDrawPolygon" runat="server" ToolTip="Draw Polygon" ImageUrl="~/images/harta/polygon28.png"
                OnClientClick="ctl00_ContentPlaceHolder1_harta.SetDrawMode('Polygon');return false;" />
            <asp:ImageButton ID="buttonDrawCircle" runat="server" ToolTip="Draw Circle" ImageUrl="~/images/harta/circle28.png"
                OnClientClick="ctl00_ContentPlaceHolder1_harta.SetDrawMode('Circle');return false;" />
            <asp:ImageButton ID="buttonDrawEllipse" runat="server" ToolTip="Draw Ellipse" ImageUrl="~/images/harta/ellipse28.png"
                OnClientClick="ctl00_ContentPlaceHolder1_harta.SetDrawMode('Ellipse');return false;" />
        </p>
        <p>
            <asp:LinkButton ID="lbLeagaLinii" runat="server" OnClick="lbLeagaLinii_Click">Unire linii parcelă</asp:LinkButton>
        </p>
        <p>
            <asp:LinkButton ID="lbVizualizareSelectie" runat="server" OnClick="lbTransformaInPoligon_Click">Previzualizare selecţie</asp:LinkButton>
        </p>
        <p>
            <asp:LinkButton ID="lbSalvareParcela" runat="server" OnClick="lbSalvareParcela_Click"
                OnClientClick='return confirm ("Coordonatele existente la parcela aleasă vor fi înlocuite de coordonatele noi. Sigur continuaţi ?")'>Salvare parcelă</asp:LinkButton>
        </p>
    </div>
    <div style="position: absolute; right: 0px; top: 80px; width: 400px; z-index: 1000;
        border-left: solid 1px #5A540B; background-color: #FFF">
        <asp:Panel ID="pnLista" runat="server" CssClass="panel_general" Visible="true">
            <asp:UpdatePanel ID="upLista" runat="server">
                <ContentTemplate>
                    <h1 style="text-align: left; width: 400px;">
                        Gospodării</h1>
                    <asp:Panel ID="pnCautare" runat="server" CssClass="cauta">
                        <asp:Label ID="lbCautaDupa" runat="server" Text="Caută:"></asp:Label>
                        <asp:Label ID="lbfVolum" runat="server" Text="Vol."></asp:Label>
                        <asp:TextBox ID="tbfVolum" Width="27px" runat="server" AutoPostBack="True"></asp:TextBox>
                        <asp:Label ID="lbfNrPoz" runat="server" Text="Poz."></asp:Label>
                        <asp:TextBox ID="tbfNrPoz" Width="27px" runat="server" AutoPostBack="True"></asp:TextBox>
                        <asp:Label ID="ldfNume" runat="server" Text="Nume"></asp:Label>
                        <asp:TextBox ID="tbfNume" Width="70px" runat="server" AutoPostBack="True"></asp:TextBox>
                    </asp:Panel>
                    <asp:Panel ID="pnGrid" runat="server" Visible="true">
                        <asp:GridView ID="gvGospodarii" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            BorderStyle="None" CellPadding="3" GridLines="Vertical" ShowFooter="True" CssClass="tabela"
                            DataKeyNames="gospodarieId" DataSourceID="SqlGospodarii" EmptyDataText="Nu sunt adaugate inregistrari"
                            AllowPaging="True" OnRowDataBound="gvGospodarii_RowDataBound" OnSelectedIndexChanged="gvGospodarii_SelectedIndexChanged"
                            PageSize="7">
                            <Columns>
                                <asp:TemplateField HeaderText="gospodarieId" SortExpression="gospodarieId" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGospodarieId" runat="server" Text='<%# Bind("gospodarieId") %>'
                                            ToolTip='<%# Bind("volumInt") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="volum" HeaderText="Volum" SortExpression="volumInt" />
                                <asp:BoundField DataField="nrPozitie" HeaderText="Număr poziţie" SortExpression="nrPozitieInt" />
                                <asp:BoundField DataField="nume" HeaderText="Nume" SortExpression="nume" />
                                <asp:BoundField DataField="tip" HeaderText="Tip" SortExpression="tip" />
                                <asp:BoundField DataField="strada" HeaderText="Strada" SortExpression="strada" />
                                <asp:BoundField DataField="nr" HeaderText="Număr" SortExpression="nr" />
                                <asp:BoundField DataField="localitate" HeaderText="Localitate" SortExpression="localitate" />
                            </Columns>
                            <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                            <HeaderStyle Font-Bold="True" />
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlGospodarii" runat="server" 
                            
                            SelectCommand="SELECT (SELECT TOP (1) cnp FROM membri WHERE (unitateId = @unitateId) AND (an = @an) AND (gospodarieId = gospodarii.gospodarieId) AND (codRudenie = '1')) AS cnp, gospodarii.gospodarieId, gospodarii.unitateId, gospodarii.volum, gospodarii.nrPozitie, gospodarii.volumInt, gospodarii.nrPozitieInt, gospodarii.codSiruta, gospodarii.gospodarieCui, CASE gospodarii.tip WHEN '1' THEN 'Localnic' WHEN '2' THEN 'Străinaş' WHEN '3' THEN 'Firmă pe raza localităţii' ELSE 'Firmă străinaşă' END AS tip, CASE strainas WHEN 1 THEN sstrada ELSE strada END AS strada, CASE strainas WHEN 1 THEN snr ELSE nr END AS nr, gospodarii.nrInt, gospodarii.bl, gospodarii.sc, gospodarii.et, gospodarii.ap, gospodarii.codExploatatie, gospodarii.codUnic, CASE strainas WHEN 1 THEN sjudet ELSE judet END AS judet, CASE strainas WHEN 1 THEN slocalitate ELSE localitate END AS localitate, gospodarii.persJuridica, gospodarii.jUnitate, gospodarii.jSubunitate, gospodarii.jCodFiscal, gospodarii.jNumeReprez, gospodarii.strainas, gospodarii.sStrada, gospodarii.sNr, gospodarii.sBl, gospodarii.sSc, gospodarii.sEtj, gospodarii.sAp, gospodarii.sJudet, gospodarii.sLocalitate, CASE persjuridica WHEN 0 THEN (SELECT TOP (1) nume FROM membri WHERE an = @an AND gospodarieId = gospodarii.gospodarieid AND codrudenie = 1) ELSE junitate END AS nume FROM gospodarii INNER JOIN unitati ON gospodarii.unitateId = unitati.unitateId WHERE (COALESCE (gospodarii.volum, N'') LIKE @volum) AND (COALESCE (gospodarii.nrPozitie, N'') LIKE @nrPozitie) AND (COALESCE (CASE persjuridica WHEN 0 THEN (SELECT TOP (1) nume FROM membri WHERE an = @an AND gospodarieId = gospodarii.gospodarieid AND codrudenie = 1) ELSE junitate END, N'') LIKE '%' + @nume + '%') AND (gospodarii.unitateId = @unitateId) AND (gospodarii.an = @an) ORDER BY gospodarii.volumInt, gospodarii.nrPozitieInt">
                            <SelectParameters>
                                <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" />
                                <asp:SessionParameter Name="an" SessionField="SESan" />
                                <asp:ControlParameter ControlID="tbfVolum" DefaultValue="%" Name="volum" PropertyName="Text" />
                                <asp:ControlParameter ControlID="tbfNrPoz" DefaultValue="%" Name="nrPozitie" PropertyName="Text" />
                                <asp:ControlParameter ControlID="tbfNume" DefaultValue="%" Name="nume" PropertyName="Text" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </asp:Panel>
                    <h1 style="text-align: left; width: 400px;">
                        Parcele</h1>
                    <p>
                        <asp:GridView ID="gvParcele" AllowPaging="True" DataKeyNames="parcelaId" AllowSorting="True"
                            CssClass="tabela" runat="server" AutoGenerateColumns="False" DataSourceID="SqlParcele"
                            PageSize="10" OnRowDataBound="gvParcele_RowDataBound" EmptyDataText="Alegeţi o gospodărie">
                            <Columns>
                                <asp:BoundField DataField="parcelaDenumire" HeaderText="Denumire" SortExpression="parcelaDenumire" />
                                <asp:BoundField DataField="parcelaNrTopo" HeaderText="Nr. topo" SortExpression="parcelaNrTopo" />
                                <asp:BoundField DataField="parcelaCF" HeaderText="CF" SortExpression="parcelaCF" />
                                <asp:BoundField DataField="parcelaNrCadastral" HeaderText="Nr. cad." SortExpression="parcelaNrCadastral" />
                                <asp:BoundField DataField="parcelaSuprafataIntravilanHa" HeaderText="Ha intra" SortExpression="parcelaSuprafataIntravilanHa" />
                                <asp:BoundField DataField="parcelaSuprafataIntravilanAri" HeaderText="Ari intra"
                                    SortExpression="parcelaSuprafataIntravilanAri" />
                                <asp:BoundField DataField="parcelaSuprafataExtravilanHa" HeaderText="Ha extra" SortExpression="parcelaSuprafataExtravilanHa" />
                                <asp:BoundField DataField="parcelaSuprafataExtravilanAri" HeaderText="Ari extra"
                                    SortExpression="parcelaSuprafataExtravilanAri" />
                            </Columns>
                            <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlParcele" runat="server" 
                            SelectCommand="SELECT parcelaId, parcelaDenumire, parcelaCodRand, parcelaSuprafataIntravilanHa, parcelaSuprafataIntravilanAri, parcelaSuprafataExtravilanHa, parcelaSuprafataExtravilanAri, parcelaNrTopo, parcelaCF, parcelaCategorie, parcelaNrBloc, parcelaMentiuni, unitateaId, gospodarieId, an, capitolId, parcelaNrCadastral, parcelaNrCadastralProvizoriu, parcelaLocalitate, parcelaAdresa, volum, nrPozitie, importId, parcelaTarla, parcelaTitluProprietate, parcelaClasaBonitate, parcelaPunctaj, parcelaIdInitial FROM parcele WHERE (gospodarieId = @gospodarieId) AND (an = @an) ORDER BY parcelaCategorie">
                            <SelectParameters>
                                <asp:ControlParameter Name="gospodarieId" ControlID="gvGospodarii" PropertyName="SelectedValue" />
                                <asp:SessionParameter Name="an" SessionField="SESan" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </p>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </div>
</asp:Content>
