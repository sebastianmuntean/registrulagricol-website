<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="printCapitoleToateVolum.aspx.cs" Inherits="printCapitoleToateVolum" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt"
        Height="400px" Width="400px">
        <LocalReport ReportPath="rapoarte\raportCapitoleToateVolum.rdlc">
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" 
                    Name="dsRapoarte_dtRapCapitoleRegistru" />
            </DataSources>
        </LocalReport>
    </rsweb:ReportViewer>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
        InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" 
        SelectMethod="GetData" 
        TypeName="dsRapoarteTableAdapters.dtRapCapitoleRegistruTableAdapter">
        <SelectParameters>
            <asp:SessionParameter Name="utilizatorId" SessionField="SESutilizatorId" 
                Type="Int32" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="utilizatorId" Type="Int32" />
            <asp:Parameter Name="tipRap" Type="Int32" />
            <asp:Parameter Name="capitolId" Type="Int64" />
            <asp:Parameter Name="gospodarieId" Type="Int64" />
            <asp:Parameter Name="an" Type="Int32" />
            <asp:Parameter Name="nume" Type="String" />
            <asp:Parameter Name="codRand" Type="Int32" />
            <asp:Parameter Name="cnp" Type="String" />
            <asp:Parameter Name="codSex" Type="Int32" />
            <asp:Parameter Name="codRudenie" Type="Int32" />
            <asp:Parameter Name="denumireRudenie" Type="String" />
            <asp:Parameter Name="dataNasterii" Type="DateTime" />
            <asp:Parameter Name="mentiuni" Type="String" />
            <asp:Parameter Name="volum" Type="String" />
            <asp:Parameter Name="nrPozitie" Type="String" />
            <asp:Parameter Name="codSiruta" Type="String" />
            <asp:Parameter Name="tip" Type="Int32" />
            <asp:Parameter Name="strada" Type="String" />
            <asp:Parameter Name="nr" Type="String" />
            <asp:Parameter Name="nrInt" Type="Int32" />
            <asp:Parameter Name="bl" Type="String" />
            <asp:Parameter Name="sc" Type="String" />
            <asp:Parameter Name="et" Type="String" />
            <asp:Parameter Name="ap" Type="String" />
            <asp:Parameter Name="codExploatatie" Type="String" />
            <asp:Parameter Name="codUnic" Type="String" />
            <asp:Parameter Name="judet" Type="String" />
            <asp:Parameter Name="localitate" Type="String" />
            <asp:Parameter Name="persJuridica" Type="Boolean" />
            <asp:Parameter Name="jUnitate" Type="String" />
            <asp:Parameter Name="jSubunitate" Type="String" />
            <asp:Parameter Name="jCodFiscal" Type="String" />
            <asp:Parameter Name="jNumeReprez" Type="String" />
            <asp:Parameter Name="strainas" Type="Boolean" />
            <asp:Parameter Name="sStrada" Type="String" />
            <asp:Parameter Name="sNr" Type="String" />
            <asp:Parameter Name="sBl" Type="String" />
            <asp:Parameter Name="sSc" Type="String" />
            <asp:Parameter Name="sEtj" Type="String" />
            <asp:Parameter Name="sAp" Type="String" />
            <asp:Parameter Name="sJudet" Type="String" />
            <asp:Parameter Name="sLocalitate" Type="String" />
            <asp:Parameter Name="Membru" Type="String" />
            <asp:Parameter Name="cnpMembru" Type="String" />
            <asp:Parameter Name="mentiuneText" Type="String" />
            <asp:Parameter Name="codCapitol" Type="String" />
            <asp:Parameter Name="an1" Type="Int32" />
            <asp:Parameter Name="col1_1" Type="Decimal" />
            <asp:Parameter Name="col1_2" Type="Decimal" />
            <asp:Parameter Name="col1_3" Type="Decimal" />
            <asp:Parameter Name="col1_4" Type="Decimal" />
            <asp:Parameter Name="col1_5" Type="Decimal" />
            <asp:Parameter Name="col1_6" Type="Decimal" />
            <asp:Parameter Name="col1_7" Type="Decimal" />
            <asp:Parameter Name="col1_8" Type="Decimal" />
            <asp:Parameter Name="an2" Type="Int32" />
            <asp:Parameter Name="col2_1" Type="Decimal" />
            <asp:Parameter Name="col2_2" Type="Decimal" />
            <asp:Parameter Name="col2_3" Type="Decimal" />
            <asp:Parameter Name="col2_4" Type="Decimal" />
            <asp:Parameter Name="col2_5" Type="Decimal" />
            <asp:Parameter Name="col2_6" Type="Decimal" />
            <asp:Parameter Name="col2_7" Type="Decimal" />
            <asp:Parameter Name="col2_8" Type="Decimal" />
            <asp:Parameter Name="an3" Type="Int32" />
            <asp:Parameter Name="col3_1" Type="Decimal" />
            <asp:Parameter Name="col3_2" Type="Decimal" />
            <asp:Parameter Name="col3_3" Type="Decimal" />
            <asp:Parameter Name="col3_4" Type="Decimal" />
            <asp:Parameter Name="col3_5" Type="Decimal" />
            <asp:Parameter Name="col3_6" Type="Decimal" />
            <asp:Parameter Name="col3_7" Type="Decimal" />
            <asp:Parameter Name="col3_8" Type="Decimal" />
            <asp:Parameter Name="an4" Type="Int32" />
            <asp:Parameter Name="col4_1" Type="Decimal" />
            <asp:Parameter Name="col4_2" Type="Decimal" />
            <asp:Parameter Name="col4_3" Type="Decimal" />
            <asp:Parameter Name="col4_4" Type="Decimal" />
            <asp:Parameter Name="col4_5" Type="Decimal" />
            <asp:Parameter Name="col4_6" Type="Decimal" />
            <asp:Parameter Name="col4_7" Type="Decimal" />
            <asp:Parameter Name="col4_8" Type="Decimal" />
            <asp:Parameter Name="an5" Type="Int32" />
            <asp:Parameter Name="col5_1" Type="Decimal" />
            <asp:Parameter Name="col5_2" Type="Decimal" />
            <asp:Parameter Name="col5_3" Type="Decimal" />
            <asp:Parameter Name="col5_4" Type="Decimal" />
            <asp:Parameter Name="col5_5" Type="Decimal" />
            <asp:Parameter Name="col5_6" Type="Decimal" />
            <asp:Parameter Name="col5_7" Type="Decimal" />
            <asp:Parameter Name="col5_8" Type="Decimal" />
            <asp:Parameter Name="camp1" Type="String" />
            <asp:Parameter Name="camp2" Type="String" />
            <asp:Parameter Name="camp3" Type="String" />
            <asp:Parameter Name="camp4" Type="String" />
            <asp:Parameter Name="camp5" Type="String" />
            <asp:Parameter Name="camp6" Type="String" />
            <asp:Parameter Name="camp7" Type="String" />
            <asp:Parameter Name="camp8" Type="String" />
            <asp:Parameter Name="camp9" Type="String" />
            <asp:Parameter Name="camp10" Type="String" />
            <asp:Parameter Name="denumire1" Type="String" />
            <asp:Parameter Name="denumire2" Type="String" />
            <asp:Parameter Name="denumire3" Type="String" />
            <asp:Parameter Name="denumire4" Type="String" />
            <asp:Parameter Name="denumire5" Type="String" />
        </InsertParameters>
    </asp:ObjectDataSource>
</asp:Content>
