﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DosareVanzare : System.Web.UI.Page
{
    SqlCommand command = new SqlCommand();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "oferte vanzare", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
            LoadOferteVanzare();
        }

    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["SESgospodarieId"] == null)
        {
            HttpCookie vCookie = Request.Cookies["COOKTNT"];
            vCookie = CriptareCookie.DecodeCookie(vCookie);
            if (vCookie != null)
            {
                if (vCookie["COOKgospodarieId"] != null)
                {
                    Session["SESgospodarieId"] = vCookie["COOKgospodarieId"].ToString();
                }
                else if (Request.Url.AbsolutePath.IndexOf("Gospodarii.aspx") == -1 && Session["SESgospodarieId"] == null)
                    Response.Redirect("~/Gospodarii.aspx");
            }
        }
    }

    protected void oferteGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(oferteGridView, e, this);
        e.Row.Cells[0].Visible = false;
    }

    protected void oferteGridView_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetVisibilityOnOferteGridviewSelectedIndexChanged();
        Session["SESidOferta"] = oferteGridView.SelectedValue;
        VanzareTerenExtravilanServices.HasCereri(Convert.ToInt16(Session["SESan"]));
    }

    protected void parceleGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(parceleGridView, e, this);
        e.Row.Cells[0].Visible = false;
    }

    protected void listaCereriGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(listaCereriGridView, e, this);
        e.Row.Cells[0].Visible = false;
    }

    protected void listaCereriGridView_SelectedIndexChanged(object sender, EventArgs e)
    {
        stergeCerereButton.Visible = true;
        tiparesteAcceptareButton.Visible = true;
        modificaCerereButton.Visible = true;
        Session["SESidCerere"] = listaCereriGridView.SelectedValue;
    }

    protected void parceleGridView_SelectedIndexChanged(object sender, EventArgs e)
    {
        salveazaDosarButton.Visible = true;
        ofertaTextBoxuriPanel.Visible = true;
        Session["SESparcelaId"] = parceleGridView.SelectedValue;
        
        if (ViewState["oferta"].ToString() == "adauga")
        {
            nrDosarTextBox.Text = VanzareTerenExtravilanServices.GetLastNumarCerere(Convert.ToInt16(HttpContext.Current.Session["SESan"])).ToString();
            ofertaDetaliiTextBoxuriPanel.Visible = true;
            listaParcelePanel.Visible = false;
            listaParceleButton.Visible = true;
            DisablePersJuridicaTextBoxes();
        }
        else
        {
            listaParceleButton.Visible = false;
        }
        FillSuprafeteTextBox();
    }

    protected void parceleGridView_PreRender(object sender, EventArgs e)
    {
        string vInterogareTotal = "";
        if (ddlCCategoria.SelectedValue == "%" || ddlCCategoria.SelectedValue == "")
        {
            vInterogareTotal = "SELECT count(parcelaId) as x, SUM(CONVERT(decimal(18,4),parcelaSuprafataIntravilanHa)*100) + SUM(CONVERT(decimal(18,4),parcelaSuprafataIntravilanAri)) as intravilan, SUM(CONVERT(decimal(18,4),parcelaSuprafataExtravilanHa)*100) + SUM(CONVERT(decimal(18,4),parcelaSuprafataExtravilanAri)) as extravilan FROM parcele WHERE gospodarieId='" + Session["SESgospodarieId"].ToString() + "' and an='" + Session["SESan"].ToString() + "'";
        }
        else
        {
            vInterogareTotal = "SELECT  count(parcelaId) as x, SUM(CONVERT(decimal(18,4),parcelaSuprafataIntravilanHa)*100) + SUM(CONVERT(decimal(18,4),parcelaSuprafataIntravilanAri)) as intravilan, SUM(CONVERT(decimal(18,4),parcelaSuprafataExtravilanHa)*100) + SUM(CONVERT(decimal(18,4),parcelaSuprafataExtravilanAri)) as extravilan FROM parcele WHERE gospodarieId='" + Session["SESgospodarieId"].ToString() + "' and an='" + Session["SESan"].ToString() + "' AND parcelaCategorie = '" + ddlCCategoria.SelectedValue + "'";
        }
        // cate parcele sa afiseze pe pagina
        if (ddlNrPagina.SelectedValue == "%")
        {
            parceleGridView.AllowPaging = false;
        }
        else
        {
            parceleGridView.AllowPaging = true;
            parceleGridView.PageSize = Convert.ToInt16(ddlNrPagina.SelectedValue);
        }

        for (int i = 0; i < parceleGridView.Rows.Count; i++)
        {
            if (parceleGridView.Rows[i].RowType == DataControlRowType.DataRow)
            {
                try
                {
                    if (parceleGridView.Rows[i].Cells[19].Text == "0")
                        parceleGridView.Rows[i].Cells[19].Text = "-";
                    if (parceleGridView.Rows[i].Cells[18].Text == "0")
                        parceleGridView.Rows[i].Cells[18].Text = "-";
                }
                catch { }
            }
        }
        AfiseazaMesajDacaExistaSauNuParceleExtravilane();
    }

    protected void PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        parceleGridView.PageIndex = e.NewPageIndex;
        RefreshListaParceleGridView();
    }
    private void SaveOferta()
    {
        int sistArheologic = SistArheologic();
        int sistArheologicMonument = SistArheologicMonument();
        int litigiu = TerenLitigiu();
        int sarcina = TerenSarcini();
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        command.Connection = connection;
        int idParcela = Convert.ToInt32(parceleGridView.SelectedValue);
        command.CommandText = "INSERT INTO OferteVanzare (idParcela,idGospodarie,nrDosar,nrRegistruGeneral,suprafataOferitaHa,suprafataOferitaAri,valoareParcela, dataInregistrarii,unitateId,an) VALUES (" + idParcela + "," + Session["SESgospodarieId"] + "," + nrDosarTextBox.Text + "," + nrDosarRegGeneralTextBox.Text + "," + suprafataHaTextBox.Text.Replace(",", ".") + "," + suprafataAriTextBox.Text.Replace(",", ".") + "," + sumaTextBox.Text.Replace(",", ".") + ",convert(datetime,'" + dataInregistrariiTextBox.Text + "',104)," + Session["SESunitateId"] + "," + Session["SESan"] + ") select scope_identity()";
        int idOferta = Convert.ToInt32(command.ExecuteScalar());
        command.CommandText = string.Empty;
        command.CommandText = "INSERT INTO OferteVanzareDetalii(idOferta,buletin,serieBuletin,nrBuletin,eliberatDe,dataEliberariiBuletin,dataNasterii,localitateaNasterii,judetulNasterii,codPostal,tara,telefon,fax,email,site,cetatenia,stareCivila,inCalitateDe,conform,coproprietari,arendasi,vecini,nrCarteFunciara,apartine,conditiiVanzare,observatii,prin,pentru,prinCNPCIF,pentruCNPCIF,nrOrdine,act1,act2,act3,act4,act5,pentruLocaliate,pentruStrada,pentruNr,pentruBloc,pentruScara,pentruEtaj,pentruJudet,pentruCodPostal,pentruTelefon,pentruFax,pentruEmail,pentruSite,pentruApartament,sistArheologic,sistArheologicMonument,terenLitigiu,terenSarcini) VALUES ('" + idOferta + "','" + buletinTextBox.Text + "','" + serieBuletinTextBox.Text + "','" + nrBuletinTextBox.Text + "','" + eliberatDeTextBox.Text + "',convert(datetime,'" + dataEliberariiBuletinTextBox.Text + "',104),convert(datetime,'" + dataNasteriiTextBox.Text + "',104),'" + localitateaNasteriiTexBox.Text + "','" + judetulNasteriiTextBox.Text + "','" + codPostalTextBox.Text + "','" + taraTextBox.Text + "','" + telefonTextBox.Text + "','" + faxTextBox.Text + "','" + emailTextBox.Text + "','" + siteTextBox.Text + "','" + cetateniaTextBox.Text + "','" + stareaCivilaTextBox.Text + "','" + inCalitateDeTextBox.Text + "','" + conformTextBox.Text + "','" + coproprietariTextBox.Text + "','" + arendasiTextBox.Text + "','" + veciniTextBox.Text + "','" + nrCarteFunciaraTextBox.Text + "','" + apartineTextBox.Text + "','" + conditiiVanzareTextBox.Text + "','" + observatiiTextBox.Text + "','" + prinTextBox.Text + "','" + pentruTextBox.Text + "','" + prinCNPCIFTextBox.Text + "','" + pentruCNPCIFTextBox.Text + "','" + nrOrdineTextBox.Text + "','" + act1TextBox.Text + "','" + act2TextBox.Text + "','" + act3TextBox.Text + "','" + act4TextBox.Text + "','" + act5TextBox.Text + "','" + pentruLocalitateTextBox.Text + "','" + pentruStradaTextBox.Text + "','" + pentruNrTextBox.Text + "','" + pentruBlocTextBox.Text + "','" + pentruScaraTextBox.Text + "','" + pentruEtajTextBox.Text + "','" + pentruJudetTextBox.Text + "','" + pentruCodPostalTextBox.Text + "','" + pentruTelefonTextBox.Text + "','" + pentruFaxTextBox.Text + "','" + pentruEmailTextBox.Text + "','" + pentruSiteTextBox.Text + "','" + pentruApartamentTextBox.Text + "','" + sistArheologic + "','" + sistArheologicMonument + "','" + litigiu + "','" + sarcina + "')";
        command.ExecuteNonQuery();
        ManipuleazaBD.InchideConexiune(connection);
    }

    private void SaveCerere()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        command.Connection = connection;
        command.CommandText = "INSERT into CereriVanzare (idOferta,numeOfertant,sumaOferita,dataInregistrarii,act1,act2,act3,act4,act5) values ('" + oferteGridView.SelectedValue + "','" + numeOfertantTextBox.Text + "','" + sumaOferitaTextBox.Text.Replace(",", ".") + "', convert(datetime,'" + dataInregistrariiOfertantTextBox.Text + "',104),'" + cerereAct1TextBox.Text + "','" + cerereAct2TextBox.Text + "','" + cerereAct3TextBox.Text + "','" + cerereAct4TextBox.Text + "','" + cerereAct5TextBox.Text + "')";
        command.ExecuteNonQuery();
        ManipuleazaBD.InchideConexiune(connection);

    }

    private void DeleteOferta()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        command.Connection = connection;
        int idOferta = Convert.ToInt32(oferteGridView.SelectedValue);
        command.CommandText = "DELETE FROM OferteVanzare WHERE idOferta = " + idOferta + " and idGospodarie =" + Session["SESgospodarieId"] + " and unitateId = " + Session["SESunitateId"] + ";Delete from OferteVanzareDetalii where idOferta = " + idOferta + ";Delete from CereriVanzare where idOferta =" + idOferta + "";
        command.ExecuteNonQuery();
        ManipuleazaBD.InchideConexiune(connection);
        LoadOferteVanzare();
    }

    protected void DeleteCerere()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        command.Connection = connection;
        int idCerere = Convert.ToInt32(listaCereriGridView.SelectedValue);
        command.CommandText = "Delete from CereriVanzare where idCerere = " + idCerere + "";
        command.ExecuteNonQuery();
        ManipuleazaBD.InchideConexiune(connection);
        RefreshListaCereriGridView();
    }

    private void UpdateOferta()
    {
        int sistArheologic = SistArheologic();
        int sistArheologicMonument = SistArheologicMonument();
        int litigiu = TerenLitigiu();
        int sarcina = TerenSarcini();
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        command.Connection = connection;
        if (parceleGridView.SelectedValue == null)
        {
            ManipuleazaBD.InchideConexiune(connection);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Alegeti parcela care se doreste a fi modificata')", true);
            return;
        }
        else
        {
            command.CommandText += "update OferteVanzare set idParcela = " + parceleGridView.SelectedValue + ", nrDosar = " + nrDosarTextBox.Text + ", nrRegistruGeneral = " + nrDosarRegGeneralTextBox.Text + ", suprafataOferitaHa =" + suprafataHaTextBox.Text.Replace(",", ".") + ",suprafataOferitaAri=" + suprafataAriTextBox.Text.Replace(",", ".") + ", valoareParcela = " + sumaTextBox.Text.Replace(",", ".") + ", dataInregistrarii = convert(datetime,'" + dataInregistrariiTextBox.Text + "',104) where idOferta = " + oferteGridView.SelectedValue + " and idGospodarie = " + Session["SESgospodarieId"] + " and unitateId = " + Session["SESunitateId"] + "";

            command.CommandText += "update OferteVanzareDetalii set  buletin= '" + buletinTextBox.Text + "' , serieBuletin = '" + serieBuletinTextBox.Text + "', nrBuletin= '" + nrBuletinTextBox.Text + "', eliberatDe = '" + eliberatDeTextBox.Text + "', dataEliberariiBuletin = convert(datetime,'" + dataEliberariiBuletinTextBox.Text + "',104), dataNasterii = convert(datetime,'" + dataNasteriiTextBox.Text + "',104), localitateaNasterii= '" + localitateaNasteriiTexBox.Text + "',judetulNasterii= '" + judetulNasteriiTextBox.Text + "',codPostal= '" + codPostalTextBox.Text + "', tara= '" + taraTextBox.Text + "', telefon= '" + telefonTextBox.Text + "', fax= '" + faxTextBox.Text + "',email= '" + emailTextBox.Text + "',site= '" + siteTextBox.Text + "',cetatenia= '" + cetateniaTextBox.Text + "', stareCivila= '" + stareaCivilaTextBox.Text + "',inCalitateDe= '" + inCalitateDeTextBox.Text + "',conform= '" + conformTextBox.Text + "',coproprietari= '" + coproprietariTextBox.Text + "',arendasi= '" + arendasiTextBox.Text + "',vecini= '" + veciniTextBox.Text + "',nrCarteFunciara = '" + nrCarteFunciaraTextBox.Text + "',apartine= '" + apartineTextBox.Text + "',observatii= '" + observatiiTextBox.Text + "',prin= '" + prinTextBox.Text + "',pentru= '" + pentruTextBox.Text + "',prinCNPCIF= '" + prinCNPCIFTextBox.Text + "',pentruCNPCIF= '" + pentruCNPCIFTextBox.Text + "',pentruStrada= '" + pentruStradaTextBox.Text + "',pentruNr= '" + pentruNrTextBox.Text + "',pentruBloc= '" + pentruBlocTextBox.Text + "',pentruScara= '" + pentruScaraTextBox.Text + "',pentruEtaj= '" + pentruEtajTextBox.Text + "',pentruJudet= '" + pentruJudetTextBox.Text + "',pentruCodPostal= '" + pentruCodPostalTextBox.Text + "',pentruTelefon= '" + pentruTelefonTextBox.Text + "',pentruFax= '" + pentruFaxTextBox.Text + "',pentruEmail= '" + pentruEmailTextBox.Text + "',pentruSite= '" + pentruSiteTextBox.Text + "',pentruApartament= '" + pentruApartamentTextBox.Text + "',sistArheologic='" + sistArheologic + "',sistArheologicMonument='" + sistArheologicMonument + "',terenLitigiu='" + litigiu + "',terenSarcini='" + sarcina + "'  where idOferta = '" + oferteGridView.SelectedValue + "'";

            command.ExecuteNonQuery();
            ManipuleazaBD.InchideConexiune(connection);
        }
    }

    protected void UpdateCerere()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        command.Connection = connection;
        command.CommandText = "update CereriVanzare set numeOfertant ='" + numeOfertantTextBox.Text + "', sumaOferita ='" + sumaOferitaTextBox.Text.Replace(",", ".") + "',dataInregistrarii=convert(datetime,'" + dataInregistrariiOfertantTextBox.Text + "',104), act1='" + cerereAct1TextBox.Text + "', act2='" + cerereAct2TextBox.Text + "', act3='" + cerereAct3TextBox.Text + "', act4='" + cerereAct4TextBox.Text + "', act5='" + cerereAct5TextBox.Text + "'  where idCerere = " + Session["SESidCerere"] + "";
        command.ExecuteNonQuery();
        ManipuleazaBD.InchideConexiune(connection);
    }


    private void FillTextBoxesModificaOferta()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        command.Connection = connection;
       // command.CommandText = "SELECT nrDosar, nrRegistruGeneral, suprafataOferitaHa, suprafataOferitaAri, valoareParcela, dataInregistrarii FROM OferteVanzare WHERE idOferta = " + oferteGridView.SelectedValue + " and idGospodarie = " + Session["SESgospodarieId"] + " and unitateId = " + Session["SESunitateId"] + "";
        command.CommandText = "SELECT * FROM OferteVanzare join OferteVanzareDetalii on OferteVanzareDetalii.idOferta = OferteVanzare.idOferta where OferteVanzare.idOferta = " + oferteGridView.SelectedValue + " and idGospodarie = " + Session["SESgospodarieId"] + " and unitateId = " + Session["SESunitateId"] + "";
        SqlDataReader dataReader = command.ExecuteReader();
        if (dataReader.Read())
        {
            nrDosarTextBox.Text = dataReader["nrDosar"].ToString();
            nrDosarRegGeneralTextBox.Text = dataReader["nrRegistruGeneral"].ToString();
            suprafataHaTextBox.Text = dataReader["suprafataOferitaHa"].ToString();
            suprafataAriTextBox.Text = dataReader["suprafataOferitaAri"].ToString();
            sumaTextBox.Text = dataReader["valoareParcela"].ToString();
            dataInregistrariiTextBox.Text = Convert.ToDateTime(dataReader["dataInregistrarii"]).ToShortDateString();
            buletinTextBox.Text = dataReader["buletin"].ToString();
            serieBuletinTextBox.Text = dataReader["nrBuletin"].ToString();
            eliberatDeTextBox.Text = dataReader["eliberatDe"].ToString();
            dataEliberariiBuletinTextBox.Text = Convert.ToDateTime(dataReader["dataEliberariiBuletin"]).ToShortDateString();
            dataNasteriiTextBox.Text = Convert.ToDateTime(dataReader["dataNasterii"]).ToShortDateString();
            localitateaNasteriiTexBox.Text = dataReader["localitateaNasterii"].ToString();
            judetulNasteriiTextBox.Text = dataReader["judetulNasterii"].ToString();
            codPostalTextBox.Text = dataReader["codPostal"].ToString();
            taraTextBox.Text = dataReader["tara"].ToString();
            telefonTextBox.Text = dataReader["telefon"].ToString();
            faxTextBox.Text = dataReader["fax"].ToString();
            emailTextBox.Text = dataReader["email"].ToString();
            siteTextBox.Text = dataReader["site"].ToString();
            cetateniaTextBox.Text = dataReader["cetatenia"].ToString();
            stareaCivilaTextBox.Text = dataReader["stareCivila"].ToString();
            inCalitateDeTextBox.Text = dataReader["inCalitateDe"].ToString();
            conformTextBox.Text = dataReader["conform"].ToString();
            coproprietariTextBox.Text = dataReader["coproprietari"].ToString();
            arendasiTextBox.Text = dataReader["arendasi"].ToString();
            veciniTextBox.Text = dataReader["vecini"].ToString();
            nrCarteFunciaraTextBox.Text = dataReader["nrCarteFunciara"].ToString();
            apartineTextBox.Text = dataReader["apartine"].ToString();
            conditiiVanzareTextBox.Text = dataReader["conditiiVanzare"].ToString();
            observatiiTextBox.Text = dataReader["observatii"].ToString();
            prinTextBox.Text = dataReader["prin"].ToString();
            pentruTextBox.Text = dataReader["pentru"].ToString();
            prinCNPCIFTextBox.Text = dataReader["prinCNPCIF"].ToString();
            pentruCNPCIFTextBox.Text = dataReader["pentruCNPCIF"].ToString();
            nrOrdineTextBox.Text = dataReader["nrOrdine"].ToString();
            act1TextBox.Text = dataReader["act1"].ToString();
            act2TextBox.Text = dataReader["act2"].ToString();
            act3TextBox.Text = dataReader["act3"].ToString();
            act4TextBox.Text = dataReader["act4"].ToString();
            act5TextBox.Text = dataReader["act5"].ToString();
            pentruLocalitateTextBox.Text = dataReader["pentruLocaliate"].ToString();
            pentruStradaTextBox.Text = dataReader["pentruStrada"].ToString();
            pentruNrTextBox.Text = dataReader["pentruNr"].ToString();
            pentruBlocTextBox.Text = dataReader["pentruBloc"].ToString();
            pentruScaraTextBox.Text = dataReader["pentruScara"].ToString();
            pentruEtajTextBox.Text = dataReader["pentruEtaj"].ToString();
            pentruJudetTextBox.Text = dataReader["pentruJudet"].ToString();
            pentruCodPostalTextBox.Text = dataReader["pentruCodPostal"].ToString();
            pentruTelefonTextBox.Text = dataReader["pentruTelefon"].ToString();
            pentruFaxTextBox.Text = dataReader["pentruFax"].ToString();
            pentruEmailTextBox.Text = dataReader["pentruEmail"].ToString();
            pentruSiteTextBox.Text = dataReader["pentruSite"].ToString();
            pentruApartamentTextBox.Text = dataReader["pentruApartament"].ToString();
            sistArheologicCkeckBox.Checked = Convert.ToBoolean(dataReader["sistArheologic"].ToString());
            sistArheologicMonumentCheckBox.Checked = Convert.ToBoolean(dataReader["sistArheologicMonument"].ToString());
            terenLitigiuCheckBox.Checked = Convert.ToBoolean(dataReader["terenLitigiu"].ToString());
            terenSarciniCheckBox.Checked = Convert.ToBoolean(dataReader["terenSarcini"].ToString());
        }
        dataReader.Close();
        ManipuleazaBD.InchideConexiune(connection);
    }

    private void FillTextBoxesModificaCerere()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        command.Connection = connection;
        command.CommandText = "SELECT * FROM CereriVanzare WHERE idCerere = " + listaCereriGridView.SelectedValue + "";
        SqlDataReader dataReader = command.ExecuteReader();
        if (dataReader.Read())
        {
            numeOfertantTextBox.Text = dataReader["numeOfertant"].ToString();
            sumaOferitaTextBox.Text = dataReader["sumaOferita"].ToString();
            dataInregistrariiOfertantTextBox.Text = Convert.ToDateTime(dataReader["dataInregistrarii"]).ToShortDateString();
            cerereAct1TextBox.Text = dataReader["act1"].ToString();
            cerereAct2TextBox.Text = dataReader["act2"].ToString();
            cerereAct3TextBox.Text = dataReader["act3"].ToString();
            cerereAct4TextBox.Text = dataReader["act4"].ToString();
            cerereAct5TextBox.Text = dataReader["act5"].ToString();
        }
        dataReader.Close();
        ManipuleazaBD.InchideConexiune(connection);
    }

    private void FillSuprafeteTextBox()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        command.Connection = connection;
        command.CommandText = "select parcelaSuprafataExtravilanHa,parcelaSuprafataExtravilanAri,parcele.parcelaCF FROM parcele WHERE parcelaId = " + parceleGridView.SelectedValue + " and gospodarieId = " + Session["SESgospodarieId"] + " and unitateaId = " + Session["SESunitateId"] + "";
        SqlDataReader dataReader = command.ExecuteReader();
        if (dataReader.Read())
        {
            suprafataHaTextBox.Text = dataReader["parcelaSuprafataExtravilanHa"].ToString();
            suprafataAriTextBox.Text = dataReader["parcelaSuprafataExtravilanAri"].ToString();
            nrCarteFunciaraTextBox.Text = VanzareTerenExtravilanServices.GetNumarCarteFunciara(Convert.ToInt16(HttpContext.Current.Session["SESan"]));
        }
        dataReader.Close();
        ManipuleazaBD.InchideConexiune(connection);
    }

    private int SistArheologic()
    {
        int sistArheologic = 0;
        if (sistArheologicCkeckBox.Checked)
            sistArheologic = 1;
        return sistArheologic;
    }

    private int SistArheologicMonument()
    {
        int sistArheologicMonument = 0;
        if (sistArheologicMonumentCheckBox.Checked)
            sistArheologicMonument = 1;
        return sistArheologicMonument;
    }

    private int TerenLitigiu()
    {
        int litigiu = 0;
        if (terenLitigiuCheckBox.Checked)
            litigiu = 1;
        return litigiu;
    }

    private int TerenSarcini()
    {
        int saricina = 0;
        if (terenSarciniCheckBox.Checked)
            saricina = 1;
        return saricina;
    }

    //protected void nrRegistruGeneralCheckBox_OnCheckedChanged(object sender, EventArgs e)
    //{
    //    if (nrRegistruGeneralCheckBox.Checked == true)
    //    {
    //        nrDosarRegGeneralTextBox.Enabled = false;
    //    }
    //    else
    //    {
    //        nrDosarRegGeneralTextBox.Enabled = true;
    //    }
    //}

    //protected void nrDosarCheckBox_OnCheckedChanged(object sender, EventArgs e)
    //{
    //    if (nrDosarCheckBox.Checked == true)
    //    {
    //        nrDosarTextBox.Enabled = false;
    //    }
    //    else
    //    {
    //        nrDosarTextBox.Enabled = true;
    //    }
    //}

    protected void ClearCerereTextBoxes()
    {
        foreach (Control c in this.ofertaDetaliiTextBoxuriPanel.Controls)
        {
            if (c is TextBox)
            {
                ((TextBox)c).Text = string.Empty;
            }
        }
    }

    protected void ClearAdaugaCerereTextBoxes()
    {

        foreach (Control c in this.cerereTextBoxuriPanel.Controls)
        {
            if (c is TextBox)
            {
                ((TextBox)c).Text = string.Empty;
            }
        }
    }

    protected void ClearOfertaTextBoxes()
    {
        foreach (Control c in this.ofertaTextBoxuriPanel.Controls)
        {
            if (c is TextBox )
            {
                ((TextBox)c).Text = string.Empty;
            }
        }
    }

    protected void DisablePersJuridicaTextBoxes()
    {
        if (VanzareTerenExtravilanServices.IsPersoanaFizica(Convert.ToInt16(Session["SESan"])))
        {
            pentruTextBox.Enabled = false;
            pentruCNPCIFTextBox.Enabled = false;
            nrOrdineTextBox.Enabled = false;
        }
        else
        {
            pentruTextBox.Enabled = true;
            pentruCNPCIFTextBox.Enabled = true;
            nrOrdineTextBox.Enabled = true;
        }
    }

    private void AfiseazaMesajDacaExistaSauNuParceleExtravilane()
    {
        if (parceleGridView.Rows.Count == 0)
        {
            parceleLabel.Text = "Nu aveți parcele extravilane";
        }
        else
        {
            parceleLabel.Text = "Selectați parcela care se dorește a fi oferită spre vânzare";
        }
    }

    #region butoane
    protected void adaugaDosarButton_OnClick(object sender, EventArgs e)
    {
        ViewState["oferta"] = "adauga";
        mesajEroareOferteLabel.Text = string.Empty;
        numeMembruSelectatLabel.Text = VanzareTerenExtravilanServices.GetNumeMembruSelectat(Convert.ToInt16(Session["SESan"])).ToString();
        SetVisibilityOnAdaugaOfertaButton();
        RefreshListaParceleGridView();
        parceleGridView.SelectedIndex = -1;
        ClearOfertaTextBoxes();
        ClearCerereTextBoxes();
        AfiseazaMesajDacaExistaSauNuParceleExtravilane();
    }


    protected void modificaDosarButton_Click(object sender, EventArgs e)
    {
        ViewState["oferta"] = "modifica";
        ddlCCategoria.SelectedValue = "%";
        mesajEroareOferteLabel.Text = string.Empty;
        numeMembruSelectatLabel.Text = VanzareTerenExtravilanServices.GetNumeMembruSelectat(Convert.ToInt16(Session["SESan"])).ToString();
        SetVisibilityOnModificaOfertaButton();
        FillTextBoxesModificaOferta();
        RefreshListaParceleGridView();
    }

    protected void listaParceleButton_Click(object sender, EventArgs e)
    {
        SetVisibilityOnListaParceleButton();
        parceleGridView.SelectedIndex = -1;
        ClearCerereTextBoxes();
        ClearOfertaTextBoxes();
    }

    protected void salveazaDosarButton_Click(object sender, EventArgs e)
    {
        if (VanzareTerenExtravilanServices.IsPersoanaFizica(Convert.ToInt16(Session["SESan"])))
        {
            if (ViewState["oferta"].ToString() == "adauga")
            {
                if (ValidateOfertaVanzareInputs() && ValidateSuprafateInputs() && VanzareTerenExtravilanServices.VerifyIfNumarCerereExist(nrDosarTextBox.Text, Convert.ToInt16(HttpContext.Current.Session["SESan"])))
                {
                    SaveOferta();
                    SetVisibilityOnSalveazaOfertaButton();
                }
                else
                {
                    if (ValidateOfertaVanzareInputs() && ValidateSuprafateInputs() && !VanzareTerenExtravilanServices.VerifyIfNumarCerereExist(nrDosarTextBox.Text, Convert.ToInt16(HttpContext.Current.Session["SESan"])))
                    {
                        mesajEroareOferteLabel.Text = "Acest nr de cerere este deja inregistrat";
                        Page.SetFocus(ofertaTextBoxuriPanel);
                    }
                    return;
                }
            }
            else
                if (ViewState["oferta"].ToString() == "modifica")
                {
                    if (ValidateOfertaVanzareInputs() && ValidateSuprafateInputs())
                    {
                        UpdateOferta();
                        SetVisibilityOnSalveazaOfertaButton();
                    }
                    else return;
                }


        }
        else
            if (ViewState["oferta"].ToString() == "adauga")
            {
                if (ValidateOfertaVanzareInputs() && ValidateSuprafateInputs() && VanzareTerenExtravilanServices.VerifyIfNumarCerereExist(nrDosarTextBox.Text, Convert.ToInt16(HttpContext.Current.Session["SESan"])))
                {
                    SaveOferta();
                    SetVisibilityOnSalveazaOfertaButton();
                }
                else
                    if (ValidateOfertaVanzareInputs() && ValidateSuprafateInputs() && !VanzareTerenExtravilanServices.VerifyIfNumarCerereExist(nrDosarTextBox.Text, Convert.ToInt16(HttpContext.Current.Session["SESan"])))
                {
                  mesajEroareOferteLabel.Text = "Acest nr de cerere este deja inregistrat";
                  Page.SetFocus(ofertaTextBoxuriPanel);
                  return;
                }
            }
            else
                if (ViewState["oferta"].ToString() == "modifica")
                {
                    if (ValidateOfertaVanzareInputs() && ValidateSuprafateInputs())
                    {
                        UpdateOferta();
                        SetVisibilityOnSalveazaOfertaButton();
                    }
                    else return;
                }


        adaugaDosarPanel.Visible = false;
        listaOfertePanel.Visible = true;
        LoadOferteVanzare();
        adaugaDosarButton.Visible = true;
        modificaDosarButton.Visible = false;
        stergeDosarButton.Visible = false;
        oferteGridView.SelectedIndex = -1;
    }

    protected void stergeDosarButton_Click(object sender, EventArgs e)
    {
        DeleteOferta();
        SetVisibilityOnStergeOfertaButton();
        oferteGridView.SelectedIndex = -1;
    }

    protected void listaCereriButton_Click(object sender, EventArgs e)
    {
        listaOfertePanel.Visible = false;
        listaCereriPanel.Visible = true;
        listaCereriGridView.SelectedIndex = -1;
        stergeCerereButton.Visible = false;
        tiparesteAcceptareButton.Visible = false;
        modificaCerereButton.Visible = false;
        RefreshListaCereriGridView();
    }

    protected void adaugaCerereButton_Click(object sender, EventArgs e)
    {
        ViewState["cerere"] = "adauga";
        listaCereriPanel.Visible = false;
        ClearAdaugaCerereTextBoxes();
        mesajEroareCereriLabel.Text = "";
        adaugaCererePanel.Visible = true;
    }

    protected void modificaCerereButton_Click(object sender, EventArgs e)
    {
        ViewState["cerere"] = "modifica";
        listaCereriPanel.Visible = false;
        adaugaCererePanel.Visible = true;
        cerereTextBoxuriPanel.Visible = true;
        mesajEroareCereriLabel.Text = "";
        FillTextBoxesModificaCerere();
    }


    protected void stergeCerereButton_Click(object sender, EventArgs e)
    {
        DeleteCerere();
        SetVisibilityOnStergeCerereButton();
        listaCereriGridView.SelectedIndex = -1;
    }


    protected void listaDosareButton_Click(object sender, EventArgs e)
    {
        SetVisibilityOnListaOferteButton();
        listaCereriButton.Visible = false;
        oferteGridView.SelectedIndex = -1;
    }

    protected void salveazaCerereButton_Click(object sender, EventArgs e)
    {
        if (ValidateSumaOferita() && ValidateCerereInputs())
        {
            if (ViewState["cerere"].ToString() == "adauga")
            {
                SaveCerere();
                RefreshListaCereriGridView();
                SetVisibilityOnSalveazaCerereButton();
                listaCereriGridView.SelectedIndex = -1;
            }
            else
            {
                if (ViewState["cerere"].ToString() == "modifica")
                {
                    UpdateCerere();
                    RefreshListaCereriGridView();
                    SetVisibilityOnSalveazaCerereButton();
                    listaCereriGridView.SelectedIndex = -1;
                }
            }
        }
        else
            return;
    }


    protected void listaOferteButton_Click(object sender, EventArgs e)
    {
        listaCereriPanel.Visible = false;
        listaOfertePanel.Visible = true;
        SetVisibilityOnStergeOfertaButton();
        oferteGridView.SelectedIndex = -1;
    }

    protected void backToListaOferteButton_Click(object sender, EventArgs e)
    {
        listaCereriPanel.Visible = false;
        adaugaCererePanel.Visible = false;
        SetVisibilityOnStergeOfertaButton();
        oferteGridView.SelectedIndex = -1;
        listaOfertePanel.Visible = true;
    }
    protected void backToListaCereriButton_Click(object sender, EventArgs e)
    {
        adaugaCererePanel.Visible = false;
        listaCereriPanel.Visible = true;
        stergeCerereButton.Visible = false;
        tiparesteAcceptareButton.Visible = false;
        modificaCerereButton.Visible = false;
        listaCereriGridView.SelectedIndex = -1;
    }
    #endregion

    #region validariImputuri
    private bool ValidateOfertaVanzareInputs()
    {
        if (nrDosarTextBox.Text == string.Empty || nrDosarRegGeneralTextBox.Text == string.Empty || suprafataHaTextBox.Text == string.Empty || sumaTextBox.Text == string.Empty || suprafataAriTextBox.Text == string.Empty || dataInregistrariiTextBox.Text == string.Empty)
        {
            mesajEroareOferteLabel.Text = "Numarul cererii, numarul registrului general, suma si data intregistrarii sunt obligatoriu de completat";
            Page.SetFocus(ofertaTextBoxuriPanel);
            return false;
        }
        return true;
    }

    private bool ValidateSumaOferita()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        command.Connection = connection;
        command.CommandText = "select valoareParcela from OferteVanzare where idOferta = '" + oferteGridView.SelectedValue + "'";
        SqlDataReader dataReader = command.ExecuteReader();
        if (dataReader.Read())
        {
            if (Convert.ToDecimal(sumaOferitaTextBox.Text) < Convert.ToDecimal(dataReader["valoareParcela"]))
            {
                mesajEroareCereriLabel.Text = "suma oferita nu trebuie sa fie mai mica decat valoarea parcelei";
                return false;
            }
        }
        ManipuleazaBD.InchideConexiune(connection);
        return true;
    }

    private bool ValidateSuprafateInputs()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        command.Connection = connection;
        if (parceleGridView.SelectedValue == null)
        {
            ManipuleazaBD.InchideConexiune(connection);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Selectati parcela care se doreste a fi modificata')", true);
            return false;
        }
        else
        {
            command.CommandText = "select parcelaSuprafataExtravilanHa,parcelaSuprafataExtravilanAri from parcele where parcelaId = " + parceleGridView.SelectedValue + " and gospodarieId = " + Session["SESgospodarieId"] + " and unitateaId = " + Session["SESunitateId"] + "";

            SqlDataReader dataReader = command.ExecuteReader();
            if (dataReader.Read())
            {
                if (Convert.ToDecimal(suprafataHaTextBox.Text) > Convert.ToDecimal(dataReader["parcelaSuprafataExtravilanHa"]) || Convert.ToDecimal(suprafataAriTextBox.Text) > Convert.ToDecimal(dataReader["parcelaSuprafataExtravilanAri"]))
                {
                    mesajEroareOferteLabel.Text = "Suprafata oferita spre vanzare nu trebuie sa depaseasca suprafata totala a parcelei";
                    Page.SetFocus(ofertaTextBoxuriPanel);
                    return false;
                }
            }

            dataReader.Close();
            ManipuleazaBD.InchideConexiune(connection);
            return true;
        }
    }

    private bool ValidateCerereInputs()
    {
        if (numeOfertantTextBox.Text == string.Empty || dataInregistrariiOfertantTextBox.Text == string.Empty || sumaOferitaTextBox.Text == string.Empty)
        {
            mesajEroareCereriLabel.Text = "Campurile nume, suma si data sunt obligatoriu de completat";
            return false;
        }
        return true;
    }

    #endregion

    #region tipariri
    protected void tiparesteCerereButton_Click(object sender, EventArgs e)
    {
        if (VanzareTerenExtravilanServices.IsPersoanaFizica(Convert.ToInt16(Session["SESan"])))
        {
            ResponseHelper.Redirect("~/printCerereVanzarePF.aspx", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "cerereVanzarePF", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
        }
        else
        {
            ResponseHelper.Redirect("~/printCerereVanzarePJ.aspx", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "cerereVanzarePJ", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
        }
    }

    protected void tiparesteOfertaButton_Click(object sender, EventArgs e)
    {
        if (VanzareTerenExtravilanServices.IsPersoanaFizica(Convert.ToInt16(Session["SESan"])))
        {
            ResponseHelper.Redirect("~/printOfertaVanzarePF.aspx", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "ofertaVanzarePF", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
        }
        else
        {
            ResponseHelper.Redirect("~/printOfertaVanzarePJ.aspx", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "ofertaVanzarePJ", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
        }
    }

    protected void tiparesteAdeverintaButton_Click(object sender, EventArgs e)
    {
        if (!VanzareTerenExtravilanServices.HasCereri(Convert.ToInt16(Session["SESan"])))
        {
            ResponseHelper.Redirect("~/printAdeverintaVanzare.aspx", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "adeverinta", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Nu puteti tipari adeverinta deoarece aveti cereri de comunicare pentru aceasta oferta')", true);
        }
    }

    protected void tiparesteAdresaButton_Click(object sender, EventArgs e)
    {
        ResponseHelper.Redirect("~/printAdresaVanzare.aspx", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
        ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "adresa", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
    }

    protected void tiparesteCerereCulturaButton_Click(object sender, EventArgs e)
    {
        if (VanzareTerenExtravilanServices.IsPersoanaFizica(Convert.ToInt16(Session["SESan"])))
        {
            ResponseHelper.Redirect("~/printCerereCulturaPF.aspx", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "cerereCulturaPF", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
        }
        else
        {
            ResponseHelper.Redirect("~/printCerereCulturaPJ.aspx", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "cerereCulturaPJ", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
        }
    }

    protected void tiparesteAcceptareButton_Click(object sender, EventArgs e)
    {
        if (VanzareTerenExtravilanServices.IsPersoanaFizica(Convert.ToInt16(Session["SESan"])))
        {
            ResponseHelper.Redirect("~/printComunicareDeAcceptarePF.aspx", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "comunicareAcceptarePF", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
        }
        else
        {
            ResponseHelper.Redirect("~/printComunicareDeAcceptarePJ.aspx", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "comunicareAcceptarePJ", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
        }
    }
    #endregion

    #region filtrari

    //filtrare lista parcele
    protected void categoriaDropDown_SelectedIndexChanged(object sender, EventArgs e)
    {
        RefreshListaParceleGridView();
        LoadOferteVanzare();
        if (parceleGridView.Rows.Count == 0)
        {
            ofertaTextBoxuriPanel.Visible = false;
        }
        if (oferteGridView.Rows.Count == 0)
        {
            foreach (Control c in this.pnListaButoanePrincipale.Controls)
            {
                if (c is Button)
                {
                    if (c != adaugaDosarButton)
                        ((Button)c).Visible = false;
                }
            }
        }
        else
        {
            oferteGridView.SelectedIndex = -1;
        }
    }

    protected void tbCBloc_TextChanged(object sender, EventArgs e)
    {
        RefreshListaParceleGridView();
    }

    protected void tbCNrCadastral_TextChanged(object sender, EventArgs e)
    {
        RefreshListaParceleGridView();
    }
    protected void tbCNrCadastralProvizoriu_TextChanged(object sender, EventArgs e)
    {
        RefreshListaParceleGridView();
    }
    protected void tbCTopo_TextChanged(object sender, EventArgs e)
    {
        RefreshListaParceleGridView();
    }

    protected void tbCCF_TextChanged(object sender, EventArgs e)
    {
        RefreshListaParceleGridView();
    }
    protected void tbCDenumire_TextChanged(object sender, EventArgs e)
    {
        RefreshListaParceleGridView();
    }

    //filtrare lista oferte
    protected void tbNrDosar_TextChanged(object sender, EventArgs e)
    {
        LoadOferteVanzare();
    }
    protected void tbNrRegGeneral_TextChanged(object sender, EventArgs e)
    {
        LoadOferteVanzare();
    }
    protected void tbTopo_TextChanged(object sender, EventArgs e)
    {
        LoadOferteVanzare();
    }
    protected void tbDenumireParcela_TextChanged(object sender, EventArgs e)
    {
        LoadOferteVanzare();
    }
    protected void tbCF_TextChanged(object sender, EventArgs e)
    {
        LoadOferteVanzare();
    }
    protected void tbBloc_TextChanged(object sender, EventArgs e)
    {
        LoadOferteVanzare();
    }
    protected void tbNumarCadastral_TextChanged(object sender, EventArgs e)
    {
        LoadOferteVanzare();
    }
    protected void tbNumarCadastralProvizoriu_TextChanged(object sender, EventArgs e)
    {
        LoadOferteVanzare();
    }

    //filtrari lista cereri
    protected void ofertantTextBox_TextChanged(object sender, EventArgs e)
    {
        RefreshListaCereriGridView();
    }
    #endregion

    #region refreshGriduri
    private void RefreshListaParceleGridView()
    {
        MasterModel.parcelaCF = tbCCF.Text;
        MasterModel.parcelaCategorie = ddlCCategoria.SelectedValue;
        MasterModel.parcelaNrCadastral = tbCNrCadastral.Text;
        MasterModel.parcelaNrCadastralProvizoriu = tbCNrCadastralProvizoriu.Text;
        MasterModel.parcelaTopo = tbCTopo.Text;
        MasterModel.parcelaDenumire = tbCDenumire.Text;
        parceleGridView.DataSource = VanzareTerenExtravilanServices.GetListaParceleExtravilanByUser();
        parceleGridView.DataBind();
    }

    private void LoadOferteVanzare()
    {
        MasterModel.parcelaCF = tbCF.Text;
        MasterModel.parcelaCategorie = ddlCategoria.SelectedValue;
        MasterModel.parcelaNrCadastral = tbNumarCadastral.Text;
        MasterModel.parcelaNrCadastralProvizoriu = tbNumarCadastralProvizoriu.Text;
        MasterModel.parcelaTopo = tbTopo.Text;
        MasterModel.parcelaDenumire = tbDenumireParcela.Text;
        MasterModel.nrDosar = tbNrDosar.Text;
        MasterModel.nrRegistruGeneral = tbNrRegGeneral.Text;
        oferteGridView.DataSource = VanzareTerenExtravilanServices.GetListaOferte();
        oferteGridView.DataBind();
    }

    private void RefreshListaCereriGridView()
    {
        MasterModel.numeOfertant = numeOfertantTextBox.Text;
        listaCereriGridView.DataSource = VanzareTerenExtravilanServices.GetListaCereri(Convert.ToInt16(Session["SESan"]));
        listaCereriGridView.DataBind();
    }
    #endregion

    #region vizibilitatiButoane
    private void SetVisibilityOnAdaugaOfertaButton()
    {
        adaugaDosarPanel.Visible = true;
        listaOfertePanel.Visible = false;
        salveazaDosarButton.Visible = false;
        ofertaTextBoxuriPanel.Visible = false;
        ofertaDetaliiTextBoxuriPanel.Visible = false;
        listaParcelePanel.Visible = true;
        listaParceleButton.Visible = false;
    }
    private void SetVisibilityOnModificaOfertaButton()
    {
        adaugaDosarPanel.Visible = true;
        ofertaTextBoxuriPanel.Visible = true;
        salveazaDosarButton.Visible = true;
        listaParcelePanel.Visible = true;
        listaParceleButton.Visible = false;
        listaOfertePanel.Visible = false;
        ofertaDetaliiTextBoxuriPanel.Visible = true;
    }
    private void SetVisibilityOnListaParceleButton()
    {
        listaParcelePanel.Visible = true;
        ofertaDetaliiTextBoxuriPanel.Visible = false;
        ofertaTextBoxuriPanel.Visible = false;
        listaParceleButton.Visible = false;
        salveazaDosarButton.Visible = false;
    }
    private void SetVisibilityOnSalveazaOfertaButton()
    {
        tiparesteCerereButton.Visible = false;
        tiparesteOfertaButton.Visible = false;
        tiparesteAdeverintaButton.Visible = false;
        tiparesteAdresaButton.Visible = false;
        tiparesteCerereCulturaButton.Visible = false;
        listaCereriButton.Visible = false;
    }
    private void SetVisibilityOnStergeOfertaButton()
    {
        modificaDosarButton.Visible = false;
        stergeDosarButton.Visible = false;
        tiparesteCerereButton.Visible = false;
        tiparesteOfertaButton.Visible = false;
        tiparesteAdeverintaButton.Visible = false;
        tiparesteAdresaButton.Visible = false;
        tiparesteCerereCulturaButton.Visible = false;
        listaCereriButton.Visible = false;
    }
    private void SetVisibilityOnSalveazaModificareOfertaButton()
    {
        modificaDosarButton.Visible = false;
        stergeDosarButton.Visible = false;
        listaOfertePanel.Visible = true;
    }
    private void SetVisibilityOnListaOferteButton()
    {
        adaugaDosarPanel.Visible = false;
        listaOfertePanel.Visible = true;
        modificaDosarButton.Visible = false;
        stergeDosarButton.Visible = false;
        tiparesteCerereButton.Visible = false;
        tiparesteOfertaButton.Visible = false;
        tiparesteAdeverintaButton.Visible = false;
        tiparesteAdresaButton.Visible = false;
        tiparesteCerereCulturaButton.Visible = false;
        tiparesteAcceptareButton.Visible = false;
        ofertaDetaliiTextBoxuriPanel.Visible = false;
    }
    private void SetVisibilityOnOferteGridviewSelectedIndexChanged()
    {
        modificaDosarButton.Visible = true;
        stergeDosarButton.Visible = true;
        tiparesteCerereButton.Visible = true;
        tiparesteOfertaButton.Visible = true;
        tiparesteAdeverintaButton.Visible = true;
        tiparesteAdresaButton.Visible = true;
        tiparesteCerereCulturaButton.Visible = true;
        listaCereriButton.Visible = true;
    }
    private void SetVisibilityOnSalveazaCerereButton()
    {
        adaugaCererePanel.Visible = false;
        listaCereriPanel.Visible = true;
        modificaCerereButton.Visible = false;
        stergeCerereButton.Visible = false;
        tiparesteAcceptareButton.Visible = false;
    }
    private void SetVisibilityOnStergeCerereButton()
    {
        stergeCerereButton.Visible = false;
        tiparesteAcceptareButton.Visible = false;
        modificaCerereButton.Visible = false;
    }
    #endregion
}