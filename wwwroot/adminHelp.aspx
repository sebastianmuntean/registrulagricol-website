﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="adminHelp.aspx.cs" Inherits="adminHelp" EnableEventValidation="false"
    Culture="ro-RO" UICulture="ro-RO" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Administrare / Help"></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
            <asp:Panel ID="pnAfiseazaMesaje" runat="server">
                <asp:ValidationSummary ID="valCapitole" runat="server" DisplayMode="SingleParagraph"
                    Visible="true" CssClass="validator" ForeColor="" />
            </asp:Panel>
            <asp:Panel ID="Panel5" CssClass="ascunsa" runat="server">
                <!-- //////////// VALIDATOARE  \\\\\\\\\\\\\\\\\\ -->
                <asp:CustomValidator ID="valCapitol" runat="server" ErrorMessage=""></asp:CustomValidator>
            </asp:Panel>
            <asp:Panel ID="pnGrid" runat="server" Visible="true">
                <asp:GridView ID="gvHelp" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                    BorderStyle="None" CellPadding="3" GridLines="Vertical" ShowFooter="True" CssClass="tabela"
                    DataKeyNames="helpId" DataSourceID="SqlSabloaneCapitole" EmptyDataText="Nu sunt adaugate inregistrari"
                    AllowPaging="True" OnRowDataBound="gvHelp_RowDataBound" OnSelectedIndexChanged="gvHelp_SelectedIndexChanged">
                    <Columns>
                        <asp:BoundField DataField="paginaDenumire" HeaderText="Pagina" ReadOnly="True" SortExpression="paginaDenumire" />
                        <asp:BoundField DataField="helpText" HeaderText="Text help" SortExpression="helpText" />
                    </Columns>
                    <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                    <HeaderStyle Font-Bold="True" />
                </asp:GridView>
                <asp:SqlDataSource ID="SqlSabloaneCapitole" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016 %>"
                    SelectCommand="SELECT help.helpId, help.paginaId, help.helpText, COALESCE (pagini.paginaDenumire, '') AS paginaDenumire FROM help LEFT OUTER JOIN pagini ON help.paginaId = pagini.paginaId ORDER BY paginaDenumire">
                </asp:SqlDataSource>
            </asp:Panel>
            <asp:Panel ID="pnButoanePrincipale" CssClass="butoane" runat="server">
                <asp:Button CssClass="buton" ID="btModifica" Visible="false" runat="server" Text="modifică help"
                    OnClick="btModifica_Click" />
                <asp:Button CssClass="buton" ID="btSterge" Visible="false" runat="server" Text="şterge help"
                    OnClientClick="return confirm (&quot;Sigur stergeti ?&quot;)" OnClick="btSterge_Click" />
                <asp:Button CssClass="buton" ID="btAdauga" runat="server" Text="adaugă help" OnClick="btAdauga_Click" />
            </asp:Panel>
            <asp:Panel ID="pnAdaugaHelp" CssClass="panel_general" runat="server" Visible="false">
                <asp:Panel ID="Panel1" CssClass="adauga" runat="server">
                    <asp:Panel ID="pnEroare" runat="server">
                        <h2>
                            ADAUGĂ HELP</h2>
                    </asp:Panel>
                    <asp:Panel ID="Panel3" runat="server">
                        <p>
                            <asp:Label ID="lblPagina" runat="server" Text="Pagina" />
                            <asp:DropDownList ID="ddlPagina" Width="300px" runat="server" DataSourceID="sdsPagini"
                                DataTextField="paginaDenumire" DataValueField="paginaId">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="sdsPagini" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016 %>"
                                SelectCommand="SELECT [paginaId], [paginaDenumire] FROM [pagini] ORDER BY [paginaDenumire]">
                            </asp:SqlDataSource>
                        </p>
                        <p>
                            <asp:Label ID="lblTextHelp" runat="server" Text="Text Help" />
                            <asp:TextBox ID="tbTextHelp" runat="server" TextMode="MultiLine" Rows="20" Width="800px"></asp:TextBox>

                            <script type="text/javascript">
                        //<![CDATA[

                        CKEDITOR.replace('ctl00_ContentPlaceHolder1_tbTextHelp',
					{
					    contentsCss: ['http://www.legex.ro/styles/mo.css', CKEDITOR.basePath + 'mo.css'],
					    fullPage: true,
					    	height : 600,
					    	
					    
					});

                        //]]>
                            </script>

                        </p>
                    </asp:Panel>
                    <asp:Panel ID="pnButoaneAdaugare" runat="server" CssClass="butoane">
                        <asp:Button ID="btSalveazaHelp" runat="server" CssClass="buton" Text="adaugă help"
                            OnClick="btSalveazaHelp_Click" />
                        <asp:Button ID="btAnuleazaSalvarea" runat="server" CssClass="buton" Text="listă help"
                            OnClick="btAnuleazaSalvarea_Click" />
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
</asp:Content>
