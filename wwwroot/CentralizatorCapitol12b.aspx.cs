﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

/// <summary>
/// Adaugare capitol
/// Creata la:                  18.02.2011
/// Autor:                      Laza Tudor Mihai
/// Ultima                      actualizare: 18.02.2011
/// Autor:                      Laza Tudor Mihai
/// </summary> 
public partial class CentralizatorCapitol12b : System.Web.UI.Page
{
    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion
    protected void Page_Init(object sender, EventArgs e)
    {
        CalculCapitole.PopuleazaTabelaCapitole(Convert.ToInt32(Session["SESan"]), -1, Convert.ToInt64(Session["SESunitateId"]), "c12b");
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        ViewState["TabIndex"] = 30;
        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "Centr.12b", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
        }
    }
    protected void gvCapitol_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (ViewState["Capitole"] != null)
        {
            List<Capitole> vCapitole = (List<Capitole>)ViewState["Capitole"];
            ViewState["TabIndex"] = CalculCapitole.PopulareGridView(vCapitole, this, gvCapitol, e, Convert.ToInt16(ViewState["TabIndex"]), 0);
        }
    }
    protected void gvCapitol_PreRender(object sender, EventArgs e)
    {
        List<Capitole> vCapitole = new List<Capitole>();
        vCapitole.Clear();
        vCapitole = CalculCapitole.PopuleazaViewState(gvCapitol, 0, 0, 0, 0, 0);
        ViewState.Add("Capitole", vCapitole);
        gvCapitol.DataBind();
    }
    protected void btListaGospodarii_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Gospodarii.aspx");
    }
    protected void btSalveaza_Click(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        List<Capitole> vCapitole = new List<Capitole>();
        vCapitole.Clear();
        vCapitole = CalculCapitole.PopuleazaViewState(gvCapitol, 0, 0, 0, 0, 0);
        ViewState.Add("Capitole", vCapitole);
        SqlConnection vCon = ManipuleazaBD.CreareConexiune();
        SqlTransaction vTranz = ManipuleazaBD.DeschideTranzactie(vCon);
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.Transaction = vTranz;
        try
        {
            vCapitole = (List<Capitole>)ViewState["Capitole"];
            for (int i = 0; i < vCapitole.Count; i++)
            {
                vCmd.CommandText = @"UPDATE capitole SET col1 = '" + vCapitole[i].Col1.Replace(',', '.') + "', col2 = '" + vCapitole[i].Col2.Replace(',', '.') + "', col3 = '" + vCapitole[i].Col3.Replace(',', '.') + "', col4 = '" + vCapitole[i].Col4.Replace(',', '.') + "', col5 = '" + vCapitole[i].Col5.Replace(',', '.') + "', col6 = '" + vCapitole[i].Col6.Replace(',', '.') + "', col7 = '" + vCapitole[i].Col7.Replace(',', '.') + "', col8 = '" + vCapitole[i].Col8.Replace(',', '.') + "' WHERE (capitolId = '" + vCapitole[i].Id.ToString() + "')";
                vCmd.ExecuteNonQuery();
            }
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "capitole", "salvare capitol c12b", "", vCmd, Convert.ToInt64(Session["SESgospodarieId"]), 2);
            ClassLog.UpdateDataUltimeiModificari(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESgospodarieId"]), vCmd);
            vTranz.Commit();
        }
        catch { vTranz.Rollback(); }
        finally
        {
            ManipuleazaBD.InchideConexiune(vCon);
            clsCorelatii.VerificaCorelatie(Convert.ToInt32(Session["SESunitateId"]), Convert.ToInt32(Session["SESgospodarieId"]), Convert.ToInt32(Session["SESan"]), "c12b");
            ((MasterPage)this.Page.Master).VerificaCorelatii();
        }
        Alert.Show("Capitolul centralizator 12 B a fost salvat cu succes");
    }
    protected void btTiparire_Click(object sender, EventArgs e)
    {
        clsTiparireCapitole.UmpleRapCapitole(Session["SESutilizatorId"].ToString(), Session["SESan"].ToString(), Session["SESgospodarieId"].ToString(), "12b");
        ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "capitole", "tiparire capitol c12b", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
        ResponseHelper.Redirect("~/printCentralizatorCapitolul12bUnitate.aspx?codCapitol=c12b", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
    }
    protected void gvCapitol_DataBound(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune();
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select top(1) coalesce(unitateFocusLaEnter,'False') from unitati where unitateId='" + Session["SESunitateId"].ToString() + "'";
        Int16 vTabIndex = Convert.ToInt16(((TextBox)(this.Master.FindControl("tbTabIndex"))).Text);
        if (Convert.ToBoolean(vCmd.ExecuteScalar()))
            vTabIndex = Convert.ToInt16("0");
        ControlTabIndex.SetTabIndex(this, gvCapitol, vTabIndex);
        ManipuleazaBD.InchideConexiune(vCon);
    }
}
