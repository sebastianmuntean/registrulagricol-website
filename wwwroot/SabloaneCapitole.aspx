﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" EnableEventValidation="false"
    AutoEventWireup="true" CodeFile="SabloaneCapitole.aspx.cs" Inherits="SabloaneCapitole"
    Culture="ro-RO" UICulture="ro-RO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="upSablaoneCapitole" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnListaCapitole" runat="server" CssClass="panel_general" Visible="true">
                <asp:Panel ID="pnCautare" runat="server" CssClass="cauta">
                    <asp:Label ID="lblCautaDupa" runat="server" Text="Cauta dupa:"></asp:Label>
                    <asp:Label ID="lblfAn" runat="server" Text="An"></asp:Label>
                    <asp:TextBox ID="tbfAn" runat="server" AutoPostBack="True"></asp:TextBox>
                    <asp:Label ID="lblCapitol" runat="server" Text="Capitol"></asp:Label>
                    <asp:TextBox ID="tbfCapitol" runat="server" AutoPostBack="True"></asp:TextBox>
                    <asp:Label ID="lblCodRand" runat="server" Text="Cod rand"></asp:Label>
                    <asp:TextBox ID="tbfCodRand" runat="server" AutoPostBack="True"></asp:TextBox>
                </asp:Panel>
                <asp:Panel ID="pnGrid" runat="server" Visible="true">
                    <asp:GridView ID="gvCapitole" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        BorderStyle="None" CellPadding="3" GridLines="Vertical" ShowFooter="True" CssClass="tabela"
                        OnDataBound="gvCapitole_DataBound" 
                        OnRowDataBound="gvCapitole_RowDataBound" OnSelectedIndexChanged="gvCapitole_SelectedIndexChanged"
                        DataKeyNames="sablonCapitoleId" DataSourceID="SqlSabloaneCapitole" EmptyDataText="Nu sunt adaugate inregistrari"
                        AllowPaging="True">
                        <Columns>
                            <asp:BoundField DataField="denumire1" HeaderText="Coloana 1" 
                                SortExpression="denumire1" />
                            <asp:BoundField DataField="denumire2" HeaderText="Coloana 2" 
                                SortExpression="denumire2" />
                            <asp:BoundField DataField="denumire3" HeaderText="Coloana 3" 
                                SortExpression="denumire3" />
                            <asp:BoundField DataField="denumire4" HeaderText="Coloana 4" 
                                SortExpression="denumire4" />
                            <asp:BoundField DataField="denumire5" HeaderText="Coloana 5" 
                                SortExpression="denumire5" />
                            <asp:BoundField DataField="an" HeaderText="An" SortExpression="an" />
                            <asp:BoundField DataField="capitol" HeaderText="Capitol" 
                                SortExpression="capitol" />
                            <asp:BoundField DataField="codRand" HeaderText="Cod rand" 
                                SortExpression="codRand" />
                            <asp:BoundField DataField="formula" HeaderText="Formula" 
                                SortExpression="formula" />
                        </Columns>
                        <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                        <HeaderStyle Font-Bold="True" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlSabloaneCapitole" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016%>"
                        
                        SelectCommand="SELECT sablonCapitoleId, an, capitol, codRand, formula, denumire1, denumire2, denumire3, denumire4, denumire5 FROM sabloaneCapitole WHERE (capitol LIKE @capitol) AND (CONVERT (nvarchar, an) LIKE @an) AND (CONVERT (nvarchar, codRand) LIKE @codrand) ORDER BY an, capitol, codRand">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="tbfCapitol" DefaultValue="%" Name="capitol" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfAn" DefaultValue="%" Name="an" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfCodRand" DefaultValue="%" Name="codrand" PropertyName="Text" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </asp:Panel>
                <asp:Panel ID="pnButoanePrincipale" CssClass="butoane" runat="server">
                    <asp:Button CssClass="buton" ID="btModifica" runat="server" Text="modifică sablon"
                        OnClick="btModifica_Click" />
                    <asp:Button CssClass="buton" ID="btSterge" runat="server" Text="șterge sablon" OnClick="btSterge_Click"
                        OnClientClick="return confirm (&quot;Sigur stergeti ?&quot;)" />
                    <asp:Button CssClass="buton" ID="btAdauga" runat="server" Text="adaugă sablon" OnClick="btAdauga_Click" />
                </asp:Panel>
                <!-- adaugam unitate -->
                <asp:Panel ID="pnAdaugaSabloneCapitole" CssClass="panel_general" runat="server" Visible="false">
                    <asp:Panel ID="Panel1" CssClass="adauga" runat="server">
                        <asp:Panel ID="pnEroare" runat="server">
                            <h2>
                                ADAUGĂ ȘABLOANE PENTRU CAPITOLE</h2>
                            <asp:Label ID="lblEroare" runat="server" CssClass="validator" Text="Eroare" Visible="false" />
                            <asp:ValidationSummary ID="valSumUnitati" runat="server" DisplayMode="SingleParagraph"
                                Visible="true" ValidationGroup="GrupValidareSabloaneCapitole" CssClass="validator"
                                ForeColor="" />
                        </asp:Panel>
                        <asp:Panel ID="Panel3" runat="server">
                            <p>
                                <asp:Label ID="lblCol1" runat="server" Text="Coloana 1" />
                                <asp:TextBox ID="tbCol1" runat="server" Width="300px"></asp:TextBox>
                            </p>
                            <p>
                                <asp:Label ID="lblCol2" runat="server" Text="Coloana 2" />
                                <asp:TextBox ID="tbCol2" runat="server" Width="300px"></asp:TextBox>
                            </p>
                            <p>
                                <asp:Label ID="lblCol3" runat="server" Text="Coloana 3" />
                                <asp:TextBox ID="tbCol3" runat="server" Width="300px"></asp:TextBox>
                            </p>
                            <p>
                                <asp:Label ID="lblCol4" runat="server" Text="Coloana 4" />
                                <asp:TextBox ID="tbCol4" runat="server" Width="300px"></asp:TextBox>
                            </p>
                            <p>
                                <asp:Label ID="lblCol5" runat="server" Text="Coloana 5" />
                                <asp:TextBox ID="tbCol5" runat="server" Width="300px"></asp:TextBox>
                            </p>
                            <p>
                                <asp:Label ID="lblAn" runat="server" Text="An" />
                                <asp:TextBox ID="tbAn" runat="server" Width="100px"></asp:TextBox>
                                <asp:Label ID="lbCapitol" runat="server" Text="Capitol" />
                                <asp:TextBox ID="tbCapitol" runat="server" Width="100px"></asp:TextBox>
                            </p>
                            <p>
                                <asp:Label ID="lbCodRand" runat="server" Text="Cod rand" />
                                <asp:TextBox ID="tbCodRand" runat="server" Width="100px"></asp:TextBox>
                                <asp:Label ID="lbFormula" runat="server" Text="Formula" />
                                <asp:TextBox ID="tbFormula" runat="server"></asp:TextBox>
                            </p>
                        </asp:Panel>
                        <asp:Panel ID="PanelValidatoareAscuns" CssClass="ascunsa" runat="server">
                            <!-- //////////// VALIDATOARE  \\\\\\\\\\\\\\\\\\ -->
                            
                            <asp:RequiredFieldValidator ID="ValidatorAn" runat="server" ErrorMessage="Campul an este obligatoriu!"
                                Display="Dynamic" ValidationGroup="GrupValidareSabloaneCapitole" ControlToValidate="tbAn"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="ValidatorCapitol" runat="server" ErrorMessage="Campul capitol este obligatoriu!"
                                ControlToValidate="tbCapitol" Display="Dynamic" ValidationGroup="GrupValidareSabloaneCapitole"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="ValidareCodRand" runat="server" ErrorMessage="Campul cod rand este obligatoriu!"
                                ControlToValidate="tbCodRand" Display="Dynamic" ValidationGroup="GrupValidareSabloaneCapitole"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="ValidareFormula" runat="server" ErrorMessage="Campul formula este obligatoriu!"
                                ControlToValidate="tbFormula" Display="Dynamic" ValidationGroup="GrupValidareSabloaneCapitole"></asp:RequiredFieldValidator>
                            <asp:CustomValidator ID="VerDacaExistaCodRand" runat="server" ErrorMessage="Acest cod mai exista odata pentru anul selectat!"
                                OnServerValidate="VerDacaExiataCodRand_ServerValidate" ValidationGroup="GrupValidareSabloaneCapitole"></asp:CustomValidator>
                            <!-- //////////// VALIDATOARE  \\\\\\\\\\\\\\\\\\ -->
                        </asp:Panel>
                        <asp:Panel ID="pnButoaneAdaugare" runat="server" CssClass="butoane">
                            <asp:Button ID="btSalveazaSablon" runat="server" CssClass="buton" Text="adauga un șablon"
                                ValidationGroup="GrupValidareSabloaneCapitole" OnClick="btSalveazaSablon_Click" />
                            <asp:Button ID="btAnuleazaSalvarea" runat="server" CssClass="buton" Text="lista sabloane"
                                OnClick="btAnuleazaSalvarea_Click" />
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
