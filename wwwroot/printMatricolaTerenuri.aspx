﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="printMatricolaTerenuri.aspx.cs" Inherits="printMatricolaTerenuri"  Culture="ro-RO" UICulture="ro-RO" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" 
        Font-Size="8pt" Height="400px" Width="400px">
        <localreport reportpath="rapoarte\raportMatricolaTerenuri.rdlc">
            <datasources>
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" 
                    Name="dsRapoarte_dtMatricolaTerenuri" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource2" 
                    Name="dsRapoarte_dtUnitati" />
            </datasources>
        </localreport>
    </rsweb:ReportViewer>
 
        <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" 
        SelectMethod="GetData" 
        TypeName="dsRapoarteTableAdapters.dtUnitatiTableAdapter" 
        OldValuesParameterFormatString="original_{0}">
            <SelectParameters>
                <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" 
                    Type="Int32" />
            </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="dsRapoarteTableAdapters.dtMatricolaTerenuriTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="%" Name="volum" QueryStringField="vol" 
                Type="String" />
            <asp:QueryStringParameter DefaultValue="" Name="nrint1" QueryStringField="nr1" 
                Type="Int32" />
            <asp:QueryStringParameter Name="nrint2" QueryStringField="nr2" Type="Int32" />
            <asp:QueryStringParameter DefaultValue="%" Name="strainas" 
                QueryStringField="strainas" Type="String" />
            <asp:QueryStringParameter DefaultValue="%" Name="strada" QueryStringField="str" 
                Type="String" />
            <asp:QueryStringParameter DefaultValue="%" Name="unitateid" 
                QueryStringField="unit" Type="String" />
            <asp:QueryStringParameter DefaultValue="%" Name="persJuridica" 
                QueryStringField="pj" Type="String" />
            <asp:SessionParameter Name="an" SessionField="SESan" Type="Int32" />
            <asp:QueryStringParameter DefaultValue="" Name="judetId" 
                QueryStringField="judet" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>

