﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class reparaPreiaDin2aIn3 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btAdauga_Click(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd1 = new SqlCommand();
        SqlCommand vCmd2 = new SqlCommand();
        vCmd1.Connection = vCon;
        vCmd2.Connection = vCon;
        vCmd2.CommandText = "SELECT col1, col2, gospodarieId, an FROM capitole WHERE (unitateId = '" + ddlUnitate.SelectedValue + "') AND (codCapitol = '2a') AND (codRand = '10') AND (col1 + col2 > 0) AND (an = '" + Session["SESan"].ToString() + "')";
        SqlDataReader vTabel = vCmd2.ExecuteReader();
        while (vTabel.Read())
        {
            vCmd1.CommandText = "select count(*) from capitole where gospodarieId='" + vTabel["gospodarieId"].ToString() + "' and an='" + vTabel["an"].ToString() + "' and codCapitol='3'";
            if (Convert.ToInt32(vCmd1.ExecuteScalar()) > 0)
            {
                // daca are pusa bifa verific daca are completate date
                vCmd1.CommandText = "select count(*) from capitole where gospodarieId='" + vTabel["gospodarieId"].ToString() + "' and an='" + vTabel["an"].ToString() + "' and codCapitol='3' and col1+col2>0";
                if (cbPreiaDoar0.Checked && Convert.ToInt32(vCmd1.ExecuteScalar()) > 0)
                    continue;
                else
                {
                    // fac update la randul 1 si la randul 17
                    List<string> vListaCapitolId = new List<string>();
                    vCmd1.CommandText = "select * from capitole where gospodarieId='" + vTabel["gospodarieId"].ToString() + "' and an='" + vTabel["an"].ToString() + "' and codCapitol='3' and codRand in ('1', '17')";
                    SqlDataReader vTabel1 = vCmd1.ExecuteReader();
                    while (vTabel1.Read())
                        vListaCapitolId.Add(vTabel1["capitolId"].ToString());
                    vTabel1.Close();
                    vCmd1.CommandText = "";
                    foreach (string a in vListaCapitolId)
                    {
                        vCmd1.CommandText += "update capitole set col1='" + vTabel["col1"].ToString().Replace(",", ".") + "', col2='" + vTabel["col2"].ToString().Replace(",", ".") + "' where capitolId='" + a.ToString() + "'; ";
                    }
                    if (vCmd1.CommandText != "")
                        vCmd1.ExecuteNonQuery();
                }
            }
            else
            {
                // adaug inregistrari noi
                List<string> vListaRanduri = new List<string>();
                vCmd1.CommandText = "select codRand from sabloaneCapitole where an='" + Session["SESan"].ToString() + "' and capitol='3'";
                SqlDataReader vTabel1 = vCmd1.ExecuteReader();
                while (vTabel1.Read())
                    vListaRanduri.Add(vTabel1["codRand"].ToString());
                vTabel1.Close();
                vCmd1.CommandText = "";
                foreach (string a in vListaRanduri)
                {
                    vCmd1.CommandText += "INSERT INTO capitole (unitateId, gospodarieId, an, codCapitol, codRand, col1, col2, col3, col4, col5, col6, col7, col8) VALUES ('" + ddlUnitate.SelectedValue + "', '" + vTabel["gospodarieId"].ToString() + "', '" + vTabel["an"].ToString() + "', '3', '" + a + "', '" + vTabel["col1"].ToString().Replace(",", ".") + "', '" + vTabel["col2"].ToString().Replace(",", ".") + "', 0, 0, 0, 0, 0, 0); ";
                }
                if (vCmd1.CommandText != "")
                    vCmd1.ExecuteNonQuery();
            }
        }
        vTabel.Close();
        ManipuleazaBD.InchideConexiune(vCon);
    }
}
