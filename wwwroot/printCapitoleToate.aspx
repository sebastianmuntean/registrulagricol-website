﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="printCapitoleToate.aspx.cs" Inherits="printCapitoleToate" Culture="ro-RO" UICulture="ro-RO"  %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" 
        Font-Size="8pt" Height="400px" Width="400px">
        <LocalReport ReportPath="rapoarte\raportCapitoleToate.rdlc">
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" 
                    Name="dsRapoarte_dtUnitati" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource2" 
                    Name="dsRapoarte_dtCapitoleToate" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource3" 
                    Name="dsRapoarte_dtMembri" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource4" 
                    Name="dsRapoarte_dtParcele" />
              <%--  <rsweb:ReportDataSource DataSourceId="ObjectDataSource5" 
                    Name="dsRapoarte_dtPaduri" />--%>
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource6" 
                    Name="dsRapoarte_dtGospodarii" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource7" 
                    Name="dsRapoarte_dtMentiuni" />
            </DataSources>
        </LocalReport>
    </rsweb:ReportViewer>
    <asp:ObjectDataSource ID="ObjectDataSource7" runat="server" 
        InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" 
        SelectMethod="GetData" 
        TypeName="dsRapoarteTableAdapters.dtMentiuniTableAdapter">
        <SelectParameters>
            <asp:SessionParameter Name="gospodarieId" SessionField="SESgospodarieId" 
                Type="Int64" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="mentiuneText" Type="String" />
        </InsertParameters>
    </asp:ObjectDataSource>

    <asp:ObjectDataSource ID="ObjectDataSource6" runat="server" 
        DeleteMethod="Delete" InsertMethod="Insert" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="dsRapoarteTableAdapters.dtGospodariiTableAdapter" 
        UpdateMethod="Update">
        <DeleteParameters>
            <asp:Parameter Name="Original_gospodarieId" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="unitateId" Type="Int32" />
            <asp:Parameter Name="volum" Type="String" />
            <asp:Parameter Name="nrPozitie" Type="String" />
            <asp:Parameter Name="codSiruta" Type="String" />
            <asp:Parameter Name="tip" Type="String" />
            <asp:Parameter Name="nrInt" Type="Int32" />
            <asp:Parameter Name="codExploatatie" Type="String" />
            <asp:Parameter Name="codUnic" Type="String" />
            <asp:Parameter Name="judet" Type="String" />
            <asp:Parameter Name="localitate" Type="String" />
            <asp:Parameter Name="persJuridica" Type="Boolean" />
            <asp:Parameter Name="jUnitate" Type="String" />
            <asp:Parameter Name="jSubunitate" Type="String" />
            <asp:Parameter Name="jCodFiscal" Type="String" />
            <asp:Parameter Name="jNumeReprez" Type="String" />
            <asp:Parameter Name="strainas" Type="Boolean" />
            <asp:Parameter Name="sStrada" Type="String" />
            <asp:Parameter Name="sNr" Type="String" />
            <asp:Parameter Name="sBl" Type="String" />
            <asp:Parameter Name="sSc" Type="String" />
            <asp:Parameter Name="sEtj" Type="String" />
            <asp:Parameter Name="sAp" Type="String" />
            <asp:Parameter Name="sJudet" Type="String" />
            <asp:Parameter Name="sLocalitate" Type="String" />
            <asp:Parameter Name="Original_gospodarieId" Type="Int32" />
        </UpdateParameters>
        <SelectParameters>
            <asp:SessionParameter Name="gospodarieId" SessionField="SESgospodarieId" 
                Type="Int32" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="unitateId" Type="Int32" />
            <asp:Parameter Name="volum" Type="String" />
            <asp:Parameter Name="nrPozitie" Type="String" />
            <asp:Parameter Name="codSiruta" Type="String" />
            <asp:Parameter Name="tip" Type="String" />
            <asp:Parameter Name="nrInt" Type="Int32" />
            <asp:Parameter Name="codExploatatie" Type="String" />
            <asp:Parameter Name="codUnic" Type="String" />
            <asp:Parameter Name="judet" Type="String" />
            <asp:Parameter Name="localitate" Type="String" />
            <asp:Parameter Name="persJuridica" Type="Boolean" />
            <asp:Parameter Name="jUnitate" Type="String" />
            <asp:Parameter Name="jSubunitate" Type="String" />
            <asp:Parameter Name="jCodFiscal" Type="String" />
            <asp:Parameter Name="jNumeReprez" Type="String" />
            <asp:Parameter Name="strainas" Type="Boolean" />
            <asp:Parameter Name="sStrada" Type="String" />
            <asp:Parameter Name="sNr" Type="String" />
            <asp:Parameter Name="sBl" Type="String" />
            <asp:Parameter Name="sSc" Type="String" />
            <asp:Parameter Name="sEtj" Type="String" />
            <asp:Parameter Name="sAp" Type="String" />
            <asp:Parameter Name="sJudet" Type="String" />
            <asp:Parameter Name="sLocalitate" Type="String" />
        </InsertParameters>
    </asp:ObjectDataSource>
    
    <%--<asp:ObjectDataSource ID="ObjectDataSource5" runat="server" 
        DeleteMethod="Delete" InsertMethod="Insert" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="dsRapoarteTableAdapters.dtPaduriTableAdapter" UpdateMethod="Update">
        <DeleteParameters>
            <asp:Parameter Name="Original_paduriId" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="paduriProprietar" Type="String" />
            <asp:Parameter Name="paduriCodRand" Type="Int32" />
            <asp:Parameter Name="paduriUnitateDeProductie" Type="String" />
            <asp:Parameter Name="paduriUnitateaAmenajistica" Type="String" />
            <asp:Parameter Name="paduriSuprafataHa" Type="Int32" />
            <asp:Parameter Name="an" Type="Int32" />
            <asp:Parameter Name="paduriSuprafataAri" Type="Decimal" />
            <asp:Parameter Name="unitateId" Type="Int32" />
            <asp:Parameter Name="gospodarieId" Type="Int32" />
            <asp:Parameter Name="paduriSuprafataGrupa2Ha" Type="Int32" />
            <asp:Parameter Name="paduriSuprafataGrupa2Ari" Type="Decimal" />
            <asp:Parameter Name="Original_paduriId" Type="Int32" />
        </UpdateParameters>
        <SelectParameters>
            <asp:SessionParameter Name="an" SessionField="SESan" Type="Int32" />
            <asp:SessionParameter Name="gospodarieId" SessionField="SESgospodarieId" 
                Type="Int32" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="paduriProprietar" Type="String" />
            <asp:Parameter Name="paduriCodRand" Type="Int32" />
            <asp:Parameter Name="paduriUnitateDeProductie" Type="String" />
            <asp:Parameter Name="paduriUnitateaAmenajistica" Type="String" />
            <asp:Parameter Name="paduriSuprafataHa" Type="Int32" />
            <asp:Parameter Name="an" Type="Int32" />
            <asp:Parameter Name="paduriSuprafataAri" Type="Decimal" />
            <asp:Parameter Name="unitateId" Type="Int32" />
            <asp:Parameter Name="gospodarieId" Type="Int32" />
            <asp:Parameter Name="paduriSuprafataGrupa2Ha" Type="Int32" />
            <asp:Parameter Name="paduriSuprafataGrupa2Ari" Type="Decimal" />
        </InsertParameters>
    </asp:ObjectDataSource>--%>
   
    <asp:ObjectDataSource ID="ObjectDataSource4" runat="server" 
        DeleteMethod="Delete" InsertMethod="Insert" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="dsRapoarteTableAdapters.dtParceleTableAdapter" UpdateMethod="Update">
        <DeleteParameters>
            <asp:Parameter Name="Original_parcelaId" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="parcelaDenumire" Type="String" />
            <asp:Parameter Name="parcelaCodRand" Type="Int32" />
            <asp:Parameter Name="parcelaSuprafataIntravilanHa" Type="Int32" />
            <asp:Parameter Name="parcelaSuprafataIntravilanAri" Type="Decimal" />
            <asp:Parameter Name="parcelaSuprafataExtravilanHa" Type="Int32" />
            <asp:Parameter Name="parcelaSuprafataExtravilanAri" Type="Decimal" />
            <asp:Parameter Name="parcelaNrTopo" Type="String" />
            <asp:Parameter Name="parcelaCF" Type="String" />
            <asp:Parameter Name="parcelaCategorie" Type="String" />
            <asp:Parameter Name="parcelaNrBloc" Type="String" />
            <asp:Parameter Name="parcelaMentiuni" Type="String" />
            <asp:Parameter Name="unitateaId" Type="Int32" />
            <asp:Parameter Name="gospodarieId" Type="Int32" />
            <asp:Parameter Name="an" Type="Int32" />
            <asp:Parameter Name="capitolId" Type="Int32" />
            <asp:Parameter Name="parcelaNrCadastral" Type="String" />
            <asp:Parameter Name="parcelaNrCadastralProvizoriu" Type="String" />
            <asp:Parameter Name="parcelaLocalitate" Type="String" />
            <asp:Parameter Name="parcelaAdresa" Type="String" />
            <asp:Parameter Name="volum" Type="String" />
            <asp:Parameter Name="nrPozitie" Type="String" />
            <asp:Parameter Name="importId" Type="Int32" />
            <asp:Parameter Name="Original_parcelaId" Type="Int32" />
        </UpdateParameters>
        <SelectParameters>
            <asp:SessionParameter Name="an" SessionField="SESan" Type="Int32" />
            <asp:SessionParameter Name="gospodarieId" SessionField="SESgospodarieId" 
                Type="Int32" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="parcelaDenumire" Type="String" />
            <asp:Parameter Name="parcelaCodRand" Type="Int32" />
            <asp:Parameter Name="parcelaSuprafataIntravilanHa" Type="Int32" />
            <asp:Parameter Name="parcelaSuprafataIntravilanAri" Type="Decimal" />
            <asp:Parameter Name="parcelaSuprafataExtravilanHa" Type="Int32" />
            <asp:Parameter Name="parcelaSuprafataExtravilanAri" Type="Decimal" />
            <asp:Parameter Name="parcelaNrTopo" Type="String" />
            <asp:Parameter Name="parcelaCF" Type="String" />
            <asp:Parameter Name="parcelaCategorie" Type="String" />
            <asp:Parameter Name="parcelaNrBloc" Type="String" />
            <asp:Parameter Name="parcelaMentiuni" Type="String" />
            <asp:Parameter Name="unitateaId" Type="Int32" />
            <asp:Parameter Name="gospodarieId" Type="Int32" />
            <asp:Parameter Name="an" Type="Int32" />
            <asp:Parameter Name="capitolId" Type="Int32" />
            <asp:Parameter Name="parcelaNrCadastral" Type="String" />
            <asp:Parameter Name="parcelaNrCadastralProvizoriu" Type="String" />
            <asp:Parameter Name="parcelaLocalitate" Type="String" />
            <asp:Parameter Name="parcelaAdresa" Type="String" />
            <asp:Parameter Name="volum" Type="String" />
            <asp:Parameter Name="nrPozitie" Type="String" />
            <asp:Parameter Name="importId" Type="Int32" />
        </InsertParameters>
    </asp:ObjectDataSource>
    
    <asp:ObjectDataSource ID="ObjectDataSource3" runat="server" 
        DeleteMethod="Delete" InsertMethod="Insert" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="dsRapoarteTableAdapters.dtMembriTableAdapter" UpdateMethod="Update">
        <DeleteParameters>
            <asp:Parameter Name="Original_capitolId" Type="Int64" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="unitateId" Type="Int32" />
            <asp:Parameter Name="gospodarieId" Type="Int32" />
            <asp:Parameter Name="an" Type="Int32" />
            <asp:Parameter Name="nume" Type="String" />
            <asp:Parameter Name="codRand" Type="Int32" />
            <asp:Parameter Name="cnp" Type="String" />
            <asp:Parameter Name="codSex" Type="String" />
            <asp:Parameter Name="codRudenie" Type="Int32" />
            <asp:Parameter Name="denumireRudenie" Type="String" />
            <asp:Parameter Name="dataNasterii" Type="DateTime" />
            <asp:Parameter Name="mentiuni" Type="String" />
            <asp:Parameter Name="Original_capitolId" Type="Int64" />
        </UpdateParameters>
        <SelectParameters>
            <asp:SessionParameter Name="gospodarieId" SessionField="SESgospodarieId" 
                Type="Int32" />
            <asp:SessionParameter Name="an" SessionField="SESan" Type="Int32" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="unitateId" Type="Int32" />
            <asp:Parameter Name="gospodarieId" Type="Int32" />
            <asp:Parameter Name="an" Type="Int32" />
            <asp:Parameter Name="nume" Type="String" />
            <asp:Parameter Name="codRand" Type="Int32" />
            <asp:Parameter Name="cnp" Type="String" />
            <asp:Parameter Name="codSex" Type="String" />
            <asp:Parameter Name="codRudenie" Type="Int32" />
            <asp:Parameter Name="denumireRudenie" Type="String" />
            <asp:Parameter Name="dataNasterii" Type="DateTime" />
            <asp:Parameter Name="mentiuni" Type="String" />
        </InsertParameters>
    </asp:ObjectDataSource>
    
    <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="dsRapoarteTableAdapters.dtCapitoleToateTableAdapter">
        <SelectParameters>
            <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" 
                Type="Int32" />
            <asp:SessionParameter Name="gopodarieId" SessionField="SESgospodarieId" 
                Type="Int32" />
            <asp:SessionParameter Name="an" SessionField="SESan" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="dsRapoarteTableAdapters.dtUnitatiTableAdapter">
        <SelectParameters>
            <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" 
                Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>

