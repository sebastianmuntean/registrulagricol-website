﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class reparaStrainasi : System.Web.UI.Page
{
    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btReparaTipGospodarie_Click(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd1 = new SqlCommand();
        SqlCommand vCmd2 = new SqlCommand();
        vCmd1.Connection = vCon;
        vCmd2.Connection = vCon;
        // parcurg toate gospodariile pentru unitatea aleasa
        vCmd2.CommandText = "select * from gospodarii where unitateId='" + ddlUnitati.SelectedValue + "' AND an='" + Session["SESan"].ToString() + "'";
        SqlDataReader vTabel = vCmd2.ExecuteReader();
        int vNr = 0;
        while (vTabel.Read())
        { 
            // daca nu este strainas tip = 1, daca este strainas tip = 2, daca este pj tip = 3, daca este pj strainas tip = 4
            bool vStrainas = Convert.ToBoolean(vTabel["strainas"].ToString());
            bool vPersJuridica = Convert.ToBoolean(vTabel["persJuridica"].ToString());
            int vTip = 1;
            if (!vPersJuridica)
            {
                // PF
                if (!vStrainas)
                    vTip = 1;
                else vTip = 2;
            }
            else
            { 
                // PJ
                if (!vStrainas)
                    vTip = 3;
                else vTip = 4;
            }
            // modific tip in BD
            vCmd1.CommandText = "update gospodarii set tip='" + vTip.ToString() + "' where gospodarieId='" + vTabel["gospodarieId"].ToString() + "'";
            vCmd1.ExecuteNonQuery();
            vNr++;
        }
        vTabel.Close();
        ManipuleazaBD.InchideConexiune(vCon);
        valCapitol.IsValid = false;
        valCapitol.ErrorMessage = "Am reparat " + vNr.ToString() + " gospodarii";
    }
    protected void ddlUnitati_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlLocalitate.DataBind();
    }
    protected void btReparaAdresaStrainasi_Click(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd1 = new SqlCommand();
        SqlCommand vCmd2 = new SqlCommand();
        vCmd1.Connection = vCon;
        vCmd2.Connection = vCon;
        vCmd2.CommandText = "select * from gospodarii where unitateId='" + ddlUnitati.SelectedValue + "' AND (sLocalitate = '') AND (strainas = 1) AND an='" + Session["SESan"].ToString() + "'";
        SqlDataReader vTabel = vCmd2.ExecuteReader();
        int vNr = 0;
        while (vTabel.Read())
        {
            vCmd1.CommandText = "update gospodarii set sStrada=N'" + vTabel["strada"].ToString() + "', sNr ='" + vTabel["nr"].ToString() + "', sBl='" + vTabel["bl"].ToString() + "', sSc='" + vTabel["sc"].ToString() + "', sEtj='" + vTabel["et"].ToString() + "', sAp='" + vTabel["ap"].ToString() + "', sJudet=N'" + vTabel["judet"].ToString() + "', sLocalitate=N'" + vTabel["localitate"].ToString() + "', strada='-', nr='-', nrInt='0', bl='-', sc='-', et='-', ap='-', judet=N'" + tbJudet.Text + "', localitate=N'" + ddlLocalitate.SelectedValue + "' where gospodarieId='" + vTabel["gospodarieId"].ToString() + "'";
            vCmd1.ExecuteNonQuery();
            vNr++;
        }
        vTabel.Close();
        ManipuleazaBD.InchideConexiune(vCon);
        valCapitol.IsValid = false;
        valCapitol.ErrorMessage = "Am reparat " + vNr.ToString() + " gospodarii";
    }
}
