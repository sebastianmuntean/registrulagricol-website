﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Data.SqlClient;

public partial class reparaMentiuni : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    public static string vFisier = "";
    protected string XMLAlegeFisier(object sender, EventArgs e)
    {
        string filepath = "";
        if (fuFisier.HasFile)
        {
            // alegem fisierul de importat
            filepath = fuFisier.PostedFile.FileName;
            vFisier = filepath;
            if (filepath.IndexOf(".xml") != (filepath.Length - 4))
            {
                valCustom.IsValid = false;
                valCustom.ErrorMessage = "Fișierul pentru importul datelor trebuie sa fie de tip .zip sau .xml!";

                //       lblMesaj1.Text = "S-a ales fişierul " + filepath;
                //       upStare.Update();
                filepath = ""; return filepath;
            }
            // il copiem in directorul de importuri
            //      GridView1.Rows[0].Cells[1].Text = "sadgfdsgsg";
            //lblMesaj1.Text = "Se copiaza pe server fişierul " + filepath;
            //      GridView1.DataBind();
            //      upStare.Update();
            try
            {
                fuFisier.PostedFile.SaveAs(Server.MapPath(".\\Exporturi\\Importuri\\") + filepath);
                //           lblMesaj1.Text = "S-a copiat pe server fişierul " + filepath;
            }
            catch
            {
                //            lblMesaj1.Text = "A intervenit o eroare la copierea pe server a fisierului " + filepath;
            }
            //      upStare.Update();
            //if (filepath.IndexOf(".zip") == (filepath.Length - 4))
            //{
            //    XMLUnZip(Server.MapPath(".\\Exporturi\\Importuri\\") + filepath);
            //    filepath = Server.MapPath(".\\Exporturi\\Importuri\\") + filepath.Remove(filepath.IndexOf(".zip"));
            //}
            //else
            {
                filepath = Server.MapPath(".\\Exporturi\\Importuri\\") + filepath;
            }
            //     upStare.Update();

        }
        return filepath;
    }
    protected void btAdauga_Click(object sender, EventArgs e)
    {
        if (fuFisier.HasFile)
        {
            string vCaleImportZip = XMLAlegeFisier(sender, e);
            SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
            SqlCommand vCmd = new SqlCommand();
            vCmd.Connection = vCon;
            XmlTextReader reader = new XmlTextReader(vCaleImportZip);
            string vUnitateCodFiscal = "";
            while (reader.Read())
            {
                if (reader.Name == "Export")
                    vUnitateCodFiscal = reader.GetAttribute("unitateCodFiscal");
                if (vUnitateCodFiscal == "")
                    continue;
                // iau id unitate
                vCmd.CommandText = "select unitateId from unitati where unitateCodFiscal='" + vUnitateCodFiscal + "'";
                string vUnitateId = "0";
                try { vUnitateId = vCmd.ExecuteScalar().ToString(); }
                catch { continue; }
                string vVolum = reader.GetAttribute("volum"), vPozitie = reader.GetAttribute("pozitie");
                vCmd.CommandText = "select top(1) gospodarieId from gospodarii where volum='" + vVolum + "' and nrPozitie='" + vPozitie + "' and unitateId='" + vUnitateId + "'";
                if (vVolum == "VOL 01-1-I" && vPozitie == "1")
                { }
                string vGospodarieId = "0";
                try { vGospodarieId = vCmd.ExecuteScalar().ToString(); }
                catch { continue; }
                // adaugare in baza de date
                vCmd.CommandText = "INSERT INTO mentiuni (unitateId, gospodarieId, mentiuneText) VALUES ('" + vUnitateId + "', '" + vGospodarieId + "', N'" + reader.GetAttribute("mentiuneText") + "')";
                vCmd.ExecuteNonQuery();
            }
            reader.Close();
            ManipuleazaBD.InchideConexiune(vCon);
        }
    }
}
