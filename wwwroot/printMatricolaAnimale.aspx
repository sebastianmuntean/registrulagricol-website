﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="printMatricolaAnimale.aspx.cs" Inherits="printMatricolaAnimale"  Culture="ro-RO" UICulture="ro-RO" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" 
        Font-Size="8pt" Height="400px" Width="400px">
        <LocalReport ReportPath="rapoarte\raportMatricolaAnimale.rdlc">
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" 
                    Name="dsRapoarte_dtUnitati" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource2" 
                    Name="dsRapoarte_dtAnimale" />
            </DataSources>
        </LocalReport>
    </rsweb:ReportViewer>
    <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="dsRapoarteTableAdapters.dtAnimaleTableAdapter" onselecting="ObjectDataSource2_Selecting">
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="%" Name="volum" QueryStringField="vol" 
                Type="String" />
            <asp:QueryStringParameter DefaultValue="" Name="nr1" QueryStringField="nr1" 
                Type="Int32" />
            <asp:QueryStringParameter Name="nr2" QueryStringField="nr2" Type="Int32" />
            <asp:QueryStringParameter DefaultValue="%" Name="strainas" 
                QueryStringField="strainas" Type="String" />
            <asp:QueryStringParameter DefaultValue="%" Name="strada" QueryStringField="str" 
                Type="String" />
            <asp:QueryStringParameter DefaultValue="%" Name="persJuridica" 
                QueryStringField="pj" Type="String" />
            <asp:QueryStringParameter DefaultValue="%" Name="localitate" 
                QueryStringField="loc" Type="String" />
            <asp:SessionParameter Name="an" SessionField="SESan" Type="Int32" />
            <asp:QueryStringParameter DefaultValue="%" Name="unitate" 
                QueryStringField="unit" Type="String" />
            <asp:QueryStringParameter DefaultValue="" Name="judetId" 
                QueryStringField="judet" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="dsRapoarteTableAdapters.dtUnitatiTableAdapter" 
        >
        <SelectParameters>
            <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" 
                Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    </asp:Content>

