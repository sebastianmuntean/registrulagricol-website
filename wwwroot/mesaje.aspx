﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="mesaje.aspx.cs" Inherits="mesaje" EnableEventValidation="false" Culture="ro-RO" UICulture="ro-RO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Mesaje" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="pnAfiseazaMesaje" runat="server">
        <asp:ValidationSummary ID="valCapitole" runat="server" DisplayMode="SingleParagraph"
            Visible="true" CssClass="validator" ForeColor="" />
    </asp:Panel>
    <asp:Panel ID="Panel5" CssClass="ascunsa" runat="server">
        <!-- //////////// VALIDATOARE  \\\\\\\\\\\\\\\\\\ -->
        <asp:CustomValidator ID="valCapitol" runat="server" ErrorMessage=""></asp:CustomValidator>
    </asp:Panel>
    <asp:Panel ID="pnListaMesaje" runat="server" CssClass="panel_general" Visible="true">
        <asp:Panel ID="pnCautare" runat="server" CssClass="cauta">
        </asp:Panel>
        <h2>Mesaje primite</h2>
        <asp:Panel ID="pnGrid" runat="server" Visible="true">
            <asp:GridView ID="gvMesaje" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                BorderStyle="None" CellPadding="3" GridLines="Vertical" ShowFooter="True" CssClass="tabela"
                EmptyDataText="Nu sunt adaugate inregistrari" AllowPaging="True" DataKeyNames="mesajId"
                DataSourceID="SqlMesaje" OnRowDataBound="gvMesaje_RowDataBound" OnSelectedIndexChanged="gvMesaje_SelectedIndexChanged">
                <Columns>
                    <asp:BoundField DataField="unitateDenumire" HeaderText="mesaj de la unitatea" SortExpression="unitateDenumire" />
                    <asp:BoundField DataField="utilizatorNumePrenume" HeaderText="mesaj de la utilizatorul"
                        ReadOnly="True" SortExpression="utilizatorNumePrenume" />
                    <asp:BoundField DataField="subiect" HeaderText="Subiect" SortExpression="subiect" />
                    <asp:BoundField DataField="dataAdaugarii" HeaderText="Data primirii" SortExpression="dataAdaugarii" />
                    <asp:BoundField DataField="dataCitirii" HeaderText="Data citirii" SortExpression="dataCitirii" />
                    <asp:TemplateField Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblMesajId" Visible="False" runat="server" Text='<%# Eval("mesajId") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                <HeaderStyle Font-Bold="True" />
            </asp:GridView>
            <asp:SqlDataSource ID="SqlMesaje" runat="server" 
                SelectCommand="SELECT mesaje.mesajId, mesaje.dataAdaugarii, mesaje.dataCitirii, mesaje.subiect, utilizatori.utilizatorNume + ' ' + utilizatori.utilizatorPrenume AS utilizatorNumePrenume, unitati.unitateDenumire FROM mesaje INNER JOIN utilizatori ON mesaje.utilizatorIdDeLa = utilizatori.utilizatorId INNER JOIN unitati ON utilizatori.unitateId = unitati.unitateId WHERE (mesaje.utilizatorId = @utilizatorId) ORDER BY mesaje.dataAdaugarii DESC">
                <SelectParameters>
                    <asp:SessionParameter Name="utilizatorId" SessionField="SESutilizatorId" />
                </SelectParameters>
            </asp:SqlDataSource>
        </asp:Panel>
        <!-- adaugam unitate -->
        <asp:Panel ID="pnDetaliiMesaj" CssClass="panel_general" runat="server" Visible="false">
            <asp:Panel ID="Panel1" CssClass="adauga" runat="server">
                <asp:Panel ID="pnEroare" runat="server">
                    <h2>DETALII MESAJ PRIMIT</h2>
                    <p>
                        <asp:Label ID="lblMesaj" runat="server" Text=""></asp:Label>
                    </p>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
        <h2>Mesaje trimise</h2>
        <asp:Panel ID="pnGridTrimis" runat="server" Visible="true">
            <asp:GridView ID="gvMesajeTrimise" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                BorderStyle="None" CellPadding="3" GridLines="Vertical" ShowFooter="True" CssClass="tabela"
                EmptyDataText="Nu sunt adaugate inregistrari" AllowPaging="True" DataKeyNames="mesajId"
                DataSourceID="SqlMesajeTrimise" OnRowDataBound="gvMesajeTrimise_RowDataBound" OnSelectedIndexChanged="gvMesajeTrimise_SelectedIndexChanged" EnableModelValidation="True">
                <Columns>
                    <asp:TemplateField HeaderText="Mesaj trimis la" SortExpression="utilizatorNumePrenume">
                        <EditItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("utilizatorNumePrenume") %>'></asp:Label>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text="administrator"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="subiect" HeaderText="Subiect" SortExpression="subiect" />
                    <asp:BoundField DataField="dataAdaugarii" HeaderText="Data trimiterii" SortExpression="dataAdaugarii" />
                    <asp:TemplateField Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblMesajId" Visible="False" runat="server" Text='<%# Eval("mesajId") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                <HeaderStyle Font-Bold="True" />
            </asp:GridView>
            <asp:SqlDataSource ID="SqlMesajeTrimise" runat="server" 
                SelectCommand="SELECT mesaje.mesajId, mesaje.dataAdaugarii, mesaje.dataCitirii, mesaje.subiect, utilizatori.utilizatorNume + ' ' + utilizatori.utilizatorPrenume AS utilizatorNumePrenume, unitati.unitateDenumire FROM mesaje INNER JOIN utilizatori ON mesaje.utilizatorIdDeLa = utilizatori.utilizatorId INNER JOIN unitati ON utilizatori.unitateId = unitati.unitateId WHERE (mesaje.utilizatorIdDeLa = @utilizatorId) AND (mesaje.utilizatorId = 52) ORDER BY mesaje.dataAdaugarii DESC">
                <SelectParameters>
                    <asp:SessionParameter Name="utilizatorId" SessionField="SESutilizatorId" />
                </SelectParameters>
            </asp:SqlDataSource>
        </asp:Panel>
        <!-- adaugam unitate -->
        <asp:Panel ID="pnDetaliiMesajTrimis" CssClass="panel_general" runat="server" Visible="false">
            <asp:Panel ID="Panel4" CssClass="adauga" runat="server">
                <asp:Panel ID="pnEroareTrimis" runat="server">
                    <h2>DETALII MESAJ TRIMIS</h2>
                    <p>
                        <asp:Label ID="lblMesajTrimis" runat="server" Text=""></asp:Label>
                    </p>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
</asp:Content>
