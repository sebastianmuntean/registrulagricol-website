﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="cautaContract.aspx.cs" Inherits="cautaContract" EnableEventValidation="false" UICulture="ro-Ro" Culture="ro-Ro"%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Rapoarte / Căutare contract" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="pnListaContracte" runat="server" CssClass="panel_general" Visible="true">
        <asp:Panel ID="Panel7" CssClass="" runat="server" Visible="true">
            <asp:Panel ID="Panel6" runat="server" CssClass="cauta">
                <asp:Label ID="lblCCauta" runat="server" Text="Caută: " />
                <asp:Label ID="lblCInceput" runat="server" Text="Derulate în perioada" />
                <asp:TextBox ID="tbCInceput" runat="server" Width="80" Format="dd.MM.yyyy" onchange="this.value=ValidareData(this.value, 'dd.MM.yyyy')"
                    AutoPostBack="true" OnTextChanged="tbCInceput_TextChanged"></asp:TextBox>
                <asp:CalendarExtender ID="caltbCInceput" runat="server" Enabled="True" Format="dd.MM.yyyy"
                    PopupButtonID="tbCInceput" TargetControlID="tbCInceput">
                </asp:CalendarExtender>
                <asp:Label ID="lblCFinal" runat="server" Text="-" />
                <asp:TextBox ID="tbCFinal" AutoPostBack="true" runat="server" Width="80" Format="dd.MM.yyyy"
                    onchange="this.value=ValidareData(this.value, 'dd.MM.yyyy')" OnTextChanged="tbCFinal_TextChanged"></asp:TextBox>
                <asp:CalendarExtender ID="caltbCFinal" runat="server" Enabled="True" Format="dd.MM.yyyy"
                    PopupButtonID="tbCFinal" TargetControlID="tbCFinal">
                </asp:CalendarExtender>
                <asp:Label ID="lblCNrContract" runat="server" Text="Nr. contract"></asp:Label>
                <asp:TextBox ID="tbCNrContract" AutoPostBack="true" runat="server"></asp:TextBox>
            </asp:Panel>
        </asp:Panel>
        <asp:Panel ID="Panel8" CssClass="" runat="server" Visible="true">
            <asp:GridView ID="gvModUtilizareContracte" runat="server" AutoGenerateColumns="False"
                DataSourceID="SqlModUtilizareContracteGrid" AllowPaging="True" AllowSorting="True"
                CssClass="tabela" OnPreRender="gvModUtilizareContracte_PreRender" OnRowDataBound="gvModUtilizareContracte_RowDataBound"
                DataKeyNames="contractId" OnDataBound="gvModUtilizareContracte_DataBound"
                ShowFooter="True" EmptyDataText="nu există contracte introduse" EnableModelValidation="True">
                <Columns>
                    <asp:BoundField DataField="denumire1" HeaderText="Tip contract" SortExpression="denumire1" />
                    <asp:BoundField DataField="contractId" HeaderText="contractId" SortExpression="contractId"
                        Visible="False" />
                    <asp:BoundField DataField="contractNr" HeaderText="contract număr" SortExpression="contractNr"
                        Visible="true" />
                    <asp:BoundField DataField="contractDataSemnarii" HeaderText="data semnării" SortExpression="contractDataSemnarii"
                        Visible="true" DataFormatString="{0:d}" />
                    <asp:BoundField DataField="contractGospodarieIdDa" HeaderText="contractGospodarieIdDa"
                        SortExpression="contractGospodarieIdDa" Visible="false" />
                    <asp:BoundField DataField="contractGospodarieIdPrimeste" HeaderText="contractGospodarieIdPrimeste"
                        SortExpression="contractGospodarieIdPrimeste" Visible="false" />
                    <asp:TemplateField HeaderText="valabil între anii" SortExpression="contractDataFinal">
                        <ItemTemplate>
                            <asp:Label ID="lblDataInceput" runat="server" Text='<%# Bind("contractDataInceput", "{0:yyyy}") %>'></asp:Label>
                            -
                                    <asp:Label ID="lblDataFinal" runat="server" Text='<%# Bind("contractDataFinal", "{0:yyyy}") %>'></asp:Label>
                            <asp:Label ID="lblExpirat" runat="server" CssClass="validator rosu" Text="expirat!"
                                Visible="false"></asp:Label>
                            <asp:Label ID="lblInCurs" runat="server" CssClass="validator verde" Text="în curs"
                                Visible="false"></asp:Label>
                            <asp:Label ID="lblNeinceput" runat="server" CssClass="validator galben" Text="neînceput"
                                Visible="false"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="detalii contract" SortExpression="contractDetalii">
                        <ItemTemplate>
                            <asp:Label ID="lblGvContracteDetalii" runat="server" Text='<%# Bind("contractDetalii") %>'></asp:Label>
                            <asp:Label ID="lblGvContracteId" runat="server" Text='<%# Bind("contractId") %>'
                                Visible="false"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="gospodăria care dă" SortExpression="gospodarie1">
                        <ItemTemplate>
                            <asp:Label ID="lblNumeDa" runat="server" Text=''></asp:Label>
                            <asp:Label ID="lblGospodarieDa" runat="server" Text='<%# Bind("gospodarie1") %>'></asp:Label>
                            <asp:Label ID="lblIdDa" runat="server" Text='<%# Bind("idDa") %>' Visible="false"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="gospodăria care primeşte" SortExpression="gospodarie">
                        <ItemTemplate>
                            <asp:Label ID="lblNumePrimeste" runat="server" Text=''></asp:Label>
                            <asp:Label ID="lblGospodariePrimeste" runat="server" Text='<%# Bind("gospodarie") %>'></asp:Label>
                            <asp:Label ID="lblIdPrimeste" runat="server" Text='<%# Bind("idPrimeste") %>' Visible="false"></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="Label2" runat="server" Text="TOTAL" Font-Bold="true"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Total Ha" SortExpression="ha">
                        <ItemTemplate>
                            <asp:Label ID="lblHa" runat="server" Text='<%# Bind("ha", "{0:F}") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblHaT" runat="server" Text="0,00" Font-Bold="true"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Total Ari" SortExpression="ari">
                        <ItemTemplate>
                            <asp:Label ID="lblAri" runat="server" Text='<%# Bind("ari", "{0:F}") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblAriT" runat="server" Text="0,00" Font-Bold="true"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#FFFF99" ForeColor="#990000" />
                <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
            </asp:GridView>
            <asp:SqlDataSource ID="SqlModUtilizareContracteGrid" runat="server" 
                SelectCommand="SELECT gospodarii_1.an, gospodarii_1.gospodarieId AS idDa, gospodarii.gospodarieId AS idPrimeste, parceleAtribuiriContracte.contractId, parceleAtribuiriContracte.contractNr, parceleAtribuiriContracte.contractDataSemnarii, parceleAtribuiriContracte.contractGospodarieIdDa, parceleAtribuiriContracte.contractGospodarieIdPrimeste, parceleAtribuiriContracte.contractDataInceput, parceleAtribuiriContracte.contractDataFinal, parceleAtribuiriContracte.contractDetalii, ' (' + gospodarii_1.strada + ', ' + gospodarii_1.nr + ')' AS gospodarie1, ' (' + gospodarii.strada + ', ' + gospodarii.nr + ')' AS gospodarie, sabloaneCapitole.denumire1, SUM(parceleModUtilizare.c3Ha) + ROUND(SUM(parceleModUtilizare.c3Ari) / 100, 0, 1) AS ha, SUM(parceleModUtilizare.c3Ari) - ROUND(SUM(parceleModUtilizare.c3Ari) / 100, 0, 1) * 100 AS ari FROM sabloaneCapitole INNER JOIN parceleModUtilizare ON sabloaneCapitole.codRand = parceleModUtilizare.c3Rand INNER JOIN gospodarii INNER JOIN parceleAtribuiriContracte ON gospodarii.gospodarieId = parceleAtribuiriContracte.contractGospodarieIdPrimeste INNER JOIN gospodarii AS gospodarii_1 ON parceleAtribuiriContracte.contractGospodarieIdDa = gospodarii_1.gospodarieId ON parceleModUtilizare.contractId = parceleAtribuiriContracte.contractId WHERE (sabloaneCapitole.capitol = '3') AND (sabloaneCapitole.an = @an) AND (parceleAtribuiriContracte.contractNr LIKE @Numar) AND (parceleAtribuiriContracte.contractDataFinal &gt;= CONVERT (datetime, @data1, 104)) AND (parceleAtribuiriContracte.contractDataInceput &lt;= CONVERT (datetime, @data2, 104)) AND (parceleModUtilizare.unitateId = @unitateId) AND (gospodarii_1.an = @an) GROUP BY gospodarii_1.gospodarieId, gospodarii.gospodarieId, parceleAtribuiriContracte.contractId, parceleAtribuiriContracte.contractNr, parceleAtribuiriContracte.contractDataSemnarii, parceleAtribuiriContracte.contractGospodarieIdDa, parceleAtribuiriContracte.contractGospodarieIdPrimeste, parceleAtribuiriContracte.contractDataInceput, parceleAtribuiriContracte.contractDataFinal, parceleAtribuiriContracte.contractDetalii, ' (' + gospodarii_1.strada + ', ' + gospodarii_1.nr + ')', ' (' + gospodarii.strada + ', ' + gospodarii.nr + ')', sabloaneCapitole.denumire1, gospodarii_1.an ORDER BY sabloaneCapitole.denumire1">
                <SelectParameters>
                    <asp:SessionParameter Name="an" SessionField="SESan" Type="Int32" />
                    <asp:ControlParameter ControlID="tbCNrContract" DefaultValue="%" Name="Numar" PropertyName="Text" />
                    <asp:ControlParameter ControlID="tbCInceput" Name="data1" PropertyName="Text" DefaultValue="" />
                    <asp:ControlParameter ControlID="tbCFinal" Name="data2" PropertyName="Text" />
                    <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" />
                </SelectParameters>
            </asp:SqlDataSource>
        </asp:Panel>
    </asp:Panel>
</asp:Content>
