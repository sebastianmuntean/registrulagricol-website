﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
/// <summary>
/// Adaugare capitol
/// Creata la:                  ??.02.2011
/// Autor:                      SM
/// Ultima                      actualizare: 06.04.2011
/// Autor:                      SM - adaugare log, modificare sa nu poata sterge cap de gospodarie
/// </summary> 
public partial class Capitol1 : System.Web.UI.Page
{
    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
         DBConnections connection = new DBConnections(Convert.ToInt16(Session["SESan"]));

        SqlMembri.ConnectionString = connection.Create();
        SqlMembriCap.ConnectionString = connection.Create();

        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "Cap.1", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["SESgospodarieId"] == null)
        {
            HttpCookie vCookie = Request.Cookies["COOKTNT"];
            vCookie = CriptareCookie.DecodeCookie(vCookie);
            if (vCookie != null)
            {
                if (vCookie["COOKgospodarieId"] != null)
                {
                    Session["SESgospodarieId"] = vCookie["COOKgospodarieId"].ToString();
                }
                else if (Request.Url.AbsolutePath.IndexOf("Gospodarii.aspx") == -1 && Session["SESgospodarieId"] == null)
                    Response.Redirect("~/Gospodarii.aspx");
            }
        }
    }
    protected int ExistaCapGospodarie()
    {
        int vExista = 0;
        string[] vCamp = { "codRudenie", "gospodarieId" };
        string[] vValoareCamp = { "1", Session["SESgospodarieId"].ToString() };
        string vLogic = " AND ";
        if (ManipuleazaBD.fVerificaExistentaArray("membri", vCamp, vValoareCamp, vLogic, Convert.ToInt16(Session["SESan"])) == 1) { vExista = 1; }
        return vExista;
    }
    protected void ArataAdaugaMembru(object sender, EventArgs e)
    {
        pnAdaugaMembru.Visible = true;
        pnListaMembri.Visible = false;
        RefaceListaRudenie();

        // daca e adaugare
        if (((Button)sender).ID.ToString() == "btListaAdaugaMembru")
        {

            tbCnp.Text = "";
            tbMentiuni.Text = "";
            tbNume.Text = "";
            tbDataNasterii.Text = "";
            cbDecedat.Checked = false;
            pnDecedat.Visible = false;
            tbSerieActDeces.Text = "";
            tbNumarActDeces.Text = "";
            // verificam daca exista un cap de gospodarie; daca exista si nu e cel modificat nu afisam cap de gospodarie in dd
            ddRudenie.Visible = true;
            lblRudenie.Text = "Rudenie";

            if (ExistaCapGospodarie() == 1)
            {
                ddRudenie.Items.RemoveAt(0);
            }

            btAdaugaMembru.Visible = true;
            btAdaugaModificaMembru.Visible = false;
            btModificaArataListaMembri.Visible = false;
            btAdaugaArataListaMembri.Visible = true;
            // ascund controalele aferente modificarii capului de gospodarie
            cbRudenie.Checked = false;
            cbRudenie.Visible = false;
            lblCbRudenie.Visible = false;
            ddRudenie.Visible = true;
            lblRudenieNou.Visible = false;
            ddlCapGospodarie.Visible = false;
        }
        // daca e modificare
        else
        {
            bool cnpChanged = false;

            Session["cnpChanged"] = cnpChanged;
            // if (gvMembri.SelectedValue != 

            btAdaugaMembru.Visible = false;
            btAdaugaModificaMembru.Visible = true;
            btModificaArataListaMembri.Visible = true;
            btAdaugaArataListaMembri.Visible = false;
            ddRudenie.Visible = true;
            lblRudenie.Text = "Rudenie";
            // luam valoarea camp2 

            string vMembruId = gvMembri.SelectedValue.ToString();
            string[] vWhereCampuri = { "capitolId" };
            string[] vWhereValori = { vMembruId };
            string[] vOperatoriConditionali = { "=" };
            string[] vOperatoriLogici = { " " };
            string[] vCampuriRezultate = { "nume", "codRand", "cnp", "codSex", "codRudenie", "denumireRudenie", "dataNasterii", "mentiuni", "decedat", "decedatData", "decedatSerie", "decedatNumar" };
            List<ListaSelect> vListaCampuri = ManipuleazaBD.fSelectCuRezultatMultiplu("membri", vWhereCampuri, vWhereValori, vOperatoriConditionali, vOperatoriLogici, vCampuriRezultate, Convert.ToInt16(Session["SESan"]));
            cbRudenie.Checked = false;
            cbRudenie.Visible = false;
            lblCbRudenie.Visible = false;
            ddRudenie.Visible = true;
            lblRudenieNou.Visible = false;
            ddlCapGospodarie.Visible = false;
            foreach (ListaSelect vMembruCampuri in vListaCampuri)
            {
                tbNume.Text = vMembruCampuri.Col1;
                //lblCodRand.Text = vMembruCampuri.Col2;
                tbCnp.Text = vMembruCampuri.Col3;
                ddSex.SelectedValue = vMembruCampuri.Col4;
                // verificam daca exista un cap de gospodarie; daca exista si nu e cel modificat nu afisam cap de gospodarie in dd
                ddRudenie.SelectedValue = vMembruCampuri.Col5;
                if (ExistaCapGospodarie() == 1 && vMembruCampuri.Col5 != "1")
                {
                    ddRudenie.Items.RemoveAt(0);
                    cbRudenie.Checked = false;
                    cbRudenie.Visible = false;
                    lblCbRudenie.Visible = false;
                    ddRudenie.Visible = true;
                    lblRudenieNou.Visible = false;
                    ddlCapGospodarie.Visible = false;
                }
                else if (vMembruCampuri.Col5 == "1")
                {
                    ddRudenie.Items.RemoveAt(0);
                    ddRudenie.Visible = false;
                    lblRudenie.Text += ": cap de gospodărie; ";
                    cbRudenie.Visible = true;
                    cbRudenie.Checked = false;
                    lblCbRudenie.Visible = true;
                    ddRudenie.Visible = false;
                    lblRudenieNou.Visible = false;
                    ddlCapGospodarie.Visible = false;
                    ddRudenie.SelectedIndex = 1;
                }

                // este decedat
                bool vDecedat = false;
                try { vDecedat = Convert.ToBoolean(vMembruCampuri.Col9); }
                catch { }
                if (vDecedat)
                {
                    cbDecedat.Checked = true;
                    pnDecedat.Visible = true;
                    try { tbDataDeces.Text = Convert.ToDateTime(vMembruCampuri.Col10).ToString("dd.MM.yyyy"); }
                    catch { }
                    tbSerieActDeces.Text = vMembruCampuri.Col11;
                    tbNumarActDeces.Text = vMembruCampuri.Col12;
                }
                else
                {
                    cbDecedat.Checked = false;
                    pnDecedat.Visible = false;
                    tbDataDeces.Text = "";
                    tbSerieActDeces.Text = "";
                    tbNumarActDeces.Text = "";
                }

                if (vMembruCampuri.Col7.Contains("01.01.1900"))
                {
                    tbDataNasterii.Text = "";
                }
                else
                {
                    tbDataNasterii.Text = Convert.ToDateTime(vMembruCampuri.Col7).ToString("dd.MM.yyyy");
                }
                tbMentiuni.Text = vMembruCampuri.Col8;
            }
        }
    }
    protected void StergeMembrul(object sender, EventArgs e)
    {
        string vMembruId = gvMembri.SelectedValue.ToString();
        string vNume = ManipuleazaBD.fRezultaUnString("SELECT nume FROM [membri] WHERE [capitolId] = '" + vMembruId + "'", "nume", Convert.ToInt16(Session["SESan"]));
        string vCodRudenie = ManipuleazaBD.fRezultaUnString("SELECT codRudenie FROM [membri] WHERE [capitolId] = '" + vMembruId + "'", "codRudenie", Convert.ToInt16(Session["SESan"]));

        if (vCodRudenie == "1")
        {
            valCustom.IsValid = false;
            valCustom.ErrorMessage = "Nu puteti sterge un cap de gospodarie";
        }
        else
        {
            ManipuleazaBD.fManipuleazaBD("DELETE FROM [membri] WHERE [capitolId] = '" + vMembruId + "'", Convert.ToInt16(Session["SESan"]));

            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "membri", "sterge membru ID: " + vMembruId, "nume membru sters: " + vNume, Convert.ToInt64(Session["SESgospodarieId"]), 4);
            ClassLog.UpdateDataUltimeiModificari(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESgospodarieId"]));
            gvMembri.DataBind();
            gvMembri.SelectedIndex = -1;
            btStergeMembru.Visible = false;
            btModificaMembru.Visible = false;
        }
    }

    protected void AdaugaModificaMembru(object sender, EventArgs e)
    {
        if (((Button)sender).ID.ToString() == "btAdaugaMembru")
        {

            if (!MembriServices.VerificaDacaMaiExistaUnMembruCuAcestCNP(tbCnp.Text, Convert.ToInt16(Session["SESan"])) || !Utils.ValidateCNP(tbCnp.Text))
            {
                valCustom.IsValid = false;
                valCustom.ErrorMessage = "CNP-ul deja exista la aceasta gospodarie sau este invalid! Introduceti un CNP valid sau '-'!";
                return;
            }
        }

        // daca mai are deja un sot sau o sotie nu il las sa mai adauge unu
        if (ddRudenie.SelectedValue == "2")
        {
            SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
            SqlCommand vCmd = new SqlCommand();
            vCmd.Connection = vCon;
            string vMembruId = "0", vMembruNou = "0";
            try { vMembruNou = ddlCapGospodarie.SelectedValue; }
            catch { }
            if (((Button)sender).ID.ToString() != "btAdaugaMembru")
            { vMembruId = gvMembri.SelectedValue.ToString(); }

            vCmd.CommandText = "select count(*) from membri where gospodarieId='" + Session["SESgospodarieId"].ToString() + "' and an='" + Session["SESan"].ToString() + "' and codRudenie=2 and capitolId<>'" + vMembruId + "' and capitolId<>'" + vMembruNou + "'";
            if (Convert.ToInt32(vCmd.ExecuteScalar()) > 0)
            {
                valCustom.IsValid = false;
                valCustom.ErrorMessage = "Nu puteţi avea 2 persoane de tip soţ/soţie la aceeaşi gospodărie ! ";
                ManipuleazaBD.InchideConexiune(vCon);
                return;
            }
            ManipuleazaBD.InchideConexiune(vCon);
        }
        //Session["SESgospodarie"] = "0";
        // daca un camp este tip data ii punem in fata ### si atunci functia il converteste; ex: "###"+tbDataNasterii.Text
        int vValoareSelectata = Convert.ToInt32(gvMembri.SelectedValue);
        // aflam numarul maxim de membri si dam valoare lui codRand
        // il folosim doar daca e adaugare
        if (((Button)sender).ID.ToString() == "btAdaugaMembru")
        {
            lblCodRand.Text = (ManipuleazaBD.fRezultaUnMaximInt("membri", "codRand", "gospodarieId", Session["SESgospodarieId"].ToString(), "=", Convert.ToInt16(Session["SESan"])) + 1).ToString();
            if (lblCodRand.Text == "0") { lblCodRand.Text = "1"; }
        }

        string[] vCampuri = { "unitateId", "gospodarieId", "nume", "codRand", "cnp", "codSex", "codRudenie", "denumireRudenie", "dataNasterii", "mentiuni", "an", "decedat", "decedatData", "decedatSerie", "decedatNumar" };

        string[] vValori = { Session["SESunitateId"].ToString(), Session["SESgospodarieId"].ToString(), tbNume.Text, lblCodRand.Text, tbCnp.Text, ddSex.SelectedValue.ToString(), ddRudenie.SelectedValue.ToString(), ddRudenie.SelectedItem.Text, "###" + tbDataNasterii.Text, tbMentiuni.Text, Session["SESan"].ToString(), cbDecedat.Checked.ToString(), "###" + tbDataDeces.Text, tbSerieActDeces.Text, tbNumarActDeces.Text };
        if (((Button)sender).ID.ToString() == "btAdaugaMembru")
        {
            int vAMaiScris = ManipuleazaBD.fVerificaExistentaExtins("membri", "nume", tbNume.Text, " AND gospodarieId = " + Session["SESgospodarieId"].ToString(), Convert.ToInt16(Session["SESan"]));
            if (vAMaiScris == 1) vAMaiScris = ManipuleazaBD.fVerificaExistentaExtins("membri", "cnp", tbCnp.Text, " AND gospodarieId = " + Session["SESgospodarieId"].ToString(), Convert.ToInt16(Session["SESan"]));
            if (vAMaiScris == 0)
            {
                ManipuleazaBD.fManipuleazaBDArray(0, "membri", vCampuri, vValori, "", 0, Convert.ToInt16(Session["SESan"]));
                // update nume membru si cnp pe gospodarii
                if (ddRudenie.SelectedValue == "1")
                    ManipuleazaBD.fManipuleazaBD("update gospodarii set membruNume=N'" + tbNume.Text + "', membruCnp='" + tbCnp.Text + "' where gospodarieId='" + Session["SESgospodarieId"].ToString() + "'", Convert.ToInt16(Session["SESan"]));
                ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "membri", "adaugare membru: " + tbNume.Text, "", Convert.ToInt64(Session["SESgospodarieId"]), 2);
                ClassLog.UpdateDataUltimeiModificari(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESgospodarieId"]));
            }
        }
        else
        {
            if ((!MembriServices.VerificaDacaMaiExistaUnMembruCuAcestCNP(tbCnp.Text, Convert.ToInt16(Session["SESan"])) || !Utils.ValidateCNP(tbCnp.Text)) && Convert.ToBoolean(Session["cnpChanged"]))
            {
                valCustom.IsValid = false;
                valCustom.ErrorMessage = "CNP-ul deja exista la aceasta gospodarie sau este invalid ! ";
                return;
            }
            DateTime? vDataDeces = null;
            try { vDataDeces = Convert.ToDateTime(tbDataDeces.Text); }
            catch { }
            // verific daca este bifat pentru modificarea capului de gospodarie
            if (cbRudenie.Checked)
            {
                if (ddlCapGospodarie.Items.Count > 0)
                {
                    // pun noul cap de gospodarie
                    ManipuleazaBD.fManipuleazaBD("update membri set codRudenie='1', denumireRudenie=N'cap de gospodărie' where capitolId='" + ddlCapGospodarie.SelectedValue + "'", Convert.ToInt16(Session["SESan"]));
                    // fac update pe gospodarii cu noul membru
                    SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
                    SqlCommand vCmd = new SqlCommand();
                    vCmd.Connection = vCon;
                    vCmd.CommandText = "select * from membri where capitolId='" + ddlCapGospodarie.SelectedValue + "'";
                    SqlDataReader vTabel = vCmd.ExecuteReader();
                    if (vTabel.Read())
                    {
                        string vSelect = "update gospodarii set membruNume=N'" + vTabel["nume"].ToString() + "', membruCnp='" + vTabel["cnp"].ToString() + "'";
                        vSelect += " where gospodarieId='" + Session["SESgospodarieId"].ToString() + "'";
                        ManipuleazaBD.fManipuleazaBD(vSelect, Convert.ToInt16(Session["SESan"]));
                    }
                    vTabel.Close();
                    ManipuleazaBD.InchideConexiune(vCon);
                }
                else
                {
                    valCustom.IsValid = false;
                    valCustom.ErrorMessage = "Va rugăm sa alegeţi noul cap de gospodărie. Trebuie sa aveţi introdus cel puţin un membru în afara capului de gospodărie !";
                    return;
                }
            }
            ManipuleazaBD.fManipuleazaBDArray(1, "membri", vCampuri, vValori, "capitolId", Convert.ToInt32(gvMembri.SelectedValue), Convert.ToInt16(Session["SESan"]));
            if (!cbRudenie.Checked && cbRudenie.Visible)
            {
                ManipuleazaBD.fManipuleazaBD("update membri set codRudenie='1', denumireRudenie=N'cap de gospodărie' where capitolId='" + gvMembri.SelectedValue + "'", Convert.ToInt16(Session["SESan"]));
                //if (ddRudenie.SelectedValue == "1")
                string vSelect = "update gospodarii set membruNume=N'" + tbNume.Text + "', membruCnp=N'" + tbCnp.Text + "'";
                vSelect += " where gospodarieId='" + Session["SESgospodarieId"].ToString() + "'";
                ManipuleazaBD.fManipuleazaBD(vSelect, Convert.ToInt16(Session["SESan"]));

            }
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "membri", "modificare membru ID: " + Convert.ToInt32(gvMembri.SelectedValue), "nume: " + tbNume.Text, Convert.ToInt64(Session["SESgospodarieId"]), 3);
            ClassLog.UpdateDataUltimeiModificari(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESgospodarieId"]));
        }
        gvMembri.DataBind();
        if (((Button)sender).ID.ToString() == "btAdaugaMembru") { gvMembri.SelectedIndex = -1; }
        else
        {
            //   gvMembri.SelectedIndex = GasesteRandul(vValoareSelectata);
        }
        gvMembri.SelectedIndex = -1;
        pnAdaugaMembru.Visible = false;
        pnListaMembri.Visible = true;
        btModificaMembru.Visible = false;
        btStergeMembru.Visible = false;

    }
    protected void RefaceListaRudenie()
    {
        ddRudenie.Items.Clear();
        string[] vValoriRudenie = { "cap de gospodărie", "soţ/soţie", "fiu/fiică", "alte rude", "neînrudit" };
        int contor = 1;
        foreach (string vValoareRudenie in vValoriRudenie)
        {
            ListItem vLista = new ListItem(vValoareRudenie, contor.ToString());
            ddRudenie.Items.Add(vLista);
            contor++;
        }
    }
    protected int GasesteRandul(int pValoare)
    {
        int vRand = -1;
        for (int i = 0; i < gvMembri.Rows.Count; i++)
        {
            if (gvMembri.Rows[i].Cells[0].Text == pValoare.ToString())
            {
                vRand = i;
                break;
            }
        }
        return vRand;
    }
    protected void ModificaArataListaMembri(object sender, EventArgs e)
    {
        pnAdaugaMembru.Visible = false;
        pnListaMembri.Visible = true;
        btStergeMembru.Visible = true;
        btModificaMembru.Visible = true;
    }
    protected void AdaugaArataListaMembri(object sender, EventArgs e)
    {
        pnAdaugaMembru.Visible = false;
        pnListaMembri.Visible = true;
        btStergeMembru.Visible = false;
        btModificaMembru.Visible = false;
        gvMembri.SelectedIndex = -1;
    }
    protected void gvMembri_SelectedIndexChanged(object sender, EventArgs e)
    {
        // actiuni la selectare rand; aratam butoanele de sterge si modifica
        btAdaugaMembru.Visible = false;
        btModificaMembru.Visible = true;
        btStergeMembru.Visible = true;
        gvMembri.DataBind();
    }
    protected void gvMembri_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(gvMembri, e, this);
    }
    protected void gvMembri_DataBound(object sender, EventArgs e)
    {
        for (int i = 0; i < gvMembri.Rows.Count; i++)
        {
            // schimbam codsex
            if (gvMembri.Rows[i].RowType == DataControlRowType.DataRow)
            {
                if (gvMembri.Rows[i].Cells[4].Text == "1")
                {
                    gvMembri.Rows[i].Cells[4].Text = "masculin";
                }
                else
                {
                    gvMembri.Rows[i].Cells[4].Text = "feminin";
                }
                if (gvMembri.Rows[i].Cells[6].Text == "01.01.1900")
                {
                    gvMembri.Rows[i].Cells[6].Text = " - ";
                }

            }
        }
    }
    protected void tbCnp_TextChanged(object sender, EventArgs e)
    {
        CNP vValidCNP = new CNP(tbCnp.Text);
        if (vValidCNP.IsValidB())
        {
            ddSex.SelectedValue = vValidCNP.GetSex().ToString();
            tbDataNasterii.Text = vValidCNP.GetDataNasterii().ToString("dd.MM.yyyy");
        }

        bool cnpChanged = true;

        Session["cnpChanged"] = cnpChanged;
    }
    protected void btTiparesteCapitol_Click(object sender, EventArgs e)
    {
        ResponseHelper.Redirect("~/printCapitolul1.aspx?codCapitol=1", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
        ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "tipareste", "tiparire lista membri ", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
    }
    protected void btTiparesteCapitolXls_Click(object sender, EventArgs e)
    {
        ResponseHelper.Redirect("~/printCapitolul1xls.aspx?codCapitol=1", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
        ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "tipareste", "tiparire lista membri ", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
    }
    protected void cbRudenie_CheckedChanged(object sender, EventArgs e)
    {
        if (cbRudenie.Checked)
        {
            ddlCapGospodarie.Items.Clear();
            ddlCapGospodarie.DataBind();
            ddRudenie.Visible = true;
            lblRudenieNou.Visible = true;
            ddlCapGospodarie.Visible = true;
        }
        else
        {
            ddRudenie.Visible = false;
            lblRudenieNou.Visible = false;
            ddlCapGospodarie.Visible = false;
        }
    }
    protected void cbDecedat_CheckedChanged(object sender, EventArgs e)
    {
        if (cbDecedat.Checked)
        {
            pnDecedat.Visible = true;
            tbDataDeces.Text = DateTime.Now.ToString("dd.MM.yyyy");
            tbDataDeces.Focus();
        }
        else
        {
            pnDecedat.Visible = false;
            tbDataDeces.Text = "";
            tbSerieActDeces.Text = "";
            tbNumarActDeces.Text = "";
            tbMentiuni.Focus();
        }
    }
}
