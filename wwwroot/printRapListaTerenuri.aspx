﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="printRapListaTerenuri.aspx.cs" Inherits="printRapListaTerenuri" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <rsweb:ReportViewer ID="rvListaTerenuri" runat="server" Font-Names="Verdana" 
        Font-Size="8pt" Height="400px" Width="400px">
        <LocalReport ReportPath="rapoarte\raportListaTerenuri.rdlc">
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" 
                    Name="dsRapoarte_dtUnitati" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource2" 
                    Name="dsRapoarte_dtRapCentralizareCuFiltrari" />
            </DataSources>
        </LocalReport>
    </rsweb:ReportViewer>
    <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="dsRapoarteTableAdapters.dtRapCentralizareCuFiltrariTableAdapter">
        <SelectParameters>
            <asp:SessionParameter Name="utilizatorId" SessionField="SESutilizatorId" 
                Type="Int32" />
            <asp:Parameter DefaultValue="2a" Name="capitol" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="dsRapoarteTableAdapters.dtUnitatiTableAdapter">
        <SelectParameters>
            <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" 
                Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>

