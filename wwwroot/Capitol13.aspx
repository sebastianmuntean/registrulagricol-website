﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Capitol13.aspx.cs" Inherits="Capitol13" UICulture="ro-Ro" Culture="ro-Ro"
    EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Capitole / Capitolul XIII: Mențiuni cu privire la sesizările/cererile pentru deschiderea procedurilor succesorale înaintate notarilor publici" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="succesibiliUpdatePanel" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnAfiseazaMesaje" runat="server">
                <asp:ValidationSummary ID="valSumMembri" runat="server" DisplayMode="SingleParagraph"
                    Visible="true" ValidationGroup="GrupValidareSuccesibili" CssClass="validator" ForeColor="" />
            </asp:Panel>
            <asp:Panel ID="listaSuccesibiliPanel" runat="server" CssClass="panel_general" Visible="true">
                <asp:Panel ID="succesibiliPanel" runat="server">
                    <asp:GridView ID="succesibiliGridView" AllowPaging="True" AllowSorting="True" CssClass="tabela" EmptyDataText ="Nu exista înregistrări!" EmptyDataRowStyle-BorderColor="Red" EmptyDataRowStyle-Font-Bold="true"
                        runat="server" AutoGenerateColumns="False" DataKeyNames="idSuccesibil" 
                        DataSourceID="SqlSuccesibili" OnRowDataBound="succesibiliGridView_RowDataBound" OnSelectedIndexChanged="SuccesibiliGridView_SelectedIndexChanged" EnableModelValidation="True">
                        <Columns>
                            <asp:BoundField DataField="idSuccesibil" HeaderText="id" ReadOnly="True" Visible="false" />
                            <asp:BoundField DataField="numeDefunct" HeaderText="Nume si prenume defunct" SortExpression="numeDefunct" Visible="true" />
                            <asp:BoundField DataField="dataDeces" HeaderText="Data decesului" SortExpression="dataDeces" DataFormatString="{0:dd.MM.yyyy}" HtmlEncode="false"/>
                            <asp:BoundField DataField="numeSuccesibil" HeaderText="Nume si prenume succesibil" SortExpression="numeSuccesibil" />
                            <asp:BoundField DataField="localitate" HeaderText="Localitate succesibil" SortExpression="localitate" />
                            <asp:BoundField DataField="adresa" HeaderText="Strada / Numar" SortExpression="adresa" />

                            <asp:TemplateField SortExpression="numarInregistrare">
						        <ItemTemplate>
							        <asp:Label ID="Label7" runat="server" Text='<%# Bind("numarInregistrare") %>' ></asp:Label>
							        /
								    <asp:Label ID="Label8" runat="server" Text='<%# Bind("dataInregistrarii", "{0:d}") %>'></asp:Label>
						        </ItemTemplate>
						        <HeaderTemplate>
							        <asp:Label ID="Label10b" runat="server" Text="Nr."></asp:Label>
							        /
							        <asp:Label ID="Label11" runat="server" Text="Data inregistrarii"></asp:Label>
						        </HeaderTemplate>
					        </asp:TemplateField>
                            <asp:BoundField DataField="denumireBirouNotarial" HeaderText="SPN/BIN către care se transmite" SortExpression="denumireBirouNotarial" />
                        </Columns>
                        <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlSuccesibili" runat="server" 
                        SelectCommand="SELECT idSuccesibil, membri.nume as numeDefunct, membri.decedatData as dataDeces, succesibili.nume as numeSuccesibil, localitateDenumire +', ' + judete.judetDenumire as localitate, strada + ' / ' + numar as 'adresa', numarInregistrare, dataInregistrarii, denumireBirouNotarial, succesibili.idUnitate, succesibili.an FROM succesibili INNER JOIN membri ON capitolId = idDefunct INNER JOIN localitati on localitateId = idLocalitate INNER JOIN judete on judete.judetId = idJudet WHERE succesibili.idUnitate = @idUnitate  and succesibili.an = @an and idGospodarie = @idGospodarie">
                        <SelectParameters>
                            <asp:SessionParameter DefaultValue="0" Name="idUnitate" SessionField="SESUnitateId"
                                Type="Int64" />
                            <asp:SessionParameter DefaultValue="0" Name="an" SessionField="SESAn"
                                Type="Int64" />
                             <asp:SessionParameter DefaultValue="0" Name="idGospodarie" SessionField="SESgospodarieId"
                                Type="Int64" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </asp:Panel>
                <asp:Panel ID="pnListaButoanePrincipale" CssClass="butoane" runat="server">
                    <asp:Button CssClass="buton" ID="btModificaSuccesibil" runat="server" Text="modifică succesibilul"
                        OnClick="ArataModificaSuccesibilPanel_Click" Visible="false" />
                    <asp:Button CssClass="buton" ID="stergeSuccesibilButton" runat="server" Text="șterge succesibilul"
                        OnClick="StergeSuccesibilulButtonClick" ValidationGroup="GrupValidareSuccesibili" OnClientClick="return confirm(&quot;Sunteți sigur că doriți să ștergeți succesibilul?&quot;)"
                        Visible="false" />
                    <asp:Button CssClass="buton" ID="AdaugaSuccesibilPanelButton" runat="server" Text="adaugă un succesibil"
                        OnClick="AdaugaSuccesibilPanelButton_Click" />
                    <asp:Button ID="btListaCapitole" runat="server" CssClass="buton" Text="listă gospodării"
                        Visible="true" PostBackUrl="~/Gospodarii.aspx" />
                    <asp:Button ID="btTiparesteCapitol" runat="server" CssClass="buton" Text="tipărire capitol"
                        Visible="true" OnClick="btTiparesteCapitol_Click" />
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="adaugaSuccesibilPanel" CssClass="panel_general" runat="server" Visible="false">
                <asp:Panel ID="pnAd" CssClass="adauga" runat="server">
                    <asp:Panel ID="pnAdTitlu" runat="server">
                        <h2>ADAUGĂ / MODIFICĂ SUCCESIBIL</h2>
                    </asp:Panel>
                    <asp:Panel ID="pnAd1" runat="server">
                         <p>   
                            <asp:Label runat="server" Text="Defunct"></asp:Label>
                            <asp:DropDownList ID="defunctDropDownList" runat="server" Width="200px" AutoPostBack="true" OnSelectedIndexChanged="DefunctDropDownList_SelectedIndexChanged" />
                            <asp:Label ID="Label2" runat="server" Text="Data deces"></asp:Label>
                            <asp:TextBox ID="dataDecesTextBox" runat="server" Width="80" Format="dd.MM.yyyy" ReadOnly="false"></asp:TextBox>
                         </p>
                        <br />
                        <p>
                            <asp:Label runat="server" Text ="Nume si prenume succesibil"></asp:Label>
                            <asp:TextBox ID="tbNumePrenume" runat="server" Width="350"></asp:TextBox>
                            <br />
                            <br />
                            <asp:Label runat="server" Text="Judetul"/>
                            <asp:DropDownList ID="judeteDropDownList" runat="server" AutoPostBack="true" Width="200px" OnSelectedIndexChanged="JudeteDropDownList_SelectedIndexChanged"/>
                            <asp:Label runat="server" Text="Localitatea" />
                            <asp:DropDownList ID="localitatiDropDownList" Width="200px" runat="server" />
                            <asp:Label Text="Strada" runat="server" />
                            <asp:TextBox ID="stradaTextBox" runat="server"  Width="250"></asp:TextBox>
                            <asp:Label Text="Numărul locuinței" runat="server"/>
                            <asp:TextBox ID="numarTextBox" runat="server"  Width="50"></asp:TextBox>
                        </p>
                        <br />
                        <p>
                            <asp:Label ID="Label1" runat="server" Text="SPN/BIN" ToolTip ="Societate profesionala notariala / Birou individual notarial" />
                             <asp:TextBox runat ="server" ID="binTextBox" Width="250"></asp:TextBox>
                            <asp:Label ID="nrDataReg" runat="server" Text="Nr./Data inregistrării" />
                            <asp:TextBox runat ="server" ID="nrTextBox" Width="50"></asp:TextBox>
                            <asp:Label runat="server" Text ="/" Width="20" Font-Size="Larger"/>
                            <asp:TextBox ID="dataInregistrareTextBox" runat="server" Width="80" Format="dd.MM.yyyy" onchange="this.value=ValidareData(this.value, 'dd.MM.yyyy')"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True"
                                Format="dd.MM.yyyy" PopupButtonID="dataInregistrareTextBox" TargetControlID="dataInregistrareTextBox">
                            </asp:CalendarExtender>
                        </p>
                          
                    </asp:Panel>
                    <asp:Panel ID="pnButoanePrincipale" runat="server" CssClass="butoane">
                        <asp:Button CssClass="buton" ID="adaugaSuccesibilButton" runat="server" Text="salvează"
                            OnClick="AdaugaSuccesibilButton_Click" ValidationGroup="GrupValidareSuccesibili" Visible="false" />
                        <asp:Button ID="modificaSuccesibilButton" runat="server" CssClass="buton" OnClick="ModificaSuccesibilButton_Click"
                            Text="modifică succesibilul" ValidationGroup="GrupValidareSuccesibili" Visible="false" />
                        <asp:Button ID="arataListaSuccesibiliButton" runat="server" CssClass="buton" OnClick="ArataListaSuccesibiliButton_Click"
                            Text="lista succesibililor" Visible="false" />
                    </asp:Panel>
                    <asp:Panel ID="Panel5" CssClass="ascunsa" runat="server">
                        <!-- //////////// VALIDATOARE  \\\\\\\\\\\\\\\\\\ -->
                        <asp:RequiredFieldValidator ID="valNume" ControlToValidate="tbNumePrenume" ValidationGroup="GrupValidareSuccesibili"
                            runat="server" ErrorMessage=" Numele si prenumele sunt obligatorii! "></asp:RequiredFieldValidator>
                          <asp:RequiredFieldValidator ID="valjudeteDropDownList" ControlToValidate="judeteDropDownList" ValidationGroup="GrupValidareSuccesibili"
                            runat="server" ErrorMessage="Judetul este obligatoriu!" InitialValue="- Selectati un judet -"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="valLocalitatiDropDownList" ControlToValidate="localitatiDropDownList" ValidationGroup="GrupValidareSuccesibili"
                            runat="server" ErrorMessage="Localitatea este obligatorie!" InitialValue="- Selectati o localitate -"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="valStrada" ControlToValidate="stradaTextBox" ValidationGroup="GrupValidareSuccesibili"
                            runat="server" ErrorMessage=" Numele strazii este obligatoriu! "></asp:RequiredFieldValidator>
                         <asp:RequiredFieldValidator ID="valNr" ControlToValidate="numarTextBox" ValidationGroup="GrupValidareSuccesibili"
                            runat="server" ErrorMessage=" Numarul locuintei este obligatoriu! "></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="valBinTextBox" ControlToValidate="binTextBox" ValidationGroup="GrupValidareSuccesibili"
                            runat="server" ErrorMessage="SPN/BIN este obligatoriu! "></asp:RequiredFieldValidator>
                         <asp:RequiredFieldValidator ID="valNrReg" ControlToValidate="nrTextBox" ValidationGroup="GrupValidareSuccesibili"
                            runat="server" ErrorMessage=" Numarul inregistrarii este obligatoriu! "></asp:RequiredFieldValidator>
                         <asp:RequiredFieldValidator ID="valDataReg" ControlToValidate="dataInregistrareTextBox" ValidationGroup="GrupValidareSuccesibili"
                            runat="server" ErrorMessage="Data inregistrarii este obligatorie! "></asp:RequiredFieldValidator>
                        <asp:CustomValidator ID="valCustom" runat="server" ErrorMessage="" ValidationGroup="GrupValidareSuccesibili"></asp:CustomValidator>
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
