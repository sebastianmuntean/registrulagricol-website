<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="printListaGospodarii.aspx.cs" Inherits="printListaGospodarii" Culture="ro-RO" UICulture="ro-RO" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <rsweb:ReportViewer ID="rvListaGospodarii" runat="server" Font-Names="Verdana" 
        Font-Size="8pt" Height="400px" Width="400px">
        <LocalReport ReportPath="rapoarte\raportListaGospodarii.rdlc">
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" 
                    Name="dsRapoarte_dtListaGospodarii" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource2" 
                    Name="dsRapoarte_dtUnitati" />
            </DataSources>
        </LocalReport>
    </rsweb:ReportViewer>
    <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="dsRapoarteTableAdapters.dtUnitatiTableAdapter">
        <SelectParameters>
            <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" 
                Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="dsRapoarteTableAdapters.dtListaGospodariiTableAdapter">
        <SelectParameters>
            <asp:SessionParameter Name="an" SessionField="SESan" Type="Int32" />
            <asp:QueryStringParameter Name="volum" QueryStringField="volum" Type="String" />
            <asp:QueryStringParameter Name="nrPozitie" QueryStringField="nrPozitie" 
                Type="String" />
            <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" 
                Type="String" />
            <asp:QueryStringParameter Name="tip" QueryStringField="tip" Type="String" />
            <asp:QueryStringParameter Name="localitate" QueryStringField="localitate" 
                Type="String" />
            <asp:QueryStringParameter Name="strada" QueryStringField="strada" 
                Type="String" />
            <asp:QueryStringParameter Name="nr" QueryStringField="nr" Type="String" />
            <asp:QueryStringParameter Name="nume" QueryStringField="nume" Type="String" />
            <asp:QueryStringParameter Name="judetId" QueryStringField="judet" 
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>

