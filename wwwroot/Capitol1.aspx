﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Capitol1.aspx.cs" Inherits="Capitol1" UICulture="ro-Ro" Culture="ro-Ro"
    EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Capitole / Capitolul I: Componenţa gospodăriei/ exploataţiei agricole fără personalitate juridică" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="upMembri" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnAfiseazaMesaje" runat="server">
                <asp:ValidationSummary ID="valSumMembri" runat="server" DisplayMode="SingleParagraph"
                    Visible="true" ValidationGroup="GrupValidareMembri" CssClass="validator" ForeColor="" />
            </asp:Panel>
            <!-- lista Membri -->
            <asp:Panel ID="pnListaMembri" runat="server" CssClass="panel_general" Visible="true">
                <asp:Panel ID="pnMembri" runat="server">
                    <asp:GridView ID="gvMembri" AllowPaging="True" AllowSorting="True" CssClass="tabela"
                        runat="server" AutoGenerateColumns="False" DataKeyNames="capitolId" OnDataBound="gvMembri_DataBound"
                        DataSourceID="SqlMembri" OnRowDataBound="gvMembri_RowDataBound" OnSelectedIndexChanged="gvMembri_SelectedIndexChanged" EnableModelValidation="True">
                        <Columns>
                            <asp:BoundField DataField="capitolId" HeaderText="Nr." InsertVisible="False" ReadOnly="True"
                                SortExpression="capitolId" Visible="false" />
                            <asp:BoundField DataField="codRand" HeaderText="rand" SortExpression="codRand" Visible="false" />
                            <asp:BoundField DataField="nume" HeaderText="nume și prenume" SortExpression="nume" />
                            <asp:BoundField DataField="denumireRudenie" HeaderText="grad de rudenie" SortExpression="denumireRudenie" />
                            <asp:BoundField DataField="codSex" HeaderText="sex" SortExpression="codSex" />
                            <asp:BoundField DataField="mentiuni" HeaderText="mențiuni" SortExpression="mentiuni" />
                            <asp:BoundField DataField="dataNasterii" HeaderText="data nașterii" SortExpression="dataNasterii"
                                DataFormatString="{0:d}" HtmlEncode="False" />
                            <asp:BoundField DataField="cnp" HeaderText="CNP" SortExpression="cnp" />
                            <asp:CheckBoxField DataField="decedat" HeaderText="Decedat" SortExpression="decedat" />
                        </Columns>
                        <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlMembri" runat="server"
                        SelectCommand="SELECT codSex, codRand, mentiuni, dataNasterii, denumireRudenie, cnp, nume, capitolId, COALESCE (decedat, 'False') AS decedat FROM membri WHERE (gospodarieId = @gospodarieId)">
                        <SelectParameters>
                            <asp:SessionParameter DefaultValue="0" Name="gospodarieId" SessionField="SESgospodarieId"
                                Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </asp:Panel>
                <asp:Panel ID="pnListaButoanePrincipale" CssClass="butoane" runat="server">
                    <asp:Button CssClass="buton" ID="btModificaMembru" runat="server" Text="modificare membru"
                        OnClick="ArataAdaugaMembru" Visible="false" />
                    <asp:Button CssClass="buton" ID="btStergeMembru" runat="server" Text="șterge membrul"
                        OnClick="StergeMembrul" ValidationGroup="GrupValidareMembri" OnClientClick="return confirm(&quot;Sunteți sigur că doriți să ștergeți membrul gospodăriei?&quot;)"
                        Visible="false" />
                    <asp:Button CssClass="buton" ID="btListaAdaugaMembru" runat="server" Text="adaugă un membru"
                        OnClick="ArataAdaugaMembru" />
                    <asp:Button ID="btListaCapitole" runat="server" CssClass="buton" Text="listă gospodării"
                        Visible="true" PostBackUrl="~/Gospodarii.aspx" />
                    <asp:Button ID="btTiparesteCapitol" runat="server" CssClass="buton" Text="tipărire capitol"
                        Visible="true" OnClick="btTiparesteCapitol_Click" />
                    <asp:Button ID="btTiparireExcel" runat="server" Text="salveaza in Excel" CssClass="buton" OnClick="btTiparesteCapitolXls_Click" />
                </asp:Panel>
            </asp:Panel>
            <!-- adaugam Membri -->
            <asp:Panel ID="pnAdaugaMembru" CssClass="panel_general" runat="server" Visible="false">
                <asp:Panel ID="pnAd" CssClass="adauga" runat="server">
                    <asp:Panel ID="pnAdTitlu" runat="server">
                        <h2>ADAUGĂ / MODIFICĂ UN MEMBRU AL GOSPODĂRIEI</h2>
                    </asp:Panel>
                    <asp:Panel ID="pnAd1" runat="server">
                        <p>
                            <asp:Label ID="lblNume" runat="server" Text="Nume şi Prenume " />
                            <asp:Label ID="lblCodRand" runat="server" Visible="false" />
                            <asp:TextBox ID="tbNume" runat="server"></asp:TextBox>
                            <asp:Label ID="lblSex" runat="server" Text="Sex " />
                            <asp:DropDownList ID="ddSex" runat="server">
                                <asp:ListItem Selected="True" Value="1">Masculin</asp:ListItem>
                                <asp:ListItem Value="2">Feminin</asp:ListItem>
                            </asp:DropDownList>
                            <asp:Label ID="lblCnp" runat="server" Text=" CNP " />
                            <asp:TextBox ID="tbCnp" runat="server" Width="110" AutoPostBack="True" OnTextChanged="tbCnp_TextChanged"></asp:TextBox>
                            <asp:Label ID="lblDataNasterii" runat="server" Text=" Data naşterii " />
                            <asp:TextBox ID="tbDataNasterii" runat="server" Width="80" Format="dd.MM.yyyy" onchange="this.value=ValidareData(this.value, 'dd.MM.yyyy')"></asp:TextBox>
                            <asp:CalendarExtender ID="tbDataNasterii_CalendarExtender" runat="server" Enabled="True"
                                Format="dd.MM.yyyy" PopupButtonID="tbDataNasterii" TargetControlID="tbDataNasterii">
                            </asp:CalendarExtender>
                        </p>
                        <p class="ora">
                            <asp:Label ID="lblRudenie" runat="server" Text="Rudenie " />
                            <asp:CheckBox ID="cbRudenie" AutoPostBack="true" runat="server" Text=""
                                Checked="false" OnCheckedChanged="cbRudenie_CheckedChanged" />
                            <asp:Label ID="lblCbRudenie" runat="server" Text="Schimbă cap de gospodărie în: "></asp:Label>
                            <asp:DropDownList ID="ddRudenie" runat="server">
                                <asp:ListItem Selected="True" Value="1">cap de gospodărie</asp:ListItem>
                                <asp:ListItem Value="2">soţ/soţie</asp:ListItem>
                                <asp:ListItem Value="3">fiu/fiică</asp:ListItem>
                                <asp:ListItem Value="4">alte rude</asp:ListItem>
                                <asp:ListItem Value="5">neînrudit</asp:ListItem>
                            </asp:DropDownList>
                            <asp:Label ID="lblRudenieNou" runat="server" Text=" noul cap de gospodărie: "></asp:Label>
                            <asp:DropDownList ID="ddlCapGospodarie" runat="server"
                                DataSourceID="SqlMembriCap" DataTextField="nume" DataValueField="capitolId">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="SqlMembriCap" runat="server"
                                SelectCommand="SELECT [nume], [capitolId] FROM [membri] WHERE (([gospodarieId] = @gospodarieId) AND ([capitolId] &lt;&gt; @capitolId)) ORDER BY [nume]">
                                <SelectParameters>
                                    <asp:SessionParameter Name="gospodarieId" SessionField="SESgospodarieId"
                                        Type="Int32" />
                                    <asp:ControlParameter ControlID="gvMembri" Name="capitolId"
                                        PropertyName="SelectedValue" Type="Int64" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </p>
                        <p>
                            <asp:CheckBox ID="cbDecedat" CssClass="checkboxuri" Text="Decedat" AutoPostBack="true" runat="server" OnCheckedChanged="cbDecedat_CheckedChanged" />
                        </p>
                        <asp:Panel ID="pnDecedat" runat="server" Visible="false">
                            <asp:Label ID="lblDataDeces" runat="server" Text=" Data deces " />
                            <asp:TextBox ID="tbDataDeces" runat="server" Width="80" Format="dd.MM.yyyy" onchange="this.value=ValidareData(this.value, 'dd.MM.yyyy')"></asp:TextBox>
                            <asp:CalendarExtender ID="tbDataDeces_CalendarExtender" runat="server" Enabled="True"
                                Format="dd.MM.yyyy" PopupButtonID="tbDataDeces" TargetControlID="tbDataDeces">
                            </asp:CalendarExtender>
                            <asp:Label ID="lblSerieActDeces" runat="server" Text="Serie act deces"></asp:Label>
                            <asp:TextBox ID="tbSerieActDeces" runat="server"></asp:TextBox>
                            <asp:Label ID="lblNumarActDeces" runat="server" Text="Nr. act deces"></asp:Label>
                            <asp:TextBox ID="tbNumarActDeces" runat="server"></asp:TextBox>
                        </asp:Panel>
                        <p>
                            <asp:Label ID="lblMentiuni" runat="server" Text=" Mențiuni" />
                            <br />
                            <asp:TextBox ID="tbMentiuni" runat="server" TextMode="MultiLine" Rows="5" Width="600px"></asp:TextBox>
                        </p>
                    </asp:Panel>
                    <asp:Panel ID="pnButoanePrincipale" runat="server" CssClass="butoane">
                        <asp:Button CssClass="buton" ID="btAdaugaMembru" runat="server" Text="adaugă un membru"
                            OnClick="AdaugaModificaMembru" ValidationGroup="GrupValidareMembri" Visible="false" />
                        <asp:Button ID="btAdaugaModificaMembru" runat="server" CssClass="buton" OnClick="AdaugaModificaMembru"
                            Text="modificare membru" ValidationGroup="GrupValidareMembri" Visible="false" />
                        <asp:Button ID="btModificaArataListaMembri" runat="server" CssClass="buton" OnClick="ModificaArataListaMembri"
                            Text="lista membrilor" Visible="false" />
                        <asp:Button ID="btAdaugaArataListaMembri" runat="server" CssClass="buton" OnClick="AdaugaArataListaMembri"
                            Text="lista membrilor" Visible="false" />
                    </asp:Panel>
                    <asp:Panel ID="Panel5" CssClass="ascunsa" runat="server">
                        <!-- //////////// VALIDATOARE  \\\\\\\\\\\\\\\\\\ -->
                        <asp:RequiredFieldValidator ID="valNume" ControlToValidate="tbNume" ValidationGroup="GrupValidareMembri"
                            runat="server" ErrorMessage=" Numele este obligatoriu! "></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="valCnp" ControlToValidate="tbCnp" ValidationGroup="GrupValidareMembri"
                            runat="server" ErrorMessage=" CNP-ul este obligatoriu! "></asp:RequiredFieldValidator>
                        <asp:CustomValidator ID="valCustom" runat="server" ErrorMessage="" ValidationGroup="GrupValidareMembri"></asp:CustomValidator>
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
