﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="adevSabloane.aspx.cs" Inherits="adevSabloane" UICulture="ro-Ro" Culture="ro-Ro"
    EnableEventValidation="false" validateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Adeverinţe / Şabloane"></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <asp:Panel ID="pnCautare" runat="server" CssClass="cauta">
            <asp:Label ID="lblCCauta" runat="server" Text="Caută după:" />
            <asp:Label ID="lblCCreatDe" runat="server" Text="Creat de" />
            <asp:TextBox ID="tbCCreatDe" runat="server" AutoPostBack="True" Width="160px" />
            <asp:Label ID="lblCDenumire" runat="server" Text="Denumire" />
            <asp:TextBox ID="tbCDenumire" runat="server" AutoPostBack="True" Width="160px" />
            <asp:Label ID="lblCCreatPentru" runat="server" Text="Creat pentru"></asp:Label>
            <asp:DropDownList ID="ddlCCreatPentru" AutoPostBack="true" runat="server">
            </asp:DropDownList>
        </asp:Panel>
            <asp:Panel ID="pnListaAdevSabloane" runat="server" Visible="true" CssClass="panel_general">
                <asp:GridView ID="gvListaAdevSabloane" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    DataKeyNames="adevSablonId" DataSourceID="SqlListaAdevSabloane" CssClass="tabela"
                    OnRowDataBound="gvListaAdevSabloane_RowDataBound" OnSelectedIndexChanged="gvListaAdevSabloane_SelectedIndexChanged"
                    OnDataBound="gvListaAdevSabloane_DataBound" AllowSorting="True" 
                    oninit="gvListaAdevSabloane_Init"  
                    EmptyDataText="Nu există șabloane proprii create. Apăsați butonul 'adaugă un șablon de adeverință'! " 
                    onprerender="gvListaAdevSabloane_PreRender">
                    <Columns>
                        <asp:BoundField DataField="adevSablonId" HeaderText="Nr." InsertVisible="False" ReadOnly="True"
                            SortExpression="adevSablonId" Visible="false" />
                        <asp:BoundField DataField="unitateDenumire" HeaderText="Creat de" SortExpression="unitateDenumire" />
                        <asp:BoundField DataField="adevSablonDenumire" HeaderText="Denumire șablon" SortExpression="adevSablonDenumire" />
                        <asp:BoundField DataField="adevAntetId" HeaderText="Antet" SortExpression="adevAntetId" />
                        <asp:BoundField DataField="adevSubsolId" HeaderText="Subsol" SortExpression="adevSubsolId" />
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                            <asp:Label ID="lblText" runat="server" Text='<%# Bind("adevSablonText") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="adevTip" HeaderText="vizibil pentru" SortExpression="adevTip" />
                    </Columns>
                    <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                </asp:GridView>
                <asp:SqlDataSource ID="SqlListaAdevSabloane" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016 %>"
                    
                    
                    SelectCommand="SELECT adeverinteSabloane.adevSablonId, adeverinteSabloane.unitateId, adeverinteSabloane.adevTip, adeverinteSabloane.adevSablonTip, adeverinteSabloane.adevSablonTitlu, adeverinteSabloane.adevSablonText, adeverinteSabloane.adevSubsolId, adeverinteSabloane.adevAntetId, adeverinteSabloane.adevSablonObservatii, adeverinteSabloane.adevSablonDenumire, unitati.unitateId AS Expr1, unitati.unitateDenumire, unitati.unitateCodSiruta, unitati.judetId, unitati.localitateId, unitati.unitateStrada, unitati.unitateNr, unitati.unitateAp, unitati.unitateCodPostal, unitati.localitateComponentaId, unitati.unitateCodFiscal, unitati.unitateActiva, unitati.unitatePrincipala, unitati.unitatePrimar, unitati.unitateVicePrimar, unitati.unitateSecretar, unitati.unitateContabil, unitati.unitateSefServiciuAgricol, unitati.unitateInspector1, unitati.unitateInspector2, unitati.unitateInspector3, unitati.unitateEmail, unitati.unitateWeb, unitati.unitateTelefon, unitati.unitateFax, unitati.unitateLocalitateRang, unitati.unitateAdaugaDinC2B, unitati.unitateAdaugaDinC2BC4abc, unitati.unitateAdaugaDinC2BC3, unitati.unitateFocusLaEnter, unitati.unitateAdaugaAnimale, unitati.unitateAnDeschis, unitati.unitateAniInchisi, unitati.unitateTipAfisareStrainas FROM adeverinteSabloane LEFT OUTER JOIN unitati ON unitati.unitateId = adeverinteSabloane.unitateId WHERE (adeverinteSabloane.adevSablonDenumire LIKE '%' + @denumire + '%') AND (unitati.unitateDenumire LIKE '%' + @unitateDenumire + '%') AND (CONVERT (nvarchar, adeverinteSabloane.adevTip) LIKE @creatPentru) ORDER BY adeverinteSabloane.adevSablonId DESC">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="tbCDenumire" DefaultValue="%" Name="denumire" 
                            PropertyName="Text" />
                        <asp:ControlParameter ControlID="tbCCreatDe" DefaultValue="%" 
                            Name="unitateDenumire" PropertyName="Text" />
                        <asp:ControlParameter ControlID="ddlCCreatPentru" DefaultValue="" Name="creatPentru" 
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:Panel ID="pnListaButoanePrincipale" CssClass="butoane" runat="server">
                    <asp:Button CssClass="buton" ID="btModificaAdevSablon" runat="server" Text="modificare şablon de adeverinţă"
                        OnClick="ArataAdaugaAdevSablon" Visible="false" />
                    <asp:Button CssClass="buton" ID="btStergeAdevSablon" runat="server" Text="şterge şablonul"
                        OnClick="StergeSablonul" ValidationGroup="GrupValidareAdevSablon" OnClientClick="return confirm(&quot;Sunteţi sigur că doriţi să ştergeţi?&quot;)"
                        Visible="false" />
                    <asp:Button CssClass="buton" ID="btListaAdaugaAdevSablon" runat="server" Text="adaugă un şablon de adeverinţă"
                        OnClick="ArataAdaugaAdevSablon" />
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="pnAdaugaAdevSablon" CssClass="panel_general" runat="server" Visible="false">
                <asp:Panel ID="Panel1" CssClass="adauga" runat="server">
                    <asp:Panel ID="Panel2" runat="server">
                        <h2>
                            ADAUGĂ / MODIFICĂ UN ŞABLON DE ADEVERINŢĂ</h2>
                    </asp:Panel>
                    <asp:Panel ID="pn9Coloane" runat="server">
                                                <asp:Button ID="bt1" runat="server" Text="1col" onclick="bt1_Click" Visible="false" /> 
                        <asp:UpdatePanel ID="upAntetId" runat="server">
                            <ContentTemplate>
                                <p>
                                    <asp:Label ID="lblDenumireSablon" runat="server" Text="Denumire șablon "></asp:Label>
                                    <asp:TextBox ID="tbDenumireSablon" runat="server" CssClass="rand1"></asp:TextBox>
                                    <asp:Label ID="lblAdevTip" runat="server" Text="Tip șablon adeverință" Visible="false"></asp:Label>
                                    <asp:DropDownList Width="270px" ID="ddAdevTip" runat="server" 
                                        DataSourceID="SqlUnitati" DataTextField="unitateDenumire" 
                                        DataValueField="unitateId" ondatabound="ddAdevTip_DataBound" Visible="false">
                                        <asp:ListItem Value="0">
                                        vizibilă pentru toate unitățile</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="SqlUnitati" runat="server" 
                                        ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016 %>" 
                                        
                                        SelectCommand="SELECT unitati.unitateId, judete.judetDenumire + ' - ' + unitati.unitateDenumire AS unitateDenumire FROM unitati INNER JOIN judete ON unitati.judetId = judete.judetId ORDER BY unitateDenumire">
                                    </asp:SqlDataSource>
                                </p>
                                <p>
                                    <br />
                                    <asp:Label ID="lblAntet" runat="server" Text="(antet)"></asp:Label>
                                    <asp:DropDownList Width="500px" ID="ddAntetId" runat="server" DataSourceID="SqlAnteturi"
                                        DataTextField="adevSubsolDenumire" DataValueField="adevSubsolId" 
                                        oninit="ddAntetId_Init">
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="SqlAnteturi" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016 %>"
                                        SelectCommand="SELECT [adevSubsolId], [adevSubsolDenumire] FROM [adeverinteSubsoluri] WHERE ( adevSubsolTip = '0') ORDER BY [adevSubsolDenumire]">
                                        <SelectParameters>
                                            <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" Type="Int32" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </p>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <p>
                            <asp:Label ID="lblText" runat="server" Text="(textul principal)" CssClass="rand3"></asp:Label>
                            <br />
                            <asp:TextBox TextMode="MultiLine" Rows="15" ID="tbText" runat="server" CssClass="rand2_sablon"></asp:TextBox>
                                                <script type="text/javascript">
                        //<![CDATA[
                        CKEDITOR.replace('ctl00_ContentPlaceHolder1_tbText',
					{
					    fullPage: true,
					    width:750,
					    height: 750
					});
					//]]>
                    </script>
                         <br /><asp:Label ID="lblLegenda1" runat="server" Text=" " CssClass="rand3"></asp:Label>
                        </p>
                        <asp:UpdatePanel ID="upSubsol" runat="server">
                            <ContentTemplate>
                                <p class="butoane">
                                    <br />
                                    <asp:Label ID="lblSubsol" runat="server" Text="(subsol)"></asp:Label>
                                    <asp:DropDownList ID="ddSubsol" runat="server" Width="500px" DataSourceID="SqlSubsoluri"
                                        DataTextField="adevSubsolDenumire" DataValueField="adevSubsolId" 
                                        oninit="ddSubsol_Init">
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="SqlSubsoluri" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016 %>"
                                        SelectCommand="SELECT [adevSubsolId], [adevSubsolDenumire] FROM [adeverinteSubsoluri] WHERE ( adevSubsolTip = '1') ORDER BY [adevSubsolDenumire]">
                                        <SelectParameters>
                                            <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" Type="Int32" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </p>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                        <br />
                    </asp:Panel>
                    <asp:Panel ID="pnButoanePrincipale" runat="server" CssClass="butoane">
                        <asp:Button CssClass="buton" ID="btAdaugaAdevSablon" runat="server" Text="adaugă un şablon de adeverinţă"
                            OnClick="AdaugaModificaAdevSablon" Visible="true" />
                        <asp:Button ID="btAdaugaModificaAdevSablon" runat="server" CssClass="buton" OnClick="AdaugaModificaAdevSablon"
                            Text="modificare şablon de adeverinţă" Visible="true" />
                        <asp:Button ID="btModificaArataListaAdevSablon" runat="server" CssClass="buton" OnClick="ModificaArataListaAdevSablon"
                            Text="lista şabloanelor" Visible="true" />
                        <asp:Button ID="btAdaugaArataListaAdevSablon" runat="server" CssClass="buton" OnClick="AdaugaArataListaAdevSablon"
                            Text="lista şabloanelor" Visible="true" />
                    </asp:Panel>                        
                    <asp:Label   Width="100%" 
                                ID="lblLegenda"   runat="server" CssClass="rand2_sablon" 
                                ></asp:Label>
                </asp:Panel>
            </asp:Panel>

</asp:Content>
