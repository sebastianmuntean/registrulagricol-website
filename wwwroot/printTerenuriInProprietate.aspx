﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="printTerenuriInProprietate.aspx.cs" Inherits="printTerenuriInProprietate"
    Culture="ro-RO" UICulture="ro-RO" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
        <asp:Label ID="url" runat="server" Text="Rapoarte / Centralizator Terenuri aflate in proprietate" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server" AsyncRendering="true" SizeToReportContent="true">
    <asp:Panel ID="pnLista" runat="server" CssClass="panel_general" Visible="true">
    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt"
        Height="800px" Width="100%">
        <LocalReport ReportPath="rapoarte\raportTerenuriInProprietate.rdlc">
           
        </LocalReport>
    </rsweb:ReportViewer>
    
        </asp:Panel>
</asp:Content>
