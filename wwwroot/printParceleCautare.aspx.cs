﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;
using System.Data.SqlClient;

public partial class printParceleCautare : System.Web.UI.Page
{
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            switch (Session["cpOrdonare"].ToString())
            {
                case "0": ObjectDataSource2.SelectMethod = "GetData"; break;
                case "1": ObjectDataSource2.SelectMethod = "GetData1"; break;
                case "2": ObjectDataSource2.SelectMethod = "GetData2"; break;
                case "3": ObjectDataSource2.SelectMethod = "GetData3"; break;
                case "4": ObjectDataSource2.SelectMethod = "GetData4"; break;
                case "5": ObjectDataSource2.SelectMethod = "GetData5"; break;
            }
           
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "printCautareParcela", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
            rvParceleCautare.LocalReport.Refresh();
        }
    }

    /*protected void Page_SaveStateComplete(object sender, EventArgs e)
    {
        Warning[] warnings;
        string[] streamids;
        string mimeType;
        string encoding;
        string fileNameExtension;
        byte[] bytes = rvParceleCautare.LocalReport.Render("PDF", null, out mimeType, out encoding, out fileNameExtension, out streamids, out warnings);
        Response.Buffer = true;
        Response.Clear();
        Response.ContentType = mimeType;
        Response.AddHeader("content-disposition", "inline; filename=Nume" + fileNameExtension);
        Response.BinaryWrite(bytes);
        Response.Flush();
        Response.End();
        Response.Close();
    }*/
}
