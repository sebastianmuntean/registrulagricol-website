﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="certificatProducator.aspx.cs" Inherits="certificatProducator" EnableEventValidation="false"
    UICulture="ro-Ro" Culture="ro-Ro" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnListaGospodarii" runat="server" CssClass="panel_general" Visible="true">
                <h1>
                    Rapoarte / Certificat de producător</h1>
                <asp:Panel ID="Panel4" runat="server">
                    <asp:Label ID="lblEroare1" runat="server" CssClass="validator" Text="Eroare" Visible="false" />
                </asp:Panel>
                <asp:Panel ID="pnlAfis" runat="server" Visible="true">
                    <asp:Panel ID="pnCautare" runat="server" CssClass="cauta">
                        <asp:Label ID="lbCautaDupa" runat="server" Text="Caută:"></asp:Label>
                        <asp:Label ID="lblCNume" runat="server" Text="Nume"></asp:Label>
                        <asp:TextBox ID="tbCNume" AutoPostBack="true" runat="server"></asp:TextBox>
                        <asp:Label ID="lblCCNP" runat="server" Text="CNP"></asp:Label>
                        <asp:TextBox ID="tbCCNP" AutoPostBack="true" runat="server"></asp:TextBox>
                        <asp:Label ID="lblCDataB" runat="server" Text="Din data "></asp:Label>
                        <asp:TextBox ID="tbCDataB" Width="80px" AutoPostBack="true" runat="server" OnInit="tbCDataB_Init"></asp:TextBox>
                        <asp:Label ID="lblCDataE" runat="server" Text=" la data "></asp:Label>
                        <asp:TextBox ID="tbCDataE" Width="80px" AutoPostBack="true" runat="server" OnInit="tbCDataE_Init"></asp:TextBox>
                        <asp:Label ID="lblCNr" runat="server" Text="Număr"></asp:Label>
                        <asp:TextBox ID="tbCNr" AutoPostBack="true" runat="server"></asp:TextBox>
                        <asp:Label ID="lblCSerie" runat="server" Text="Serie"></asp:Label>
                        <asp:TextBox ID="tbCSerie" AutoPostBack="true" runat="server"></asp:TextBox>
                    </asp:Panel>
                    <asp:GridView ID="gvAfis" EmptyDataText="Nu sunt înregistrări !!!" CssClass="tabela"
                        Width="100%" runat="server" AutoGenerateColumns="False" DataKeyNames="certProdId"
                        DataSourceID="SqlDataSourceAfis" AllowPaging="True" AllowSorting="True" OnPreRender="gvAfis_PreRender"
                        OnRowDataBound="gvAfis_RowDataBound" OnSelectedIndexChanged="gvAfis_SelectedIndexChanged">
                        <Columns>
                            <asp:BoundField DataField="membriCpNume" HeaderText="Nume" SortExpression="membriCpNume" />
                            <asp:BoundField DataField="membriCpCNP" HeaderText="CNP" SortExpression="membriCpCNP" />
                            <asp:BoundField DataField="certProdNr" HeaderText="Nr." SortExpression="certProdNr" />
                            <asp:BoundField DataField="certProdData" DataFormatString="{0:d}" HeaderText="Data"
                                SortExpression="certProdData" />
                            <asp:BoundField DataField="certificatProducatorSerie" HeaderText="Serie certificat original"
                                SortExpression="certificatProducatorSerie" />
                            <asp:BoundField DataField="certificatProducatorNr" HeaderText="Nr. certificat original"
                                SortExpression="certificatProducatorNr" />
                        </Columns>
                        <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                        <HeaderStyle Font-Bold="True" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlDataSourceAfis" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString %>"
                        SelectCommand="SELECT certProd.certProdId, certProd.unitateId, certProd.gospodarieId, certProd.an, certProd.certProdNr, certProd.certProdData, certProd.certificatProducatorSerie, certProd.certificatProducatorNr, membriCp.membriCpNume, membriCp.membriCpCNP FROM certProd INNER JOIN membriCp ON certProd.certProdId = membriCp.certProdId LEFT OUTER JOIN produsCp ON certProd.certProdId = produsCp.certProdId WHERE (certProd.unitateId = @unitateId) AND (membriCp.membriCpTip = 0) AND (certProd.an = @an) AND (membriCp.membriCpCNP LIKE @cnp + '%') AND (certProd.certificatProducatorSerie LIKE @serie + '%') AND (certProd.certificatProducatorNr LIKE @nr + '%') AND (membriCp.membriCpNume LIKE @nume + '%') AND (certProd.certProdData &gt;= CONVERT (datetime, @dataB, 104)) AND (certProd.certProdData &lt;= CONVERT (datetime, @dataE, 104)) GROUP BY certProd.certProdId, certProd.unitateId, certProd.gospodarieId, certProd.an, certProd.certProdNr, certProd.certProdData, certProd.certificatProducatorSerie, certProd.certificatProducatorNr, membriCp.membriCpNume, membriCp.membriCpCNP">
                        <SelectParameters>
                            <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" />
                            <asp:SessionParameter Name="an" SessionField="SESan" />
                            <asp:ControlParameter ControlID="tbCCNP" DefaultValue="%" Name="cnp" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbCSerie" DefaultValue="%" Name="serie" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbCNr" DefaultValue="%" Name="nr" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbCNume" DefaultValue="%" Name="nume" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbCDataB" DefaultValue="" Name="dataB" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbCDataE" Name="dataE" PropertyName="Text" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:Panel ID="pnlButoane" CssClass="butoane" runat="server" Visible="true">
                        <asp:Button ID="btModifica" CssClass="buton" runat="server" Text="Modifică" OnClick="btModifica_Click" />
                        <asp:Button ID="btSterge" CssClass="buton" OnClientClick="return confirm('Sunteţi sigur ştergeţi !!!');"
                            runat="server" Text="Şterge" OnClick="btSterge_Click" />
                        <asp:Button ID="btAdauga" CssClass="buton" runat="server" Text="Adaugă" OnClick="btAdauga_Click" />
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel ID="pnlAdaugaModifica" CssClass="adauga" runat="server" Visible="False">
                    <asp:Panel ID="pnlAdaugaModificaH" runat="server">
                        <asp:Label ID="lblCertProdId" Visible="false" runat="server" Text=""></asp:Label>
                        <p>
                            <asp:Label ID="Label10" runat="server" Text="Serie certificat de producător"></asp:Label>
                            <asp:TextBox ID="tbSerieCP" runat="server"></asp:TextBox>
                            <asp:Label ID="Label11" runat="server" Text="Nr."></asp:Label>
                            <asp:TextBox ID="tbNrCp" runat="server"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="tbNrCp_FilteredTextBoxExtender" runat="server" Enabled="True"
                                TargetControlID="tbNrCp" ValidChars="0123456789">
                            </asp:FilteredTextBoxExtender>
                        </p>
                        <p>
                            <asp:Label ID="lblNr" runat="server" Text="Număr"></asp:Label>
                            <asp:TextBox ID="tbNrCertificat" runat="server"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="tbNrCertificat_FilteredTextBoxExtender" runat="server"
                                Enabled="True" TargetControlID="tbNrCertificat" ValidChars="0123456789">
                            </asp:FilteredTextBoxExtender>
                            <asp:Label ID="lblData" runat="server" Text="Data"></asp:Label>
                            <asp:TextBox ID="tbData" runat="server"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="tbData_FilteredTextBoxExtender" runat="server" Enabled="True"
                                TargetControlID="tbData" ValidChars="0123456789./">
                            </asp:FilteredTextBoxExtender>
                            <asp:CalendarExtender ID="tbData_CalendarExtender" runat="server" Enabled="True"
                                Format="dd.MM.yyyy" TargetControlID="tbData">
                            </asp:CalendarExtender>
                        </p>
                         <p>
                              <asp:Label ID="Label24" runat="server" Text="Număr aviz"></asp:Label>
                            <asp:TextBox ID="numarAvizTextBox" runat="server"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
                                Enabled="True" TargetControlID="numarAvizTextBox" ValidChars="0123456789">
                            </asp:FilteredTextBoxExtender>
                            <asp:Label ID="Label25" runat="server" Text="Data aviz"></asp:Label>
                            <asp:TextBox ID="dataAvizTextBox" runat="server"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" Enabled="True"
                                TargetControlID="dataAvizTextBox" ValidChars="0123456789./">
                            </asp:FilteredTextBoxExtender>
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True"
                                Format="dd.MM.yyyy" TargetControlID="dataAvizTextBox">
                            </asp:CalendarExtender>
                                 </p>

                              <p>
                              <asp:Label ID="Label26" runat="server" Text="Număr atestat"></asp:Label>
                            <asp:TextBox ID="numarAtestatTextBox" runat="server"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server"
                                Enabled="True" TargetControlID="numarAtestatTextBox" ValidChars="0123456789">
                            </asp:FilteredTextBoxExtender>
                            <asp:Label ID="Label27" runat="server" Text="Data atestat"></asp:Label>
                            <asp:TextBox ID="dataAtestatTextBox" runat="server"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" Enabled="True"
                                TargetControlID="dataAtestatTextBox" ValidChars="0123456789./">
                            </asp:FilteredTextBoxExtender>
                            <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True"
                                Format="dd.MM.yyyy" TargetControlID="dataAtestatTextBox">
                            </asp:CalendarExtender>
                                 </p>
                        <h1>
                            Adaugă / modifică membrii</h1>
                    </asp:Panel>
                    <asp:Panel ID="pnlMembrii" runat="server">
                        <asp:Panel ID="pnlAfisMembrii" runat="server">
                            <asp:GridView ID="gvMembrii" EmptyDataText="Nu sunt înregistrări !!!" CssClass="tabela"
                                Width="100%" runat="server" OnPreRender="gvMembrii_PreRender" OnRowDataBound="gvMembrii_RowDataBound"
                                OnSelectedIndexChanged="gvMembrii_SelectedIndexChanged" AllowPaging="True" AllowSorting="True"
                                AutoGenerateColumns="False" DataKeyNames="membriCpId" DataSourceID="SqlDataSourceAfisMembrii">
                                <Columns>
                                    <asp:BoundField DataField="membriCpNume" HeaderText="Nume" SortExpression="membriCpNume" />
                                    <asp:BoundField DataField="membriCpJudet" HeaderText="Judet" SortExpression="membriCpJudet" />
                                    <asp:BoundField DataField="membriCpLocalitate" HeaderText="Localitate" SortExpression="membriCpLocalitate" />
                                    <asp:BoundField DataField="membriCpStrada" HeaderText="Strada" SortExpression="membriCpStrada" />
                                    <asp:BoundField DataField="membriCpNr" HeaderText="Nr" SortExpression="membriCpNr" />
                                    <asp:BoundField DataField="membriCpCNP" HeaderText="CNP" SortExpression="membriCpCNP" />
                                </Columns>
                                <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                                <HeaderStyle Font-Bold="True" />
                            </asp:GridView>
                            <asp:SqlDataSource ID="SqlDataSourceAfisMembrii" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString %>"
                                SelectCommand="SELECT membriCpId, membriCpNume, membriCpJudet, membriCpLocalitate, membriCpStrada, membriCpNr, membriCpActIdenSerie, membriCpActIdentNr, membriCpCNP, membriCpTip, unitateId, certProdId FROM membriCp WHERE (unitateId = @unitateId) AND (certProdId = @certProdId)">
                                <SelectParameters>
                                    <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" />
                                    <asp:ControlParameter ControlID="lblCertProdId" DefaultValue="-1" Name="certProdId"
                                        PropertyName="Text" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                            <asp:Panel ID="pnlButoaneMembrii" runat="server">
                                <asp:Button ID="btModificaMembri" CssClass="buton" runat="server" Text="Modifică"
                                    OnClick="btModificaMembri_Click" />
                                <asp:Button ID="btStergeMembri" CssClass="buton" OnClientClick="return confirm('Sunteţi sigur ştergeţi !!!');"
                                    runat="server" Text="Şterge" OnClick="btStergeMembri_Click" />
                                <asp:Button ID="btAdaugaMembri" CssClass="buton" runat="server" Text="Adaugă" OnClick="btAdaugaMembri_Click" />
                                <asp:Button Visible="false" ID="btModificaPreluareMembri" CssClass="buton" runat="server" 
                                    Text="Preluare membri" onclick="btModificaPreluareMembri_Click" />
                            </asp:Panel>
                        </asp:Panel>
                        <asp:Panel ID="pnlAdaugaModificaMembrii" Visible="false" runat="server">
                            <p>
                                <asp:Label ID="Label3" runat="server" Text="Tip"></asp:Label>
                                <asp:DropDownList ID="ddlTip" runat="server">
                                    <asp:ListItem Selected="True" Text="Titular" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Membru din componenţa gospodăriei" Value="1"></asp:ListItem>
                                </asp:DropDownList>
                            </p>
                            <p>
                                <asp:Label ID="Label1" runat="server" Text="Nume"></asp:Label>
                                <asp:TextBox ID="tbNume" runat="server"></asp:TextBox>
                                <asp:Label ID="Label2" runat="server" Text="Prenume"></asp:Label>
                                <asp:TextBox ID="tbPrenume" runat="server"></asp:TextBox></p>
                            <p>
                                <asp:Label ID="Label4" runat="server" Text="Judet"></asp:Label>
                                <asp:TextBox ID="tbJudet" runat="server"></asp:TextBox>
                                <asp:Label ID="Label5" runat="server" Text="Localitate"></asp:Label>
                                <asp:TextBox ID="tbLocalitate" runat="server"></asp:TextBox></p>
                            <p>
                                <asp:Label ID="Label6" runat="server" Text="Strada"></asp:Label>
                                <asp:TextBox ID="tbStrada" runat="server"></asp:TextBox>
                                <asp:Label ID="Label7" runat="server" Text="Nr"></asp:Label>
                                <asp:TextBox ID="tbNr" runat="server"></asp:TextBox></p>
                            <p>
                                <asp:Label ID="Label8" runat="server" Text="Act de identitate seria"></asp:Label>
                                <asp:TextBox ID="tbSerie" runat="server"></asp:TextBox>
                                <asp:Label ID="Label9" runat="server" Text=" Nr."></asp:Label>
                                <asp:TextBox ID="tbNrActIdentitate" runat="server"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="tbNrActIdentitate_FilteredTextBoxExtender" runat="server"
                                    Enabled="True" TargetControlID="tbNrActIdentitate" ValidChars="0123456789">
                                </asp:FilteredTextBoxExtender>
                            </p>
                            <asp:Label ID="Label13" runat="server" Text=" CNP"></asp:Label>
                            <asp:TextBox ID="tbCNP" runat="server"></asp:TextBox></p>
                            <asp:FilteredTextBoxExtender ID="tbCNP_FilteredTextBoxExtender" runat="server" Enabled="True"
                                TargetControlID="tbCNP" ValidChars="0123456789">
                            </asp:FilteredTextBoxExtender>
                            <asp:Panel ID="pnlButoaneSalveazaInapoiMembrii" CssClass="butoane" runat="server"
                                Visible="true">
                                <asp:Button ID="btSalveazaM" CssClass="buton" runat="server" Text="Salvează" OnClick="btSalveazaM_Click" />
                                <asp:Button ID="btInapoiM" CssClass="buton" runat="server" Text="Înapoi" OnClick="btInapoiM_Click" />
                            </asp:Panel>
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel ID="pnlProduse" runat="server">
                        <h1>
                            Adaugă / modifică produse
                        </h1>
                        <p>
                            <asp:GridView ID="gvProduseV" AutoGenerateColumns="false" CssClass="tabela" Width="100%"
                                runat="server">
                                <Columns>
                                    <asp:TemplateField HeaderText="Produsul">
                                        <ItemTemplate>
                                            <asp:TextBox BorderStyle="None" ID="tbProdus" Text='<%# DataBinder.Eval(Container, "DataItem") %>'
                                                runat="server"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <table style="width: 100%;">
                                                <thead>
                                                    Suprafaţa</thead>
                                                <tr>
                                                    <td width="50%">
                                                        ha
                                                    </td>
                                                    <td width="50%">
                                                        ari
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="tbHa" onfocus="this.select();" Width="98%" BorderStyle="None" runat="server"></asp:TextBox>
                                                        <asp:FilteredTextBoxExtender ID="tbNrCertificat_FilteredTextBoxExtender1" runat="server"
                                                            Enabled="True" TargetControlID="tbHa" ValidChars="0123456789,.">
                                                        </asp:FilteredTextBoxExtender>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="tbAri" onfocus="this.select();" Width="98%" BorderStyle="None" runat="server"></asp:TextBox>
                                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="True"
                                                            TargetControlID="tbAri" ValidChars="0123456789,.">
                                                        </asp:FilteredTextBoxExtender>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Producţia estimată (kg)">
                                        <ItemTemplate>
                                            <asp:TextBox ID="tbProductieEstimata" onfocus="this.select();" Width="98%" BorderStyle="None"
                                                runat="server"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="tbNrCertificat_FilteredTextBoxExtender2" runat="server"
                                                Enabled="True" TargetControlID="tbProductieEstimata" ValidChars="0123456789,.">
                                            </asp:FilteredTextBoxExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Din care pt. comercializare (kg)">
                                        <ItemTemplate>
                                            <asp:TextBox ID="tbProductieComercializata" onfocus="this.select();" Width="98%"
                                                BorderStyle="None" runat="server"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="tbNrCertificat_FilteredTextBoxExtender3" runat="server"
                                                Enabled="True" TargetControlID="tbProductieComercializata" ValidChars="0123456789,.">
                                            </asp:FilteredTextBoxExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </p>
                        <p>
                            <asp:GridView ID="gvProduseA" AutoGenerateColumns="false" CssClass="tabela" Width="100%"
                                runat="server">
                                <Columns>
                                    <asp:TemplateField HeaderText="Produsul">
                                        <ItemTemplate>
                                            <asp:TextBox BorderStyle="None" Width="98%" ID="tbProdus" Text='<%# DataBinder.Eval(Container, "DataItem") %>'
                                                runat="server"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Număr">
                                        <ItemTemplate>
                                            <asp:TextBox ID="tbNr" BorderStyle="None" onfocus="this.select();" Width="98%" runat="server"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="tbNrCertificat_FilteredTextBoxExtender5" runat="server"
                                                Enabled="True" TargetControlID="tbNr" ValidChars="0123456789">
                                            </asp:FilteredTextBoxExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Producţia estimată (kg)">
                                        <ItemTemplate>
                                            <asp:TextBox ID="tbProductieEstimata" onfocus="this.select();" Width="98%" BorderStyle="None"
                                                runat="server"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="tbNrCertificat_FilteredTextBoxExtender6" runat="server"
                                                Enabled="True" TargetControlID="tbProductieEstimata" ValidChars="0123456789,.">
                                            </asp:FilteredTextBoxExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Din care pt. comercializare (kg)">
                                        <ItemTemplate>
                                            <asp:TextBox ID="tbProductieComercializata" onfocus="this.select();" Width="98%"
                                                BorderStyle="None" runat="server"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="tbNrCertificat_FilteredTextBoxExtender7" runat="server"
                                                Enabled="True" TargetControlID="tbProductieComercializata" ValidChars="0123456789,.">
                                            </asp:FilteredTextBoxExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </p>
                    </asp:Panel>
                    <asp:Panel ID="pnlButoaneSalveazaInapoi" CssClass="butoane" runat="server" Visible="true">
                        <asp:Button ID="btSalveaza" CssClass="buton" runat="server" Text="Salvează" OnClick="btSalveaza_Click" />
                        <asp:Button ID="btInapoi" CssClass="buton" runat="server" Text="Înapoi" OnClick="btInapoi_Click" />
                    </asp:Panel>
                    <asp:Panel ID="pnlPopupAdaugaProduseComercializate" runat="server" Visible="false"
                        ScrollBars="Vertical">
                        <div style="width: 100%; height: 100%; min-height: 800px; background-color: LightGrey;
                            position: absolute; top: 0; left: 0; opacity: 0.7; filter: alpha(opacity=70);
                            z-index: 100;">
                        </div>
                        <div style="position: absolute; left: 50%; top: 50%; width: 1000px; height: 550px;
                            position: fixed; background-color: white; border: solid 1px black; margin: -275px auto 0 -500px;
                            text-align: center; opacity: 1; filter: alpha(opacity=100); border-radius: 5px;
                            overflow: hidden; -moz-box-shadow: 10px 10px 5px #888; -webkit-box-shadow: 10px 10px 5px #888;
                            box-shadow: 10px 10px 5px #888; z-index: 100; overflow: auto;">
                            <br />
                            <asp:Panel ID="pnAfiseazaMesaje" runat="server">
                                <asp:ValidationSummary ID="valSumParcele" runat="server" DisplayMode="SingleParagraph"
                                    Visible="true" ValidationGroup="GrupValidare" CssClass="validator" ForeColor="" />
                            </asp:Panel>
                            <asp:Panel ID="pnlValidator" CssClass="ascunsa" runat="server">
                                <asp:CustomValidator ID="valCustom" runat="server" ErrorMessage="" ValidationGroup="GrupValidare"></asp:CustomValidator>
                            </asp:Panel>
                            <br />
                            <asp:Panel ID="pnlPopUp" runat="server">
                                <asp:Label ID="lblMsg" runat="server" Text="Adaugă produsele şi cantităţile destinate comercializării ?"></asp:Label>
                            </asp:Panel>
                            <asp:Panel ID="pnlButoanePopup" runat="server">
                                <asp:Button ID="btNu" OnClick="btDa_OnClick" CssClass="buton" runat="server" Text="DA" />
                                <asp:Button ID="btDa" OnClick="btNu_OnClick" CssClass="buton" runat="server" Text="NU" />
                            </asp:Panel>
                            <asp:Panel ID="pnlDa" Visible="false" runat="server">
                                <asp:GridView ID="gvProdusePopup" OnRowDataBound="gvProdusePopup_OnRowDataBound"
                                    Width="96%" CssClass="tabela" AutoGenerateColumns="false" runat="server">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Produsul">
                                            <ItemTemplate>
                                                <asp:TextBox ID="tbProdusul" ReadOnly="true" Width="98%" BorderStyle="None" Text='<%# DataBinder.Eval(Container, "DataItem[0]") %>'
                                                    runat="server"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Disponibil (kg)">
                                            <ItemTemplate>
                                                <asp:Label ID="lblIndex" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem[2]") %>'></asp:Label>
                                                <asp:Label ID="lblTip" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem[3]") %>'></asp:Label>
                                                <asp:Label ID="lblDisponibil" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem[1]") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <table border="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <td colspan="4">
                                                                <asp:Label ID="Label23" runat="server" Text="CANTITĂŢI PE TRIMESTRE"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" width="50%">
                                                                <asp:Label ID="Label21" runat="server" Text="Anul"></asp:Label>
                                                                <asp:TextBox ID="tbAnul1" Width="60px" BorderStyle="None" onfocus="this.select();"
                                                                    runat="server"></asp:TextBox>
                                                                <asp:FilteredTextBoxExtender ID="tbData_FilteredTextBoxExtender534" runat="server"
                                                                    Enabled="True" TargetControlID="tbAnul1" ValidChars="0123456789">
                                                                </asp:FilteredTextBoxExtender>
                                                            </td>
                                                            <td colspan="2" width="50%">
                                                                <asp:Label ID="Label22" runat="server" Text="Anul"></asp:Label>
                                                                <asp:TextBox ID="tbAnul2" Width="60px" BorderStyle="None" onfocus="this.select();"
                                                                    runat="server"></asp:TextBox>
                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3452" runat="server" Enabled="True"
                                                                    TargetControlID="tbAnul2" ValidChars="0123456789">
                                                                </asp:FilteredTextBoxExtender>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p>
                                                                    <asp:Label ID="Label12" runat="server" Text="Trim. III"></asp:Label></p>
                                                                <p>
                                                                    <asp:Label ID="Label14" runat="server" Text="iul-aug-sept"></asp:Label></p>
                                                            </td>
                                                            <td>
                                                                <p>
                                                                    <asp:Label ID="Label15" runat="server" Text="Trim. IV"></asp:Label></p>
                                                                <p>
                                                                    <asp:Label ID="Label16" runat="server" Text="oct-nov-dec"></asp:Label></p>
                                                            </td>
                                                            <td>
                                                                <p>
                                                                    <asp:Label ID="Label17" runat="server" Text="Trim. I"></asp:Label></p>
                                                                <p>
                                                                    <asp:Label ID="Label18" runat="server" Text="ian-feb-mart"></asp:Label></p>
                                                            </td>
                                                            <td>
                                                                <p>
                                                                    <asp:Label ID="Label19" runat="server" Text="Trim. II"></asp:Label></p>
                                                                <p>
                                                                    <asp:Label ID="Label20" runat="server" Text="apr-mai-iun"></asp:Label></p>
                                                            </td>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <table border="0" width="100%">
                                                    <tr>
                                                        <td width="25%">
                                                            <asp:TextBox ID="tbT3" BorderStyle="None" Widt="98%" onfocus="this.select();" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td width="25%">
                                                            <asp:TextBox ID="tbT4" BorderStyle="None" Widt="98%" onfocus="this.select();" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td width="25%">
                                                            <asp:TextBox ID="tbT1" BorderStyle="None" Widt="98%" onfocus="this.select();" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td width="25%">
                                                            <asp:TextBox ID="tbT2" BorderStyle="None" Widt="98%" onfocus="this.select();" runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <asp:Button ID="btSalveazaPopup" OnClick="btSalveazaPopup_OnClick" CssClass="buton"
                                    runat="server" Text="Salvează" />
                                <asp:Button ID="btInapoiPopup" OnClick="btInapoiPopup_OnClick" CssClass="buton" runat="server"
                                    Text="Înapoi" />
                            </asp:Panel>
                        </div>
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
