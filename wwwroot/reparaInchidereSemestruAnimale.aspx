﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="reparaInchidereSemestruAnimale.aspx.cs" Inherits="reparaInchidereSemestruAnimale" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnAfiseazaMesaje" runat="server">
                <asp:ValidationSummary ID="valSumUtilizatori" runat="server" DisplayMode="SingleParagraph"
                    Visible="true" ValidationGroup="GrupValidareExport" CssClass="validator" ForeColor="" />
                <asp:CustomValidator ID="valCustom" runat="server" ErrorMessage="" ValidationGroup="GrupValidareExport"></asp:CustomValidator>
            </asp:Panel>
            <asp:Panel ID="pnListaLocalitati" runat="server" CssClass="panel_general" Visible="true">
                <asp:Panel ID="pnGrid" runat="server" Visible="true">
                    <h1>Preia date din semestrul 1 in semestrul 2 la capitolul 7
                    </h1>
                    <p>
                        Atentie!!! Datele din semestrul 2 vor fi inlocuite cu datele din semestrul 1
                    </p>
                    <p>
                        <asp:Label ID="lblAn" runat="server" Text="An"></asp:Label>
                        <asp:TextBox ID="tbAn" runat="server"></asp:TextBox>
                    </p>
                    <p>
                        <asp:Label ID="lblUnitatea" runat="server" Text="Unitatea"></asp:Label>
                        <asp:DropDownList ID="ddlUnitate" runat="server" DataSourceID="SqlUnitati" DataTextField="unitate"
                            DataValueField="unitateId">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="SqlUnitati" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString %>"
                            SelectCommand="SELECT judete.judetDenumire + ' - ' + unitati.unitateDenumire AS unitate, unitati.unitateId FROM unitati INNER JOIN judete ON unitati.judetId = judete.judetId ORDER BY unitate"></asp:SqlDataSource>
                    </p>
                    <asp:Panel ID="pnButoane" CssClass="butoane" runat="server">
                        <asp:Button CssClass="buton" ID="btAdauga" runat="server" Text="salveaza" OnClick="btAdauga_Click" />
                    </asp:Panel>
                    <asp:Panel ID="Panel1" CssClass="butoane" runat="server">
                        <br />
                        Total Inregistrari Inainte Pe Capitol: <asp:Label ID="lblTotalInregistrariInaintePeCapitol" runat="server"></asp:Label>
                        <br />
                        Total Inregistrari Dupa Stergere Pe Capitol: <asp:Label ID="lblTotalInregistrariDupaStergerePeCapitol" runat="server"></asp:Label>
                        <br />
                        Total Sterse: <asp:Label ID="lblTotalSterse" runat="server"></asp:Label>
                        <br />
                        Ce ar trebui sters pentru unitate: <asp:Label ID="lblColoane" runat="server"></asp:Label>
                        <br />
                        <asp:Button CssClass="buton" ID="btVerifica" runat="server" Text="verifica" OnClick="btVerifica_Click" />
                        <asp:Button CssClass="buton" ID="btCurataCapitole" runat="server" Text="curata capitole" OnClick="btCurataCapitole_Click" />
                        <asp:Button CssClass="buton" ID="btCurataMaiMulte" runat="server" Text="curata mai multe unitati" OnClick="btCurataMaiMulte_Click" />
                        <asp:Button CssClass="buton" ID="btCurataMaiMulteDeLaPanaLa" runat="server" Text="curata mai multe unitati" OnClick="btCurataMaiMulteDeLaPanaLa_Click" />

                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TextBox8" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TextBox9" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TextBox10" runat="server"></asp:TextBox>                        
                        
                        <asp:TextBox ID="TextBox11" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TextBox12" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TextBox13" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TextBox14" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TextBox15" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TextBox16" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TextBox17" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TextBox18" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TextBox19" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TextBox20" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TextBox21" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TextBox22" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TextBox23" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TextBox24" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TextBox25" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TextBox26" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TextBox27" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TextBox28" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TextBox29" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TextBox30" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TextBox31" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TextBox32" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TextBox33" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TextBox34" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TextBox35" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TextBox36" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TextBox37" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TextBox38" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TextBox39" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TextBox40" runat="server"></asp:TextBox>

                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
