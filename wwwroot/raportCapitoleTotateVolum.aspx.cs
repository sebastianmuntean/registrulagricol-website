﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class raportCapitoleTotateVolum : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    private void calculCapitole(string pGospodarieId)
    {
        // umplem tabela de rapoarte
        // scoatem lista anilor din ciclul curent
        string vCiclu = ManipuleazaBD.fRezultaUnString("SELECT * FROM cicluri WHERE cicluAni LIKE '%" + Session["SESan"].ToString() + "%'", "cicluAni", Convert.ToInt16(Session["SESan"]));
        char[] vDelimitator = { '#' };
        string[] vAniDinCiclu = vCiclu.Split(vDelimitator, StringSplitOptions.RemoveEmptyEntries);
        List<string> vListaCampuri = new List<string> { "codCapitol", "capitoleCodRand", "denumire1", "denumire2", "denumire3", "denumire4", "denumire5" };
        // gasim gospodaria initiala pentru a lua datele din totii anii pentru aceasta - dupa gospodarieidinitial
        string vGospodarieIdInitial = ManipuleazaBD.fRezultaUnString("SELECT * FROM gospodarii WHERE gospodarieId = '" + pGospodarieId + "'", "gospodarieIdInitial", Convert.ToInt16(Session["SESan"]));
        int vCodRand = 0;
        string vComanda = "";
        int vAnOrdine = 1;
        foreach (string vAn in vAniDinCiclu)
        {
            string vAnCamp = "an" + vAnOrdine.ToString();
            vListaCampuri = new List<string> { "camp1", "camp2", "camp3", "camp4", "camp5", "camp6", "col1", "col2", "col3", "col4", vAnCamp };
            List<List<string>> vValori = new List<List<string>> { };
            vValori = ManipuleazaBD.fRezultaListaStringuri(" SELECT parcele.parcelaDenumire + ' ' + parcele.parcelaTarla as camp1, parcele.parcelaSuprafataIntravilanHa as col1, parcele.parcelaSuprafataIntravilanAri as col2, parcele.parcelaSuprafataExtravilanHa as col3, parcele.parcelaSuprafataExtravilanAri as col4, parcele.parcelaNrTopo as camp2, CASE coalesce(parcele.parcelaCF, '') WHEN '' THEN '' ELSE 'CF: ' + convert(nvarchar, parcele.parcelaCF) + ' / ' END + CASE coalesce(parcele.parcelaNrCadastral, '') WHEN '' THEN '' ELSE 'nr.cad: ' + convert(nvarchar, parcele.parcelaNrCadastral) + ' / ' END + CASE coalesce(parcele.parcelaNrCadastralProvizoriu, '') WHEN '' THEN '' ELSE ' nr.cad.prv. ' + convert(nvarchar, parcele.parcelaNrCadastralProvizoriu) END AS camp3, sabloaneCapitole.denumire1 AS camp4,parcele.parcelaNrBloc AS camp5, CASE coalesce(parcele.parcelaMentiuni, '') WHEN '' THEN '' ELSE convert(nvarchar, parcele.parcelaMentiuni) + ' / ' END  +  convert(nvarchar, parcele.parcelaLocalitate) + ' ' + convert(nvarchar, parcele.parcelaAdresa) + ' ' + CASE parcele.parcelaTitluProprietate WHEN '-' THEN '' WHEN '' THEN '' ELSE 'titlu propr: ' + convert(nvarchar, parcele.parcelaTitluProprietate) + ' / ' END + CASE  parcele.parcelaClasaBonitate WHEN '-' THEN '' ELSE ' cls.bonitate: ' + convert(nvarchar, parcele.parcelaClasaBonitate) + ' / ' END + CASE parcele.parcelaPunctaj WHEN '0' THEN '' ELSE 'punctaj: ' + convert(nvarchar, parcele.parcelaPunctaj) END AS camp6,an" + vAnOrdine.ToString() + " = '" + vAn + "' FROM parcele INNER JOIN  gospodarii ON parcele.gospodarieId = gospodarii.gospodarieId INNER JOIN sabloaneCapitole ON parcele.parcelaCategorie = sabloaneCapitole.codRand AND sabloaneCapitole.capitol = '2a' AND sabloaneCapitole.an = '" + vAn + "' AND parcele.an = '" + vAn + "' WHERE (gospodarii.gospodarieIdInitial = '" + vGospodarieIdInitial + "') ORDER BY camp1 ", vListaCampuri, Convert.ToInt16(Session["SESan"]));
            // construim comanda pentru fiecare rand
            vCodRand = 1;
            if (vValori.Count != 0)
            {
                foreach (List<string> vRandValori in vValori)
                {
                    vComanda += @"INSERT INTO [rapCapitole] ([utilizatorId], [capitol], [codRand], [camp1], [camp2], [camp3], [camp4], [camp5], [camp6], [col1_1], [col1_2], [col1_3], [col1_4], [" + vAnCamp + "], [gospodarieId]) VALUES ('" +
                        Convert.ToInt32(Session["SESutilizatorId"]) + "','2b','" + vCodRand + "','" +
                        vRandValori[0] + "','" +
                        vRandValori[1] + "','" +
                        vRandValori[2] + "','" +
                        vRandValori[3] + "','" +
                        vRandValori[4] + "','" +
                        vRandValori[5] + "','" +
                        vRandValori[6].Replace(',', '.') + "', '" +
                        vRandValori[7].Replace(',', '.') + "', '" +
                        vRandValori[8].Replace(',', '.') + "', '" +
                        vRandValori[9].Replace(',', '.') + "', '" +
                        vRandValori[10] + "', '" + pGospodarieId + "');";
                    vCodRand++;
                }
            }
            else
            {
                vComanda += @"INSERT INTO [rapCapitole] ([utilizatorId], [codRand], [capitol], [" + vAnCamp + "]) VALUES ('" +
                            Convert.ToInt32(Session["SESutilizatorId"]) + "','-','2b', '" + vAn + "');";
            }
            vAnOrdine++;
        }
        // scriem in rapCapitole de unde vom lua la print
        ManipuleazaBD.fManipuleazaBD(vComanda, Convert.ToInt16(Session["SESan"]));
        clsTiparireCapitole.UmpleRapCapitoleFaraStergere(Session["SESutilizatorId"].ToString(), Session["SESan"].ToString(), pGospodarieId, "2a");
        clsTiparireCapitole.UmpleRapCapitoleFaraStergere(Session["SESutilizatorId"].ToString(), Session["SESan"].ToString(), pGospodarieId, "3");
        clsTiparireCapitole.UmpleRapCapitoleFaraStergere(Session["SESutilizatorId"].ToString(), Session["SESan"].ToString(), pGospodarieId, "4a");
        clsTiparireCapitole.UmpleRapCapitoleFaraStergere(Session["SESutilizatorId"].ToString(), Session["SESan"].ToString(), pGospodarieId, "4b");
        clsTiparireCapitole.UmpleRapCapitoleFaraStergere(Session["SESutilizatorId"].ToString(), Session["SESan"].ToString(), pGospodarieId, "4c");
        clsTiparireCapitole.UmpleRapCapitoleFaraStergere(Session["SESutilizatorId"].ToString(), Session["SESan"].ToString(), pGospodarieId, "5a");
        clsTiparireCapitole.UmpleRapCapitoleFaraStergere(Session["SESutilizatorId"].ToString(), Session["SESan"].ToString(), pGospodarieId, "5b");
        clsTiparireCapitole.UmpleRapCapitoleFaraStergere(Session["SESutilizatorId"].ToString(), Session["SESan"].ToString(), pGospodarieId, "5c");
        clsTiparireCapitole.UmpleRapCapitoleFaraStergere(Session["SESutilizatorId"].ToString(), Session["SESan"].ToString(), pGospodarieId, "5d");
        clsTiparireCapitole.UmpleRapCapitoleFaraStergere(Session["SESutilizatorId"].ToString(), Session["SESan"].ToString(), pGospodarieId, "6");
        clsTiparireCapitole.UmpleRapCapitoleFaraStergere(Session["SESutilizatorId"].ToString(), Session["SESan"].ToString(), pGospodarieId, "7");
        clsTiparireCapitole.UmpleRapCapitoleFaraStergere(Session["SESutilizatorId"].ToString(), Session["SESan"].ToString(), pGospodarieId, "8");
        clsTiparireCapitole.UmpleRapCapitoleFaraStergere(Session["SESutilizatorId"].ToString(), Session["SESan"].ToString(), pGospodarieId, "9");
        clsTiparireCapitole.UmpleRapCapitoleFaraStergere(Session["SESutilizatorId"].ToString(), Session["SESan"].ToString(), pGospodarieId, "10a");
        clsTiparireCapitole.UmpleRapCapitoleFaraStergere(Session["SESutilizatorId"].ToString(), Session["SESan"].ToString(), pGospodarieId, "10b");
        clsTiparireCapitole.UmpleRapCapitoleFaraStergere(Session["SESutilizatorId"].ToString(), Session["SESan"].ToString(), pGospodarieId, "11");
    }
    private string pregatesteInsert(long utilizatorId, int tipRap, long capitolId, long gospodarieId, int an, string nume, int codRand, string cnp, int codSex, int codRudenie, string denumireRudenie, DateTime dataNasterii, string mentiuni, string volum, string nrPozitie, string codSiruta, int tip, string strada, string nr, int nrInt, string bl, string sc, string et, string ap, string codExploatatie, string codUnic, string judet, string localitate, bool persJuridica, string jUnitate, string jSubunitate, string jCodFiscal, string jNumeReprez, bool strainas, string sStrada, string sNr, string sBl, string sSc, string sEtj, string sAp, string sJudet, string sLocalitate, string Membru, string cnpMembru, string mentiuneText, string codCapitol, int an1, decimal col1_1, decimal col1_2, decimal col1_3, decimal col1_4, decimal col1_5, decimal col1_6, decimal col1_7, decimal col1_8, int an2, decimal col2_1, decimal col2_2, decimal col2_3, decimal col2_4, decimal col2_5, decimal col2_6, decimal col2_7, decimal col2_8, int an3, decimal col3_1, decimal col3_2, decimal col3_3, decimal col3_4, decimal col3_5, decimal col3_6, decimal col3_7, decimal col3_8, int an4, decimal col4_1, decimal col4_2, decimal col4_3, decimal col4_4, decimal col4_5, decimal col4_6, decimal col4_7, decimal col4_8, int an5, decimal col5_1, decimal col5_2, decimal col5_3, decimal col5_4, decimal col5_5, decimal col5_6, decimal col5_7, decimal col5_8, string camp1, string camp2, string camp3, string camp4, string camp5, string camp6, string camp7, string camp8, string camp9, string camp10, string denumire1, string denumire2, string denumire3, string denumire4, string denumire5, int volumInt, int nrPozitieInt)
    {
        string vInterogare = "";
        vInterogare = "INSERT INTO rapCapitoleRegistru (utilizatorId, tipRap, capitolId, gospodarieId, an, nume, codRand, cnp, codSex, codRudenie, denumireRudenie, dataNasterii, mentiuni, volum, nrPozitie, codSiruta, tip, strada, nr, nrInt, bl, sc, et, ap, codExploatatie, codUnic, judet, localitate, persJuridica, jUnitate, jSubunitate, jCodFiscal, jNumeReprez, strainas, sStrada, sNr, sBl, sSc, sEtj, sAp, sJudet, sLocalitate, Membru, cnpMembru, mentiuneText, codCapitol, an1, col1_1, col1_2, col1_3, col1_4, col1_5, col1_6, col1_7, col1_8, an2, col2_1, col2_2, col2_3, col2_4, col2_5, col2_6, col2_7, col2_8, an3, col3_1, col3_2, col3_3, col3_4, col3_5, col3_6, col3_7, col3_8, an4, col4_1, col4_2, col4_3, col4_4, col4_5, col4_6, col4_7, col4_8, an5, col5_1, col5_2, col5_3, col5_4, col5_5, col5_6, col5_7, col5_8, camp1, camp2, camp3, camp4, camp5, camp6, camp7, camp8, camp9, camp10, denumire1, denumire2, denumire3, denumire4, denumire5, volumInt, nrPozitieInt) VALUES ('" + utilizatorId + "', '" + tipRap + "', '" + capitolId + "', '" + gospodarieId + "', '" + an + "', N'" + nume.Replace("'", "") + "', '" + codRand + "', '" + cnp.Replace("'", "") + "', '" + codSex + "', '" + codRudenie + "', N'" + denumireRudenie.Replace("'", "") + "', convert(datetime,'" + dataNasterii + "',104), N'" + mentiuni.Replace("'", "") + "', '" + volum.Replace("'", "") + "', '" + nrPozitie.Replace("'", "") + "', '" + codSiruta + "', '" + tip + "', N'" + strada.Replace("'", "") + "', '" + nr.Replace("'", "") + "', '" + nrInt + "', '" + bl.Replace("'", "") + "', '" + sc.Replace("'", "") + "', '" + et.Replace("'", "") + "', '" + ap.Replace("'", "") + "', '" + codExploatatie.Replace("'", "") + "', '" + codUnic.Replace("'", "") + "', N'" + judet.Replace("'", "") + "', N'" + localitate.Replace("'", "") + "', '" + persJuridica + "', N'" + jUnitate.Replace("'", "") + "', N'" + jSubunitate.Replace("'", "") + "', '" + jCodFiscal.Replace("'", "") + "', N'" + jNumeReprez.Replace("'", "") + "', '" + strainas + "', N'" + sStrada.Replace("'", "") + "', '" + sNr.Replace("'", "") + "', '" + sBl.Replace("'", "") + "', '" + sSc.Replace("'", "") + "', '" + sEtj.Replace("'", "") + "', '" + sAp.Replace("'", "") + "', N'" + sJudet.Replace("'", "") + "', N'" + sLocalitate.Replace("'", "") + "', N'" + Membru.Replace("'", "") + "', '" + cnpMembru.Replace("'", "") + "', N'" + mentiuneText.Replace("'", "") + "', '" + codCapitol.Replace("'", "") + "', '" + an1 + "', '" + col1_1.ToString().Replace(",", ".") + "', '" + col1_2.ToString().Replace(",", ".") + "', '" + col1_3.ToString().Replace(",", ".") + "', '" + col1_4.ToString().Replace(",", ".") + "', '" + col1_5.ToString().Replace(",", ".") + "', '" + col1_6.ToString().Replace(",", ".") + "', '" + col1_7.ToString().Replace(",", ".") + "', '" + col1_8.ToString().Replace(",", ".") + "', '" + an2 + "', '" + col2_1.ToString().Replace(",", ".") + "', '" + col2_2.ToString().Replace(",", ".") + "', '" + col2_3.ToString().Replace(",", ".") + "', '" + col2_4.ToString().Replace(",", ".") + "', '" + col2_5.ToString().Replace(",", ".") + "', '" + col2_6.ToString().Replace(",", ".") + "', '" + col2_7.ToString().Replace(",", ".") + "', '" + col2_8.ToString().Replace(",", ".") + "', '" + an3 + "', '" + col3_1.ToString().Replace(",", ".") + "', '" + col3_2.ToString().Replace(",", ".") + "', '" + col3_3.ToString().Replace(",", ".") + "', '" + col3_4.ToString().Replace(",", ".") + "', '" + col3_5.ToString().Replace(",", ".") + "', '" + col3_6.ToString().Replace(",", ".") + "', '" + col3_7.ToString().Replace(",", ".") + "', '" + col3_8.ToString().Replace(",", ".") + "', '" + an4 + "', '" + col4_1.ToString().Replace(",", ".") + "', '" + col4_2.ToString().Replace(",", ".") + "', '" + col4_3.ToString().Replace(",", ".") + "', '" + col4_4.ToString().Replace(",", ".") + "', '" + col4_5.ToString().Replace(",", ".") + "', '" + col4_6.ToString().Replace(",", ".") + "', '" + col4_7.ToString().Replace(",", ".") + "', '" + col4_8.ToString().Replace(",", ".") + "', '" + an5 + "', '" + col5_1.ToString().Replace(",", ".") + "', '" + col5_2.ToString().Replace(",", ".") + "', '" + col5_3.ToString().Replace(",", ".") + "', '" + col5_4.ToString().Replace(",", ".") + "', '" + col5_5.ToString().Replace(",", ".") + "', '" + col5_6.ToString().Replace(",", ".") + "', '" + col5_7.ToString().Replace(",", ".") + "', '" + col5_8.ToString().Replace(",", ".") + "', N'" + camp1.Replace("'", "") + "', N'" + camp2.Replace("'", "") + "', N'" + camp3.Replace("'", "") + "', N'" + camp4.Replace("'", "") + "', N'" + camp5.Replace("'", "") + "', N'" + camp6.Replace("'", "") + "', N'" + camp7.Replace("'", "") + "', N'" + camp8.Replace("'", "") + "', N'" + camp9.Replace("'", "") + "', N'" + camp10.Replace("'", "") + "', N'" + denumire1.Replace("'", "") + "', N'" + denumire2.Replace("'", "") + "', N'" + denumire3.Replace("'", "") + "', N'" + denumire4.Replace("'", "") + "', N'" + denumire5.Replace("'", "") + "', '" + volumInt + "', '" + nrPozitieInt + "'); ";
        return vInterogare;
    }
    protected void btTiparire_Click(object sender, EventArgs e)
    {
        if (tbVolum.Text == "")
        {
            valCustom.IsValid = false;
            valCustom.ErrorMessage = "Câmpul Volum este obligatoriu! ";
            return;
        }
        Thread.CurrentThread.CurrentCulture = new CultureInfo("ro-RO");
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd1 = new SqlCommand();
        vCmd1.Connection = vCon;
        vCmd1.CommandTimeout = 0;
        /**/
        // golire tabela temporara
        vCmd1.CommandText = "delete from rapCapitoleRegistru where utilizatorId='" + Session["SESutilizatorId"].ToString() + "'";
        vCmd1.ExecuteNonQuery();

        // fac lista de gospodarii pentru care generez raportul
        List<string> vListaGospodarii = new List<string>();
        if (tbVolum.Text == "%")
        {
            vCmd1.CommandText = "select gospodarieId from gospodarii where unitateId='" + Session["SESunitateId"].ToString() + "' and an='" + Session["SESan"].ToString() + "' order by volum, nrPozitie";
        }
        else
        {
            vCmd1.CommandText = "select gospodarieId from gospodarii where unitateId='" + Session["SESunitateId"].ToString() + "' and an='" + Session["SESan"].ToString() + "' and volum='" + tbVolum.Text + "'";
        }
        SqlDataReader vTabel = vCmd1.ExecuteReader();
        while (vTabel.Read())
            vListaGospodarii.Add(vTabel["gospodarieId"].ToString());
        vTabel.Close();

        // golesc calcule pe capitole
        clsTiparireCapitole.StergeRapoarteVechi(Session["SESutilizatorId"].ToString(), Convert.ToInt16(Session["SESan"]));
        // calculez rapoartele pentru fiecare gospodarie
        foreach (string a in vListaGospodarii)
            calculCapitole(a);

        if (tbVolum.Text == "%")
        {
            vCmd1.CommandText = "SELECT membri.unitateId, membri.gospodarieId, membri.an, coalesce(membri.nume, '') as nume, membri.codRand, coalesce(membri.cnp, '') as cnp, coalesce(membri.codSex,1) as codSex, coalesce(membri.codRudenie,5) as codRudenie, coalesce(membri.denumireRudenie,'') as denumireRudenie, coalesce(membri.dataNasterii,'01.01.1900') as dataNasterii, coalesce(membri.mentiuni,'') as mentiuni, membri.capitolId, gospodarii.volumInt, gospodarii.nrPozitieInt,gospodarii.volum, gospodarii.nrPozitie FROM membri INNER JOIN gospodarii ON membri.gospodarieId = gospodarii.gospodarieId WHERE (membri.an = '" + Session["SESan"].ToString() + "') AND (membri.unitateId = '" + Session["SESunitateId"].ToString() + "') ";
        }
        else
        {
            // membri tipRap=1
            vCmd1.CommandText = "SELECT membri.unitateId, membri.gospodarieId, membri.an, coalesce(membri.nume, '') as nume, membri.codRand, coalesce(membri.cnp, '') as cnp, coalesce(membri.codSex,1) as codSex, coalesce(membri.codRudenie,5) as codRudenie, coalesce(membri.denumireRudenie,'') as denumireRudenie, coalesce(membri.dataNasterii,'01.01.1900') as dataNasterii, coalesce(membri.mentiuni,'') as mentiuni, membri.capitolId, gospodarii.volumInt, gospodarii.nrPozitieInt,gospodarii.volum, gospodarii.nrPozitie FROM membri INNER JOIN gospodarii ON membri.gospodarieId = gospodarii.gospodarieId WHERE (membri.an = '" + Session["SESan"].ToString() + "') AND (membri.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (gospodarii.volum = '" + tbVolum.Text + "')";
        }
        vTabel = vCmd1.ExecuteReader();
        string vInterogare = "";
        while (vTabel.Read())
        {
            vInterogare += pregatesteInsert(Convert.ToInt64(Session["SESutilizatorId"]), 1, Convert.ToInt64(vTabel["capitolId"].ToString()), Convert.ToInt64(vTabel["gospodarieId"].ToString()), Convert.ToInt32(Session["SESan"]), vTabel["nume"].ToString(), Convert.ToInt32(vTabel["codRand"].ToString()), vTabel["cnp"].ToString(), Convert.ToInt32(vTabel["codSex"].ToString()), Convert.ToInt32(vTabel["codRudenie"].ToString()), vTabel["denumireRudenie"].ToString(), Convert.ToDateTime(vTabel["dataNasterii"].ToString()), vTabel["mentiuni"].ToString(), vTabel["volum"].ToString(), vTabel["nrPozitie"].ToString(), "", 1, "", "", 0, "", "", "", "", "", "", "", "", false, "", "", "", "", false, "", "", "", "", "", "", "", "", "", "", "", "", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", Convert.ToInt32(vTabel["volumInt"].ToString()), Convert.ToInt32(vTabel["nrPozitieInt"].ToString()));
        }
        vTabel.Close();
        if (vInterogare != "")
        {
            vCmd1.CommandText = vInterogare;
            vCmd1.ExecuteNonQuery();
            vInterogare = "";
        }

        if (tbVolum.Text == "%")
        {
            vCmd1.CommandText = "SELECT gospodarieId, unitateId, volum, nrPozitie, codSiruta, tip, COALESCE (strada, '') AS strada, COALESCE (nr, '') AS nr, nrInt, COALESCE (bl, '') AS bl, COALESCE (sc, '') AS sc, COALESCE (et, '') AS et, COALESCE (ap, '') AS ap, codExploatatie, codUnic, judet, localitate, persJuridica, jUnitate, jSubunitate, jCodFiscal, jNumeReprez, strainas, sStrada, sNr, sBl, sSc, sEtj, sAp, sJudet, sLocalitate, (SELECT TOP (1) nume FROM membri WHERE (gospodarieId = gospodarii.gospodarieId) ORDER BY codRudenie) AS Membru, (SELECT  TOP (1) cnp FROM membri AS membri_1 WHERE (gospodarieId = gospodarii.gospodarieId) ORDER BY codRudenie) AS cnpMembru, volumInt, nrPozitieInt FROM gospodarii WHERE (unitateId = '" + Session["SESunitateId"].ToString() + "') AND (an = '" + Session["SESan"].ToString() + "') ";
        }
        else
        {
            // gospodarii tipRap=2
            vCmd1.CommandText = "SELECT gospodarieId, unitateId, volum, nrPozitie, codSiruta, tip, COALESCE (strada, '') AS strada, COALESCE (nr, '') AS nr, nrInt, COALESCE (bl, '') AS bl, COALESCE (sc, '') AS sc, COALESCE (et, '') AS et, COALESCE (ap, '') AS ap, codExploatatie, codUnic, judet, localitate, persJuridica, jUnitate, jSubunitate, jCodFiscal, jNumeReprez, strainas, sStrada, sNr, sBl, sSc, sEtj, sAp, sJudet, sLocalitate, (SELECT TOP (1) nume FROM membri WHERE (gospodarieId = gospodarii.gospodarieId) ORDER BY codRudenie) AS Membru, (SELECT  TOP (1) cnp FROM membri AS membri_1 WHERE (gospodarieId = gospodarii.gospodarieId) ORDER BY codRudenie) AS cnpMembru, volumInt, nrPozitieInt FROM gospodarii WHERE (unitateId = '" + Session["SESunitateId"].ToString() + "') AND (an = '" + Session["SESan"].ToString() + "') AND (volum = '" + tbVolum.Text + "')";
        }
        vTabel = vCmd1.ExecuteReader();
        while (vTabel.Read())
        {
            vInterogare += pregatesteInsert(Convert.ToInt64(Session["SESutilizatorId"]), 2, 0, Convert.ToInt64(vTabel["gospodarieId"].ToString()), Convert.ToInt32(Session["SESan"]), vTabel["Membru"].ToString(), 0, vTabel["cnpMembru"].ToString(), 0, 0, "", DateTime.Now.Date, "", vTabel["volum"].ToString(), vTabel["nrPozitie"].ToString(), vTabel["codSiruta"].ToString(), Convert.ToInt32(vTabel["tip"].ToString()), vTabel["strada"].ToString(), vTabel["nr"].ToString(), Convert.ToInt32(vTabel["nrInt"].ToString()), vTabel["bl"].ToString(), vTabel["sc"].ToString(), vTabel["et"].ToString(), vTabel["ap"].ToString(), vTabel["codExploatatie"].ToString(), vTabel["codUnic"].ToString(), vTabel["judet"].ToString(), vTabel["localitate"].ToString(), Convert.ToBoolean(vTabel["persJuridica"].ToString()), vTabel["jUnitate"].ToString(), vTabel["jSubunitate"].ToString(), vTabel["jCodFiscal"].ToString(), vTabel["jNumeReprez"].ToString(), Convert.ToBoolean(vTabel["strainas"].ToString()), vTabel["sStrada"].ToString(), vTabel["sNr"].ToString(), vTabel["sBl"].ToString(), vTabel["sSc"].ToString(), vTabel["sEtj"].ToString(), vTabel["sAp"].ToString(), vTabel["sJudet"].ToString(), vTabel["sLocalitate"].ToString(), vTabel["Membru"].ToString(), vTabel["cnpMembru"].ToString(), "", "", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", Convert.ToInt32(vTabel["volumInt"].ToString()), Convert.ToInt32(vTabel["nrPozitieInt"].ToString()));
        }
        vTabel.Close();
        if (vInterogare != "")
        {
            vCmd1.CommandText = vInterogare;
            vCmd1.ExecuteNonQuery();
            vInterogare = "";
        }

        // mentiuni tipRap=3
        if (tbVolum.Text == "%")
        {
            vCmd1.CommandText = "SELECT mentiuni.mentiuneText, gospodarii.gospodarieId, gospodarii.volumInt, gospodarii.nrPozitieInt FROM mentiuni INNER JOIN gospodarii ON mentiuni.gospodarieId = gospodarii.gospodarieId WHERE (gospodarii.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (gospodarii.an = '" + Session["SESan"].ToString() + "') ";
        }
        else
        {
            vCmd1.CommandText = "SELECT mentiuni.mentiuneText, gospodarii.gospodarieId, gospodarii.volumInt, gospodarii.nrPozitieInt FROM mentiuni INNER JOIN gospodarii ON mentiuni.gospodarieId = gospodarii.gospodarieId WHERE (gospodarii.unitateId = '" + Session["SESunitateId"].ToString() + "') AND (gospodarii.an = '" + Session["SESan"].ToString() + "') AND (gospodarii.volum = '" + tbVolum.Text + "')";
        }
        vTabel = vCmd1.ExecuteReader();
        while (vTabel.Read())
        {
            vInterogare += pregatesteInsert(Convert.ToInt64(Session["SESutilizatorId"]), 3, 0, Convert.ToInt64(vTabel["gospodarieId"].ToString()), Convert.ToInt32(Session["SESan"]), "", 0, "", 0, 0, "", DateTime.Now.Date, "", "", "", "", 0, "", "", 0, "", "", "", "", "", "", "", "", false, "", "", "", "", false, "", "", "", "", "", "", "", "", "", "", vTabel["mentiuneText"].ToString(), "", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", Convert.ToInt32(vTabel["volumInt"].ToString()), Convert.ToInt32(vTabel["nrPozitieInt"].ToString()));
        }
        vTabel.Close();
        if (vInterogare != "")
        {
            vCmd1.CommandText = vInterogare;
            vCmd1.ExecuteNonQuery();
            vInterogare = "";
        }

        // capitole
        vCmd1.CommandText = "SELECT rapCapitole.utilizatorId, rapCapitole.capitol, rapCapitole.codRand, rapCapitole.denumire1, rapCapitole.denumire2, rapCapitole.denumire3, rapCapitole.denumire4, rapCapitole.denumire5, coalesce(rapCapitole.an1,0) as an1, coalesce(rapCapitole.col1_1,0) as col1_1, coalesce(rapCapitole.col1_2,0) as col1_2, coalesce(rapCapitole.col1_3,0) as col1_3, coalesce(rapCapitole.col1_4,0) as col1_4, coalesce(rapCapitole.col1_5,0) as col1_5, coalesce(rapCapitole.col1_6,0) as col1_6, coalesce(rapCapitole.col1_7,0) as col1_7, coalesce(rapCapitole.col1_8,0) as col1_8, coalesce(rapCapitole.an2,0) as an2, coalesce(rapCapitole.col2_1,0) as col2_1, coalesce(rapCapitole.col2_2,0) as col2_2, coalesce(rapCapitole.col2_3,0) as col2_3, coalesce(rapCapitole.col2_4,0) as col2_4, coalesce(rapCapitole.col2_5,0) as col2_5, coalesce(rapCapitole.col2_6,0) as col2_6, coalesce(rapCapitole.col2_7,0) as col2_7, coalesce(rapCapitole.col2_8,0) as col2_8, coalesce(rapCapitole.an3,0) as an3, coalesce(rapCapitole.col3_1,0) as col3_1, coalesce(rapCapitole.col3_2,0) as col3_2, coalesce(rapCapitole.col3_3,0) as col3_3, coalesce(rapCapitole.col3_4,0) as col3_4, coalesce(rapCapitole.col3_5,0) as col3_5, coalesce(rapCapitole.col3_6,0) as col3_6, coalesce(rapCapitole.col3_7,0) as col3_7, coalesce(rapCapitole.col3_8,0) as col3_8, coalesce(rapCapitole.an4,0) as an4, coalesce(rapCapitole.col4_1,0) as col4_1, coalesce(rapCapitole.col4_2,0) as col4_2, coalesce(rapCapitole.col4_3,0) as col4_3, coalesce(rapCapitole.col4_4,0) as col4_4, coalesce(rapCapitole.col4_5,0) as col4_5, coalesce(rapCapitole.col4_6,0) as col4_6, coalesce(rapCapitole.col4_7,0) as col4_7, coalesce(rapCapitole.col4_8,0) as col4_8, coalesce(rapCapitole.an5,0) as an5, coalesce(rapCapitole.col5_1,0) as col5_1, coalesce(rapCapitole.col5_2,0) as col5_2, coalesce(rapCapitole.col5_3,0) as col5_3, coalesce(rapCapitole.col5_4,0) as col5_4, coalesce(rapCapitole.col5_5,0) as col5_5, coalesce(rapCapitole.col5_6,0) as col5_6, coalesce(rapCapitole.col5_7,0) as col5_7, coalesce(rapCapitole.col5_8,0) as col5_8, rapCapitole.camp1, rapCapitole.camp2, rapCapitole.camp3, rapCapitole.camp4, rapCapitole.camp5, rapCapitole.camp6, rapCapitole.camp7, rapCapitole.camp8, rapCapitole.camp9, rapCapitole.camp10, rapCapitole.gospodarieId, gospodarii.volumInt, gospodarii.nrPozitieInt FROM rapCapitole INNER JOIN gospodarii ON rapCapitole.gospodarieId = gospodarii.gospodarieId WHERE (rapCapitole.utilizatorId = '" + Session["SESutilizatorId"].ToString() + "')";
        vTabel = vCmd1.ExecuteReader();
        while (vTabel.Read())
        {
            int vTip = 0;
            switch (vTabel["capitol"].ToString())
            {
                case "2b": vTip = 4; break;
                case "2c": vTip = 5; break;
                case "2a": vTip = 6; break;
                case "3": vTip = 7; break;
                case "4a": vTip = 8; break;
                case "4b": vTip = 9; break;
                case "4c": vTip = 10; break;
                case "5a": vTip = 11; break;
                case "5b": vTip = 12; break;
                case "5c": vTip = 13; break;
                case "5d": vTip = 14; break;
                case "6": vTip = 15; break;
                case "7": vTip = 16; break;
                case "8": vTip = 17; break;
                case "9": vTip = 18; break;
                case "10a": vTip = 19; break;
                case "10b": vTip = 20; break;
                case "11": vTip = 21; break;
            }
            vInterogare += pregatesteInsert(Convert.ToInt64(Session["SESutilizatorId"].ToString()), vTip, 0, Convert.ToInt64(vTabel["gospodarieId"].ToString()), Convert.ToInt32(Session["SESan"].ToString()), "", Convert.ToInt32(vTabel["codRand"].ToString()), "", 0, 0, "", DateTime.Now.Date, "", "", "", "", 0, "", "", 0, "", "", "", "", "", "", "", "", false, "", "", "", "", false, "", "", "", "", "", "", "", "", "", "", "", vTabel["capitol"].ToString(), Convert.ToInt32(vTabel["an1"].ToString()), Convert.ToDecimal(vTabel["col1_1"].ToString()), Convert.ToDecimal(vTabel["col1_2"].ToString()), Convert.ToDecimal(vTabel["col1_3"].ToString()), Convert.ToDecimal(vTabel["col1_4"].ToString()), Convert.ToDecimal(vTabel["col1_5"].ToString()), Convert.ToDecimal(vTabel["col1_6"].ToString()), Convert.ToDecimal(vTabel["col1_7"].ToString()), Convert.ToDecimal(vTabel["col1_8"].ToString()), Convert.ToInt32(vTabel["an2"].ToString()), Convert.ToDecimal(vTabel["col2_1"].ToString()), Convert.ToDecimal(vTabel["col2_2"].ToString()), Convert.ToDecimal(vTabel["col2_3"].ToString()), Convert.ToDecimal(vTabel["col2_4"].ToString()), Convert.ToDecimal(vTabel["col2_5"].ToString()), Convert.ToDecimal(vTabel["col2_6"].ToString()), Convert.ToDecimal(vTabel["col2_7"].ToString()), Convert.ToDecimal(vTabel["col2_8"].ToString()), Convert.ToInt32(vTabel["an3"].ToString()), Convert.ToDecimal(vTabel["col3_1"].ToString()), Convert.ToDecimal(vTabel["col3_2"].ToString()), Convert.ToDecimal(vTabel["col3_3"].ToString()), Convert.ToDecimal(vTabel["col3_4"].ToString()), Convert.ToDecimal(vTabel["col3_5"].ToString()), Convert.ToDecimal(vTabel["col3_6"].ToString()), Convert.ToDecimal(vTabel["col3_7"].ToString()), Convert.ToDecimal(vTabel["col3_8"].ToString()), Convert.ToInt32(vTabel["an4"].ToString()), Convert.ToDecimal(vTabel["col4_1"].ToString()), Convert.ToDecimal(vTabel["col4_2"].ToString()), Convert.ToDecimal(vTabel["col4_3"].ToString()), Convert.ToDecimal(vTabel["col4_4"].ToString()), Convert.ToDecimal(vTabel["col4_5"].ToString()), Convert.ToDecimal(vTabel["col4_6"].ToString()), Convert.ToDecimal(vTabel["col4_7"].ToString()), Convert.ToDecimal(vTabel["col4_8"].ToString()), Convert.ToInt32(vTabel["an5"].ToString()), Convert.ToDecimal(vTabel["col5_1"].ToString()), Convert.ToDecimal(vTabel["col5_2"].ToString()), Convert.ToDecimal(vTabel["col5_3"].ToString()), Convert.ToDecimal(vTabel["col5_4"].ToString()), Convert.ToDecimal(vTabel["col5_5"].ToString()), Convert.ToDecimal(vTabel["col5_6"].ToString()), Convert.ToDecimal(vTabel["col5_7"].ToString()), Convert.ToDecimal(vTabel["col5_8"].ToString()), vTabel["camp1"].ToString(), vTabel["camp2"].ToString(), vTabel["camp3"].ToString(), vTabel["camp4"].ToString(), vTabel["camp5"].ToString(), vTabel["camp6"].ToString(), vTabel["camp7"].ToString(), vTabel["camp8"].ToString(), vTabel["camp9"].ToString(), vTabel["camp10"].ToString(), vTabel["denumire1"].ToString(), vTabel["denumire2"].ToString(), vTabel["denumire3"].ToString(), vTabel["denumire4"].ToString(), vTabel["denumire5"].ToString(), Convert.ToInt32(vTabel["volumInt"].ToString()), Convert.ToInt32(vTabel["nrPozitieInt"].ToString()));
        }
        vTabel.Close();
        if (vInterogare != "")
        {
            vCmd1.CommandText = vInterogare;
            vCmd1.ExecuteNonQuery();
            vInterogare = "";
        }

        ManipuleazaBD.InchideConexiune(vCon);

        Response.Redirect("~/printCapitoleToateVolum.aspx");
    }
}