﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Functii de stabilire a focusului pe componente
/// Creata la:                  09.02.2011
/// Autor:                      Laza Tudor Mihai
/// Ultima                      actualizare: 09.02.2011
/// Autor:                      Laza Tudor Mihai
/// </summary>
public class ControlTabIndex
{
	public ControlTabIndex()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    // setez focus pe WebControl-ul
    public static void SetTabIndex(Page pPage, GridView pGridView, Int16 pIndexNou)
    {
        if (pPage.IsPostBack)
        {
            WebControl wcICausedPostBack = (WebControl)GetControlThatCausedPostBackp(pPage);
            if (wcICausedPostBack != null)
            {
                int vIndx = wcICausedPostBack.TabIndex;
                ContentPlaceHolder ctph = (ContentPlaceHolder)wcICausedPostBack.Page.Master.FindControl("ContentPlaceHolder1");
                var vCtrl = from control in wcICausedPostBack.Parent.Parent.Controls.OfType<WebControl>()
                           where control.TabIndex > vIndx
                           select control;
                Int16 vIndexNou = pIndexNou;
                if (pIndexNou == 0)
                    vIndexNou = Convert.ToInt16(vCtrl.DefaultIfEmpty(wcICausedPostBack).First().TabIndex + 1);
                WebControl vWebControl = null;
                int vOK = 0;
                // parcurg controalele din gridview si imi iau WebControl-ul pe care trebuie pus focus
                for (int i = 0; i < pGridView.Rows.Count; i++)
                {
                    for (int a = 0; a < pGridView.Columns.Count; a++)
                    {   
                        foreach (Control c in pGridView.Rows[i].Cells[a].Controls)
                        {
                            if (c is System.Web.UI.WebControls.TextBox)
                            {
                                WebControl vWctrl = (WebControl)c;
                                if (vWctrl.TabIndex == vIndexNou)
                                {
                                    vWebControl = vWctrl;
                                    vOK = 1;
                                    break;
                                }
                            }
                        }
                        if (vOK == 1)
                            break;
                    }
                    if (vOK == 1)
                        break;
                }
                if (vWebControl != null)
                    vWebControl.Focus();
            }
        }
    }
    // caut WebControl-ul care face postback
    private static Control GetControlThatCausedPostBackp(Page pPage)
    {
        Control control = null;
        string ctrlname = pPage.Request.Params.Get("__EVENTTARGET");
        if (ctrlname != null && ctrlname != string.Empty)
        {
            control = pPage.FindControl(ctrlname);
        }
        else
        {
            foreach (string ctl in pPage.Request.Form)
            {
                Control c = pPage.FindControl(ctl);
                if (c is System.Web.UI.WebControls.TextBox || c is System.Web.UI.WebControls.TextBox)
                {
                    control = c;
                    break;
                }
            }
        }
        return control;

    }
}
