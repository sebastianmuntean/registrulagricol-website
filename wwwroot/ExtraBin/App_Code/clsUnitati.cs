﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for clsUnitati
/// </summary>
public class clsUnitati
{
	public clsUnitati()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public static void AdaugaInCapitoleCentralizate(string vUnitate, string pAn)
    {
        // luam lista de ani din ciclu
        //iclul curent 
        string vListaAniDinCiclu = ManipuleazaBD.fRezultaUnString("SELECT * FROM cicluri WHERE cicluAni like '%" + pAn + "%'", "cicluAni");
        char[] vCar = new char[] { '#' };
        string[] vListaAni = vListaAniDinCiclu.Split(vCar, StringSplitOptions.RemoveEmptyEntries);
        string vInsert = "";
        string vAnul = "2011";
        foreach (string vAn in vListaAni)
        {
            if (vAn == "2010")
                vAnul = "2011";
            else
                vAnul = vAn;
            // din sabloaneCapitole luam fiecare rand si introducem in centralizatoare
            List<string> vCampuri = new List<string> { "capitol", "codRand" };
            List<List<string>> vListaSabloane = ManipuleazaBD.fRezultaListaStringuri("SELECT * from sabloaneCapitole WHERE an = " + vAnul + " ORDER BY capitol, codRand", vCampuri);

            for (int i = 0; i <= 3; i++)
            {
                if (vListaSabloane.Count > 0)
                {
                    foreach (List<string> vSablon in vListaSabloane)
                    {
                        vInsert += "INSERT INTO capitoleCentralizate (unitateId, gospodarieId, an, codCapitol, codRand, col1, col2, col3, col4, col5, col6, col7, col8, gospodarieTip) VALUES ('" + vUnitate + "', '0', '" + vAn + "','" + vSablon[0] + "', '" + vSablon[1] + "', '0', '0', '0', '0', '0', '0', '0', '0', '" + i.ToString() + "'); ";
                    }
                }
                ManipuleazaBD.fManipuleazaBD(vInsert);
                vInsert = "";
            }
        }
    }
}
