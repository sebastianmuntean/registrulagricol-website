﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Security.Cryptography;
using System.Configuration;

/// <summary>
/// Summary description for CodareCookie
/// </summary>
public class CriptareCookie
{
    public CriptareCookie()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public static HttpCookie EncodeCookie(HttpCookie cookie)
    {
        if (cookie == null) return null;

        //get key and algorithm from web.config/appsettings    
        //Comment this section if you do now what this method to be static    
        char[] chars = { ',' };
        string[] splits = ConfigurationManager.AppSettings["CookieSecurityKey"].Split(chars);
        string AlgorithmName = splits[0];
        byte[] Key = new byte[Int32.Parse(splits[1])];// = KEY_64;    
        for (int i = 2; i < Key.Length + 2; i++)
            Key[i - 2] = byte.Parse(splits[i].Trim());

        HttpCookie eCookie = new HttpCookie(cookie.Name);

        for (int i = 0; i < cookie.Values.Count; i++)
        {
            string value = HttpContext.Current.Server.UrlEncode(Encode(cookie.Values[i], Key, AlgorithmName));
            string name = HttpContext.Current.Server.UrlEncode(Encode(cookie.Values.GetKey(i), Key, AlgorithmName));
            eCookie.Values.Set(name, value);
        }
        return eCookie;
    }

    public static HttpCookie DecodeCookie(HttpCookie cookie)
    {
        if (cookie == null) return null;

        //Comment this section if you do now what this method to be static    
        char[] chars = { ',' };
        string[] splits = ConfigurationManager.AppSettings["CookieSecurityKey"].Split(chars);
        string AlgorithmName = splits[0];
        byte[] Key = new byte[Int32.Parse(splits[1])];// = KEY_64;    
        for (int i = 2; i < Key.Length + 2; i++)
            Key[i - 2] = byte.Parse(splits[i].Trim());

        HttpCookie dCookie = new HttpCookie(cookie.Name);

        for (int i = 0; i < cookie.Values.Count; i++)
        {
            string value = Decode(HttpContext.Current.Server.UrlDecode(cookie.Values[i]), Key, AlgorithmName);
            string name = Decode(HttpContext.Current.Server.UrlDecode(cookie.Values.GetKey(i)), Key, AlgorithmName);
            dCookie.Values.Set(name, value);
        }
        return dCookie;
    }
    public static string Encode(string value, Byte[] key, string AlgorithmName)
    {
        // Convert string data to byte array  
        byte[] ClearData = System.Text.Encoding.UTF8.GetBytes(value);

        // Now create the algorithm from the provided name  
        SymmetricAlgorithm Algorithm = SymmetricAlgorithm.Create(AlgorithmName);
        Algorithm.Key = key; //you can apply your own key mechs here  
        MemoryStream Target = new MemoryStream();

        // Generate a random initialization vector (IV), helps to prevent brute-force  
        Algorithm.GenerateIV();
        Target.Write(Algorithm.IV, 0, Algorithm.IV.Length);

        // Encrypt information  
        CryptoStream cs = new CryptoStream(Target, Algorithm.CreateEncryptor(), CryptoStreamMode.Write);
        cs.Write(ClearData, 0, ClearData.Length);
        cs.FlushFinalBlock();

        // Convert the encrypted stream back to string  
        return Convert.ToBase64String(Target.GetBuffer(), 0, (int)Target.Length);
    }

    public static string Decode(string value, Byte[] key, string AlgorithmName)
    {
        // Convert string data to byte array  
        byte[] ClearData = Convert.FromBase64String(value);

        // Create the algorithm  
        SymmetricAlgorithm Algorithm = SymmetricAlgorithm.Create(AlgorithmName);
        Algorithm.Key = key;
        MemoryStream Target = new MemoryStream();

        // Read IV and initialize the algorithm with it  
        int ReadPos = 0;
        byte[] IV = new byte[Algorithm.IV.Length];
        Array.Copy(ClearData, IV, IV.Length);
        Algorithm.IV = IV;
        ReadPos += Algorithm.IV.Length;

        // Decrypt information  
        CryptoStream cs = new CryptoStream(Target, Algorithm.CreateDecryptor(), CryptoStreamMode.Write);
        cs.Write(ClearData, ReadPos, ClearData.Length - ReadPos);
        cs.FlushFinalBlock();

        // Get the bytes from the memory stream and convert them to text  
        return System.Text.Encoding.UTF8.GetString(Target.ToArray());
    }
    public static string GenereazaFursec()
    {
        string vFursec = "";
        vFursec = Guid.NewGuid().ToString();
        return vFursec;
    }
    public static string CripteazaFursec(string pFursec)
    {
        string vFursec = "";
     //   vFursec = pFursec.Insert(27, DateTime.Now.Year.ToString()).Insert(23, DateTime.Now.Month.ToString()).Insert(17, DateTime.Now.Date.Day.ToString()).Insert(7, DateTime.Now.Minute.ToString()).Insert(3, DateTime.Now.Second.ToString());
        return pFursec;
    }
    public static string DeCripteazaFursec(string pFursec)
    {
        string vFursec = "";
      //  vFursec = pFursec.Remove(3, 2).Remove(7, 2).Remove(17, 2).Remove(23, 2).Remove(27, 4);
        return pFursec;
    }

}
