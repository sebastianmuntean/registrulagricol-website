﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;

// <summary>
/// Manipuleaza baza de date
/// Creata la:                  07.02.2011
/// Autor:                      Sebastian Muntean
/// Ultima actualizare:         07.02.2011
/// Autor:                      Sebastian Muntean
/// </summary>
public class clsUtilizatori
{
	public clsUtilizatori()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public static List<string> CitesteDrepturi(int pTipUtilizatorId) 
    {
        List<string> vDrepturi = new List<string> { };
        string vInterogare = "SELECT  paginaId, dreptCitire, dreptModificare, dreptTiparire, dreptCreare, dreptStergere FROM sabloaneDrepturi WHERE tipUtilizatorId = '" + pTipUtilizatorId +"' ORDER BY paginaId";
        string strConn = ConfigurationManager.ConnectionStrings["TNTRegistruAgricolConnectionString"].ConnectionString;
        SqlConnection con = new SqlConnection(strConn);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = vInterogare;
        con.Open();
        SqlDataReader vLinieSabloane = cmd.ExecuteReader();
        int contorX = 0;
        while (vLinieSabloane.Read())
        {
            vDrepturi.Add(vLinieSabloane["paginaId"].ToString() + "," + vLinieSabloane["dreptCitire"].ToString() +","+ vLinieSabloane["dreptCreare"].ToString() +","+ vLinieSabloane["dreptModificare"] + "," + vLinieSabloane["dreptStergere"].ToString() + "," + vLinieSabloane["dreptTiparire"].ToString());
            contorX++;
        }
        return vDrepturi;
    } 
    public static void AdaugaDrepturi(int pTipUtilizator, int pUtilizatorId)
    {
        List<string> vDrepturi = CitesteDrepturi(pTipUtilizator);
        int vContor = 0;
        foreach (string vLinie in vDrepturi)
        {
            string[] vDrepturiImpartite = vDrepturi[vContor].Split(',');
            string vInterogare = @"INSERT INTO drepturiUtilizatori 
                        (utilizatorId, paginaId, dreptCitire, dreptCreare, dreptModificare, dreptStergere, dreptTiparire)
                         VALUES ('" + pUtilizatorId + "','" + vDrepturiImpartite[0]+"','"+vDrepturiImpartite[1]+"','"+vDrepturiImpartite[2]+"','"+vDrepturiImpartite[3]+"','"+vDrepturiImpartite[4]+"','"+vDrepturiImpartite[5]+"')";
            ManipuleazaBD.fManipuleazaBD(vInterogare);
            vContor++;
        }
        

        
    }
    public static void ParolaVerifica(string pParolaNoua, string pParolaConfirm, string pUtilizator, out string pExpresie)
    {
        pExpresie = "";
        // verifica dimensiunea si alte caractere
        // daca parola moua nu corespunde criteriilor
        if (!Valideaza.Parola(pParolaNoua))
        {
            pExpresie = "Parola nouă trebuie să conţină minim 8 caractere, cel puţin o literă mare şi una mică, o cifră şi un caracter special ( @ # $ % ^ & + = ? ! > < . * ~ | ). Mai incercaţi!";
            return;
        }
        // daca nu sunt egale noua = confirm
        if (pParolaNoua != pParolaConfirm)
        {
            pExpresie = "Parola nouă şi confirmarea acesteia nu sunt identice. Mai incercaţi!";
            return;
        }
        string vParolaNoua = Criptare.Encrypt(pParolaNoua);
        string vParolaConfirm = Criptare.Encrypt(pParolaConfirm);
        if (pUtilizator != "")
        {
            // verificam si sa nu mai fie introdusa in ultimele 10 parole
            // luam ultimele 10 parole din utilizatoriParole
            List<string> vListaCampuri = new List<string> { "utilizatorParola", "utilizatorData" };
            List<List<string>> vListaParole = ManipuleazaBD.fRezultaListaStringuri("SELECT TOP(10) * FROM utilizatoriParole WHERE utilizatorId = " + pUtilizator + " ORDER BY utilizatorData DESC", vListaCampuri);
            // daca nu are nicio parola salvata verificam cu cea din utilizatori
            if (vListaParole.Count == 0)
            {
                string vParolaUtilizator = ManipuleazaBD.fRezultaUnString("SELECT * FROM utilizatori WHERE utilizatorId = " + pUtilizator, "utilizatorParola");
                if (vParolaUtilizator == Criptare.Encrypt(pParolaNoua))
                {
                    pExpresie = Resources.Resursa.raParolaFolositaRecent;
                    return;
                }
                // introducem si parola care nu a existat
                ManipuleazaBD.fManipuleazaBD("INSERT INTO utilizatoriParole (utilizatorParola,utilizatorId,utilizatorData) VALUES (N'" + vParolaUtilizator + "', " + pUtilizator + ", CONVERT(datetime, '" + DateTime.Now.ToString() + "', 104))");
            }
            else
            {
                // daca are parole salvate
                foreach (List<string> vParola1 in vListaParole)
                {
                    if (vParola1[0] == Criptare.Encrypt(pParolaNoua))
                    {
                        pExpresie = Resources.Resursa.raParolaFolositaRecent;
                        return;
                    }
                }
            }
        }
    }
}
