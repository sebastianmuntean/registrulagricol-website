﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for clsTiparireCapitole
/// </summary>
public class clsTiparireCapitole
{
    public clsTiparireCapitole()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public static void UmpleRapCapitole(string pUtilizatorId, string pSesAn, string pGospodarieId, string pCapitol)
    {
        // stergem rapoartele vechi ale utilizatorului
        ManipuleazaBD.fManipuleazaBD("DELETE FROM rapCapitole WHERE utilizatorId = '" + pUtilizatorId + "'");

        // umplem tabela de rapoarte
        // scoatem lista anilor din ciclul curent
        string vCiclu = ManipuleazaBD.fRezultaUnString("SELECT * FROM cicluri WHERE cicluAni LIKE '%" + pSesAn + "%'", "cicluAni");
        char[] vDelimitator = { '#' };
        string[] vAniDinCiclu = vCiclu.Split(vDelimitator, StringSplitOptions.RemoveEmptyEntries);
        List<string> vListaCampuri = new List<string> { "codCapitol", "capitoleCodRand", "denumire1", "denumire2", "denumire3", "denumire4", "denumire5" };
        string vGospodarieIdInitial = ManipuleazaBD.fRezultaUnString("SELECT * FROM gospodarii WHERE gospodarieId = '" + pGospodarieId + "'", "gospodarieIdInitial");
        // luam valorile generale, indiferent de ani
        List<List<string>> vValoriGenerale = new List<List<string>> { };
        vValoriGenerale = ManipuleazaBD.fRezultaListaStringuri(" SELECT  capitole.codCapitol, capitole.codRand AS capitoleCodRand,  sabloaneCapitole.denumire1, sabloaneCapitole.denumire2, sabloaneCapitole.denumire3, sabloaneCapitole.denumire4, sabloaneCapitole.denumire5 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId  WHERE     (gospodarii.gospodarieIdInitial = '" + vGospodarieIdInitial + "') AND (capitole.an = '" + pSesAn + "')  AND (sabloaneCapitole.an = '" + pSesAn + "') AND (capitole.codCapitol = '" + pCapitol + "') ORDER BY capitoleCodRand", vListaCampuri);

        vListaCampuri = new List<string> { "capitoleAn", "col1", "col2", "col3", "col4", "col5", "col6", "col7", "col8" };
        List<List<List<string>>> vListaValori = new List<List<List<string>>> { };
        int vCodRand = 0;
        string vComanda = "";
        foreach (string vAn in vAniDinCiclu)
        {
            List<List<string>> vValori = new List<List<string>> { };
            vValori = ManipuleazaBD.fRezultaListaStringuri(" SELECT  capitole.an AS capitoleAn, capitole.codCapitol, capitole.codRand AS capitoleCodRand, CONVERT(decimal(18, 2), capitole.col1) AS col1, CONVERT(decimal(18, 2), capitole.col2) AS col2, CONVERT(decimal(18, 2), capitole.col3) AS col3, CONVERT(decimal(18, 2), capitole.col4) AS col4, CONVERT(decimal(18, 2), capitole.col5) AS col5, CONVERT(decimal(18, 2), capitole.col6) AS col6, CONVERT(decimal(18, 2), capitole.col7) AS col7, CONVERT(decimal(18, 2), capitole.col8) AS col8 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId  WHERE     (gospodarii.gospodarieIdInitial = '" + vGospodarieIdInitial + "') AND (capitole.an = '" + vAn + "')  AND (sabloaneCapitole.an = '" + vAn + @"') AND (capitole.codCapitol = '" + pCapitol + "') ORDER BY capitoleCodRand", vListaCampuri);
            if (vValori.Count != 0)
            { vListaValori.Add(vValori); }
            else
            {
                vValori = new List<List<string>> { };
                for (int i = 0; i < vValoriGenerale.Count; i++)
                {
                    List<string> vListaGoala = new List<string> { vAn, "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0" };
                    vValori.Add(vListaGoala);
                }
                vListaValori.Add(vValori);
            }

        }
        // construim comanda pentru fiecare rand
        foreach (List<string> vRandValori in vValoriGenerale)
        {
            vComanda += @"INSERT INTO [rapCapitole] ([utilizatorId], [capitol], [codRand], [denumire1], [denumire2], [denumire3], [denumire4], [denumire5], [col1_1], [col1_2], [col1_3], [col1_4], [col1_5], [col1_6], [col1_7], [col1_8], [an1],[col2_1], [col2_2], [col2_3], [col2_4], [col2_5], [col2_6], [col2_7], [col2_8], [an2],[col3_1], [col3_2], [col3_3], [col3_4], [col3_5], [col3_6], [col3_7], [col3_8], [an3],[col4_1], [col4_2], [col4_3], [col4_4], [col4_5], [col4_6], [col4_7], [col4_8], [an4],[col5_1], [col5_2], [col5_3], [col5_4], [col5_5], [col5_6], [col5_7], [col5_8], [an5]) VALUES ('" +
                Convert.ToInt32(pUtilizatorId) + "','" +
                vValoriGenerale[vCodRand][0] + "','" +
                vValoriGenerale[vCodRand][1] + "','" +
                vValoriGenerale[vCodRand][2] + "','" +
                vValoriGenerale[vCodRand][3] + "','" +
                vValoriGenerale[vCodRand][4] + "','" +
                vValoriGenerale[vCodRand][5] + "','" +
                vValoriGenerale[vCodRand][6] + "','" +
                vListaValori[0][vCodRand][1].Replace(',', '.') + "', '" +
                vListaValori[0][vCodRand][2].Replace(',', '.') + "', '" +
                vListaValori[0][vCodRand][3].Replace(',', '.') + "', '" +
                vListaValori[0][vCodRand][4].Replace(',', '.') + "', '" +
                vListaValori[0][vCodRand][5].Replace(',', '.') + "', '" +
                vListaValori[0][vCodRand][6].Replace(',', '.') + "', '" +
                vListaValori[0][vCodRand][7].Replace(',', '.') + "', '" +
                vListaValori[0][vCodRand][8].Replace(',', '.') + "', '" +
                vListaValori[0][vCodRand][0].Replace(',', '.') + "', '" +
                vListaValori[1][vCodRand][1].Replace(',', '.') + "', '" +
                vListaValori[1][vCodRand][2].Replace(',', '.') + "', '" +
                vListaValori[1][vCodRand][3].Replace(',', '.') + "', '" +
                vListaValori[1][vCodRand][4].Replace(',', '.') + "', '" +
                vListaValori[1][vCodRand][5].Replace(',', '.') + "', '" +
                vListaValori[1][vCodRand][6].Replace(',', '.') + "', '" +
                vListaValori[1][vCodRand][7].Replace(',', '.') + "', '" +
                vListaValori[1][vCodRand][8].Replace(',', '.') + "', '" +
                vListaValori[1][vCodRand][0].Replace(',', '.') + "', '" +
                vListaValori[2][vCodRand][1].Replace(',', '.') + "', '" +
                vListaValori[2][vCodRand][2].Replace(',', '.') + "', '" +
                vListaValori[2][vCodRand][3].Replace(',', '.') + "', '" +
                vListaValori[2][vCodRand][4].Replace(',', '.') + "', '" +
                vListaValori[2][vCodRand][5].Replace(',', '.') + "', '" +
                vListaValori[2][vCodRand][6].Replace(',', '.') + "', '" +
                vListaValori[2][vCodRand][7].Replace(',', '.') + "', '" +
                vListaValori[2][vCodRand][8].Replace(',', '.') + "', '" +
                vListaValori[2][vCodRand][0].Replace(',', '.') + "', '" +
                vListaValori[3][vCodRand][1].Replace(',', '.') + "', '" +
                vListaValori[3][vCodRand][2].Replace(',', '.') + "', '" +
                vListaValori[3][vCodRand][3].Replace(',', '.') + "', '" +
                vListaValori[3][vCodRand][4].Replace(',', '.') + "', '" +
                vListaValori[3][vCodRand][5].Replace(',', '.') + "', '" +
                vListaValori[3][vCodRand][6].Replace(',', '.') + "', '" +
                vListaValori[3][vCodRand][7].Replace(',', '.') + "', '" +
                vListaValori[3][vCodRand][8].Replace(',', '.') + "', '" +
                vListaValori[3][vCodRand][0].Replace(',', '.') + "', '" +
                vListaValori[4][vCodRand][1].Replace(',', '.') + "', '" +
                vListaValori[4][vCodRand][2].Replace(',', '.') + "', '" +
                vListaValori[4][vCodRand][3].Replace(',', '.') + "', '" +
                vListaValori[4][vCodRand][4].Replace(',', '.') + "', '" +
                vListaValori[4][vCodRand][5].Replace(',', '.') + "', '" +
                vListaValori[4][vCodRand][6].Replace(',', '.') + "', '" +
                vListaValori[4][vCodRand][7].Replace(',', '.') + "', '" +
                vListaValori[4][vCodRand][8].Replace(',', '.') + "', '" +
                vListaValori[4][vCodRand][0].Replace(',', '.') + "'); ";
            vCodRand++;
        }

        ManipuleazaBD.fManipuleazaBD(vComanda);
    }
    public static void StergeRapoarteVechi(string pUtilizatorId)
    {
        // stergem rapoartele vechi ale utilizatorului
        ManipuleazaBD.fManipuleazaBD("DELETE FROM rapCapitole WHERE utilizatorId = '" + pUtilizatorId + "'");
    }
    public static void UmpleRapCapitoleFaraStergere(string pUtilizatorId, string pSesAn, string pGospodarieId, string pCapitol)
    {
        // umplem tabela de rapoarte
        // scoatem lista anilor din ciclul curent
        string vCiclu = ManipuleazaBD.fRezultaUnString("SELECT * FROM cicluri WHERE cicluAni LIKE '%" + pSesAn + "%'", "cicluAni");
        char[] vDelimitator = { '#' };
        string[] vAniDinCiclu = vCiclu.Split(vDelimitator, StringSplitOptions.RemoveEmptyEntries);
        List<string> vListaCampuri = new List<string> { "codCapitol", "capitoleCodRand", "denumire1", "denumire2", "denumire3", "denumire4", "denumire5" };
        string vGospodarieIdInitial = ManipuleazaBD.fRezultaUnString("SELECT * FROM gospodarii WHERE gospodarieId = '" + pGospodarieId + "'", "gospodarieIdInitial");
        // luam valorile generale, indiferent de ani
        List<List<string>> vValoriGenerale = new List<List<string>> { };
        vValoriGenerale = ManipuleazaBD.fRezultaListaStringuri(" SELECT  capitole.codCapitol, capitole.codRand AS capitoleCodRand,  sabloaneCapitole.denumire1, sabloaneCapitole.denumire2, sabloaneCapitole.denumire3, sabloaneCapitole.denumire4, sabloaneCapitole.denumire5 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId  WHERE     (gospodarii.gospodarieIdInitial = '" + vGospodarieIdInitial + "') AND (capitole.an = '" + pSesAn + "')  AND (sabloaneCapitole.an = '" + pSesAn + "') AND (capitole.codCapitol = '" + pCapitol + "') ORDER BY capitoleCodRand", vListaCampuri);

        vListaCampuri = new List<string> { "capitoleAn", "col1", "col2", "col3", "col4", "col5", "col6", "col7", "col8" };
        List<List<List<string>>> vListaValori = new List<List<List<string>>> { };
        int vCodRand = 0;
        string vComanda = "";
        foreach (string vAn in vAniDinCiclu)
        {
            List<List<string>> vValori = new List<List<string>> { };
            vValori = ManipuleazaBD.fRezultaListaStringuri(" SELECT  capitole.an AS capitoleAn, capitole.codCapitol, capitole.codRand AS capitoleCodRand, CONVERT(decimal(18, 2), capitole.col1) AS col1, CONVERT(decimal(18, 2), capitole.col2) AS col2, CONVERT(decimal(18, 2), capitole.col3) AS col3, CONVERT(decimal(18, 2), capitole.col4) AS col4, CONVERT(decimal(18, 2), capitole.col5) AS col5, CONVERT(decimal(18, 2), capitole.col6) AS col6, CONVERT(decimal(18, 2), capitole.col7) AS col7, CONVERT(decimal(18, 2), capitole.col8) AS col8 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId  WHERE     (gospodarii.gospodarieIdInitial = '" + vGospodarieIdInitial + "') AND (capitole.an = '" + vAn + "')  AND (sabloaneCapitole.an = '" + vAn + @"') AND (capitole.codCapitol = '" + pCapitol + "') ORDER BY capitoleCodRand", vListaCampuri);
            if (vValori.Count != 0)
            { vListaValori.Add(vValori); }
            else
            {
                vValori = new List<List<string>> { };
                for (int i = 0; i < vValoriGenerale.Count; i++)
                {
                    List<string> vListaGoala = new List<string> { vAn, "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0" };
                    vValori.Add(vListaGoala);
                }
                vListaValori.Add(vValori);
            }

        }
        // construim comanda pentru fiecare rand
        foreach (List<string> vRandValori in vValoriGenerale)
        {
            vComanda += @"INSERT INTO [rapCapitole] ([utilizatorId], [capitol], [codRand], [denumire1], [denumire2], [denumire3], [denumire4], [denumire5], [col1_1], [col1_2], [col1_3], [col1_4], [col1_5], [col1_6], [col1_7], [col1_8], [an1],[col2_1], [col2_2], [col2_3], [col2_4], [col2_5], [col2_6], [col2_7], [col2_8], [an2],[col3_1], [col3_2], [col3_3], [col3_4], [col3_5], [col3_6], [col3_7], [col3_8], [an3],[col4_1], [col4_2], [col4_3], [col4_4], [col4_5], [col4_6], [col4_7], [col4_8], [an4],[col5_1], [col5_2], [col5_3], [col5_4], [col5_5], [col5_6], [col5_7], [col5_8], [an5], [gospodarieId]) VALUES ('" +
                Convert.ToInt32(pUtilizatorId) + "','" +
                vValoriGenerale[vCodRand][0] + "','" +
                vValoriGenerale[vCodRand][1] + "','" +
                vValoriGenerale[vCodRand][2] + "','" +
                vValoriGenerale[vCodRand][3] + "','" +
                vValoriGenerale[vCodRand][4] + "','" +
                vValoriGenerale[vCodRand][5] + "','" +
                vValoriGenerale[vCodRand][6] + "','" +
                vListaValori[0][vCodRand][1].Replace(',', '.') + "', '" +
                vListaValori[0][vCodRand][2].Replace(',', '.') + "', '" +
                vListaValori[0][vCodRand][3].Replace(',', '.') + "', '" +
                vListaValori[0][vCodRand][4].Replace(',', '.') + "', '" +
                vListaValori[0][vCodRand][5].Replace(',', '.') + "', '" +
                vListaValori[0][vCodRand][6].Replace(',', '.') + "', '" +
                vListaValori[0][vCodRand][7].Replace(',', '.') + "', '" +
                vListaValori[0][vCodRand][8].Replace(',', '.') + "', '" +
                vListaValori[0][vCodRand][0].Replace(',', '.') + "', '" +
                vListaValori[1][vCodRand][1].Replace(',', '.') + "', '" +
                vListaValori[1][vCodRand][2].Replace(',', '.') + "', '" +
                vListaValori[1][vCodRand][3].Replace(',', '.') + "', '" +
                vListaValori[1][vCodRand][4].Replace(',', '.') + "', '" +
                vListaValori[1][vCodRand][5].Replace(',', '.') + "', '" +
                vListaValori[1][vCodRand][6].Replace(',', '.') + "', '" +
                vListaValori[1][vCodRand][7].Replace(',', '.') + "', '" +
                vListaValori[1][vCodRand][8].Replace(',', '.') + "', '" +
                vListaValori[1][vCodRand][0].Replace(',', '.') + "', '" +
                vListaValori[2][vCodRand][1].Replace(',', '.') + "', '" +
                vListaValori[2][vCodRand][2].Replace(',', '.') + "', '" +
                vListaValori[2][vCodRand][3].Replace(',', '.') + "', '" +
                vListaValori[2][vCodRand][4].Replace(',', '.') + "', '" +
                vListaValori[2][vCodRand][5].Replace(',', '.') + "', '" +
                vListaValori[2][vCodRand][6].Replace(',', '.') + "', '" +
                vListaValori[2][vCodRand][7].Replace(',', '.') + "', '" +
                vListaValori[2][vCodRand][8].Replace(',', '.') + "', '" +
                vListaValori[2][vCodRand][0].Replace(',', '.') + "', '" +
                vListaValori[3][vCodRand][1].Replace(',', '.') + "', '" +
                vListaValori[3][vCodRand][2].Replace(',', '.') + "', '" +
                vListaValori[3][vCodRand][3].Replace(',', '.') + "', '" +
                vListaValori[3][vCodRand][4].Replace(',', '.') + "', '" +
                vListaValori[3][vCodRand][5].Replace(',', '.') + "', '" +
                vListaValori[3][vCodRand][6].Replace(',', '.') + "', '" +
                vListaValori[3][vCodRand][7].Replace(',', '.') + "', '" +
                vListaValori[3][vCodRand][8].Replace(',', '.') + "', '" +
                vListaValori[3][vCodRand][0].Replace(',', '.') + "', '" +
                vListaValori[4][vCodRand][1].Replace(',', '.') + "', '" +
                vListaValori[4][vCodRand][2].Replace(',', '.') + "', '" +
                vListaValori[4][vCodRand][3].Replace(',', '.') + "', '" +
                vListaValori[4][vCodRand][4].Replace(',', '.') + "', '" +
                vListaValori[4][vCodRand][5].Replace(',', '.') + "', '" +
                vListaValori[4][vCodRand][6].Replace(',', '.') + "', '" +
                vListaValori[4][vCodRand][7].Replace(',', '.') + "', '" +
                vListaValori[4][vCodRand][8].Replace(',', '.') + "', '" +
                vListaValori[4][vCodRand][0].Replace(',', '.') + "', '" + pGospodarieId + "'); ";
            vCodRand++;
        }

        ManipuleazaBD.fManipuleazaBD(vComanda);
    }
}
