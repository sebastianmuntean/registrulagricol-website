using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for CNP
/// </summary>
public class CNP
{
    public String cod;
	public CNP(String cod_p)
	{
        cod = cod_p;
	}
    public Boolean ImplinesteVarsta(Int32 varsta, Int32 luna, Int32 an)
    {
        try
        {
            Int32 luna2 = GetLuna();
            Int32 an2 = GetAnLung();
            if (luna2 == luna && (an - an2) == varsta)
                return true;
            return false;
        }
        catch
        {
            return false;
        }
    }
    public String IsValid()
    {
        String ret = "";
        Int32 suma=0,cifc=-1,cifc2=-1,rest;
        String cheiet ="279146358279";
        
        if (cod.Length < 13)
            return "CNP prea scurt";
        if (cod.Length > 13)
            return "CNP prea lung";
        if (GetAnScurt() < 0 || GetAnScurt() > 99)
            return "Anul din CNP este incorect";
        if (GetLuna() < 0 || GetLuna() > 12)
            return "Luna din CNP este incroecta";
        if (GetZi() < 0 || GetZi() > 31)
            return "Ziua din CNP este incorecta";
        cifc = Convert.ToInt32(cod[12].ToString());
        for (int i = 0; i < cod.Length-1; i++)
        {
            suma = suma + Convert.ToInt32(cod[i].ToString())*Convert.ToInt32(cheiet[i].ToString());
        }
        rest = suma % 11;
        if (rest < 10)
        {
            cifc2 = rest;
        }
        if (rest == 10)
        {
            cifc2 = 1;
        }
        if (cifc2 != cifc)
        {
            return "CNP invalid";
        }
        return ret;
    }
    public Boolean IsValidB()
    {
        String ret = "";
        Int32 suma = 0, cifc = -1, cifc2 = -1, rest;
        String cheiet = "279146358279";
        if (cod.Length < 13)
            return false;
        if (cod.Length > 13)
            return false;
        if (GetAnScurt() < 0 || GetAnScurt() > 99)
            return false;
        if (GetLuna() < 0 || GetLuna() > 12)
            return false;
        if (GetZi() < 0 || GetZi() > 31)
            return false;
        cifc = Convert.ToInt32(cod[12].ToString());
        for (int i = 0; i < cod.Length - 1; i++)
        {
            suma = suma + Convert.ToInt32(cod[i].ToString()) * Convert.ToInt32(cheiet[i].ToString());
        }
        rest = suma % 11;
        if (rest < 10)
        {
            cifc2 = rest;
        }
        if (rest == 10)
        {
            cifc2 = 1;
        }
        if (cifc2 != cifc)
        {
            return false;
        }
        return true;
    }
    public Int32 GetVarstaAni(DateTime data)
    {
        Int32 ani,luni,zile;
        ani = data.Year - GetDataNasterii().Year;
        luni = data.Month - GetDataNasterii().Month;
        zile = data.Day - GetDataNasterii().Day;
        if (zile < 0)
            luni -= 1;
        while (luni < 0)
        {
            luni += 12;
            ani -=1;
        }
        return ani;
    }
    public Int32 GetVarstaLuni(DateTime data)
    {
        Int32 ani, luni, zile;
        ani = data.Year - GetDataNasterii().Year;
        luni = data.Month - GetDataNasterii().Month;
        zile = data.Day - GetDataNasterii().Day;
        if (zile < 0)
            luni -= 1;
        while (luni < 0)
        {
            luni += 12;
            ani -= 1;
        }
        return luni;
    }
    public Int32 GetVarstaZile(DateTime data)
    {
        Int32 ani, luni, zile;
        ani = data.Year - GetDataNasterii().Year;
        luni = data.Month - GetDataNasterii().Month;
        zile = data.Day - GetDataNasterii().Day;
        if (zile < 0)
            luni -= 1;
        while (luni < 0)
        {
            luni += 12;
            ani -= 1;
        }
        TimeSpan t = data - GetDataNasterii().AddYears(ani).AddMonths(luni);
        zile = (int)Math.Round(t.TotalDays);
        return zile;
    }
    public Int64 GetVarstaOre(DateTime data)
    {
        Int32 ani, luni;
        Int64 zile;
        ani = data.Year - GetDataNasterii().Year;
        luni = data.Month - GetDataNasterii().Month;
        zile = data.Day - GetDataNasterii().Day;
        if (zile < 0)
            luni -= 1;
        while (luni < 0)
        {
            luni += 12;
            ani -= 1;
        }
        TimeSpan t = data - GetDataNasterii().AddYears(ani).AddMonths(luni);
        zile = (Int64)Math.Round(t.TotalHours);
        return zile;
    }
    public Int64 GetVarstaMinute(DateTime data)
    {
        Int32 ani, luni;
        Int64 zile;
        ani = data.Year - GetDataNasterii().Year;
        luni = data.Month - GetDataNasterii().Month;
        zile = data.Day - GetDataNasterii().Day;
        if (zile < 0)
            luni -= 1;
        while (luni < 0)
        {
            luni += 12;
            ani -= 1;
        }
        TimeSpan t = data - GetDataNasterii().AddYears(ani).AddMonths(luni);
        zile = (Int64)Math.Round(t.TotalMinutes);
        return zile;
    }
    public Int32 GetZi()
    {
        // returneaza o zi in caz de succes, -1 in caz de format incorect
        Int32 zi=0;
        try
        {
            zi = Convert.ToInt32(cod[5].ToString());
            zi = zi * 10 + Convert.ToInt32(cod[6].ToString());
            return zi;
        }
        catch
        {
            return -1;
        }
    }
    public Int32 GetLuna()
    {
        // returneaza luna in caz de succes sau -1 in caz de eroare
        Int32 luna = 0;
        try
        {
            luna = Convert.ToInt32(cod[3].ToString());
            luna = luna * 10 + Convert.ToInt32(cod[4].ToString());
            return luna;
        }
        catch
        {
            return -1;
        }

    }
    public Int32 GetAnScurt()
    {
        // returneaza anul in forma scurta in caz de succes, -1 in caz de eroare
        Int32 an = 0;
        try
        {
            an = Convert.ToInt32(cod[1].ToString());
            an = an * 10 + Convert.ToInt32(cod[2].ToString());
            return an;
        }
        catch
        {
            return -1;
        }

    }
    public Int32 GetAnLung()
    {
        Int32 an = 0;
        if (cod[0] == '1' || cod[0] == '2')
            an = 19;
        if (cod[0] == '3' || cod[0] == '4')
            an = 18;
        if (cod[0] == '5' || cod[0] == '6')
            an = 20;
        try
        {
            an = an*10 + Convert.ToInt32(cod[1].ToString());
            an = an * 10 + Convert.ToInt32(cod[2].ToString());
            return an;
        }
        catch
        {
            return -1;
        }
    }
    public Int32 GetSex()
    {
        //Int32 sex = 0;
        try
        {
            // returneaza 1 la masculin si 2 la feminin
            if (cod[0] == '1' || cod[0] == '3' || cod[0] == '5' || cod[0] == '7')
                return 1;
            else if (cod[0] == '2' || cod[0] == '4' || cod[0] == '6' || cod[0] == '8')
                return 2;
            else
                return -1;
        }
        catch
        {
            return -1;
        }
    }
    public String GetSexText()
    {
        //Int32 sex = 0;
        try
        {
            // returneaza 1 la masculin si 2 la feminin
            if (cod[0] == '1' || cod[0] == '3' || cod[0] == '5' || cod[0] == '7')
                return "masculin";
            else if (cod[0] == '2' || cod[0] == '4' || cod[0] == '6' || cod[0] == '8')
                return "feminin";
            else
                return "Nu se poate afla sexul";
        }
        catch
        {
            return "Nu se poate afla sexul.";
        }

    }
    public DateTime GetDataNasterii()
    {
        DateTime d;
        try
        {
            d = Convert.ToDateTime(GetZi().ToString()+"."+GetLuna().ToString()+"."+GetAnLung().ToString());
            return d;
        }
        catch
        {
            d = Convert.ToDateTime("01.01.9999"); 
        }
        return d;
    }



}
