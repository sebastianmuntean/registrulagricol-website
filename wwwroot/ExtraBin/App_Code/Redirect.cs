﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Redirect
/// </summary>
public class Redirect
{
	public static int RedirectUnitatePrincipala(string pSesiuneUnitateId)
	{
        int vRedirect = 0;
        // daca nu e unitate principala nu poate accesa pagina
        string vUnitateIdPrincipala = ManipuleazaBD.fRezultaUnString("SELECT unitatePrincipala FROM unitati WHERE unitateId='" + pSesiuneUnitateId + "'", "unitatePrincipala");
        if (vUnitateIdPrincipala != "1" && vUnitateIdPrincipala != "2")
        { vRedirect = 1; }
        return vRedirect;
	}
    public static int RedirectUnitateTnt(string pSesiuneUnitateId)
    {
        int vRedirect = 0;
        // daca nu e unitate principala nu poate accesa pagina
        string vUnitateTNT = ManipuleazaBD.fRezultaUnString("SELECT unitatePrincipala FROM unitati WHERE unitateId='" + pSesiuneUnitateId + "'", "unitatePrincipala");
        if (vUnitateTNT != "2")
        { vRedirect = 1; }
        return vRedirect;
    }
    public static int RedirectUtilizator(string pSesiuneUtilizatorLogin)
    {
        int vRedirect = 0;
        // daca nu e utilizator admin nu poate accesa pagina
        string vUtilizatorAdmin = ManipuleazaBD.fRezultaUnString("SELECT tipUtilizatorId FROM utilizatori WHERE utilizatorId='" + pSesiuneUtilizatorLogin + "'", "tipUtilizatorId");
        if (vUtilizatorAdmin != "1")
        { vRedirect = 1; }
        return vRedirect;
    }

}
