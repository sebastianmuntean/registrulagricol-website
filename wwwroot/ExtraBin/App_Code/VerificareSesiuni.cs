﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Threading;

/// <summary>
/// Summary description for VerificareSesiuni
/// </summary>
public class VerificareSesiuni : System.Web.UI.Page
{
	public VerificareSesiuni()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public void VerificaSesiuniCookie()
    {
        Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("ro-RO");
        HttpCookie vCookie = HttpContext.Current.Request.Cookies["COOKTNT"];
        vCookie = CriptareCookie.DecodeCookie(vCookie);
        if (Session["SESunitateId"] == null || Session["SESutilizatorId"] == null || Session["SESan"] == null || Session["SESordonareGospodarii"] == null || Session["SESjudetId"] == null || Session["SESmesaj"] == null)
        {
            if (vCookie != null)
            {
                if (vCookie["COOKunitateId"] != null)
                {
                    Session["SESunitateId"] = vCookie["COOKunitateId"].ToString();
                }
                else { HttpContext.Current.Response.Redirect("Login.aspx"); }
                if (vCookie["COOKjudetId"] != null)
                {
                    Session["SESjudetId"] = vCookie["COOKjudetId"].ToString();
                }
                else { HttpContext.Current.Response.Redirect("Login.aspx"); }
                if (vCookie["COOKutilizatorId"] != null)
                {
                    Session["SESutilizatorId"] = vCookie["COOKutilizatorId"].ToString();
                }
                else { HttpContext.Current.Response.Redirect("Login.aspx"); }
                if (vCookie["COOKan"] != null)
                {
                    Session["SESan"] = vCookie["COOKan"].ToString();
                }
                if (vCookie["COOKordonareGospodarii"] != null)
                    Session["SESordonareGospodarii"] = vCookie["COOKordonareGospodarii"].ToString();
                else Session["SESordonareGospodarii"] = "0";
                if (vCookie["COOKmesaj"] != null)
                    Session["SESmesaj"] = vCookie["COOKmesaj"].ToString();
            }
            else { HttpContext.Current.Response.Redirect("Login.aspx"); }
        }
        if (vCookie["COOKrandCurent"] != null)
            Session["SESrandCurent"] = vCookie["COOKrandCurent"].ToString();
        if (vCookie["COOKfVol"] != null)
            Session["SESfVol"] = vCookie["COOKfVol"].ToString();
        if (vCookie["COOKfNrPoz"] != null)
            Session["SESfNrPoz"] = vCookie["COOKfNrPoz"].ToString();
        if (vCookie["COOKfNume"] != null)
            Session["SESfNume"] = vCookie["COOKfNume"].ToString();
        if (vCookie["COOKfLoc"] != null)
            Session["SESfLoc"] = vCookie["COOKfLoc"].ToString();
        if (vCookie["COOKfStrada"] != null)
            Session["SESfStrada"] = vCookie["COOKfStrada"].ToString();
        if (vCookie["COOKfNr"] != null)
            Session["SESfNr"] = vCookie["COOKfNr"].ToString();
        if (vCookie["COOKfBl"] != null)
            Session["SESfBl"] = vCookie["COOKfBl"].ToString();
        if (vCookie["COOKfTip"] != null)
            Session["SESfTip"] = vCookie["COOKfTip"].ToString();
        if (Session["SESgospodarieId"] == null)
        {
            if (vCookie != null)
            {
                if (vCookie["COOKgospodarieId"] != null)
                {
                    Session["SESgospodarieId"] = vCookie["COOKgospodarieId"].ToString();
                }
                if (vCookie["COOKrandCurent"] != null)
                    Session["SESrandSelectat"] = vCookie["COOKrandCurent"].ToString();
            }
        }
    }
}
