﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;

/// <summary>
/// Summary description for agricol
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class agricol : System.Web.Services.WebService {

    public HeaderAutentificare Autentificare { get; set; }

    public agricol () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld() {
        return "Hello World";
    }

    private bool validareUserParola(HeaderAutentificare pAutentificare)
    {
        try
        {
            if (pAutentificare.Utilizator == "tntcomputers" && pAutentificare.Parola == "traxdata")
                return true;
            else return false;
        }
        catch { return false; }
    }

    #region Lista parcele

    /// <summary>
    /// Lista parcele
    /// </summary>
    /// <param name="pCnp"></param>
    /// <param name="pCodFiscal"></param>
    /// <param name="pAn"></param>
    /// <param name="pPj"></param>
    /// <returns></returns>
    [SoapHeader("Autentificare")]
    [WebMethod(Description = "Returneaza lista de parcele")]
    public RaspunsParcele Parcele(string pCnp, string pCodFiscal, string pAn, bool pPj)
    {
        RaspunsParcele Raspuns = new RaspunsParcele();
        InterogariParcele vInterogareParcele = new InterogariParcele();
        Raspuns.OperatieReusita = validareUserParola(Autentificare);
        if (!Raspuns.OperatieReusita)
        {
            Raspuns.MesajEroare = "Autentificare nereusita!";
            return Raspuns;
        }
        try
        {
            Raspuns.ListaParcele = vInterogareParcele.SelectListaParcele(pCnp, pCodFiscal, pAn, pPj);
        }
        catch (Exception e)
        {
            Raspuns.MesajEroare = e.Message;
            Raspuns.OperatieReusita = false;
        }
        return Raspuns;
    }
    
    #endregion

    #region Sabloane capitole

    /// <summary>
    /// Sabloane capitole
    /// </summary>
    /// <param name="pAn"></param>
    /// <param name="pCodCapitol"></param>
    /// <returns></returns>
    [SoapHeader("Autentificare")]
    [WebMethod(Description = "Returneaza o lista cu denumirea sabloanelor pentru un anumit capitol")]
    public RaspunsSabloaneCapitole SabloaneCapitole(string pAn, string pCodCapitol)
    {
        RaspunsSabloaneCapitole Raspuns = new RaspunsSabloaneCapitole();
        InterogariSabloane vInterogareSabloaneCapitole = new InterogariSabloane();
        Raspuns.OperatieReusita = validareUserParola(Autentificare);
        if (!Raspuns.OperatieReusita)
        {
            Raspuns.MesajEroare = "Autentificare nereusita!";
            return Raspuns;
        }
        try
        {
            Raspuns.ListaSabloaneCapitole = vInterogareSabloaneCapitole.SelectListaSabloane(pAn, pCodCapitol);
        }
        catch (Exception e)
        {
            Raspuns.MesajEroare = e.Message;
            Raspuns.OperatieReusita = false;
        }
        return Raspuns;
    }

    #endregion
}
