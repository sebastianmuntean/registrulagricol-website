﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for TipuriUtilaje
/// </summary>
[Serializable]
public class TipuriUtilaje
{
	public TipuriUtilaje()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public int Tractor { get; set; }
    public int Moara { get; set; }
    public int Autovehicule { get; set; }
}