﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for InterogariSabloane
/// </summary>
public class InterogariSabloane
{
    public InterogariSabloane()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public List<TipSabloaneCapitole> SelectListaSabloane(string pAn, string pCodCapitol)
    {
        List<TipSabloaneCapitole> vRezultat = new List<TipSabloaneCapitole>();
        SqlConnection vCon = ManipuleazaBD.CreareConexiune();
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "SELECT codRand, denumire4 FROM sabloaneCapitole WHERE (an = '" + pAn + "') AND (capitol = '" + pCodCapitol + "') AND (denumire4<>'') AND (CHARINDEX('+', formula) = 0) AND (CHARINDEX('-', formula) = 0)";
        SqlDataReader vTabel = vCmd.ExecuteReader();
        while (vTabel.Read())
        {
            TipSabloaneCapitole vElement = new TipSabloaneCapitole()
            {
                codRand = Convert.ToInt32(vTabel["codRand"].ToString()),
                denumire4 = vTabel["denumire4"].ToString()
            };
            vRezultat.Add(vElement);
        }
        vTabel.Close();
        ManipuleazaBD.InchideConexiune(vCon);
        return vRezultat;
    }
}