﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for TipParcele
/// </summary>
/// 
[Serializable]
public class TipParcele
{
	public TipParcele()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public long parcelaId { get; set; }
    public string parcelaDenumire { get; set; }
    public string parcelaCodRand { get; set; }
    public decimal parcelaSuprafataIntravilanHa { get; set; }
    public decimal parcelaSuprafataIntravilanAri { get; set; }
    public decimal parcelaSuprafataExtravilanHa { get; set; }
    public decimal parcelaSuprafataExtravilanAri { get; set; }
    public string parcelaNrTopo { get; set; }
    public string parcelaCF { get; set; }
    public string parcelaNrCadastral { get; set; }
    public string parcelaNrCadastralProvizoriu { get; set; }
    public string denumire4 { get; set; }
    public string parcelaNrBloc { get; set; }
    public string parcelaLocalitate { get; set; }
    public int an { get; set; }
    public string parcelaTarla { get; set; }
    public string parcelaTitluProprietate { get; set; }
    public string parcelaClasaBonitate { get; set; }
    public string parcelaPunctaj { get; set; }
    public string parcelaAdresa { get; set; }
    public string parcelaCategorie { get; set; }
}