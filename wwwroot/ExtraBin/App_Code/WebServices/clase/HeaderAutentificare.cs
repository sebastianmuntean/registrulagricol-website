﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

/// <summary>
/// Headerul folosit la autentificarea clientului
/// Contine doar adresa si cheia de autentificare
/// </summary>
[Serializable]
public class HeaderAutentificare : System.Web.Services.Protocols.SoapHeader
{

    /// <summary>
    /// Numele de login al utilizatorului
    /// </summary>
    public String Utilizator { get; set; }

    /// <summary>
    ///Parola
    /// </summary>
    public String Parola { get; set; }

}