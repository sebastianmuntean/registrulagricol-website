﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for InterogariAnimale
/// </summary>
public class InterogariAnimale
{
	public InterogariAnimale()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public TipuriAnimale SelectTipuriAnimale(List<string> pGospodarieId,DateTime pDataCererii)
    {
        TipuriAnimale vTipuriAnimale = new TipuriAnimale();
       
        // daca data cererii <= luna 6, iau valorile din sem I, (col2)
        string vColoana="1";
        if (pDataCererii.Month<=6)
        {
            vColoana="1";
        }
        else 
        {
            vColoana="2";
        }

        SqlConnection vConn = ManipuleazaBD.CreareConexiune();
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vConn;

        foreach (string vGospodarieId in pGospodarieId)
        {
            vCmd.CommandText = "select coalesce(col" + vColoana + ",0) from capitole where gospodarieId='" + vGospodarieId + "' and codCapitol='7' and codRand=1";
            vTipuriAnimale.Bovine += Convert.ToInt32(vCmd.ExecuteScalar());

            vCmd.CommandText = "select coalesce(col" + vColoana + ",0) from capitole where gospodarieId='" + vGospodarieId + "' and codCapitol='7' and codRand=50";
            vTipuriAnimale.Cabaline += Convert.ToInt32(vCmd.ExecuteScalar());

            vCmd.CommandText = "select coalesce(col" + vColoana + ",0) from capitole where gospodarieId='" + vGospodarieId + "' and codCapitol='7' and codRand=37";
            vTipuriAnimale.Porcine += Convert.ToInt32(vCmd.ExecuteScalar());

            vCmd.CommandText = "select coalesce(sum(col" + vColoana + "),0) from capitole where gospodarieId='" + vGospodarieId + "' and codCapitol='7' and (codRand=26 or codRand=32)";
            vTipuriAnimale.Ovine += Convert.ToInt32(vCmd.ExecuteScalar());

            vCmd.CommandText = "select coalesce(sum(col" + vColoana + "),0) from capitole where gospodarieId='" + vGospodarieId + "' and codCapitol='7' and (codRand=72)";
            vTipuriAnimale.Albine += Convert.ToInt32(vCmd.ExecuteScalar());

            vCmd.CommandText = "select coalesce(sum(col" + vColoana + "),0) from capitole where gospodarieId='" + vGospodarieId + "' and codCapitol='7' and (codRand=55)";
            vTipuriAnimale.Iepuri = Convert.ToInt32(vCmd.ExecuteScalar());

            vCmd.CommandText = "select coalesce(sum(col" + vColoana + "),0) from capitole where gospodarieId='" + vGospodarieId + "' and codCapitol='7' and (codRand=62)";
            vTipuriAnimale.Pasari = Convert.ToInt32(vCmd.ExecuteScalar());
        }

        ManipuleazaBD.InchideConexiune(vConn);
        return vTipuriAnimale;
    }

  
}