﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for TipuriTerenuri
/// </summary>
[Serializable]
public class TipuriTerenuri
{
	public TipuriTerenuri()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public decimal TerenConstructiiSiIntravilan { get; set; }
    public decimal TerenExtravilanProductiv { get; set; }
    public decimal TerenConstructiiInAfaraLocuinteiDeDomiciliu { get; set; }
}