﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Raspunsul serverului la cererile de actualizare
/// </summary>
[Serializable]
public class RaspunsGeneric
{

	/// <summary>
	/// Ultima operatie solicitata catre server a reusit sau nu
	/// </summary>
    public Boolean OperatieReusita { get; set; }

    /// <summary>
    /// Mesajul de eroare returnat de server
    /// </summary>
    public String MesajEroare { get; set; }

}