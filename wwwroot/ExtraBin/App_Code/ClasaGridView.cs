using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for ClasaGridView
/// </summary>
public class ClasaGridView
{
	public ClasaGridView()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public static void SelectGrid(GridView gv, GridViewRowEventArgs e, Page page)
    {
        if (e.Row.DataItemIndex == -1)
            return;

        //e.Row.Attributes.Add("onMouseOver", "this.style.cursor='pointer';this.ceva=this.style.backgroundColor;this.style.culoare=this.style.color;this.style.background='#e4d931';this.style.color='#5a540b';");
        e.Row.Attributes.Add("onMouseOver", "this.style.cursor='pointer';this.ceva=this.style.backgroundColor;this.className ='y';");
        e.Row.Attributes.Add("onMouseOut", "this.style.background=this.ceva;this.className ='x';");
        //e.Row.Attributes.Add("onMouseOut", "this.style.color='#ffddee';this.style.background=this.ceva;");
        e.Row.Attributes.Add("onclick", page.GetPostBackClientEvent(gv, "Select$" + e.Row.RowIndex.ToString()));
        
    }
    public static void SelectGrid1(GridView gv, GridViewRowEventArgs e, Page page, String culoare)
    {
        if (e.Row.DataItemIndex == -1)
            return;

        e.Row.Attributes.Add("onMouseOver", "this.style.cursor='pointer';this.ceva=this.style.backgroundColor;this.style.background='" + culoare + "'");
        e.Row.Attributes.Add("onMouseOut", "this.style.textDecoration='none';this.style.background=this.ceva");
        e.Row.Attributes.Add("onclick", page.GetPostBackClientEvent(gv, "Select$" + e.Row.RowIndex.ToString()));
    }
}
