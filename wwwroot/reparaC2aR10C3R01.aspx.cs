﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class reparaC2aR10C3R01 : System.Web.UI.Page
{
    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd1 = new SqlCommand();
        SqlCommand vCmd2 = new SqlCommand();
        vCmd1.Connection = vCon;
        vCmd2.Connection = vCon;
        // parcurg toate gospodariile de la unitatea selectata
        vCmd2.CommandText = "SELECT capitole.gospodarieId, gospodarii.volum, gospodarii.nrPozitie FROM capitole INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId WHERE (capitole.unitateId = '"+ddlUnitate.SelectedValue.ToString()+"') AND (capitole.codCapitol = '2a') AND (capitole.codRand = '9') AND (capitole.col1 + capitole.col2 <> 0) AND (capitole.an = '2011')";
        SqlDataReader vTabel = vCmd2.ExecuteReader();
        lbGospodarii.Items.Clear();
        while (vTabel.Read())
        {
            // verific daca rand 10 c2a este mai mare decat rand 1 c3
            vCmd1.CommandText = "SELECT coalesce(SUM(col1),0) AS Expr1 FROM capitole AS capitole_1 WHERE (gospodarieId = '" + vTabel["gospodarieId"].ToString() + "') AND (codRand = 10) AND (codCapitol = '2a')";
            Decimal vRand10C2a = Convert.ToDecimal(vCmd1.ExecuteScalar()) * 100;
            vCmd1.CommandText = "SELECT coalesce(SUM(col2),0) AS Expr1 FROM capitole AS capitole_1 WHERE (gospodarieId = '" + vTabel["gospodarieId"].ToString() + "') AND (codRand = 10) AND (codCapitol = '2a')";
            vRand10C2a += Convert.ToDecimal(vCmd1.ExecuteScalar());
            vCmd1.CommandText = "SELECT coalesce(SUM(col1),0) AS Expr1 FROM capitole AS capitole_1 WHERE (gospodarieId = '" + vTabel["gospodarieId"].ToString() + "') AND (codRand = 1) AND (codCapitol = '3')";
            Decimal vRand1C3 = Convert.ToDecimal(vCmd1.ExecuteScalar()) * 100;
            vCmd1.CommandText = "SELECT coalesce(SUM(col2),0) AS Expr1 FROM capitole AS capitole_1 WHERE (gospodarieId = '" + vTabel["gospodarieId"].ToString() + "') AND (codRand = 1) AND (codCapitol = '3')";
            vRand1C3 += Convert.ToDecimal(vCmd1.ExecuteScalar());
            vCmd1.CommandText = "SELECT coalesce(SUM(col1),0) AS Expr1 FROM capitole AS capitole_1 WHERE (gospodarieId = '" + vTabel["gospodarieId"].ToString() + "') AND (codRand = 9) AND (codCapitol = '2a')";
            Decimal vRand9C2a = Convert.ToDecimal(vCmd1.ExecuteScalar()) * 100;
            vCmd1.CommandText = "SELECT coalesce(SUM(col2),0) AS Expr1 FROM capitole AS capitole_1 WHERE (gospodarieId = '" + vTabel["gospodarieId"].ToString() + "') AND (codRand = 9) AND (codCapitol = '2a')";
            vRand9C2a += Convert.ToDecimal(vCmd1.ExecuteScalar());
            if (vRand10C2a - vRand1C3 == vRand9C2a && vRand9C2a != 0)
            {
                // iau suma de la rand 17 c3
                vCmd1.CommandText = "SELECT coalesce(SUM(col1),0) AS Expr1 FROM capitole AS capitole_1 WHERE (gospodarieId = '" + vTabel["gospodarieId"].ToString() + "') AND (codRand = 17) AND (codCapitol = '3')";
                Decimal vRand17C3 = Convert.ToDecimal(vCmd1.ExecuteScalar()) * 100;
                vCmd1.CommandText = "SELECT coalesce(SUM(col2),0) AS Expr1 FROM capitole AS capitole_1 WHERE (gospodarieId = '" + vTabel["gospodarieId"].ToString() + "') AND (codRand = 17) AND (codCapitol = '3')";
                vRand17C3 += Convert.ToDecimal(vCmd1.ExecuteScalar());
                // extrag ha si ari de la sume
                Decimal vHaRand9C2a = 0, vAriRand9C2a = 0, vHaRand1C3 = 0, vAriRand1C3 = 0, vHaRand17C3 = 0, vAriRand17C3 = 0, vHaRand10C2a = 0, vAriRand10C2a = 0;
                vHaRand9C2a = Math.Floor(vRand9C2a / 100);
                vAriRand9C2a = vRand9C2a - Math.Floor(vRand9C2a / 100) * 100;
                vHaRand10C2a = Math.Floor(vRand10C2a / 100);
                vAriRand10C2a = vRand10C2a - Math.Floor(vRand10C2a / 100) * 100;
                vHaRand1C3 = Math.Floor(vRand1C3 / 100) + vHaRand9C2a;
                vAriRand1C3 = (vRand1C3 - Math.Floor(vRand1C3 / 100) * 100) + vAriRand9C2a;
                if (vAriRand1C3 >= 100)
                {
                    vHaRand1C3 += Math.Floor(vAriRand1C3 / 100);
                    vAriRand1C3 -= Math.Floor(vAriRand1C3 / 100) * 100;
                }
                vHaRand17C3 = Math.Floor(vRand17C3 / 100) + vHaRand9C2a;
                vAriRand17C3 = (vRand17C3 - Math.Floor(vRand17C3 / 100)) + vAriRand9C2a;
                if (vAriRand17C3 >= 100)
                {
                    vHaRand17C3 += Math.Floor(vAriRand17C3 / 100);
                    vAriRand17C3 -= Math.Floor(vAriRand17C3 / 100) * 100;
                }
                // adaug la rand 1 c3 suma de rand 9 c2a
                if (vHaRand10C2a == vHaRand1C3 && vAriRand10C2a == vAriRand1C3)
                {
                    vCmd1.CommandText = "update capitole set col1='" + vHaRand1C3.ToString().Replace(",", ".") + "', col2='" + vAriRand1C3.ToString().Replace(",", ".") + "' where unitateId='" + ddlUnitate.SelectedValue.ToString() + "' and gospodarieId='" + vTabel["gospodarieId"].ToString() + "' and codCapitol='3' and codRand='1'";
                    vCmd1.ExecuteNonQuery();
                    vCmd1.CommandText = "update capitole set col1='" + vHaRand17C3.ToString().Replace(",", ".") + "', col2='" + vAriRand17C3.ToString().Replace(",", ".") + "' where unitateId='" + ddlUnitate.SelectedValue.ToString() + "' and gospodarieId='" + vTabel["gospodarieId"].ToString() + "' and codCapitol='3' and codRand='17'";
                    vCmd1.ExecuteNonQuery();
                    // verific corelatiile
                    clsCorelatii.VerificaCorelatie(Convert.ToInt32(ddlUnitate.SelectedValue), Convert.ToInt32(vTabel["gospodarieId"].ToString()), Convert.ToInt32(Session["SESan"]), "2a");
                    ListItem vItem = new ListItem("Volum: " + vTabel["volum"].ToString() + "; Nr pozitie: " + vTabel["nrPozitie"].ToString(), vTabel["gospodarieId"].ToString());
                    lbGospodarii.Items.Add(vItem);
                }
            }
        }
        vTabel.Close();
        ManipuleazaBD.InchideConexiune(vCon);
    }
}
