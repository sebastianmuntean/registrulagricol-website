﻿<%@ Page Title="Sabloane" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Sabloane.aspx.cs" Inherits="Sabloane" UICulture="ro-RO" Culture="ro-RO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="upSabloane" runat="server">
    
        <ContentTemplate>
            <asp:Panel ID="pnListaSabloaneDrepturi" runat="server" CssClass="panel_general">
                <asp:Panel runat="server" ID="pnGridView">
                    <asp:GridView ID="gvSabloane" runat="server" CssClass="tabela" AutoGenerateColumns="False"
                        DataSourceID="SqlListaPagini" PageSize="100">
                        <Columns>
                            <asp:TemplateField HeaderText="pagina">
                                <ItemTemplate>
                                   
                                    <asp:Label ID="paginaDenumire" runat="server" Text='<%# Bind("paginaDenumire") %>'
                                        Visible="true"></asp:Label>
                                    <asp:Label ID="paginaId" runat="server" Text='<%# Bind("paginaId") %>' Visible="false"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Citire">
                                <ItemTemplate>
                                    <asp:CheckBox ID="cbCitire" runat="server" Enabled="True" Checked="true" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Stergere">
                                <ItemTemplate>
                                    <asp:CheckBox ID="cbStergere" runat="server" Enabled="True" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Creare">
                                <ItemTemplate>
                                    <asp:CheckBox ID="cbCreare" runat="server" Enabled="True" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Modificare">
                                <ItemTemplate>
                                    <asp:CheckBox ID="cbModificare" runat="server" Enabled="True" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Tiparire">
                                <ItemTemplate>
                                    <asp:CheckBox ID="cbTiparire" runat="server" Enabled="True" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlListaPagini" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016 %>"
                        SelectCommand="SELECT  [paginaId], [paginaDenumire] FROM [pagini]"></asp:SqlDataSource>
                    <asp:DataList ID="dlNumarPagini" runat="server" DataSourceID="SqlPagini" Visible="True">
                        <ItemTemplate>
                            <asp:Label ID="lblNumarPagini" runat="server" Text='<%# "Număr total de pagini: " + Eval("numarPagini") %>' />
                        </ItemTemplate>
                    </asp:DataList>
                    <asp:SqlDataSource ID="SqlPagini" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016 %>"
                        SelectCommand="SELECT COUNT(*) AS numarPagini FROM pagini"></asp:SqlDataSource>
                </asp:Panel>
                <asp:Panel ID="pnButoane" CssClass="adauga" runat="server">
                    <asp:Button ID="btAdauga" CssClass="buton" runat="server" Text="Salvează şablonul pentru "
                        OnClick="btSabloane_Salveaza" /><br />
                    <asp:DropDownList ID="TipUtilizator" runat="server" DataSourceID="SqlTipuriUtilizatori"
                        DataTextField="tipUtilizatorDenumire" DataValueField="tipUtilizatorId" AutoPostBack="true"
                        OnDataBound="TipUtilizator_DataBound" OnSelectedIndexChanged="TipUtilizator_SelectedIndexChanged">
                    </asp:DropDownList><br /><br />
                    <asp:SqlDataSource ID="SqlTipuriUtilizatori" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016 %>"
                        SelectCommand="SELECT [tipUtilizatorId], [tipUtilizatorDenumire] FROM [tipuriUtilizatori]">
                    </asp:SqlDataSource>
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
