﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;

public partial class printCapitoleToateCompactXls : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "printCapToate", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
            clsTiparireCapitole.StergeRapoarteVechi(Session["SESutilizatorId"].ToString(), Convert.ToInt16(Session["SESan"]));
            // umplem tabela de rapoarte
            // scoatem lista anilor din ciclul curent
            string vCiclu = ManipuleazaBD.fRezultaUnString("SELECT * FROM cicluri WHERE cicluAni LIKE '%" + Session["SESan"].ToString() + "%'", "cicluAni", Convert.ToInt16(Session["SESan"]));
            char[] vDelimitator = { '#' };
            string[] vAniDinCiclu = vCiclu.Split(vDelimitator, StringSplitOptions.RemoveEmptyEntries);
            List<string> vListaCampuri = new List<string> { "codCapitol", "capitoleCodRand", "denumire1", "denumire2", "denumire3", "denumire4", "denumire5" };
            // gasim gospodaria initiala pentru a lua datele din totii anii pentru aceasta - dupa gospodarieidinitial
            string vGospodarieIdInitial = ManipuleazaBD.fRezultaUnString("SELECT * FROM gospodarii WHERE gospodarieId = '" + Session["SESgospodarieId"].ToString() + "'", "gospodarieIdInitial", Convert.ToInt16(Session["SESan"]));
            int vCodRand = 0;
            string vComanda = "";
            int vAnOrdine = 1;
            foreach (string vAn in vAniDinCiclu)
            {
                string vAnCamp = "an" + vAnOrdine.ToString();
                vListaCampuri = new List<string> { "camp1", "camp2", "camp3", "camp4", "camp5", "camp6", "col1", "col2", "col3", "col4", vAnCamp };
                List<List<string>> vValori = new List<List<string>> { };
                vValori = ManipuleazaBD.fRezultaListaStringuri(" SELECT parcele.parcelaDenumire + ' ' + parcele.parcelaTarla as camp1, parcele.parcelaSuprafataIntravilanHa as col1, parcele.parcelaSuprafataIntravilanAri as col2, parcele.parcelaSuprafataExtravilanHa as col3, parcele.parcelaSuprafataExtravilanAri as col4, parcele.parcelaNrTopo as camp2, CASE coalesce(parcele.parcelaCF, '') WHEN '' THEN '' ELSE 'CF: ' + convert(nvarchar, parcele.parcelaCF) + ' / ' END + CASE coalesce(parcele.parcelaNrCadastral, '') WHEN '' THEN '' ELSE 'nr.cad: ' + convert(nvarchar, parcele.parcelaNrCadastral) + ' / ' END + CASE coalesce(parcele.parcelaNrCadastralProvizoriu, '') WHEN '' THEN '' ELSE ' nr.cad.prv. ' + convert(nvarchar, parcele.parcelaNrCadastralProvizoriu) END AS camp3, sabloaneCapitole.denumire1 AS camp4,parcele.parcelaNrBloc AS camp5, CASE coalesce(parcele.parcelaMentiuni, '') WHEN '' THEN '' ELSE convert(nvarchar, parcele.parcelaMentiuni) + ' / ' END  +  convert(nvarchar, parcele.parcelaLocalitate) + ' ' + convert(nvarchar, parcele.parcelaAdresa) + ' ' + CASE parcele.parcelaTitluProprietate WHEN '-' THEN '' WHEN '' THEN '' ELSE 'titlu propr: ' + convert(nvarchar, parcele.parcelaTitluProprietate) + ' / ' END + CASE  parcele.parcelaClasaBonitate WHEN '-' THEN '' ELSE ' cls.bonitate: ' + convert(nvarchar, parcele.parcelaClasaBonitate) + ' / ' END + CASE parcele.parcelaPunctaj WHEN '0' THEN '' ELSE 'punctaj: ' + convert(nvarchar, parcele.parcelaPunctaj) END AS camp6,an" + vAnOrdine.ToString() + " = '" + vAn + "' FROM parcele INNER JOIN  gospodarii ON parcele.gospodarieId = gospodarii.gospodarieId INNER JOIN sabloaneCapitole ON parcele.parcelaCategorie = sabloaneCapitole.codRand AND sabloaneCapitole.capitol = '2a' AND sabloaneCapitole.an = '" + vAn + "' AND parcele.an = '" + vAn + "' WHERE (gospodarii.gospodarieIdInitial = '" + vGospodarieIdInitial + "') ORDER BY camp1 ", vListaCampuri, Convert.ToInt16(Session["SESan"]));
                // construim comanda pentru fiecare rand
                vCodRand = 1;
                if (vValori.Count != 0)
                {
                    foreach (List<string> vRandValori in vValori)
                    {
                        vComanda += @"INSERT INTO [rapCapitole] ([utilizatorId], [capitol], [codRand], [camp1], [camp2], [camp3], [camp4], [camp5], [camp6], [col1_1], [col1_2], [col1_3], [col1_4], [" + vAnCamp + "]) VALUES ('" +
                            Convert.ToInt32(Session["SESutilizatorId"]) + "','2b','" + vCodRand + "','" +
                            vRandValori[0] + "','" +
                            vRandValori[1] + "','" +
                            vRandValori[2] + "','" +
                            vRandValori[3] + "','" +
                            vRandValori[4] + "','" +
                            vRandValori[5] + "','" +
                            vRandValori[6].Replace(',', '.') + "', '" +
                            vRandValori[7].Replace(',', '.') + "', '" +
                            vRandValori[8].Replace(',', '.') + "', '" +
                            vRandValori[9].Replace(',', '.') + "', '" +
                            vRandValori[10] + "');";
                        vCodRand++;
                    }
                }
                else
                {
                    vComanda += @"INSERT INTO [rapCapitole] ([utilizatorId], [codRand], [capitol], [" + vAnCamp + "]) VALUES ('" +
                                Convert.ToInt32(Session["SESutilizatorId"]) + "','-','2b', '" + vAn + "');";
                }
                vAnOrdine++;
            }
            // scriem in rapCapitole de unde vom lua la print
            ManipuleazaBD.fManipuleazaBD(vComanda, Convert.ToInt16(Session["SESan"]));
            clsTiparireCapitole.UmpleRapCapitoleFaraStergere(Session["SESutilizatorId"].ToString(), Session["SESan"].ToString(), Session["SESgospodarieId"].ToString(), "2a");
            clsTiparireCapitole.UmpleRapCapitoleFaraStergere(Session["SESutilizatorId"].ToString(), Session["SESan"].ToString(), Session["SESgospodarieId"].ToString(), "3");
            clsTiparireCapitole.UmpleRapCapitoleFaraStergere(Session["SESutilizatorId"].ToString(), Session["SESan"].ToString(), Session["SESgospodarieId"].ToString(), "4a");
            clsTiparireCapitole.UmpleRapCapitoleFaraStergere(Session["SESutilizatorId"].ToString(), Session["SESan"].ToString(), Session["SESgospodarieId"].ToString(), "4b");
            clsTiparireCapitole.UmpleRapCapitoleFaraStergere(Session["SESutilizatorId"].ToString(), Session["SESan"].ToString(), Session["SESgospodarieId"].ToString(), "4c");
            clsTiparireCapitole.UmpleRapCapitoleFaraStergere(Session["SESutilizatorId"].ToString(), Session["SESan"].ToString(), Session["SESgospodarieId"].ToString(), "5a");
            clsTiparireCapitole.UmpleRapCapitoleFaraStergere(Session["SESutilizatorId"].ToString(), Session["SESan"].ToString(), Session["SESgospodarieId"].ToString(), "5b");
            clsTiparireCapitole.UmpleRapCapitoleFaraStergere(Session["SESutilizatorId"].ToString(), Session["SESan"].ToString(), Session["SESgospodarieId"].ToString(), "5c");
            clsTiparireCapitole.UmpleRapCapitoleFaraStergere(Session["SESutilizatorId"].ToString(), Session["SESan"].ToString(), Session["SESgospodarieId"].ToString(), "5d");
            clsTiparireCapitole.UmpleRapCapitoleFaraStergere(Session["SESutilizatorId"].ToString(), Session["SESan"].ToString(), Session["SESgospodarieId"].ToString(), "6");
            clsTiparireCapitole.UmpleRapCapitoleFaraStergere(Session["SESutilizatorId"].ToString(), Session["SESan"].ToString(), Session["SESgospodarieId"].ToString(), "7");
            clsTiparireCapitole.UmpleRapCapitoleFaraStergere(Session["SESutilizatorId"].ToString(), Session["SESan"].ToString(), Session["SESgospodarieId"].ToString(), "8");
            clsTiparireCapitole.UmpleRapCapitoleFaraStergere(Session["SESutilizatorId"].ToString(), Session["SESan"].ToString(), Session["SESgospodarieId"].ToString(), "9");
            clsTiparireCapitole.UmpleRapCapitoleFaraStergere(Session["SESutilizatorId"].ToString(), Session["SESan"].ToString(), Session["SESgospodarieId"].ToString(), "10a");
            clsTiparireCapitole.UmpleRapCapitoleFaraStergere(Session["SESutilizatorId"].ToString(), Session["SESan"].ToString(), Session["SESgospodarieId"].ToString(), "10b");
            clsTiparireCapitole.UmpleRapCapitoleFaraStergere(Session["SESutilizatorId"].ToString(), Session["SESan"].ToString(), Session["SESgospodarieId"].ToString(), "11");
            ReportViewer1.LocalReport.Refresh();
        }
    }
    protected void Page_SaveStateComplete(object sender, EventArgs e)
    {
        Warning[] warnings;
        string[] streamids;
        string mimeType;
        string encoding;
        string fileNameExtension;
        byte[] bytes = ReportViewer1.LocalReport.Render("Excel", null, out mimeType, out encoding, out fileNameExtension, out streamids, out warnings);
        Response.Buffer = true;
        Response.Clear();
        Response.ContentType = mimeType;
        Response.AddHeader("content-disposition", "inline; filename=ToateCapitoleleRA." + fileNameExtension);
        Response.BinaryWrite(bytes);
        Response.Flush();
        Response.End();
        Response.Close();
    }
}
