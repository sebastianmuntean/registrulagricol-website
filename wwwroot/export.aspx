﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="export.aspx.cs" Inherits="export" UICulture="ro-Ro" Culture="ro-Ro"
    EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Export date în format XML"></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="pnAfiseazaMesaje" runat="server">
        <asp:ValidationSummary ID="valSumUtilizatori" runat="server" DisplayMode="SingleParagraph"
            Visible="true" ValidationGroup="GrupValidareExport" CssClass="validator" ForeColor="" />
    </asp:Panel>
    <asp:Label ID="Label1" runat="server" Visible="false"></asp:Label>
    <asp:Panel runat="server" CssClass="panel_general">
        <asp:Table ID="Table1" runat="server" Width="90%">
            <asp:TableHeaderRow>
                <asp:TableHeaderCell>
                    <asp:Button ID="btExporta" runat="server" Text="Exportă datele" OnClick="btExporta_Click"
                        CssClass="buton" />
                </asp:TableHeaderCell>
                <asp:TableHeaderCell>
                    <asp:Button ID="btImporta" runat="server" Text="Importă datele" OnClick="btImporta_Click"
                        CssClass="buton" />
                    <asp:FileUpload ID="fuImporta" runat="server" Height="40" OnLoad="fuImporta_Load"  />
                </asp:TableHeaderCell>
            </asp:TableHeaderRow>
            <asp:TableRow VerticalAlign="Top">
                <asp:TableCell>
                    <asp:UpdatePanel ID="upExport" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="gvExporturi" CssClass="tabela" runat="server" AutoGenerateColumns="False"
                                DataSourceID="SqlExporturi" AllowPaging="True" AllowSorting="True" OnDataBound="gvExporturi_OnDataBound">
                                <Columns>
                                    <asp:BoundField DataField="exportUnitateCodFiscal" HeaderText="Cod Fiscal" SortExpression="exportUnitateCodFiscal" />
                                    <asp:BoundField DataField="unitateDenumire" HeaderText="Denumirea unității" SortExpression="unitateDenumire" />
                                    <asp:BoundField DataField="exportData" HeaderText="Data Exportului" SortExpression="exportData"
                                        HtmlEncode="False" DataFormatString="{0:d}" />
                                    <asp:BoundField DataField="exportOra" HeaderText="Ora" SortExpression="exportOra" />
                                    <asp:BoundField DataField="exportCale" HeaderText="descarcă fişierul" HtmlEncode="False"
                                        SortExpression="exportCale" />
                                </Columns>
                            </asp:GridView>
                            <asp:SqlDataSource ID="SqlExporturi" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString %>"
                                SelectCommand="SELECT [exportCale],[unitati].[unitateId],[unitati].[unitateDenumire],[exporturi].[exportUnitateCodFiscal],[exportData], [exportOra], [utilizatorCNP] FROM [exporturi] LEFT OUTER JOIN unitati ON exporturi.exportUnitateCodFiscal = unitati.unitateCodFiscal WHERE (([exportTip] = @exportTip) AND ([unitati].[unitateId] = @unitateId))">
                                <SelectParameters>
                                    <asp:Parameter DefaultValue="0" Name="exportTip" Type="Byte" />
                                    <asp:SessionParameter DefaultValue="0" Name="unitateId" SessionField="SESunitateId"
                                        Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:UpdatePanel ID="upImport" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="gvImporturi" CssClass="tabela" runat="server" AutoGenerateColumns="False"
                                DataSourceID="SqlImporturi" AllowPaging="true" AllowSorting="true" OnDataBound="gvImporturi_OnDataBound">
                                <Columns>
                                    <asp:BoundField DataField="exportUnitateCodFiscal" HeaderText="Cod Fiscal" SortExpression="exportUnitateCodFiscal" />
                                    <asp:BoundField DataField="unitateDenumire" HeaderText="Denumirea unității" SortExpression="unitateDenumire" />
                                    <asp:BoundField DataField="importData" HeaderText="Data Importului" SortExpression="importData"
                                        HtmlEncode="False" DataFormatString="{0:d}" />
                                    <asp:BoundField DataField="exportCale" HeaderText="descarcă fişierul" HtmlEncode="False"
                                        SortExpression="exportCale" />
                                </Columns>
                            </asp:GridView>
                            <asp:SqlDataSource ID="SqlImporturi" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString %>"
                                SelectCommand="SELECT [exportCale],[unitati].[unitateId],[unitati].[unitateDenumire],[exporturi].[exportUnitateCodFiscal],[importData], [exportOra], [utilizatorCNP] FROM [exporturi] LEFT OUTER JOIN unitati ON exporturi.exportUnitateCodFiscal = unitati.unitateCodFiscal WHERE (([exportTip] = @exportTip) AND ([unitati].[unitateId] = @unitateId))">
                                <SelectParameters>
                                    <asp:Parameter DefaultValue="1" Name="exportTip" Type="Byte" />
                                    <asp:SessionParameter DefaultValue="0" Name="unitateId" SessionField="SESunitateId"
                                        Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </asp:Panel>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnValidatoare" CssClass="ascunsa" runat="server">
                <!-- //////////// VALIDATOARE  \\\\\\\\\\\\\\\\\\ -->
                <asp:CustomValidator ID="valCustom" runat="server" ErrorMessage="" ValidationGroup="GrupValidareExport"></asp:CustomValidator>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
