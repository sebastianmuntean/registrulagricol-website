﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class RapoarteCentralizatoare5Ani2012 : System.Web.UI.Page
{
    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "RaportCentralizatoare", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
        }
    }
    protected void lbCapitol1_Click(object sender, EventArgs e)
    {
        // Centralizator capitolul 1
        StergeCapitolCentralizatorUtilizator(Convert.ToInt32(Session["SESan"]), Convert.ToInt32(Session["SESutilizatorId"]), "1");
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        SqlCommand vCmd1 = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd1.Connection = vCon;
        int vCodRand = 1;
        vCmd1.CommandText = "SELECT COUNT(*) AS Expr1 FROM gospodarii INNER JOIN unitati ON gospodarii.unitateId = unitati.unitateId where strainas = 0 and persJuridica = 0  AND (an='" + Session["SESan"].ToString() + "') and convert(nvarchar,unitati.unitateId) like '" + ddlUnitate.SelectedValue.ToString() + "' AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "')";
        decimal vNrTotalGospodarii = Convert.ToDecimal(vCmd1.ExecuteScalar());
        AdaugaCapitolCentralizator(Convert.ToInt32(Session["SESutilizatorId"]), Convert.ToInt32(Session["SESan"]), "1", vCodRand, "Poziţii ale gospodăriilor populaţiei/exploataţiilor agricole individuale/persoanelor fizice autorizate/întreprinderilor individuale/asociaţiilor familiale cu domiciliul în localitate - total", "", "", "", "", vNrTotalGospodarii, 0, 0, 0, 0, 0, 0, 0);
        vCodRand++;
        vCmd1.CommandText = "SELECT COUNT(*) AS Expr1 FROM gospodarii INNER JOIN unitati ON gospodarii.unitateId = unitati.unitateId INNER JOIN localitati ON unitati.localitateId = localitati.localitateId AND gospodarii.localitate = localitati.localitateDenumire WHERE (gospodarii.strainas = 0) AND (gospodarii.persJuridica = 0)  AND (an='" + Session["SESan"].ToString() + "') AND  (CONVERT(nvarchar, gospodarii.unitateId) LIKE '" + ddlUnitate.SelectedValue.ToString() + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "')";
        decimal vNrGospodariiDomiciliu = Convert.ToDecimal(vCmd1.ExecuteScalar());
        AdaugaCapitolCentralizator(Convert.ToInt32(Session["SESutilizatorId"]), Convert.ToInt32(Session["SESan"]), "1", vCodRand, "- în satul de reşedinţă", "", "", "", "", vNrGospodariiDomiciliu, 0, 0, 0, 0, 0, 0, 0);
        vCodRand++;

        // scoatem toata lista cu localitati si nr gospodarii pe fiecare
        vCmd.CommandText = @" SELECT     COUNT(gospodarii.localitate) AS numar, unitati.unitateDenumire + ': localitatea ' + gospodarii.localitate AS unitatelocalitate, gospodarii.unitateId,  localitati.localitateDenumire FROM         gospodarii INNER JOIN unitati ON gospodarii.unitateId = unitati.unitateId INNER JOIN localitati ON unitati.localitateId = localitati.localitateId WHERE     (gospodarii.strainas = '0') AND (gospodarii.persJuridica = '0')  AND (an='" + Session["SESan"].ToString() + "') AND (CONVERT(nvarchar, gospodarii.unitateId) LIKE '" + ddlUnitate.SelectedValue.ToString() + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') GROUP BY gospodarii.localitate, gospodarii.unitateId, localitati.localitateDenumire, unitati.unitateDenumire ORDER BY unitati.unitateDenumire, gospodarii.localitate";
        SqlDataReader vTabel = vCmd.ExecuteReader();
        string vComanda = "";
        while (vTabel.Read())
        { 
            vComanda += @"INSERT INTO [rapCentralizatoare] ([utilizatorId], [an], [capitol], [codRand], [denumire1], [denumire2], [denumire3], [denumire4], [denumire5], [col1], [col2], [col3], [col4], [col5], [col6], [col7], [col8]) VALUES ('" + Convert.ToInt32(Session["SESutilizatorId"]) + "','" + Convert.ToInt32(Session["SESan"]) + "','1','" + vCodRand.ToString() + "',N'" + vTabel["unitatelocalitate"].ToString() + "','','','','','" + vTabel["numar"].ToString() + "','0','0','0','0','0','0','0'); ";
            vCodRand++;
        }
        vTabel.Close();
        vCmd.CommandText = vComanda;
        vCmd.ExecuteNonQuery();
        vCmd1.CommandText = "SELECT COUNT(*) AS Expr1 FROM gospodarii INNER JOIN unitati ON gospodarii.unitateId = unitati.unitateId where strainas=1  AND (an='" + Session["SESan"].ToString() + "') and convert(nvarchar,unitati.unitateId) like '" + ddlUnitate.SelectedValue.ToString() + "' AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "')";
        decimal vNrGospodariiStrainasi = Convert.ToDecimal(vCmd1.ExecuteScalar());
        AdaugaCapitolCentralizator(Convert.ToInt32(Session["SESutilizatorId"]), Convert.ToInt32(Session["SESan"]), "1", vCodRand, "Poziţii ale persoanelor cu domiciliul în alte localităţi", "", "", "", "", vNrGospodariiStrainasi, 0, 0, 0, 0, 0, 0, 0);
        vCodRand++;
        vCmd1.CommandText = "SELECT COUNT(*) AS Expr1 FROM gospodarii INNER JOIN unitati ON gospodarii.unitateId = unitati.unitateId where strainas=0 and persJuridica=1  AND (an='" + Session["SESan"].ToString() + "') and convert(nvarchar,unitati.unitateId) like '" + ddlUnitate.SelectedValue.ToString() + "' AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "')";
        decimal vNrGospodariiPJ = Convert.ToDecimal(vCmd1.ExecuteScalar());
        AdaugaCapitolCentralizator(Convert.ToInt32(Session["SESutilizatorId"]), Convert.ToInt32(Session["SESan"]), "1", vCodRand, "Poziţii ale unităţilor cu personalitate juridică, care au activitate pe raza localităţii", "", "", "", "", vNrGospodariiPJ, 0, 0, 0, 0, 0, 0, 0);
        ManipuleazaBD.InchideConexiune(vCon);
        ResponseHelper.Redirect("~/printCentralizatorCapitolul1.aspx?codCapitol=1", "_new", "");
    }
    public static void AdaugaCapitolCentralizator(Int32 pUtilizatorId, Int32 pAn, String pCapitol, Int32 pCodRand, String pDenumire1, String pDenumire2, String pDenumire3, String pDenumire4, String pDenumire5, Decimal pCol1, Decimal pCol2, Decimal pCol3, Decimal pCol4, Decimal pCol5, Decimal pCol6, Decimal pCol7, Decimal pCol8)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(HttpContext.Current.Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = @"INSERT INTO [rapCentralizatoare] ([utilizatorId], [an], [capitol], [codRand], [denumire1], [denumire2], [denumire3], [denumire4], [denumire5], [col1], [col2], [col3], [col4], [col5], [col6], [col7], [col8]) VALUES ('" + pUtilizatorId.ToString() + "','" + pAn.ToString() + "','" + pCapitol.ToString() + "','" + pCodRand.ToString() + "',N'" + pDenumire1 + "',N'" + pDenumire2 + "',N'" + pDenumire3 + "',N'" + pDenumire4 + "',N'" + pDenumire5 + "','" + pCol1.ToString().Replace(",", ".") + "','" + pCol2.ToString().Replace(",", ".") + "','" + pCol3.ToString().Replace(",", ".") + "','" + pCol4.ToString().Replace(",", ".") + "','" + pCol5.ToString().Replace(",", ".") + "','" + pCol6.ToString().Replace(",", ".") + "','" + pCol7.ToString().Replace(",", ".") + "','" + pCol8.ToString().Replace(",", ".") + "')";
        vCmd.ExecuteNonQuery();
        ManipuleazaBD.InchideConexiune(vCon);
    }
    public static void StergeCapitolCentralizatorUtilizator(Int32 pAn, Int32 pUtilizatorId, String pCapitol)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(HttpContext.Current.Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = @"delete from rapCentralizatoare where utilizatorId='" + pUtilizatorId.ToString() + "' and an='" + pAn.ToString() + "' and capitol='" + pCapitol + "'";
        vCmd.ExecuteNonQuery();
        vCmd.CommandText = @"delete from rapCapitole where utilizatorId='" + pUtilizatorId.ToString() + "' and capitol='" + pCapitol + "'";
        vCmd.ExecuteNonQuery();
        ManipuleazaBD.InchideConexiune(vCon);
    }
    protected List<string> C2aUmpleLista(List<string> pLista, List<string> pListaRand, List<List<string>> pListaRanduri)
    {
        List<string> vLista = new List<string> { };
        foreach (List<string> vListaRanduri in pListaRanduri)
        { 
            int i=0; int vAdaugat = 0;
            foreach (string vCaut in pListaRand)
            {
                // daca e codrand iau indexul din lista cu randurile
                if (vCaut==vListaRanduri[0])
                {
                    // si adaug in lista valoarea din prima lista, cea de valori
                    vLista.Add(pLista[i]);
                    vAdaugat = 1;
                    break;
                }
                i++;
            }
            // daca nu s-a gasit randul mai sus adaug valoare zero
            if (vAdaugat == 0)
            {
                vLista.Add("0");
            }
        }
        return vLista;
    }
    protected void lbCapitol2a_Click(object sender, EventArgs e)
    {
        lbCapitol2a_3_Click("2a");
    }    
    protected void lbCapitol3_Click(object sender, EventArgs e)
    {
        lbCapitol2a_3_Click("3");
    }
    protected void lbCapitol4_Click(object sender, EventArgs e)
    {
        lbCapitol2a_3_Click("4a");
    }
    protected void lbCapitol2a_3_Click(string vCapitol)
    {
        // Centralizator capitolul 2a

        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        
        StergeCapitolCentralizatorUtilizator(Convert.ToInt32(Session["SESan"]), Convert.ToInt32(Session["SESutilizatorId"]), vCapitol);

        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));

        SqlCommand vCmd = new SqlCommand();
        SqlCommand vCmd1 = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd1.Connection = vCon;
        vCmd.CommandText = @"SELECT * FROM  sabloaneCapitole WHERE (capitol = '" + vCapitol + "') AND (an='" + Session["SESan"].ToString() + "') order by codRand";
        SqlDataReader vTabelCapitole = vCmd.ExecuteReader();
        // facem o lista cu capitolele din sabloane
        List<List<string>> vListaCapitoleSabloane = new List<List<string>> { };
        while (vTabelCapitole.Read())
        {
            List<string> vListaX = new List<string> { };
            vListaX.Add(vTabelCapitole["codRand"].ToString());
            vListaX.Add(vTabelCapitole["denumire1"].ToString());
            vListaX.Add(vTabelCapitole["denumire2"].ToString());
            vListaX.Add(vTabelCapitole["denumire3"].ToString());
            vListaX.Add(vTabelCapitole["denumire4"].ToString());
            vListaX.Add(vTabelCapitole["denumire5"].ToString());
            vListaCapitoleSabloane.Add(vListaX);
        }
        vTabelCapitole.Close();

        // scoatem anii din ciclu
        string[] vAni = CalculCapitole.vListaAni(Session["SESan"].ToString());

        // lista pe 5 ani
        List<List<string>> vColoaneListe1 = new List<List<string>> { };
        List<List<string>> vColoaneListe2 = new List<List<string>> { };
        List<List<string>> vColoaneListe3 = new List<List<string>> { };
        List<List<string>> vColoaneListe4 = new List<List<string>> { };
        foreach (string vAn in vAni)
        {
            // pt toate randurile, toate tipurile de gospodarii
            vCmd1.CommandText = "SELECT     COALESCE ((SUM(capitoleCentralizate.col1) + (SUM(capitoleCentralizate.col2) - { fn MOD(SUM(capitoleCentralizate.col2), 100) }) / 100) + { fn MOD(SUM(capitoleCentralizate.col2), 100) } / 100, 0) AS Expr1, capitoleCentralizate.codRand FROM capitoleCentralizate  INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + vAn + "') AND (capitoleCentralizate.codCapitol = '" + vCapitol + "') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND unitati.unitatePrincipala <> '0'  GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";
            SqlDataReader vTabelCapitole1 = vCmd1.ExecuteReader();
            List<string> vColLista1 = new List<string> { };
            List<string> vColListaRand1 = new List<string> { };
            while (vTabelCapitole1.Read())
            {
                vColLista1.Add(vTabelCapitole1["Expr1"].ToString());
                vColListaRand1.Add(vTabelCapitole1["codRand"].ToString());
            }
            vTabelCapitole1.Close();
            vColLista1 = C2aUmpleLista(vColLista1, vColListaRand1, vListaCapitoleSabloane);
            // pt toate randurile, localnici
            vCmd1.CommandText = "SELECT     COALESCE ((SUM(capitoleCentralizate.col1) + (SUM(capitoleCentralizate.col2) - { fn MOD(SUM(capitoleCentralizate.col2), 100) }) / 100) + { fn MOD(SUM(capitoleCentralizate.col2), 100) } / 100, 0) AS Expr1, capitoleCentralizate.codRand FROM capitoleCentralizate  INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + vAn + "') AND (capitoleCentralizate.codCapitol = '" + vCapitol + "') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND unitati.unitatePrincipala <> '0' AND (capitoleCentralizate.gospodarieTip = 0) GROUP BY capitoleCentralizate.codRand   ORDER BY capitoleCentralizate.codRand";
            vTabelCapitole1 = vCmd1.ExecuteReader();
            List<string> vColLista2 = new List<string> { };
            List<string> vColListaRand2 = new List<string> { };
            while (vTabelCapitole1.Read())
            {
                vColLista2.Add(vTabelCapitole1["Expr1"].ToString());
                vColListaRand2.Add(vTabelCapitole1["codRand"].ToString());
            }
            vTabelCapitole1.Close();
            vColLista2 = C2aUmpleLista(vColLista2, vColListaRand2, vListaCapitoleSabloane);

            // pt toate randurile, strainasi
            vCmd1.CommandText = "SELECT     COALESCE ((SUM(capitoleCentralizate.col1) + (SUM(capitoleCentralizate.col2) - { fn MOD(SUM(capitoleCentralizate.col2), 100) }) / 100) + { fn MOD(SUM(capitoleCentralizate.col2), 100) } / 100, 0) AS Expr1, capitoleCentralizate.codRand FROM capitoleCentralizate  INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + vAn + "') AND (capitoleCentralizate.codCapitol = '" + vCapitol + "') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "')  AND (capitoleCentralizate.gospodarieTip >= 2)  AND unitati.unitatePrincipala <> '0' GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";
            vTabelCapitole1 = vCmd1.ExecuteReader();
            List<string> vColLista3 = new List<string> { };
            List<string> vColListaRand3 = new List<string> { };
            while (vTabelCapitole1.Read())
            {
                vColLista3.Add(vTabelCapitole1["Expr1"].ToString());
                vColListaRand3.Add(vTabelCapitole1["codRand"].ToString());
            }
            vTabelCapitole1.Close();
            vColLista3 = C2aUmpleLista(vColLista3, vColListaRand3, vListaCapitoleSabloane);
            // pt toate randurile, pers juridice
            vCmd1.CommandText = "SELECT     COALESCE ((SUM(capitoleCentralizate.col1) + (SUM(capitoleCentralizate.col2) - { fn MOD(SUM(capitoleCentralizate.col2), 100) }) / 100) + { fn MOD(SUM(capitoleCentralizate.col2), 100) } / 100, 0) AS Expr1, capitoleCentralizate.codRand FROM capitoleCentralizate  INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + vAn + "') AND (capitoleCentralizate.codCapitol = '" + vCapitol + "') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "')  AND (capitoleCentralizate.gospodarieTip in (1,3))  AND unitati.unitatePrincipala <> '0' GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";
            vTabelCapitole1 = vCmd1.ExecuteReader();
            List<string> vColLista4 = new List<string> { };
            List<string> vColListaRand4 = new List<string> { };
            while (vTabelCapitole1.Read())
            {
                vColLista4.Add(vTabelCapitole1["Expr1"].ToString());
                vColListaRand4.Add(vTabelCapitole1["codRand"].ToString());
            }

            vTabelCapitole1.Close();
            vColLista4 = C2aUmpleLista(vColLista4, vColListaRand4, vListaCapitoleSabloane);

            vColoaneListe1.Add(vColLista1);
            vColoaneListe2.Add(vColLista2);
            vColoaneListe3.Add(vColLista3);
            vColoaneListe4.Add(vColLista4);
        }
        int vContor = 0;

        // facem stringul pt insert

        string vInsertX = "INSERT INTO [rapCapitole] ([utilizatorId], [capitol], [codRand], [denumire1], [denumire2], [denumire3], [denumire4], [denumire5] ";
        string vInsertY = ") VALUES (";
        string vInsertW = "'" + Convert.ToInt32(Session["SESutilizatorId"]) + "','" + vCapitol + "', ";
        string vInsertZ = ");";

        List<string> vInsertPartea1 = new List<string>{};
        List<string> vInsertPartea2 = new List<string>{};
        int i = 0;
        int j = 0;
        foreach (string vAn in vAni)
        {
            vInsertPartea1.Add(", [an" + (j + 1).ToString() + "], [col" + (j + 1).ToString() + "_1], [col" + (j + 1).ToString() + "_2], [col" + (j + 1).ToString() + "_3], [col" + (j + 1).ToString() + "_4]");
         //   vInsertPartea2.Add(", '" + vAn + "', '" + vColoaneListe1[j][j].ToString().Replace(",", ".") + "','" + vColoaneListe2[1][j].ToString().Replace(",", ".") + "','" + vColoaneListe3[2][j].ToString().Replace(",", ".") + "','" + vColoaneListe4[3][j].ToString().Replace(",", ".") + "'");
            j++;
            vContor++;
        }

        vCmd.CommandText = "";
        foreach (List<string> vRand in vListaCapitoleSabloane)
        {
            j = 0;
            vInsertPartea2 = new List<string>();
            foreach (string vAn in vAni)
            {
                vInsertPartea2.Add(", '" + vAn + "', '" + vColoaneListe1[j][i].ToString().Replace(",", ".") + "','" + vColoaneListe2[j][i].ToString().Replace(",", ".") + "','" + vColoaneListe3[j][i].ToString().Replace(",", ".") + "','" + vColoaneListe4[j][i].ToString().Replace(",", ".") + "'");
                j++;
                vContor++;
            }

            vCmd.CommandText += vInsertX + vInsertPartea1[0] + vInsertPartea1[1] + vInsertPartea1[2] + vInsertPartea1[3] + vInsertPartea1[4] + vInsertY + vInsertW + "'" + Convert.ToInt32(vRand[0].ToString()) + "',N'" + vRand[1].ToString() + "',N'" + vRand[2].ToString() + "',N'" + vRand[3].ToString() + "',N'" + vRand[4].ToString() + "',N'" + vRand[5].ToString() + "'" + vInsertPartea2[0] + vInsertPartea2[1] + vInsertPartea2[2] + vInsertPartea2[3] + vInsertPartea2[4] + vInsertZ;
            i++;
        }
        vCmd.ExecuteNonQuery();
        ManipuleazaBD.InchideConexiune(vCon);
        ResponseHelper.Redirect("~/printCentralizatorCapitolul" + vCapitol + ".aspx?codCapitol=" + vCapitol + "&judet=" + ddlFJudet.SelectedValue, "_new", "");
    }
    protected void lbCapitol2b_Click(object sender, EventArgs e)
    {
        ResponseHelper.Redirect("~/printCentralizatorCapitolul2b.aspx?unitateId=" + ddlUnitate.SelectedValue + "&judet=" + ddlFJudet.SelectedValue, "_new","");
    }
    protected void lbCapitol2c_Click(object sender, EventArgs e)
    {
        ResponseHelper.Redirect("~/printCentralizatorCapitolul2c.aspx?unitateId=" + ddlUnitate.SelectedValue + "&judet=" + ddlFJudet.SelectedValue, "_new", "");
    }

    protected void lbCapitol5a_Click(object sender, EventArgs e)
    {
        // Centralizator capitolul 5a
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();

        StergeCapitolCentralizatorUtilizator(Convert.ToInt32(Session["SESan"]), Convert.ToInt32(Session["SESutilizatorId"]), "5a");
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        SqlCommand vCmd1 = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd1.Connection = vCon;
        vCmd.CommandText = @"SELECT * FROM  sabloaneCapitole WHERE (an = '" + Session["SESan"].ToString() + "') AND (capitol = '5a') order by codRand";
        SqlDataReader vTabelCapitole = vCmd.ExecuteReader();
        // facem o lista cu capitolele din sabloane
        List<List<string>> vListaCapitoleSabloane = new List<List<string>> { };
        while (vTabelCapitole.Read())
        {
            List<string> vListaX = new List<string> { };
            vListaX.Add(vTabelCapitole["codRand"].ToString());
            vListaX.Add(vTabelCapitole["denumire1"].ToString());
            vListaX.Add(vTabelCapitole["denumire2"].ToString());
            vListaX.Add(vTabelCapitole["denumire3"].ToString());
            vListaX.Add(vTabelCapitole["denumire4"].ToString());
            vListaX.Add(vTabelCapitole["denumire5"].ToString());
            string vDenumireRod = "", vDenumireTineri = "";
            if (vTabelCapitole["codRand"].ToString() == "1")
            {
                vDenumireRod = "Pe rod cod rând (03 + 15 + 25 + 27 + 29 + 31 + 33 + 35 + 37 + 39 + 41 + 43)";
                vDenumireTineri = "Tineri pe rod rând (04 + 16 + 26 + 28 + 30 + 32 + 34 + 36 + 38 + 40 + 42 + 44)";
            }
            else if (vTabelCapitole["codRand"].ToString() == "2")
            {
                vDenumireRod = "Pe rod (05 + 07 + 09 + 11 + 13";
                vDenumireTineri = "Tineri (06 + 08 + 10 + 12 + 14)";
            }
            else if (vTabelCapitole["codRand"].ToString() == "8")
            {
                vDenumireRod = "Pe rod (17 + 19 + 21 + 23)";
                vDenumireTineri = "Tineri (18 + 20 + 22 + 24)";
            }
            else
            {
                vDenumireRod = "Pe rod";
                vDenumireTineri = "Tineri";
            }
            vListaX.Add(vDenumireRod);
            vListaX.Add(vDenumireTineri);
            vListaCapitoleSabloane.Add(vListaX);
        }
        vTabelCapitole.Close();

        // scoatem anii din ciclu
        string[] vAni = CalculCapitole.vListaAni(Session["SESan"].ToString());

        // lista pe 5 ani
        List<List<string>> vColoaneListe1 = new List<List<string>> { };
        List<List<string>> vColoaneListe2 = new List<List<string>> { };
        List<List<string>> vColoaneListe3 = new List<List<string>> { };
        List<List<string>> vColoaneListe4 = new List<List<string>> { };
        List<List<string>> vColoaneListe5 = new List<List<string>> { };
        List<List<string>> vColoaneListe6 = new List<List<string>> { };
        List<List<string>> vColoaneListe7 = new List<List<string>> { };
        List<List<string>> vColoaneListe8 = new List<List<string>> { };
        foreach (string vAn in vAni)
        {
            // pt toate randurile, total localitate
            vCmd1.CommandText = "SELECT coalesce(SUM(capitoleCentralizate.col1),0) AS Expr1, capitoleCentralizate.codRand  FROM capitoleCentralizate INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + vAn + "') AND (capitoleCentralizate.codCapitol = '5a') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "')  AND unitati.unitatePrincipala <> '0'  GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";
            SqlDataReader vTabelCapitole1 = vCmd1.ExecuteReader();
            List<string> vColLista1 = new List<string> { };
            List<string> vColListaRand1 = new List<string> { };
            while (vTabelCapitole1.Read())
            {
                vColLista1.Add(vTabelCapitole1["Expr1"].ToString());
                vColListaRand1.Add(vTabelCapitole1["codRand"].ToString());
            }
            vTabelCapitole1.Close();
            vColLista1 = C2aUmpleLista(vColLista1, vColListaRand1, vListaCapitoleSabloane);

            // pt toate randurile, localnici
            vCmd1.CommandText = "SELECT coalesce(SUM(capitoleCentralizate.col1),0) AS Expr1, capitoleCentralizate.codRand  FROM capitoleCentralizate INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + vAn + "') AND (capitoleCentralizate.codCapitol = '5a') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (capitoleCentralizate.gospodarieTip = 0)  AND unitati.unitatePrincipala <> '0'  GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";
            vTabelCapitole1 = vCmd1.ExecuteReader();
            List<string> vColLista2 = new List<string> { };
            List<string> vColListaRand2 = new List<string> { };
            while (vTabelCapitole1.Read())
            {
                vColLista2.Add(vTabelCapitole1["Expr1"].ToString());
                vColListaRand2.Add(vTabelCapitole1["codRand"].ToString());
            }
            vTabelCapitole1.Close();
            vColLista2 = C2aUmpleLista(vColLista2, vColListaRand2, vListaCapitoleSabloane);

            // pt toate randurile, strainasi
            vCmd1.CommandText = "SELECT coalesce(SUM(capitoleCentralizate.col1),0) AS Expr1, capitoleCentralizate.codRand  FROM capitoleCentralizate INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + vAn + "') AND (capitoleCentralizate.codCapitol = '5a') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (capitoleCentralizate.gospodarieTip >= 2)  AND unitati.unitatePrincipala <> '0'  GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";
            vTabelCapitole1 = vCmd1.ExecuteReader();
            List<string> vColLista3 = new List<string> { };
            List<string> vColListaRand3 = new List<string> { };
            while (vTabelCapitole1.Read())
            {
                vColLista3.Add(vTabelCapitole1["Expr1"].ToString());
                vColListaRand3.Add(vTabelCapitole1["codRand"].ToString());
            }
            vTabelCapitole1.Close();
            vColLista3 = C2aUmpleLista(vColLista3, vColListaRand3, vListaCapitoleSabloane);
            // pt toate randurile, firme
            vCmd1.CommandText = "SELECT coalesce(SUM(capitoleCentralizate.col1),0) AS Expr1, capitoleCentralizate.codRand  FROM capitoleCentralizate INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + vAn + "') AND (capitoleCentralizate.codCapitol = '5a') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (capitoleCentralizate.gospodarieTip in (1,3))  AND unitati.unitatePrincipala <> '0'  GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";
            vTabelCapitole1 = vCmd1.ExecuteReader();
            List<string> vColLista4 = new List<string> { };
            List<string> vColListaRand4 = new List<string> { };
            while (vTabelCapitole1.Read())
            {
                vColLista4.Add(vTabelCapitole1["Expr1"].ToString());
                vColListaRand4.Add(vTabelCapitole1["codRand"].ToString());
            }
            vTabelCapitole1.Close();
            vColLista4 = C2aUmpleLista(vColLista4, vColListaRand4, vListaCapitoleSabloane);

            /****** tineri ***/
            // pt toate randurile, total localitate

            vCmd1.CommandText = "SELECT coalesce(SUM(capitoleCentralizate.col2),0) AS Expr1, capitoleCentralizate.codRand  FROM capitoleCentralizate INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + vAn + "') AND (capitoleCentralizate.codCapitol = '5a') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND unitati.unitatePrincipala <> '0' GROUP BY capitoleCentralizate.codRand   ORDER BY capitoleCentralizate.codRand";
            vTabelCapitole1 = vCmd1.ExecuteReader();
            List<string> vColLista5 = new List<string> { };
            List<string> vColListaRand5 = new List<string> { };
            while (vTabelCapitole1.Read())
            {
                vColLista5.Add(vTabelCapitole1["Expr1"].ToString());
                vColListaRand5.Add(vTabelCapitole1["codRand"].ToString());
            }
            vTabelCapitole1.Close();
            vColLista5 = C2aUmpleLista(vColLista5, vColListaRand5, vListaCapitoleSabloane);

            // pt toate randurile, localnici
            vCmd1.CommandText = "SELECT coalesce(SUM(capitoleCentralizate.col2),0) AS Expr1, capitoleCentralizate.codRand  FROM capitoleCentralizate INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + vAn + "') AND (capitoleCentralizate.codCapitol = '5a') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (capitoleCentralizate.gospodarieTip = 0)  AND unitati.unitatePrincipala <> '0'  GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";
            vTabelCapitole1 = vCmd1.ExecuteReader();
            List<string> vColLista6 = new List<string> { };
            List<string> vColListaRand6 = new List<string> { };
            while (vTabelCapitole1.Read())
            {
                vColLista6.Add(vTabelCapitole1["Expr1"].ToString());
                vColListaRand6.Add(vTabelCapitole1["codRand"].ToString());
            }
            vTabelCapitole1.Close();
            vColLista6 = C2aUmpleLista(vColLista6, vColListaRand6, vListaCapitoleSabloane);

            // pt toate randurile, strainasi
            vCmd1.CommandText = "SELECT coalesce(SUM(capitoleCentralizate.col2),0) AS Expr1, capitoleCentralizate.codRand  FROM capitoleCentralizate INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + vAn + "') AND (capitoleCentralizate.codCapitol = '5a') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (capitoleCentralizate.gospodarieTip >= 2)  AND unitati.unitatePrincipala <> '0'  GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";
            vTabelCapitole1 = vCmd1.ExecuteReader();
            List<string> vColLista7 = new List<string> { };
            List<string> vColListaRand7 = new List<string> { };
            while (vTabelCapitole1.Read())
            {
                vColLista7.Add(vTabelCapitole1["Expr1"].ToString());
                vColListaRand7.Add(vTabelCapitole1["codRand"].ToString());
            }
            vTabelCapitole1.Close();
            vColLista7 = C2aUmpleLista(vColLista7, vColListaRand7, vListaCapitoleSabloane);
            // pt toate randurile, firme
            vCmd1.CommandText = "SELECT coalesce(SUM(capitoleCentralizate.col2),0) AS Expr1, capitoleCentralizate.codRand  FROM capitoleCentralizate INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + vAn + "') AND (capitoleCentralizate.codCapitol = '5a') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (capitoleCentralizate.gospodarieTip  in (1,3))  AND unitati.unitatePrincipala <> '0'  GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";
            vTabelCapitole1 = vCmd1.ExecuteReader();
            List<string> vColLista8 = new List<string> { };
            List<string> vColListaRand8 = new List<string> { };
            while (vTabelCapitole1.Read())
            {
                vColLista8.Add(vTabelCapitole1["Expr1"].ToString());
                vColListaRand8.Add(vTabelCapitole1["codRand"].ToString());
            }
            vTabelCapitole1.Close();
            vColLista8 = C2aUmpleLista(vColLista8, vColListaRand8, vListaCapitoleSabloane);
            /******* final pomi tineri ******/
            vColoaneListe1.Add(vColLista1);
            vColoaneListe2.Add(vColLista2);
            vColoaneListe3.Add(vColLista3);
            vColoaneListe4.Add(vColLista4);
            vColoaneListe5.Add(vColLista5);
            vColoaneListe6.Add(vColLista6);
            vColoaneListe7.Add(vColLista7);
            vColoaneListe8.Add(vColLista8);
        }
        // facem stringul pt insert
        int vContor = 0;

        // scriem alternativ din 1-4 si 5-8
        int i = 0; int nr = 0;
        vCmd.CommandText = "";
        string vInsertX = "INSERT INTO [rapCapitole] ([utilizatorId], [capitol], [codRand], [denumire1], [denumire2], [denumire3], [denumire4], [denumire5] ";
        string vInsertY = ") VALUES (";
        string vInsertW = "'" + Convert.ToInt32(Session["SESutilizatorId"]) + "','5a', ";
        string vInsertZ = ");";

        List<string> vInsertPartea1 = new List<string> { };
        List<string> vInsertPartea2 = new List<string> { };
       
        int j = 0;
        foreach (string vAn in vAni)
        {
            vInsertPartea1.Add(", [an" + (j + 1).ToString() + "], [col" + (j + 1).ToString() + "_1], [col" + (j + 1).ToString() + "_2], [col" + (j + 1).ToString() + "_3], [col" + (j + 1).ToString() + "_4]");
            j++;
            vContor++;
        }
        vContor = 0;
        foreach (List<string> vRand in vListaCapitoleSabloane)
        {
            j = 0;
            vInsertPartea2 = new List<string>();
            // daca nu sunt "duzi" scriem pt rod si tineri
            if (vRand[0].ToString() != "23")
            {
                foreach (string vAn in vAni)
                {
                    vInsertPartea2.Add(", '" + vAn + "', '" + vColoaneListe1[j][i].ToString().Replace(",", ".") + "','" + vColoaneListe2[j][i].ToString().Replace(",", ".") + "','" + vColoaneListe3[j][i].ToString().Replace(",", ".") + "','" + vColoaneListe4[j][i].ToString().Replace(",", ".") + "'" );
                    vInsertPartea2.Add(", '" + vAn + "', '" + vColoaneListe5[j][i].ToString().Replace(",", ".") + "','" + vColoaneListe6[j][i].ToString().Replace(",", ".") + "','" + vColoaneListe7[j][i].ToString().Replace(",", ".") + "','" + vColoaneListe8[j][i].ToString().Replace(",", ".") + "'");
                    j++;
                }
                vContor++;
                  vCmd.CommandText += vInsertX + vInsertPartea1[0] + vInsertPartea1[1] + vInsertPartea1[2] + vInsertPartea1[3] + vInsertPartea1[4] + vInsertY + vInsertW + "'" + vContor.ToString() + "',N'" + vRand[1].ToString() + "',N'" + vRand[6].ToString() + "',N'" + vRand[3].ToString() + "',N'" + vRand[4].ToString() + "',N'" + vRand[5].ToString() + "'" + vInsertPartea2[0] + vInsertPartea2[2] + vInsertPartea2[4] + vInsertPartea2[6] + vInsertPartea2[8] + vInsertZ;
                  vContor++;
                  vCmd.CommandText += vInsertX + vInsertPartea1[0] + vInsertPartea1[1] + vInsertPartea1[2] + vInsertPartea1[3] + vInsertPartea1[4] + vInsertY + vInsertW + "'" + vContor.ToString() + "',N'',N'" + vRand[7].ToString() + "',N'" + vRand[3].ToString() + "',N'" + vRand[4].ToString() + "',N'" + vRand[5].ToString() + "'" + vInsertPartea2[1] + vInsertPartea2[3] + vInsertPartea2[5] + vInsertPartea2[7] + vInsertPartea2[9] + vInsertZ;
            }
            // sunt duzi, luam total
            else
            {
                foreach (string vAn in vAni)
                {
                    vInsertPartea2.Add(", '" + vAn + "', '" + 
                        (Convert.ToDecimal(vColoaneListe1[j][i].ToString().Replace(".", ",")) + Convert.ToDecimal(vColoaneListe5[j][i].ToString().Replace(".", ","))).ToString().Replace(',', '.') + "', '" + 
                        (Convert.ToDecimal(vColoaneListe2[j][i].ToString().Replace(".", ",")) + Convert.ToDecimal(vColoaneListe6[j][i].ToString().Replace(".", ","))).ToString().Replace(',', '.') + "', '" + 
                        (Convert.ToDecimal(vColoaneListe3[j][i].ToString().Replace(".", ",")) + Convert.ToDecimal(vColoaneListe7[j][i].ToString().Replace(".", ","))).ToString().Replace(',', '.') + "', '" + 
                        (Convert.ToDecimal(vColoaneListe4[j][i].ToString().Replace(".", ",")) + Convert.ToDecimal(vColoaneListe8[j][i].ToString().Replace(".", ","))).ToString().Replace(',', '.') + "'" );
                    j++;
                    //vContor++;
                }
                vContor++;
                vCmd.CommandText += vInsertX + vInsertPartea1[0] + vInsertPartea1[1] + vInsertPartea1[2] + vInsertPartea1[3] + vInsertPartea1[4] + vInsertY + vInsertW + "'" + vContor.ToString() + "',N'" + vRand[5].ToString() + "',N'',N'" + vRand[3].ToString() + "',N'" + vRand[4].ToString() + "',N'" + vRand[5].ToString() + "'" + vInsertPartea2[0] + vInsertPartea2[1] + vInsertPartea2[2] + vInsertPartea2[3] + vInsertPartea2[4] + vInsertZ;
                nr++;
            }
            i++;
        }
        vCmd.ExecuteNonQuery();
        ManipuleazaBD.InchideConexiune(vCon);
        ResponseHelper.Redirect("~/printCentralizatorCapitolul5a.aspx?codCapitol=5a", "_new", "");
    }

    protected void lbCapitol5b_Click(object sender, EventArgs e)
    {
        // Centralizator capitolul 5b
        StergeCapitolCentralizatorUtilizator(Convert.ToInt32(Session["SESan"]), Convert.ToInt32(Session["SESutilizatorId"]), "5b");
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        SqlCommand vCmd1 = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd1.Connection = vCon;
        vCmd.CommandText = @"SELECT * FROM  sabloaneCapitole WHERE (an = '" + Session["SESan"].ToString() + "') AND (capitol = '5b') order by codRand";
        SqlDataReader vTabelCapitole = vCmd.ExecuteReader();
        // facem o lista cu capitolele din sabloane
        List<List<string>> vListaCapitoleSabloane = new List<List<string>> { };
        while (vTabelCapitole.Read())
        {
            List<string> vListaX = new List<string> { };
            vListaX.Add(vTabelCapitole["codRand"].ToString());
            vListaX.Add(vTabelCapitole["denumire1"].ToString());
            vListaX.Add(vTabelCapitole["denumire2"].ToString());
            vListaX.Add(vTabelCapitole["denumire3"].ToString());
            vListaX.Add(vTabelCapitole["denumire4"].ToString());
            vListaX.Add(vTabelCapitole["denumire5"].ToString());
            string vDenumireRod = "", vDenumireTineri = "";
            if (vTabelCapitole["codRand"].ToString() == "1")
            {
                vDenumireRod = "Suprafaţa cod rând (03 + 15 + 25 + 27 + 29 + 31 + 33 + 35 + 37 + 39 + 41 + 43 + 47 + 49)";
                vDenumireTineri = "Nr. pomi cod rând (04 + 16 + 26 + 28 + 30 + 32 + 34 + 36 + 38 + 40 + 42 + 44 + 48 + 50)";
            }
            else if (vTabelCapitole["codRand"].ToString() == "2")
            {
                vDenumireRod = "Suprafaţa (05 + 07 + 09 + 11 + 13";
                vDenumireTineri = "Nr. pomi (06 + 08 + 10 + 12 + 14)";
            }
            else if (vTabelCapitole["codRand"].ToString() == "8")
            {
                vDenumireRod = "Suprafaţa (17 + 19 + 21 + 23)";
                vDenumireTineri = "Nr. pomi (18 + 20 + 22 + 24)";
            }
            else
            {
                vDenumireRod = "Suprafaţa";
                vDenumireTineri = "Nr. pomi";
            }
            vListaX.Add(vDenumireRod);
            vListaX.Add(vDenumireTineri);
            vListaCapitoleSabloane.Add(vListaX);
        }
        vTabelCapitole.Close();
        /** suprafete **/
        // pt toate randurile, total localitate
        vCmd1.CommandText = "SELECT  coalesce((SUM(capitoleCentralizate.col1) + (SUM(capitoleCentralizate.col2) - { fn MOD(SUM(capitoleCentralizate.col2), 100) }) / 100) + { fn MOD(SUM(capitoleCentralizate.col2), 100) } / 100,0) AS Expr1, capitoleCentralizate.codRand  FROM capitoleCentralizate INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + Session["SESan"].ToString() + "') AND (capitoleCentralizate.codCapitol = '5b') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "')  GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";
        SqlDataReader vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista1 = new List<string> { };
        List<string> vColListaRand1 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista1.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand1.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista1 = C2aUmpleLista(vColLista1, vColListaRand1, vListaCapitoleSabloane);
        // pt toate randurile, total localnici

        vCmd1.CommandText = "SELECT  coalesce((SUM(capitoleCentralizate.col1) + (SUM(capitoleCentralizate.col2) - { fn MOD(SUM(capitoleCentralizate.col2), 100) }) / 100) + { fn MOD(SUM(capitoleCentralizate.col2), 100) } / 100,0) AS Expr1, capitoleCentralizate.codRand  FROM capitoleCentralizate INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + Session["SESan"].ToString() + "') AND (capitoleCentralizate.codCapitol = '5b') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "')  AND (capitoleCentralizate.gospodarieTip = 0)  GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista2 = new List<string> { };
        List<string> vColListaRand2 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista2.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand2.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista2 = C2aUmpleLista(vColLista2, vColListaRand2, vListaCapitoleSabloane);
        // pt toate randurile, strainasi
        vCmd1.CommandText = "SELECT  coalesce((SUM(capitoleCentralizate.col1) + (SUM(capitoleCentralizate.col2) - { fn MOD(SUM(capitoleCentralizate.col2), 100) }) / 100) + { fn MOD(SUM(capitoleCentralizate.col2), 100) } / 100,0) AS Expr1, capitoleCentralizate.codRand  FROM capitoleCentralizate INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + Session["SESan"].ToString() + "') AND (capitoleCentralizate.codCapitol = '5b') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "')  AND (capitoleCentralizate.gospodarieTip >=2)  GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista3 = new List<string> { };
        List<string> vColListaRand3 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista3.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand3.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista3 = C2aUmpleLista(vColLista3, vColListaRand3, vListaCapitoleSabloane);
        // pt toate randurile, firme
        vCmd1.CommandText = "SELECT  coalesce((SUM(capitoleCentralizate.col1) + (SUM(capitoleCentralizate.col2) - { fn MOD(SUM(capitoleCentralizate.col2), 100) }) / 100) + { fn MOD(SUM(capitoleCentralizate.col2), 100) } / 100,0) AS Expr1, capitoleCentralizate.codRand  FROM capitoleCentralizate INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + Session["SESan"].ToString() + "') AND (capitoleCentralizate.codCapitol = '5b') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "')  AND (capitoleCentralizate.gospodarieTip in (1,3))  GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista4 = new List<string> { };
        List<string> vColListaRand4 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista4.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand4.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista4 = C2aUmpleLista(vColLista4, vColListaRand4, vListaCapitoleSabloane);


        /* numar pomi */
        // pt toate randurile, total localitate
        vCmd1.CommandText = "SELECT coalesce(SUM(capitoleCentralizate.col3),0) AS Expr1, capitoleCentralizate.codRand   FROM capitoleCentralizate INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + Session["SESan"].ToString() + "') AND (capitoleCentralizate.codCapitol = '5b') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "')   GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista5 = new List<string> { };
        List<string> vColListaRand5 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista5.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand5.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista5 = C2aUmpleLista(vColLista5, vColListaRand5, vListaCapitoleSabloane);
        // pt toate randurile, total localnici
        vCmd1.CommandText = "SELECT coalesce(SUM(capitoleCentralizate.col3),0) AS Expr1, capitoleCentralizate.codRand   FROM capitoleCentralizate INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + Session["SESan"].ToString() + "') AND (capitoleCentralizate.codCapitol = '5b') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "')  AND (capitoleCentralizate.gospodarieTip = 0)   GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";

        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista6 = new List<string> { };
        List<string> vColListaRand6 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista6.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand6.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista6 = C2aUmpleLista(vColLista6, vColListaRand6, vListaCapitoleSabloane);
        // pt toate randurile, strainasi
        vCmd1.CommandText = "SELECT coalesce(SUM(capitoleCentralizate.col3),0) AS Expr1, capitoleCentralizate.codRand   FROM capitoleCentralizate INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + Session["SESan"].ToString() + "') AND (capitoleCentralizate.codCapitol = '5b') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "')  AND (capitoleCentralizate.gospodarieTip >=2 )   GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista7 = new List<string> { };
        List<string> vColListaRand7 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista7.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand7.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista7 = C2aUmpleLista(vColLista7, vColListaRand7, vListaCapitoleSabloane);
        // pt toate randurile, firme
        vCmd1.CommandText = "SELECT coalesce(SUM(capitoleCentralizate.col3),0) AS Expr1, capitoleCentralizate.codRand   FROM capitoleCentralizate INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + Session["SESan"].ToString() + "') AND (capitoleCentralizate.codCapitol = '5b') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "')  AND (capitoleCentralizate.gospodarieTip in (1,3))   GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista8 = new List<string> { };
        List<string> vColListaRand8 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista8.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand8.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista8 = C2aUmpleLista(vColLista8, vColListaRand8, vListaCapitoleSabloane);

        // facem stringul pt insert
        // scriem alternativ din 1-4 si 5-8
        int i = 0; int nr = 0;
        vCmd.CommandText = "";
        foreach (List<string> vRand in vListaCapitoleSabloane)
        {
            // daca nu sunt "27" scriem pt rod si tineri
            if (vRand[0].ToString() != "24" && vRand[0].ToString() != "27")
            {
                nr++;
                vCmd.CommandText += @"INSERT INTO [rapCentralizatoare] ([utilizatorId], [an], [capitol], [codRand], [denumire1], [denumire2], [denumire3], [denumire4], [denumire5], [col1], [col2], [col3], [col4], [col5], [col6], [col7], [col8]) VALUES ('" + Convert.ToInt32(Session["SESutilizatorId"]) + "','" + Convert.ToInt32(Session["SESan"]) + "','5b','" + nr.ToString() + "',N'" + vRand[1].ToString() + "',N'" + vRand[6].ToString() + "',N'" + vRand[3].ToString() + "',N'" + vRand[4].ToString() + "',N'" + vRand[5].ToString() + "','" + vColLista1[i].ToString().Replace(",", ".") + "','" + vColLista2[i].ToString().Replace(",", ".") + "','" + vColLista3[i].ToString().Replace(",", ".") + "','" + vColLista4[i].ToString().Replace(",", ".") + "','0','0','0','0'); ";
                nr++;
                vCmd.CommandText += @"INSERT INTO [rapCentralizatoare] ([utilizatorId], [an], [capitol], [codRand], [denumire1], [denumire2], [denumire3], [denumire4], [denumire5], [col1], [col2], [col3], [col4], [col5], [col6], [col7], [col8]) VALUES ('" + Convert.ToInt32(Session["SESutilizatorId"]) + "','" + Convert.ToInt32(Session["SESan"]) + "','5b','" + nr.ToString() + "',N'',N'" + vRand[7].ToString() + "',N'" + vRand[3].ToString() + "',N'" + vRand[4].ToString() + "',N'" + vRand[5].ToString() + "','" + vColLista5[i].ToString().Replace(",", ".") + "','" + vColLista6[i].ToString().Replace(",", ".") + "','" + vColLista7[i].ToString().Replace(",", ".") + "','" + vColLista8[i].ToString().Replace(",", ".") + "','0','0','0','0'); ";
            }
            // sunt exceptii scriem altceva
            else
            {
                nr++;
                nr++;
                vCmd.CommandText += @"INSERT INTO [rapCentralizatoare] ([utilizatorId], [an], [capitol], [codRand], [denumire1], [denumire2], [denumire3], [denumire4], [denumire5], [col1], [col2], [col3], [col4], [col5], [col6], [col7], [col8]) VALUES ('" + Convert.ToInt32(Session["SESutilizatorId"]) + "','" + Convert.ToInt32(Session["SESan"]) + "','5b','" + nr.ToString() + "',N'" + vRand[1].ToString() + "',N'" + vRand[6].ToString() + "',N'" + vRand[3].ToString() + "',N'" + vRand[4].ToString() + "',N'" + vRand[5].ToString() + "','" + vColLista1[i].ToString().Replace(",", ".") + "','" + vColLista2[i].ToString().Replace(",", ".") + "','" + vColLista3[i].ToString().Replace(",", ".") + "','" + vColLista4[i].ToString().Replace(",", ".") + "','0','0','0','0'); ";
            }
            i++;
        }
        vCmd.ExecuteNonQuery();
        ManipuleazaBD.InchideConexiune(vCon);
        ResponseHelper.Redirect("~/printCentralizatorCapitolul5b.aspx?codCapitol=5b", "_new", "");
    }
    protected void lbCapitol5c_Click(object sender, EventArgs e)
    {
        lbCapitol2a_3_Click("5c");
    }
    protected void lbCapitol5d_Click(object sender, EventArgs e)
    {
        lbCapitol2a_3_Click("5d");
    }
    protected void lbCapitol6_Click(object sender, EventArgs e)
    {
        // Centralizator capitolul 6
        StergeCapitolCentralizatorUtilizator(Convert.ToInt32(Session["SESan"]), Convert.ToInt32(Session["SESutilizatorId"]), "6");
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        SqlCommand vCmd1 = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd1.Connection = vCon;
        vCmd.CommandText = @"SELECT * FROM  sabloaneCapitole WHERE (an = '" + Session["SESan"].ToString() + "') AND (capitol = '6') order by codRand";
        SqlDataReader vTabelCapitole = vCmd.ExecuteReader();

        // facem o lista cu capitolele din sabloane
        List<List<string>> vListaCapitoleSabloane = new List<List<string>> { };
        while (vTabelCapitole.Read())
        {
            List<string> vListaX = new List<string> { };
            vListaX.Add(vTabelCapitole["codRand"].ToString());
            vListaX.Add(vTabelCapitole["denumire1"].ToString());
            vListaX.Add(vTabelCapitole["denumire2"].ToString());
            vListaX.Add(vTabelCapitole["denumire3"].ToString());
            vListaX.Add(vTabelCapitole["denumire4"].ToString());
            vListaX.Add(vTabelCapitole["denumire5"].ToString());
            vListaCapitoleSabloane.Add(vListaX);
        }
        vTabelCapitole.Close();


        // pt toate randurile, 
        vCmd1.CommandText = "SELECT CONVERT(int, coalesce(SUM(capitoleCentralizate.col1),0) + coalesce((SUM(capitoleCentralizate.col2) - { fn MOD(SUM(capitoleCentralizate.col2), 100) }) / 100,0)) AS ha, capitoleCentralizate.codRand FROM capitoleCentralizate INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + Session["SESan"].ToString() + "') AND (capitoleCentralizate.codCapitol = '6') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";
        SqlDataReader vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista1 = new List<string> { };
        List<string> vColListaRand1 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista1.Add(vTabelCapitole1["ha"].ToString());
            vColListaRand1.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista1 = C2aUmpleLista(vColLista1, vColListaRand1, vListaCapitoleSabloane);

        // pt toate randurile, 
        vCmd1.CommandText = "SELECT coalesce(convert(decimal(18,2),{ fn MOD(SUM(capitoleCentralizate.col2), 100) }),0) as ari, capitoleCentralizate.codRand   FROM capitoleCentralizate INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + Session["SESan"].ToString() + "') AND (capitoleCentralizate.codCapitol = '6') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista2 = new List<string> { };
        List<string> vColListaRand2 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista2.Add(vTabelCapitole1["ari"].ToString());
            vColListaRand2.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista2 = C2aUmpleLista(vColLista2, vColListaRand2, vListaCapitoleSabloane);

        // facem stringul pt insert
        int i = 0;
        vCmd.CommandText = "";
        foreach (List<string> vRand in vListaCapitoleSabloane)
        {
            vCmd.CommandText += @"INSERT INTO [rapCentralizatoare] ([utilizatorId], [an], [capitol], [codRand], [denumire1], [denumire2], [denumire3], [denumire4], [denumire5], [col1], [col2], [col3], [col4], [col5], [col6], [col7], [col8]) VALUES ('" + Convert.ToInt32(Session["SESutilizatorId"]) + "','" + Convert.ToInt32(Session["SESan"]) + "','6','" + Convert.ToInt32(vRand[0].ToString()) + "',N'" + vRand[1].ToString() + "',N'" + vRand[2].ToString() + "',N'" + vRand[3].ToString() + "',N'" + vRand[4].ToString() + "',N'" + vRand[5].ToString() + "','" + vColLista1[i].ToString().Replace(",", ".") + "','" + vColLista2[i].ToString().Replace(",", ".") + "','0','0','0','0','0','0'); ";
            i++;
        }
        vCmd.ExecuteNonQuery();
        ManipuleazaBD.InchideConexiune(vCon);
        ResponseHelper.Redirect("~/printCentralizatorCapitolul6.aspx?codCapitol=6", "_new", "");
    }
    protected void lbCapitol7_Click(object sender, EventArgs e)
    {
        //***** apelam doar functia cap7_8
        lbCapitol7_8_Click("7");
    }
    protected void lbCapitol8_Click(object sender, EventArgs e)
    {
        //***** apelam doar functia cap7_8
        lbCapitol7_8_Click("8");
    }
    protected void lbCapitol7_8_Click(string vCapitol)
    {
        // Centralizator capitolul 7+8
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();

        StergeCapitolCentralizatorUtilizator(Convert.ToInt32(Session["SESan"]), Convert.ToInt32(Session["SESutilizatorId"]), vCapitol);


        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));

        SqlCommand vCmd = new SqlCommand();
        SqlCommand vCmd1 = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd1.Connection = vCon;
        vCmd.CommandText = @"SELECT * FROM  sabloaneCapitole WHERE (capitol = '" + vCapitol + "') AND (an='" + Session["SESan"].ToString() + "') order by codRand";
        SqlDataReader vTabelCapitole = vCmd.ExecuteReader();
        // facem o lista cu capitolele din sabloane
        List<List<string>> vListaCapitoleSabloane = new List<List<string>> { };
        while (vTabelCapitole.Read())
        {
            List<string> vListaX = new List<string> { };
            vListaX.Add(vTabelCapitole["codRand"].ToString());
            vListaX.Add(vTabelCapitole["denumire1"].ToString());
            vListaX.Add(vTabelCapitole["denumire2"].ToString());
            vListaX.Add(vTabelCapitole["denumire3"].ToString());
            vListaX.Add(vTabelCapitole["denumire4"].ToString());
            vListaX.Add(vTabelCapitole["denumire5"].ToString());
            vListaCapitoleSabloane.Add(vListaX);
        }
        vTabelCapitole.Close();

        // scoatem anii din ciclu
        string[] vAni = CalculCapitole.vListaAni(Session["SESan"].ToString());

        // lista pe 5 ani
        List<List<string>> vColoaneListe1 = new List<List<string>> { };
        List<List<string>> vColoaneListe2 = new List<List<string>> { };
        List<List<string>> vColoaneListe3 = new List<List<string>> { };
        List<List<string>> vColoaneListe4 = new List<List<string>> { }; 
        List<List<string>> vColoaneListe5 = new List<List<string>> { };
        List<List<string>> vColoaneListe6 = new List<List<string>> { };
        List<List<string>> vColoaneListe7 = new List<List<string>> { };
        List<List<string>> vColoaneListe8 = new List<List<string>> { };
        foreach (string vAn in vAni)
        {
            // pt toate randurile, toate tipurile de gospodarii
            // pt toate randurile, toate tipurile de gospodarii, sem1
            vCmd1.CommandText = "SELECT coalesce(SUM(capitoleCentralizate.col1),0) AS Expr1, capitoleCentralizate.codRand FROM capitoleCentralizate INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + vAn + "') AND (capitoleCentralizate.codCapitol = '" + vCapitol + "') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND unitati.unitatePrincipala <> '0'  GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";

            SqlDataReader vTabelCapitole1 = vCmd1.ExecuteReader();
            List<string> vColLista1 = new List<string> { };
            List<string> vColListaRand1 = new List<string> { };
            while (vTabelCapitole1.Read())
            {
                vColLista1.Add(vTabelCapitole1["Expr1"].ToString());
                vColListaRand1.Add(vTabelCapitole1["codRand"].ToString());
            }
            vTabelCapitole1.Close();
            vColLista1 = C2aUmpleLista(vColLista1, vColListaRand1, vListaCapitoleSabloane);
            // pt toate randurile, toate tipurile de gospodarii sem 2
            vCmd1.CommandText = "SELECT coalesce(SUM(capitoleCentralizate.col2),0) AS Expr1, capitoleCentralizate.codRand FROM capitoleCentralizate INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + vAn + "') AND (capitoleCentralizate.codCapitol = '" + vCapitol + "') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND unitati.unitatePrincipala <> '0'  GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";
            vTabelCapitole1 = vCmd1.ExecuteReader();
            List<string> vColLista2 = new List<string> { };
            List<string> vColListaRand2 = new List<string> { };
            while (vTabelCapitole1.Read())
            {
                vColLista2.Add(vTabelCapitole1["Expr1"].ToString());
                vColListaRand2.Add(vTabelCapitole1["codRand"].ToString());
            }
            vTabelCapitole1.Close();
            vColLista2 = C2aUmpleLista(vColLista2, vColListaRand2, vListaCapitoleSabloane);
            /***************
             * localnici fizice - 0
             * localnici firme - 1
             * strainasi fizice - 2
             * strainasi firme - 3
             * */
            // pt toate randurile,  tipurile de gospodarii - localnici - sem 1
            vCmd1.CommandText = "SELECT coalesce(SUM(capitoleCentralizate.col1),0) AS Expr1, capitoleCentralizate.codRand FROM capitoleCentralizate INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + vAn + "') AND (capitoleCentralizate.codCapitol = '" + vCapitol + "') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (capitoleCentralizate.gospodarieTip = 0)  AND unitati.unitatePrincipala <> '0' GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";
            vTabelCapitole1 = vCmd1.ExecuteReader();
            List<string> vColLista3 = new List<string> { };
            List<string> vColListaRand3 = new List<string> { };
            while (vTabelCapitole1.Read())
            {
                vColLista3.Add(vTabelCapitole1["Expr1"].ToString());
                vColListaRand3.Add(vTabelCapitole1["codRand"].ToString());
            }
            vTabelCapitole1.Close();
            vColLista3 = C2aUmpleLista(vColLista3, vColListaRand3, vListaCapitoleSabloane);
            // pt toate randurile,  tipurile de gospodarii - localnici - sem 2
            vCmd1.CommandText = "SELECT coalesce(SUM(capitoleCentralizate.col2),0) AS Expr1, capitoleCentralizate.codRand FROM capitoleCentralizate  INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + vAn + "') AND (capitoleCentralizate.codCapitol = '" + vCapitol + "') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (capitoleCentralizate.gospodarieTip = 0)  AND unitati.unitatePrincipala <> '0'  GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";
            vTabelCapitole1 = vCmd1.ExecuteReader();
            List<string> vColLista4 = new List<string> { };
            List<string> vColListaRand4 = new List<string> { };
            while (vTabelCapitole1.Read())
            {
                vColLista4.Add(vTabelCapitole1["Expr1"].ToString());
                vColListaRand4.Add(vTabelCapitole1["codRand"].ToString());
            }
            vTabelCapitole1.Close();
            vColLista4 = C2aUmpleLista(vColLista4, vColListaRand4, vListaCapitoleSabloane);
            // pt toate randurile,  tipurile de gospodarii - strainasi - sem 1
            vCmd1.CommandText = "SELECT coalesce(SUM(capitoleCentralizate.col1),0) AS Expr1, capitoleCentralizate.codRand FROM capitoleCentralizate  INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + vAn + "') AND (capitoleCentralizate.codCapitol = '" + vCapitol + "') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (capitoleCentralizate.gospodarieTip >= 2) AND unitati.unitatePrincipala <> '0'   GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";
            vTabelCapitole1 = vCmd1.ExecuteReader();
            List<string> vColLista5 = new List<string> { };
            List<string> vColListaRand5 = new List<string> { };
            while (vTabelCapitole1.Read())
            {
                vColLista5.Add(vTabelCapitole1["Expr1"].ToString());
                vColListaRand5.Add(vTabelCapitole1["codRand"].ToString());
            }
            vTabelCapitole1.Close();
            vColLista5 = C2aUmpleLista(vColLista5, vColListaRand5, vListaCapitoleSabloane);
            // pt toate randurile,  tipurile de gospodarii - strainasi - sem 2
            vCmd1.CommandText = "SELECT coalesce(SUM(capitoleCentralizate.col2),0) AS Expr1, capitoleCentralizate.codRand FROM capitoleCentralizate  INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + vAn + "') AND (capitoleCentralizate.codCapitol = '" + vCapitol + "') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (capitoleCentralizate.gospodarieTip >= 2)  AND unitati.unitatePrincipala <> '0'  GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";
            vTabelCapitole1 = vCmd1.ExecuteReader();
            List<string> vColLista6 = new List<string> { };
            List<string> vColListaRand6 = new List<string> { };
            while (vTabelCapitole1.Read())
            {
                vColLista6.Add(vTabelCapitole1["Expr1"].ToString());
                vColListaRand6.Add(vTabelCapitole1["codRand"].ToString());
            }
            vTabelCapitole1.Close();
            vColLista6 = C2aUmpleLista(vColLista6, vColListaRand6, vListaCapitoleSabloane);

            // pt toate randurile,  tipurile de gospodarii - firme - sem 1
            vCmd1.CommandText = "SELECT coalesce(SUM(capitoleCentralizate.col1),0) AS Expr1, capitoleCentralizate.codRand FROM capitoleCentralizate  INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + vAn + "') AND (capitoleCentralizate.codCapitol = '" + vCapitol + "') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (capitoleCentralizate.gospodarieTip = 1 OR capitoleCentralizate.gospodarieTip = 3 )  AND unitati.unitatePrincipala <> '0'  GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";
            vTabelCapitole1 = vCmd1.ExecuteReader();
            List<string> vColLista7 = new List<string> { };
            List<string> vColListaRand7 = new List<string> { };
            while (vTabelCapitole1.Read())
            {
                vColLista7.Add(vTabelCapitole1["Expr1"].ToString());
                vColListaRand7.Add(vTabelCapitole1["codRand"].ToString());
            }
            vTabelCapitole1.Close();
            vColLista7 = C2aUmpleLista(vColLista7, vColListaRand7, vListaCapitoleSabloane);

            // pt toate randurile,  tipurile de gospodarii - firme - sem 2
            vCmd1.CommandText = "SELECT coalesce(SUM(capitoleCentralizate.col2),0) AS Expr1, capitoleCentralizate.codRand FROM capitoleCentralizate  INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + vAn + "') AND (capitoleCentralizate.codCapitol = '" + vCapitol + "') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (capitoleCentralizate.gospodarieTip = 1 OR capitoleCentralizate.gospodarieTip = 3 )  AND unitati.unitatePrincipala <> '0'  GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";
            vTabelCapitole1 = vCmd1.ExecuteReader();
            List<string> vColLista8 = new List<string> { };
            List<string> vColListaRand8 = new List<string> { };
            while (vTabelCapitole1.Read())
            {
                vColLista8.Add(vTabelCapitole1["Expr1"].ToString());
                vColListaRand8.Add(vTabelCapitole1["codRand"].ToString());
            }
            vTabelCapitole1.Close();
            vColLista8 = C2aUmpleLista(vColLista8, vColListaRand8, vListaCapitoleSabloane);

            vColoaneListe1.Add(vColLista1);
            vColoaneListe2.Add(vColLista2);
            vColoaneListe3.Add(vColLista3);
            vColoaneListe4.Add(vColLista4);
            vColoaneListe5.Add(vColLista5);
            vColoaneListe6.Add(vColLista6);
            vColoaneListe7.Add(vColLista7);
            vColoaneListe8.Add(vColLista8);
        }

        int vContor = 0;

        // facem stringul pt insert

        string vInsertX = "INSERT INTO [rapCapitole] ([utilizatorId], [capitol], [codRand], [denumire1], [denumire2], [denumire3], [denumire4], [denumire5] ";
        string vInsertY = ") VALUES (";
        string vInsertW = "'" + Convert.ToInt32(Session["SESutilizatorId"]) + "','" + vCapitol + "', ";
        string vInsertZ = ");";

        List<string> vInsertPartea1 = new List<string> { };
        List<string> vInsertPartea2 = new List<string> { };
        int i = 0;
        int j = 0;
        foreach (string vAn in vAni)
        {
            vInsertPartea1.Add(", [an" + (j + 1).ToString() + "], [col" + (j + 1).ToString() + "_1], [col" + (j + 1).ToString() + "_2], [col" + (j + 1).ToString() + "_3], [col" + (j + 1).ToString() + "_4], [col" + (j + 1).ToString() + "_5], [col" + (j + 1).ToString() + "_6], [col" + (j + 1).ToString() + "_7], [col" + (j + 1).ToString() + "_8]");
            j++;
            vContor++;
        }

        vCmd.CommandText = "";
        foreach (List<string> vRand in vListaCapitoleSabloane)
        {
            j = 0;
            vInsertPartea2 = new List<string>();
            foreach (string vAn in vAni)
            {
                vInsertPartea2.Add(", '" + vAn + "', '" + vColoaneListe1[j][i].ToString().Replace(",", ".") + "','" + vColoaneListe2[j][i].ToString().Replace(",", ".") + "','" + vColoaneListe3[j][i].ToString().Replace(",", ".") + "','" + vColoaneListe4[j][i].ToString().Replace(",", ".") + "', '" + vColoaneListe5[j][i].ToString().Replace(",", ".") + "','" + vColoaneListe6[j][i].ToString().Replace(",", ".") + "','" + vColoaneListe7[j][i].ToString().Replace(",", ".") + "','" + vColoaneListe8[j][i].ToString().Replace(",", ".") + "'" );
                j++;
                vContor++;
            }

            vCmd.CommandText += vInsertX + vInsertPartea1[0] + vInsertPartea1[1] + vInsertPartea1[2] + vInsertPartea1[3] + vInsertPartea1[4] + vInsertY + vInsertW + "'" + Convert.ToInt32(vRand[0].ToString()) + "',N'" + vRand[1].ToString() + "',N'" + vRand[2].ToString() + "',N'" + vRand[3].ToString() + "',N'" + vRand[4].ToString() + "',N'" + vRand[5].ToString() + "'" + vInsertPartea2[0] + vInsertPartea2[1] + vInsertPartea2[2] + vInsertPartea2[3] + vInsertPartea2[4] + vInsertZ;
            i++;
        }
        vCmd.ExecuteNonQuery();
        ManipuleazaBD.InchideConexiune(vCon);
        ResponseHelper.Redirect("~/printCentralizatorCapitolul" + vCapitol + ".aspx?codCapitol=" + vCapitol + "&judet=" + ddlFJudet.SelectedValue, "_new", "");
    }

    protected void lbCapitol9_Click(object sender, EventArgs e)
    {
        lbCapitol2a_3_Click("9");
    }
    protected void lbCapitol10a_Click(object sender, EventArgs e)
    {
        lbCapitol7_8_Click("10a");
    }
    protected void lbCapitol10b_Click(string pCapitol)
    {
        string col1 = "1";
        string col2 = "2";
        string vCapitol = "10b" + pCapitol;
        switch (pCapitol)
        {
            case "1":
                col1 = "1";
                col2 = "2";
                break;
            case "2":
                col1 = "3";
                col2 = "4";
                break;
            case "3":
                col1 = "5";
                col2 = "6";
                break;
            case "4":
                col1 = "7";
                col2 = "8";
                break;
        }
        // Centralizator capitolul 10b - toate 4
        StergeCapitolCentralizatorUtilizator(Convert.ToInt32(Session["SESan"]), Convert.ToInt32(Session["SESutilizatorId"]), "10b");
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        SqlCommand vCmd1 = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd1.Connection = vCon;
        vCmd.CommandText = @"SELECT * FROM  sabloaneCapitole WHERE (an = '" + Session["SESan"].ToString() + "') AND (capitol = '10b') order by codRand";
        SqlDataReader vTabelCapitole = vCmd.ExecuteReader();
        // facem o lista cu capitolele din sabloane
        List<List<string>> vListaCapitoleSabloane = new List<List<string>> { };
        while (vTabelCapitole.Read())
        {
            List<string> vListaX = new List<string> { };
            vListaX.Add(vTabelCapitole["codRand"].ToString());
            vListaX.Add(vTabelCapitole["denumire1"].ToString());
            vListaX.Add(vTabelCapitole["denumire2"].ToString());
            vListaX.Add(vTabelCapitole["denumire3"].ToString());
            vListaX.Add(vTabelCapitole["denumire4"].ToString());
            vListaX.Add(vTabelCapitole["denumire5"].ToString());
            vListaCapitoleSabloane.Add(vListaX);
        }
        vTabelCapitole.Close();
        // pt toate randurile, toate tipurile de gospodarii, sem1
        vCmd1.CommandText = "SELECT coalesce(SUM(capitoleCentralizate.col" + col1 + "),0) AS Expr1, capitoleCentralizate.codRand FROM capitoleCentralizate INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + Session["SESan"].ToString() + "') AND (capitoleCentralizate.codCapitol = '10b') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";
        SqlDataReader vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista1 = new List<string> { };
        List<string> vColListaRand1 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista1.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand1.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista1 = C2aUmpleLista(vColLista1, vColListaRand1, vListaCapitoleSabloane);
        // pt toate randurile, toate tipurile de gospodarii sem 2
        vCmd1.CommandText = "SELECT coalesce(SUM(capitoleCentralizate.col" + col2 + "),0) AS Expr1, capitoleCentralizate.codRand FROM capitoleCentralizate INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + Session["SESan"].ToString() + "') AND (capitoleCentralizate.codCapitol = '10b') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista2 = new List<string> { };
        List<string> vColListaRand2 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista2.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand2.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista2 = C2aUmpleLista(vColLista2, vColListaRand2, vListaCapitoleSabloane);

        // pt toate randurile,  tipurile de gospodarii - localnici - sem 1
        vCmd1.CommandText = "SELECT coalesce(SUM(capitoleCentralizate.col" + col1 + "),0) AS Expr1, capitoleCentralizate.codRand FROM capitoleCentralizate INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + Session["SESan"].ToString() + "') AND (capitoleCentralizate.codCapitol = '10b') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (capitoleCentralizate.gospodarieTip = 0) GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista3 = new List<string> { };
        List<string> vColListaRand3 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista3.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand3.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista3 = C2aUmpleLista(vColLista3, vColListaRand3, vListaCapitoleSabloane);
        // pt toate randurile,  tipurile de gospodarii - localnici - sem 2
        vCmd1.CommandText = "SELECT coalesce(SUM(capitoleCentralizate.col" + col2 + "),0) AS Expr1, capitoleCentralizate.codRand FROM capitoleCentralizate INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + Session["SESan"].ToString() + "') AND (capitoleCentralizate.codCapitol = '10b') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (capitoleCentralizate.gospodarieTip = 0) GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista4 = new List<string> { };
        List<string> vColListaRand4 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista4.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand4.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista4 = C2aUmpleLista(vColLista4, vColListaRand4, vListaCapitoleSabloane);
        // pt toate randurile,  tipurile de gospodarii - strainasi - sem 1
        vCmd1.CommandText = "SELECT coalesce(SUM(capitoleCentralizate.col" + col1 + "),0) AS Expr1, capitoleCentralizate.codRand FROM capitoleCentralizate INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + Session["SESan"].ToString() + "') AND (capitoleCentralizate.codCapitol = '10b') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (capitoleCentralizate.gospodarieTip >= 2) GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista5 = new List<string> { };
        List<string> vColListaRand5 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista5.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand5.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista5 = C2aUmpleLista(vColLista5, vColListaRand5, vListaCapitoleSabloane);

        // pt toate randurile,  tipurile de gospodarii - strainasi - sem 2
        vCmd1.CommandText = "SELECT coalesce(SUM(capitoleCentralizate.col" + col2 + "),0) AS Expr1, capitoleCentralizate.codRand FROM capitoleCentralizate INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + Session["SESan"].ToString() + "') AND (capitoleCentralizate.codCapitol = '10b') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (capitoleCentralizate.gospodarieTip >= 2) GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista6 = new List<string> { };
        List<string> vColListaRand6 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista6.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand6.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista6 = C2aUmpleLista(vColLista6, vColListaRand6, vListaCapitoleSabloane);

        // pt toate randurile,  tipurile de gospodarii - firme - sem 1
        vCmd1.CommandText = "SELECT coalesce(SUM(capitoleCentralizate.col" + col1 + "),0) AS Expr1, capitoleCentralizate.codRand FROM capitoleCentralizate INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + Session["SESan"].ToString() + "') AND (capitoleCentralizate.codCapitol = '10b') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (capitoleCentralizate.gospodarieTip in (1,3)) GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista7 = new List<string> { };
        List<string> vColListaRand7 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista7.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand7.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista7 = C2aUmpleLista(vColLista7, vColListaRand7, vListaCapitoleSabloane);

        // pt toate randurile,  tipurile de gospodarii - firme - sem 2
        vCmd1.CommandText = "SELECT coalesce(SUM(capitoleCentralizate.col" + col2 + "),0) AS Expr1, capitoleCentralizate.codRand FROM capitoleCentralizate INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + Session["SESan"].ToString() + "') AND (capitoleCentralizate.codCapitol = '10b') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (capitoleCentralizate.gospodarieTip in (1,3)) GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista8 = new List<string> { };
        List<string> vColListaRand8 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista8.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand8.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista8 = C2aUmpleLista(vColLista8, vColListaRand8, vListaCapitoleSabloane);
        // facem stringul pt insert
        int i = 0;
        vCmd.CommandText = "";
        foreach (List<string> vRand in vListaCapitoleSabloane)
        {
            vCmd.CommandText += @"INSERT INTO [rapCentralizatoare] ([utilizatorId], [an], [capitol], [codRand], [denumire1], [denumire2], [denumire3], [denumire4], [denumire5], [col1], [col2], [col3], [col4], [col5], [col6], [col7], [col8]) VALUES ('" + Convert.ToInt32(Session["SESutilizatorId"]) + "','" + Convert.ToInt32(Session["SESan"]) + "','10b','" + Convert.ToInt32(vRand[0].ToString()) + "',N'" + vRand[1].ToString() + "',N'" + vRand[2].ToString() + "',N'" + vRand[3].ToString() + "',N'" + vRand[4].ToString() + "',N'" + vRand[5].ToString() + "','" + vColLista1[i].ToString().Replace(",", ".") + "','" + vColLista2[i].ToString().Replace(",", ".") + "','" + vColLista3[i].ToString().Replace(",", ".") + "','" + vColLista4[i].ToString().Replace(",", ".") + "','" + vColLista5[i].ToString().Replace(",", ".") + "','" + vColLista6[i].ToString().Replace(",", ".") + "','" + vColLista7[i].ToString().Replace(",", ".") + "','" + vColLista8[i].ToString().Replace(",", ".") + "'); ";
            i++;
        }
        vCmd.ExecuteNonQuery();
        ManipuleazaBD.InchideConexiune(vCon);
        ResponseHelper.Redirect("~/printCentralizatorCapitolul10b" + pCapitol + ".aspx?codCapitol=10b", "_new", "");
    }
    protected void lbCapitol10b1_Click(object sender, EventArgs e)
    {
        lbCapitol10b_Click("1");
        return;
        // Centralizator capitolul 10b1
        StergeCapitolCentralizatorUtilizator(Convert.ToInt32(Session["SESan"]), Convert.ToInt32(Session["SESutilizatorId"]), "10b");
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        SqlCommand vCmd1 = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd1.Connection = vCon;
        vCmd.CommandText = @"SELECT * FROM  sabloaneCapitole WHERE (an = '" + Session["SESan"].ToString() + "') AND (capitol = '10b') order by codRand";
        SqlDataReader vTabelCapitole = vCmd.ExecuteReader();
        // facem o lista cu capitolele din sabloane
        List<List<string>> vListaCapitoleSabloane = new List<List<string>> { };
        while (vTabelCapitole.Read())
        {
            List<string> vListaX = new List<string> { };
            vListaX.Add(vTabelCapitole["codRand"].ToString());
            vListaX.Add(vTabelCapitole["denumire1"].ToString());
            vListaX.Add(vTabelCapitole["denumire2"].ToString());
            vListaX.Add(vTabelCapitole["denumire3"].ToString());
            vListaX.Add(vTabelCapitole["denumire4"].ToString());
            vListaX.Add(vTabelCapitole["denumire5"].ToString());
            vListaCapitoleSabloane.Add(vListaX);
        }
        vTabelCapitole.Close();
        // pt toate randurile, toate tipurile de gospodarii, sem1
        vCmd1.CommandText = "SELECT coalesce(SUM(capitole.col1),0) AS Expr1, capitole.codRand FROM capitole INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId INNER JOIN unitati ON capitole.unitateId = unitati.unitateId WHERE (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.codCapitol = '10b') AND (CONVERT(nvarchar, capitole.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') GROUP BY capitole.codRand ORDER BY capitole.codRand";
        SqlDataReader vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista1 = new List<string> { };
        List<string> vColListaRand1 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista1.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand1.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista1 = C2aUmpleLista(vColLista1, vColListaRand1, vListaCapitoleSabloane);
        // pt toate randurile, toate tipurile de gospodarii sem 2
        vCmd1.CommandText = "SELECT coalesce(SUM(capitole.col2),0) AS Expr1, capitole.codRand FROM capitole INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId INNER JOIN unitati ON capitole.unitateId = unitati.unitateId WHERE (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.codCapitol = '10b') AND (CONVERT(nvarchar, capitole.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') GROUP BY capitole.codRand ORDER BY capitole.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista2 = new List<string> { };
        List<string> vColListaRand2 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista2.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand2.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista2 = C2aUmpleLista(vColLista2, vColListaRand2, vListaCapitoleSabloane);

        // pt toate randurile,  tipurile de gospodarii - localnici - sem 1
        vCmd1.CommandText = "SELECT coalesce(SUM(capitole.col1),0) AS Expr1, capitole.codRand FROM capitole INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId INNER JOIN unitati ON capitole.unitateId = unitati.unitateId WHERE (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.codCapitol = '10b') AND (CONVERT(nvarchar, capitole.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (gospodarii.persJuridica = 0) AND (gospodarii.strainas = 0)  GROUP BY capitole.codRand ORDER BY capitole.codRand";

        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista3 = new List<string> { };
        List<string> vColListaRand3 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista3.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand3.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista3 = C2aUmpleLista(vColLista3, vColListaRand3, vListaCapitoleSabloane);
        // pt toate randurile,  tipurile de gospodarii - localnici - sem 2
        vCmd1.CommandText = "SELECT coalesce(SUM(capitole.col2),0) AS Expr1, capitole.codRand FROM capitole INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId INNER JOIN unitati ON capitole.unitateId = unitati.unitateId WHERE (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.codCapitol = '10b') AND (CONVERT(nvarchar, capitole.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (gospodarii.persJuridica = 0) AND (gospodarii.strainas = 0)  GROUP BY capitole.codRand ORDER BY capitole.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista4 = new List<string> { };
        List<string> vColListaRand4 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista4.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand4.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista4 = C2aUmpleLista(vColLista4, vColListaRand4, vListaCapitoleSabloane);
        // pt toate randurile,  tipurile de gospodarii - strainasi - sem 1
        vCmd1.CommandText = "SELECT coalesce(SUM(capitole.col1),0) AS Expr1, capitole.codRand FROM capitole INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId INNER JOIN unitati ON capitole.unitateId = unitati.unitateId WHERE (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.codCapitol = '10b') AND (CONVERT(nvarchar, capitole.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (gospodarii.strainas = 1)  GROUP BY capitole.codRand ORDER BY capitole.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista5 = new List<string> { };
        List<string> vColListaRand5 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista5.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand5.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista5 = C2aUmpleLista(vColLista5, vColListaRand5, vListaCapitoleSabloane);

        // pt toate randurile,  tipurile de gospodarii - strainasi - sem 2
        vCmd1.CommandText = "SELECT coalesce(SUM(capitole.col2),0) AS Expr1, capitole.codRand FROM capitole INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId INNER JOIN unitati ON capitole.unitateId = unitati.unitateId WHERE (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.codCapitol = '10b') AND (CONVERT(nvarchar, capitole.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (gospodarii.strainas = 1)  GROUP BY capitole.codRand ORDER BY capitole.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista6 = new List<string> { };
        List<string> vColListaRand6 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista6.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand6.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista6 = C2aUmpleLista(vColLista6, vColListaRand6, vListaCapitoleSabloane);

        // pt toate randurile,  tipurile de gospodarii - firme - sem 1
        vCmd1.CommandText = "SELECT coalesce(SUM(capitole.col1),0) AS Expr1, capitole.codRand FROM capitole INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId INNER JOIN unitati ON capitole.unitateId = unitati.unitateId WHERE (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.codCapitol = '10b') AND (CONVERT(nvarchar, capitole.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (gospodarii.persJuridica = 1) GROUP BY capitole.codRand ORDER BY capitole.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista7 = new List<string> { };
        List<string> vColListaRand7 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista7.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand7.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista7 = C2aUmpleLista(vColLista7, vColListaRand7, vListaCapitoleSabloane);

        // pt toate randurile,  tipurile de gospodarii - firme - sem 2
        vCmd1.CommandText = "SELECT coalesce(SUM(capitole.col2),0) AS Expr1, capitole.codRand FROM capitole INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId INNER JOIN unitati ON capitole.unitateId = unitati.unitateId WHERE (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.codCapitol = '10b') AND (CONVERT(nvarchar, capitole.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (gospodarii.persJuridica = 1) GROUP BY capitole.codRand ORDER BY capitole.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista8 = new List<string> { };
        List<string> vColListaRand8 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista8.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand8.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista8 = C2aUmpleLista(vColLista8, vColListaRand8, vListaCapitoleSabloane);



        // facem stringul pt insert
        int i = 0;
        vCmd.CommandText = "";
        foreach (List<string> vRand in vListaCapitoleSabloane)
        {
            vCmd.CommandText += @"INSERT INTO [rapCentralizatoare] ([utilizatorId], [an], [capitol], [codRand], [denumire1], [denumire2], [denumire3], [denumire4], [denumire5], [col1], [col2], [col3], [col4], [col5], [col6], [col7], [col8]) VALUES ('" + Convert.ToInt32(Session["SESutilizatorId"]) + "','" + Convert.ToInt32(Session["SESan"]) + "','10b','" + Convert.ToInt32(vRand[0].ToString()) + "',N'" + vRand[1].ToString() + "',N'" + vRand[2].ToString() + "',N'" + vRand[3].ToString() + "',N'" + vRand[4].ToString() + "',N'" + vRand[5].ToString() + "','" + vColLista1[i].ToString().Replace(",", ".") + "','" + vColLista2[i].ToString().Replace(",", ".") + "','" + vColLista3[i].ToString().Replace(",", ".") + "','" + vColLista4[i].ToString().Replace(",", ".") + "','" + vColLista5[i].ToString().Replace(",", ".") + "','" + vColLista6[i].ToString().Replace(",", ".") + "','" + vColLista7[i].ToString().Replace(",", ".") + "','" + vColLista8[i].ToString().Replace(",", ".") + "'); ";
            i++;
        }
        vCmd.ExecuteNonQuery();
        ManipuleazaBD.InchideConexiune(vCon);
        ResponseHelper.Redirect("~/printCentralizatorCapitolul10b1.aspx?codCapitol=10b", "_new", "");
    }
    protected void lbCapitol10b2_Click(object sender, EventArgs e)
    {
        lbCapitol10b_Click("2");
        return;
        // Centralizator capitolul 10b2
        StergeCapitolCentralizatorUtilizator(Convert.ToInt32(Session["SESan"]), Convert.ToInt32(Session["SESutilizatorId"]), "10b");
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        SqlCommand vCmd1 = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd1.Connection = vCon;
        vCmd.CommandText = @"SELECT * FROM  sabloaneCapitole WHERE (an = '" + Session["SESan"].ToString() + "') AND (capitol = '10b') order by codRand";
        SqlDataReader vTabelCapitole = vCmd.ExecuteReader();

        // facem o lista cu capitolele din sabloane
        List<List<string>> vListaCapitoleSabloane = new List<List<string>> { };
        while (vTabelCapitole.Read())
        {
            List<string> vListaX = new List<string> { };
            vListaX.Add(vTabelCapitole["codRand"].ToString());
            vListaX.Add(vTabelCapitole["denumire1"].ToString());
            vListaX.Add(vTabelCapitole["denumire2"].ToString());
            vListaX.Add(vTabelCapitole["denumire3"].ToString());
            vListaX.Add(vTabelCapitole["denumire4"].ToString());
            vListaX.Add(vTabelCapitole["denumire5"].ToString());
            vListaCapitoleSabloane.Add(vListaX);
        }
        vTabelCapitole.Close();
        // pt toate randurile, toate tipurile de gospodarii, sem1
        vCmd1.CommandText = "SELECT coalesce(SUM(capitole.col3),0) AS Expr1, capitole.codRand FROM capitole INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId INNER JOIN unitati ON capitole.unitateId = unitati.unitateId WHERE (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.codCapitol = '10b') AND (CONVERT(nvarchar, capitole.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') GROUP BY capitole.codRand ORDER BY capitole.codRand";
        SqlDataReader vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista1 = new List<string> { };
        List<string> vColListaRand1 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista1.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand1.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista1 = C2aUmpleLista(vColLista1, vColListaRand1, vListaCapitoleSabloane);
        // pt toate randurile, toate tipurile de gospodarii sem 2
        vCmd1.CommandText = "SELECT coalesce(SUM(capitole.col4),0) AS Expr1, capitole.codRand FROM capitole INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId INNER JOIN unitati ON capitole.unitateId = unitati.unitateId WHERE (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.codCapitol = '10b') AND (CONVERT(nvarchar, capitole.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') GROUP BY capitole.codRand ORDER BY capitole.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista2 = new List<string> { };
        List<string> vColListaRand2 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista2.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand2.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista2 = C2aUmpleLista(vColLista2, vColListaRand2, vListaCapitoleSabloane);

        // pt toate randurile,  tipurile de gospodarii - localnici - sem 1
        vCmd1.CommandText = "SELECT coalesce(SUM(capitole.col3),0) AS Expr1, capitole.codRand FROM capitole INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId INNER JOIN unitati ON capitole.unitateId = unitati.unitateId WHERE (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.codCapitol = '10b') AND (CONVERT(nvarchar, capitole.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (gospodarii.persJuridica = 0) AND (gospodarii.strainas = 0)  GROUP BY capitole.codRand ORDER BY capitole.codRand";

        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista3 = new List<string> { };
        List<string> vColListaRand3 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista3.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand3.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista3 = C2aUmpleLista(vColLista3, vColListaRand3, vListaCapitoleSabloane);
        // pt toate randurile,  tipurile de gospodarii - localnici - sem 2
        vCmd1.CommandText = "SELECT coalesce(SUM(capitole.col4),0) AS Expr1, capitole.codRand FROM capitole INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId INNER JOIN unitati ON capitole.unitateId = unitati.unitateId WHERE (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.codCapitol = '10b') AND (CONVERT(nvarchar, capitole.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (gospodarii.persJuridica = 0) AND (gospodarii.strainas = 0)  GROUP BY capitole.codRand ORDER BY capitole.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista4 = new List<string> { };
        List<string> vColListaRand4 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista4.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand4.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista4 = C2aUmpleLista(vColLista4, vColListaRand4, vListaCapitoleSabloane);
        // pt toate randurile,  tipurile de gospodarii - strainasi - sem 1
        vCmd1.CommandText = "SELECT coalesce(SUM(capitole.col3),0) AS Expr1, capitole.codRand FROM capitole INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId INNER JOIN unitati ON capitole.unitateId = unitati.unitateId WHERE (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.codCapitol = '10b') AND (CONVERT(nvarchar, capitole.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (gospodarii.strainas = 1)  GROUP BY capitole.codRand ORDER BY capitole.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista5 = new List<string> { };
        List<string> vColListaRand5 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista5.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand5.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista5 = C2aUmpleLista(vColLista5, vColListaRand5, vListaCapitoleSabloane);

        // pt toate randurile,  tipurile de gospodarii - strainasi - sem 2
        vCmd1.CommandText = "SELECT coalesce(SUM(capitole.col4),0) AS Expr1, capitole.codRand FROM capitole INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId INNER JOIN unitati ON capitole.unitateId = unitati.unitateId WHERE (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.codCapitol = '10b') AND (CONVERT(nvarchar, capitole.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (gospodarii.strainas = 1)  GROUP BY capitole.codRand ORDER BY capitole.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista6 = new List<string> { };
        List<string> vColListaRand6 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista6.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand6.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista6 = C2aUmpleLista(vColLista6, vColListaRand6, vListaCapitoleSabloane);

        // pt toate randurile,  tipurile de gospodarii - firme - sem 1
        vCmd1.CommandText = "SELECT coalesce(SUM(capitole.col3),0) AS Expr1, capitole.codRand FROM capitole INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId INNER JOIN unitati ON capitole.unitateId = unitati.unitateId WHERE (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.codCapitol = '10b') AND (CONVERT(nvarchar, capitole.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (gospodarii.persJuridica = 1) GROUP BY capitole.codRand ORDER BY capitole.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista7 = new List<string> { };
        List<string> vColListaRand7 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista7.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand7.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista7 = C2aUmpleLista(vColLista7, vColListaRand7, vListaCapitoleSabloane);

        // pt toate randurile,  tipurile de gospodarii - firme - sem 2
        vCmd1.CommandText = "SELECT coalesce(SUM(capitole.col4),0) AS Expr1, capitole.codRand FROM capitole INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId INNER JOIN unitati ON capitole.unitateId = unitati.unitateId WHERE (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.codCapitol = '10b') AND (CONVERT(nvarchar, capitole.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (gospodarii.persJuridica = 1) GROUP BY capitole.codRand ORDER BY capitole.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista8 = new List<string> { };
        List<string> vColListaRand8 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista8.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand8.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista8 = C2aUmpleLista(vColLista8, vColListaRand8, vListaCapitoleSabloane);



        // facem stringul pt insert
        int i = 0;
        vCmd.CommandText = "";
        foreach (List<string> vRand in vListaCapitoleSabloane)
        {
            vCmd.CommandText += @"INSERT INTO [rapCentralizatoare] ([utilizatorId], [an], [capitol], [codRand], [denumire1], [denumire2], [denumire3], [denumire4], [denumire5], [col1], [col2], [col3], [col4], [col5], [col6], [col7], [col8]) VALUES ('" + Convert.ToInt32(Session["SESutilizatorId"]) + "','" + Convert.ToInt32(Session["SESan"]) + "','10b','" + Convert.ToInt32(vRand[0].ToString()) + "',N'" + vRand[1].ToString() + "',N'" + vRand[2].ToString() + "',N'" + vRand[3].ToString() + "',N'" + vRand[4].ToString() + "',N'" + vRand[5].ToString() + "','" + vColLista1[i].ToString().Replace(",", ".") + "','" + vColLista2[i].ToString().Replace(",", ".") + "','" + vColLista3[i].ToString().Replace(",", ".") + "','" + vColLista4[i].ToString().Replace(",", ".") + "','" + vColLista5[i].ToString().Replace(",", ".") + "','" + vColLista6[i].ToString().Replace(",", ".") + "','" + vColLista7[i].ToString().Replace(",", ".") + "','" + vColLista8[i].ToString().Replace(",", ".") + "'); ";
            i++;
        }
        vCmd.ExecuteNonQuery();
 
        ManipuleazaBD.InchideConexiune(vCon);
        ResponseHelper.Redirect("~/printCentralizatorCapitolul10b2.aspx?codCapitol=10b", "_new", "");
    }
    protected void lbCapitol10b3_Click(object sender, EventArgs e)
    {
        lbCapitol10b_Click("3");
        return;
        // Centralizator capitolul 10b3
        StergeCapitolCentralizatorUtilizator(Convert.ToInt32(Session["SESan"]), Convert.ToInt32(Session["SESutilizatorId"]), "10b");
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        SqlCommand vCmd1 = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd1.Connection = vCon;
        vCmd.CommandText = @"SELECT * FROM  sabloaneCapitole WHERE (an = '" + Session["SESan"].ToString() + "') AND (capitol = '10b') order by codRand";
        SqlDataReader vTabelCapitole = vCmd.ExecuteReader();


        // facem o lista cu capitolele din sabloane
        List<List<string>> vListaCapitoleSabloane = new List<List<string>> { };
        while (vTabelCapitole.Read())
        {
            List<string> vListaX = new List<string> { };
            vListaX.Add(vTabelCapitole["codRand"].ToString());
            vListaX.Add(vTabelCapitole["denumire1"].ToString());
            vListaX.Add(vTabelCapitole["denumire2"].ToString());
            vListaX.Add(vTabelCapitole["denumire3"].ToString());
            vListaX.Add(vTabelCapitole["denumire4"].ToString());
            vListaX.Add(vTabelCapitole["denumire5"].ToString());
            vListaCapitoleSabloane.Add(vListaX);
        }
        vTabelCapitole.Close();
        // pt toate randurile, toate tipurile de gospodarii, sem1
        vCmd1.CommandText = "SELECT coalesce(SUM(capitole.col5),0) AS Expr1, capitole.codRand FROM capitole INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId INNER JOIN unitati ON capitole.unitateId = unitati.unitateId WHERE (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.codCapitol = '10b') AND (CONVERT(nvarchar, capitole.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') GROUP BY capitole.codRand ORDER BY capitole.codRand";
        SqlDataReader vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista1 = new List<string> { };
        List<string> vColListaRand1 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista1.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand1.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista1 = C2aUmpleLista(vColLista1, vColListaRand1, vListaCapitoleSabloane);
        // pt toate randurile, toate tipurile de gospodarii sem 2
        vCmd1.CommandText = "SELECT coalesce(SUM(capitole.col6),0) AS Expr1, capitole.codRand FROM capitole INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId INNER JOIN unitati ON capitole.unitateId = unitati.unitateId WHERE (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.codCapitol = '10b') AND (CONVERT(nvarchar, capitole.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') GROUP BY capitole.codRand ORDER BY capitole.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista2 = new List<string> { };
        List<string> vColListaRand2 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista2.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand2.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista2 = C2aUmpleLista(vColLista2, vColListaRand2, vListaCapitoleSabloane);

        // pt toate randurile,  tipurile de gospodarii - localnici - sem 1
        vCmd1.CommandText = "SELECT coalesce(SUM(capitole.col5),0) AS Expr1, capitole.codRand FROM capitole INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId INNER JOIN unitati ON capitole.unitateId = unitati.unitateId WHERE (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.codCapitol = '10b') AND (CONVERT(nvarchar, capitole.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (gospodarii.persJuridica = 0) AND (gospodarii.strainas = 0)  GROUP BY capitole.codRand ORDER BY capitole.codRand";

        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista3 = new List<string> { };
        List<string> vColListaRand3 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista3.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand3.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista3 = C2aUmpleLista(vColLista3, vColListaRand3, vListaCapitoleSabloane);
        // pt toate randurile,  tipurile de gospodarii - localnici - sem 2
        vCmd1.CommandText = "SELECT coalesce(SUM(capitole.col6),0) AS Expr1, capitole.codRand FROM capitole INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId INNER JOIN unitati ON capitole.unitateId = unitati.unitateId WHERE (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.codCapitol = '10b') AND (CONVERT(nvarchar, capitole.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (gospodarii.persJuridica = 0) AND (gospodarii.strainas = 0)  GROUP BY capitole.codRand ORDER BY capitole.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista4 = new List<string> { };
        List<string> vColListaRand4 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista4.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand4.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista4 = C2aUmpleLista(vColLista4, vColListaRand4, vListaCapitoleSabloane);
        // pt toate randurile,  tipurile de gospodarii - strainasi - sem 1
        vCmd1.CommandText = "SELECT coalesce(SUM(capitole.col5),0) AS Expr1, capitole.codRand FROM capitole INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId INNER JOIN unitati ON capitole.unitateId = unitati.unitateId WHERE (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.codCapitol = '10b') AND (CONVERT(nvarchar, capitole.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (gospodarii.strainas = 1)  GROUP BY capitole.codRand ORDER BY capitole.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista5 = new List<string> { };
        List<string> vColListaRand5 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista5.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand5.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista5 = C2aUmpleLista(vColLista5, vColListaRand5, vListaCapitoleSabloane);

        // pt toate randurile,  tipurile de gospodarii - strainasi - sem 2
        vCmd1.CommandText = "SELECT coalesce(SUM(capitole.col6),0) AS Expr1, capitole.codRand FROM capitole INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId INNER JOIN unitati ON capitole.unitateId = unitati.unitateId WHERE (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.codCapitol = '10b') AND (CONVERT(nvarchar, capitole.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (gospodarii.strainas = 1)  GROUP BY capitole.codRand ORDER BY capitole.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista6 = new List<string> { };
        List<string> vColListaRand6 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista6.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand6.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista6 = C2aUmpleLista(vColLista6, vColListaRand6, vListaCapitoleSabloane);

        // pt toate randurile,  tipurile de gospodarii - firme - sem 1
        vCmd1.CommandText = "SELECT coalesce(SUM(capitole.col5),0) AS Expr1, capitole.codRand FROM capitole INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId INNER JOIN unitati ON capitole.unitateId = unitati.unitateId WHERE (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.codCapitol = '10b') AND (CONVERT(nvarchar, capitole.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (gospodarii.persJuridica = 1) GROUP BY capitole.codRand ORDER BY capitole.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista7 = new List<string> { };
        List<string> vColListaRand7 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista7.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand7.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista7 = C2aUmpleLista(vColLista7, vColListaRand7, vListaCapitoleSabloane);

        // pt toate randurile,  tipurile de gospodarii - firme - sem 2
        vCmd1.CommandText = "SELECT coalesce(SUM(capitole.col6),0) AS Expr1, capitole.codRand FROM capitole INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId INNER JOIN unitati ON capitole.unitateId = unitati.unitateId WHERE (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.codCapitol = '10b') AND (CONVERT(nvarchar, capitole.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (gospodarii.persJuridica = 1) GROUP BY capitole.codRand ORDER BY capitole.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista8 = new List<string> { };
        List<string> vColListaRand8 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista8.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand8.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista8 = C2aUmpleLista(vColLista8, vColListaRand8, vListaCapitoleSabloane);



        // facem stringul pt insert
        int i = 0;
        vCmd.CommandText = "";
        foreach (List<string> vRand in vListaCapitoleSabloane)
        {
            vCmd.CommandText += @"INSERT INTO [rapCentralizatoare] ([utilizatorId], [an], [capitol], [codRand], [denumire1], [denumire2], [denumire3], [denumire4], [denumire5], [col1], [col2], [col3], [col4], [col5], [col6], [col7], [col8]) VALUES ('" + Convert.ToInt32(Session["SESutilizatorId"]) + "','" + Convert.ToInt32(Session["SESan"]) + "','10b','" + Convert.ToInt32(vRand[0].ToString()) + "',N'" + vRand[1].ToString() + "',N'" + vRand[2].ToString() + "',N'" + vRand[3].ToString() + "',N'" + vRand[4].ToString() + "',N'" + vRand[5].ToString() + "','" + vColLista1[i].ToString().Replace(",", ".") + "','" + vColLista2[i].ToString().Replace(",", ".") + "','" + vColLista3[i].ToString().Replace(",", ".") + "','" + vColLista4[i].ToString().Replace(",", ".") + "','" + vColLista5[i].ToString().Replace(",", ".") + "','" + vColLista6[i].ToString().Replace(",", ".") + "','" + vColLista7[i].ToString().Replace(",", ".") + "','" + vColLista8[i].ToString().Replace(",", ".") + "'); ";
            i++;
        }
        vCmd.ExecuteNonQuery();
 
        ManipuleazaBD.InchideConexiune(vCon);
        ResponseHelper.Redirect("~/printCentralizatorCapitolul10b3.aspx?codCapitol=10b", "_new", "");
    }
    protected void lbCapitol10b4_Click(object sender, EventArgs e)
    {
        lbCapitol10b_Click("4");
        return;
        // Centralizator capitolul 10b4
        StergeCapitolCentralizatorUtilizator(Convert.ToInt32(Session["SESan"]), Convert.ToInt32(Session["SESutilizatorId"]), "10b");
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        SqlCommand vCmd1 = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd1.Connection = vCon;
        vCmd.CommandText = @"SELECT * FROM  sabloaneCapitole WHERE (an = '" + Session["SESan"].ToString() + "') AND (capitol = '10b') order by codRand";
        SqlDataReader vTabelCapitole = vCmd.ExecuteReader();

        

        // facem o lista cu capitolele din sabloane
        List<List<string>> vListaCapitoleSabloane = new List<List<string>> { };
        while (vTabelCapitole.Read())
        {
            List<string> vListaX = new List<string> { };
            vListaX.Add(vTabelCapitole["codRand"].ToString());
            vListaX.Add(vTabelCapitole["denumire1"].ToString());
            vListaX.Add(vTabelCapitole["denumire2"].ToString());
            vListaX.Add(vTabelCapitole["denumire3"].ToString());
            vListaX.Add(vTabelCapitole["denumire4"].ToString());
            vListaX.Add(vTabelCapitole["denumire5"].ToString());
            vListaCapitoleSabloane.Add(vListaX);
        }
        vTabelCapitole.Close();
        // pt toate randurile, toate tipurile de gospodarii, sem1
        vCmd1.CommandText = "SELECT coalesce(SUM(capitole.col7),0) AS Expr1, capitole.codRand FROM capitole INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId INNER JOIN unitati ON capitole.unitateId = unitati.unitateId WHERE (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.codCapitol = '10b') AND (CONVERT(nvarchar, capitole.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') GROUP BY capitole.codRand ORDER BY capitole.codRand";
        SqlDataReader vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista1 = new List<string> { };
        List<string> vColListaRand1 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista1.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand1.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista1 = C2aUmpleLista(vColLista1, vColListaRand1, vListaCapitoleSabloane);
        // pt toate randurile, toate tipurile de gospodarii sem 2
        vCmd1.CommandText = "SELECT coalesce(SUM(capitole.col8),0) AS Expr1, capitole.codRand FROM capitole INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId INNER JOIN unitati ON capitole.unitateId = unitati.unitateId WHERE (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.codCapitol = '10b') AND (CONVERT(nvarchar, capitole.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') GROUP BY capitole.codRand ORDER BY capitole.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista2 = new List<string> { };
        List<string> vColListaRand2 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista2.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand2.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista2 = C2aUmpleLista(vColLista2, vColListaRand2, vListaCapitoleSabloane);

        // pt toate randurile,  tipurile de gospodarii - localnici - sem 1
        vCmd1.CommandText = "SELECT coalesce(SUM(capitole.col7),0) AS Expr1, capitole.codRand FROM capitole INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId INNER JOIN unitati ON capitole.unitateId = unitati.unitateId WHERE (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.codCapitol = '10b') AND (CONVERT(nvarchar, capitole.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (gospodarii.persJuridica = 0) AND (gospodarii.strainas = 0)  GROUP BY capitole.codRand ORDER BY capitole.codRand";

        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista3 = new List<string> { };
        List<string> vColListaRand3 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista3.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand3.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista3 = C2aUmpleLista(vColLista3, vColListaRand3, vListaCapitoleSabloane);
        // pt toate randurile,  tipurile de gospodarii - localnici - sem 2
        vCmd1.CommandText = "SELECT coalesce(SUM(capitole.col8),0) AS Expr1, capitole.codRand FROM capitole INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId INNER JOIN unitati ON capitole.unitateId = unitati.unitateId WHERE (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.codCapitol = '10b') AND (CONVERT(nvarchar, capitole.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (gospodarii.persJuridica = 0) AND (gospodarii.strainas = 0)  GROUP BY capitole.codRand ORDER BY capitole.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista4 = new List<string> { };
        List<string> vColListaRand4 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista4.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand4.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista4 = C2aUmpleLista(vColLista4, vColListaRand4, vListaCapitoleSabloane);
        // pt toate randurile,  tipurile de gospodarii - strainasi - sem 1
        vCmd1.CommandText = "SELECT coalesce(SUM(capitole.col7),0) AS Expr1, capitole.codRand FROM capitole INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId INNER JOIN unitati ON capitole.unitateId = unitati.unitateId WHERE (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.codCapitol = '10b') AND (CONVERT(nvarchar, capitole.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (gospodarii.strainas = 1)  GROUP BY capitole.codRand ORDER BY capitole.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista5 = new List<string> { };
        List<string> vColListaRand5 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista5.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand5.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista5 = C2aUmpleLista(vColLista5, vColListaRand5, vListaCapitoleSabloane);

        // pt toate randurile,  tipurile de gospodarii - strainasi - sem 2
        vCmd1.CommandText = "SELECT coalesce(SUM(capitole.col8),0) AS Expr1, capitole.codRand FROM capitole INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId INNER JOIN unitati ON capitole.unitateId = unitati.unitateId WHERE (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.codCapitol = '10b') AND (CONVERT(nvarchar, capitole.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (gospodarii.strainas = 1)  GROUP BY capitole.codRand ORDER BY capitole.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista6 = new List<string> { };
        List<string> vColListaRand6 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista6.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand6.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista6 = C2aUmpleLista(vColLista6, vColListaRand6, vListaCapitoleSabloane);

        // pt toate randurile,  tipurile de gospodarii - firme - sem 1
        vCmd1.CommandText = "SELECT coalesce(SUM(capitole.col7),0) AS Expr1, capitole.codRand FROM capitole INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId INNER JOIN unitati ON capitole.unitateId = unitati.unitateId WHERE (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.codCapitol = '10b') AND (CONVERT(nvarchar, capitole.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (gospodarii.persJuridica = 1) GROUP BY capitole.codRand ORDER BY capitole.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista7 = new List<string> { };
        List<string> vColListaRand7 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista7.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand7.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista7 = C2aUmpleLista(vColLista7, vColListaRand7, vListaCapitoleSabloane);

        // pt toate randurile,  tipurile de gospodarii - firme - sem 2
        vCmd1.CommandText = "SELECT coalesce(SUM(capitole.col8),0) AS Expr1, capitole.codRand FROM capitole INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId INNER JOIN unitati ON capitole.unitateId = unitati.unitateId WHERE (capitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.codCapitol = '10b') AND (CONVERT(nvarchar, capitole.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (gospodarii.persJuridica = 1) GROUP BY capitole.codRand ORDER BY capitole.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista8 = new List<string> { };
        List<string> vColListaRand8 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista8.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand8.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista8 = C2aUmpleLista(vColLista8, vColListaRand8, vListaCapitoleSabloane);



        // facem stringul pt insert
        int i = 0;
        vCmd.CommandText = "";
        foreach (List<string> vRand in vListaCapitoleSabloane)
        {
            vCmd.CommandText += @"INSERT INTO [rapCentralizatoare] ([utilizatorId], [an], [capitol], [codRand], [denumire1], [denumire2], [denumire3], [denumire4], [denumire5], [col1], [col2], [col3], [col4], [col5], [col6], [col7], [col8]) VALUES ('" + Convert.ToInt32(Session["SESutilizatorId"]) + "','" + Convert.ToInt32(Session["SESan"]) + "','10b','" + Convert.ToInt32(vRand[0].ToString()) + "',N'" + vRand[1].ToString() + "',N'" + vRand[2].ToString() + "',N'" + vRand[3].ToString() + "',N'" + vRand[4].ToString() + "',N'" + vRand[5].ToString() + "','" + vColLista1[i].ToString().Replace(",", ".") + "','" + vColLista2[i].ToString().Replace(",", ".") + "','" + vColLista3[i].ToString().Replace(",", ".") + "','" + vColLista4[i].ToString().Replace(",", ".") + "','" + vColLista5[i].ToString().Replace(",", ".") + "','" + vColLista6[i].ToString().Replace(",", ".") + "','" + vColLista7[i].ToString().Replace(",", ".") + "','" + vColLista8[i].ToString().Replace(",", ".") + "'); ";
            i++;
        }
        vCmd.ExecuteNonQuery();
        ManipuleazaBD.InchideConexiune(vCon);
        ResponseHelper.Redirect("~/printCentralizatorCapitolul10b4.aspx?codCapitol=10b", "_new", "");
    }
    protected void lbCapitol11_Click(object sender, EventArgs e)
    {
        // Centralizator capitolul 11
        StergeCapitolCentralizatorUtilizator(Convert.ToInt32(Session["SESan"]), Convert.ToInt32(Session["SESutilizatorId"]), "11");
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        SqlCommand vCmd1 = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd1.Connection = vCon;
        vCmd.CommandText = @"SELECT * FROM  sabloaneCapitole WHERE (an = '" + Session["SESan"].ToString() + "') AND (capitol = '11') and (codRand = '1' or codRand = '4' or codRand = '7' or codRand = '10' or codRand>=13) order by codRand";
        SqlDataReader vTabelCapitole = vCmd.ExecuteReader();
        // facem o lista cu capitolele din sabloane
        List<List<string>> vListaCapitoleSabloane = new List<List<string>> { };
        while (vTabelCapitole.Read())
        {
            List<string> vListaX = new List<string> { };
            vListaX.Add(vTabelCapitole["codRand"].ToString());
            vListaX.Add(vTabelCapitole["denumire1"].ToString());
            vListaX.Add(vTabelCapitole["denumire2"].ToString());
            vListaX.Add(vTabelCapitole["denumire3"].ToString());
            vListaX.Add(vTabelCapitole["denumire4"].ToString());
            vListaX.Add(vTabelCapitole["denumire5"].ToString());
            vListaCapitoleSabloane.Add(vListaX);
        }
        vTabelCapitole.Close();
       
        int vCodRand = 0;


        // pt toate randurile, total
        vCmd1.CommandText = "SELECT coalesce(SUM(capitoleCentralizate.col1),0) AS Expr1, capitoleCentralizate.codRand FROM capitoleCentralizate INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + Session["SESan"].ToString() + "') AND (capitoleCentralizate.codCapitol = '11') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') and (capitoleCentralizate.codRand = '1' or capitoleCentralizate.codRand = '4' or capitoleCentralizate.codRand = '7' or capitoleCentralizate.codRand = '10' or capitoleCentralizate.codRand>=13) GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";
        SqlDataReader vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista1 = new List<string> { };
        List<string> vColListaRand1 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista1.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand1.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista1 = C2aUmpleLista(vColLista1, vColListaRand1, vListaCapitoleSabloane);
        // pt toate randurile, localnici
        vCmd1.CommandText = "SELECT coalesce(SUM(capitoleCentralizate.col1),0) AS Expr1, capitoleCentralizate.codRand FROM capitoleCentralizate INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + Session["SESan"].ToString() + "') AND (capitoleCentralizate.codCapitol = '11') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') and (capitoleCentralizate.codRand = '1' or capitoleCentralizate.codRand = '4' or capitoleCentralizate.codRand = '7' or capitoleCentralizate.codRand = '10' or capitoleCentralizate.codRand>=13) AND (capitoleCentralizate.gospodarieTip = 0 ) GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista2 = new List<string> { };
        List<string> vColListaRand2 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista2.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand2.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista2 = C2aUmpleLista(vColLista2, vColListaRand2, vListaCapitoleSabloane);

        // pt toate randurile,   strainasi 
        vCmd1.CommandText = "SELECT coalesce(SUM(capitoleCentralizate.col1),0) AS Expr1, capitoleCentralizate.codRand FROM capitoleCentralizate INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + Session["SESan"].ToString() + "') AND (capitoleCentralizate.codCapitol = '11') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') and (capitoleCentralizate.codRand = '1' or capitoleCentralizate.codRand = '4' or capitoleCentralizate.codRand = '7' or capitoleCentralizate.codRand = '10' or capitoleCentralizate.codRand>=13) AND (capitoleCentralizate.gospodarieTip >=2 ) GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";

        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista3 = new List<string> { };
        List<string> vColListaRand3 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista3.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand3.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista3 = C2aUmpleLista(vColLista3, vColListaRand3, vListaCapitoleSabloane);
        // pt toate randurile,  firme
        vCmd1.CommandText = "SELECT coalesce(SUM(capitoleCentralizate.col1),0) AS Expr1, capitoleCentralizate.codRand FROM capitoleCentralizate INNER JOIN unitati ON capitoleCentralizate.unitateId = unitati.unitateId WHERE (capitoleCentralizate.an = '" + Session["SESan"].ToString() + "') AND (capitoleCentralizate.codCapitol = '11') AND (CONVERT(nvarchar, capitoleCentralizate.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') and (capitoleCentralizate.codRand = '1' or capitoleCentralizate.codRand = '4' or capitoleCentralizate.codRand = '7' or capitoleCentralizate.codRand = '10' or capitoleCentralizate.codRand>=13) AND (capitoleCentralizate.gospodarieTip in (1,3)) GROUP BY capitoleCentralizate.codRand ORDER BY capitoleCentralizate.codRand";
        vTabelCapitole1 = vCmd1.ExecuteReader();
        List<string> vColLista4 = new List<string> { };
        List<string> vColListaRand4 = new List<string> { };
        while (vTabelCapitole1.Read())
        {
            vColLista4.Add(vTabelCapitole1["Expr1"].ToString());
            vColListaRand4.Add(vTabelCapitole1["codRand"].ToString());
        }
        vTabelCapitole1.Close();
        vColLista4 = C2aUmpleLista(vColLista4, vColListaRand4, vListaCapitoleSabloane);


        // facem stringul pt insert
        
        vCmd.CommandText = "";
        // adunam primele patru
        decimal vSuma1 = 0, vSuma2 = 0, vSuma3 = 0, vSuma4 = 0;
        for (int j = 0; j < 4; j++)
        {
            vSuma1 += Convert.ToDecimal(vColLista1[j].ToString().Replace('.',','));
            vSuma2 += Convert.ToDecimal(vColLista2[j].ToString().Replace('.',','));
            vSuma3 += Convert.ToDecimal(vColLista3[j].ToString().Replace('.',','));
            vSuma4 += Convert.ToDecimal(vColLista4[j].ToString().Replace('.',','));

        }
        vColLista1[0] = vSuma1.ToString().Replace(',', '.');
        vColLista2[0] = vSuma2.ToString().Replace(',', '.');
        vColLista3[0] = vSuma3.ToString().Replace(',', '.');
        vColLista4[0] = vSuma4.ToString().Replace(',', '.');
        int i = 0, k=0;;
        foreach (List<string> vRand in vListaCapitoleSabloane)
        {
            // daca e in primele 4 le adunam
            if (i == 0 || i >= 4)
            {
                vCmd.CommandText += @"INSERT INTO [rapCentralizatoare] ([utilizatorId], [an], [capitol], [codRand], [denumire1], [denumire2], [denumire3], [denumire4], [denumire5], [col1], [col2], [col3], [col4], [col5], [col6], [col7], [col8]) VALUES ('" + Convert.ToInt32(Session["SESutilizatorId"]) + "','" + Convert.ToInt32(Session["SESan"]) + "','11','" + k.ToString() + "',N'" + vRand[1].ToString() + "',N'" + vRand[2].ToString() + "',N'" + vRand[3].ToString() + "',N'" + vRand[4].ToString() + "',N'" + vRand[5].ToString() + "','" + vColLista1[i].ToString().Replace(",", ".") + "','" + vColLista2[i].ToString().Replace(",", ".") + "','" + vColLista3[i].ToString().Replace(",", ".") + "','" + vColLista4[i].ToString().Replace(",", ".") + "','0','0','0','0'); ";
                k++;
            }
            i++;
        }
        vCmd.ExecuteNonQuery();
        ManipuleazaBD.InchideConexiune(vCon);
        ResponseHelper.Redirect("~/printCentralizatorCapitolul11.aspx?codCapitol=11", "_new", "");
    }
    protected void lbCapitol12a_Click(object sender, EventArgs e)
    {
        if (ddlUnitate.SelectedValue == "%")
            ResponseHelper.Redirect("~/printCentralizatorCapitolul12a.aspx?codCapitol=c12a&judet=" + ddlFJudet.SelectedValue, "_new", "");
        else Response.Redirect("~/CentralizatorCapitol12a.aspx");
    }
    protected void lbCapitol12b_Click(object sender, EventArgs e)
    {
        if (ddlUnitate.SelectedValue == "%")
            ResponseHelper.Redirect("~/printCentralizatorCapitolul12b.aspx?codCapitol=c12b&judet=" + ddlFJudet.SelectedValue, "_new", "");
        else Response.Redirect("~/CentralizatorCapitol12b.aspx");
    }
    protected void lbCapitol12c_Click(object sender, EventArgs e)
    {
        if (ddlUnitate.SelectedValue == "%")
            ResponseHelper.Redirect("~/printCentralizatorCapitolul12c.aspx?codCapitol=c12c&judet=" + ddlFJudet.SelectedValue, "_new", "");
        else Response.Redirect("~/CentralizatorCapitol12c.aspx");
    }
    protected void lbCapitol13_Click(object sender, EventArgs e)
    {
        if (ddlUnitate.SelectedValue == "%")
            ResponseHelper.Redirect("~/printCentralizatorCapitolul13.aspx?codCapitol=c13&judet=" + ddlFJudet.SelectedValue, "_new", "");
        else Response.Redirect("~/CentralizatorCapitol13.aspx");
    }
    protected void ddlUnitate_Init(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select top(1) tipUtilizatorId from utilizatori where utilizatorId='" + Session["SESutilizatorId"].ToString() + "'";
        int vTipUtilizator = Convert.ToInt32(vCmd.ExecuteScalar());
        if (vTipUtilizator == 2 || vTipUtilizator == 3 || vTipUtilizator == 5)
        {
            vCmd.CommandText = "SELECT TOP (1) unitati.judetId FROM utilizatori INNER JOIN unitati ON utilizatori.unitateId = unitati.unitateId WHERE (utilizatori.utilizatorId = '" + Session["SESutilizatorId"].ToString() + "')";
            string vJudetId = vCmd.ExecuteScalar().ToString();
            ddlFJudet.Items.Clear();
            ddlFJudet.Items.Add(new ListItem("", vJudetId));
            vCmd.CommandText = "select unitateDenumire,unitateId from unitati where judetId='" + vJudetId + "' order by unitateDenumire";
            ddlUnitate.Items.Clear();
            ListItem vItem = new ListItem("-toate-", "%");
            ddlUnitate.Items.Add(vItem);
            SqlDataReader vTabel = vCmd.ExecuteReader();
            while (vTabel.Read())
            {
                vItem = new ListItem(vTabel["unitateDenumire"].ToString(), vTabel["unitateId"].ToString());
                ddlUnitate.Items.Add(vItem);
            }
            vTabel.Close();
            ddlUnitate.SelectedValue = Session["SESunitateId"].ToString();
            ((MasterPage)this.Page.Master).SchimbaGospodaria();
        }
        ManipuleazaBD.InchideConexiune(vCon);
    }
    protected void ddlFJudet_PreRender(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select judetDenumire,judetId from judete order by judetDenumire";
        ddlFJudet.Items.Clear();
        ListItem vItem = new ListItem("-toate-", "%");
        ddlFJudet.Items.Add(vItem);
        SqlDataReader vTabel = vCmd.ExecuteReader();
        while (vTabel.Read())
        {
            vItem = new ListItem(vTabel["judetDenumire"].ToString(), vTabel["judetId"].ToString());
            ddlFJudet.Items.Add(vItem);
        }
        vTabel.Close();
        vCmd.CommandText = "select top(1) coalesce(judetId,0) from unitati where convert(nvarchar,unitateId)='" + Session["SESunitateId"].ToString() + "'";
        if (ViewState["judetId"] == null)
            try { ViewState["judetId"] = vCmd.ExecuteScalar().ToString(); }
            catch { ViewState["judetId"] = "%"; }
        vCmd.CommandText = "select unitateDenumire,unitateId from unitati where convert(nvarchar,judetId) like '" + ViewState["judetId"].ToString() + "' order by unitateDenumire";
        ddlUnitate.Items.Clear();
        vItem = new ListItem("-toate-", "%");
        ddlUnitate.Items.Add(vItem);
        vTabel = vCmd.ExecuteReader();
        while (vTabel.Read())
        {
            vItem = new ListItem(vTabel["unitateDenumire"].ToString(), vTabel["unitateId"].ToString());
            ddlUnitate.Items.Add(vItem);
        }
        vTabel.Close();
        ManipuleazaBD.InchideConexiune(vCon);
        try
        {
            ddlUnitate.SelectedValue = Session["SESunitateId"].ToString();
        }
        catch
        {
            ddlUnitate.SelectedValue = "%";
        }
        ddlFJudet.SelectedValue = ViewState["judetId"].ToString();
    }
    protected void ddlFJudet_SelectedIndexChanged(object sender, EventArgs e)
    {
        ViewState["judetId"] = ddlFJudet.SelectedValue;
    }
    protected void ddlUnitate_SelectedIndexChanged(object sender, EventArgs e)
    {
        HttpCookie vCook = Request.Cookies["COOKTNT"];
        string v1 = "";
        string v2 = "";
        string v3 = "";

        if (vCook != null)
        {
            vCook = CriptareCookie.DecodeCookie(vCook);
            v1 = vCook["COOKan"];
            v2 = vCook["COOKutilizatorId"];
            v3 = vCook["COOKunitateId"];
            vCook = CriptareCookie.EncodeCookie(vCook);
            Response.Cookies.Add(vCook);
            Response.Cookies.Remove("COOKTNT");
        }
        HttpCookie vCook1 = new HttpCookie("COOKTNT");
        vCook1["COOKan"] = v1;
        vCook1["COOKutilizatorId"] = v2;
        vCook1["COOKunitateId"] = v3;
        vCook1 = CriptareCookie.EncodeCookie(vCook1);
        Response.Cookies.Add(vCook1);
        if (ddlUnitate.SelectedValue == "%")
            Session["SESunitateId"] = 0;
        else Session["SESunitateId"] = ddlUnitate.SelectedValue;
        //   Session["SESgospodarieId"] = "NULA";
        Session["SESgospodarieId"] = null;

        ((MasterPage)this.Page.Master).SchimbaGospodaria();
    }
}
