﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Threading;
using System.Globalization;

public partial class raportListeCentralizate : System.Web.UI.Page
{
    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion

    private void populareDdlCodRand(string pCodCapitol, string pDenumireCapitol)
    {
        lblCodRand.Text = pDenumireCapitol;
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        ddlCodRand.Items.Clear();
        vCmd.CommandText = "SELECT codRand, denumire1 FROM sabloaneCapitole WHERE (an = '" + Session["SESan"].ToString() + "') AND (capitol = '" + pCodCapitol + "') ORDER BY codRand";
        SqlDataReader vTabel = vCmd.ExecuteReader();
        while (vTabel.Read())
        {
            ddlCodRand.Items.Add(new ListItem(vTabel["denumire1"].ToString(), vTabel["codRand"].ToString()));
        }
        vTabel.Close();
        ManipuleazaBD.InchideConexiune(vCon);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "raportMatricolaAnimale", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
            populareDdlCodRand("7", "Tip animal");
        }
    }
    protected void btnFiltrare_Click(object sender, EventArgs e)
    {
        Thread.CurrentThread.CurrentCulture = new CultureInfo("ro-RO");
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd1 = new SqlCommand();
        vCmd1.Connection = vCon;
        SqlCommand vCmd2 = new SqlCommand();
        vCmd2.Connection = vCon;
        // golesc tabela temp
        vCmd1.CommandText = "delete from rapCentralizareCuFiltrari where utilizatorId='" + Session["SESutilizatorId"].ToString() + "'";
        vCmd1.CommandTimeout = 10000;
        vCmd1.ExecuteNonQuery();
        vCmd2.CommandText = "SELECT gospodarii.gospodarieId, gospodarii.unitateId, capitole.an, capitole.codRand, capitole.col1, capitole.col2, capitole.col3, capitole.col4, capitole.col5, capitole.col6, capitole.col7, capitole.col8, (SELECT TOP (1) nume FROM membri WHERE (gospodarieId = gospodarii.gospodarieId) AND (an = capitole.an) ORDER BY codRudenie) AS nume FROM gospodarii INNER JOIN capitole ON gospodarii.gospodarieId = capitole.gospodarieId INNER JOIN unitati ON gospodarii.unitateId = unitati.unitateId WHERE (CONVERT(nvarchar, gospodarii.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (capitole.codCapitol = '" + ddlTipRaport.SelectedValue + "') AND (gospodarii.an = '" + Session["SESan"].ToString() + "') AND (capitole.codRand = '" + ddlCodRand.SelectedValue + "') AND (gospodarii.volum LIKE '" + ((tbVolum.Text == "") ? "%" : tbVolum.Text) + "') AND (gospodarii.nrPozitie LIKE '" + ((tbNrPozitie.Text == "") ? "%" : tbNrPozitie.Text) + "') AND (COALESCE (gospodarii.nrInt, 0) >= " + tbDeLaNr.Text + ") AND (COALESCE (gospodarii.nrInt, 0) <= " + tbLaNr.Text + ") AND (COALESCE (gospodarii.strainas, N'false') LIKE '" + ddlStrainas.SelectedValue + "') AND (gospodarii.strada LIKE '%' + '" + ((tbStrada.Text == "") ? "%" : tbStrada.Text) + "' + '%') AND (gospodarii.localitate LIKE '%' + '" + ((tbLocalitate.Text == "") ? "%" : tbLocalitate.Text) + "' + '%') AND (COALESCE (gospodarii.persJuridica, N'false') LIKE '" + ddlPersJuridica.SelectedValue + "')";
        switch (ddlTipRaport.SelectedValue)
        {
            case "2a":
                vCmd2.CommandText += " AND (col1+col2/100>=" + tbBuc1.Text + ") AND (col1+col2/100<=" + tbBuc2.Text + ")";
                break;
            case "7":
                if (ddlSemestru.SelectedValue == "1")
                    vCmd2.CommandText += " AND (col1>=" + tbBuc1.Text + ") AND (col1<=" + tbBuc2.Text + ")";
                else
                    vCmd2.CommandText += " AND (col2>=" + tbBuc1.Text + ") AND (col2<=" + tbBuc2.Text + ")";
                break;
            default:
                vCmd2.CommandText += " AND (col1>=" + tbBuc1.Text + ") AND (col1<=" + tbBuc2.Text + ")";
                break;
        }
        vCmd2.CommandText += " ORDER BY ";
        switch (ddlOrdonareDupa.SelectedValue)
        {
            case "0": vCmd2.CommandText += "nume"; break;
            case "1": vCmd2.CommandText += "judet, localitate, strada, nr"; break;
            case "2": vCmd2.CommandText += "volum, nrPozitie"; break;
        }

        string vInterogare = "";
        int vCurent = 0;
        vCmd2.CommandTimeout = 10000;
        SqlDataReader vTabel = vCmd2.ExecuteReader();
        while (vTabel.Read())
        {
            string vCol1 = vTabel["col1"].ToString();
            if (ddlSemestru.SelectedValue == "2")
                vCol1 = vTabel["col2"].ToString();
            vInterogare += "INSERT INTO rapCentralizareCuFiltrari (unitateId, utilizatorId, gospodarieId, an, codRand, col1, col2, col3, col4, col5, col6, col7, col8) VALUES ('" + vTabel["unitateId"].ToString() + "', '" + Session["SESutilizatorId"].ToString() + "', '" + vTabel["gospodarieId"].ToString() + "', '" + vTabel["an"].ToString() + "', '" + vTabel["codRand"].ToString() + "', '" + vCol1.Replace(",", ".") + "', '" + vTabel["col2"].ToString().Replace(",", ".") + "', '" + vTabel["col3"].ToString().Replace(",", ".") + "', '" + vTabel["col4"].ToString().Replace(",", ".") + "', '" + vTabel["col5"].ToString().Replace(",", ".") + "', '" + vTabel["col6"].ToString().Replace(",", ".") + "', '" + vTabel["col7"].ToString().Replace(",", ".") + "', '" + vTabel["col8"].ToString().Replace(",", ".") + "'); ";
            vCurent++;
            if (vCurent >= 5000 && vInterogare != "")
            {
                vCmd1.CommandText = vInterogare;
                vCmd1.CommandTimeout = 100000;
                vCmd1.ExecuteNonQuery();
                vCurent = 0;
                vInterogare = "";
            }
        }
        vTabel.Close();
        if (vInterogare != "")
        {
            vCmd1.CommandText = vInterogare;
            vCmd1.ExecuteNonQuery();
            vInterogare = "";
        }
        ManipuleazaBD.InchideConexiune(vCon);
        if (tbVolum.Text == "")
        {
            tbVolum.Text = "%";
        }
        if (tbStrada.Text == "")
        {
            tbStrada.Text = "%";
        }
        if (tbLocalitate.Text == "")
        {
            tbLocalitate.Text = "%";
        }

        ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "raportListeCentralizate", "tiparire lista centralizata", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
        switch (ddlTipRaport.SelectedValue)
        {
            case "2a": ResponseHelper.Redirect("~/printRapListaTerenuri.aspx", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no"); break;
            case "4a": ResponseHelper.Redirect("~/printRapListaCulturi.aspx", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no"); break;
            case "7": ResponseHelper.Redirect("~/printRapListaAnimale.aspx", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no"); break;
            case "9": ResponseHelper.Redirect("~/printRapListaUtilaje.aspx", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no"); break;
        }
    }
    protected void ddlUnitate_Init(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select top(1) tipUtilizatorId from utilizatori where utilizatorId='" + Session["SESutilizatorId"].ToString() + "'";
        int vTipUtilizator = Convert.ToInt32(vCmd.ExecuteScalar());
        if (vTipUtilizator == 2 || vTipUtilizator == 3 || vTipUtilizator == 5)
        {
            vCmd.CommandText = "SELECT TOP (1) unitati.judetId FROM utilizatori INNER JOIN unitati ON utilizatori.unitateId = unitati.unitateId WHERE (utilizatori.utilizatorId = '" + Session["SESutilizatorId"].ToString() + "')";
            string vJudetId = vCmd.ExecuteScalar().ToString();
            ddlFJudet.Items.Clear();
            ddlFJudet.Items.Add(new ListItem("", vJudetId));
            vCmd.CommandText = "select unitateDenumire,unitateId from unitati where judetId='" + vJudetId + "' order by unitateDenumire";
            ddlUnitate.Items.Clear();
            ListItem vItem = new ListItem("-toate-", "%");
            ddlUnitate.Items.Add(vItem);
            SqlDataReader vTabel = vCmd.ExecuteReader();
            while (vTabel.Read())
            {
                vItem = new ListItem(vTabel["unitateDenumire"].ToString(), vTabel["unitateId"].ToString());
                ddlUnitate.Items.Add(vItem);
            }
            vTabel.Close();
            ddlUnitate.SelectedValue = Session["SESunitateId"].ToString();
            ((MasterPage)this.Page.Master).SchimbaGospodaria();
        }
        ManipuleazaBD.InchideConexiune(vCon);
    }
    protected void ddlFJudet_PreRender(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select judetDenumire,judetId from judete order by judetDenumire";
        ddlFJudet.Items.Clear();
        ListItem vItem = new ListItem("-toate-", "%");
        ddlFJudet.Items.Add(vItem);
        SqlDataReader vTabel = vCmd.ExecuteReader();
        while (vTabel.Read())
        {
            vItem = new ListItem(vTabel["judetDenumire"].ToString(), vTabel["judetId"].ToString());
            ddlFJudet.Items.Add(vItem);
        }
        vTabel.Close();
        vCmd.CommandText = "select top(1) coalesce(judetId,0) from unitati where convert(nvarchar,unitateId)='" + Session["SESunitateId"].ToString() + "'";
        if (ViewState["judetId"] == null)
            try { ViewState["judetId"] = vCmd.ExecuteScalar().ToString(); }
            catch { ViewState["judetId"] = "%"; }
        vCmd.CommandText = "select unitateDenumire,unitateId from unitati where convert(nvarchar,judetId) like '" + ViewState["judetId"].ToString() + "' order by unitateDenumire";
        ddlUnitate.Items.Clear();
        vItem = new ListItem("-toate-", "%");
        ddlUnitate.Items.Add(vItem);
        vTabel = vCmd.ExecuteReader();
        while (vTabel.Read())
        {
            vItem = new ListItem(vTabel["unitateDenumire"].ToString(), vTabel["unitateId"].ToString());
            ddlUnitate.Items.Add(vItem);
        }
        vTabel.Close();
        ManipuleazaBD.InchideConexiune(vCon);
        ((MasterPage)this.Page.Master).SchimbaGospodaria();
        try
        {
            ddlUnitate.SelectedValue = Session["SESunitateId"].ToString();
        }
        catch
        {
            ddlUnitate.SelectedValue = "%";
        }
        ddlFJudet.SelectedValue = ViewState["judetId"].ToString();
    }
    protected void ddlFJudet_SelectedIndexChanged(object sender, EventArgs e)
    {
        ViewState["judetId"] = ddlFJudet.SelectedValue;
    }
    protected void ddlTipRaport_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlSemestru.Visible = false;
        switch (ddlTipRaport.SelectedValue)
        {
            case "2a": populareDdlCodRand("2a", "Tip teren"); break;
            case "4a": populareDdlCodRand("4a", "Tip cultură"); break;
            case "7": populareDdlCodRand("7", "Tip animal"); ddlSemestru.Visible = true; break;
            case "9": populareDdlCodRand("9", "Tip utilaj"); break;
        }
        ddlCodRand.Focus();
    }
}
