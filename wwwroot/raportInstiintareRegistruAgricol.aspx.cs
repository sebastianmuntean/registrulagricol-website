﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data.SqlClient;
using System.Threading;
using System.Globalization;

public partial class RaportInstiintareRegistruAgricol : System.Web.UI.Page
{
    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion
    protected void Page_Init(object sender, EventArgs e)
    {
        tbDeLaNr.Text = "0";
        tbLaNr.Text = "999";
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "raportInstiintare", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
        }
    }
    protected void btnFiltrare_Click(object sender, EventArgs e)
    {
        if (tbVolum.Text == "")
        {
            tbVolum.Text = "%";
        }
        if (tbNrPozitie.Text == "")
            tbNrPozitie.Text = "%";
        if (tbStrada.Text == "")
        {
            tbStrada.Text = "%";
        }
        if (tbLocalitate.Text == "")
        {
            tbLocalitate.Text = "%";
        }
        Thread.CurrentThread.CurrentCulture = new CultureInfo("ro-RO");
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandTimeout = 0;
        // golesc tabela temporara
        vCmd.CommandText = "delete from rapInstiintareRA where utilizatorId='" + Session["SESutilizatorId"].ToString() + "'";
        vCmd.ExecuteNonQuery();
        vCmd.CommandText = "SELECT gospodarii.judet, gospodarii.localitate, gospodarii.strada, gospodarii.nr, CASE persjuridica WHEN 0 THEN membri.nume ELSE junitate END AS nume, CASE persjuridica WHEN 0 THEN membri.cnp ELSE jcodfiscal END AS cnp, capitole.gospodarieId, sabloaneCapitole.denumire1, sabloaneCapitole.denumire2,  sabloaneCapitole.denumire3, sabloaneCapitole.denumire4, sabloaneCapitole.denumire5, capitole.col1 + capitole.col2 AS col1, CONVERT(decimal(18, 4), 0) AS col2, capitole.col3, capitole.col4, capitole.col5, capitole.col6, capitole.col7, capitole.col8, '1' AS tip, gospodarii.volum, gospodarii.nrPozitie, '' AS den5, gospodarii.volumInt, gospodarii.nrPozitieInt FROM capitole INNER JOIN sabloaneCapitole ON CONVERT(nvarchar, capitole.codRand) = CONVERT(nvarchar, sabloaneCapitole.codRand) AND CONVERT(nvarchar, capitole.codRand) = sabloaneCapitole.formula AND sabloaneCapitole.an = '" + Session["SESan"].ToString() + "' INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId INNER JOIN unitati ON capitole.unitateId = unitati.unitateId LEFT OUTER JOIN membri ON gospodarii.gospodarieId = membri.gospodarieId AND membri.codRudenie = 1 AND membri.an = '" + Session["SESan"].ToString() + "' WHERE (sabloaneCapitole.capitol = '7') AND (CONVERT(nvarchar, capitole.gospodarieId) LIKE '" + ddlGospodarie.SelectedValue + "') AND (CONVERT(nvarchar, capitole.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (capitole.col1 + capitole.col2 <> 0) AND (capitole.codCapitol = '7') AND (gospodarii.volum LIKE '" + tbVolum.Text + "') AND (CONVERT(nvarchar, gospodarii.strainas) LIKE '" + ddlStrainas.SelectedValue + "') AND (gospodarii.strada LIKE '%' + '" + tbStrada.Text + "' + '%') AND (gospodarii.localitate LIKE '%' + '" + tbLocalitate.Text + "' + '%') AND (CONVERT(nvarchar, gospodarii.persJuridica) LIKE '" + ddlPersJuridica.SelectedValue + "') AND (gospodarii.nrInt >= " + tbDeLaNr.Text + ") AND (gospodarii.nrInt <= " + tbLaNr.Text + ") AND (gospodarii.nrPozitie LIKE '" + tbNrPozitie.Text + "') AND (CONVERT(nvarchar, unitati.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (sabloaneCapitole.an = '" + Session["SESan"].ToString() + "') AND (capitole.an = '" + Session["SESan"].ToString() + "') UNION ALL SELECT        gospodarii_2.judet, gospodarii_2.localitate, gospodarii_2.strada, gospodarii_2.nr, CASE persjuridica WHEN 0 THEN nume ELSE junitate END AS nume, CASE persjuridica WHEN 0 THEN cnp ELSE jcodfiscal END AS cnp, capitole_1.gospodarieId, sabloaneCapitole_1.denumire1, sabloaneCapitole_1.denumire2, sabloaneCapitole_1.denumire3, sabloaneCapitole_1.denumire4, sabloaneCapitole_1.denumire5, capitole_1.col1 + capitole_1.col2 AS col1, CONVERT(decimal(18, 4), 0) AS col2, capitole_1.col3, capitole_1.col4, capitole_1.col5, capitole_1.col6, capitole_1.col7, capitole_1.col8, '2' AS tip, gospodarii_2.volum, gospodarii_2.nrPozitie, '' AS den5, gospodarii_2.volumInt, gospodarii_2.nrPozitieInt FROM capitole AS capitole_1 INNER JOIN sabloaneCapitole AS sabloaneCapitole_1 ON CONVERT(nvarchar, capitole_1.codRand) = CONVERT(nvarchar, sabloaneCapitole_1.codRand) AND CONVERT(nvarchar, capitole_1.codRand) = sabloaneCapitole_1.formula AND sabloaneCapitole_1.an = '" + Session["SESan"].ToString() + "' INNER JOIN gospodarii AS gospodarii_2 ON capitole_1.gospodarieId = gospodarii_2.gospodarieId INNER JOIN unitati AS unitati_2 ON capitole_1.unitateId = unitati_2.unitateId LEFT OUTER JOIN membri AS membri_2 ON gospodarii_2.gospodarieId = membri_2.gospodarieId AND membri_2.codRudenie = 1 AND membri_2.an = '" + Session["SESan"].ToString() + "' WHERE (sabloaneCapitole_1.capitol = '9') AND (CONVERT(nvarchar, capitole_1.gospodarieId) LIKE '" + ddlGospodarie.SelectedValue + "') AND (CONVERT(nvarchar, capitole_1.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (capitole_1.codCapitol = '9') AND (gospodarii_2.volum LIKE '" + tbVolum.Text + "') AND (CONVERT(nvarchar, gospodarii_2.strainas) LIKE '" + ddlStrainas.SelectedValue + "') AND (gospodarii_2.strada LIKE '%' + '" + tbStrada.Text + "' + '%') AND (gospodarii_2.localitate LIKE '%' + '" + tbLocalitate.Text + "' + '%') AND (CONVERT(nvarchar, gospodarii_2.persJuridica) LIKE '" + ddlPersJuridica.SelectedValue + "') AND (gospodarii_2.nrInt >= " + tbDeLaNr.Text + ") AND (gospodarii_2.nrInt <= " + tbLaNr.Text + ") AND (gospodarii_2.nrPozitie LIKE '" + tbNrPozitie.Text + "') AND (capitole_1.col1 <> 0) AND (CONVERT(nvarchar, unitati_2.judetId) LIKE '" + ddlFJudet.SelectedValue + "') AND (sabloaneCapitole_1.an = '" + Session["SESan"].ToString() + "') AND (capitole_1.an = '" + Session["SESan"].ToString() + "') UNION ALL SELECT gospodarii_1.judet, gospodarii_1.localitate, gospodarii_1.strada, gospodarii_1.nr, CASE persjuridica WHEN 0 THEN nume ELSE junitate END AS nume, CASE persjuridica WHEN 0 THEN cnp ELSE jcodfiscal END AS cnp, parcele.gospodarieId, parcele.parcelaDenumire AS denumire1, parcele.parcelaNrTopo AS denumire2, parcele.parcelaCF AS denumire3, parcele.parcelaNrCadastral AS denumire4, '' AS denumire5, parcele.parcelaSuprafataIntravilanHa AS col1, parcele.parcelaSuprafataIntravilanAri AS col2, parcele.parcelaSuprafataExtravilanHa AS col3, parcele.parcelaSuprafataExtravilanAri AS col4, CONVERT(decimal(18, 4), 0) AS col5, CONVERT(decimal(18, 4), 0) AS col6, CONVERT(decimal(18, 4), 0) AS col7, CONVERT(decimal(18, 4), 0) AS col8, '3' AS tip, gospodarii_1.volum, gospodarii_1.nrPozitie, sabloaneCapitole_2.denumire5 AS den5, gospodarii_1.volumInt, gospodarii_1.nrPozitieInt FROM parcele INNER JOIN gospodarii AS gospodarii_1 ON parcele.gospodarieId = gospodarii_1.gospodarieId INNER JOIN sabloaneCapitole AS sabloaneCapitole_2 ON parcele.parcelaCategorie = sabloaneCapitole_2.codRand AND sabloaneCapitole_2.capitol = '2a' AND sabloaneCapitole_2.an = '" + Session["SESan"].ToString() + "' INNER JOIN unitati AS unitati_1 ON gospodarii_1.unitateId = unitati_1.unitateId LEFT OUTER JOIN membri AS membri_1 ON gospodarii_1.gospodarieId = membri_1.gospodarieId AND membri_1.codRudenie = 1 AND membri_1.an = '" + Session["SESan"].ToString() + "' WHERE (gospodarii_1.volum LIKE '" + tbVolum.Text + "') AND (CONVERT(nvarchar, gospodarii_1.strainas) LIKE '" + ddlStrainas.SelectedValue + "') AND (gospodarii_1.strada LIKE '%' + '" + tbStrada.Text + "' + '%') AND (gospodarii_1.localitate LIKE '%' + '" + tbLocalitate.Text + "' + '%') AND (CONVERT(nvarchar, gospodarii_1.persJuridica) LIKE '" + ddlPersJuridica.SelectedValue + "') AND (gospodarii_1.nrInt >= " + tbDeLaNr.Text + ") AND (gospodarii_1.nrInt <= " + tbLaNr.Text + ") AND (gospodarii_1.nrPozitie LIKE '" + tbNrPozitie.Text + "') AND (CONVERT(nvarchar, parcele.gospodarieId) LIKE '" + ddlGospodarie.SelectedValue + "') AND (parcele.an = '" + Session["SESan"].ToString() + "') AND (CONVERT(nvarchar, gospodarii_1.unitateId) LIKE '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati_1.judetId) LIKE '" + ddlFJudet.SelectedValue + "') ";
        string vInterogare = "";
        SqlDataReader vTabel = vCmd.ExecuteReader();
        while (vTabel.Read())
        {
            vInterogare += "INSERT INTO rapInstiintareRA (utilizatorId, judet, localitate, strada, nr, nume, cnp, gospodarieId, denumire1, denumire2, denumire3, denumire4, denumire5, col1, col2, col3, col4, col5, col6, col7, col8, volum, nrPozitie, tip, den5, volumInt, nrPozitieInt) VALUES ('" + Session["SESutilizatorId"].ToString() + "', N'" + vTabel["judet"].ToString().Replace("'", "") + "', N'" + vTabel["localitate"].ToString().Replace("'", "") + "', N'" + vTabel["strada"].ToString().Replace("'", "") + "', '" + vTabel["nr"].ToString() + "', N'" + vTabel["nume"].ToString().Replace("'", "") + "', '" + vTabel["cnp"].ToString().Replace("'", "") + "', '" + vTabel["gospodarieId"].ToString() + "', N'" + vTabel["denumire1"].ToString() + "', N'" + vTabel["denumire2"].ToString() + "', N'" + vTabel["denumire3"].ToString() + "', N'" + vTabel["denumire4"].ToString() + "', N'" + vTabel["denumire5"].ToString() + "', '" + Convert.ToDecimal(vTabel["col1"].ToString()).ToString().Replace(",", ".") + "', '" + Convert.ToDecimal(vTabel["col2"].ToString()).ToString().Replace(",", ".") + "', '" + Convert.ToDecimal(vTabel["col3"].ToString()).ToString().Replace(",", ".") + "', '" + Convert.ToDecimal(vTabel["col4"].ToString()).ToString().Replace(",", ".") + "', '" + Convert.ToDecimal(vTabel["col5"].ToString()).ToString().Replace(",", ".") + "', '" + Convert.ToDecimal(vTabel["col6"].ToString()).ToString().Replace(",", ".") + "', '" + Convert.ToDecimal(vTabel["col7"].ToString()).ToString().Replace(",", ".") + "', '" + Convert.ToDecimal(vTabel["col8"].ToString()).ToString().Replace(",", ".") + "', '" + vTabel["volum"].ToString() + "', '" + vTabel["nrPozitie"].ToString() + "', '" + vTabel["tip"].ToString() + "', N'" + vTabel["den5"].ToString() + "', '" + vTabel["volumInt"].ToString() + "', '" + vTabel["nrPozitieInt"].ToString() + "'); ";
        }
        vTabel.Close();
        if (vInterogare != "")
        {
            vCmd.CommandText = vInterogare;
            vCmd.ExecuteNonQuery();
            vInterogare = "";
        }
        ManipuleazaBD.InchideConexiune(vCon);
        ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "raportInstiintare", "tiparire instiintare", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
        ResponseHelper.Redirect("~/printInstiitareRegistruAgricol.aspx?&vol=" + tbVolum.Text + "&nrPoz=" + tbNrPozitie.Text + "&nr1=" + tbDeLaNr.Text + "&nr2=" + tbLaNr.Text + "&strainas=" + ddlStrainas.SelectedValue + "&unit=" + ddlUnitate.SelectedValue + "&str=" + tbStrada.Text + "&loc=" + tbLocalitate.Text + "&gospo=" + ddlGospodarie.SelectedValue + "&ordonare=" + ddlOrdonareDupa.SelectedValue.ToString() + "&judet=" + ddlFJudet.SelectedValue, "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
    }

    protected void ddlUnitate_Init(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select top(1) tipUtilizatorId from utilizatori where utilizatorId='" + Session["SESutilizatorId"].ToString() + "'";
        int vTipUtilizator = Convert.ToInt32(vCmd.ExecuteScalar());
        if (vTipUtilizator == 2 || vTipUtilizator == 3 || vTipUtilizator == 5)
        {
            vCmd.CommandText = "SELECT TOP (1) unitati.judetId FROM utilizatori INNER JOIN unitati ON utilizatori.unitateId = unitati.unitateId WHERE (utilizatori.utilizatorId = '" + Session["SESutilizatorId"].ToString() + "')";
            string vJudetId = vCmd.ExecuteScalar().ToString();
            ddlFJudet.Items.Clear();
            ddlFJudet.Items.Add(new ListItem("", vJudetId));
            vCmd.CommandText = "select unitateDenumire,unitateId from unitati where judetId='" + vJudetId + "' order by unitateDenumire";
            ddlUnitate.Items.Clear();
            ListItem vItem = new ListItem("-toate-", "%");
            ddlUnitate.Items.Add(vItem);
            SqlDataReader vTabel = vCmd.ExecuteReader();
            while (vTabel.Read())
            {
                vItem = new ListItem(vTabel["unitateDenumire"].ToString(), vTabel["unitateId"].ToString());
                ddlUnitate.Items.Add(vItem);
            }
            vTabel.Close();
            ddlUnitate.SelectedValue = Session["SESunitateId"].ToString();
            ((MasterPage)this.Page.Master).SchimbaGospodaria();
        }
        ManipuleazaBD.InchideConexiune(vCon);
    }
    protected void ddlFJudet_PreRender(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select judetDenumire,judetId from judete order by judetDenumire";
        ddlFJudet.Items.Clear();
        ListItem vItem = new ListItem("-toate-", "%");
        ddlFJudet.Items.Add(vItem);
        SqlDataReader vTabel = vCmd.ExecuteReader();
        while (vTabel.Read())
        {
            vItem = new ListItem(vTabel["judetDenumire"].ToString(), vTabel["judetId"].ToString());
            ddlFJudet.Items.Add(vItem);
        }
        vTabel.Close();
        vCmd.CommandText = "select top(1) coalesce(judetId,0) from unitati where convert(nvarchar,unitateId)='" + Session["SESunitateId"].ToString() + "'";
        if (ViewState["judetId"] == null)
            try { ViewState["judetId"] = vCmd.ExecuteScalar().ToString(); }
            catch { ViewState["judetId"] = "%"; }
        vCmd.CommandText = "select unitateDenumire,unitateId from unitati where convert(nvarchar,judetId) like '" + ViewState["judetId"].ToString() + "' order by unitateDenumire";
        ddlUnitate.Items.Clear();
        vItem = new ListItem("-toate-", "%");
        ddlUnitate.Items.Add(vItem);
        vTabel = vCmd.ExecuteReader();
        while (vTabel.Read())
        {
            vItem = new ListItem(vTabel["unitateDenumire"].ToString(), vTabel["unitateId"].ToString());
            ddlUnitate.Items.Add(vItem);
        }
        vTabel.Close();
        ManipuleazaBD.InchideConexiune(vCon);
        ((MasterPage)this.Page.Master).SchimbaGospodaria();
        try
        {
            ddlUnitate.SelectedValue = Session["SESunitateId"].ToString();
        }
        catch
        {
            ddlUnitate.SelectedValue = "%";
        }
        ddlFJudet.SelectedValue = ViewState["judetId"].ToString();
    }
    protected void ddlFJudet_SelectedIndexChanged(object sender, EventArgs e)
    {
        ViewState["judetId"] = ddlFJudet.SelectedValue;
    }
}
