﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="LogOperatii.aspx.cs" Inherits="LogOperatii" Culture="ro-RO" UICulture="ro-RO" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
<asp:Label ID="url" runat="server" Text="Administrare / Istoric operaţii" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="upUpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnListaLogOperatii" runat="server" CssClass="panel_general" Visible="true">
                <asp:Panel ID="pnFiltrareUnitate" runat="server" CssClass="cauta">
            <asp:Label ID="lblUnitate" runat="server" Text="Unitatea:"></asp:Label>
            <asp:DropDownList ID="ddlUnitate" AutoPostBack="true" runat="server" oninit="ddlUnitate_Init">
            </asp:DropDownList>
        </asp:Panel>
                <asp:Panel ID="pnCautare" runat="server" CssClass="cauta">
                    <asp:Label ID="lblCautaDupa" runat="server" Text="Cauta dupa:"></asp:Label>
                    <asp:Label ID="lblfData1" runat="server" Text="Din data"></asp:Label>
                    <asp:TextBox ID="tbfData1" Width="100px" onchange="this.value=ValidareData(this.value, 'dd.MM.yyyy')" runat="server" AutoPostBack="True"></asp:TextBox>
                    <asp:CalendarExtender ID="tbfData_CalendarExtender" runat="server" 
                        Enabled="True" Format="dd.MM.yyyy" TargetControlID="tbfData1">
                    </asp:CalendarExtender>
                    <asp:Label ID="lblfData2" runat="server" Text="La data"></asp:Label>
                    <asp:TextBox ID="tbfData2" Width="100px" onchange="this.value=ValidareData(this.value, 'dd.MM.yyyy')" runat="server" AutoPostBack="True"></asp:TextBox>
                    <asp:CalendarExtender ID="CalendarExtender1" runat="server" 
                        Enabled="True" Format="dd.MM.yyyy" TargetControlID="tbfData2">
                    </asp:CalendarExtender>
                    <asp:Label ID="lblOra" runat="server" Text="Ora"></asp:Label>
                    <asp:TextBox ID="tbfOra" Width="20px" runat="server" AutoPostBack="True"></asp:TextBox>
                    <asp:Label ID="lblMin" runat="server" Text="Min"></asp:Label>
                    <asp:TextBox ID="tbfMin" Width="20px" runat="server" AutoPostBack="True"></asp:TextBox>
                    <asp:Label ID="lblfNume" runat="server" Text="Nume"></asp:Label>
                    <asp:TextBox ID="tbfNume" Width="100px" AutoPostBack="true" runat="server" autocomplete="on"></asp:TextBox>
                    <asp:AutoCompleteExtender ID="tbfNume_AutoCompleteExtender" runat="server" DelimiterCharacters=""
                        Enabled="True" ServicePath="" TargetControlID="tbfNume" ServiceMethod="GetCompletionList"
                        UseContextKey="True">
                    </asp:AutoCompleteExtender>
                    <asp:Label ID="lblfOperatie" runat="server" Text="Operaţia"></asp:Label>
                    <asp:TextBox ID="tbfOperatie" runat="server" AutoPostBack="True"></asp:TextBox>
                </asp:Panel>
                <asp:Panel ID="pnGrid" runat="server" Visible="true">
                    <asp:GridView ID="gvLogOperatii" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        BorderStyle="None" CellPadding="3" GridLines="Vertical" ShowFooter="True" CssClass="tabela"
                        DataKeyNames="logOperatiiId" DataSourceID="SqlLogOperatii" EmptyDataText="Nu sunt adaugate inregistrari"
                        AllowPaging="True" PageSize="250">
                        <Columns>
                            <asp:BoundField DataField="utilizatorNume" HeaderText="Nume" SortExpression="utilizatorNume" />
                            <asp:TemplateField HeaderText="Data" SortExpression="Data">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Data") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("data", "{0:d}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Ora" HeaderText="Ora" SortExpression="Ora" />
                            <asp:BoundField DataField="Min" HeaderText="Minut" SortExpression="Min" />
                            <asp:BoundField DataField="Tabela" HeaderText="Tabela" SortExpression="Tabela" />
                            <asp:BoundField DataField="Operatia" HeaderText="Operatia" SortExpression="Operatia" />
                            <asp:BoundField DataField="ValoareVeche" HeaderText="Valoare veche" SortExpression="ValoareVeche" />
                        </Columns>
                        <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                        <HeaderStyle Font-Bold="True" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlLogOperatii" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016 %>"
                        
                        
                        
                        
                        SelectCommand="SELECT logOperatii.data, logOperatii.ora, logOperatii.min, logOperatii.tabela, logOperatii.operatia, logOperatii.valoareVeche, utilizatori.utilizatorNume + ' ' + utilizatori.utilizatorPrenume + ' din unitatea ' + unitati.unitateDenumire AS utilizatorNume, logOperatii.logOperatiiId FROM unitati INNER JOIN utilizatori ON unitati.unitateId = utilizatori.unitateId RIGHT OUTER JOIN logOperatii ON utilizatori.utilizatorId = logOperatii.utilizatorId WHERE (CONVERT (nvarchar, logOperatii.ora) LIKE @ora) AND (CONVERT (nvarchar, logOperatii.min) LIKE @Min) AND (utilizatori.utilizatorNume LIKE @nume) AND (COALESCE (logOperatii.operatia, N'') LIKE '%' + @operatia + '%') AND (CONVERT (nvarchar, logOperatii.unitateId) LIKE @unitateId) AND (logOperatii.data &gt;= CONVERT (datetime, @data1, 104)) AND (logOperatii.data &lt;= CONVERT (datetime, @data2, 104)) ORDER BY logOperatii.data DESC, logOperatii.ora DESC, logOperatii.min DESC, logOperatii.logOperatiiId DESC">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="tbfOra" DefaultValue="%" Name="ora" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfMin" DefaultValue="%" Name="Min" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfNume" DefaultValue="%" Name="nume" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfOperatie" DefaultValue="%" Name="operatia" 
                                PropertyName="Text" />
                            <asp:ControlParameter ControlID="ddlUnitate" Name="unitateId" 
                                PropertyName="SelectedValue" />
                            <asp:ControlParameter ControlID="tbfData1"  Name="data1" PropertyName="Text" 
                                DefaultValue="" />
                            <asp:ControlParameter ControlID="tbfData2" Name="data2" PropertyName="Text" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
