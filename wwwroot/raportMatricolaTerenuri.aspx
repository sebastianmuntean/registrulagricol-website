﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="raportMatricolaTerenuri.aspx.cs" Inherits="raportMatricolaTerenuri"
    Culture="ro-RO" UICulture="ro-RO" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Rapoarte / Matricolă terenuri" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="pnGeneral" runat="server" CssClass="panel_general">
        <asp:Panel ID="pnFiltrareUnitati" runat="server" CssClass="cauta">
            <asp:Label ID="lblFJudet" runat="server" Text="Judeţ"></asp:Label>
            <asp:DropDownList ID="ddlFJudet" AutoPostBack="true" runat="server" OnPreRender="ddlFJudet_PreRender"
                OnSelectedIndexChanged="ddlFJudet_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:Label ID="lblUnitate" runat="server" Text="unitatea:"></asp:Label>
            <asp:DropDownList ID="ddlUnitate" runat="server" AutoPostBack="True" OnInit="ddlUnitate_Init">
            </asp:DropDownList>
        </asp:Panel>
        <asp:Panel ID="pnFiltrare" runat="server" CssClass="cauta">
            <asp:Label ID="lblFiltrareDupa" runat="server" Text="Caută după:"></asp:Label>
            <asp:Label ID="lblfVolum" runat="server" Text="vol."></asp:Label>
            <asp:TextBox ID="tbfVolum" runat="server" AutoPostBack="True" Width="30px"></asp:TextBox>
            <asp:Label ID="lblDeLaNr" runat="server" Text="de la nr."></asp:Label>
            <asp:TextBox ID="tbfDeLaNr" runat="server" AutoPostBack="True" Width="40px"></asp:TextBox>
            <asp:Label ID="lblLaNr" runat="server" Text="la"></asp:Label>
            <asp:TextBox ID="tbfLaNr" runat="server" AutoPostBack="True" Width="40px"></asp:TextBox>
            <asp:Label ID="LabelStrainas" runat="server" Text="strainaș"></asp:Label>
            <asp:DropDownList ID="ddlStrainas" runat="server" AutoPostBack="true">
                <asp:ListItem Value="%">toți</asp:ListItem>
                <asp:ListItem Value="1">da</asp:ListItem>
                <asp:ListItem Value="0">nu</asp:ListItem>
            </asp:DropDownList>
            <asp:Label ID="lblStrada" runat="server" Text="str."></asp:Label>
            <asp:TextBox ID="tbStrada" runat="server" AutoPostBack="True" Width="100px"></asp:TextBox>
            <asp:Label ID="lblLocalitate" runat="server" Text="loc."></asp:Label>
            <asp:TextBox ID="tbLocalitate" runat="server" AutoPostBack="True" Width="100px"></asp:TextBox>
            <asp:Label ID="lblTipPers1" runat="server" Text="pers.jur."></asp:Label>
            <asp:DropDownList ID="ddlPersJuridica" runat="server" AutoPostBack="true">
                <asp:ListItem Value="%">toți</asp:ListItem>
                <asp:ListItem Value="1">da</asp:ListItem>
                <asp:ListItem Value="0">nu</asp:ListItem>
            </asp:DropDownList>
        </asp:Panel>
        <asp:Panel ID="pnOrdonare" runat="server" CssClass="cauta">
            <asp:Label ID="lblOrdonareDupa" runat="server" Text="Ordonează după:"></asp:Label>
            <asp:DropDownList ID="ddlOrdonareDupa" runat="server" AutoPostBack="true">
                <asp:ListItem Value="0">nume</asp:ListItem>
                <asp:ListItem Value="1">județ + localitate + stradă + nr.</asp:ListItem>
                <asp:ListItem Value="2">volum + nr. poziție</asp:ListItem>
                <asp:ListItem Value="3">C.F + nr. TOPO</asp:ListItem>
            </asp:DropDownList>
        </asp:Panel>
        <asp:Panel ID="pnButoane" runat="server" CssClass="butoane">
            <asp:Button ID="btTiparire" runat="server" Text="tipărește" CssClass="buton" OnClick="btnFiltrare_Click" />
        </asp:Panel>
        <asp:Panel ID="pnRaport" runat="server" CssClass="panel_general">
        </asp:Panel>
    </asp:Panel>
</asp:Content>
