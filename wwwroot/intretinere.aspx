﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="intretinere.aspx.cs" Inherits="intretinere" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="pnAfiseazaMesaje" runat="server">
        <br />
        <br />
        <asp:ValidationSummary ID="valSumUtilizatori" runat="server" DisplayMode="SingleParagraph"
            Visible="true" ValidationGroup="GrupValidareExport" CssClass="validator" ForeColor="" />
        <asp:CustomValidator ID="valCustom" runat="server" ErrorMessage="" ValidationGroup="GrupValidareExport"></asp:CustomValidator>
    </asp:Panel>
    <asp:Panel ID="pnListaLocalitati" runat="server" CssClass="panel_general" Visible="true">
        <asp:Panel ID="pnGrid" runat="server" Visible="true">
            <h1>Operatii de intretinere
            </h1>
            <h3></h3>
            <asp:Panel ID="Panel4" CssClass="butoane" runat="server">
                <asp:Label ID="lblCorecteazaCapitoleCuNull" runat="server" Text="An"></asp:Label>
                <asp:TextBox ID="tbAn" runat="server"></asp:TextBox>
                <h3>Alegeti unitatea</h3>
                <asp:Label ID="lblUnitatea" runat="server" Text="Unitatea"></asp:Label>
                <asp:DropDownList ID="ddlUnitate" runat="server" DataSourceID="SqlUnitati" DataTextField="unitate"
                    DataValueField="unitateId" AppendDataBoundItems="True">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlUnitati" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016 %>"
                    SelectCommand="SELECT judete.judetDenumire + ' - ' + unitati.unitateDenumire AS unitate, unitati.unitateId FROM unitati INNER JOIN judete ON unitati.judetId = judete.judetId ORDER BY unitate"></asp:SqlDataSource>
            </asp:Panel>
            <br />
            <br />
            <br />
            <h2>Parcele</h2>
            <asp:Panel ID="Panel2" runat="server" CssClass="butoane">
                <h3>Stabileste proprietarii parcelelor ca fiind capii de gospodarie (alegeti unitatea si anul de mai sus)</h3>
                <asp:Label ID="Label4" runat="server" Text=""></asp:Label>
                <asp:Button ID="btStabilesteProprietariiIn2B" runat="server" CssClass="buton" OnClick="btStabilesteProprietariiIn2B_Click" Text="Stabileste Proprietarii in 2B" />
            </asp:Panel>

            <h2>Membri</h2>
            <asp:Panel ID="pn2" runat="server" CssClass="butoane">
                <h3>Corecteaza membri din 2015 in 2016</h3>
                <asp:Label ID="lblCorecteazaMembri" runat="server" Text=""></asp:Label>
                <asp:Button ID="btCorecteazaMembri" runat="server" CssClass="buton" OnClick="btCorecteazaMembri_Click" Text="Corecteaza membri din 2015 in 2016" />
            </asp:Panel>

            <asp:Panel ID="pn3" runat="server" CssClass="butoane">
                <h3>Corecteaza diacritice membri din 2015 in 2016</h3>
                <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                <asp:Button ID="btCorecteazaDiactriticeMembri" runat="server" CssClass="buton" OnClick="btCorecteazaDiacriticeMembri_Click" Text="Corecteaza DIACRITICE membri din 2015 in 2016" />
            </asp:Panel>
            <asp:Panel ID="Panel1" runat="server" CssClass="butoane">

                <h3>Pt. eroare la Raport Toate Capitolele</h3>
                <asp:Button ID="btAdaugNul" runat="server" OnClick="btAdaugaUmpleNuluri_Click" CssClass="buton" Text="Umple nuluri" />
            </asp:Panel>
            <br />
            <h3>Repara capitole  (alegeti unitatea in selectorul de sus!)</h3>
            <asp:Panel ID="pn4" runat="server" CssClass="butoane">
                <h4>Corecteaza dubluri de randuri in capitole</h4>
                <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                <asp:TextBox ID="tbAnCorecteazaCapitoleLiniiDublateSauLipsa" runat="server" ToolTip="anul"></asp:TextBox>
                <asp:Button ID="btCorecteazaCapitoleLiniiDublate" runat="server" CssClass="buton" OnClick="btCorecteazaCapitoleLiniiDublate_Click" Text="Corecteaza dubluri" />
            </asp:Panel>
            <asp:Panel ID="pn5" runat="server" CssClass="butoane">
                <h4>Pentru eroare de afisare in capitole (completeaza nr corect de randuri)</h4>
                <asp:Label ID="Label3" runat="server" Text=""></asp:Label>
                <asp:TextBox ID="tbAnCorecteazaCapitoleLiniiLipsa" runat="server" ToolTip="anul"></asp:TextBox>
                <asp:DropDownList ID="ddlCapitoleCorecteazaCapitoleLiniiDublateSauLipsa" runat="server" AppendDataBoundItems="True" DataSourceID="SqlCapitole" DataTextField="capitol" DataValueField="capitol">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlCapitole" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString_2016 %>" SelectCommand="SELECT [capitol]
                                        FROM [TNTRegistruAgricol3].[tntcomputers].[sabloaneCapitole] where  an = @an
                                        group by capitol 
                                        order by capitol">
                    <SelectParameters>
                        <asp:SessionParameter Name="an" SessionField="SESan" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:Button ID="btCorecteazaCapitoleLiniiLipsa" runat="server" CssClass="buton" OnClick="btCorecteazaCapitoleLiniiLipsa_Click" Text="Corecteaza linii capitole" />
            </asp:Panel>
            <asp:Panel ID="pnCopiazaCulturiDinAnPrecedent" runat="server" CssClass="butoane">
                <h4>Copiaza Culturi Din An Precedent</h4>
                <asp:Label ID="lblCopiazaCulturiDinAnPrecedentAn" runat="server" Text="Anul"></asp:Label>
                <asp:TextBox ID="tbCopiazaCulturiDinAnPrecedentAn" runat="server" ToolTip="anul din care se preiau culturile"></asp:TextBox>
                <asp:Button ID="btCopiazaCulturiDinAnPrecedent" runat="server" CssClass="buton" OnClick="btCopiazaCulturiDinAnPrecedent_Click" Text="Copiaza Culturi Din An Precedent" />
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
</asp:Content>
