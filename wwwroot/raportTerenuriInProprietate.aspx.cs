﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data.SqlClient;
using System.IO;
using System.Text;

public partial class raportTerenuriInProprietate : System.Web.UI.Page
{
    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion
    protected void Page_Init(object sender, EventArgs e)
    {
        tbDeLaNr.Text = "0";
        tbLaNr.Text = "9999";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "raportMatricolaTerenuri", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
        }
    }
    protected void ddlUnitate_Init(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select top(1) tipUtilizatorId from utilizatori where utilizatorId='" + Session["SESutilizatorId"].ToString() + "'";
        int vTipUtilizator = Convert.ToInt32(vCmd.ExecuteScalar());
        if (vTipUtilizator == 2 || vTipUtilizator == 3 || vTipUtilizator == 5)
        {
            vCmd.CommandText = "SELECT TOP (1) unitati.judetId FROM utilizatori INNER JOIN unitati ON utilizatori.unitateId = unitati.unitateId WHERE (utilizatori.utilizatorId = '" + Session["SESutilizatorId"].ToString() + "')";
            string vJudetId = vCmd.ExecuteScalar().ToString();
            ddlFJudet.Items.Clear();
            ddlFJudet.Items.Add(new ListItem("", vJudetId));
            vCmd.CommandText = "select unitateDenumire,unitateId from unitati where judetId='" + vJudetId + "' order by unitateDenumire";
            ddlUnitate.Items.Clear();
            ListItem vItem = new ListItem("-toate-", "%");
            ddlUnitate.Items.Add(vItem);
            SqlDataReader vTabel = vCmd.ExecuteReader();
            while (vTabel.Read())
            {
                vItem = new ListItem(vTabel["unitateDenumire"].ToString(), vTabel["unitateId"].ToString());
                ddlUnitate.Items.Add(vItem);
            }
            vTabel.Close();
            ddlUnitate.SelectedValue = Session["SESunitateId"].ToString();
            ((MasterPage)this.Page.Master).SchimbaGospodaria();
        }
        ManipuleazaBD.InchideConexiune(vCon);
    }
    protected void ddlFJudet_PreRender(object sender, EventArgs e)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select judetDenumire,judetId from judete order by judetDenumire";
        ddlFJudet.Items.Clear();
        ListItem vItem = new ListItem("-toate-", "%");
        ddlFJudet.Items.Add(vItem);
        SqlDataReader vTabel = vCmd.ExecuteReader();
        while (vTabel.Read())
        {
            vItem = new ListItem(vTabel["judetDenumire"].ToString(), vTabel["judetId"].ToString());
            ddlFJudet.Items.Add(vItem);
        }
        vTabel.Close();
        vCmd.CommandText = "select top(1) coalesce(judetId,0) from unitati where convert(nvarchar,unitateId)='" + Session["SESunitateId"].ToString() + "'";
        if (ViewState["judetId"] == null)
            try { ViewState["judetId"] = vCmd.ExecuteScalar().ToString(); }
            catch { ViewState["judetId"] = "%"; }
        vCmd.CommandText = "select unitateDenumire,unitateId from unitati where convert(nvarchar,judetId) like '" + ViewState["judetId"].ToString() + "' order by unitateDenumire";
        ddlUnitate.Items.Clear();
        vItem = new ListItem("-toate-", "%");
        ddlUnitate.Items.Add(vItem);
        vTabel = vCmd.ExecuteReader();
        while (vTabel.Read())
        {
            vItem = new ListItem(vTabel["unitateDenumire"].ToString(), vTabel["unitateId"].ToString());
            ddlUnitate.Items.Add(vItem);
        }
        vTabel.Close();
        ManipuleazaBD.InchideConexiune(vCon);
        ((MasterPage)this.Page.Master).SchimbaGospodaria();
        try
        {
            ddlUnitate.SelectedValue = Session["SESunitateId"].ToString();
        }
        catch
        {
            ddlUnitate.SelectedValue = "%";
        }
        ddlFJudet.SelectedValue = ViewState["judetId"].ToString();
    }
    protected void ddlFJudet_SelectedIndexChanged(object sender, EventArgs e)
    {
        ViewState["judetId"] = ddlFJudet.SelectedValue;
    }
    protected void btTiparire_Click(object sender, EventArgs e)
    {
        if (tbVolum.Text == "")
        {
            tbVolum.Text = "%";
        }
        if (tbStrada.Text == "")
        {
            tbStrada.Text = "%";
        }
        if (tbLocalitate.Text == "")
        {
            tbLocalitate.Text = "%";
        }
        if (tbPozitie.Text == "")
            tbPozitie.Text = "%";
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "delete from rapTerenuriProprietate where utilizatorId='" + Session["SESutilizatorId"].ToString() + "'";
        vCmd.ExecuteNonQuery();
        vCmd.CommandText = "SELECT gospodarii.gospodarieId, gospodarii.unitateId, gospodarii.volum, gospodarii.nrPozitie, gospodarii.codSiruta, gospodarii.tip, case gospodarii.persJuridica when 0 then membruCnp else jCodFiscal  end as membrucnp ,gospodarii.strada, gospodarii.nr, gospodarii.nrInt, gospodarii.bl, gospodarii.sc, gospodarii.et, gospodarii.ap, gospodarii.codExploatatie, gospodarii.codUnic, gospodarii.judet, gospodarii.localitate, gospodarii.persJuridica, gospodarii.jUnitate, gospodarii.jSubunitate, gospodarii.jCodFiscal, gospodarii.jNumeReprez, gospodarii.strainas, gospodarii.sStrada, gospodarii.sNr, gospodarii.sBl, gospodarii.sSc, gospodarii.sEtj, gospodarii.sAp, gospodarii.sJudet, gospodarii.sLocalitate, gospodarii.dataModificare, gospodarii.oraModificare, gospodarii.minutModificare, gospodarii.volumInt, gospodarii.nrPozitieInt, gospodarii.an, gospodarii.gospodarieIdInitial, gospodarii.gospodarieCui, gospodarii.gospodarieIdImport, gospodarii.observatii, gospodarii.gospodarieSat, COALESCE ((SELECT TOP (1) nume FROM membri WHERE (gospodarieId = gospodarii.gospodarieId) AND (an = gospodarii.an) ORDER BY codRudenie), '') AS nume, coalesce(parcele.parcelaCategorie,0) as parcelaCategorie, parcele.parcelaSuprafataIntravilanHa, parcele.parcelaSuprafataIntravilanAri, parcele.parcelaSuprafataExtravilanHa, parcele.parcelaSuprafataExtravilanAri FROM gospodarii INNER JOIN unitati ON gospodarii.unitateId = unitati.unitateId INNER JOIN parcele ON gospodarii.gospodarieId = parcele.gospodarieId AND gospodarii.an = parcele.an WHERE (CONVERT(nvarchar, gospodarii.unitateId) = '" + ddlUnitate.SelectedValue + "') AND (CONVERT(nvarchar, unitati.judetId) = '" + ddlFJudet.SelectedValue + "') AND (gospodarii.volum LIKE '" + tbVolum.Text + "') AND (gospodarii.nrPozitie LIKE '" + tbPozitie.Text + "') AND (gospodarii.nrInt >= '" + tbDeLaNr.Text + "') AND (gospodarii.nrInt <= '" + tbLaNr.Text + "') AND (CONVERT(nvarchar, gospodarii.strainas) LIKE '" + ddlStrainas.Text + "') AND (gospodarii.localitate LIKE '%" + tbLocalitate.Text + "%') AND (gospodarii.strada LIKE '%" + tbStrada.Text + "%') AND (CONVERT(nvarchar, gospodarii.persJuridica) LIKE '" + ddlPersJuridica.SelectedValue + "') AND (gospodarii.an = '" + Session["SESan"].ToString() + "') AND (parcele.an = '" + Session["SESan"].ToString() + "') ORDER BY ";
        switch (ddlOrdonareDupa.SelectedValue)
        {
            case "0": vCmd.CommandText += "nume"; break;
            case "1": vCmd.CommandText += "gospodarii.judet, gospodarii.localitate, gospodarii.strada, gospodarii.nr"; break;
            case "2": vCmd.CommandText += "gospodarii.volumInt, gospodarii.nrPozitieInt"; break;
        }
        SqlDataReader vTabel = vCmd.ExecuteReader();
        string vGospodarieId = "0", vNume = "", vVolum = "", vPozitie = "", vAdresa = "", vAdresaS = "";
        StringBuilder vInterogare = new StringBuilder();
        decimal vArabil = 0, vPasuni = 0, vFanete = 0, vLivezi = 0, vVii = 0, vPaduri = 0, vTufaris = 0, vCurti = 0, vGradini = 0;
        bool vStrainas = false; string cnp="";
        while (vTabel.Read())
        {
            if (vGospodarieId != vTabel["gospodarieId"].ToString() && vGospodarieId != "0")
            {
                // insert
                if (vArabil + vPasuni + vFanete + vLivezi + vVii + vPaduri + vTufaris + vCurti + vGradini > 0)
                    vInterogare.Append ("INSERT INTO rapTerenuriProprietate (unitateId, utilizatorId, raportNume, raportAdresa, raportVolum, raportPozitia, raportArabil, raportPasuni, raportFanete, raportLivezi, raportVii, raportPaduri, raportTufaris, raportCurti, raportGradini, raportAdresaS,cnp) VALUES ('" + Session["SESunitateId"].ToString() + "', '" + Session["SESutilizatorId"].ToString() + "', N'" + vNume + "', N'" + vAdresa + "', '" + vVolum + "', '" + vPozitie + "', '" + vArabil.ToString().Replace(",", ".") + "', '" + vPasuni.ToString().Replace(",", ".") + "', '" + vFanete.ToString().Replace(",", ".") + "', '" + vLivezi.ToString().Replace(",", ".") + "', '" + vVii.ToString().Replace(",", ".") + "', '" + vPaduri.ToString().Replace(",", ".") + "', '" + vTufaris.ToString().Replace(",", ".") + "', '" + vCurti.ToString().Replace(",", ".") + "', '" + vGradini.ToString().Replace(",", ".") + "', N'" + vAdresaS + "','"+cnp+"');");
                vNume = "";
                vVolum = "";
                vPozitie = "";
                vAdresa = "";
                vAdresaS = "";
                vArabil = 0;
                vPasuni = 0;
                vFanete = 0;
                vLivezi = 0;
                vVii = 0;
                vPaduri = 0;
                vTufaris = 0;
                vCurti = 0;
                vGradini = 0;
                vNume = vTabel["nume"].ToString();
                vVolum = vTabel["volum"].ToString();
                vPozitie = vTabel["nrPozitie"].ToString();
                cnp = vTabel["membrucnp"].ToString();
                vStrainas = Convert.ToBoolean(vTabel["strainas"].ToString());
                vAdresa = "Jud. " + vTabel["judet"].ToString() + ",loc. " + vTabel["localitate"].ToString() + ", str. " + vTabel["strada"].ToString() + ", nr. " + vTabel["nr"].ToString() + ", bl. " + vTabel["bl"].ToString() + ", sc. " + vTabel["sc"].ToString() + ", et. " + vTabel["et"].ToString() + ", ap. " + vTabel["ap"].ToString();
                if (vStrainas)
                    vAdresaS = "; Adresa domiciliu: Jud. " + vTabel["sJudet"].ToString() + ",loc. " + vTabel["sLocalitate"].ToString() + ", str. " + vTabel["sStrada"].ToString() + ", nr. " + vTabel["sNr"].ToString() + ", bl. " + vTabel["sBl"].ToString() + ", sc. " + vTabel["sSc"].ToString() + ", ap. " + vTabel["sAp"].ToString();
                decimal vHaIntra = Convert.ToDecimal(vTabel["parcelaSuprafataIntravilanHa"]), vAriIntra = Convert.ToDecimal(vTabel["parcelaSuprafataIntravilanAri"]), vHaExtra = Convert.ToDecimal(vTabel["parcelaSuprafataExtravilanHa"]), vAriExtra = Convert.ToDecimal(vTabel["parcelaSuprafataExtravilanAri"]);
                decimal vIntra = ((vHaIntra * 100) + vAriIntra) / 100;
                decimal vExtra = ((vHaExtra * 100) + vAriExtra) / 100;
                decimal vSuprafata = 0;
                switch (ddlTipTeren.SelectedValue)
                {
                    case "0": vSuprafata = vIntra + vExtra;
                        break;
                    case "1": vSuprafata = vIntra;
                        break;
                    case "2": vSuprafata = vExtra;
                        break;
                }
                switch (vTabel["parcelaCategorie"].ToString())
                {
                    case "1": vArabil += vSuprafata; break;
                    case "2": vPasuni += vSuprafata; break;
                    case "3": vFanete += vSuprafata; break;
                    case "4": vVii += vSuprafata; break;
                    case "7": vLivezi += vSuprafata; break;
                    case "9": vGradini += vSuprafata; break;
                    case "11": vPaduri += vSuprafata; break;
                    case "14": vCurti += vSuprafata; break;
                    case "16": vTufaris += vSuprafata; break;
                }
            }
            else
            {
                vAdresaS = "";
                vAdresa = "";
                vStrainas = Convert.ToBoolean(vTabel["strainas"].ToString());
                cnp = vTabel["membrucnp"].ToString();
                vNume = vTabel["nume"].ToString();
                vVolum = vTabel["volum"].ToString();
                vPozitie = vTabel["nrPozitie"].ToString();
                vAdresa = "Jud. " + vTabel["judet"].ToString() + ",loc. " + vTabel["localitate"].ToString() + ", str. " + vTabel["strada"].ToString() + ", nr. " + vTabel["nr"].ToString() + ", bl. " + vTabel["bl"].ToString() + ", sc. " + vTabel["sc"].ToString() + ", et. " + vTabel["et"].ToString() + ", ap. " + vTabel["ap"].ToString();
                if (vStrainas)
                    vAdresaS = "; Adresa de domiciliu: Jud. " + vTabel["sJudet"].ToString() + ",loc. " + vTabel["sLocalitate"].ToString() + ", str. " + vTabel["sStrada"].ToString() + ", nr. " + vTabel["sNr"].ToString() + ", bl. " + vTabel["sBl"].ToString() + ", sc. " + vTabel["sSc"].ToString() + ", ap. " + vTabel["sAp"].ToString();
                decimal vHaIntra = Convert.ToDecimal(vTabel["parcelaSuprafataIntravilanHa"]), vAriIntra = Convert.ToDecimal(vTabel["parcelaSuprafataIntravilanAri"]), vHaExtra = Convert.ToDecimal(vTabel["parcelaSuprafataExtravilanHa"]), vAriExtra = Convert.ToDecimal(vTabel["parcelaSuprafataExtravilanAri"]);
                decimal vIntra = ((vHaIntra * 100) + vAriIntra) / 100;
                decimal vExtra = ((vHaExtra * 100) + vAriExtra) / 100;
                decimal vSuprafata = 0;
                switch (ddlTipTeren.SelectedValue)
                {
                    case "0": vSuprafata = vIntra + vExtra;
                        break;
                    case "1": vSuprafata = vIntra;
                        break;
                    case "2": vSuprafata = vExtra;
                        break;                
                }
                switch (vTabel["parcelaCategorie"].ToString())
                {
                    case "1": vArabil += vSuprafata; break;
                    case "2": vPasuni += vSuprafata; break;
                    case "3": vFanete += vSuprafata; break;
                    case "4": vVii += vSuprafata; break;
                    case "7": vLivezi += vSuprafata; break;
                    case "9": vGradini += vSuprafata; break;
                    case "11": vPaduri += vSuprafata; break;
                    case "14": vCurti += vSuprafata; break;
                    case "16": vTufaris += vSuprafata; break;
                }
            }
            vGospodarieId = vTabel["gospodarieId"].ToString();
        }
        vTabel.Close();
        if (vGospodarieId != "0" && vArabil + vPasuni + vFanete + vLivezi + vVii + vPaduri + vTufaris + vCurti + vGradini > 0)
            vInterogare.Append("INSERT INTO rapTerenuriProprietate (unitateId, utilizatorId, raportNume, raportAdresa, raportVolum, raportPozitia, raportArabil, raportPasuni, raportFanete, raportLivezi, raportVii, raportPaduri, raportTufaris, raportCurti, raportGradini, raportAdresaS,cnp) VALUES ('" + Session["SESunitateId"].ToString() + "', '" + Session["SESutilizatorId"].ToString() + "', N'" + vNume + "', N'" + vAdresa + "', '" + vVolum + "', '" + vPozitie + "', '" + vArabil.ToString().Replace(",", ".") + "', '" + vPasuni.ToString().Replace(",", ".") + "', '" + vFanete.ToString().Replace(",", ".") + "', '" + vLivezi.ToString().Replace(",", ".") + "', '" + vVii.ToString().Replace(",", ".") + "', '" + vPaduri.ToString().Replace(",", ".") + "', '" + vTufaris.ToString().Replace(",", ".") + "', '" + vCurti.ToString().Replace(",", ".") + "', '" + vGradini.ToString().Replace(",", ".") + "', N'" + vAdresaS + "','"+cnp+"');");
        if (vInterogare.ToString() != "")
        {
            vCmd.CommandText = vInterogare.ToString();
            vCmd.ExecuteNonQuery();
        }
        ManipuleazaBD.InchideConexiune(vCon);
        ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "raportTerenuriInProprietate", "tiparire terenuri in proprietate", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
        ResponseHelper.Redirect("~/printTerenuriInProprietate.aspx", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
    }
}
