﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="reparaRenumerotarePozitii.aspx.cs" Inherits="reparaRenumerotarePozitii" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    renumerotare volum si pozitie
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="upLocalitati" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnAfiseazaMesaje" runat="server">
                <asp:ValidationSummary ID="valCapitoleSummary" runat="server" DisplayMode="SingleParagraph"
                    Visible="true" CssClass="validator" ForeColor="" />
            </asp:Panel>
            <asp:Panel ID="Panel5" CssClass="ascunsa" runat="server">
                <!-- //////////// VALIDATOARE  \\\\\\\\\\\\\\\\\\ -->
                <asp:CustomValidator ID="valCustom" runat="server" ErrorMessage=""></asp:CustomValidator>
            </asp:Panel>
            <asp:Panel ID="pnListaOptiuni" runat="server" CssClass="panel_general" Visible="true">
                <asp:Panel ID="pnlAdauga" CssClass="adauga" runat="server">
                    <h2>
                        UNITATEA
                    </h2>
                    <p>
                        <asp:Label ID="lblFCautaDupa1" runat="server" Text="Caută:"></asp:Label>
                        <asp:Label ID="lblFJudet" runat="server" Text="Judeţ"></asp:Label>
                        <asp:DropDownList ID="ddlFJudet" AutoPostBack="true" runat="server" OnPreRender="ddlFJudet_PreRender"
                            OnSelectedIndexChanged="ddlFJudet_SelectedIndexChanged">
                        </asp:DropDownList>
                        <asp:Label ID="lblUnitate" runat="server" Text="Unitatea"></asp:Label>
                        <asp:DropDownList ID="ddlUnitate" Width="177px" AutoPostBack="True" runat="server"
                            OnInit="ddlfUnitati_Init" OnPreRender="ddlUnitate_PreRender" OnSelectedIndexChanged="ddlUnitate_SelectedIndexChanged">
                        </asp:DropDownList>
                    </p>
                    <h2>
                        ORDINE LOCALITATI
                    </h2>
                    <asp:Panel ID="pnGrid" runat="server" Visible="true">
                        <asp:GridView ID="gvLocalitati" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            BorderStyle="None" CellPadding="3" GridLines="Vertical" ShowFooter="True" CssClass="tabela"
                            EmptyDataText="Nu sunt adaugate inregistrari" AllowPaging="True" DataKeyNames="localitateId"
                            DataSourceID="SqlLocalitati">
                            <Columns>
                                <asp:TemplateField HeaderText="Denumire localitate" SortExpression="localitateDenumire">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlLocalitate" runat="server" DataSourceID="SqlLocalitati"
                                            DataTextField="localitateDenumire" DataValueField="localitateDenumire" SelectedValue='<%# Bind("localitateDenumire") %>'>
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                            <HeaderStyle Font-Bold="True" />
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlLocalitati" runat="server" ConnectionString="<%$ ConnectionStrings:TNTRegistruAgricolConnectionString %>"
                            SelectCommand="SELECT * FROM [localitatiComuna] WHERE ([unitateId] = @unitateId)">
                            <SelectParameters>
                                <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" Type="Int32" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </asp:Panel>
                    <h2>
                        RENUMEROTARE VOLUM SI POZITIE
                    </h2>
                    <p>
                        <asp:Label ID="lblVolumDeLa" runat="server" Text="De la volumul"></asp:Label>
                        <asp:TextBox ID="tbVolumDeLa" Width="50px" runat="server" Text="1"></asp:TextBox>
                        <asp:Label ID="lblPozitieDeLa" runat="server" Text="De la pozitia"></asp:Label>
                        <asp:TextBox ID="tbPozitieDeLa" Width="50px" runat="server" Text="1"></asp:TextBox>
                        <asp:Label ID="lbTip" runat="server" Text="Tipul" />
                        <asp:DropDownList ID="ddlTip" runat="server" Width="110px">
                            <asp:ListItem Value="%">Toate</asp:ListItem>
                            <asp:ListItem Value="1">Localnic</asp:ListItem>
                            <asp:ListItem Value="2">Străinaş</asp:ListItem>
                            <asp:ListItem Value="3">Firmă pe raza localităţii</asp:ListItem>
                            <asp:ListItem Value="4">Firmă străinaşă</asp:ListItem>
                        </asp:DropDownList>
                        <asp:Label ID="lblPozitiiVolum" runat="server" Text="Numar de pozitii pe un volum"></asp:Label>
                        <asp:TextBox ID="tbPozitiiVolum" Width="50px" runat="server" Text="50"></asp:TextBox>
                    </p>
                    <asp:Panel ID="pnButoanePrincipale" CssClass="butoane" runat="server">
                        <asp:Button CssClass="buton" ID="btRenumerotare" runat="server" Text="renumerotare volum si pozitie"
                            OnClick="btRenumerotare_Click" />
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
