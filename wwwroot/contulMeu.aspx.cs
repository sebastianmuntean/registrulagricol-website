﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

/// <summary>
/// Contul meu
/// Creata la:                  26.02.2011
/// Autor:                      Laza Tudor Mihai
/// Ultima                      actualizare: 26.02.2011
/// Autor:                      Laza Tudor Mihai
/// </summary> 
public partial class contulMeu : System.Web.UI.Page
{
    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "Contul meu", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "SELECT utilizatorIp, utilizatorId, utilizatorLogin, utilizatorNume, utilizatorPrenume, utilizatorFunctia, utilizatorAdresa, utilizatorCNP, utilizatorTelefon, utilizatorMobil, unitateId, utilizatorActiv, utilizatorEmail, utilizatorParola, tipUtilizatorId, utilizatorFursec, coalesce(utilizatorDurataSesiune,30) as utilizatorDurataSesiune FROM utilizatori where utilizatorId='" + Session["SESutilizatorId"].ToString() + "'";
        SqlDataReader vTabel = vCmd.ExecuteReader();
        if (vTabel.Read())
        {
            tbAdresa.Text = vTabel["utilizatorAdresa"].ToString();
            tbCNP.Text = vTabel["utilizatorCNP"].ToString();
            tbEmail.Text = vTabel["utilizatorEmail"].ToString();
            tbFunctia.Text = vTabel["utilizatorFunctia"].ToString();
            tbMobil.Text = vTabel["utilizatorMobil"].ToString();
            tbNume.Text = vTabel["utilizatorNume"].ToString();
            tbPrenume.Text = vTabel["utilizatorPrenume"].ToString();
            tbTelefon.Text = vTabel["utilizatorTelefon"].ToString();
            tbDurataSesiuneLucru.Text = vTabel["utilizatorDurataSesiune"].ToString();
        }
        vTabel.Close();
        ManipuleazaBD.InchideConexiune(vCon);
    }
    private string validareDurataSesiune()
    {
        string vError = "";
        if (tbDurataSesiuneLucru.Text == "")
            vError += "Durata sesiunii de lucru este obligatorie ! ";
        else
            try
            {
                if (Convert.ToInt32(tbDurataSesiuneLucru.Text) > 30)
                    vError += "Durata sesiunii de lucru nu poate fi mai mare de 30 de minute ! ";
                if (Convert.ToInt32(tbDurataSesiuneLucru.Text) < 1)
                    vError += "Duarata sesiunii de lucru nu poate fi mai mică de un minut ! ";
            }
            catch { vError += "Durata sesiunii de lucru este invalidă ! "; }
        return vError;
    }
    protected void btModificaCont_Click(object sender, EventArgs e)
    {
        if (validareDurataSesiune() != "")
        {
            valCustom.IsValid = false;
            valCustom.ErrorMessage = validareDurataSesiune();
            return;
        }
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlTransaction vTranz = ManipuleazaBD.DeschideTranzactie(vCon);
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.Transaction = vTranz;
        try
        {
            // daca este completata parola veche ii modific parola daca nu fac un update la datele generale
            int ok = 0;
            if (tbParolaVeche.Text != "")
            {

                string vParolaVeche = "";
                vCmd.CommandText = "select top(1) utilizatorParola from utilizatori where utilizatorId='" + Session["SESutilizatorId"].ToString() + "'";
                vParolaVeche = vCmd.ExecuteScalar().ToString();
                string vParola = Criptare.Encrypt(tbParolaVeche.Text);
                if (vParolaVeche == vParola)
                {
                    // verificam parolele noi sa corespunda criteriile de securitate ( min. 8, una mare una mica, 1 cifra, 1 special
                    string vExpresie = "";
                    clsUtilizatori.ParolaVerifica(tbParolaNoua.Text, tbParolaConfirm.Text, Session["SEsutilizatorId"].ToString(), out vExpresie, Convert.ToInt16(HttpContext.Current.Session["SESan"]));
                    if (vExpresie != "")
                    {
                        valCustom.IsValid = false;
                        valCustom.ErrorMessage = vExpresie;
                        return;
                    }
                    vCmd.CommandText = "UPDATE [utilizatori] SET [utilizatorParola] = '" + Criptare.Encrypt(tbParolaNoua.Text) + "' WHERE (([utilizatorId] = '" + Session["SESutilizatorId"].ToString() + "'))";
                    vCmd.CommandText = vCmd.CommandText.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
                    vCmd.ExecuteNonQuery();
                    // salvam in utilizatoriParole
                    ManipuleazaBD.fManipuleazaBD("INSERT INTO utilizatoriParole (utilizatorParola,utilizatorId,utilizatorData) VALUES (N'" + Criptare.Encrypt(tbParolaNoua.Text) + "', " + Session["SESutilizatorId"].ToString() + ", CONVERT(datetime, '" + DateTime.Now.ToString() + "', 104))", Convert.ToInt16(Session["SESan"]));
                    ok = 1;
                    valCustom.IsValid = false;
                    valCustom.ErrorMessage = "Datele dumnavoastră au fost modificate!";
                }
                else
                {
                    valCustom.IsValid = false;
                    valCustom.ErrorMessage = "Parola veche introdusă este incorectă!";
                }
            }
            else ok = 1;
            if (ok == 1)
            {
                vCmd.CommandText = "UPDATE [utilizatori] SET [utilizatorNume] = '" + tbNume.Text + "', [utilizatorPrenume] = '" + tbPrenume.Text + "', [utilizatorFunctia] = '" + tbFunctia.Text + "', [utilizatorAdresa] = '" + tbAdresa.Text + "', [utilizatorCNP] = '" + tbCNP.Text + "', [utilizatorTelefon] = '" + tbTelefon.Text + "', [utilizatorMobil] = '" + tbMobil.Text + "', [utilizatorEmail] = '" + tbEmail.Text + "', utilizatorDurataSesiune = '" + tbDurataSesiuneLucru.Text + "' WHERE (([utilizatorId] = '" + Session["SESutilizatorId"].ToString() + "'))";
                vCmd.CommandText = vCmd.CommandText.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
                vCmd.ExecuteNonQuery();
                ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "utilizatori", "modificare date contul meu", "", vCmd, Convert.ToInt64(Session["SESgospodarieId"]), 3);
                vTranz.Commit();
                valCustom.IsValid = false;
                valCustom.ErrorMessage = "Datele dumnavoastră au fost modificate!";
            }
        }
        catch { vTranz.Rollback(); }
        finally { ManipuleazaBD.InchideConexiune(vCon); }
    }
}
