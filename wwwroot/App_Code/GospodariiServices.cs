﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Gospodarie;

public class GospodariiServices
{
//    public static int GetIdsByIdForIdInitial(long id)
//    {
//        Gospodarii gospodarie = new Gospodarii();
//        SqlCommand vCmd = new SqlCommand();

//        vCmd.CommandText = @"SELECT  gospodarieIdInitial
//                                    FROM gospodarii
//                                    WHERE gospodarieId = @gospodarieId
//                             ";

//        vCmd.Parameters.AddWithValue("@gospodarieId", id);

//        SqlConnection connection = ManipuleazaBD.CreareConexiune();
//        vCmd.Connection = connection;

//        SqlDataReader result = vCmd.ExecuteReader();

//        if ( result.Read() )
//        {
//            gospodarie.IdInitial = Convert.ToInt32(result["gospodarieIdInitial"]);
//        }

//        result.Close();
//        ManipuleazaBD.InchideConexiune(connection);

//        return gospodarie.IdInitial;
//    }

//    public static List<Tuple<Int64, Int64>> GetIdsByUnitate(int idUnitate)
//    {
//        List<Tuple<Int64, Int64>> idsList = new List<Tuple<long, long>>();
//        Gospodarii gospodarie = new Gospodarii();
//        SqlCommand vCmd = new SqlCommand();

//        vCmd.CommandText = @"SELECT  gospodarieId
//                                        ,gospodarieIdInitial
//                                    FROM gospodarii
//                                    WHERE unitateId = @unitateId AND an = 2016
//                             ";

//        vCmd.Parameters.AddWithValue("@unitateId", idUnitate);

//        SqlConnection connection = ManipuleazaBD.CreareConexiune();
//        vCmd.Connection = connection;

//        SqlDataReader result = vCmd.ExecuteReader();

//        while ( result.Read() )
//        {
//            gospodarie.Id = Convert.ToInt64(result["gospodarieId"]);
//            gospodarie.IdInitial = Convert.ToInt32(result["gospodarieIdInitial"]); 

//            Tuple<Int64, Int64> ids = new Tuple<long, long>(gospodarie.Id,gospodarie.IdInitial);
                
//            idsList.Add(ids);
//        }

//        result.Close();
//        ManipuleazaBD.InchideConexiune(connection);

//        return idsList;
//    }

    public static List<Gospodarii> GetGospodariiByUnitate(int idUnitate,int an)
    {
        List<Gospodarii> gospodarii = new List<Gospodarii>();

        SqlCommand vCmd = new SqlCommand();
            
        vCmd.CommandText = @"SELECT  gospodarieId
                                        ,volum
                                        ,nrPozitie
                                        ,codSiruta
                                        ,tip
                                        ,strada
                                        ,nr
                                        ,nrInt
                                        ,bl
                                        ,sc
                                        ,et
                                        ,ap
                                        ,codExploatatie
                                        ,codUnic
                                        ,judet
                                        ,localitate
                                        ,persJuridica
                                        ,jUnitate
                                        ,jSubunitate
                                        ,jCodFiscal
                                        ,jNumeReprez
                                        ,strainas
                                        ,sStrada
                                        ,sNr
                                        ,sBl
                                        ,sSc
                                        ,sEtj
                                        ,sAp
                                        ,sJudet
                                        ,sLocalitate
                                        ,dataModificare
                                        ,oraModificare
                                        ,minutModificare
                                        ,volumInt
                                        ,nrPozitieInt
                                        ,an
                                        ,gospodarieIdInitial
                                        ,gospodarieCui
                                        ,gospodarieIdImport
                                        ,observatii
                                        ,gospodarieSat
                                        ,membruNume
                                        ,membruCnp
                                        ,taraStrainas
                                        ,nrRolNominal
                                        ,volumVechi
                                        ,pozitieVeche
                                    FROM gospodarii
                                    WHERE unitateId = @unitateId
                                    AND an = @an
                             ";

        vCmd.Parameters.AddWithValue("@unitateId", idUnitate);
        vCmd.Parameters.AddWithValue("@an", an);

        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(an));
        vCmd.Connection = connection;

        SqlDataReader result = vCmd.ExecuteReader();

        while ( result.Read() )
        {
            Gospodarii gospodarie = GetGospodarie(result, idUnitate);

            gospodarii.Add(gospodarie);
        }

        result.Close();
        ManipuleazaBD.InchideConexiune(connection);

        return gospodarii;
    }
 
    private static Gospodarii GetGospodarie(SqlDataReader result, int idUnitate)
    {
        Gospodarii gospodarie = new Gospodarii();

        gospodarie.Id = Convert.ToInt64(result["gospodarieId"]);
        gospodarie.IdUnitate = idUnitate;
        gospodarie.Volum = result["volum"].ToString();
        gospodarie.NrPozitie = result["nrPozitie"].ToString();
        gospodarie.Siruta = result["codSiruta"].ToString();
        gospodarie.Tip = result["tip"].ToString();
        gospodarie.CodExploatatie = result["codExploatatie"].ToString();
            
        gospodarie.CodUnic = result["codUnic"].ToString();
        gospodarie.IsPersoanaJuridica = Convert.ToBoolean(result["persJuridica"]);
        gospodarie.Unitate = result["jUnitate"].ToString();
        gospodarie.Subunitate = result["jSubunitate"].ToString();
        gospodarie.CodFiscal = result["jCodFiscal"].ToString();
        gospodarie.Reprezentant = result["jNumeReprez"].ToString();
                
        gospodarie.IsStrainas = Convert.ToBoolean(result["strainas"]);

        gospodarie.DataModificare = Convert.ToDateTime(result["dataModificare"]).Date;   
        gospodarie.OraModificare = Convert.ToDateTime(result["oraModificare"]).Hour;
        gospodarie.MinutModificare = Convert.ToDateTime(result["minutModificare"]).Hour;
        gospodarie.VolumIntreg = Convert.ToInt32(result["volumInt"]);
        gospodarie.NrPozitieIntreg = Convert.ToInt32(result["nrPozitieInt"]);
        gospodarie.An = "2016";
        gospodarie.IdInitial = Convert.ToInt32(result["gospodarieIdInitial"]); 
        gospodarie.Cui = result["gospodarieCui"].ToString(); 
        gospodarie.Observatii = result["observatii"].ToString();                         

        gospodarie.NumeMembru = result["membruNume"].ToString();
        gospodarie.CnpMembru = result["membruCnp"].ToString();
        gospodarie.NrRolNominal = Convert.ToInt32(result["nrRolNominal"]);
        gospodarie.VolumVechi = Convert.ToString(result["volumVechi"]);
        gospodarie.PozitieVeche = Convert.ToString(result["pozitieVeche"]);
           
        return gospodarie;
    }
 
//    internal static bool InsertGospodarii(List<Gospodarii> gospodarii)
//    {
//        SqlCommand command = new SqlCommand();

//        foreach ( Gospodarii gospodarie in gospodarii )
//        {
//            gospodarie.An = "2016";
//            string commandText = @"INSERT INTO  [gospodarii]
//                                                (
//                                                 [unitateId]
//                                                ,[volum]
//                                                ,[nrPozitie]
//                                                ,[codSiruta]
//                                                ,[tip]
//                                                ,[strada]
//                                                ,[nr]
//                                                ,[nrInt]
//                                                ,[bl]
//                                                ,[sc]
//                                                ,[et]
//                                                ,[ap]
//                                                ,[codExploatatie]
//                                                ,[codUnic]
//                                                ,[judet]
//                                                ,[localitate]
//                                                ,[persJuridica]
//                                                ,[jUnitate]
//                                                ,[jSubunitate]
//                                                ,[jCodFiscal]
//                                                ,[jNumeReprez]
//                                                ,[strainas]
//                                                ,[sStrada]
//                                                ,[sNr]
//                                                ,[sBl]
//                                                ,[sSc]
//                                                ,[sEtj]
//                                                ,[sAp]
//                                                ,[sJudet]
//                                                ,[sLocalitate]
//                                                ,[dataModificare]
//                                                ,[oraModificare]
//                                                ,[minutModificare]
//                                                ,[volumInt]
//                                                ,[nrPozitieInt]
//                                                ,[an]
//                                                ,[gospodarieIdInitial]
//                                                ,[gospodarieCui]
//                                                ,[gospodarieIdImport]
//                                                ,[observatii]
//                                                ,[gospodarieSat]
//                                                ,[membruNume]
//                                                ,[membruCnp]
//                                                ,[taraStrainas])
//                                        VALUES
//                                        ('" + gospodarie.IdUnitate + "','" +
//                                 gospodarie.Volum + "','" +
//                                 gospodarie.NrPozitie + "','" +
//                                 gospodarie.Siruta + "','" +
//                                 gospodarie.CodExploatatie + "','" +
//                                 gospodarie.CodUnic + "','" +
//                                 gospodarie.IsPersoanaJuridica + "','" +
//                                 gospodarie.Unitate + "','" +
//                                 gospodarie.Subunitate + "','" +
//                                 gospodarie.CodFiscal + "','" +
//                                 gospodarie.Reprezentant + "','" +
//                                 gospodarie.IsStrainas + "','" +
//                                 "',Convert(datetime,'" + gospodarie.DataModificare + "',104),'" +
//                                 gospodarie.OraModificare + "','" +
//                                 gospodarie.MinutModificare + "','" +
//                                 gospodarie.VolumIntreg + "','" +
//                                 gospodarie.NrPozitieIntreg + "','" +
//                                 gospodarie.An + "','" +
//                                 gospodarie.Id + "','" +
//                                 gospodarie.Cui + "','" +
//                                 gospodarie.IdImport + "','" +
//                                 gospodarie.Observatii + "','" +
//                                 gospodarie.NumeMembru + "','" +
//                                 gospodarie.CnpMembru + "','" +
//                                 "');";
                
//            command.CommandText += commandText.ResolveDiacritics();
//        }

//        SqlConnection connection = ManipuleazaBD.CreareConexiune();
//        command.Connection = connection;

//        try
//        {
//            command.ExecuteNonQuery();

//            return true;
//        }
//        catch
//        {
//            return false;
//        }
//        finally
//        {
//            connection.Close();
//        }
//    }

    internal static long GetIdsByIdAndAnForIdFinal(long idInitial,int an)
    {
        Gospodarii gospodarie = new Gospodarii();
        SqlCommand vCmd = new SqlCommand();

        vCmd.CommandText = @"SELECT  gospodarieId
                                    FROM gospodarii
                                    WHERE gospodarieIdInitial = @gospodarieId AND an = @an
                             ";

        vCmd.Parameters.AddWithValue("@gospodarieId", idInitial);
        vCmd.Parameters.AddWithValue("@an", an);

        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(an));
        vCmd.Connection = connection;

        SqlDataReader result = vCmd.ExecuteReader();

        if (result.Read())
        {
            gospodarie.Id = Convert.ToInt64(result["gospodarieId"]);
        }

        result.Close();
        ManipuleazaBD.InchideConexiune(connection);

        return gospodarie.Id;
    }

    public static int GetIdInitialById(int idGospodarie, short an)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(an);
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select gospodarieIdInitial from gospodarii where gospodarieId ='" + idGospodarie + "'";

        return Convert.ToInt32(vCmd.ExecuteScalar());
    }

}