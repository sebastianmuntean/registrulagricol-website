﻿using System;
using System.Linq;


public class Membri
{
	public Membri()
	{
	}

    private long idMembru;
    public long IdMembru
    {
        get { return idMembru; }
        set { idMembru = value; }
    }

    private string nume = string.Empty;
    public string Nume
    {
        get { return nume; }
        set { nume = value; }
    }

    private int idUnitate;
    public int IdUnitate
    {
        get { return idUnitate; }
        set { idUnitate = value; }
    }

    private long idGospodarie;
    public long IdGospodarie
    {
        get
        {
            return idGospodarie;
        }
        set
        {
            idGospodarie = value;
        }
    }

    public string Cnp
    {
        get;
        set;
    }
}