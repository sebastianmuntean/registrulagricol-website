﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsCorelatii
/// </summary>
public class clsCorelatii
{
    public clsCorelatii()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public static List<string[]> EliminaDubluriListaCampuriSeparate(List<string> pListaCampuriSeparate)
    {
        // desfacem corelatiile in vector string[] din care pastram doar rand si capitol
        // eliminam dublurile cap/rand // facem diferenta pentru parcele si paduri -- inca nu
        List<string[]> pLista = new List<string[]> { };
        List<string> pListaDeVerificat = new List<string> { };
        foreach (string pCamp in pListaCampuriSeparate)
        {
            string[] pCampVector = { "", "" };
            string pCampVectorDeVerificat = "";
            // extragem capitolul
            pCampVector[0] = pCamp.Substring(pCamp.IndexOf('*') + 1, pCamp.IndexOf('#') - pCamp.IndexOf('*') - 1);
            // extragem randul
            pCampVector[1] = pCamp.Substring(pCamp.IndexOf('#') + 1, pCamp.IndexOf(';') - pCamp.IndexOf('#') - 1);
            // pt verificarea existentei avem nevoie de un camp string
            pCampVectorDeVerificat = pCampVector[0] + "###" + pCampVector[1];
            if (pListaDeVerificat.IndexOf(pCampVectorDeVerificat) == -1)
            {
                pLista.Add(pCampVector);
                pListaDeVerificat.Add(pCampVectorDeVerificat);
            }
            else
            { }
        }
        return pLista;
    }
    public static string[] CreeazaSelectPentruCampuriSeparate(List<string> pCampuri, int pUnitate, int pGospodarie, int pAn, string pCapitol)
    {
        // construim selectul
        string vSelect = "SELECT codCapitol, codRand, col1, col2, col3, col4, col5, col6, col7, col8 FROM capitole WHERE (an ='" + pAn + "') AND (gospodarieId = '" + pGospodarie + "') AND ( ";
        string vSelectParcele = "SELECT 'parcele' as parcele, parcelaCategorie, parcelaSuprafataIntravilanHa, parcelaSuprafataIntravilanAri, parcelaSuprafataExtravilanHa, parcelaSuprafataExtravilanAri FROM parcele WHERE (an ='" + pAn + "') AND (gospodarieId = '" + pGospodarie + "') AND ( ";
        string vSelectPaduri = "SELECT 'paduri' as paduri,  paduriSuprafataHa, paduriSuprafataAri FROM paduri WHERE (an ='" + pAn + "') AND (gospodarieId = '" + pGospodarie + "')";
        // facem o lista cu campurile de interogat
        List<string[]> vCampuriSeparate = EliminaDubluriListaCampuriSeparate(pCampuri);
        int vContorParcele = 0;
        int vContor = 0;
        foreach (string[] pCamp in vCampuriSeparate)
        {
            if (pCamp[0] != "parcele" && pCamp[0] != "paduri")
            {
                if (vContor == 0)
                {
                    vSelect += "( codCapitol = '" + pCamp[0] + "' AND codRand = '" + pCamp[1] + "' ) ";
                }
                else vSelect += " OR ( codCapitol = '" + pCamp[0] + "' AND codRand = '" + pCamp[1] + "' ) ";
                vContor++;
            }
            else if (pCamp[0] == "parcele")
            {
                if (vContorParcele == 0)
                {
                    vSelectParcele += " ( parcelaCategorie  ='" + pCamp[1] + "') ";
                }
                else
                {
                    vSelectParcele += " OR ( parcelaCategorie  ='" + pCamp[1] + "') ";
                }
                vContorParcele++;
            }
        }
        vSelect += " ) ";
        vSelectParcele += " ) ";
        // returnam toate trei interogarile
        string[] vSelecturi = { vSelect, vSelectParcele, vSelectPaduri };
        return vSelecturi;
    }
    public static List<string> ListaCampuriSeparate(string pCapitol, out List<List<string>> vListaCorelatii, short an)
    {
        // din capitolul salvat pentru care facem interogarea in corelatii extragem o lista de corelatii
        // introduc intr-o lista bidimensionala corelatiile ce trebuie facute pentru un anumit capitol
        List<string> vCampuri = new List<string> { "corelatieId", "camp1", "camp2", "semn" };
        string vInterogare = "SELECT  corelatieId, camp1, camp2, semn FROM corelatii WHERE (camp1 LIKE '%*" + pCapitol + "%') OR  (camp2 LIKE '%*" + pCapitol + "%')";

        vListaCorelatii = ManipuleazaBD.fRezultaListaStringuri(vInterogare, vCampuri, an);
        // din lista de mai sus facem o alta lista cu campurile separate care trebuie extrase din capitole
        List<string> vListaCampuriSeparate = new List<string> { };
        foreach (List<string> vCorelatie in vListaCorelatii)
        {
            List<int> vCampuri12 = new List<int> { 1, 2 };
            foreach (int vCampX in vCampuri12)
            {
                int vNrCampuri = vCorelatie[vCampX].Split('.').Count(); // calculam cate campuri sunt in corelatie
                string[] vCampuriIndividuale = vCorelatie[vCampX].Split('.'); // impartim in campuri 
                for (int i = 0; i < vNrCampuri - 1; i++)
                {
                    vListaCampuriSeparate.Add(vCampuriIndividuale[i]);
                }
            }
        }
        // returnam 2 liste, una corelatiile din tab corelatii si una cu camp1 si 2 separate
        return vListaCampuriSeparate;
    }
    public static string CampValoareGasita(int pTip, List<List<List<string>>> pListaValoriCampuriSeparate, string pCapitol, string pRand, string pColoana)
    {
        // tip: 0- capitole, 1 - parcele, 2 - paduri  
        string vValoare = "";
        decimal vValoareInt = 0;
        // desfacem lista multi in sublista
        List<List<string>> vListaValoriCampuri = pListaValoriCampuriSeparate[pTip];
        foreach (List<string> vListaI in vListaValoriCampuri)
        {
            if (vListaI.IndexOf(pCapitol) == 0 && vListaI.IndexOf(pRand) == 1)
            {
                switch (pCapitol)
                {
                    // daca sunt parcele luam patru coloane si le adunam
                    case "parcele":
                        vValoareInt += 100 * (Convert.ToDecimal(vListaI[Convert.ToInt32(pColoana) + 1]) + Convert.ToDecimal(vListaI[Convert.ToInt32(pColoana) + 3])) + Convert.ToDecimal(vListaI[Convert.ToInt32(pColoana) + 2]) + Convert.ToDecimal(vListaI[Convert.ToInt32(pColoana) + 4]);
                        break;
                    // daca sunt paduri luam doua coloane si le adunam
                    case "paduri":
                        vValoareInt += 100 * Convert.ToDecimal(vListaI[Convert.ToInt32(pColoana) + 1]) + Convert.ToDecimal(vListaI[Convert.ToInt32(pColoana) + 2]);
                        break;
                    // daca sunt capitole luam doar coloana specificata
                    default:
                        vValoareInt = Convert.ToDecimal(vListaI[Convert.ToInt32(pColoana) + 1]);
                        break;
                }
                // daca sunt parcele sau paduri continuam parcurgerea listei pentru adunarea tuturor loturilor; daca nu, iesim din ciclu
                if (pCapitol != "parcele" && pCapitol != "paduri")
                    break;
            }
        }
        vValoare = vValoareInt.ToString();
        return vValoare;
    }
    public static string GasesteValoarea(string pCamp, List<List<List<string>>> pListaValoriCampuriSeparate, int pGospodarie, int pAn)
    {
        string vOperatie = DesfaCampuriInElemente(pCamp)[1];

        string vValoareColoana = "";
        // daca sunt parcele rezulta o suma de toate parcelele existente pe categorii
        // randul il consideram ca echivalent la categoria de folosinta; 
        if (DesfaCampuriInElemente(pCamp)[2] == "parcele")
        {
            // 1 = parcele
            vValoareColoana = CampValoareGasita(1, pListaValoriCampuriSeparate, "parcele", DesfaCampuriInElemente(pCamp)[3], DesfaCampuriInElemente(pCamp)[4]);
        }
        else if (DesfaCampuriInElemente(pCamp)[2] == "paduri")
        {
            // 2 = paduri
            vValoareColoana = CampValoareGasita(2, pListaValoriCampuriSeparate, "paduri", "x", DesfaCampuriInElemente(pCamp)[4]);
        }
        // daca sunt capitole
        else
        {
            // 0 = capitole
            vValoareColoana = CampValoareGasita(0, pListaValoriCampuriSeparate, DesfaCampuriInElemente(pCamp)[2], DesfaCampuriInElemente(pCamp)[3], DesfaCampuriInElemente(pCamp)[4]);
        }
        return vValoareColoana;
    }
    public static decimal ValoareCampDupaFactorAmplificare(string pValoareColoana, string pCamp)
    {
        int vUMFactor = 1; string vUM = "";
        // un factor de amplificare, daca sunt ari "0" = 1, daca sunt ha "1" = 100
        vUM = DesfaCampuriInElemente(pCamp)[5];
        vUMFactor = 1;
        if (vUM == "1") { vUMFactor = 100; }
        if (pValoareColoana == "") { pValoareColoana = "0"; }
        decimal vValoareColoana = vUMFactor * Convert.ToDecimal(pValoareColoana);
        return vValoareColoana;
    }
    public static decimal ValoareTotalaCamp(string pCampIndividual, List<List<List<string>>> pListaValoriCampuriSeparate, int pGospodarie, int pAn)
    {
        decimal vValoareTotalaCamp = 0;
        // gasim valoarea campului individual
        string vValoareaCampIndividual = GasesteValoarea(pCampIndividual, pListaValoriCampuriSeparate, pGospodarie, pAn);
        // operatie + sau -
        string vOperatie = DesfaCampuriInElemente(pCampIndividual)[1];
        if (vOperatie == "-")
        {
            vValoareTotalaCamp += -ValoareCampDupaFactorAmplificare(vValoareaCampIndividual, pCampIndividual);
        }
        else
        {
            vValoareTotalaCamp += ValoareCampDupaFactorAmplificare(vValoareaCampIndividual, pCampIndividual);
        }
        return vValoareTotalaCamp;
    }
    public static string MesajeComparaCamp1Camp2(List<string> pCamp1, decimal pValoareTotalaCamp1, List<string> pCamp2, decimal vValoareTotalaCamp2, string pCorelatieSemn, short an)
    {
        string vMesajSemn = "";
        // scriem mesajele
        switch (pCorelatieSemn)
        {
            case "0":
                // =
                if (Math.Round(pValoareTotalaCamp1, 2, MidpointRounding.AwayFromZero) != Math.Round(vValoareTotalaCamp2, 2, MidpointRounding.AwayFromZero)) vMesajSemn = " egal cu ";
                break;
            case "1":
                // <=
                if (Math.Round(pValoareTotalaCamp1, 2, MidpointRounding.AwayFromZero) > Math.Round(vValoareTotalaCamp2, 2, MidpointRounding.AwayFromZero)) vMesajSemn = " mai puţin decât ";
                break;
            case "2":
                // >=
                if (Math.Round(pValoareTotalaCamp1, 2, MidpointRounding.AwayFromZero) < Math.Round(vValoareTotalaCamp2, 2, MidpointRounding.AwayFromZero)) vMesajSemn = " mai mult decât ";
                break;
            case "3":
                // <>
                if (Math.Round(pValoareTotalaCamp1, 2, MidpointRounding.AwayFromZero) == Math.Round(vValoareTotalaCamp2, 2, MidpointRounding.AwayFromZero)) vMesajSemn = " diferit de ";
                break;
            default:
                break;
        }
        // daca dupa comparatie a rezultat un mesaj inseamna ca acea corelatie nu e valida si o scriem in tabela
        int vNumarPtSemn = 0;
        string vMesaj = "";
        if (vMesajSemn != "")
        {
            vMesaj = "<span class=\"mesaj_1\">";
            foreach (string vCamp1CampSeparat in pCamp1)
            {
                if (vCamp1CampSeparat != "")
                {
                    if (vNumarPtSemn != 0)
                    {
                        vMesaj += "<span class=\"mesaj_2\"> " + DesfaCampuriInElemente(vCamp1CampSeparat)[1] + "</span> " + AfiseazaStringCorelatie(vCamp1CampSeparat, Convert.ToInt16(an)) + " ";
                    }
                    else
                    {
                        vMesaj += AfiseazaStringCorelatie(vCamp1CampSeparat, an) + " ";
                    }
                }
                vNumarPtSemn++;
            }
            vMesaj += "</span> trebuie să fie " + vMesajSemn + " <span class=\"mesaj_1\">";
            vNumarPtSemn = 0;
            foreach (string vCamp2CampSeparat in pCamp2)
            {
                if (vCamp2CampSeparat != "")
                {
                    if (vNumarPtSemn != 0)
                    {
                        vMesaj += "<span class=\"mesaj_2\"> " + DesfaCampuriInElemente(vCamp2CampSeparat)[1] + "</span> " + AfiseazaStringCorelatie(vCamp2CampSeparat, an) + " ";
                    }
                    else
                    {
                        vMesaj += AfiseazaStringCorelatie(vCamp2CampSeparat, an) + " ";
                    }
                }
                vNumarPtSemn++;
            }
            vMesaj += "</span>";
        }
        return vMesaj;
    }
    public static void VerificaListaCorelatii(List<List<string>> pListaCorelatiiId, List<List<List<string>>> pListaValoriCampuriSeparate, int pGospodarie, int pAn, int pUnitate)
    {
        // verificam fiecare corelatie in parte
        foreach (List<string> vListaCorelatieId in pListaCorelatiiId)
        {
            // luam corelatieId
            string vCorelatieId = vListaCorelatieId[0];
            // luam camp1 si il desfacem
            string[] vCamp1Tot = DesfaCorelatieInCampuri(vListaCorelatieId[1]);
            List<string> vCamp1 = new List<string> { };
            decimal vValoareTotalaCamp1 = 0;
            foreach (string vCampIndividual in vCamp1Tot)
            {
                if (vCampIndividual != "")
                {
                    // calculam valoarea camp1
                    vValoareTotalaCamp1 += ValoareTotalaCamp(vCampIndividual, pListaValoriCampuriSeparate, pGospodarie, pAn);
                    vCamp1.Add(vCampIndividual);
                }
            }
            // luam camp2 si il desfacem
            string[] vCamp2Tot = DesfaCorelatieInCampuri(vListaCorelatieId[2]);
            List<string> vCamp2 = new List<string> { };
            decimal vValoareTotalaCamp2 = 0;
            foreach (string vCampIndividual in vCamp2Tot)
            {
                if (vCampIndividual != "")
                {
                    // calculam valoarea camp2
                    vValoareTotalaCamp2 += ValoareTotalaCamp(vCampIndividual, pListaValoriCampuriSeparate, pGospodarie, pAn);
                    vCamp2.Add(vCampIndividual);
                }
            }
            string vMesaj = MesajeComparaCamp1Camp2(vCamp1, vValoareTotalaCamp1, vCamp2, vValoareTotalaCamp2, vListaCorelatieId[3], Convert.ToInt16(pAn));
            if (vMesaj != "")
            {
                string vInsert = "INSERT INTO mesajeInvalidari (unitateId, gospodarieId, an, corelatieId, mesajInvalidare) VALUES ('" + pUnitate.ToString() + "','" + pGospodarie + "','" + pAn + "','" + vCorelatieId + "',N'" + vMesaj + "')";
                ManipuleazaBD.fManipuleazaBD(vInsert, Convert.ToInt16(pAn));
            }
        }
    }
    public static void StergeMesajeCorelatii(List<List<string>> pListaCorelatiiId, int pGospodarie, int pAn)
    {
        // cream interogarea pentru stergerea mesajelor referitoare la gospodaria respectiva / capitolul in cauza
        string vInterogare = "DELETE FROM mesajeInvalidari WHERE (gospodarieId = '" + pGospodarie.ToString() + "') AND (an = '" + pAn.ToString() + "') AND ( ";
        int vContor = 0;
        foreach (List<string> vListaCorelatieId in pListaCorelatiiId)
        {
            if (vContor == 0)
            {
                vInterogare += " (corelatieId = '" + vListaCorelatieId[0] + "') ";
            }
            else
            {
                vInterogare += " OR (corelatieId = '" + vListaCorelatieId[0] + "') ";
            }
            vContor++;
        }
        vInterogare += ")";
        // stergem din mesaje liniile corespunzatoare corelatiilor
        ManipuleazaBD.fManipuleazaBD(vInterogare, Convert.ToInt16(pAn));
    }
    public static void VerificaCorelatie(int pUnitate, int pGospodarie, int pAn, string pCapitol)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        // scoatem toate campurile care vor trebui extrase din tabele
        List<List<string>> vListaCorelatii = new List<List<string>> { }; //lista corelatiilor
        List<string> vListaCampuriSeparate = ListaCampuriSeparate(pCapitol, out vListaCorelatii, Convert.ToInt16(pAn));
        // facem selecturile pentru: [0]capitole, [1]parcele si [2]paduri
        string[] vInterogari = CreeazaSelectPentruCampuriSeparate(vListaCampuriSeparate, pUnitate, pGospodarie, pAn, pCapitol);
        List<List<List<string>>> vListaValoriCampuriSeparate = new List<List<List<string>>> { };
        // apelam interogarile si introducem valorile in liste
        // [0]capitole
        List<string> vListaRezultate = new List<string> { "codCapitol", "codRand", "col1", "col2", "col3", "col4", "col5", "col6", "col7", "col8" };
        vListaValoriCampuriSeparate.Add(ManipuleazaBD.fRezultaListaStringuri(vInterogari[0], vListaRezultate, Convert.ToInt16(pAn)));
        // [1]parcele 
        vListaRezultate = new List<string> { "parcele", "parcelaCategorie", "parcelaSuprafataIntravilanHa", "parcelaSuprafataIntravilanAri", "parcelaSuprafataExtravilanHa", "parcelaSuprafataExtravilanAri" };
        vListaValoriCampuriSeparate.Add(ManipuleazaBD.fRezultaListaStringuri(vInterogari[1], vListaRezultate, Convert.ToInt16(pAn)));
        // [2]paduri 
        vListaRezultate = new List<string> { "paduri", "paduri", "paduriSuprafataHa", "paduriSuprafataAri" };
        vListaValoriCampuriSeparate.Add(ManipuleazaBD.fRezultaListaStringuri(vInterogari[2], vListaRezultate, Convert.ToInt16(pAn)));
        //daca nu sunt nule padurile schimbam al doilea element din "paduri" in x
        for (int i = 0; i < vListaValoriCampuriSeparate[2].Count; i++)
        {
            if (vListaValoriCampuriSeparate[2].Count > 0)
                vListaValoriCampuriSeparate[2][i][1] = "x";
        }
        // stergem mesajele de corelatii existente
        StergeMesajeCorelatii(vListaCorelatii, pGospodarie, pAn);
        // verificam din lista de corelatii
        VerificaListaCorelatii(vListaCorelatii, vListaValoriCampuriSeparate, pGospodarie, pAn, pUnitate);

    }
    public static void VerificaCorelatieX(int pUnitate, int pGospodarie, int pAn, string pCapitol)
    {
        // verific daca au picat sesiunile
        VerificareSesiuni vVerificaSesiuni = new VerificareSesiuni();
        vVerificaSesiuni.VerificaSesiuniCookie();
        // introduc intr-o lista bidimensionala corelatiile ce trebuie facute pentru un anumit capitol
        string[] vWhereCampuri = { "camp1", "camp2" };
        string[] vWhereValori = { "*" + pCapitol, "*" + pCapitol };
        string[] vOperatoriConditionali = { "LIKE", "LIKE" };
        string[] vOperatoriLogici = { "OR", "OR" };
        string[] vCampuriRezultate = { "corelatieId", "camp1", "camp2", "semn" };
        List<ListaSelect> vListaCampuri = ManipuleazaBD.fSelectCuRezultatMultiplu("corelatii", vWhereCampuri, vWhereValori, vOperatoriConditionali, vOperatoriLogici, vCampuriRezultate, Convert.ToInt16(pAn));
        // verificam daca aceste corelatii sunt valide pentru unitate/gospodarie/an
        foreach (ListaSelect vCorelatieCampuri in vListaCampuri)
        {
            // 
            string vCorelatieId = vCorelatieCampuri.Col1;
            // stergem din mesaje linia corespunzatoare corelatiei
            ManipuleazaBD.fManipuleazaBD("DELETE FROM mesajeInvalidari WHERE corelatieId = '" + vCorelatieId + "' AND gospodarieId = '" + pGospodarie.ToString() + "' AND an = '" + pAn.ToString() + "'", Convert.ToInt16(pAn));
            string vCorelatieCamp1 = vCorelatieCampuri.Col2;
            string vCorelatieCamp2 = vCorelatieCampuri.Col3;
            string vCorelatieSemn = vCorelatieCampuri.Col4;
            string[] vCorelatieCampuri2 = DesfaCorelatieInCampuri(vCorelatieCamp2);
            // scoatem valoarea pt camp1

            // daca sunt parcele rezulta o suma de toate parcelele existente pe categorii
            // randul il consideram ca echivalent la categoria de folosinta; 
            string vValoareCamp1String = "";
            if (DesfaCampuriInElemente(vCorelatieCamp1)[2] == "parcele")
            {
                vValoareCamp1String = ScoateValoriParcele(ConstruiesteSelectColoana(vCorelatieCamp1, pUnitate, pGospodarie, pAn), ConstruiesteColoana(vCorelatieCamp1), "parcele", Convert.ToInt16(pAn));
            }
            else if (DesfaCampuriInElemente(vCorelatieCamp1)[2] == "paduri")
            {
                vValoareCamp1String = ScoateValoriParcele(ConstruiesteSelectColoana(vCorelatieCamp1, pUnitate, pGospodarie, pAn), ConstruiesteColoana(vCorelatieCamp1), "paduri", Convert.ToInt16(pAn));
            }
            // daca sunt capitole
            else
            {
                vValoareCamp1String = ManipuleazaBD.fRezultaUnStringScalar(ConstruiesteSelectColoana(vCorelatieCamp1, pUnitate, pGospodarie, pAn), ConstruiesteColoana(vCorelatieCamp1), Convert.ToInt16(pAn));
            }


            //            string vValoareCamp1String = ManipuleazaBD.fRezultaUnStringScalar(ConstruiesteSelectColoana(vCorelatieCamp1, pUnitate, pGospodarie, pAn), ConstruiesteColoana(vCorelatieCamp1));
            if (vValoareCamp1String == "") { vValoareCamp1String = "0"; }
            decimal vValoareCamp1 = Convert.ToDecimal(vValoareCamp1String);
            int vUMFactor = 1;
            string vUM = DesfaCampuriInElemente(vCorelatieCamp1)[5];
            if (vUM == "1") { vUMFactor = 100; }
            vValoareCamp1 = vUMFactor * vValoareCamp1;
            // scoatem coloana din camp2 si facem suma valorilor
            decimal vValoareCamp2 = 0;
            foreach (string vCamp2CampSeparat in vCorelatieCampuri2)
            {
                if (vCamp2CampSeparat != "")
                {
                    string vOperatie = DesfaCampuriInElemente(vCamp2CampSeparat)[1];
                    string vValoareColoana = "";
                    // daca sunt parcele rezulta o suma de toate parcelele existente pe categorii
                    // randul il consideram ca echivalent la categoria de folosinta; 
                    if (DesfaCampuriInElemente(vCamp2CampSeparat)[2] == "parcele")
                    {
                        vValoareColoana = ScoateValoriParcele(ConstruiesteSelectColoana(vCamp2CampSeparat, pUnitate, pGospodarie, pAn), ConstruiesteColoana(vCamp2CampSeparat), "parcele", Convert.ToInt16(pAn));
                    }
                    else if (DesfaCampuriInElemente(vCamp2CampSeparat)[2] == "paduri")
                    {
                        vValoareColoana = ScoateValoriParcele(ConstruiesteSelectColoana(vCamp2CampSeparat, pUnitate, pGospodarie, pAn), ConstruiesteColoana(vCamp2CampSeparat), "paduri", Convert.ToInt16(pAn));
                    }
                    // daca sunt capitole
                    else
                    {
                        vValoareColoana = ManipuleazaBD.fRezultaUnString(ConstruiesteSelectColoana(vCamp2CampSeparat, pUnitate, pGospodarie, pAn), ConstruiesteColoana(vCamp2CampSeparat), Convert.ToInt16(pAn));
                    }
                    vUMFactor = 1;
                    vUM = DesfaCampuriInElemente(vCamp2CampSeparat)[5];
                    if (vUM == "1") { vUMFactor = 100; }
                    if (vValoareColoana == "") { vValoareColoana = "0"; }
                    if (vOperatie == "-")
                    {
                        vValoareCamp2 += -vUMFactor * (Convert.ToDecimal(vValoareColoana));
                    }
                    else
                    {
                        vValoareCamp2 += vUMFactor * Convert.ToDecimal(vValoareColoana);
                    }
                }
            }
            string vMesajSemn = "";
            switch (vCorelatieSemn)
            {
                case "0":
                    // =
                    if (vValoareCamp1 != vValoareCamp2) vMesajSemn = " egal cu ";
                    break;
                case "1":
                    // <=
                    if (vValoareCamp1 > vValoareCamp2) vMesajSemn = " mai puţin decât ";
                    break;
                case "2":
                    // >=
                    if (vValoareCamp1 < vValoareCamp2) vMesajSemn = " mai mult decât ";
                    break;
                case "3":
                    // <>
                    if (vValoareCamp1 == vValoareCamp2) vMesajSemn = " diferit de ";
                    break;
                default:
                    break;
            }
            if (vMesajSemn != "")
            {
                string vMesaj = "<span class=\"mesaj_1\">" + AfiseazaStringCorelatie(vCorelatieCamp1, Convert.ToInt16(pAn)) + "</span> trebuie să fie " + vMesajSemn + " <span class=\"mesaj_1\">";
                int vNumarPtSemn = 1;
                foreach (string vCamp2CampSeparat in vCorelatieCampuri2)
                {
                    if (vCamp2CampSeparat != "")
                    {
                        if (vNumarPtSemn != 1)
                        {
                            vMesaj += "<span class=\"mesaj_2\"> " + DesfaCampuriInElemente(vCamp2CampSeparat)[1] + "</span> " + AfiseazaStringCorelatie(vCamp2CampSeparat, Convert.ToInt16(pAn)) + " ";
                        }
                        else
                        {
                            vMesaj += AfiseazaStringCorelatie(vCamp2CampSeparat, Convert.ToInt16(pAn)) + " ";
                        }
                    }
                    vNumarPtSemn++;
                }
                vMesaj += "</span>";
                string vInsert = "INSERT INTO mesajeInvalidari (unitateId, gospodarieId, an, corelatieId, mesajInvalidare) VALUES ('" + pUnitate + "','" + pGospodarie + "','" + pAn + "','" + vCorelatieId + "',N'" + vMesaj + "')";
                ManipuleazaBD.fManipuleazaBD(vInsert, Convert.ToInt16(pAn));
            }
        }

    }
    public static string ScoateValoriParcele(string pInterogare, string pRezultat, string pTabela, short an)
    {
        string vValoare = "";
        Decimal vValoareInt = 0;
        DBConnections connection = new DBConnections(an);

        string strConn = connection.Create();
        SqlConnection con = new SqlConnection(strConn);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = pInterogare.Replace("#tabela#", pTabela);
        con.Open();
        try
        {
            SqlDataReader vRezultat = cmd.ExecuteReader();
            while (vRezultat.Read())
            {
                switch (pTabela)
                {
                    // calculam parcelele in 100*ha + 100*ha + ari + ari
                    case "parcele":
                        vValoareInt += 100 * Convert.ToDecimal(vRezultat[pRezultat.Substring(0, pRezultat.IndexOf('#'))]);
                        vValoareInt += 100 * Convert.ToDecimal(vRezultat[pRezultat.Substring(pRezultat.IndexOf('#') + 1, pRezultat.IndexOf("##") - pRezultat.IndexOf('#') - 1)]);
                        vValoareInt += Convert.ToDecimal(vRezultat[pRezultat.Substring(pRezultat.IndexOf("##") + 2, pRezultat.IndexOf("###") - pRezultat.IndexOf("##") - 2)]);
                        vValoareInt += Convert.ToDecimal(vRezultat[pRezultat.Substring(pRezultat.IndexOf("###") + 3)]);
                        break;
                    case "paduri":
                        vValoareInt += 100 * Convert.ToDecimal(vRezultat[pRezultat.Substring(0, pRezultat.IndexOf('#'))]);
                        vValoareInt += Convert.ToDecimal(vRezultat[pRezultat.Substring(pRezultat.IndexOf("#") + 1)]);
                        //vValoareInt += Convert.ToDecimal(vRezultat[pRezultat]);
                        break;
                }

            }
            vRezultat.Close();
        }
        catch { }
        con.Close();
        return vValoare = vValoareInt.ToString();

    }
    public static void VerificaParcele()
    {
        // total suprafata pe categorii = 2a 
    }
    public static void VerificaPaduri()
    {

    }


    public static string ConstruiesteColoana(string pCamp)
    {
        string[] vElementeCamp = DesfaCampuriInElemente(pCamp);
        string vColoana = "";
        switch (vElementeCamp[2])
        {
            case "parcele":
                switch (vElementeCamp[4])
                {
                    case "1":
                        vColoana = "parcelaSuprafataIntravilanHa#parcelaSuprafataExtravilanHa##parcelaSuprafataIntravilanAri###parcelaSuprafataExtravilanAri";
                        break;
                    case "2":
                        vColoana = "parcelaSuprafataIntravilanAri#parcelaSuprafataExtravilanAri";
                        break;
                }
                break;
            case "paduri":
                switch (vElementeCamp[4])
                {
                    case "1":
                        vColoana = "paduriSuprafataHa#paduriSuprafataAri";
                        break;
                    case "2":
                        vColoana = "paduriSuprafataAri";
                        break;
                }
                break;
            default:
                vColoana = "col" + vElementeCamp[4];
                break;
        }

        return vColoana;
    }
    public static string ConstruiesteSelectColoana(string pCamp, int pUnitate, int pGospodarie, int pAn)
    {
        string[] vElementeCamp = DesfaCampuriInElemente(pCamp);
        string vColoana = "col" + vElementeCamp[4];
        string vCapitol = vElementeCamp[2];
        string vRand = vElementeCamp[3];
        string vSelect = "";
        switch (vCapitol)
        {
            case "parcele":
                // randul il interpretam ca o categorie de folosinta
                switch (vElementeCamp[4])
                {
                    case "1":
                        vColoana = "parcelaSuprafataIntravilanHa, parcelaSuprafataExtravilanHa";
                        break;
                    case "2":
                        vColoana = "parcelaSuprafataIntravilanAri, parcelaSuprafataExtravilanAri";
                        break;
                }
                // vSelect = "SELECT " + vColoana + " FROM #tabela# WHERE unitateaId = '" + pUnitate + "' AND gospodarieId = '" + pGospodarie + "' AND an = '" + pAn + "' AND parcelaCategorie = '" + vRand + "'";
                vSelect = "SELECT parcelaSuprafataIntravilanHa, parcelaSuprafataExtravilanHa, parcelaSuprafataIntravilanAri, parcelaSuprafataExtravilanAri FROM #tabela# WHERE unitateaId = '" + pUnitate + "' AND gospodarieId = '" + pGospodarie + "' AND an = '" + pAn + "' AND parcelaCategorie = '" + vRand + "'";
                break;
            case "paduri":
                // randul il interpretam ca si categorie de folosinta
                switch (vElementeCamp[4])
                {
                    case "1":
                        vColoana = "paduriSuprafataHa";
                        break;
                    case "2":
                        vColoana = "paduriSuprafataAri";
                        break;
                }
                //vSelect = "SELECT " + vColoana + " FROM #tabela# WHERE unitateId = '" + pUnitate + "' AND gospodarieId = '" + pGospodarie + "' AND an = '" + pAn + "' "; 
                vSelect = "SELECT paduriSuprafataHa, paduriSuprafataAri FROM #tabela# WHERE unitateId = '" + pUnitate + "' AND gospodarieId = '" + pGospodarie + "' AND an = '" + pAn + "' ";
                break;
            default:
                vSelect = "SELECT TOP(1) " + vColoana + " FROM capitole WHERE unitateId = '" + pUnitate + "' AND gospodarieId = '" + pGospodarie + "' AND an = '" + pAn + "' AND codCapitol='" + vCapitol + "' AND codRand='" + vRand + "'";
                break;
        }
        return vSelect;
    }
    public static int VerificaLinieCorelatie(string pColoanaDeValidat)
    {
        int s = 2;
        return s;
    }
    protected static string UnitateDeMasura(string pValoare)
    {
        if (pValoare == "1") { pValoare = "ha"; }
        else if (pValoare == "2") { pValoare = "ari"; }
        else { pValoare = "-"; }
        return pValoare;
    }
    public static string AfiseazaStringCorelatie(string pCamp, short an)
    {
        string[] vCampuriIndividuale = pCamp.Split('.');
        string vCampuriAfisate = "";
        int vContor = 0;
        char[] vDelimitatoare = { '^', '*', '#', ';', '!', '.' };
        foreach (string vCamp in vCampuriIndividuale)
        {
            string[] vCampuriSparte = vCamp.Split(vDelimitatoare);
            if (vCamp != "")
            {
                switch (vCampuriSparte[2])
                {
                    case "parcele":
                        if (vContor == 0)
                        {
                            vCampuriAfisate = "Cap.2b, suma parcelelor din categoria " + ManipuleazaBD.fRezultaUnString("SELECT [sabloaneCapitole].[denumire4] FROM sabloaneCapitole WHERE ([sabloaneCapitole].[capitol] = '2a') AND ([sabloaneCapitole].[codRand]='" + vCampuriSparte[3] + "' ) ", "denumire4", Convert.ToInt16(an)) + ", extravilan + intravilan   (ha+ari)";
                        }
                        else if (vCamp != "")
                        {
                            vCampuriAfisate += " " + vCampuriSparte[1] + "Cap.2b, suma parcelelor din categoria " + ManipuleazaBD.fRezultaUnString("SELECT [sabloaneCapitole].[denumire4] FROM sabloaneCapitole WHERE ([sabloaneCapitole].[capitol] = '2a') AND ([sabloaneCapitole].[codRand]='" + vCampuriSparte[3] + "' ) ", "denumire4", an) + ", extravilan + intravilan  (ha+ari)";
                        }
                        else { break; }
                        vContor++;

                        break;
                    case "paduri":
                        if (vContor == 0)
                        {
                            vCampuriAfisate = "Cap.2c,  suma pădurilor (ha+ari)";
                        }
                        else if (vCamp != "")
                        {
                            vCampuriAfisate += " " + vCampuriSparte[1] + " Cap.2c, suma pădurilor (ha+ari)";
                        }
                        else { break; }
                        vContor++;
                        break;
                    default:
                        if (vContor == 0)
                        {
                            vCampuriAfisate = "Cap." + vCampuriSparte[2] + ", rândul " + vCampuriSparte[3] + ", col. " + vCampuriSparte[4] + "(" + UnitateDeMasura(vCampuriSparte[5]) + ")";
                        }
                        else if (vCamp != "")
                        {
                            vCampuriAfisate += " " + vCampuriSparte[1] + " Cap." + vCampuriSparte[2] + ", rândul " + vCampuriSparte[3] + ", col. " + vCampuriSparte[4] + "(" + UnitateDeMasura(vCampuriSparte[5]) + ")";
                        }
                        else { break; }
                        vContor++;
                        break;
                }
            }
        }

        return vCampuriAfisate;
    }
    public static string AfiseazaSemnCorelatie(string pCamp)
    {
        string vCampAfisat = "";
        switch (pCamp)
        {
            case "0":
                vCampAfisat = "=";
                break;
            case "1":
                vCampAfisat = "<=";
                break;
            case "2":
                vCampAfisat = ">=";
                break;
            case "3":
                vCampAfisat = "<>";
                break;
            default:
                vCampAfisat = "=";
                break;
        }
        return vCampAfisat;
    }
    public static string[] DesfaCorelatieInCampuri(string pCorelatie)
    {
        string[] vCampuriIndividuale = pCorelatie.Split('.'); // impartim in campuri
        return vCampuriIndividuale;
    }
    public static string[] DoarCampuriCautate(string[] pToateCampurile, string pCampCautat)
    {

        /// nefinalizat !!! nu folosi!
        List<string> lCampuriCautate = new List<string> { };
        foreach (string vCampCautat in pToateCampurile)
        {
            if (pCampCautat.Contains(pCampCautat))
            {
                lCampuriCautate.Add(pCampCautat);
            }
        }
        return pToateCampurile;
    }
    public static string[] DesfaCampuriInElemente(string pCamp)
    {
        char[] vDelimitatoare = { '^', '*', '#', ';', '!', '.' };
        //^+*2a#5;4!1.^+*2a#6;4!1.   [0]'' ^    [1]+        * [2]2a     # [3]5      ; [4]4 !    [5]1 .[6]'' 
        //                          ----        operatia    cap         rand        col         UM
        string[] vElemente = pCamp.Split(vDelimitatoare); //impartim in elemente [1] = operatia, [2] = cap, [3] = rand, [4] = col [5] = UM
        return vElemente;
    }
    public static int NumarCorelatiiInvalidate(string pGospodarieId, string pUnitateId, string pAn)
    {
        int vNumar = Convert.ToInt32(ManipuleazaBD.fRezultaUnString("SELECT COUNT(*) as numar FROM mesajeInvalidari WHERE gospodarieId = '" + pGospodarieId + "' AND an = '" + pAn + "'", "numar", Convert.ToInt16(pAn)));
        return vNumar;
    }
}
