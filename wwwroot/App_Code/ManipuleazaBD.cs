﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using System.Web;

/// <summary>
/// Manipuleaza baza de date
/// Creata la:                  26.01.2011
/// Autor:                      Sebastian Muntean
/// </summary>
public class ManipuleazaBD
{
    public static SqlConnection CreareConexiune(short an)
    {
        DBConnections connection = new DBConnections(an);

        SqlConnection vCon = new SqlConnection(connection.Create());
        vCon.Open();
        return vCon;
    }

    public static SqlConnection CreareConexiune()
    {
        DBConnections connection = new DBConnections(2015);

        SqlConnection vCon = new SqlConnection(connection.Create());
        vCon.Open();
        return vCon;
    }

    public static void InchideConexiune(SqlConnection pCon)
    {
        pCon.Close();
    }

    public static SqlTransaction DeschideTranzactieFaraRestrictii(SqlConnection pCon, IsolationLevel pIsolation)
    {
        SqlTransaction vTranz = pCon.BeginTransaction();
        return vTranz;
    }

    public static SqlTransaction DeschideTranzactie(SqlConnection pCon)
    {
        SqlTransaction vTranz = pCon.BeginTransaction();
        return vTranz;
    }

    /* public static void fManipuleazaBDCuId(string pInterogare, out int pId)
     {
         string strConn = ConfigurationManager.ConnectionStrings["TNTRegistruAgricolConnectionString"].ConnectionString;
         SqlConnection con = new SqlConnection(strConn);
         SqlCommand cmd = new SqlCommand();
         cmd.Connection = con;
         cmd.CommandType = System.Data.CommandType.Text;
         cmd.CommandText = pInterogare.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
         con.Open();
         try
         {
             pId = (Int32)cmd.ExecuteScalar(); // id-ul inserat
             //cmd.ExecuteNonQuery();
         }
         catch { pId = -1; }
         con.Close();
     }*/
    public static void fManipuleazaBDCuId(string pInterogare, out int pId, short an)
    {
        DBConnections connection = new DBConnections(an);

        string strConn = connection.Create();
        SqlConnection con = new SqlConnection(strConn);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandType = System.Data.CommandType.Text;
        //  cmd.CommandText = pInterogare.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
        cmd.CommandText = pInterogare.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş") + "; SELECT CAST(scope_identity() AS int)";
        using (con)
        {
            con.Open();
            try
            {
                pId = (Int32)cmd.ExecuteScalar(); // id-ul inserat
                                                  //cmd.ExecuteNonQuery();
            }
            catch (Exception x) { pId = -1; }
            con.Close();
        }
    }


    public static void fManipuleazaBD(string pInterogare, short an)
    {
        DBConnections connection = new DBConnections(an);

        SqlConnection con = new SqlConnection(connection.Create());
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = pInterogare.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
        cmd.CommandTimeout = 1000000;
        using (con)
        {
            con.Open();
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception x)
            {
                ClassLog.fLog(Convert.ToInt32(HttpContext.Current.Session["SESunitateId"]), Convert.ToInt32(HttpContext.Current.Session["SESutilizatorId"]), DateTime.Now, "eroare fManipuleazaBD", "", pInterogare.Replace("'", "_") + " |||||| " + x.Message.Replace("'", "_"), 0, 0);
            }

            con.Close();
        }
    }
    public static void fManipuleazaBD(string pInterogare, short an, out int numarRanduriAfectate)
    {
        numarRanduriAfectate = 0;
        DBConnections connection = new DBConnections(an);

        SqlConnection con = new SqlConnection(connection.Create());
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = pInterogare.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
        cmd.CommandTimeout = 600;
        using (con)
        {
            con.Open();
            try
            {
                numarRanduriAfectate = cmd.ExecuteNonQuery();
            }
            catch (Exception x) { ClassLog.fLog(Convert.ToInt32(HttpContext.Current.Session["SESunitateId"]), Convert.ToInt32(HttpContext.Current.Session["SESutilizatorId"]), DateTime.Now, "eroare fManipuleazaBD", pInterogare.Replace("'", "*").Substring(0, 100), x.ToString(), 0, 0); }

            con.Close();
        }
    }

    public static void fManipuleazaBDCateRanduri(string pInterogare, out int pId, short an)
    {
        DBConnections connection = new DBConnections(an);

        string strConn = connection.Create();
        SqlConnection con = new SqlConnection(strConn);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = pInterogare.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş") + "SELECT @@rowcount AS 'RowsChanged'";
        using (con)
        {
            con.Open();
            try
            {
                pId = (Int32)cmd.ExecuteScalar();
                //cmd.ExecuteNonQuery();
            }
            catch (Exception x)
            {
                ClassLog.fLog(Convert.ToInt32(HttpContext.Current.Session["SESunitateId"]), Convert.ToInt32(HttpContext.Current.Session["SESutilizatorId"]), DateTime.Now, "eroare fManipuleazaBD", pInterogare.Replace("'", "*").Substring(0, 100), x.ToString(), 0, 0);
                pId = -1;
            }
            con.Close();
        }
    }
    public static void fManipuleazaBDArray(int TipOperatie, string pTabela, string[] pCampuri, string[] pValori, string pCampId, int pId, short an)
    {
        // tip operatie 0 -INSERT  , 1 - UPDATE
        DBConnections connection = new DBConnections(an);

        string strConn = connection.Create();
        SqlConnection con = new SqlConnection(strConn);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandType = System.Data.CommandType.Text;
        string vInterogare = "";
        // realizam interorarea pt INSERT
        if (TipOperatie == 0)
        {
            vInterogare = "INSERT INTO " + pTabela + " (";
            if (pCampuri.Length == pValori.Length)
            {
                int vContor = 0;
                foreach (string vElementDinPCampuri in pCampuri)
                {
                    if (vContor == 0) { vInterogare += vElementDinPCampuri; }
                    else { vInterogare += ", " + vElementDinPCampuri; }
                    vContor++;
                }
                vInterogare += ") VALUES (";
                vContor = 0;
                // daca vreuna din valori e tip data, conventia este ca stringul acesteia sa inceapa cu ### si atunci o convertim
                foreach (string vElementDinPCamp in pCampuri)
                {
                    if (vContor == 0)
                    {
                        if (pValori[vContor].IndexOf("###") == 0)
                        {
                            vInterogare += "CONVERT(DATETIME,'" + pValori[vContor].Replace("###", String.Empty) + "', 104)";
                        }
                        else
                        {
                            vInterogare += "N'" + pValori[vContor] + "'";
                        }
                    }
                    else
                    {
                        if (pValori[vContor].IndexOf("###") == 0)
                        {
                            vInterogare += ", CONVERT(DATETIME,'" + pValori[vContor].Replace("###", String.Empty) + "', 104)";
                        }
                        else
                        {
                            vInterogare += ", N'" + pValori[vContor] + "'";
                        }
                    }
                    vContor++;
                }
                vInterogare += ")";
            }
        }
        else
        {
            vInterogare = "UPDATE " + pTabela + " SET ";
            if (pCampuri.Length == pValori.Length)
            {
                int vContor = 0;
                foreach (string vElementDinPCampuri in pCampuri)
                {
                    if (vContor == 0)
                    {
                        if (pValori[vContor].IndexOf("###") == 0)
                        {
                            //pValori[vContor].Replace("###", String.Empty);
                            vInterogare += vElementDinPCampuri + "=CONVERT(DATETIME,'" + pValori[vContor].Replace("###", String.Empty) + "', 104) ";
                        }
                        else
                        {
                            vInterogare += vElementDinPCampuri + "=N'" + pValori[vContor] + "'";
                        }
                    }
                    else
                    {
                        if (pValori[vContor].IndexOf("###") == 0)
                        {

                            vInterogare += ", " + vElementDinPCampuri + "=CONVERT(DATETIME,'" + pValori[vContor].Replace("###", String.Empty) + "', 104) ";
                        }
                        else
                        {
                            vInterogare += ", " + vElementDinPCampuri + "=N'" + pValori[vContor] + "'";
                        }
                    }
                    vContor++;
                }
                vInterogare += " WHERE " + pCampId + " = '" + pId + "'";
            }
        }
        cmd.CommandText = vInterogare.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
        con.Open();
        try
        {
            cmd.ExecuteNonQuery();
        }
        catch (Exception x) { ClassLog.fLog(Convert.ToInt32(HttpContext.Current.Session["SESunitateId"]), Convert.ToInt32(HttpContext.Current.Session["SESutilizatorId"]), DateTime.Now, "eroare fManipuleazaBDArray", vInterogare.Replace("'", "*").Substring(0, 100), x.ToString(), 0, 0); }
        con.Close();
    }

    public static void fManipuleazaBDArray(int TipOperatie, string pTabela, List<string> pCampuri, List<string> pValori, List<string> pCampuriId, List<string> pValoriId, short an)
    {
        // tip operatie 0 -INSERT  , 1 - UPDATE
        DBConnections connection = new DBConnections(an);

        string strConn = connection.Create();
        SqlConnection con = new SqlConnection(strConn);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandType = System.Data.CommandType.Text;
        string vInterogare = "";
        // realizam interorarea pt INSERT
        if (TipOperatie == 0)
        {
            vInterogare = "INSERT INTO " + pTabela + " (";
            if (pCampuri.Count() == pValori.Count())
            {
                int vContor = 0;
                foreach (string vElementDinPCampuri in pCampuri)
                {
                    if (vContor == 0) { vInterogare += vElementDinPCampuri; }
                    else { vInterogare += ", " + vElementDinPCampuri; }
                    vContor++;
                }
                vInterogare += ") VALUES (";
                vContor = 0;
                // daca vreuna din valori e tip data, conventia este ca stringul acesteia sa inceapa cu ### si atunci o convertim
                foreach (string vElementDinPCamp in pCampuri)
                {
                    if (vContor == 0)
                    {
                        if (pValori[vContor].IndexOf("###") == 0)
                        {
                            vInterogare += "CONVERT(DATETIME,'" + pValori[vContor].Replace("###", String.Empty) + "', 104)";
                        }
                        else
                        {
                            vInterogare += "'" + pValori[vContor] + "'";
                        }
                    }
                    else
                    {
                        if (pValori[vContor].IndexOf("###") == 0)
                        {
                            vInterogare += ", CONVERT(DATETIME,'" + pValori[vContor].Replace("###", String.Empty) + "', 104)";
                        }
                        else
                        {
                            vInterogare += ", '" + pValori[vContor] + "'";
                        }
                    }
                    vContor++;
                }
                vInterogare += ")";
            }
        }
        else
        {
            vInterogare = "UPDATE " + pTabela + " SET ";
            if (pCampuri.Count() == pValori.Count())
            {
                int vContor = 0;
                foreach (string vElementDinPCampuri in pCampuri)
                {
                    if (vContor == 0)
                    {
                        if (pValori[vContor].IndexOf("###") == 0)
                        {
                            //pValori[vContor].Replace("###", String.Empty);
                            vInterogare += vElementDinPCampuri + "=CONVERT(DATETIME,'" + pValori[vContor].Replace("###", String.Empty) + "', 104) ";
                        }
                        else
                        {
                            vInterogare += vElementDinPCampuri + "='" + pValori[vContor] + "'";
                        }
                    }
                    else
                    {
                        if (pValori[vContor].IndexOf("###") == 0)
                        {

                            vInterogare += ", " + vElementDinPCampuri + "=CONVERT(DATETIME,'" + pValori[vContor].Replace("###", String.Empty) + "', 104) ";
                        }
                        else
                        {
                            vInterogare += ", " + vElementDinPCampuri + "='" + pValori[vContor] + "'";
                        }
                    }
                    vContor++;
                }

                // zona WHERE
                int vContorWhere = 0;
                vInterogare += " WHERE ";
                foreach (string vCampId in pCampuriId)
                {
                    if (vContorWhere == 0)
                    {
                        vInterogare += vCampId + " = '" + pValoriId[vContorWhere] + "'";
                    }
                    else
                    {
                        vInterogare += " AND " + vCampId + " = '" + pValoriId[vContorWhere] + "'";
                    }
                    vContorWhere++;
                }


            }
        }
        cmd.CommandText = vInterogare.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
        con.Open();
        try
        {
            cmd.ExecuteNonQuery();
        }
        catch (Exception x) { ClassLog.fLog(Convert.ToInt32(HttpContext.Current.Session["SESunitateId"]), Convert.ToInt32(HttpContext.Current.Session["SESutilizatorId"]), DateTime.Now, "eroare fManipuleazaBDArray", vInterogare.Replace("'", "*").Substring(0, 100), x.ToString(), 0, 0); }
        con.Close();
    }

    public static void fManipuleazaBDArray(out int pId, string pTabela, List<string> pCampuri, List<string> pValori, short an)
    {
        // tip operatie 0 -INSERT 
        // returnam si ID - doar pentru insert
        DBConnections connection = new DBConnections(an);

        string strConn = connection.Create();
        SqlConnection con = new SqlConnection(strConn);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandType = System.Data.CommandType.Text;
        string vInterogare = "";
        // realizam interorarea pt INSERT
        vInterogare = "INSERT INTO " + pTabela + " (";
        if (pCampuri.Count() == pValori.Count())
        {
            int vContor = 0;
            foreach (string vElementDinPCampuri in pCampuri)
            {
                if (vContor == 0) { vInterogare += vElementDinPCampuri; }
                else { vInterogare += ", " + vElementDinPCampuri; }
                vContor++;
            }
            vInterogare += ") VALUES (";
            vContor = 0;
            // daca vreuna din valori e tip data, conventia este ca stringul acesteia sa inceapa cu ### si atunci o convertim
            foreach (string vElementDinPCamp in pCampuri)
            {
                if (vContor == 0)
                {
                    if (pValori[vContor].IndexOf("###") == 0)
                    {
                        vInterogare += "CONVERT(DATETIME,'" + pValori[vContor].Replace("###", String.Empty) + "', 104)";
                    }
                    else
                    {
                        vInterogare += "'" + pValori[vContor] + "'";
                    }
                }
                else
                {
                    if (pValori[vContor].IndexOf("###") == 0)
                    {
                        vInterogare += ", CONVERT(DATETIME,'" + pValori[vContor].Replace("###", String.Empty) + "', 104)";
                    }
                    else
                    {
                        vInterogare += ", '" + pValori[vContor] + "'";
                    }
                }
                vContor++;
            }
            vInterogare += "); SELECT CAST(scope_identity() AS int)";
        }


        cmd.CommandText = vInterogare.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
        using (con)
        {
            con.Open();
            try
            {
                pId = (Int32)cmd.ExecuteScalar(); // id-ul inserat
                                                  //cmd.ExecuteNonQuery();
            }
            catch (Exception x)
            {
                ClassLog.fLog(Convert.ToInt32(HttpContext.Current.Session["SESunitateId"]), Convert.ToInt32(HttpContext.Current.Session["SESutilizatorId"]), DateTime.Now, "eroare fManipuleazaBDArray", vInterogare.Replace("'", "*").Substring(0, 100), x.ToString(), 0, 0);
                pId = -1;
            }

            con.Close();
        }
        // return pId;
    }
    public static string CreeazaComanda(int TipOperatie, string pTabela, List<string> pCampuri, List<string> pValori, List<string> pCampuriId, List<string> pValoriId, short an)
    {
        // tip operatie 0 -INSERT  , 1 - UPDATE
        DBConnections connection = new DBConnections(an);

        string strConn = connection.Create();
        SqlConnection con = new SqlConnection(strConn);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandType = System.Data.CommandType.Text;
        string vInterogare = "";
        // realizam interorarea pt INSERT
        if (TipOperatie == 0)
        {
            vInterogare = "INSERT INTO " + pTabela + " (";
            if (pCampuri.Count() == pValori.Count())
            {
                int vContor = 0;
                foreach (string vElementDinPCampuri in pCampuri)
                {
                    if (vContor == 0) { vInterogare += vElementDinPCampuri; }
                    else { vInterogare += ", " + vElementDinPCampuri; }
                    vContor++;
                }
                vInterogare += ") VALUES (";
                vContor = 0;
                // daca vreuna din valori e tip data, conventia este ca stringul acesteia sa inceapa cu ### si atunci o convertim
                foreach (string vElementDinPCamp in pCampuri)
                {
                    if (vContor == 0)
                    {
                        if (pValori[vContor].IndexOf("###") == 0)
                        {
                            vInterogare += "CONVERT(DATETIME,'" + pValori[vContor].Replace("###", String.Empty) + "', 104)";
                        }
                        else
                        {
                            vInterogare += "'" + pValori[vContor] + "'";
                        }
                    }
                    else
                    {
                        if (pValori[vContor].IndexOf("###") == 0)
                        {
                            vInterogare += ", CONVERT(DATETIME,'" + pValori[vContor].Replace("###", String.Empty) + "', 104)";
                        }
                        else
                        {
                            vInterogare += ", '" + pValori[vContor] + "'";
                        }
                    }
                    vContor++;
                }
                vInterogare += ")";
            }
        }
        else
        {
            vInterogare = "UPDATE " + pTabela + " SET ";
            if (pCampuri.Count() == pValori.Count())
            {
                int vContor = 0;
                foreach (string vElementDinPCampuri in pCampuri)
                {
                    if (vContor == 0)
                    {
                        if (pValori[vContor].IndexOf("###") == 0)
                        {
                            //pValori[vContor].Replace("###", String.Empty);
                            vInterogare += vElementDinPCampuri + "=CONVERT(DATETIME,'" + pValori[vContor].Replace("###", String.Empty) + "', 104) ";
                        }
                        else
                        {
                            vInterogare += vElementDinPCampuri + "='" + pValori[vContor] + "'";
                        }
                    }
                    else
                    {
                        if (pValori[vContor].IndexOf("###") == 0)
                        {

                            vInterogare += ", " + vElementDinPCampuri + "=CONVERT(DATETIME,'" + pValori[vContor].Replace("###", String.Empty) + "', 104) ";
                        }
                        else
                        {
                            vInterogare += ", " + vElementDinPCampuri + "='" + pValori[vContor] + "'";
                        }
                    }
                    vContor++;
                }
                // zona WHERE
                int vContorWhere = 0;
                vInterogare += " WHERE ";
                foreach (string vCampId in pCampuriId)
                {
                    if (vContorWhere == 0)
                    {
                        vInterogare += vCampId + " = '" + pValoriId[vContorWhere] + "'";
                    }
                    else
                    {
                        vInterogare += " AND " + vCampId + " = '" + pValoriId[vContorWhere] + "'";
                    }
                    vContorWhere++;
                }
            }
        }
        return vInterogare.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
    }

    public static int fRezultaUnMaximInt(string pTabela, string pCamp, short an)
    {
        DBConnections connection = new DBConnections(an);

        string strConn = connection.Create();
        SqlConnection con = new SqlConnection(strConn);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = "SELECT MAX(" + pCamp + ") as maxim FROM " + pTabela;
        int vMaxim = -1;
        con.Open();
        try
        {
            SqlDataReader vRezultat = cmd.ExecuteReader();
            if (vRezultat.Read())
            {
                vMaxim = Convert.ToInt32(vRezultat["maxim"]);
            }
        }
        catch (Exception x)
        {
            ClassLog.fLog(Convert.ToInt32(HttpContext.Current.Session["SESunitateId"]), Convert.ToInt32(HttpContext.Current.Session["SESutilizatorId"]), DateTime.Now, "eroare fRezultaUnMaximInt", pTabela + " | " + pCamp, x.ToString(), 0, 0);
        }
        con.Close();
        return vMaxim;
    }

    public static int fRezultaUnMaximInt(string pTabela, string pCamp, string pWhereCamp, string pWhereValoare, string pWhereConditional, short an)
    {
        DBConnections connection = new DBConnections(an);

        string strConn = connection.Create();
        SqlConnection con = new SqlConnection(strConn);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = "SELECT MAX(" + pCamp + ") as maxim FROM " + pTabela + " WHERE " + pWhereCamp + pWhereConditional + " '" + pWhereValoare + "' ";
        int vMaxim = -1;
        using (con)
        {
            con.Open();
            try
            {
                SqlDataReader vRezultat = cmd.ExecuteReader();
                if (vRezultat.Read())
                {
                    vMaxim = Convert.ToInt32(vRezultat["maxim"]);
                }
            }
            catch (Exception x)
            {
                ClassLog.fLog(Convert.ToInt32(HttpContext.Current.Session["SESunitateId"]), Convert.ToInt32(HttpContext.Current.Session["SESutilizatorId"]), DateTime.Now, "eroare fRezultaUnMaximInt", pTabela + " | " + pCamp, x.ToString(), 0, 0);
            }
            con.Close();
        }
        return vMaxim;
    }
    public static int fRezultaUnMaximInt(string pTabela, string pCamp, string[] pWhereCampuri, string[] pWhereValori, string[] pWhereConditionale, string[] pWhereLogici, short an)
    {
        int vMaxim = -1;
        DBConnections connection = new DBConnections(an);

        string strConn = connection.Create();
        SqlConnection con = new SqlConnection(strConn);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = "SELECT MAX(" + pCamp + ") as maxim FROM " + pTabela + " WHERE ";
        if (pWhereCampuri.Length == pWhereValori.Length)
        {
            int vContor = 0;
            foreach (string vElementDinPCampuri in pWhereCampuri)
            {
                if (vContor == 0)
                {
                    cmd.CommandText += vElementDinPCampuri + pWhereConditionale[vContor] + " '" + pWhereValori[vContor] + "' ";
                }
                else
                {
                    cmd.CommandText += " " + pWhereLogici[vContor] + " " + vElementDinPCampuri + pWhereConditionale[vContor] + " '" + pWhereValori[vContor] + "' ";
                }
                vContor++;
            }
            con.Open();
            try
            {
                SqlDataReader vRezultat = cmd.ExecuteReader();
                if (vRezultat.Read())
                {
                    vMaxim = Convert.ToInt32(vRezultat["maxim"]);
                }
            }
            catch { }
            con.Close();
        }
        return vMaxim;
    }

    public static string fRezultaUnString(string pInterogare, string pRezultat, short an)
    {
        // primeste o interogare si un camp (denumirea din bd) ce trebuie returnat
        // returneaza un string cu valoarea campului specificat
        // selectul trebuie sa ceara o singura valoare ca rezultat
        string vValoare = "";
        DBConnections connection = new DBConnections(an);

        string strConn = connection.Create();
        SqlConnection con = new SqlConnection(strConn);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = pInterogare.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
        using (con)
        {
            con.Open();
            try
            {
                SqlDataReader vRezultat = cmd.ExecuteReader();
                if (vRezultat.Read())
                {
                    vValoare = vRezultat[pRezultat].ToString();
                }
                vRezultat.Close();
            }
            catch (Exception x)
            {
                ClassLog.fLog(Convert.ToInt32(HttpContext.Current.Session["SESunitateId"]), Convert.ToInt32(HttpContext.Current.Session["SESutilizatorId"]), DateTime.Now, "eroare fRezultaUnString", pInterogare.Replace("'", "*").Substring(0, 100) + " | " + pRezultat, x.ToString(), 0, 0);
            }

            con.Close();
        }
        return vValoare;
    }


    public static string fRezultaUnString(string pInterogare, string pRezultat)
    {
        // primeste o interogare si un camp (denumirea din bd) ce trebuie returnat
        // returneaza un string cu valoarea campului specificat
        // selectul trebuie sa ceara o singura valoare ca rezultat
        string vValoare = "";
        Int16 an = HttpContext.Current.Session["SESan"] != null ? Convert.ToInt16(HttpContext.Current.Session["SESan"]) : Convert.ToInt16(DateTime.Now.Year); 

        DBConnections connection = new DBConnections(an);

        string strConn = connection.Create();
        SqlConnection con = new SqlConnection(strConn);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = pInterogare.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
        using (con)
        {
            con.Open();
            try
            {
                SqlDataReader vRezultat = cmd.ExecuteReader();
                if (vRezultat.Read())
                {
                    vValoare = vRezultat[pRezultat].ToString();
                }
                vRezultat.Close();
            }
            catch (Exception x)
            {
                ClassLog.fLog(Convert.ToInt32(HttpContext.Current.Session["SESunitateId"]), Convert.ToInt32(HttpContext.Current.Session["SESutilizatorId"]), DateTime.Now, "eroare fRezultaUnString", pInterogare.Replace("'", "*").Substring(0, 100) + " | " + pRezultat, x.ToString(), 0, 0);
            }

            con.Close();
        }
        return vValoare;
    }

    public static string fRezultaUnStringScalar(string pInterogare, string pRezultat, short an)
    {
        // primeste o interogare si un camp (denumirea din bd) ce trebuie returnat
        // returneaza un string cu valoarea campului specificat
        // selectul trebuie sa ceara o singura valoare ca rezultat
        string vValoare = "";
        DBConnections connection = new DBConnections(an);

        string strConn = connection.Create();
        SqlConnection con = new SqlConnection(strConn);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = pInterogare.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
        using (con)
        {
            con.Open();
            try
            {
                vValoare = cmd.ExecuteScalar().ToString();
            }
            catch { }

            con.Close();
        }
        return vValoare;
    }

    public static List<string> fRezultaListaStringuri(string pInterogare, string pRezultat, short an)
    {
        // primeste o interogare si un camp (denumirea din bd) ce trebuie returnat
        // returneaza lista string cu valoarile campului specificat
        // selectul cere nelimitate valori ca rezultat
        List<string> vValoare = new List<string> { };
        DBConnections connection = new DBConnections(an);

        string strConn = connection.Create();
        SqlConnection con = new SqlConnection(strConn);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = pInterogare.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
        using (con)
        {
            con.Open();
            try
            {
                SqlDataReader vRezultat = cmd.ExecuteReader();
                while (vRezultat.Read())
                {
                    vValoare.Add(vRezultat[pRezultat].ToString());
                }
            }
            catch { }
            con.Close();
        }
        return vValoare;
    }
    public static List<List<string>> fRezultaListaStringuri(string pInterogare, List<string> pRezultate, short an)
    {
        // primeste o interogare si o lista de campuri trebuie returnat ca Lista de liste

        List<List<string>> vValori = new List<List<string>> { }; // lista de stringuri mare
        List<string> vValoare = new List<string> { }; // lista de stringuri interioara
        DBConnections connection = new DBConnections(an);

        string strConn = connection.Create();
        SqlConnection con = new SqlConnection(strConn);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = pInterogare.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
        using (con)
        {
            con.Open();
            try
            {
                SqlDataReader vRezultat = cmd.ExecuteReader();
                while (vRezultat.Read())
                {
                    vValoare = new List<string> { };

                    // citim toate campurile rezultate in fiecare inregistrare
                    foreach (string vCamp in pRezultate)
                    {
                        vValoare.Add(vRezultat[vCamp].ToString());
                    }
                    vValori.Add(vValoare);
                }
            }
            catch (Exception x) { }
            con.Close();
        }
        return vValori;
    }
    public static List<List<string>> fRezultaListaStringuri(string pInterogare, List<string> pRezultate, out string pMesaj, short an)
    {
        // primeste o interogare si o lista de campuri trebuie returnat ca Lista de liste
        pMesaj = "reusit";
        List<List<string>> vValori = new List<List<string>> { }; // lista de stringuri mare
        List<string> vValoare = new List<string> { }; // lista de stringuri interioara
        DBConnections connection = new DBConnections(an);

        string strConn = connection.Create();
        SqlConnection con = new SqlConnection(strConn);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = pInterogare.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
        using (con)
        {
            con.Open();
            try
            {
                SqlDataReader vRezultat = cmd.ExecuteReader();
                while (vRezultat.Read())
                {
                    vValoare = new List<string> { };

                    // citim toate campurile rezultate in fiecare inregistrare
                    foreach (string vCamp in pRezultate)
                    {
                        vValoare.Add(vRezultat[vCamp].ToString());
                    }
                    vValori.Add(vValoare);
                }
            }
            catch (Exception ex)
            {
                pMesaj = ex.Message;
            }
            con.Close();
        }
        return vValori;
    }

    public static List<string> fRezultaUnString(string pInterogare, string[] pRezultate, short an)
    {
        // primeste o interogare si un camp (denumirea din bd) ce trebuie returnat
        // returneaza un string[] cu valoarea campurilor specificate
        List<string> vValoare = new List<string> { };
        DBConnections connection = new DBConnections(an);

        string strConn = connection.Create();
        SqlConnection con = new SqlConnection(strConn);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = pInterogare.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
        using (con)
        {
            con.Open();
            try
            {
                SqlDataReader vRezultat = cmd.ExecuteReader();
                if (vRezultat.Read())
                {
                    foreach (string vParametru in pRezultate)
                    {
                        vValoare.Add(vRezultat[vParametru].ToString());
                    }
                }
            }
            catch { }
            con.Close();
        }
        return vValoare;
    }

    public static List<string> fRezultaUnString(string pInterogare, List<string> pRezultate, short an)
    {
        // primeste o interogare si un camp (denumirea din bd) ce trebuie returnat
        // returneaza un string[] cu valoarea campurilor specificate
        List<string> vValoare = new List<string> { };
        DBConnections connection = new DBConnections(an);

        string strConn = connection.Create();
        SqlConnection con = new SqlConnection(strConn);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = pInterogare.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
        using (con)
        {
            con.Open();
            try
            {
                SqlDataReader vRezultat = cmd.ExecuteReader();
                if (vRezultat.Read())
                {
                    foreach (string vParametru in pRezultate)
                    {
                        vValoare.Add(vRezultat[vParametru].ToString());
                    }
                }
            }
            catch { }
            con.Close();
        }
        return vValoare;
    }

    public static List<ListaSelect> fSelectCuRezultatMultiplu(string pTabela, string[] pWhereCampuri, string[] pWhereValori, string[] pOperatoriConditionali, string[] pOperatoriLogici, string[] pCampuriRezultate, short an)
    {
        // returneaza un tablou stringuri cu valorile campului specificat
        // selectul poate sa ceara mai multe valori ca rezultat
        string vInterogare = "SELECT ";
        int vContor = 0;
        foreach (string vCampRezultat in pCampuriRezultate)
        {
            if (vContor == 0) { vInterogare += vCampRezultat; }
            else { vInterogare += ", " + vCampRezultat; }
            vContor++;
        }
        vInterogare += " FROM " + pTabela + " WHERE ";
        vContor = 0;
        if (pWhereCampuri.Length == pWhereValori.Length)
        {
            foreach (string vElementDinPWhereCampuri in pWhereCampuri)
            {
                if (vContor == 0)
                {
                    vInterogare += "(" + vElementDinPWhereCampuri;
                    if (pOperatoriConditionali[vContor] != "LIKE")
                    {
                        vInterogare += pOperatoriConditionali[vContor] + "'" + pWhereValori[vContor] + "'";
                    }
                    else
                    {
                        vInterogare += " LIKE '%" + pWhereValori[vContor] + "%' ";
                    }
                    vInterogare += ") ";
                }
                else
                {
                    vInterogare += " " + pOperatoriLogici[vContor] + " ( " + vElementDinPWhereCampuri + " ";
                    if (pOperatoriConditionali[vContor] != "LIKE")
                    {
                        vInterogare += pOperatoriConditionali[vContor] + "'" + pWhereValori[vContor] + "'";
                    }
                    else
                    {
                        vInterogare += " LIKE '%" + pWhereValori[vContor] + "%' ";
                    }
                    vInterogare += ") ";
                }
                vContor++;
            }

        }
        //List<string> vRezultate = new List<string> {};
        List<ListaSelect> vRezultate = new List<ListaSelect>();
        DBConnections connection = new DBConnections(an);

        string strConn = connection.Create();
        SqlConnection con = new SqlConnection(strConn);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = vInterogare.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
        using (con)
        {
            con.Open();
            try
            {
                SqlDataReader vListaRezultate = cmd.ExecuteReader();
                while (vListaRezultate.Read())
                {
                    vContor = 1;
                    //vRezultate.Add(vListaRezultate[pCampRezultat].ToString());
                    ListaSelect vListaSelect = new ListaSelect();
                    foreach (string vCampRezultat in pCampuriRezultate)
                    {
                        switch (vContor)
                        {
                            case 1:
                                vListaSelect.Col1 = vListaRezultate[pCampuriRezultate[vContor - 1]].ToString();
                                break;
                            case 2:
                                vListaSelect.Col2 = vListaRezultate[pCampuriRezultate[vContor - 1]].ToString();
                                break;
                            case 3:
                                vListaSelect.Col3 = vListaRezultate[pCampuriRezultate[vContor - 1]].ToString();
                                break;
                            case 4:
                                vListaSelect.Col4 = vListaRezultate[pCampuriRezultate[vContor - 1]].ToString();
                                break;
                            case 5:
                                vListaSelect.Col5 = vListaRezultate[pCampuriRezultate[vContor - 1]].ToString();
                                break;
                            case 6:
                                vListaSelect.Col6 = vListaRezultate[pCampuriRezultate[vContor - 1]].ToString();
                                break;
                            case 7:
                                vListaSelect.Col7 = vListaRezultate[pCampuriRezultate[vContor - 1]].ToString();
                                break;
                            case 8:
                                vListaSelect.Col8 = vListaRezultate[pCampuriRezultate[vContor - 1]].ToString();
                                break;
                            case 9:
                                vListaSelect.Col9 = vListaRezultate[pCampuriRezultate[vContor - 1]].ToString();
                                break;
                            case 10:
                                vListaSelect.Col10 = vListaRezultate[pCampuriRezultate[vContor - 1]].ToString();
                                break;
                            case 11:
                                vListaSelect.Col11 = vListaRezultate[pCampuriRezultate[vContor - 1]].ToString();
                                break;
                            case 12:
                                vListaSelect.Col12 = vListaRezultate[pCampuriRezultate[vContor - 1]].ToString();
                                break;
                            case 13:
                                vListaSelect.Col13 = vListaRezultate[pCampuriRezultate[vContor - 1]].ToString();
                                break;
                            case 14:
                                vListaSelect.Col14 = vListaRezultate[pCampuriRezultate[vContor - 1]].ToString();
                                break;
                            case 15:
                                vListaSelect.Col15 = vListaRezultate[pCampuriRezultate[vContor - 1]].ToString();
                                break;
                            case 16:
                                vListaSelect.Col16 = vListaRezultate[pCampuriRezultate[vContor - 1]].ToString();
                                break;
                            case 17:
                                vListaSelect.Col17 = vListaRezultate[pCampuriRezultate[vContor - 1]].ToString();
                                break;
                            case 18:
                                vListaSelect.Col18 = vListaRezultate[pCampuriRezultate[vContor - 1]].ToString();
                                break;
                            case 19:
                                vListaSelect.Col19 = vListaRezultate[pCampuriRezultate[vContor - 1]].ToString();
                                break;
                            case 20:
                                vListaSelect.Col20 = vListaRezultate[pCampuriRezultate[vContor - 1]].ToString();
                                break;
                            case 21:
                                vListaSelect.Col21 = vListaRezultate[pCampuriRezultate[vContor - 1]].ToString();
                                break;
                            case 22:
                                vListaSelect.Col22 = vListaRezultate[pCampuriRezultate[vContor - 1]].ToString();
                                break;
                        }
                        vContor++;
                    }
                    vRezultate.Add(vListaSelect);
                }
            }
            catch { }
            con.Close();
        }
        return vRezultate;
    }
    public static int fVerificaExistenta(string pTabela, string pCamp, string pValoareCamp, short an)
    {
        // verificam daca Count > 0
        DBConnections connection = new DBConnections(an);

        string strConn = connection.Create();
        SqlConnection con = new SqlConnection(strConn);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = "SELECT Count(*) as exista FROM " + pTabela + " WHERE " + pCamp + " = '" + pValoareCamp + "'";
        int vExista = 0;
        using (con)
        {
            con.Open();

            try
            {
                SqlDataReader vCount = cmd.ExecuteReader();
                if (vCount.Read())
                {
                    if (Convert.ToInt32(vCount["exista"]) > 0) vExista = 1;
                }
            }
            catch { vExista = 0; }
            con.Close();
        }
        return vExista;
    }

    public static int fEvitaDubluClick(List<string> pCampuri, List<string> pValori, string pTabela, short an)
    {
        // formam interogarea
        string vInterogare = "SELECT Count(*) as exista FROM " + pTabela + " WHERE ";
        ;
        int vPozitie = 0;
        foreach (string vCamp in pCampuri)
        {
            if (vPozitie != 0)
                vInterogare += " AND " + vCamp + " =" + pValori[vPozitie];
            else
                vInterogare += vCamp + " =" + pValori[vPozitie];
            vPozitie++;
        }
        DBConnections connection = new DBConnections(an);

        string strConn = connection.Create();
        SqlConnection con = new SqlConnection(strConn);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = vInterogare;
        int vExista = 0;
        using (con)
        {
            con.Open();

            try
            {
                SqlDataReader vCount = cmd.ExecuteReader();
                if (vCount.Read())
                {
                    if (Convert.ToInt32(vCount["exista"]) > 0) vExista = 1;
                }
            }
            catch { vExista = 0; }
            con.Close();
        }
        return vExista;
    }
    public static int fVerificaExistentaExtins(string pTabela, string pCamp, string pValoareCamp, string pExtins, short an)
    {
        // verificam daca Count > 0
        // pExtins e valoare suplimentara pentru Where
        DBConnections connection = new DBConnections(an);

        string strConn = connection.Create();
        SqlConnection con = new SqlConnection(strConn);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = "SELECT Count(*) as exista FROM " + pTabela + " WHERE " + pCamp + " = '" + pValoareCamp + "'" + pExtins;
        con.Open();
        int vExista = 0;
        try
        {
            SqlDataReader vCount = cmd.ExecuteReader();
            if (vCount.Read())
            {
                if (Convert.ToInt32(vCount["exista"]) > 0) vExista = 1;
            }
        }
        catch { vExista = 0; }
        con.Close();
        return vExista;
    }

    public static int fVerificaExistentaArray(string pTabela, string[] pCamp, string[] pValoareCamp, string pLogic, short an)
    {
        // verificam daca Count > 0
        // construim selectul din pCamp si pValoare camp
        string vSelect = "";
        if (pCamp.Length == pValoareCamp.Length)
        {
            int vContor = 0;
            vSelect = "SELECT Count(*) as exista FROM " + pTabela + " WHERE ";
            foreach (string vElementDinPCamp in pCamp)
            {
                if (vContor == 0) { vSelect += vElementDinPCamp + " = '" + pValoareCamp[vContor] + "' "; }
                else { vSelect += pLogic + " " + vElementDinPCamp + " = '" + pValoareCamp[vContor] + "' "; }
                vContor++;
            }
        }
        else { }
        DBConnections connection = new DBConnections(an);

        string strConn = connection.Create();
        SqlConnection con = new SqlConnection(strConn);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = vSelect;
        con.Open();
        int vExista = 0;
        try
        {
            SqlDataReader vCount = cmd.ExecuteReader();
            if (vCount.Read())
            {
                if (Convert.ToInt32(vCount["exista"]) > 0) vExista = 1;
            }
        }
        catch { vExista = 0; }
        con.Close();
        return vExista;
    }
    public static List<string> NumeleColoanelor(string pTabela, short an)
    {
        DBConnections connection = new DBConnections(an);

        string strConn = connection.Create();
        SqlConnection con = new SqlConnection(strConn);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = "SELECT     COLUMN_NAME FROM         INFORMATION_SCHEMA.COLUMNS WHERE     (TABLE_NAME = '" + pTabela + "')";
        con.Open();
        List<string> vListaNumeColoane = new List<string> { };
        vListaNumeColoane.Clear();
        try
        {
            SqlDataReader vRezultat = cmd.ExecuteReader();
            while (vRezultat.Read())
            {
                vListaNumeColoane.Add(vRezultat["COLUMN_NAME"].ToString());
            }
        }
        catch { }
        con.Close();
        return vListaNumeColoane;
    }
    public static int NumarulColoanelor(string pTabela, short an)
    {
        // extragem numarul total de coloane din tabela
        DBConnections connection = new DBConnections(an);

        string strConn = connection.Create();
        SqlConnection con = new SqlConnection(strConn);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = "SELECT COUNT(*) AS numarColoane FROM  INFORMATION_SCHEMA.COLUMNS WHERE (TABLE_NAME = '" + pTabela + "')";
        con.Open();
        int vNumarColoane = 0;
        try
        {
            SqlDataReader vRezultat = cmd.ExecuteReader();
            if (vRezultat.Read())
            {
                vNumarColoane = Convert.ToInt32(vRezultat["numarColoane"]);
            }
        }
        catch { }
        con.Close();
        return vNumarColoane;
    }

    public static bool AreDateInCapitolePeAnulRespectiv(string gospodarieId, string an, string codCapitol)
    {
        string select = @"SELECT COALESCE((SUM(col1) + SUM(col2)),0) as suma from capitole
                            where gospodarieId = " + gospodarieId + " AND an = '" + an + "' AND codCapitol LIKE '" + codCapitol + "'";
        bool x = (Convert.ToDecimal(fRezultaUnString(select, "suma", 2016)) > 0) ? true : false;

        return x;
    }
    public static int StergeDateInCapitolePeAnulRespectiv(string gospodarieId, string an, string codCapitol)
    {
        string delete = @"DELETE FROM capitole
                            where gospodarieId = " + gospodarieId + " AND an = '" + an + "' AND codCapitol LIKE '" + codCapitol + "'";
        int cateRanduriAfectate = 0;
        fManipuleazaBDCateRanduri(delete, out cateRanduriAfectate, 2016);
        return cateRanduriAfectate;
    }
    public static int AdaugaDateInCapitolePeAnulRespectiv(string gospodarieId, string an, string codCapitol)
    {
      //  string delete = @"INSERT INTO[capitole] ([unitateId], [gospodarieId], [an], [codCapitol], [codRand], [col1], [col2], [col3], [col4], [col5], [col6], [col7], [col8]) VALUES('" + unitate + "','" + gospodarieId + "','" + an + "','" + randSablon.Capitol + "', '" + randSablon.CodRand + @"', '0', '0', '0', '0', '0', '0', '0', '0'); ;
        int cateRanduriAfectate = 0;
        //  fManipuleazaBDCateRanduri(delete, out cateRanduriAfectate, 2016);
        return cateRanduriAfectate;
    }
}
