﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Gospodarie
{
    public class Gospodarii
    {
    private Int64 id = 0;

        public Int64 Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        private int idUnitate = 0;

        public int IdUnitate
        {
            get
            {
                return idUnitate;
            }
            set
            {
                idUnitate = value;
            }
        }

        private string volum = string.Empty;

        public string Volum
        {
            get
            {
                return volum;
            }
            set
            {
                volum = value;
            }
        }

        private string nrPozitie = string.Empty;

        public string NrPozitie
        {
            get
            {
                return nrPozitie;
            }
            set
            {
                nrPozitie = value;
            }
        }

        private string siruta = string.Empty;

        public string Siruta
        {
            get
            {
                return siruta;
            }
            set
            {
                siruta = value;
            }
        }

        private string tip = string.Empty;

        public string Tip
        {
            get
            {
                return tip;
            }
            set
            {
                tip = value;
            }
        }

        private string codExploatatie = string.Empty;

        public string CodExploatatie
        {
            get
            {
                return codExploatatie;
            }
            set
            {
                codExploatatie = value;
            }
        }

        private string codUnic = string.Empty;

        public string CodUnic
        {
            get
            {
                return codUnic;
            }
            set
            {
                codUnic = value;
            }
        }

        private bool isPersoanaJuridica = false;

        public bool IsPersoanaJuridica
        {
            get
            {
                return this.isPersoanaJuridica;
            }
            set
            {
                this.isPersoanaJuridica = value;
            }
        }

        private string unitate = string.Empty;

        public string Unitate
        {
            get
            {
                return unitate;
            }
            set
            {
                unitate = value;
            }
        }

        private string subunitate = string.Empty;

        public string Subunitate
        {
            get
            {
                return subunitate;
            }
            set
            {
                subunitate = value;
            }
        }

        private string codFiscal = string.Empty;

        public string CodFiscal
        {
            get
            {
                return codFiscal;
            }
            set
            {
                codFiscal = value;
            }
        }

        private string reprezentant = string.Empty;

        public string Reprezentant
        {
            get
            {
                return reprezentant;
            }
            set
            {
                reprezentant = value;
            }
        }

        private bool isStrainas = false;

        public bool IsStrainas
        {
            get
            {
                return this.isStrainas;
            }
            set
            {
                this.isStrainas = value;
            }
        }

        private DateTime dataModificare = DateTime.MinValue;

        public DateTime DataModificare
        {
            get
            {
                return dataModificare;
            }
            set
            {
                dataModificare = value;
            }
        }

        private int oraModificare = 0;

        public int OraModificare
        {
            get
            {
                return oraModificare;
            }
            set
            {
                oraModificare = value;
            }
        }

        private int minutModificare = 0;

        public int MinutModificare
        {
            get
            {
                return minutModificare;
            }
            set
            {
                minutModificare = value;
            }
        }

        private int volumIntreg = -1;

        public int VolumIntreg
        {
            get
            {
                return volumIntreg;
            }
            set
            {
                volumIntreg = value;
            }
        }

        private int nrPozitieIntreg = -1;

        public int NrPozitieIntreg
        {
            get
            {
                return nrPozitieIntreg;
            }
            set
            {
                nrPozitieIntreg = value;
            }
        }

        private string an = string.Empty;

        public string An
        {
            get
            {
                return an;
            }
            set
            {
                an = value;
            }
        }

        private int idInitial = 0;

        public int IdInitial
        {
            get
            {
                return idInitial;
            }
            set
            {
                idInitial = value;
            }
        }

        private string cui = string.Empty;

        public string Cui
        {
            get
            {
                return cui;
            }
            set
            {
                cui = value;
            }
        }

        private string observatii = string.Empty;

        public string Observatii
        {
            get
            {
                return observatii;
            }
            set
            {
                observatii = value;
            }
        }

        private string numeMembru = string.Empty;

        public string NumeMembru
        {
            get
            {
                return numeMembru;
            }
            set
            {
                numeMembru = value;
            }
        }

        private string cnpMembru = string.Empty;

        public string CnpMembru
        {
            get
            {
                return cnpMembru;
            }
            set
            {
                cnpMembru = value;
            }
        }

        private int nrRolNominal = 0;

        public int NrRolNominal
        {
            get
            {
                return nrRolNominal;
            }
            set
            {
                nrRolNominal = value;
            }
        }

        private string volumVechi = string.Empty;

        public string VolumVechi
        {
            get
            {
                return volumVechi;
            }
            set
            {
                volumVechi = value;
            }
        }

        private string pozitieVeche = string.Empty;

        public string PozitieVeche
        {
            get
            {
                return pozitieVeche;
            }
            set
            {
                pozitieVeche = value;
            }
        }

        private Int64 idImport;

        public Int64 IdImport
        {
            get
            {
                return idImport;
            }
            set
            {
                idImport = value;
            }
        }

        public Gospodarii()
        {
        }

        public List<Gospodarii> GetGospodariiByUnitate()
        {//an = 2015
            if ( idUnitate == 0 || an == string.Empty )
            {
                return new List<Gospodarii>();
            }

            return GospodariiServices.GetGospodariiByUnitate(idUnitate, Convert.ToInt16(an));
        }

        //public int GetIdsByIdForIdInitial(long id)
        //{
        //    return GospodariiServices.GetIdsByIdForIdInitial(id);
        //}

        //internal bool InsertGospodarii(List<Gospodarii> gospodarii)
        //{
        //    return GospodariiServices.InsertGospodarii(gospodarii);
        //}

        //internal List<Tuple<Int64, Int64>> GetIdsByUnitate(int idUnitate)
        //{
        //    return GospodariiServices.GetIdsByUnitate(idUnitate);
        //}

        public long GetIdsByIdAndAnForIdFinal(long idInitial, int an)
        {
            return GospodariiServices.GetIdsByIdAndAnForIdFinal(idInitial, an);
        }
    
        public int GetIdInitialById(int idGospodarie, short an)
        {
           return GospodariiServices.GetIdInitialById(idGospodarie, an);
        }
    }
}
