﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Clasa care tine o lista cu proprietati pentru un numar de coloane
/// Creata la:                  09.02.2011
/// Autor:                      Laza Tudor Mihai
/// Ultima                      actualizare: 09.02.2011
/// Autor:                      Laza Tudor Mihai
/// </summary> 
[Serializable]
public class Capitole
{
    private long vId = 0;
    private string vCol1 = "";
    private string vCol2 = "";
    private string vCol3 = "";
    private string vCol4 = "";
    private string vCol5 = "";
    private string vCol6 = "";
    private string vCol7 = "";
    private string vCol8 = "";
    private string vCol9 = "";
    private string vCol10 = "";
    private string vFormula = "";
  
    public long Id
    {
        get
        { return vId; }
        set
        { vId = value; }
    }

    public string Col1
    {
        get
        { return vCol1; }
        set
        { vCol1 = value; }
    }
    public string Col2
    {
        get
        { return vCol2; }
        set
        { vCol2 = value; }
    }
    public string Col3
    {
        get
        { return vCol3; }
        set
        { vCol3 = value; }
    }
    public string Col4
    {
        get
        { return vCol4; }
        set
        { vCol4 = value; }
    }
    public string Col5
    {
        get
        { return vCol5; }
        set
        { vCol5 = value; }
    }
    public string Col6
    {
        get
        { return vCol6; }
        set
        { vCol6 = value; }
    }
    public string Col7
    {
        get
        { return vCol7; }
        set
        { vCol7 = value; }
    }
    public string Col8
    {
        get
        { return vCol8; }
        set
        { vCol8 = value; }
    }
    public string Col9
    {
        get
        { return vCol9; }
        set
        { vCol9 = value; }
    }
    public string Col10
    {
        get
        { return vCol10; }
        set
        { vCol10 = value; }
    }
    public string Formula
    {
        get
        { return vFormula; }
        set
        { vFormula = value; }
    }

    Utils.CapitolCulturi cod;
    public Utils.CapitolCulturi Cod
    {
        get
        {
            return this.cod;
        }
        set
        {
            this.cod = value;
        }
    }

}
