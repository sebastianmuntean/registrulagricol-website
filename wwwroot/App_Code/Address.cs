﻿using System;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Adresa
/// </summary>
public class Address
{
    
    public enum AddressType
    {
        NoType = 0,
        Punct_de_lucru = 1,
        Sediu_social = 2
    }

    private UInt64 id = 0;
    public UInt64 Id
    {
        get
        {
            return id;
        }
        set
        {
            id = value;
        }
    }

    private string judet = string.Empty;
    public string Judet
    {
        get
        {
            return this.judet;
        }
        set
        {
            this.judet = value;
        }
    }

    private string bloc = string.Empty;
    public string Bloc
    {
        get
        {
            return this.bloc;
        }
        set
        {
            this.bloc = value;
        }
    }

    private string apartament = string.Empty;
    public string Apartament
    {
        get
        {
            return this.apartament;
        }
        set
        {
            this.apartament = value;
        }
    }

    private string etaj = string.Empty;
    public string Etaj
    {
        get
        {
            return this.etaj;
        }
        set
        {
            this.etaj = value;
        }
    }

    private string codPostal = string.Empty;
    public string CodPostal
    {
        get
        {
            return this.codPostal;
        }
        set
        {
            this.codPostal = value;
        }
    }

    private string localitate = string.Empty;
    public string Localitate
    {
        get
        {
            return this.localitate;
        }
        set
        {
            this.localitate = value;
        }
    }

    private string strada = string.Empty;
    public string Strada
    {
        get
        {
            return this.strada;
        }
        set
        {
            this.strada = value;
        }
    }

    private string numar = string.Empty;
    public string Numar
    {
        get
        {
            return this.numar;
        }
        set
        {
            this.numar = value;
        }
    }

    string denumire = string.Empty;
    public string Denumire
    {
        get
        {
            return denumire;
        }
        set
        {
            denumire = value;
        }
    }

    string observatii = string.Empty;
    public string Observatii
    {
        get
        {
            return observatii;
        }
        set
        {
            observatii = value;
        }
    }

    private string scara = string.Empty;
    public string Scara
    {
        get
        {
            return scara;
        }
        set
        {
            scara = value;
        }
    }

    private int gospodarieId = 0;
    public int GospodarieId
    {
        get
        {
            return this.gospodarieId;
        }
        set
        {
            this.gospodarieId = value;
        }
    }

    private int gospodarieIdInitial;
    public int GospodarieIdInitial
    {
        get
        {
            return this.gospodarieIdInitial;
        }
        set
        {
            this.gospodarieIdInitial = value;
        }
    }

    private AddressType addressType = AddressType.NoType;
    public AddressType AddresType 
    {
        get
        {
            return addressType;
        }
        set
        {
            addressType = value;
        }
    }

    string an = "";
    public string An
    {
        get
        {
            return this.an;
        }
        set
        {
            this.an = value;
        }
    }

    public Address()
	{
		
	}

    public void Clear()
    {
        judet = string.Empty;
        bloc = string.Empty;
        apartament = string.Empty;
        etaj = string.Empty;
        codPostal = string.Empty;
        localitate = string.Empty;
        strada = string.Empty;
        numar = string.Empty;
        id = 0;
        addressType = AddressType.NoType;
        scara = string.Empty;
        an = string.Empty;
        gospodarieId = 0;
        gospodarieIdInitial = 0;
        observatii = string.Empty;
        denumire = string.Empty;
    }

    public List<Address> GetAddresByGospodarie()
    {
        return AddressServices.GetAddressByGospodarieId(gospodarieId);
    }

    public Address GetAddressById()
    {
        return AddressServices.GetAddressById(this);
    }

    public bool Add()
    {
        return AddressServices.Save(this);
    }

    public bool Update()
    {
        return AddressServices.Update(this);
    }
    
    public bool Delete()
    {
        return AddressServices.DeleteAddress(id);
    }

    public override string ToString()
    {
        string address = string.Empty;

        if (id<=0)
        {
            return address;
        }

        address = "Judet: " + judet + "; Localitate: " + localitate + "; Strada: " + strada + "; Numar: " + numar + "; Cod postal: " + codPostal;
        
        if (bloc != string.Empty)
        {
            address += "; Bloc: " + bloc;
        }
        if (scara != string.Empty)
        {
            address += "; Scara: " + scara;
        }
        if (scara != string.Empty)
        {
            address += "; Etaj: " + etaj;
        }
        if (apartament !=string.Empty)
        {
            address += "; Apartament: " + apartament;
        }

        return address;
    }

    public bool IsEmpty()
    {
        return (judet == string.Empty && localitate == string.Empty && strada == string.Empty && numar == string.Empty) || gospodarieId == 0;
    }
}