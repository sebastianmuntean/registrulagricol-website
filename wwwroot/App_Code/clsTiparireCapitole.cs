﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

/// <summary>
/// Summary description for clsTiparireCapitole
/// </summary>
public class clsTiparireCapitole
{
    public clsTiparireCapitole()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static void UmpleRapCapitole(string pUtilizatorId, string pSesAn, string pGospodarieId, string pCapitol)
    {
        // stergem rapoartele vechi ale utilizatorului
        ManipuleazaBD.fManipuleazaBD("DELETE FROM rapCapitole WHERE utilizatorId = '" + pUtilizatorId + "'", Convert.ToInt16(pSesAn));

        // umplem tabela de rapoarte
        // scoatem lista anilor din ciclul curent
        string vCiclu = ManipuleazaBD.fRezultaUnString("SELECT * FROM cicluri WHERE cicluAni LIKE '%" + pSesAn + "%'", "cicluAni", Convert.ToInt16(pSesAn));
        char[] vDelimitator = { '#' };
        string[] vAniDinCiclu = vCiclu.Split(vDelimitator, StringSplitOptions.RemoveEmptyEntries);
        List<string> vListaCampuri = new List<string> { "codCapitol", "capitoleCodRand", "denumire1", "denumire2", "denumire3", "denumire4", "denumire5" };
        string vGospodarieIdInitial = ManipuleazaBD.fRezultaUnString("SELECT * FROM gospodarii WHERE gospodarieId = '" + pGospodarieId + "'", "gospodarieIdInitial", Convert.ToInt16(pSesAn));
        // luam valorile generale, indiferent de ani
        List<List<string>> vValoriGenerale = new List<List<string>> { };
        vValoriGenerale = ManipuleazaBD.fRezultaListaStringuri(" SELECT DISTINCT  capitole.codCapitol, capitole.codRand AS capitoleCodRand,  sabloaneCapitole.denumire1, sabloaneCapitole.denumire2, sabloaneCapitole.denumire3, sabloaneCapitole.denumire4, sabloaneCapitole.denumire5 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId  WHERE     (gospodarii.gospodarieIdInitial = '" + vGospodarieIdInitial + "') AND (capitole.an = '" + pSesAn + "')  AND (sabloaneCapitole.an = '" + pSesAn + "') AND (capitole.codCapitol = '" + pCapitol + "') ORDER BY capitoleCodRand", vListaCampuri, Convert.ToInt16(pSesAn));

        vListaCampuri = new List<string> { "capitoleAn", "col1", "col2", "col3", "col4", "col5", "col6", "col7", "col8","col9","col10" };
        List<List<List<string>>> vListaValori = new List<List<List<string>>> { };
        int vCodRand = 0;
        string vComanda = "";
        foreach (string vAn in vAniDinCiclu)
        {
            List<List<string>> vValori = new List<List<string>> { };
            vValori = ManipuleazaBD.fRezultaListaStringuri(" SELECT  capitole.an AS capitoleAn, capitole.codCapitol, capitole.codRand AS capitoleCodRand, CONVERT(decimal(18, 2), capitole.col1) AS col1, CONVERT(decimal(18, 2), capitole.col2) AS col2, CONVERT(decimal(18, 2), capitole.col3) AS col3, CONVERT(decimal(18, 2), capitole.col4) AS col4, CONVERT(decimal(18, 2), capitole.col5) AS col5, CONVERT(decimal(18, 2), capitole.col6) AS col6, CONVERT(decimal(18, 2), capitole.col7) AS col7, CONVERT(decimal(18, 2), capitole.col8) AS col8,capitole.col9,capitole.col10 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId  WHERE     (gospodarii.gospodarieIdInitial = '" + vGospodarieIdInitial + "') AND (capitole.an = '" + vAn + "')  AND (sabloaneCapitole.an = '" + vAn + @"') AND (capitole.codCapitol = '" + pCapitol + "') ORDER BY capitoleCodRand", vListaCampuri, Convert.ToInt16(pSesAn));
            if (vValori.Count != 0)
            { vListaValori.Add(vValori); }
            else
            {
                vValori = new List<List<string>> { };
                for (int i = 0; i < vValoriGenerale.Count; i++)
                {
                    List<string> vListaGoala = new List<string> { vAn, "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0" };
                    vValori.Add(vListaGoala);
                }
                vListaValori.Add(vValori);
            }

        }
        // construim comanda pentru fiecare rand
        foreach (List<string> vRandValori in vValoriGenerale)
        {
            string v1 = vListaValori[0][vCodRand][1].ToString() == string.Empty ? "0" : vListaValori[0][vCodRand][1].Replace(',', '.');
            string v2 = vListaValori[0][vCodRand][2].ToString() == string.Empty ? "0" : vListaValori[0][vCodRand][2].Replace(',', '.');
            string v3 = vListaValori[0][vCodRand][3].ToString() == string.Empty ? "0" : vListaValori[0][vCodRand][3].Replace(',', '.');
            string v4 = vListaValori[0][vCodRand][4].ToString() == string.Empty ? "0" : vListaValori[0][vCodRand][4].Replace(',', '.');
            string v5 = vListaValori[0][vCodRand][5].ToString() == string.Empty ? "0" : vListaValori[0][vCodRand][5].Replace(',', '.');
            string v6 = vListaValori[0][vCodRand][6].ToString() == string.Empty ? "0" : vListaValori[0][vCodRand][6].Replace(',', '.');
            string v7 = vListaValori[0][vCodRand][7].ToString() == string.Empty ? "0" : vListaValori[0][vCodRand][7].Replace(',', '.');
            string v8 = vListaValori[0][vCodRand][8].ToString() == string.Empty ? "0" : vListaValori[0][vCodRand][8].Replace(',', '.');

            string v11 = vListaValori[1][vCodRand][1].ToString() == string.Empty ? "0" : vListaValori[1][vCodRand][1].Replace(',', '.');
            string v12 = vListaValori[1][vCodRand][2].ToString() == string.Empty ? "0" : vListaValori[1][vCodRand][2].Replace(',', '.');
            string v13 = vListaValori[1][vCodRand][3].ToString() == string.Empty ? "0" : vListaValori[1][vCodRand][3].Replace(',', '.');
            string v14 = vListaValori[1][vCodRand][4].ToString() == string.Empty ? "0" : vListaValori[1][vCodRand][4].Replace(',', '.');
            string v15 = vListaValori[1][vCodRand][5].ToString() == string.Empty ? "0" : vListaValori[1][vCodRand][5].Replace(',', '.');
            string v16 = vListaValori[1][vCodRand][6].ToString() == string.Empty ? "0" : vListaValori[1][vCodRand][6].Replace(',', '.');
            string v17 = vListaValori[1][vCodRand][7].ToString() == string.Empty ? "0" : vListaValori[1][vCodRand][7].Replace(',', '.');
            string v18 = vListaValori[1][vCodRand][8].ToString() == string.Empty ? "0" : vListaValori[1][vCodRand][8].Replace(',', '.');

            string v21 = vListaValori[2][vCodRand][1].ToString() == string.Empty ? "0" : vListaValori[2][vCodRand][1].Replace(',', '.');
            string v22 = vListaValori[2][vCodRand][2].ToString() == string.Empty ? "0" : vListaValori[2][vCodRand][2].Replace(',', '.');
            string v23 = vListaValori[2][vCodRand][3].ToString() == string.Empty ? "0" : vListaValori[2][vCodRand][3].Replace(',', '.');
            string v24 = vListaValori[2][vCodRand][4].ToString() == string.Empty ? "0" : vListaValori[2][vCodRand][4].Replace(',', '.');
            string v25 = vListaValori[2][vCodRand][5].ToString() == string.Empty ? "0" : vListaValori[2][vCodRand][5].Replace(',', '.');
            string v26 = vListaValori[2][vCodRand][6].ToString() == string.Empty ? "0" : vListaValori[2][vCodRand][6].Replace(',', '.');
            string v27 = vListaValori[2][vCodRand][7].ToString() == string.Empty ? "0" : vListaValori[2][vCodRand][7].Replace(',', '.');
            string v28 = vListaValori[2][vCodRand][8].ToString() == string.Empty ? "0" : vListaValori[2][vCodRand][8].Replace(',', '.');

            string v31 = vListaValori[3][vCodRand][1].ToString() == string.Empty ? "0" : vListaValori[3][vCodRand][1].Replace(',', '.');
            string v32 = vListaValori[3][vCodRand][2].ToString() == string.Empty ? "0" : vListaValori[3][vCodRand][2].Replace(',', '.');
            string v33 = vListaValori[3][vCodRand][3].ToString() == string.Empty ? "0" : vListaValori[3][vCodRand][3].Replace(',', '.');
            string v34 = vListaValori[3][vCodRand][4].ToString() == string.Empty ? "0" : vListaValori[3][vCodRand][4].Replace(',', '.');
            string v35 = vListaValori[3][vCodRand][5].ToString() == string.Empty ? "0" : vListaValori[3][vCodRand][5].Replace(',', '.');
            string v36 = vListaValori[3][vCodRand][6].ToString() == string.Empty ? "0" : vListaValori[3][vCodRand][6].Replace(',', '.');
            string v37 = vListaValori[3][vCodRand][7].ToString() == string.Empty ? "0" : vListaValori[3][vCodRand][7].Replace(',', '.');
            string v38 = vListaValori[3][vCodRand][8].ToString() == string.Empty ? "0" : vListaValori[3][vCodRand][8].Replace(',', '.');

            string v41 = vListaValori[4][vCodRand][1].ToString() == string.Empty ? "0" : vListaValori[4][vCodRand][1].Replace(',', '.');
            string v42 = vListaValori[4][vCodRand][2].ToString() == string.Empty ? "0" : vListaValori[4][vCodRand][2].Replace(',', '.');
            string v43 = vListaValori[4][vCodRand][3].ToString() == string.Empty ? "0" : vListaValori[4][vCodRand][3].Replace(',', '.');
            string v44 = vListaValori[4][vCodRand][4].ToString() == string.Empty ? "0" : vListaValori[4][vCodRand][4].Replace(',', '.');
            string v45 = vListaValori[4][vCodRand][5].ToString() == string.Empty ? "0" : vListaValori[4][vCodRand][5].Replace(',', '.');
            string v46 = vListaValori[4][vCodRand][6].ToString() == string.Empty ? "0" : vListaValori[4][vCodRand][6].Replace(',', '.');
            string v47 = vListaValori[4][vCodRand][7].ToString() == string.Empty ? "0" : vListaValori[4][vCodRand][7].Replace(',', '.');
            string v48 = vListaValori[4][vCodRand][8].ToString() == string.Empty ? "0" : vListaValori[4][vCodRand][8].Replace(',', '.');

            vComanda += @"INSERT INTO [rapCapitole] ([utilizatorId], [capitol], [codRand], [denumire1], [denumire2], [denumire3], [denumire4], [denumire5], [col1_1], [col1_2], [col1_3], [col1_4], [col1_5], [col1_6], [col1_7], [col1_8],[col1_9],[col1_10], [an1],[col2_1], [col2_2], [col2_3], [col2_4], [col2_5], [col2_6], [col2_7], [col2_8], [col2_9],[col2_10],[an2],[col3_1], [col3_2], [col3_3], [col3_4], [col3_5], [col3_6], [col3_7], [col3_8],[col3_9],[col3_10], [an3],[col4_1], [col4_2], [col4_3], [col4_4], [col4_5], [col4_6], [col4_7], [col4_8],[col4_9],[col4_10], [an4],[col5_1], [col5_2], [col5_3], [col5_4], [col5_5], [col5_6], [col5_7], [col5_8],[col5_9],[col5_10],[an5]) VALUES ('" +
                Convert.ToInt32(pUtilizatorId) + "','" +
                vValoriGenerale[vCodRand][0] + "','" +
                vValoriGenerale[vCodRand][1] + "','" +
                vValoriGenerale[vCodRand][2] + "','" +
                vValoriGenerale[vCodRand][3] + "','" +
                vValoriGenerale[vCodRand][4] + "','" +
                vValoriGenerale[vCodRand][5] + "','" +
                vValoriGenerale[vCodRand][6] + "','" +
                v1 + "', '" +
                v2 + "', '" +
                v3 + "', '" +
                v4 + "', '" +
                v5+ "', '" +
                v6 + "', '" +
                v7 + "', '" +
                v8 + "', '" +
                vListaValori[0][vCodRand][9] + "', '" +
                vListaValori[0][vCodRand][10] + "', '" +

                vListaValori[0][vCodRand][0].Replace(',', '.') + "', '" +
                v11 + "', '" +
                v12 + "', '" +
                v13 + "', '" +
                v14 + "', '" +
                v15 + "', '" +
                v16 + "', '" +
                v17 + "', '" +
                v18 + "', '" +
                vListaValori[1][vCodRand][9] + "', '" +
                vListaValori[1][vCodRand][10] + "', '" +

                vListaValori[1][vCodRand][0].Replace(',', '.') + "', '" +
                v21 + "', '" +
                v22 + "', '" +
                v23 + "', '" +
                v24 + "', '" +
                v25 + "', '" +
                v26 + "', '" +
                v27 + "', '" +
                v28 + "', '" +
                vListaValori[2][vCodRand][9] + "', '" +
                vListaValori[2][vCodRand][10] + "', '" +


                vListaValori[2][vCodRand][0].Replace(',', '.') + "', '" +
                v31 + "', '" +
                v32 + "', '" +
                v33 + "', '" +
                v34 + "', '" +
                v35 + "', '" +
                v36 + "', '" +
                v37 + "', '" +
                v38 + "', '" +
                vListaValori[3][vCodRand][9] + "', '" +
                vListaValori[3][vCodRand][10] + "', '" +

                vListaValori[3][vCodRand][0].Replace(',', '.') + "', '" +
                v41 + "', '" +
                v42 + "', '" +
                v43 + "', '" +
                v44 + "', '" +
                v45 + "', '" +
                v46 + "', '" +
                v47 + "', '" +
                v48 + "', '" +
                vListaValori[4][vCodRand][9] + "', '" +
                vListaValori[4][vCodRand][10] + "', '" +

                vListaValori[4][vCodRand][0].Replace(',', '.') + "'); ";
            vCodRand++;
        }

        ManipuleazaBD.fManipuleazaBD(vComanda, Convert.ToInt16(pSesAn));
    }

    public static DataTable UmpleRapCapitol11_CuSablon(string pUtilizatorId, string pSesAn, string pGospodarieId, string pCapitol)
    {
        ManipuleazaBD.fManipuleazaBD("DELETE FROM rapCapitole WHERE utilizatorId = '" + pUtilizatorId + "'", Convert.ToInt16(pSesAn));

        string ani = ManipuleazaBD.fRezultaUnString("SELECT * FROM cicluri WHERE cicluAni LIKE '%" + pSesAn + "%'", "cicluAni", Convert.ToInt16(pSesAn));
      
        char[] vDelimitator =
        {
            '#'
        };
      
        string[] vectorDeAni = ani.Split(vDelimitator, StringSplitOptions.RemoveEmptyEntries);
        string idInitalGospodarie = ManipuleazaBD.fRezultaUnString("SELECT * FROM gospodarii WHERE gospodarieId = '" + pGospodarieId + "'", "gospodarieIdInitial", Convert.ToInt16(pSesAn));

        DataTable capitolDataTable = new DataTable();
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(pSesAn));

        SqlDataAdapter sablonCapitolDataAdapter = new SqlDataAdapter(@"SELECT  '' as adresa,
                                                                               '' as zona,
		                                                                       sabloaneCapitole.denumire3,
		                                                                       sabloaneCapitole.denumire4 as 'U.M.',
                                                                               '' as valoare,
                                                                               codRand
                                                                        FROM   sabloaneCapitole 
                                                                        WHERE  capitol = '11' and an in (2015,2016)",
            connection);

        sablonCapitolDataAdapter.Fill(capitolDataTable);

        SqlCommand command = new SqlCommand();
        command.Connection = connection;

        Dictionary<short, Building> chapterValuesDictionary = new Dictionary<short, Building>();
        short j = 0; 

        foreach ( string an in vectorDeAni )
        {
           // chapterValuesDictionary = new Dictionary<short, Building>();
            command.CommandText = @"SELECT 
		                                    capitol11.an AS an, 
		                                    adresa,
		                                    zona,
		                                    suprafata, 
		                                    capitol11.tip +' '+ destinatie as tipCladire, 
		                                    anTerminare
                                    FROM	capitol11 
	                                INNER JOIN gospodarii ON capitol11.gospodarieId = gospodarii.gospodarieId 
                                    WHERE   (gospodarii.gospodarieIdInitial = " + idInitalGospodarie + ") AND (capitol11.an = " + an + ")";

            SqlDataReader reader = command.ExecuteReader();
 
            while ( reader.Read() )
            {
                Building building = new Building();
                
                building.An = Convert.ToInt16(reader["an"]);
                building.Adresa = reader["adresa"].ToString();
                building.Zona = reader["zona"].ToString();
                building.Suprafata = Convert.ToDecimal(reader["suprafata"]);
                building.Tip = reader["tipCladire"].ToString();
                building.AnTerminare = Convert.ToInt16(reader["anTerminare"]);

                chapterValuesDictionary.Add(j++, building);
            }
            reader.Close();

           }
            short currentKey = 0;
            for ( short counter = 0; counter < 51; counter += 3 )
            {
                if ( chapterValuesDictionary.Keys.Count > currentKey )
                {
                    capitolDataTable.Rows[counter]["adresa"] = chapterValuesDictionary[currentKey].Adresa;
                    capitolDataTable.Rows[counter]["zona"] = chapterValuesDictionary[currentKey].Zona;
                    capitolDataTable.Rows[counter]["valoare"] = chapterValuesDictionary[currentKey].Suprafata;
               
                    capitolDataTable.Rows[counter + 1]["valoare"] = chapterValuesDictionary[currentKey].Tip;
                    capitolDataTable.Rows[counter + 2]["valoare"] = chapterValuesDictionary[currentKey].AnTerminare;
                }

                currentKey++;
            }
       
       
        //for ( int i = capitolDataTable.Rows.Count-1; i >= 21 ; i-- )
        //{
        //    if( capitolDataTable.Rows[i]["adresa"].ToString() == string.Empty && capitolDataTable.Rows[i]["zona"].ToString() == string.Empty &&  capitolDataTable.Rows[i]["valoare"].ToString() == string.Empty )
        //    {
        //        capitolDataTable.Rows.RemoveAt(i);
        //    }
        //}

            return capitolDataTable;
    }

    public static DataTable UmpleRapCapitol11(string utilizatorId, string an, string gospodarieId)
    {
        ManipuleazaBD.fManipuleazaBD("DELETE FROM rapCapitole WHERE utilizatorId = '" + utilizatorId + "'", Convert.ToInt16(an));

        string ani = ManipuleazaBD.fRezultaUnString("SELECT * FROM cicluri WHERE cicluAni LIKE '%" + an + "%'", "cicluAni", Convert.ToInt16(an));
        char[] vDelimitator =
        {
            '#'
        };
        string[] vectorDeAni = ani.Split(vDelimitator, StringSplitOptions.RemoveEmptyEntries);
        string idInitalGospodarie = ManipuleazaBD.fRezultaUnString("SELECT * FROM gospodarii WHERE gospodarieId = '" + gospodarieId + "'", "gospodarieIdInitial", Convert.ToInt16(an));

        DataTable capitolDataTable = new DataTable();
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(an));

        SqlCommand command = new SqlCommand();
        command.Connection = connection;

        SqlDataAdapter capitolDataAdapter = new SqlDataAdapter(@"SELECT 
		                                                                capitol11.an AS an, 
		                                                                adresa,
		                                                                zona,
		                                                                suprafata, 
		                                                                capitol11.tip as tipCladire,
		                                                                destinatie, 
		                                                                anTerminare
                                                                FROM	capitol11 
	                                                            INNER JOIN gospodarii ON capitol11.gospodarieId = gospodarii.gospodarieId 
                                                                WHERE   (gospodarii.gospodarieIdInitial = " + idInitalGospodarie + ") AND (capitol11.an = " + an + ")", connection);

        capitolDataAdapter.Fill(capitolDataTable);

        if(capitolDataTable.Rows.Count==0)
        {
            capitolDataTable.Rows.Add(0,"","",0,"","",0);
        }

        return capitolDataTable;
    }

    public static void StergeRapoarteVechi(string pUtilizatorId, short an)
    {
        // stergem rapoartele vechi ale utilizatorului
        ManipuleazaBD.fManipuleazaBD("DELETE FROM rapCapitole WHERE utilizatorId = '" + pUtilizatorId + "'", an);
    }
    public static void UmpleRapCapitoleFaraStergere(string pUtilizatorId, string pSesAn, string pGospodarieId, string pCapitol)
    {
        // umplem tabela de rapoarte
        // scoatem lista anilor din ciclul curent
        string vCiclu = ManipuleazaBD.fRezultaUnString("SELECT * FROM cicluri WHERE cicluAni LIKE '%" + pSesAn + "%'", "cicluAni", Convert.ToInt16(pSesAn));
        char[] vDelimitator = { '#' };
        string[] vAniDinCiclu = vCiclu.Split(vDelimitator, StringSplitOptions.RemoveEmptyEntries);
        List<string> vListaCampuri = new List<string> { "codCapitol", "capitoleCodRand", "denumire1", "denumire2", "denumire3", "denumire4", "denumire5" };
        string vGospodarieIdInitial = ManipuleazaBD.fRezultaUnString("SELECT * FROM gospodarii WHERE gospodarieId = '" + pGospodarieId + "'", "gospodarieIdInitial", Convert.ToInt16(pSesAn));
        // luam valorile generale, indiferent de ani
        List<List<string>> vValoriGenerale = new List<List<string>> { };
        vValoriGenerale = ManipuleazaBD.fRezultaListaStringuri(" SELECT  capitole.codCapitol, capitole.codRand AS capitoleCodRand,  sabloaneCapitole.denumire1, sabloaneCapitole.denumire2, sabloaneCapitole.denumire3, sabloaneCapitole.denumire4, sabloaneCapitole.denumire5 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId  WHERE     (gospodarii.gospodarieIdInitial = '" + vGospodarieIdInitial + "') AND (capitole.an = '" + pSesAn + "')  AND (sabloaneCapitole.an = '" + pSesAn + "') AND (capitole.codCapitol = '" + pCapitol + "') ORDER BY capitoleCodRand", vListaCampuri, Convert.ToInt16(pSesAn));

        vListaCampuri = new List<string> { "capitoleAn", "col1", "col2", "col3", "col4", "col5", "col6", "col7", "col8" };
        List<List<List<string>>> vListaValori = new List<List<List<string>>> { };
        int vCodRand = 0;
        string vComanda = "";
        foreach (string vAn in vAniDinCiclu)
        {
            List<List<string>> vValori = new List<List<string>> { };
            vValori = ManipuleazaBD.fRezultaListaStringuri(" SELECT  capitole.an AS capitoleAn, capitole.codCapitol, capitole.codRand AS capitoleCodRand, CONVERT(decimal(18, 2), capitole.col1) AS col1, CONVERT(decimal(18, 2), capitole.col2) AS col2, CONVERT(decimal(18, 2), capitole.col3) AS col3, CONVERT(decimal(18, 2), capitole.col4) AS col4, CONVERT(decimal(18, 2), capitole.col5) AS col5, CONVERT(decimal(18, 2), capitole.col6) AS col6, CONVERT(decimal(18, 2), capitole.col7) AS col7, CONVERT(decimal(18, 2), capitole.col8) AS col8 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand INNER JOIN gospodarii ON capitole.gospodarieId = gospodarii.gospodarieId  WHERE     (gospodarii.gospodarieIdInitial = '" + vGospodarieIdInitial + "') AND (capitole.an = '" + vAn + "')  AND (sabloaneCapitole.an = '" + vAn + @"') AND (capitole.codCapitol = '" + pCapitol + "') ORDER BY capitoleCodRand", vListaCampuri, Convert.ToInt16(pSesAn));
            if (vValori.Count != 0)
            { vListaValori.Add(vValori); }
            else
            {
                vValori = new List<List<string>> { };
                for (int i = 0; i < vValoriGenerale.Count; i++)
                {
                    List<string> vListaGoala = new List<string> { vAn, "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0" };
                    vValori.Add(vListaGoala);
                }
                vListaValori.Add(vValori);
            }

        }
        // construim comanda pentru fiecare rand
        foreach (List<string> vRandValori in vValoriGenerale)
        {
            vComanda += @"INSERT INTO [rapCapitole] ([utilizatorId], [capitol], [codRand], [denumire1], [denumire2], [denumire3], [denumire4], [denumire5], [col1_1], [col1_2], [col1_3], [col1_4], [col1_5], [col1_6], [col1_7], [col1_8], [an1],[col2_1], [col2_2], [col2_3], [col2_4], [col2_5], [col2_6], [col2_7], [col2_8], [an2],[col3_1], [col3_2], [col3_3], [col3_4], [col3_5], [col3_6], [col3_7], [col3_8], [an3],[col4_1], [col4_2], [col4_3], [col4_4], [col4_5], [col4_6], [col4_7], [col4_8], [an4],[col5_1], [col5_2], [col5_3], [col5_4], [col5_5], [col5_6], [col5_7], [col5_8], [an5], [gospodarieId]) VALUES ('" +
                Convert.ToInt32(pUtilizatorId) + "','" +
                vValoriGenerale[vCodRand][0] + "','" +
                vValoriGenerale[vCodRand][1] + "','" +
                vValoriGenerale[vCodRand][2] + "','" +
                vValoriGenerale[vCodRand][3] + "','" +
                vValoriGenerale[vCodRand][4] + "','" +
                vValoriGenerale[vCodRand][5] + "','" +
                vValoriGenerale[vCodRand][6] + "','" +
                vListaValori[0][vCodRand][1].Replace(',', '.') + "', '" +
                vListaValori[0][vCodRand][2].Replace(',', '.') + "', '" +
                vListaValori[0][vCodRand][3].Replace(',', '.') + "', '" +
                vListaValori[0][vCodRand][4].Replace(',', '.') + "', '" +
                vListaValori[0][vCodRand][5].Replace(',', '.') + "', '" +
                vListaValori[0][vCodRand][6].Replace(',', '.') + "', '" +
                vListaValori[0][vCodRand][7].Replace(',', '.') + "', '" +
                vListaValori[0][vCodRand][8].Replace(',', '.') + "', '" +
                vListaValori[0][vCodRand][0].Replace(',', '.') + "', '" +
                vListaValori[1][vCodRand][1].Replace(',', '.') + "', '" +
                vListaValori[1][vCodRand][2].Replace(',', '.') + "', '" +
                vListaValori[1][vCodRand][3].Replace(',', '.') + "', '" +
                vListaValori[1][vCodRand][4].Replace(',', '.') + "', '" +
                vListaValori[1][vCodRand][5].Replace(',', '.') + "', '" +
                vListaValori[1][vCodRand][6].Replace(',', '.') + "', '" +
                vListaValori[1][vCodRand][7].Replace(',', '.') + "', '" +
                vListaValori[1][vCodRand][8].Replace(',', '.') + "', '" +
                vListaValori[1][vCodRand][0].Replace(',', '.') + "', '" +
                vListaValori[2][vCodRand][1].Replace(',', '.') + "', '" +
                vListaValori[2][vCodRand][2].Replace(',', '.') + "', '" +
                vListaValori[2][vCodRand][3].Replace(',', '.') + "', '" +
                vListaValori[2][vCodRand][4].Replace(',', '.') + "', '" +
                vListaValori[2][vCodRand][5].Replace(',', '.') + "', '" +
                vListaValori[2][vCodRand][6].Replace(',', '.') + "', '" +
                vListaValori[2][vCodRand][7].Replace(',', '.') + "', '" +
                vListaValori[2][vCodRand][8].Replace(',', '.') + "', '" +
                vListaValori[2][vCodRand][0].Replace(',', '.') + "', '" +
                vListaValori[3][vCodRand][1].Replace(',', '.') + "', '" +
                vListaValori[3][vCodRand][2].Replace(',', '.') + "', '" +
                vListaValori[3][vCodRand][3].Replace(',', '.') + "', '" +
                vListaValori[3][vCodRand][4].Replace(',', '.') + "', '" +
                vListaValori[3][vCodRand][5].Replace(',', '.') + "', '" +
                vListaValori[3][vCodRand][6].Replace(',', '.') + "', '" +
                vListaValori[3][vCodRand][7].Replace(',', '.') + "', '" +
                vListaValori[3][vCodRand][8].Replace(',', '.') + "', '" +
                vListaValori[3][vCodRand][0].Replace(',', '.') + "', '" +
                vListaValori[4][vCodRand][1].Replace(',', '.') + "', '" +
                vListaValori[4][vCodRand][2].Replace(',', '.') + "', '" +
                vListaValori[4][vCodRand][3].Replace(',', '.') + "', '" +
                vListaValori[4][vCodRand][4].Replace(',', '.') + "', '" +
                vListaValori[4][vCodRand][5].Replace(',', '.') + "', '" +
                vListaValori[4][vCodRand][6].Replace(',', '.') + "', '" +
                vListaValori[4][vCodRand][7].Replace(',', '.') + "', '" +
                vListaValori[4][vCodRand][8].Replace(',', '.') + "', '" +
                vListaValori[4][vCodRand][0].Replace(',', '.') + "', '" + pGospodarieId + "'); ";
            vCodRand++;
        }

        ManipuleazaBD.fManipuleazaBD(vComanda, Convert.ToInt16(pSesAn));
    }
}
