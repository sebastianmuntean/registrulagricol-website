﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ProprietariParcele
/// </summary>
public class ProprietariParcele
{

	public ProprietariParcele()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    public static bool VerificaDacaExistaProrpietarul()
    {
         SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(HttpContext.Current.Session["SESan"]));
         string numeProprietar = GetNumeProrpietarImplicit();
         SqlCommand command = new SqlCommand("select count(id) from proprietariParcele   where proprietariparcele.an =" + HttpContext.Current.Session["SESan"] + " AND idGospodarie = " + HttpContext.Current.Session["SESgospodarieId"] + " and idParcela = " + HttpContext.Current.Session["idParcela"] + " and numeProprietar = '" + numeProprietar + "'", connection);
         int count = Convert.ToInt32(command.ExecuteScalar());
         if (count == 0)
         {
             ManipuleazaBD.InchideConexiune(connection);
             return true;
         }
         ManipuleazaBD.InchideConexiune(connection);
         return false;
    }

    public static string GetNumeProrpietarImplicit()
    {
        string numeProprietarImplicit = string.Empty;
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(HttpContext.Current.Session["SESan"]));
        SqlCommand command = new SqlCommand ("Select membruNume from gospodarii where gospodarieId = " + HttpContext.Current.Session["SESgospodarieId"].ToString() + " and unitateId = " + HttpContext.Current.Session["SESunitateId"].ToString() + "",connection);
        SqlDataReader dataReader = command.ExecuteReader();
        if (dataReader.Read())
        {
            numeProprietarImplicit = dataReader["membruNume"].ToString();
        }
        ManipuleazaBD.InchideConexiune(connection);
        return numeProprietarImplicit;
    }

    public static void SetCapDeGospodarieAsDefaultProprietar()
    {
        if (!VerificaDacaExistaProrpietarul())
            return;
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(HttpContext.Current.Session["SESan"]));
        string numeProprietar = ProprietariParcele.GetNumeProrpietarImplicit();
        SqlCommand command = new SqlCommand("insert into proprietariparcele (idParcela,idGospodarie,numeProprietar,cotaParte, an) values ('" + HttpContext.Current.Session["idParcela"] + "','" + HttpContext.Current.Session["SESgospodarieId"] + "','" + numeProprietar + "',100, " + HttpContext.Current.Session["SESan"] + ")", connection);
        command.ExecuteNonQuery();
        ManipuleazaBD.InchideConexiune(connection);
    }
}