﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for Controale
/// </summary>
public class Controale
{
	public Controale()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    protected void Page_Init(object sender, EventArgs e, Page pPage)
    {
        //foreach (Control c in ContentPlaceHolder1.Controls)
        
        foreach (Control c in pPage.Controls)
        {
            FindControlRecursive(c);
        }
    }
    private static Control FindControlRecursive(Control root)
    {
        if (root.GetType().ToString() == "System.Web.UI.WebControls.Button")
        {
            if (root.ID.StartsWith("Ins"))
                root.Visible = false;
            else if (root.ID.StartsWith("Up"))
                root.Visible = false;
        }
        foreach (Control ctrl in root.Controls)
        {
            Control foundctrl = FindControlRecursive(ctrl);
            if (foundctrl != null)
                return foundctrl;
        }
        return null;
    }
}
