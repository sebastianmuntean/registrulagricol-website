﻿using System;
using System.Linq;

/// <summary>
/// Summary description for Arii
/// </summary>

[Serializable]
public class Arii:ISuprafete, IComparable
{
	public Arii()
    {
	}

    private decimal arie = 0;

    public decimal Arie
    {
        get
        {
            return arie;
        }
        set
        {
            arie = value;
        }
    }

    public override string ToString()
    {
        return arie.ToString("0.0000") + " mp";
    }

    public string ToString(Utils.Marime marime)
    {
        if (marime == Utils.Marime.mp)
        {
            return arie.ToString("0.0000") + " mp";
        }

        return string.Empty;
    }

    public int CompareTo(object Arie)
    {
        if (arie > ((Arii)Arie).arie)
        {
            return 1;
        }
        else
        {
            if (arie < ((Arii)Arie).arie)
            {
                return -1;
            }
            else
            {
                return 0;
            }
        }
    }

    public static bool operator ==(Arii arie, decimal integer)
    {
        return arie.arie == integer;
    }

    public static bool operator !=(Arii arie, decimal integer)
    {
        return arie == integer;
    }

    public static Arii operator -(Arii arie1, Arii arie2)
    {
        return new Arii
        {
            arie = arie1.arie - arie2.arie
        };
    }

    public static Arii operator +(Arii arie1, Arii arie2)
    {
        return new Arii
        {
            arie = arie1.arie + arie2.arie
        };
    }
}