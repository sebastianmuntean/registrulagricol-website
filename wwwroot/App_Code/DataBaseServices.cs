﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;

/// <summary>
/// Summary description for DataBaseServices
/// </summary>
public class DataBaseServices
{
	public DataBaseServices()
	{
	}

    internal static void SalveazaSuccesibil(Succesibili succesibili)
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(succesibili.An));
        SqlCommand cmd = new SqlCommand();

        string sqlString = @"INSERT INTO  [succesibili]
                            ([idDefunct]
                            ,[idGospodarie]
                            ,[nume]
                            ,[idJudet]
                            ,[idLocalitate]
                            ,[strada]
                            ,[numar]
                            ,[numarInregistrare]
                            ,[dataInregistrarii]
                            ,[denumireBirouNotarial]
                            ,[idUnitate]
                            ,[an])
                        VALUES
                            ("     + succesibili.IdDefunct +
                            ", "   + succesibili.IdGospodarie +
                            ", '"  + succesibili.Nume +
                            "', '" + succesibili.IdJudet +
                            "', '" + succesibili.IdLocalitate +
                            "', '" + succesibili.Strada +
                            "', '" + succesibili.NrLocuinta +
                            "', '" + succesibili.NrInregistrare +
                            "', convert(date,'" + succesibili.DataInregistrare + "',104)" +
                            ", '"  + succesibili.DenumireBirouNotarial +
                            "', "  + succesibili.IdUnitate +
                            ", "   + succesibili.An +
                            ")";

        cmd.Connection = connection;
        cmd.CommandText = sqlString;

        try
        {
            cmd.ExecuteNonQuery();
        }
        catch
        {
            throw;
        }
        finally
        {
            connection.Close();
        }
    }

    internal static void StergeSuccesibil(Int64 idSuccesibil)
    {
        SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TNTRegistruAgricolConnectionString"].ConnectionString);
        connection.Open();
        SqlCommand cmd = new SqlCommand();

        string sqlString = @"DELETE FROM succesibili WHERE idSuccesibil = " + idSuccesibil;

        cmd.Connection = connection;
        cmd.CommandText = sqlString;

        try
        {
            cmd.ExecuteNonQuery();
        }
        catch
        {
            throw;
        }
        finally
        {
            connection.Close();
        }
    }

    internal static void ModificaSuccesibil(Succesibili succesibili)
    {
        SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TNTRegistruAgricolConnectionString"].ConnectionString);
        connection.Open();
        SqlCommand cmd = new SqlCommand();

        string sqlString = @"
                            UPDATE  [succesibili]
                            SET 
                                  [idDefunct] = " + succesibili.IdDefunct +
                               ", [idJudet] = " + succesibili.IdJudet +
                               ", [idLocalitate] = " + succesibili.IdLocalitate +
                               ", [nume] = '" + succesibili.Nume +
                               "',[strada] = '" + succesibili.Strada +
                               "', [numar] = '" + succesibili.NrLocuinta +
                               "', [numarInregistrare] = '" + succesibili.NrInregistrare +
                               "', [dataInregistrarii] = convert(date,'" + succesibili.DataInregistrare + "',104)" +
                               ", [denumireBirouNotarial] = '" + succesibili.DenumireBirouNotarial +
                               "' WHERE idSuccesibil = " + succesibili.IdSuccesibil;

        cmd.Connection = connection;
        cmd.CommandText = sqlString;

        try
        {
            cmd.ExecuteNonQuery();
        }
        catch
        {
            throw;
        }
        finally
        {
            connection.Close();
        }
    }
}