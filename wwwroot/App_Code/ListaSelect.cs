﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// Clasa care tine o lista cu proprietati pentru un numar de coloane
/// Creata la:                  09.02.2011
/// Autor:                      Sebastian Muntean
/// Ultima                      actualizare: 09.02.2011
/// Autor:                      Sebastian Muntean

[Serializable]
public class ListaSelect
{
    private string vCol1 = "";
    private string vCol2 = "";
    private string vCol3 = "";
    private string vCol4 = "";
    private string vCol5 = "";
    private string vCol6 = "";
    private string vCol7 = "";
    private string vCol8 = "";
    private string vCol9 = "";
    private string vCol10 = "";
    private string vCol11 = "";
    private string vCol12 = "";
    private string vCol13 = "";
    private string vCol14 = "";
    private string vCol15 = "";
    private string vCol16 = "";
    private string vCol17 = "";
    private string vCol18 = "";
    private string vCol19 = "";
    private string vCol20 = "";
    private string vCol21 = "";
    private string vCol22 = "";
    private string vCol23 = "";
    private string vCol24 = "";
    private string vCol25 = "";
    private string vCol26 = "";
    private int vColInt1 = 0;
    private decimal vColDec1 = 0;

    public int ColInt1
    {
        get
        { return vColInt1; }
        set
        { vColInt1 = value; }
    }
    public string Col1
    {
        get
        { return vCol1; }
        set
        { vCol1 = value; }
    }
    public string Col2
    {
        get
        { return vCol2; }
        set
        { vCol2 = value; }
    }
    public string Col3
    {
        get
        { return vCol3; }
        set
        { vCol3 = value; }
    }
    public string Col4
    {
        get
        { return vCol4; }
        set
        { vCol4 = value; }
    }
    public string Col5
    {
        get
        { return vCol5; }
        set
        { vCol5 = value; }
    }
    public string Col6
    {
        get
        { return vCol6; }
        set
        { vCol6 = value; }
    }
    public string Col7
    {
        get
        { return vCol7; }
        set
        { vCol7 = value; }
    }
    public string Col8
    {
        get
        { return vCol8; }
        set
        { vCol8 = value; }
    }
    public string Col9
    {
        get
        { return vCol9; }
        set
        { vCol9 = value; }
    }
    public string Col10
    {
        get
        { return vCol10; }
        set
        { vCol10 = value; }
    }
    public string Col11
    {
        get
        { return vCol11; }
        set
        { vCol11 = value; }
    }
    public string Col12
    {
        get
        { return vCol12; }
        set
        { vCol12 = value; }
    }
    public string Col13
    {
        get
        { return vCol13; }
        set
        { vCol13 = value; }
    }
    public string Col14
    {
        get
        { return vCol14; }
        set
        { vCol14 = value; }
    }
    public string Col15
    {
        get
        { return vCol15; }
        set
        { vCol15 = value; }
    }
    public string Col16
    {
        get
        { return vCol16; }
        set
        { vCol16 = value; }
    }
    public string Col17
    {
        get
        { return vCol17; }
        set
        { vCol17 = value; }
    }
    public string Col18
    {
        get
        { return vCol18; }
        set
        { vCol18 = value; }
    }
    public string Col19
    {
        get
        { return vCol19; }
        set
        { vCol19 = value; }
    }
    public string Col20
    {
        get
        { return vCol20; }
        set
        { vCol20 = value; }
    }
    public string Col21
    {
        get
        { return vCol21; }
        set
        { vCol21 = value; }
    }
    public string Col22
    {
        get
        { return vCol22; }
        set
        { vCol22 = value; }
    }
    public string Col23
    {
        get
        { return vCol23; }
        set
        { vCol23 = value; }
    }
    public string Col24
    {
        get
        { return vCol24; }
        set
        { vCol24 = value; }
    }
    public string Col25
    {
        get
        { return vCol25; }
        set
        { vCol25 = value; }
    }
    public string Col26
    {
        get
        { return vCol26; }
        set
        { vCol26 = value; }
    }
    public decimal ColDec1
    {
        get
        { return vColDec1; }
        set
        { vColDec1 = value; }
    }
}

