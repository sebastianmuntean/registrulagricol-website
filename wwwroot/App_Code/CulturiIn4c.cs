﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CulturiIn4c
/// </summary>
[Serializable]
public class CulturiIn4c : Culturi
{
	public CulturiIn4c()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public override bool AddCultura(Parcele parcela)
    {
        return CulturiServices.AdaugaInParceleCatreCapitole(parcela);
    }

    public override List<Culturi> GetCulturi()
    {
        return CulturiServices.GetCulturiInParcela(this);
    }

    public override bool UpdateCultura()
    {
        return CulturiServices.UpdateCulturi(this);
    }

    public override bool DeleteCultura()
    {
        return CulturiServices.StergeCultura(this);
    }

}