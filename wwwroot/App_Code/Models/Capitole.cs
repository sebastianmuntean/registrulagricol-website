﻿using System;
using System.Collections.Generic;
using System.Linq;
using InchidereAnRA.App_Code.Services;

namespace Modele
{
    [Serializable]
    public class Capitole
    {
        public Capitole()
        {

        }

        private long id;
        public long Id
        {
            get { return id; }
            set { id = value; }
        }

        private int unitateId;
        public int UnitateId
        {
            get { return unitateId; }
            set { unitateId = value; }
        }

        private long gospodarieId;
        public long GospodarieId
        {
            get { return gospodarieId; }
            set { gospodarieId = value; }
        }

        private long gospodarieIdNou;
        public long GospodarieIdNou
        {
            get { return gospodarieIdNou; }
            set { gospodarieIdNou = value; }
        }

        private int an = 0;
        public int An
        {
            get { return an; }
            set { an = value; }
        }

        private string cod = string.Empty;
        public string Cod
        {
            get { return cod; }
            set { cod = value; }
        }

        private int codRand = 0;
        public int CodRand
        {
            get { return codRand; }
            set { codRand = value; }
        }

        private decimal col1 = 0;
        public decimal Col1
        {
            get { return col1; }
            set { col1 = value; }
        }

        private decimal col2 = 0;
        public decimal Col2
        {
            get { return col2; }
            set { col2 = value; }
        }

        private decimal col3 = 0;
        public decimal Col3
        {
            get { return col3; }
            set { col3 = value; }
        }

        private decimal col4 = 0;
        public decimal Col4
        {
            get { return col4; }
            set { col4 = value; }
        }

        private decimal col5 = 0;
        public decimal Col5
        {
            get { return col5; }
            set { col5 = value; }
        }

        private decimal col6 = 0;
        public decimal Col6
        {
            get { return col6; }
            set { col6 = value; }
        }

        private decimal col7 = 0;
        public decimal Col7
        {
            get { return col7; }
            set { col7 = value; }
        }

        private decimal col8 = 0;
        public decimal Col8
        {
            get { return col8; }
            set { col8 = value; }
        }

        private string col9 = string.Empty;
        public string Col9
        {
            get { return col9; }
            set { col9 = value; }
        }

        private string col10 = string.Empty;
        public string Col10
        {
            get { return col10; }
            set { col10 = value; }
        }

    }
}