﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using InchidereAnRA.App_Code.Models;

namespace Services
{
    [Serializable]
    public class CapitoleServices
    {
        public static List<Modele.Capitole> GetCapitolByCodAndByUnitateAndByYear(string cod, int unitateId, int year)
        {
            List<Modele.Capitole> capitole = new List<Modele.Capitole>();

            SqlConnection vCon = ManipuleazaBD.CreareConexiune(2016);
            SqlCommand vCmd = new SqlCommand();
            vCmd.Connection = vCon;

            vCmd.CommandText = @"SELECT  [capitolId]
                                        ,[unitateId]
                                        ,[gospodarieId]
                                        ,[an]
                                        ,[codCapitol]
                                        ,[codRand]
                                        ,[col1]
                                        ,[col2]
                                        ,[col3]
                                        ,[col4]
                                        ,[col5]
                                        ,[col6]
                                        ,[col7]
                                        ,[col8]
                                        ,[col9]
                                        ,[col10]
                                    FROM [capitole]
                                   WHERE codCapitol = '" + cod + "' AND unitateId = '" + unitateId + "' AND an = " + year;

            SqlDataReader result = vCmd.ExecuteReader();

            while (result.Read())
            {
                Modele.Capitole capitol = new Modele.Capitole();

                capitol.Id = result["capitolId"] != DBNull.Value ? Convert.ToInt64(result["capitolId"]) : 0;
                capitol.UnitateId = unitateId;
                capitol.GospodarieId = result["gospodarieId"] != DBNull.Value ? Convert.ToInt64(result["gospodarieId"]) : 0;
                capitol.An = year;
                capitol.Cod = cod;
                capitol.CodRand = result["codRand"] != DBNull.Value ? Convert.ToInt16(result["codRand"]) : 0;
                capitol.Col1 = result["col1"] != DBNull.Value ? Convert.ToDecimal(result["col1"]) : 0;
                capitol.Col2 = result["col2"] != DBNull.Value ? Convert.ToDecimal(result["col2"]) : 0;
                capitol.Col3 = result["col3"] != DBNull.Value ? Convert.ToDecimal(result["col3"]) : 0;
                capitol.Col4 = result["col4"] != DBNull.Value ? Convert.ToDecimal(result["col4"]) : 0;
                capitol.Col5 = result["col5"] != DBNull.Value ? Convert.ToDecimal(result["col5"]) : 0;
                capitol.Col6 = result["col6"] != DBNull.Value ? Convert.ToDecimal(result["col6"]) : 0;
                capitol.Col7 = result["col7"] != DBNull.Value ? Convert.ToDecimal(result["col7"]) : 0;
                capitol.Col8 = result["col8"] != DBNull.Value ? Convert.ToDecimal(result["col8"]) : 0;
                capitol.Col9 = result["col9"] != DBNull.Value ? result["col9"].ToString() : "";
                capitol.Col10 = result["col10"] != DBNull.Value ? result["col10"].ToString() : "";

                capitole.Add(capitol);
            }

            result.Close();
            ManipuleazaBD.InchideConexiune(vCon);

            return capitole;
        }

        public static bool Save(List<Modele.Capitole> capitole)
        {
            SqlCommand command = new SqlCommand();
            int counterForCommands = 0;
            int counterForRemainingCommands = 0;
            int counterForNumberOfCommits = 0;
            int totalParcurse = 0;

            foreach (Modele.Capitole capitol in capitole)
            {
                counterForCommands++;
                counterForRemainingCommands++;
                totalParcurse++;

                string commandText = @"INSERT INTO  [capitole]
                                                   ([unitateId]
                                                   ,[gospodarieId]
                                                   ,[an]
                                                   ,[codCapitol]
                                                   ,[codRand]
                                                   ,[col1]
                                                   ,[col2]
                                                   ,[col3]
                                                   ,[col4]
                                                   ,[col5]
                                                   ,[col6]
                                                   ,[col7]
                                                   ,[col8]
                                                   ,[col9]
                                                   ,[col10])
                                             VALUES
                                             (" + capitol.UnitateId + "," +
                                                capitol.GospodarieId + "," +
                                                capitol.An + ",'" +
                                                capitol.Cod + "'," +
                                                capitol.CodRand + ",'" +
                                                capitol.Col1.ToString().Replace(",", ".") + "','" +
                                                capitol.Col2.ToString().Replace(",", ".") + "','" +
                                                capitol.Col3.ToString().Replace(",", ".") + "','" +
                                                capitol.Col4.ToString().Replace(",", ".") + "','" +
                                                capitol.Col5.ToString().Replace(",", ".") + "','" +
                                                capitol.Col6.ToString().Replace(",", ".") + "','" +
                                                capitol.Col7.ToString().Replace(",", ".") + "','" +
                                                capitol.Col8.ToString().Replace(",", ".") + "','" +
                                                capitol.Col9 + "','" +
                                                capitol.Col10 + "'" +
                                     ");";

                command.CommandText += commandText;

                //if ((capitole.Count >= 1000 && counterForCommands == 1000) || capitole.Count < 1000 || (capitole.Count - counterForRemainingCommands < 1000))
                if (counterForCommands >= 2500 || totalParcurse == capitole.Count)
                {
                    SqlConnection connection = ManipuleazaBD.CreareConexiune(2016);
                    command.Connection = connection;

                    command.CommandTimeout = 10000;

                    //  command.Transaction = connection.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);

                    try
                    {
                        command.ExecuteNonQuery();
                        ClassLog.fLog(1, 52, DateTime.Now, "inchidere an " + capitol.UnitateId.ToString(), "inchidere capitole" + capitol.Cod, "reusit inserare pachet " + (counterForNumberOfCommits + 1).ToString() + ", " + counterForCommands.ToString() + " din " + capitole.Count + " linii capitole", 0,0);
                        //   command.Transaction.Commit();

                        counterForNumberOfCommits++;
                    }
                    catch(Exception x)
                    {
                        //   command.Transaction.Rollback();
                        ClassLog.fLog(1, 52, DateTime.Now, "inchidere an " + capitole[0].UnitateId.ToString(), "inchidere capitole" + capitol.Cod, "ESUAT inserare pachet " + (counterForNumberOfCommits + 1).ToString() + ", " + counterForCommands.ToString() + " din " + capitole.Count + " linii capitole" + x.Message,0,0);
                        counterForNumberOfCommits--;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    command.CommandText = string.Empty;
                    counterForCommands = 0;
                }
            }
            if (capitole.Any())
            {
                ClassLog.fLog(1, 52, DateTime.Now, "inchidere an " + capitole[0].UnitateId.ToString(), "inchidere capitole" + capitole[0].Cod, "finalizat inserare " + totalParcurse.ToString() + " din " + capitole.Count.ToString() + " linii capitole",0,0);
            }
            else { ClassLog.fLog(1, 52, DateTime.Now, "inchidere an ", "inchidere capitole", "nicio linie capitole",0,0); }
            return counterForNumberOfCommits > 0;
        }
    }
}