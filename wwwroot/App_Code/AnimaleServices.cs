﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AnimaleServices
/// </summary>
public class AnimaleServices
{
    public static short ConnectionYear = 0;

	public AnimaleServices()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    // pe gospodarie / unitate / an
    public static DataTable GetEvolutiaAnimalelorInCursulAnuluiByGospodarieAndByAnAndByUnitatePeSem2(int idGospodarie, short an, short unitateId)
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(ConnectionYear);

        string interogareCapitole = @"SELECT [col2] 
                                        FROM [tntcomputers].[capitole] 
                                       WHERE [codCapitol] = '8' AND [gospodarieId] = " + idGospodarie + " AND [an] = " + an + " AND [unitateId] = " + unitateId + " AND [codRand] in (9, 17, 26, 35, 44, 53, 62, 71, 79, 85) ORDER BY [codRand]";

        SqlDataAdapter capitoleAdapter = new SqlDataAdapter(interogareCapitole, connection);
        DataTable capitoleTable = new DataTable();

        capitoleAdapter.Fill(capitoleTable);

        return capitoleTable;
    }

    public static DataTable GetEvolutiaAnimalelorInCursulAnuluiByGospodarieAndByAnAndByUnitate(int idGospodarie, short an, short unitateId)
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(ConnectionYear);

        string interogareCapitole = @"SELECT [col1], [codRand] 
                                        FROM [tntcomputers].[capitole] 
                                       WHERE [codCapitol] = '8' AND [gospodarieId] = " + idGospodarie + " AND [an] = " + an + " AND [unitateId] = " + unitateId + " ORDER BY [codRand]";

        SqlDataAdapter capitoleAdapter = new SqlDataAdapter(interogareCapitole, connection);
        DataTable capitoleTable = new DataTable();

        capitoleAdapter.Fill(capitoleTable);

        return capitoleTable;
    }

    public static DataTable GetSituatiaAnimalelorLaInceputulSemestruluiByGospodarieAndByAnAndByUnitate(int idGospodarie, short an, short unitateId)
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(ConnectionYear);

        string interogareCapitole = @"SELECT [col1], [codRand] 
                                        FROM [tntcomputers].[capitole] 
                                       WHERE [codCapitol] = '7' AND [gospodarieId] = " + idGospodarie + " AND [an] = " + an + " AND [unitateId] = " + unitateId + " ORDER BY [codRand]";

        SqlDataAdapter capitoleAdapter = new SqlDataAdapter(interogareCapitole, connection);
        DataTable capitoleTable = new DataTable();

        capitoleAdapter.Fill(capitoleTable);

        return capitoleTable;
    }

    
    // pe unitate / an 

    public static DataTable GetEvolutiaAnimalelorInCursulAnuluiByUnitateAndByAndPeSem2(short an, short unitateId)
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(ConnectionYear);
        // luam doar gospodariile care au valori
        string interogareCapitole = @"SELECT [col2], [gospodarieId], [codrand], [col1]
                                        FROM [tntcomputers].[capitole] 
                                       WHERE 
                                              gospodarieId in  (SELECT   [gospodarieId]
				  FROM [tntcomputers].[capitole] 
                  WHERE ([codCapitol] =  '8' or [codCapitol] =  '7' )  AND [an] = " + an + @"  and  unitateId = " + unitateId + @"
                  GROUP BY [gospodarieId] having sum([col1])<>0) 
                AND [codCapitol] = '8' 
                AND [an] = " + an + @" 
                AND [unitateId] = " + unitateId + @" 
                AND [codRand] in (9, 17, 26, 35, 44, 53, 62, 71, 79, 85) 

                ORDER BY [codRand]";

        SqlDataAdapter capitoleAdapter = new SqlDataAdapter(interogareCapitole, connection);
        DataTable capitoleTable = new DataTable();

        capitoleAdapter.Fill(capitoleTable);

        return capitoleTable;
    }

    public static DataTable GetEvolutiaAnimalelorInCursulAnuluiByUnitateAndByAn(short an, short unitateId)
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(ConnectionYear);

        string interogareCapitole = @"SELECT [col1], [codRand], [gospodarieId], [col2]
                                        FROM [tntcomputers].[capitole] 
                                       WHERE [codCapitol] = '8' AND [an] = " + an + " AND [unitateId] = " + unitateId + " ORDER BY [codRand]";

        SqlDataAdapter capitoleAdapter = new SqlDataAdapter(interogareCapitole, connection);
        DataTable capitoleTable = new DataTable();

        capitoleAdapter.Fill(capitoleTable);

        return capitoleTable;
    }

    public static DataTable GetSituatiaAnimalelorLaInceputulSemestruluiByUnitateAndByAn(short an, short unitateId)
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(ConnectionYear);

        string interogareCapitole = @"SELECT [col1], [codRand], [gospodarieId], [col2]
                                        FROM [tntcomputers].[capitole] 
                                       WHERE [codCapitol] = '7' AND [an] = " + an + " AND [unitateId] = " + unitateId + " ORDER BY [codRand]";

        SqlDataAdapter capitoleAdapter = new SqlDataAdapter(interogareCapitole, connection);
        DataTable capitoleTable = new DataTable();

        capitoleAdapter.Fill(capitoleTable);

        return capitoleTable;
    }

    public class RandSablon
    {
        private string an = string.Empty;
        public string An
        {
            get { return an; }
            set { an = value; }
        }
        private string capitol = string.Empty;
        public string Capitol
        {
            get
            { return capitol; }
            set
            { capitol = value; }
        }
        private string codRand = string.Empty;
        public string CodRand
        {
            get
            { return codRand; }
            set
            { codRand = value; }
        }
        public RandSablon()
        {

        }
    }
    public static List<RandSablon> CitesteRanduriSablon(short an, string capitol){
        List<RandSablon> randuriSablon = new List<RandSablon>();
        RandSablon randSablon = new RandSablon();
        List<string> rezultateCampuri = new List<string>() {"an","capitol","codRand"};
        List<List<string>> rezultate = new List<List<string>>();
        string interogare = @"SELECT 
                                 [an]
                                ,[capitol]
                                ,[codRand]
                                FROM [tntcomputers].[sabloaneCapitole]
                                WHERE an = " + an + @" AND capitol = '" + capitol + "'";
        rezultate = ManipuleazaBD.fRezultaListaStringuri(interogare, rezultateCampuri, an);
        foreach(List<string> rezultat in rezultate)
        {
            randSablon = new RandSablon();
            randSablon.An = rezultat[0];
            randSablon.Capitol = rezultat[1];
            randSablon.CodRand = rezultat[2];
            randuriSablon.Add(randSablon);
        }

        return randuriSablon;
    }
    public static List<RandSablon> CitesteRanduriSablon(short an, string capitol, string whereSuplimentar)
    {
        List<RandSablon> randuriSablon = new List<RandSablon>();
        RandSablon randSablon = new RandSablon();
        List<string> rezultateCampuri = new List<string>() { "an", "capitol", "codRand" };
        List<List<string>> rezultate = new List<List<string>>();
        string interogare = @"SELECT 
                                 [an]
                                ,[capitol]
                                ,[codRand]
                                FROM [tntcomputers].[sabloaneCapitole]
                                WHERE an = " + an + @" AND capitol = '" + capitol + "' " + whereSuplimentar; ;
        rezultate = ManipuleazaBD.fRezultaListaStringuri(interogare, rezultateCampuri, an);
        foreach (List<string> rezultat in rezultate)
        {
            randSablon = new RandSablon();
            randSablon.An = rezultat[0];
            randSablon.Capitol = rezultat[1];
            randSablon.CodRand = rezultat[2];
            randuriSablon.Add(randSablon);
        }

        return randuriSablon;
    }
}