﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


/// <summary>
/// Summary description for MembriServices
/// </summary>
public class MembriServices
{
    static SqlCommand command = new SqlCommand();
    public static string volum { get; set; }
    public static string pozitie { get; set; }
    public static string capDeGospodarie { get; set; }
    public static string numeMmebru { get; set; }
    public static string CNP { get; set; }
    public static string dataNasterii { get; set; }

	public MembriServices()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static DataTable GetDetaliiMembri(short an)
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(an);
        DataTable table = CreateListaMembriTableStructure();
        command.Connection = connection;
        command.CommandText = @"select capitolId, volum, nrPozitie, membruNume as 'Cap de gospodarie',nume as 'Nume membru', denumireRudenie, cnp, datanasterii from membri 
        join gospodarii on gospodarii.gospodarieId = membri.gospodarieId  
        WHERE membri.unitateId = " + HttpContext.Current.Session["SESunitateId"] + " and gospodarii.an  = " + HttpContext.Current.Session["SESan"] + " and cnp like '%" + CNP + "%' and volum like '%" + volum + "%' and nrPozitie like '%" + pozitie + "%' and membruNume like '%" + capDeGospodarie + "%' and nume like '%" + numeMmebru + "%' and datanasterii like '%" + dataNasterii + "%'";
        SqlDataReader dataReader = command.ExecuteReader();
        while (dataReader.Read())
        {
            PopulateListaMembriTable(table, dataReader);
        }
        ManipuleazaBD.InchideConexiune(connection);
        return table;
    }

    public static DataTable GetMembri(int unitateId , int gospodarieId, int an)
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(an));
        //command.Connection = connection;
        string comm = "select capitolId, nume, codRudenie from membri where unitateId = " + unitateId + " and gospodarieId=" + gospodarieId + " and an=" + an + "";
        //SqlCommand command = new SqlCommand(comm, connection);
        SqlDataAdapter adapter = new SqlDataAdapter(comm, connection);
        DataTable dt = new DataTable();
        adapter.Fill(dt);
        ManipuleazaBD.InchideConexiune(connection);
        return dt;
    }

    public static Membri GetCapDeGospodarie(int unitateId, int gospodarieId, int an)
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(an));
        //command.Connection = connection;
        string comm = "select top (1) capitolId, nume, gospodarieId, unitateId from membri where unitateId = " + unitateId + " and gospodarieId=" + gospodarieId + " and an='" + an + "' AND codRudenie = '1'  ";
        //SqlCommand command = new SqlCommand(comm, connection);
        SqlDataAdapter adapter = new SqlDataAdapter(comm, connection);
        DataTable dt = new DataTable();
        adapter.Fill(dt);
        ManipuleazaBD.InchideConexiune(connection);
        Membri membru = new Membri();
        membru.IdMembru = Convert.ToInt32(dt.Rows[0][0]);
        membru.Nume = dt.Rows[0][1].ToString();
        membru.IdGospodarie = Convert.ToInt32(dt.Rows[0][2]);
        membru.IdUnitate = Convert.ToInt32(dt.Rows[0][3]);
        return membru;
    }

    private static void PopulateListaMembriTable(DataTable table, SqlDataReader dataReader)
    {
        DataRow row = table.NewRow();
        row["capitolId"] = dataReader["capitolId"];
        row["Volum"] = dataReader["volum"];
        row["Pozitie"] = dataReader["nrPozitie"];
        row["Cap de gospodarie"] = dataReader["Cap de gospodarie"];
        row["Nume membru"] = dataReader["Nume membru"];
        row["Rudenie"] = dataReader["denumireRudenie"];
        row["CNP"] = dataReader["cnp"];
        row["Data nasterii"] = Convert.ToDateTime(dataReader["datanasterii"]).ToShortDateString();
        table.Rows.Add(row);
    }

    private static DataTable CreateListaMembriTableStructure()
    {
        DataTable table = new DataTable();
        table.Columns.Add("capitolId");
        table.Columns.Add("Volum");
        table.Columns.Add("Pozitie");
        table.Columns.Add("Cap de gospodarie");
        table.Columns.Add("Nume membru");
        table.Columns.Add("Rudenie");
        table.Columns.Add("CNP");
        table.Columns.Add("Data nasterii");
        return table;
    }

    public static bool VerificaDacaMaiExistaUnMembruCuAcestCNP(string cnp, short an)
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(an);
        SqlCommand command = new SqlCommand("select count(*) from membri where gospodarieId = " + HttpContext.Current.Session["SESgospodarieId"] + " AND cnp <> '-' and cnp='" + cnp + "'", connection);
        int count = Convert.ToInt32(command.ExecuteScalar());
        if (count == 0)
        {
            ManipuleazaBD.InchideConexiune(connection);
            return true;
        }
        ManipuleazaBD.InchideConexiune(connection);
        return false;
    }

    public static int GetProprietarIdForParcelaByParcelaId(long parcelaId)
    {
        int id = 0;
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(HttpContext.Current.Session["SESan"]));
        SqlCommand command = new SqlCommand("select proprietarId from parcele where parcelaId = " + parcelaId + "", connection);
        try
        {
           id = Convert.ToInt32(command.ExecuteScalar());
        }
        catch { return 0; }

        return id;
    }

     internal static bool CheckIfMemberExistsByCnpAndCifAndAn(string cnp, string cif, int an)
    {
        if (cnp == string.Empty || cif == string.Empty)
        {
            return false;
        }

        SqlConnection connection = ManipuleazaBD.CreareConexiune();
            
        SqlCommand command = new SqlCommand();
        command.Connection = connection;

        command.CommandText = @"SELECT capitolId 
                                    FROM membri 
                                    INNER JOIN unitati ON membri.unitateId = unitati.unitateid
                                    WHERE cnp = @cnp AND unitatecodfiscal = @cifUnitate AND an = @an 
                                    ";

        command.Parameters.AddWithValue("@cnp", cnp.Decrypt());
        command.Parameters.AddWithValue("@cifUnitate", cif);
        command.Parameters.AddWithValue("@an", an);

        SqlDataReader result = command.ExecuteReader();

        return result.HasRows;
    }

    internal static Membri LoadMembruByCnpAndCifAndAn(string cnp, string cif, int an)
    {
        Membri membru = new Membri();

        if (cnp == string.Empty || cif == string.Empty)
        {
            return membru;
        }

        SqlConnection connection = ManipuleazaBD.CreareConexiune();
            
        SqlCommand command = new SqlCommand();
        command.Connection = connection;

        command.CommandText = @"SELECT capitolId, unitati.unitateId, gospodarieId 
                                    FROM membri 
                                    INNER JOIN unitati ON membri.unitateId = unitati.unitateid
                                    WHERE cnp = @cnp AND unitatecodfiscal = @cifUnitate AND an = @an 
                                    ";

        command.Parameters.AddWithValue("@cnp", cnp.Decrypt());
        command.Parameters.AddWithValue("@cifUnitate", cif);
        command.Parameters.AddWithValue("@an", an);

        SqlDataReader result = command.ExecuteReader();

        if (result.HasRows)
        {
            result.Read();

            membru.IdGospodarie = Convert.ToInt64(result["gospodarieId"]);
            membru.IdMembru = Convert.ToInt64(result["capitolId"]);
            membru.IdUnitate = Convert.ToInt32(result["unitateId"]);
            membru.Cnp = cnp;
        }

        result.Close();
        result.Dispose();
        connection.Close();
        connection.Dispose();

        return membru;
    }
}