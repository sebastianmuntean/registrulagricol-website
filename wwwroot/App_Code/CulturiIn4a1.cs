﻿using System;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Summary description for CulturiIn4a1
/// </summary>

[Serializable]
public class CulturiIn4a1 : Culturi
{
	public CulturiIn4a1()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public override bool AddCultura(Parcele parcela)
    {
        return CulturiServices.AdaugaInParceleCatreCapitole(parcela);
    }

    public override List<Culturi> GetCulturi()
    {
        return CulturiServices.GetCulturiInParcela(this);
    }

    public override bool UpdateCultura()
    {
        return CulturiServices.UpdateCulturi(this);
    }

    public override bool DeleteCultura()
    {
        return CulturiServices.StergeCultura(this);
    }

}