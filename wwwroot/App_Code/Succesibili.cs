﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Succesibili
/// </summary>
public class Succesibili
{
    private string idJudet = string.Empty;
    public string IdJudet
    {
        get
        {
            return this.idJudet;
        }
        set
        {
            this.idJudet = value;
        }
    }

    private Int64 idDefunct = -1;
    public Int64 IdDefunct
    {
        get
        {
            return idDefunct;
        }
        set
        {
            idDefunct = value;
        }
    }
   
    private string nume = string.Empty;
    public string Nume
    {
        get
        {
            return nume;
        }
        set
        {
            nume = value;
        }
    }

    private string idLocalitate = string.Empty;
    public string IdLocalitate
    {
        get
        {
            return this.idLocalitate;
        }
        set
        {
            this.idLocalitate = value;
        }
    }

    private string strada = string.Empty;
    public string Strada
    {
        get
        {
            return this.strada;
        }
        set
        {
            this.strada = value;
        }
    }

    private string nrLocuinta = string.Empty;
    public string NrLocuinta
    {
        get
        {
            return this.nrLocuinta;
        }
        set
        {
            this.nrLocuinta = value;
        }
    }

    private string nrInregistrare = string.Empty;
    public string NrInregistrare
    {
        get
        {
            return this.nrInregistrare;
        }
        set
        {
            this.nrInregistrare = value;
        }
    }

    private DateTime dataInregistrare = DateTime.MinValue;
    public DateTime DataInregistrare
    {
        get
        {
            return this.dataInregistrare;
        }
        set
        {
            this.dataInregistrare = value;
        }
    }

    private string denumireBirouNotarial = string.Empty;
    public string DenumireBirouNotarial
    {
        get
        {
            return this.denumireBirouNotarial;
        }
        set
        {
            this.denumireBirouNotarial = value;
        }
    }

    private int idUnitate = -1;
    public int IdUnitate
    {
        get
        {
            return this.idUnitate;
        }
        set
        {
            this.idUnitate = value;
        }
    }

    private int an = -1;
    public int An
    {
        get
        {
            return this.an;
        }
        set
        {
            this.an = value;
        }
    }

    private Int64 idSuccesibil = -1;
    public long IdSuccesibil
    {
        get
        {
            return this.idSuccesibil;
        }
        set
        {
            this.idSuccesibil = value;
        }
    }

    private int idGospodarie = -1;
    public int IdGospodarie
    {
        get
        {
            return this.idGospodarie;
        }
        set
        {
            this.idGospodarie = value;
        }
    }


    public Succesibili()
	{
	}

    public void Salveaza()
    {
        DataBaseServices.SalveazaSuccesibil( this );
    }

    public void Sterge()
    {
        DataBaseServices.StergeSuccesibil(idSuccesibil);
    }

    public void Modifica()
    {
        DataBaseServices.ModificaSuccesibil(this);
    }
}