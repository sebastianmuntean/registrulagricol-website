﻿using System;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Summary description for Parcelel
/// </summary>
[Serializable]
public class Parcele:IComparable
{
    public Parcele()
    {
    }

    int unitateId = -1;
    public int UnitateId
    {
        get
        {
            return this.unitateId;
        }
        set
        {
            this.unitateId = value;
        }
    }

    long idGospodarie = 0;
    public long IdGospodarie
    {
        get
        {
            return this.idGospodarie;
        }
        set
        {
            this.idGospodarie = value;
        }
    }

    long id = 0;
    public long Id
    {
        get
        {
            return this.id;
        }
        set
        {
            this.id = value;
        }
    }

    int an = 0;
    public int An
    {
        get
        {
            return this.an;
        }
        set
        {
            this.an = value;
        }
    }

    string adresa = string.Empty;
    public string Adresa
    {
        get
        {
            return this.adresa;
        }
        set
        {
            this.adresa = value;
        }
    }

    string denumire = string.Empty;
    public string Denumire
    {
        get
        {
            return this.denumire;
        }
        set
        {
            this.denumire = value;
        }
    }

    string codRand = string.Empty;
    public string CodRand
    {
        get
        {
            return this.codRand;
        }
        set
        {
            this.codRand = value;
        }
    }

    string nrTopo = string.Empty;
    public string NrTopo
    {
        get
        {
            return this.nrTopo;
        }
        set
        {
            this.nrTopo = value;
        }
    }

    string cf = string.Empty;
    public string CF
    {
        get
        {
            return this.cf;
        }
        set
        {
            this.cf = value;
        }
    }

    string categorie = string.Empty;
    public string Categorie
    {
        get
        {
            return this.categorie;
        }
        set
        {
            this.categorie = value;
        }
    }

    string nrBloc = string.Empty;
    public string NrBloc
    {
        get
        {
            return this.nrBloc;
        }
        set
        {
            this.nrBloc = value;
        }
    }

    string mentiuni = string.Empty;
    public string Mentiuni
    {
        get
        {
            return this.mentiuni;
        }
        set
        {
            this.mentiuni = value;
        }
    }

    string nrCadastral = string.Empty;
    public string NrCadastral
    {
        get
        {
            return this.nrCadastral;
        }
        set
        {
            this.nrCadastral = value;
        }
    }

    long capitolId = 0;
    public long CapitolId
    {
        get
        {
            return capitolId;
        }
        set
        {
            capitolId = value;
        }
    }

    string nrCadastralProvizoriu = string.Empty;
    public string NrCadastralProvizoriu
    {
        get
        {
            return nrCadastralProvizoriu;
        }
        set
        {
            nrCadastralProvizoriu = value;
        }
    }

    string localitate = string.Empty;
    public string Localitate
    {
        get
        {
            return localitate;
        }
        set
        {
            localitate = value;
        }
    }

    string volum = string.Empty;
    public string Volum
    {
        get
        {
            return volum;
        }
        set
        {
            volum = value;
        }
    }

    string nrPozitie = string.Empty;
    public string NrPozitie
    {
        get
        {
            return nrPozitie;
        }
        set
        {
            nrPozitie = value;
        }
    }

    string tarla = string.Empty;
    public string Tarla
    {
        get
        {
            return tarla;
        }
        set
        {
            tarla = value;
        }
    }

    string titluProprietate = string.Empty;

    public string TitluProprietate
    {
        get
        {
            return titluProprietate;
        }
        set
        {
            titluProprietate = value;
        }
    }

    string clasaBonitate = string.Empty;

    public string ClasaBonitate
    {
        get
        {
            return clasaBonitate;
        }
        set
        {
            clasaBonitate = value;
        }
    }

    string punctaj = string.Empty;
    public string Punctaj
    {
        get
        {
            return punctaj;
        }
        set
        {
            punctaj = value;
        }
    }

    string idInitial = string.Empty;
    public string IdInitial
    {
        get
        {
            return idInitial;
        }
        set
        {
            idInitial = value;
        }
    }

    string timpAdaugare = string.Empty;
    public string TimpAdaugare
    {
        get
        {
            return timpAdaugare;
        }
        set
        {
            timpAdaugare = value;
        }
    }

    long proprietarId = 0;
    public long ProprietarId
    {
        get
        {
            return proprietarId;
        }
        set
        {
            proprietarId = value;
        }
    }

    public bool IsIntravilan
    {
        get
        {
            if(suprafataIntravilan!=null)
            {
                return suprafataIntravilan.Hectare > 0 || suprafataIntravilan.Ari > 0;
            }
            return false;
        }
    }

    public bool IsExtravilan
    {
        get
        {
            if(suprafataExtravilan!=null)
            {
                return suprafataExtravilan.Hectare > 0 || suprafataExtravilan.Ari > 0;
            }
            return false;
        }
    }

    public Suprafete SuprafataTotala
    {
        get
        {
            if ( suprafataExtravilan != null && suprafataIntravilan != null )
            {
                return suprafataIntravilan.CompareTo(suprafataExtravilan) > 0 ? suprafataIntravilan : suprafataExtravilan;
            }
            return new Suprafete();
        }
    }

    Suprafete suprafataData;
    public Suprafete SuprafataData
    {
        get
        {
            return this.suprafataData;
        }
        set
        {
            this.suprafataData = value;
        }
    }

    Suprafete suprafataPrimita;
    public Suprafete SuprafataPrimita
    {
        get
        {
            if (suprafataPrimita.Ari < 0)
            {
                while (suprafataPrimita.Ari < 0)
                {
                    suprafataPrimita.Hectare = suprafataPrimita.Hectare - 1;
                    suprafataPrimita.Ari = suprafataPrimita.Ari + 100;
                }
            }
            else
            {
                if (suprafataPrimita.Ari >= 100)
                {
                    while (suprafataPrimita.Ari >= 100)
                    {
                        suprafataPrimita.Hectare = suprafataPrimita.Hectare + 1;
                        suprafataPrimita.Ari = suprafataPrimita.Ari - 100;
                    }
                }
            }
            return suprafataPrimita;
        }
        set
        {
            suprafataPrimita = value;
        }
    }

    public Suprafete SuprafataInFolosinta
    {
        get
        {
            if ( suprafataData!=null && SuprafataTotala!=null )
            {
                return SuprafataTotala - suprafataData;
            }
            else
            {
                return suprafataPrimita;
            }
        }
    }

    private ISuprafete suprafataCultivata;
    public ISuprafete SuprafataCultivata
    {
        get
        {
            return suprafataCultivata;
        }
        set
        {
            suprafataCultivata = value;
        }
    }

    public Suprafete SuprafataRamasaDeCultivat
    {
        get
        {
            if ( suprafataCultivata != null && SuprafataInFolosinta != null )
            {
                Suprafete suprafata = SuprafataInFolosinta - (Suprafete)suprafataCultivata;
                
                if(suprafata.Ari<0)
                {
                    while ( suprafata.Ari<0 )
                    {
                        suprafata.Hectare = suprafata.Hectare - 1;
                        suprafata.Ari = suprafata.Ari + 100;
                    }
                }
                else
                {
                    if ( suprafata.Ari>=100 )
                    {
                        while ( suprafata.Ari>=100 )
                        {
                            suprafata.Hectare = suprafata.Hectare + 1;
                            suprafata.Ari = suprafata.Ari - 100;
                        }
                    }
                }
                return suprafata;
            }

            return new Suprafete();
        }
    }

    Suprafete suprafataIntravilan;
    public Suprafete SuprafataIntravilan
    {
        get
        {
            return suprafataIntravilan;
        }
        set
        {
            suprafataIntravilan = value;
        }
    }

    Suprafete suprafataExtravilan;
    public Suprafete SuprafataExtravilan
    {
        get
        {
            return suprafataExtravilan;
        }
        set
        {
            suprafataExtravilan = value;
        }
    }

    public List<List<Culturi>> Culturi
    {
        get
        {
            return new List<List<Culturi>>()
            {
                culturaIn4a,
                culturaIn4a1,
                culturaIn4b1,
                culturaIn4b2,
                culturaIn4c
            };
        }
    }

    List<Culturi> culturaIn4a;
    public List<Culturi> CulturaIn4a
    {
        get
        {
            return this.culturaIn4a;
        }
        set
        {
            this.culturaIn4a = value;
        }
    }

    List<Culturi> culturaIn4a1;
    public List<Culturi> CulturaIn4a1
    {
        get
        {
            return this.culturaIn4a1;
        }
        set
        {
            this.culturaIn4a1 = value;
        }
    }

    List<Culturi> culturaIn4b1;
    public List<Culturi> CulturaIn4b1
    {
        get
        {
            return this.culturaIn4b1;
        }
        set
        {
            this.culturaIn4b1 = value;
        }
    }

    List<Culturi> culturaIn4b2;
    public List<Culturi> CulturaIn4b2
    {
        get
        {
            return this.culturaIn4b2;
        }
        set
        {
            this.culturaIn4b2 = value;
        }
    }

    List<Culturi> culturaIn4c;
    public List<Culturi> CulturaIn4c
    {
        get
        {
            return this.culturaIn4c;
        }
        set
        {
            this.culturaIn4c = value;
        }
    }

    public Parcele GetParcelaById()
    {
        return ParceleServices.GetParcelaById(id);
    }

    public bool Adauga()
    {
        return ParceleServices.Adauga(this);
    }

    public bool Modifica()
    {
        return ParceleServices.Modifica(this);
    }

    public int CompareTo(object parcela)
    {
        return SuprafataTotala.CompareTo(((Parcele)parcela).SuprafataTotala);
    }

    
}

