﻿using System;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Summary description for Contracte
/// </summary>
[Serializable]
public class Contracte
{
    public enum TipuriContracte
    {
        In_folosinta = 0,
        Dat_in_arenda = 3,
        Dat_in_parte = 4,
        Dat_cu_titlu_gratuit = 5,
        Dat_in_concesiune = 6,
        Dat_in_asociere = 7,
        Dat_sub_alte_forme = 8 
    }
    
    private long idParcela;
    private string cumparator;
    private DateTime dataFinal;
    private DateTime dataInceput;
    private DateTime dataSemnarii;
    private string detalii;
    private bool expirat;
    private string numar;
    private decimal redeventa;
    private string vanzator;
    private Suprafete suprafata;
    private TipuriContracte tipContract;

    public TipuriContracte TipContract
    {
        get
        {
            return this.tipContract;
        }
        set
        {
            this.tipContract = value;
        }
    }

    public Suprafete Suprafata
    {
        get
        {
            return suprafata;
        }
        set
        {
            suprafata = value;
        }
    }

    public string Vanzator
    {
        get
        {
            return this.vanzator;
        }
        set
        {
            this.vanzator = value;
        }
    }

    public decimal Redeventa
    {
        get
        {
            return this.redeventa;
        }
        set
        {
            this.redeventa = value;
        }
    }

    public string Numar
    {
        get
        {
            return this.numar;
        }
        set
        {
            this.numar = value;
        }
    }

    public bool Expirat
    {
        get
        {
            return this.expirat;
        }
        set
        {
            this.expirat = value;
        }
    }

    public string Detalii
    {
        get
        {
            return this.detalii;
        }
        set
        {
            this.detalii = value;
        }
    }

    public DateTime DataSemnarii
    {
        get
        {
            return this.dataSemnarii;
        }
        set
        {
            this.dataSemnarii = value;
        }
    }

    public DateTime DataInceput
    {
        get
        {
            return this.dataInceput;
        }
        set
        {
            this.dataInceput = value;
        }
    }

    public DateTime DataFinal
    {
        get
        {
            return this.dataFinal;
        }
        set
        {
            this.dataFinal = value;
        }
    }

    public string Cumparator
    {
        get
        {
            return this.cumparator;
        }
        set
        {
            this.cumparator = value;
        }
    }

    public long IdParcela
    {
        get
        {
            return this.idParcela;
        }
        set
        {
            this.idParcela = value;
        }
    }

    public Contracte GetLastActiveContractByIdParcela()
    {
        return ContracteServices.GetLastActiveContractByIdParcela(idParcela);
    }

    public List<Contracte> GetAllActiveContractsByIdParcela()
    {
        return ContracteServices.GetAllActiveContractsByIdParcela(idParcela);
    }
	
    private Int64 id = 0;

    public Int64 Id
    {
        get
        {
            return id;
        }
        set
        {
            id = value;
        }
    }

    private Int64 idDa = 0;

    public Int64 IdDa
    {
        get
        {
            return idDa;
        }
        set
        {
            idDa = value;
        }
    }

    private Int64 idPrimeste = 0;

    public Int64 IdPrimeste
    {
        get
        {
            return idPrimeste;
        }
        set
        {
            idPrimeste = value;
        }
    }

    public List<Tuple<Int64, Int64>> GetGospodarieIdDa()
    {
        return ContracteServices.GetGospodarieIdDa();
    }

    public bool SchimbaIdDaCuIdInitial(List<Tuple<long, long>> idDaListCuIdInitial)
    {
        return ContracteServices.SchimbaIdDaCuIdInitial(idDaListCuIdInitial);
    }

    public List<Tuple<long, long>> GetGospodarieIdPrimeste()
    {
        return ContracteServices.GetGospodarieIdPrimeste();
    }

    public bool SchimbaIdPrimesteCuIdInitial(List<Tuple<long, long>> idListCuIdInitial)
    {
        return ContracteServices.SchimbaIdPrimesteCuIdInitial(idListCuIdInitial);
    }
}
 
