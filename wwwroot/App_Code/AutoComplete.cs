using System;
using System.Collections.Generic;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.SessionState;
using AjaxControlToolkit;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class AutoComplete : WebService
{
    public AutoComplete()
    {
    }
    [WebMethod]
    public string[] GetCompletionList(string prefixText, int count, string contextKey)
    {
        if (count == 0)
        {
            count = 10;
        }
        DataTable dt = GetRecords(prefixText, contextKey);
        if (contextKey.Contains("adeverinteTip"))
        {
            List<string> items = new List<string>(count);
            string custItem = string.Empty;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string strName = dt.Rows[i][0].ToString();
                string strValue = dt.Rows[i][1].ToString();
                custItem = AutoCompleteExtender.CreateAutoCompleteItem(strName, strValue);
                items.Add(custItem);
            }
            return items.ToArray();
        }
        else
        {
            List<string> items = new List<string>(count);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string strName = dt.Rows[i][0].ToString();
                items.Add(strName);
            }
            return items.ToArray();
        }
    }

    public DataTable GetRecords(string strName, string contextKey)
    {
        string strConn = ConfigurationManager.ConnectionStrings["TNTRegistruAgricolConnectionString"].ConnectionString;
        SqlConnection vCon = new SqlConnection(strConn);
        SqlCommand vCmd1 = new SqlCommand();
        vCmd1.Connection = vCon;
        SqlCommand vCmd2 = new SqlCommand();
        vCmd2.Connection = vCon;
        vCmd1.CommandType = System.Data.CommandType.Text;

        string vConditie1 = "", vConditie2 = "";
        if (contextKey.IndexOf("#") != -1)
        {
            string vTemp = "";
            string vContext = contextKey.Remove(0, contextKey.IndexOf("#"));
            for (int a = 0; a < vContext.Length; a++)
            {
                if (vContext[a].ToString() != "#")
                {
                    vTemp += vContext[a].ToString();
                }
                if (vContext[a].ToString() == "#" || a + 1 == vContext.Length)
                {
                    if (vTemp != "")
                    {
                        if (vConditie1 == "")
                        {
                            vConditie1 = vTemp;
                            vTemp = "";
                            continue;
                        }
                        if (vConditie1 != "" && vConditie2 == "")
                        {
                            vConditie2 = vTemp;
                            vTemp = "";
                            continue;
                        }
                    }
                }
            }
            contextKey = contextKey.Remove(contextKey.IndexOf("#"), contextKey.Length - contextKey.IndexOf("#"));
        }

        //        cmd.Parameters.AddWithValue("@Denumire", strName);
        switch (contextKey)
        {
            case "Localitate":
                vCmd1.CommandText = "SELECT TOP(10) localitateDenumire, localitateId FROM localitati WHERE (localitateDenumire LIKE '%" + strName + "%')";
                break;
            case "Login":
                vCmd1.CommandText = "SELECT TOP(10) utilizatorLogin, utilizatorId FROM utilizatori WHERE (utilizatorLogin LIKE '%" + strName + "%')";
                break;
            case "Nume":
                vCmd1.CommandText = "SELECT TOP(10) utilizatorNume, utilizatorPrenume, utilizatorId FROM utilizatori WHERE (utilizatorNume LIKE '%" + strName + "%') OR (utilizatorPrenume LIKE '%" + strName + "%')";
                break;
            case "UnitateTNT":
                vCmd1.CommandText = "SELECT TOP(10) unitateDenumire, unitateId FROM unitati WHERE (unitateDenumire LIKE N'%" + strName + "%')";
                break;
            case "Unitate":
                vCmd1.CommandText = "SELECT TOP(10) unitateDenumire, unitateId FROM unitati WHERE (unitateDenumire LIKE N'%" + strName + "%')";
                break;
            case "adeverinteTip":
                if (vConditie1 != "1")
                {
                    vCmd1.CommandText = "SELECT adevSablonDenumire, adevSablonId FROM [adeverinteSabloane] LEFT OUTER JOIN unitati ON unitati.unitateId = adeverinteSabloane.unitateID WHERE [adeverinteSabloane].[adevTip] in ('" + vConditie2 + "','0') and adevSablonDenumire like '%" + strName + "%' order by adevSablonDenumire";
                }
                else
                {
                    vCmd1.CommandText = "SELECT adevSablonDenumire, adevSablonId FROM [adeverinteSabloane] LEFT OUTER JOIN unitati ON unitati.unitateId = adeverinteSabloane.unitateID WHERE adevSablonDenumire like '%" + strName + "%' order by adevSablonDenumire";
                }
                break;
        }
        DataSet objDs = new DataSet();
        SqlDataAdapter dAdapter = new SqlDataAdapter();
        dAdapter.SelectCommand = vCmd1;
        vCon.Open();
        dAdapter.Fill(objDs);
        vCon.Close();
        return objDs.Tables[0];
    }
}

