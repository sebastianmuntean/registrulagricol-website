﻿using System;
using System.Linq;

/// <summary>
/// Summary description for ValidateCIFCUI
/// </summary>
public static class Utils
{
    public static bool ValidateCIF(string CIF)
    {

        int[] c = new int[CIF.Length];
        int[] b = new int[9];
        int produs = 0;
        b[0] = 2;
        b[1] = 3;
        b[2] = 5;
        b[3] = 7;
        b[4] = 1;
        b[5] = 2;
        b[6] = 3;
        b[7] = 5;
        b[8] = 7;

        string cif_inversat = "", msg = "", cifra_control = "";
        if (CIF.Trim().Length > 10)
        {
            msg = "invalid";
        }
        if (msg != "invalid")
        {
            for (int i = 0; i <= CIF.Length - 1; i++)
            {
                try
                {

                    c[i] = System.Convert.ToInt32(CIF.Substring(i, 1));
                    cifra_control = CIF.Substring(i, 1);
                }
                catch (Exception)
                {
                    return false;
                }
            }

            int j = 0;
            for (int i = CIF.Length - 2; i >= 0; i--)
            {
                cif_inversat = cif_inversat + Convert.ToString(c[i]);
                produs = produs + b[j] * c[i];
                j += 1;
            }
            int t = produs * 10;
            int r = t % 11;
            if (Convert.ToString(r) == cifra_control)
            {
                return true;
            }

        }
        return false;
    }

    public static bool ValidateCNP (string cnp)
    {
        if ( cnp=="-" )
        {
            return true;
        }

        if (cnp.Length != 13)
        {
            return false;
        }
        const string a = "279146358279";

        long j = 0;

        if (!long.TryParse(cnp, out j))

            return false;

        long suma = 0;

        for (int i = 0; i < 12; i++)

            suma += long.Parse(cnp.Substring(i, 1)) * long.Parse(a.Substring(i, 1));

        long rest = suma - 11 * (int)(suma / 11);

        rest = rest == 10 ?

       1 : rest;

        if (long.Parse(cnp.Substring(12, 1)) != rest)
        {
           
            return false;
        }
        return true;
    }

    public enum Marime
    {
        ha,
        ari,
        mp
    }

    public enum CapitolCulturi
    {
        A,
        A1,
        B1,
        B2,
        C
    }
   
    public static bool AutoAdaugareInCapitole(int idUnitate, short an)
    {
      return ManipuleazaBD.fRezultaUnString("select unitateAdaugaDinC2BC4abc from unitati where unitateId='" +idUnitate + "'", "unitateAdaugaDinC2BC4abc", an) == "True";
    }

    public static string Capitalize(this string text)
    {
        if (string.IsNullOrEmpty(text))
        {
            return string.Empty;
        }

        text = text.Trim().ToLower();

        if (text.Contains(" "))
        {
            string[] words = text.Split(' ');
            text = string.Empty;

            foreach (string word in words)
            {
                text += char.ToUpper(word[0]) + word.Substring(1) + " ";
            }

            return text.TrimEnd();
        }

        return char.ToUpper(text[0]) + text.Substring(1);
    }

    public static bool ContainsNumericChars(this string text)
    {
        return text.ToCharArray().Any(c => Char.IsDigit(c));
    }

    public static bool ContainsAlphaChars(this string text)
    {
        return text.ToCharArray().Any(c => Char.IsLetter(c));
    }
}