﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MasterModel
/// </summary>
public class MasterModel
{
	public MasterModel()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    public static string parcelaCategorie
    {
        get;
        set;
    }

    public static string parcelaCF
    {
        get;
        set;
    }
    public static string parcelaDenumire
    {
        get;
        set;
    }

    public static string parcelaBloc
    {
        get;
        set;
    }

    public static string parcelaTopo
    {
        get;
        set;
    }

    public static string parcelaNrCadastral
    {
        get;
        set;
    }

    public static string parcelaNrCadastralProvizoriu
    {
        get;
        set;
    }

    public static string nrDosar
    {
        get;
        set;
    }

    public static string nrRegistruGeneral
    {
        get;
        set;
    }
    public static string numeOfertant
    {
        get;
        set;
    }

}