﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for clsUnitati
/// </summary>
public class clsUnitati
{
	public clsUnitati()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public static void AdaugaInCapitoleCentralizate(string vUnitate, string pAn)
    {
        // luam lista de ani din ciclu
        //iclul curent 
        string vListaAniDinCiclu = ManipuleazaBD.fRezultaUnString("SELECT * FROM cicluri WHERE cicluAni like '%" + pAn + "%'", "cicluAni", Convert.ToInt16(pAn));
        char[] vCar = new char[] { '#' };
        string[] vListaAni = vListaAniDinCiclu.Split(vCar, StringSplitOptions.RemoveEmptyEntries);
        string vInsert = "";
        string vAnul = "2011";
        foreach (string vAn in vListaAni)
        {
            if (vAn == "2010")
                vAnul = "2011";
            else
                vAnul = vAn;
            // din sabloaneCapitole luam fiecare rand si introducem in centralizatoare
            List<string> vCampuri = new List<string> { "capitol", "codRand" };
            List<List<string>> vListaSabloane = ManipuleazaBD.fRezultaListaStringuri("SELECT * from sabloaneCapitole WHERE an = " + vAnul + " ORDER BY capitol, codRand", vCampuri, Convert.ToInt16(pAn));

            for (int i = 0; i <= 3; i++)
            {
                if (vListaSabloane.Count > 0)
                {
                    foreach (List<string> vSablon in vListaSabloane)
                    {
                        vInsert += "INSERT INTO capitoleCentralizate (unitateId, gospodarieId, an, codCapitol, codRand, col1, col2, col3, col4, col5, col6, col7, col8, gospodarieTip) VALUES ('" + vUnitate + "', '0', '" + vAn + "','" + vSablon[0] + "', '" + vSablon[1] + "', '0', '0', '0', '0', '0', '0', '0', '0', '" + i.ToString() + "'); ";
                    }
                }
                ManipuleazaBD.fManipuleazaBD(vInsert, Convert.ToInt16(pAn));
                vInsert = "";
            }
        }
    }
    public static bool ValideazaVolumSiPozitie(int unitateId, short an)
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(an);
        string select = "select coalesce(valideazaVolumSiPozitie,0) from unitati where unitateId = " + unitateId + "";
        SqlCommand command = new SqlCommand(select, connection);
        bool valid = Convert.ToBoolean(command.ExecuteScalar());
        ManipuleazaBD.InchideConexiune(connection);
        return valid;
    }

    public static string GetVolumVechi(long gospodarieId, short anCurent)
    {
        Int64 idVechi = GetIdVechi(gospodarieId, anCurent);

        return VolumVechi(idVechi, anCurent);
    }
 
    private static string VolumVechi(long idVechi, short anCurent)
    {
        string select = "select coalesce(volum,'') as volum from gospodarii where gospodarieId = " + idVechi + "";
        SqlConnection connection = ManipuleazaBD.CreareConexiune(anCurent);
        SqlCommand command = new SqlCommand(select, connection);
        string volum = command.ExecuteScalar().ToString();
        ManipuleazaBD.InchideConexiune(connection);

        if(volum==string.Empty)
        {
            VolumVechi(idVechi, Convert.ToInt16(anCurent - 1));
        }

        return volum;
    }

    public static string GetPozitieVeche(long gospodarieId, short anCurent)
    {
        Int64 idVechi = GetIdVechi(gospodarieId, anCurent);
        
        return PozitieVeche(idVechi, anCurent);
    }
 
    private static string PozitieVeche(long idVechi, short anCurent)
    {
        string select = "select coalesce(nrPozitie,'') as nrPozitie from gospodarii where gospodarieId = " + idVechi + "";
       
        SqlConnection connection = ManipuleazaBD.CreareConexiune(anCurent);
        SqlCommand command = new SqlCommand(select, connection);
        string pozitie = command.ExecuteScalar().ToString();
        ManipuleazaBD.InchideConexiune(connection);

        if (pozitie == string.Empty)
        {
            VolumVechi(idVechi, Convert.ToInt16(anCurent - 1));
        }
        
        return pozitie;
    }

    private static Int64 GetIdVechi(long gospodarieId, short an)
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(an);
        string select = "select coalesce(gospodarieIdInitial,"+gospodarieId+") from gospodarii where gospodarieId = " + gospodarieId + "";
        SqlCommand command = new SqlCommand(select, connection);
        Int64 IdVechi = Convert.ToInt64(command.ExecuteScalar());
        ManipuleazaBD.InchideConexiune(connection);
        return IdVechi;
    }
}
