﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

/// <summary>
/// Summary description for SablonAdeverintaServices
/// </summary>
public class SablonAdeverintaServices
{
    public static SablonAdeverinta GetSablonByTip(int tipSablon)
    {
        if (tipSablon == 0)
        {
            return new SablonAdeverinta();
        }

        SablonAdeverinta sablon = new  SablonAdeverinta();
        SqlCommand command = new SqlCommand();

        command.CommandText = @"SELECT [adevSablonId]
                                      ,[unitateId]
                                      ,[adevTip]
                                      ,[adevSablonTip]
                                      ,[adevSablonTitlu]
                                      ,[adevSablonText]
                                      ,[adevSubsolId]
                                      ,[adevAntetId]
                                      ,[adevSablonObservatii]
                                      ,[adevSablonDenumire]
                                  FROM [adeverinteSabloane]
                                  WHERE adevSablonId = @tipSablon
                             ";

        command.Parameters.AddWithValue("@tipSablon", tipSablon);

        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(DateTime.Now.Year));
        command.Connection = connection;

        SqlDataReader result = command.ExecuteReader();

        while (result.Read())
        {
            sablon = GetSablon(result);
        }

        result.Close();
        ManipuleazaBD.InchideConexiune(connection);

        return sablon;
    }

    public static List<SablonAdeverinta> GetSabloaneByUnitate(int unitateId)
    {
        if(unitateId == 0)
        {
            return new List<SablonAdeverinta>();
        }

        List<SablonAdeverinta> sabloane = new List<SablonAdeverinta>();

        SqlCommand command = new SqlCommand();

        command.CommandText = @"SELECT [adevSablonId]
                                      ,[unitateId]
                                      ,[adevTip]
                                      ,[adevSablonTip]
                                      ,[adevSablonTitlu]
                                      ,[adevSablonText]
                                      ,[adevSubsolId]
                                      ,[adevAntetId]
                                      ,[adevSablonObservatii]
                                      ,[adevSablonDenumire]
                                  FROM [adeverinteSabloane]
                                  WHERE unitateId = @unitateId
                             ";
        command.Parameters.AddWithValue("@unitateId", unitateId);

        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(DateTime.Now.Year));
        command.Connection = connection;

        SqlDataReader result = command.ExecuteReader();

        while (result.Read())
        {
            SablonAdeverinta sablon = GetSablon(result);

            sabloane.Add(sablon);
        }

        result.Close();
        ManipuleazaBD.InchideConexiune(connection);

        return sabloane;
    }

    private static SablonAdeverinta GetSablon(SqlDataReader result)
    {
        SablonAdeverinta sablon = new SablonAdeverinta();

        sablon.Id = Convert.ToInt32(result["adevSablonId"]);
        sablon.UnitateId = Convert.ToInt32(result["unitateId"]);
        sablon.TipAdeverinta = Convert.ToInt32(result["adevTip"]);
        sablon.Tip = Convert.ToInt32(result["adevSablonTip"]);
        sablon.Titlu = result["adevSablonTitlu"].ToString();
        sablon.Text = result["adevSablonText"].ToString();
        sablon.Denumire = result["adevSablonDenumire"].ToString();

        return sablon;
    }
}