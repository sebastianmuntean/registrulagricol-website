﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

/// <summary>
/// Summary description for BuildingServices
/// </summary>
public class BuildingServices
{
    public static short ConnectionYear = 0;

	public BuildingServices()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    internal static bool AddBuilding(Building building)
    {
        if (building.Id == -1)
        {
            return false;
        }

        SqlConnection vCon = ManipuleazaBD.CreareConexiune(ConnectionYear);
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;

        vCmd.CommandText = @"INSERT INTO capitol11
                                        ([unitateId]
                                        ,[gospodarieId]
                                        ,[an]
                                        ,[adresa]
                                        ,[zona]
                                        ,[suprafata]
                                        ,[tip]
                                        ,[anTerminare]
                                        ,[destinatie])
                                VALUES  (@unitateId
                                        ,@gospodarieId
                                        ,@an
                                        ,@adresa
                                        ,@zona
                                        ,@suprafata
                                        ,@tip
                                        ,@anTerminare
                                        ,@destinatie)";

        vCmd.Parameters.AddWithValue("@unitateId",   building.UnitateId);
        vCmd.Parameters.AddWithValue("@gospodarieId",building.IdGospodarie);
        vCmd.Parameters.AddWithValue("@an",          building.An);
        vCmd.Parameters.AddWithValue("@adresa",      building.Adresa);
        vCmd.Parameters.AddWithValue("@zona",        building.Zona);
        vCmd.Parameters.AddWithValue("@suprafata",   building.Suprafata);
        vCmd.Parameters.AddWithValue("@tip",         building.Tip);
        vCmd.Parameters.AddWithValue("@anTerminare", building.AnTerminare);
        vCmd.Parameters.AddWithValue("@destinatie", building.Destinatie);

        try
        {
            vCmd.ExecuteNonQuery();
            vCon.Close();

            return true;
        }
        catch
        {
            vCon.Close();

            return false;
        }
    }

    internal static bool RemoveBuilding(long id)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(ConnectionYear);
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;

        vCmd.CommandText = @"DELETE FROM [capitol11]
                             WHERE  [id] = @id";

        vCmd.Parameters.AddWithValue("@id", id);
       
        try
        {
            vCmd.ExecuteNonQuery();
            vCon.Close();

            return true;
        }
        catch
        {
            vCon.Close();

            return false;
        }
    }

    internal static bool UpdateBuilding(Building building)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(ConnectionYear);
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;

        vCmd.CommandText = @"UPDATE [capitol11]
                             SET
                                    [adresa]      = @adresa
                                   ,[zona]        = @zona
                                   ,[suprafata]   = @suprafata
                                   ,[tip]         = @tip
                                   ,[anTerminare] = @anTerminare
                                   ,[destinatie]  = @destinatie
                             WHERE  [id]          = @id";
            
        vCmd.Parameters.AddWithValue("@id", building.Id);
        vCmd.Parameters.AddWithValue("@adresa", building.Adresa);
        vCmd.Parameters.AddWithValue("@zona", building.Zona);
        vCmd.Parameters.AddWithValue("@suprafata", building.Suprafata);
        vCmd.Parameters.AddWithValue("@tip", building.Tip);
        vCmd.Parameters.AddWithValue("@anTerminare", building.AnTerminare);
        vCmd.Parameters.AddWithValue("@destinatie", building.Destinatie);

        try
        {
            vCmd.ExecuteNonQuery();
            vCon.Close();

            return true;
        }
        catch
        {
            vCon.Close();

            return false;
        }
    }

    internal static Building GetBuildingById(Int64 buildingId)
    {
        throw new NotImplementedException("Doar nu te asteptai sa chiar returneze cladirea dupa vreun id...");
    }

    internal static Dictionary<long, Building> GetBuildingsByIDGospodarie(Int64 idGospodarie)
    {
        if(idGospodarie == 0)
        {
            return new Dictionary<long, Building>();
        }

        Dictionary<long, Building> buildings = new Dictionary<long, Building>();
        Building building;

        SqlConnection vCon = ManipuleazaBD.CreareConexiune(ConnectionYear);
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;

        vCmd.CommandText = @"SELECT  [id]
                                    ,[unitateId]
                                    ,[an]
                                    ,[adresa]
                                    ,[zona]
                                    ,[suprafata]
                                    ,[tip]
                                    ,[destinatie]
                                    ,[anTerminare]
                               FROM  [capitol11]  
                               WHERE [gospodarieId] = @idGospodarie";

        vCmd.Parameters.AddWithValue("@idGospodarie", idGospodarie);

        SqlDataReader buildingDataReader = vCmd.ExecuteReader();

        while (buildingDataReader.Read())
        {
            building = new Building();

            building.Id =  Convert.ToInt64(buildingDataReader["id"].ToString());
            building.UnitateId = Convert.ToInt16(buildingDataReader["unitateId"].ToString());
            building.IdGospodarie = idGospodarie;
            building.An = buildingDataReader["an"] != DBNull.Value ? Convert.ToInt16(buildingDataReader["an"].ToString()) : 0;
            building.Adresa = buildingDataReader["adresa"] != DBNull.Value ? buildingDataReader["adresa"].ToString() : string.Empty;
            building.Zona = buildingDataReader["zona"] != DBNull.Value ? buildingDataReader["zona"].ToString() : string.Empty;
            building.Suprafata = buildingDataReader["suprafata"] != DBNull.Value ? Convert.ToDecimal(buildingDataReader["suprafata"]) : 0;
            building.Tip = buildingDataReader["tip"] != DBNull.Value ? buildingDataReader["tip"].ToString() : string.Empty;
            building.Destinatie = buildingDataReader["destinatie"] != DBNull.Value ? buildingDataReader["destinatie"].ToString() : string.Empty;
            building.AnTerminare = buildingDataReader["anTerminare"] != DBNull.Value ? Convert.ToInt16(buildingDataReader["anTerminare"].ToString()) : 0;

            buildings.Add(building.Id, building);
        }

        buildingDataReader.Close();
        ManipuleazaBD.InchideConexiune(vCon);

        return buildings;
    }

    internal static List<Building> GetMissingBuildingsByUInitate(int unitateId)
    {
        if (unitateId == 0)
        {
            return new List<Building>();
        }

        List<Building> buildings = new List<Building>();
        Building building;

        SqlConnection vCon = ManipuleazaBD.CreareConexiune(ConnectionYear);
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;

        vCmd.CommandText = @"SELECT  ( select gospodarieid  from gospodarii where gospodarieidInitial = cap.gospodarieid and an = 2015) as [gospodarieIdBun]
                                     ,2015 as an
                                     ,'' as adresa
                                     ,'' as zona
                                     ,capitolId
                                     ,cap.[col1] as suprafata
                                     ,0 as tip
                                     ,denumire1 as destinatie
                                     ,0 as anTerminare
                              FROM [TNTRegistruAgricol3].[tntcomputers].[capitole] cap
                              INNER JOIN sabloanecapitole on cap.codRand = sabloaneCapitole.codRand and cap.codCapitol = sabloaneCapitole.capitol and cap.an = sabloaneCapitole.an
                              WHERE cap.an = 2014 
                                   and codcapitol = '11' 
                                   and cap.codrand  in (13,14,15,16,17,18,19) 
                                and col1 > 0 
                                and sabloaneCapitole.capitol = '11'
                                and unitateid = @unitateId
                        ";

        vCmd.Parameters.AddWithValue("@unitateId", unitateId);

        SqlDataReader buildingDataReader = vCmd.ExecuteReader();

        while (buildingDataReader.Read())
        {
            building = new Building();

            building.UnitateId = unitateId;
            building.IdGospodarie = Convert.ToInt32(buildingDataReader["gospodarieIdBun"].ToString());
            building.An = buildingDataReader["an"] != DBNull.Value ? Convert.ToInt16(buildingDataReader["an"].ToString()) : 0;
            building.Adresa = string.Empty;
            building.Zona = string.Empty;
            building.Suprafata = buildingDataReader["suprafata"] != DBNull.Value ? Convert.ToDecimal(buildingDataReader["suprafata"]) : 0;
            building.Tip = buildingDataReader["tip"] != DBNull.Value ? buildingDataReader["tip"].ToString() : string.Empty;
            building.Destinatie = buildingDataReader["destinatie"] != DBNull.Value ? buildingDataReader["destinatie"].ToString() : string.Empty;
            building.AnTerminare = 0;
            building.Id = buildingDataReader["capitolId"] != DBNull.Value ? Convert.ToInt64(buildingDataReader["capitolId"]) : 0;

            buildings.Add(building);
        }

        buildingDataReader.Close();
        ManipuleazaBD.InchideConexiune(vCon);

        return buildings;
    }

    internal static bool AddMissingBuildings(List<Building> buildings)
    {
        if (buildings.Count == 0)
        {
            return false;
        }

        SqlConnection vCon = ManipuleazaBD.CreareConexiune(ConnectionYear);
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;

        foreach ( Building building in buildings )
        {
            vCmd.CommandText += @"INSERT INTO capitol11
                                        ([unitateId]
                                        ,[gospodarieId]
                                        ,[an]
                                        ,[adresa]
                                        ,[zona]
                                        ,[capitolid]
                                        ,[suprafata]
                                        ,[tip]
                                        ,[anTerminare]
                                        ,[destinatie])
                                  VALUES ("   + building.UnitateId +
                                        ","   + building.IdGospodarie +
                                        ","   + building.An +
                                        ",'"  + building.Adresa +
                                        "','" + building.Zona +
                                        "','" + building.Id +
                                        "','" + building.Suprafata.ToString().Replace(",", ".") +
                                        "','" + building.Tip +
                                        "',"  + building.AnTerminare +
                                        ",'"  + building.Destinatie +
                                        "');";
        }
        try
        {
            vCmd.ExecuteNonQuery();
            vCon.Close();

            return true;
        }
        catch
        {
            vCon.Close();

            return false;
        }
    }
}