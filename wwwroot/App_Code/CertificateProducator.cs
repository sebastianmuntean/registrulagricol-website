﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CertificateProducator
/// </summary>
public class CertificateProducator
{
	public CertificateProducator()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static DataTable GetCertificate(int unitateId, int gospodarieId, int an)
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(an));
        string select = @"select certProd.certProdId,  convert(varchar(20),nrAviz) + '/' + convert(varchar(20), dataAviz, 104) as nrDataAviz,
                        produsCpDenumire as 'produse',
                         convert(varchar(20),certificatProducatorSerie) + '/' + convert(varchar(20),certificatProducatorNr) + '/' + convert(varchar(20), certproddata, 104) as 'serieNrCertificat',
                         convert(varchar(20),nrAtestat) + '/' +convert(varchar(20), dataAtestat, 104) as 'nrAtestat'
from certProd
join produsCp
on certProd.certProdId = produsCp.certProdId
where certProd.unitateId = " + unitateId + " and certProd.gospodarieid = " + gospodarieId + " and certProd.an = " + an + "";
        SqlDataAdapter adapter = new SqlDataAdapter(select, connection);
        DataTable table = new DataTable();
        adapter.Fill(table);
        ManipuleazaBD.InchideConexiune(connection);
        return table;
    }

    public static DataTable GetAtestate(int unitateId, int gospodarieId, int an)
    {
        string select = @"SELECT certProd.certProdId,  
                                 CONVERT(VARCHAR(20),nrAviz) + '/' + CONVERT(VARCHAR(20), dataAviz, 104) as nrDataCerere,
                                 CONVERT(VARCHAR(20), serieAtestat) +' / '+ CONVERT(VARCHAR(20),nrAtestat) + '/' + CONVERT(VARCHAR(20), dataAtestat, 104) as 'nrAtestat'
                            FROM certProd
                           WHERE certProd.unitateId = " + unitateId + " AND certProd.gospodarieid = " + gospodarieId + " AND certProd.an = " + an + " AND nrAtestat <> ''"; //CONVERT(VARCHAR(20),certificatProducatorSerie) + '/' + CONVERT(VARCHAR(20),certificatProducatorNr) + '/' + CONVERT(VARCHAR(20), certproddata, 104) as 'serieNrCertificat', produsCpDenumire as 'produse',


        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(an));
        SqlDataAdapter adapter = new SqlDataAdapter(select, connection);
        DataTable table = new DataTable();

        adapter.Fill(table);

        ManipuleazaBD.InchideConexiune(connection);

        return table;
    }

    public static DataTable GetCertificateForPrintTip0(int certificatId, short an)
    {
        string select = @"SELECT certProd.certProdId  
                                ,CONVERT(VARCHAR(20),nrAviz) + '/' + CONVERT(VARCHAR(20), dataAviz, 104) as nrDataCerere
                                ,'Serie ' + CONVERT(VARCHAR(20), serieAtestat) +' Număr '+ CONVERT(VARCHAR(20),nrAtestat) + ' Data ' + CONVERT(VARCHAR(20), dataAtestat, 104) as 'nrAtestat'
	                            ,produsCpDenumire
                                ,produsCpSuprafataAri
                                ,produsCpSuprafataHa
                            FROM certProd 
	                            INNER JOIN produsCp on produsCp.certProdId = certProd.certProdId
                            WHERE certProd.certProdId = " + certificatId + " and produsCpTip = 0";


        SqlConnection connection = ManipuleazaBD.CreareConexiune(an);
        SqlDataAdapter adapter = new SqlDataAdapter(select, connection);
        DataTable table = new DataTable();

        adapter.Fill(table);

        ManipuleazaBD.InchideConexiune(connection);

        return table;
    }

    public static DataTable GetCertificateForPrintTip1(int certificatId, short an)
    {
        string select = @"SELECT certProd.certProdId  
	                            ,produsCpDenumire
                                ,produsCpNr
                            FROM certProd 
	                            INNER JOIN produsCp on produsCp.certProdId = certProd.certProdId
                            WHERE certProd.certProdId = " + certificatId + " and produsCpTip = 1";


        SqlConnection connection = ManipuleazaBD.CreareConexiune(an);
        SqlDataAdapter adapter = new SqlDataAdapter(select, connection);
        DataTable table = new DataTable();

        adapter.Fill(table);

        ManipuleazaBD.InchideConexiune(connection);

        return table;
    }

    public static DataTable GetAtestateForAnexa3(int unitateId, short an)
    {
        string select = @"SELECT	convert(nvarchar, gospodarii.membruNume) + ', '+ convert(nvarchar,gospodarii.membruCnp) +', ' +
		                            CASE strainas WHEN 1 THEN 'Str. '+ convert(nvarchar,sStrada) + ' Nr.' + convert(nvarchar,SNr) + ' Bl. ' + convert(nvarchar,sBl) + ' Sc. ' + convert(nvarchar,SSc) + ' Et. ' +                                        convert(nvarchar,sEtj) + ' Ap. ' + convert(nvarchar,sAp) ELSE 'Str. '+ convert(nvarchar,strada) + ' Nr.' + convert(nvarchar,nr) + ' Bl. ' + convert(nvarchar,bl) + ' Sc. ' +                                         convert(nvarchar,sc) + ' Et. ' + convert(nvarchar,et) + ' Ap. ' + convert(nvarchar,ap) END AS adresa
		                            ,CONVERT(VARCHAR(20),nrAviz) + '/' + CONVERT(VARCHAR(20), dataAviz, 104) as nrDataCerere
		                            ,produsCpDenumire
		                            ,case produscptip WHEN 0 THEN convert(nvarchar, produsCpSuprafataAri) ELSE '-' end as produsCpSuprafataAri
		                            ,case produscptip WHEN 0 THEN convert(nvarchar, produsCpSuprafataHa) ELSE '-' end as produsCpSuprafataHa
		                            ,case produscptip WHEN 1 THEN convert(nvarchar, produsCpNr) ELSE '-' end as produsCpNr 
		                            ,'Serie ' + CONVERT(VARCHAR(20), serieAtestat) +' Nr. '+ CONVERT(VARCHAR(20),nrAtestat) + ' Data ' + CONVERT(VARCHAR(20), dataAtestat, 104) as 'nrAtestat'
		                            ,'' as observatie
                        FROM certProd 
		                            INNER JOIN produsCp on produsCp.certProdId = certProd.certProdId
		                            inner join gospodarii on gospodarii.gospodarieId = certProd.gospodarieId
                        WHERE produsCp.unitateId = " + unitateId;

        SqlConnection connection = ManipuleazaBD.CreareConexiune(an);
        SqlDataAdapter adapter = new SqlDataAdapter(select, connection);
        DataTable table = new DataTable();

        adapter.Fill(table);

        ManipuleazaBD.InchideConexiune(connection);

        return table;
    }
}