﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

/// <summary>
/// AddressServices
/// </summary>
public class AddressServices
{
    public static short ConnectionYear = 0;

	public AddressServices()
	{
	
	}

    public static List<Address> GetAddressByGospodarieId(Int32 gospodarieId)
    {
        List<Address> adrese = new List<Address>();


        SqlConnection vCon = ManipuleazaBD.CreareConexiune(ConnectionYear);
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;

        vCmd.CommandText = @"SELECT
                                   Adrese.id
                                  ,tipAdresa
                                  ,denumire
                                  ,judet
                                  ,localitate
                                  ,codPostal
                                  ,strada
                                  ,numar
                                  ,bloc
                                  ,apartament
                                  ,etaj
                                  ,scara
                                  ,observatii
                                  ,an
                                  ,gospodarieIdInitial
                            FROM AdreseFirme 
                            INNER JOIN Adrese ON AdreseFirme.adresaId = Adrese.id  
                            WHERE AdreseFirme.gospodarieId = '" + gospodarieId + "'";

        SqlDataReader result = vCmd.ExecuteReader();

        while (result.Read())
        {
            Address address = new Address();

            address.AddresType = (Address.AddressType)Enum.Parse(typeof(Address.AddressType), result["tipAdresa"].ToString());
            address.Denumire = result["denumire"].ToString();
            address.Apartament = result["apartament"].ToString();
            address.Bloc = result["bloc"].ToString();
            address.CodPostal = result["codPostal"].ToString();
            address.Etaj = result["etaj"].ToString();
            address.Judet =  result["judet"].ToString();
            address.Localitate = result["localitate"].ToString();
            address.Numar = result["numar"].ToString();
            address.Strada = result["strada"].ToString();
            address.Scara = result["scara"].ToString();
            address.Observatii = result["observatii"].ToString();
            address.Id = Convert.ToUInt64(result["id"]);
            address.An = result["an"].ToString();
            address.GospodarieIdInitial = Convert.ToInt32(result["gospodarieIdInitial"]);

            adrese.Add(address);
        }

        result.Close();
        ManipuleazaBD.InchideConexiune(vCon);

        return adrese;
    }

    public static bool Update(Address address)
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(address.An));
        SqlCommand command = new SqlCommand();
        command.Connection = connection;
        SqlTransaction transaction = connection.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);
        command.Transaction = transaction;

        Update(address, command);
        UpdateDetails(address, command);

        try
        {
            transaction.Commit();
            return true;
        }
        catch
        {
            transaction.Rollback();
            return false;
        }
        finally
        {
            connection.Close();
        }
    }
 
    private static void UpdateDetails(Address address, SqlCommand command)
    {
        command.CommandText = @"UPDATE AdreseFirme
                                SET      denumire = '" + address.Denumire +
                                     "', observatii =  '" + address.Observatii +
                                 "' WHERE adresaId = '" + address.Id + "';";

        command.ExecuteNonQuery();
    }

    private static void Update(Address address, SqlCommand command)
    {
        command.CommandText = @"UPDATE Adrese
                                SET     judet = '" + address.Judet +
                                    "', localitate = '" + address.Localitate +
                                    "', codPostal =  '" + address.CodPostal +
                                    "', strada =  '" + address.Strada + 
                                    "', numar = '" + address.Numar + 
                                    "', bloc =  '" + address.Bloc + 
                                    "', apartament =  '" + address.Apartament + 
                                    "', etaj =  '" + address.Etaj + 
                                    "', scara =  '" + address.Scara +
                                     "', tipAdresa =  '" + (int)address.AddresType +
                                "' WHERE id = '" + address.Id + "';";

        command.ExecuteNonQuery();
    }

    public static bool Save(Address adresa)
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(adresa.An));
        SqlCommand command = new SqlCommand();
        command.Connection = connection;
        SqlTransaction transaction = connection.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);
        command.Transaction = transaction;

        Save(adresa, command);
        RelateGospodarieWithAddress(adresa, command);

        try
        {
            transaction.Commit();
            return true;
        }
        catch
        {
            transaction.Rollback();
            return false;
        }
        finally
        {
            connection.Close();
        }
    }

    public static void Save(Address address, SqlCommand command)
    {
        command.CommandText = @"INSERT INTO Adrese
                               (tipAdresa
                               ,judet
                               ,localitate
                               ,codPostal
                               ,strada
                               ,numar
                               ,bloc
                               ,apartament
                               ,etaj
                               ,scara)
                                VALUES(" +
                                (int)address.AddresType + ", '" +
                                address.Judet + "', '" +
                                address.Localitate + "', '" +
                                address.CodPostal + "', '" +
                                address.Strada + "', '" +
                                address.Numar + "', '" +
                                address.Bloc + "', '" +
                                address.Apartament + "', '" +
                                address.Etaj + "', '" +
                                address.Scara  +
                              "');" +
                              "SELECT SCOPE_IDENTITY();";

        address.Id = Convert.ToUInt64(command.ExecuteScalar().ToString());
    }

    public static void RelateGospodarieWithAddress(Address adresa, SqlCommand command)
    {
        command.CommandText = @"INSERT INTO AdreseFirme
                                (
                                 adresaId
                                ,gospodarieId
                                ,gospodarieIdInitial
                                ,an
                                ,denumire
                                ,observatii
                                )
                                VALUES(" +
                                adresa.Id + "," +
                                adresa.GospodarieId + "," +
                                adresa.GospodarieIdInitial + ",'" +
                                adresa.An + "','" +
                                adresa.Denumire + "','" +
                                adresa.Observatii + "')";

        command.ExecuteNonQuery();
    }

    public static DataTable Counties
    {
        get
        {
            SqlConnection vCon = ManipuleazaBD.CreareConexiune(ConnectionYear);

            SqlDataAdapter dataAdapter = new SqlDataAdapter(
                @"SELECT judetDenumire FROM judete ",
                vCon);

            DataTable judeteDataTable = new DataTable();
            dataAdapter.Fill(judeteDataTable);

            vCon.Close();

            return judeteDataTable;
        }
    }

    public static DataTable GetCities(string county)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(ConnectionYear);

        SqlDataAdapter dataAdapter = new SqlDataAdapter(
            @"SELECT localitateDenumire FROM Localitati WHERE judetDenumire = '" + county + "'",
            vCon);

        DataTable localitatiDataTable = new DataTable();
        dataAdapter.Fill(localitatiDataTable);

        vCon.Close();

        return localitatiDataTable;
    }

    public static bool DeleteAddress(ulong id)
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(ConnectionYear);
        SqlCommand command = new SqlCommand();
        command.Connection = connection;

        command.CommandText = @"DELETE FROM AdreseFirme
                                WHERE adresaId = " + id + ";";

        command.CommandText += @"DELETE FROM Adrese
                                 WHERE Adrese.Id = " + id; 
 
        try
        {
            command.ExecuteNonQuery();

            return true;
        }
        catch
        {
            return false;
        }
        finally
        {
            connection.Close();
        }
    }

    internal static Address GetAddressById(Address address)
    {
        throw new NotImplementedException();
    }
}