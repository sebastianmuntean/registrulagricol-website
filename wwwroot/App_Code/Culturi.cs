﻿using System;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Summary description for Culturi
/// </summary>
[Serializable]
public abstract class Culturi
{
    int unitateId = -1;
    public int UnitateId
    {
        get
        {
            return this.unitateId;
        }
        set
        {
            this.unitateId = value;
        }
    }

    long idGospodarie = 0;
    public long IdGospodarie
    {
        get
        {
            return this.idGospodarie;
        }
        set
        {
            this.idGospodarie = value;
        }
    }

    long id = 0;
    public long Id
    {
        get
        {
            return this.id;
        }
        set
        {
            this.id = value;
        }
    }

    long parcelaId = 0;
    public long ParcelaId
    {
        get
        {
            return this.parcelaId;
        }
        set
        {
            this.parcelaId = value;
        }
    }

    int an = 0;
    public int An
    {
        get
        {
            return this.an;
        }
        set
        {
            this.an = value;
        }
    }

    private string codRand = string.Empty;
    public string CodRand
    {
        get
        {
            return this.codRand;
        }
        set
        {
            this.codRand = value;
        }
    }

    private string denumire = string.Empty;
    public string Denumire
    {
        get
        {
            return this.denumire;
        }
        set
        {
            this.denumire = value;
        }
    }

    public Capitole Capitol
    {
        get;
        set;
    }

    ISuprafete suprafataCultivata;
    public ISuprafete SuprafataCultivata
    {
        get
        {
            return this.suprafataCultivata;
        }
        set
        {
            this.suprafataCultivata = value;
        }
    }

    public abstract List<Culturi> GetCulturi();

    public abstract bool UpdateCultura();

    public abstract bool DeleteCultura();

    public abstract bool AddCultura(Parcele parcela);
}