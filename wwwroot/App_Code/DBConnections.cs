﻿using System;
using System.Configuration;
using System.Linq;

/// <summary>
/// Summary description for DBConnections
/// </summary>
public class DBConnections
{
    private readonly short an=0;

	public DBConnections(short an)
	{
        this.an = an;
	}

    public string Create()
    {
        if(an<2016)
        {
            return ConfigurationManager.ConnectionStrings["TNTRegistruAgricolConnectionString"].ConnectionString;
        }
        else
        {
            return ConfigurationManager.ConnectionStrings["TNTRegistruAgricolConnectionString_"+an].ConnectionString;
        }
    }
}