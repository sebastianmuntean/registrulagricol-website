﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;

/// <summary>
/// Summary description for ContracteServices
/// </summary>
public class ContracteServices
{
    public static short ConnectionYear;

    public static void DezactiveazaContracteExpirate(string gospodarieId)
    {
        string conString = ConfigurationManager.ConnectionStrings["TNTRegistruAgricolConnectionString"].ConnectionString;
        SqlConnection connection = new SqlConnection(conString);
        string update = "UPDATE [parceleAtribuiriContracte] SET contractActiv = 0 WHERE (contractGospodarieIdDa = " + gospodarieId + " or contractGospodarieIdPrimeste = " + gospodarieId + ") AND contractDataFinal < CONVERT(datetime,'01.01." + DateTime.Now.Year + "', 104) AND contractActiv = 1";
        using (connection)
        {
            connection.Open();
            SqlCommand cmd = new SqlCommand(update, connection);
            cmd.ExecuteNonQuery();
        }
    }


    internal static Contracte GetLastActiveContractByIdParcela(long idParcela)
    {
        Contracte contract = new Contracte();

        if (idParcela == 0)
        {
            return new Contracte();
        }

        SqlConnection vCon = ManipuleazaBD.CreareConexiune(ConnectionYear);
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;

        vCmd.CommandText = @"SELECT 
                                     vanzator.membruNume as vanzator
	                                ,cumparator.membruNume +' (' + cumparator.volum + '/' + cumparator.nrPozitie + ')' as cumparator 
                                    ,contractTip
                                    ,contractDataSemnarii
                                    ,contractNr
	                                ,c3ha
	                                ,c3ari
                            FROM    parceleAtribuiriContracte 
	                                INNER JOIN parcelemodutilizare ON parceleModUtilizare.contractId = parceleAtribuiriContracte.contractId AND parceleAtribuiriContracte.contractActiv = 1
	                                INNER JOIN gospodarii as vanzator on vanzator.gospodarieId = contractGospodarieIdDa 
	                                INNER JOIN gospodarii as cumparator on cumparator.gospodarieId = contractGospodarieIdPrimeste
                            WHERE   parcelaId = @idParcela and contractActiv = 1 AND
	                                parcelemodutilizare.contractId = (SELECT MAX(parcelemodutilizare.contractId) FROM  parceleModUtilizare)
                            ";

        vCmd.Parameters.AddWithValue("@idParcela", idParcela);

        SqlDataReader contractDataReader = vCmd.ExecuteReader();
      
        contract.Suprafata = new Suprafete();
        
        if ( contractDataReader.HasRows )
        {
            while ( contractDataReader.Read() )
            {
                contract.IdParcela = idParcela;
                contract.Cumparator = contractDataReader["cumparator"].ToString();
                contract.Vanzator = contractDataReader["vanzator"].ToString();
                contract.TipContract = (Contracte.TipuriContracte)Enum.Parse(typeof( Contracte.TipuriContracte ), contractDataReader["contractTip"].ToString());
                contract.DataSemnarii = Convert.ToDateTime(contractDataReader["contractDataSemnarii"].ToString());
                contract.Suprafata.Ari = Convert.ToDecimal(contractDataReader["c3ari"]);
                contract.Suprafata.Hectare = Convert.ToDecimal(contractDataReader["c3ha"]);
                contract.Numar = contractDataReader["contractNr"].ToString();
            }

            contractDataReader.Close();
            ManipuleazaBD.InchideConexiune(vCon);
        }
        else
        {
            contract = GetActiveContractByIdParcela(idParcela);
        }

        return contract;
    }

    private static Contracte GetActiveContractByIdParcela(long idParcela)
    {
        SqlDataReader contractDataReader;
        SqlConnection vCon;
        
        ReadContracte(idParcela, out contractDataReader, out vCon);
      
        Contracte contract = new Contracte();
        
        contract.Suprafata = new Suprafete();

        if (contractDataReader.HasRows)
        {
            while ( contractDataReader.Read() )
            {
                IncarcaContract(contract, idParcela, contractDataReader);
            }

            contractDataReader.Close();
            ManipuleazaBD.InchideConexiune(vCon);
        }
        return contract;
    }
 
    private static void IncarcaContract(Contracte contract, long idParcela, SqlDataReader contractDataReader)
    {
        contract.IdParcela = idParcela;
        contract.Cumparator = contractDataReader["cumparator"].ToString();
        contract.Vanzator = contractDataReader["vanzator"].ToString();
        contract.TipContract = (Contracte.TipuriContracte)Enum.Parse(typeof( Contracte.TipuriContracte ), contractDataReader["contractTip"].ToString());
        contract.DataSemnarii = Convert.ToDateTime(contractDataReader["contractDataSemnarii"].ToString());
        contract.Suprafata.Ari = Convert.ToDecimal(contractDataReader["c3ari"]);
        contract.Suprafata.Hectare = Convert.ToDecimal(contractDataReader["c3ha"]);
        contract.Numar = contractDataReader["contractNr"].ToString();
    }
 
    private static void ReadContracte(long idParcela, out SqlDataReader contractDataReader, out SqlConnection vCon)
    {
        vCon = ManipuleazaBD.CreareConexiune(ConnectionYear);
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;

        vCmd.CommandText = @"SELECT 
                                     vanzator.membruNume as vanzator
	                                ,cumparator.membruNume +' (' + cumparator.volum + '/' + cumparator.nrPozitie + ')' as cumparator 
                                    ,contractTip
                                    ,contractDataSemnarii
                                    ,contractNr
	                                ,c3ha
	                                ,c3ari
                            FROM    parceleAtribuiriContracte 
	                                INNER JOIN parcelemodutilizare on parcelemodutilizare.contractid = parceleatribuiricontracte.contractid
	                                INNER JOIN gospodarii as vanzator on vanzator.gospodarieId = contractGospodarieIdDa 
	                                INNER JOIN gospodarii as cumparator on cumparator.gospodarieId = contractGospodarieIdPrimeste
                            WHERE   parcelaId = @idParcela and contractActiv = 1
                            ";

        vCmd.Parameters.AddWithValue("@idParcela", idParcela);

        contractDataReader = vCmd.ExecuteReader();
    }

    internal static List<Contracte> GetAllActiveContractsByIdParcela(long idParcela)
    {
        if ( idParcela == 0 )
        {
            return new List<Contracte>();
        }

        SqlDataReader contractDataReader;
        SqlConnection vCon;
        List<Contracte> contracte = new List<Contracte>();

        Contracte contract;

        ReadContracte(idParcela, out contractDataReader, out vCon);

        if ( contractDataReader.HasRows )
        {
            while ( contractDataReader.Read() )
            {
                contract = new Contracte();
                contract.Suprafata = new Suprafete();

                IncarcaContract(contract, idParcela, contractDataReader);

                contracte.Add(contract);
            }

            contractDataReader.Close();
            ManipuleazaBD.InchideConexiune(vCon);
        }
        return contracte;
    }

	 public static List<Tuple<long, long>> GetGospodarieIdPrimeste()
        {
            List<Tuple<Int64, Int64>> idsContracteParcele = new List<Tuple<Int64, Int64>>();
            Contracte contractParcele;
            SqlCommand vCmd = new SqlCommand();

            vCmd.CommandText = @"SELECT    [contractId]
                                          ,[contractGospodarieIdPrimeste]
                                    FROM   [parceleAtribuiriContracte]
                                    WHERE  unitateId = @unitateId
                                ";

            vCmd.Parameters.AddWithValue("@unitateId", 26);

            SqlConnection connection = ManipuleazaBD.CreareConexiune(ConnectionYear);
            vCmd.Connection = connection;

            SqlDataReader result = vCmd.ExecuteReader();

            while (result.Read())
            {
                contractParcele = new Contracte();

                contractParcele.Id = Convert.ToInt64(result["contractId"]);
                contractParcele.IdDa = Convert.ToInt32(result["contractGospodarieIdDa"]);

                Tuple<Int64, Int64> ids = new Tuple<long, long>(contractParcele.Id, contractParcele.IdDa);

                idsContracteParcele.Add(ids);
            }

            result.Close();
            ManipuleazaBD.InchideConexiune(connection);

            return idsContracteParcele;
        }

        internal static List<Tuple<Int64, Int64>> GetGospodarieIdDa()
        {
            List<Tuple<Int64, Int64>> idsContracteParcele = new List<Tuple<Int64, Int64>>();
            Contracte contractParcele;
            SqlCommand vCmd = new SqlCommand();

            vCmd.CommandText = @"SELECT    [contractId]
                                          ,[contractGospodarieIdDa]
                                    FROM   [parceleAtribuiriContracte]
                                    WHERE  unitateId = @unitateId
                                ";

            vCmd.Parameters.AddWithValue("@unitateId", 26);

            SqlConnection connection = ManipuleazaBD.CreareConexiune(ConnectionYear);
            vCmd.Connection = connection;

            SqlDataReader result = vCmd.ExecuteReader();

            while (result.Read())
            {
                contractParcele = new Contracte();

                contractParcele.Id = Convert.ToInt64(result["contractId"]);
                contractParcele.IdDa = Convert.ToInt32(result["contractGospodarieIdDa"]);

                Tuple<Int64, Int64> ids = new Tuple<long, long>(contractParcele.Id, contractParcele.IdDa);

                idsContracteParcele.Add(ids);
            }

            result.Close();
            ManipuleazaBD.InchideConexiune(connection);

            return idsContracteParcele;
        }

        internal static bool SchimbaIdDaCuIdInitial(List<Tuple<long, long>> idDaListCuIdInitial)
        {

            SqlCommand command = new SqlCommand();

            foreach ( Tuple<long, long> ids in idDaListCuIdInitial )
            {
                if ( ids.Item2 != 0 )
                {
                    string commandText = @" UPDATE parceleAtribuiriContracte
                                            SET contractGospodarieIdDa = " + ids.Item2 +
                                         " WHERE contractID = " + ids.Item1 + ";";

                    command.CommandText += commandText;
                }
            }

            SqlConnection connection = ManipuleazaBD.CreareConexiune(ConnectionYear);
            command.Connection = connection;

            try
            {
            //    command.ExecuteNonQuery();

                return true;
            }
            catch
            {
                return false;
            }
            finally
            {
                connection.Close();
            }
           
        }

        internal static bool SchimbaIdPrimesteCuIdInitial(List<Tuple<long, long>> idListCuIdInitial)
        {

            SqlCommand command = new SqlCommand();

            foreach (Tuple<long, long> ids in idListCuIdInitial)
            {
                if (ids.Item2 != 0)
                {
                    string commandText = @" UPDATE parceleAtribuiriContracte
                                            SET contractGospodarieIdPrimeste = " + ids.Item2 +
                                         " WHERE contractID = " + ids.Item1 + ";";

                    command.CommandText += commandText;
                }
            }

            SqlConnection connection = ManipuleazaBD.CreareConexiune(ConnectionYear);
            command.Connection = connection;

            try
            {
                //    command.ExecuteNonQuery();

                return true;
            }
            catch
            {
                return false;
            }
            finally
            {
                connection.Close();
            }

        }
}