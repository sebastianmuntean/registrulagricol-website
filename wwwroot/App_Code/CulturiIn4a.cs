﻿using System;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Summary description for CulturiIn4a
/// </summary>

[Serializable]
public class CulturiIn4a : Culturi
{
	public CulturiIn4a()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public override List<Culturi> GetCulturi()
    {
        return CulturiServices.GetCulturiInParcela(this);
    }

    public override bool AddCultura(Parcele parcela)
    {
        return CulturiServices.AdaugaInParceleCatreCapitole(parcela);
    }

    public override bool DeleteCultura()
    {
        return CulturiServices.StergeCultura(this);
    }

    public override bool UpdateCultura()
    {
        return CulturiServices.UpdateCulturi(this);
    }

}