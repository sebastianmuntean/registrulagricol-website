﻿using System;
using System.Linq;

/// <summary>
/// Summary description for Suprafete
/// </summary>
[Serializable]

public class Suprafete:ISuprafete, IComparable
{
	public Suprafete()
	{
	}

    private decimal hectare = 0;
    public decimal Hectare
    {
        get
        {

            //if (hectare < 1)
            //{
            //    ari = hectare * 100;
            //    hectare = 0;
            //}

            return hectare;
        }
        set
        {
            hectare = value;

            //if(hectare < 1)
            //{
            //    ari = hectare * 100;
            //    hectare = 0;
            //}
        }
    }

    private decimal ari = 0;
    public decimal Ari
    {
        get
        {
            //if (ari < 0)
            //{
            //    while (ari < 0)
            //    {
            //        hectare = hectare - 1;
            //        ari = ari + 100;
            //    }
            //}
            //else
            //{
            //    if (ari >= 100)
            //    {
            //        while (ari >= 100)
            //        {
            //            hectare = hectare + 1;
            //            ari = ari - 100;
            //        }
            //    }
            //}
            return ari;
        }
        set
        {
            ari = value;

            //if (ari < 0)
            //{
            //    while (ari < 0)
            //    {
            //        hectare = hectare - 1;
            //        ari = ari + 100;
            //    }
            //}
            //else
            //{
            //    if (ari >= 100)
            //    {
            //        while (ari >= 100)
            //        {
            //            hectare = hectare + 1;
            //            ari = ari - 100;
            //        }
            //    }
            //}
          
        }
    }

    public override string ToString()
    {
        return hectare.ToString("0") + " ha " + ari.ToString("0.0000") +" ari";
    }

    public string ToString(Utils.Marime marime)
    {
        switch(marime)
        {
            case Utils.Marime.ari:
                return Ari.ToString("0.0000") + " ari";
            case Utils.Marime.ha:
                return Hectare.ToString("0") + " ha";
            default:
                return string.Empty;
        }
    }

    public int CompareTo(object Suprafata)
    {
        decimal valoareSuprafataObiectStrain = ((Suprafete)Suprafata).Hectare * 100 + ((Suprafete)Suprafata).Ari;
        decimal valoareSuprafataObiectPropriu = Hectare * 100 + Ari;

        if ( valoareSuprafataObiectPropriu < valoareSuprafataObiectStrain )
        {
            return -1;
        }
        else
        {
            if ( valoareSuprafataObiectPropriu > valoareSuprafataObiectStrain )
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }

    public static bool operator ==(Suprafete s1, int integer)
    {
        return s1.Ari + s1.Hectare * 100 == integer * 100;
    }

    public static bool operator !=(Suprafete s1, int integer)
    {
        return s1.Ari + s1.Hectare * 100 != integer * 100;
    }

    public static Suprafete operator -(Suprafete s1, Suprafete s2)
    {
        Suprafete suprafata = new Suprafete();

        if ( s2.ari > s1.ari )
        {
            s1.ari += 100;
            s1.hectare -= 1;
        }
        suprafata.ari = s1.ari - s2.ari;
        suprafata.hectare = s1.hectare - s2.hectare;

        return suprafata;
    }

    public static Suprafete operator +(Suprafete s1, Suprafete s2)
    {

        Suprafete suprafata = new Suprafete();
     
        suprafata.ari = s1.ari + s2.ari;
        suprafata.hectare = s1.hectare + s2.hectare;

        while(suprafata.ari>100)
        {
            suprafata.hectare += 1;
            suprafata.ari -= 100;
        }

        return suprafata;
    }
}