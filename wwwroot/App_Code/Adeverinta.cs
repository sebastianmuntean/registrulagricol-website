﻿using System;
using System.Linq;
using System.Text;

public class Adeverinta
{
    private SablonAdeverinta sablonAdeverinta;
    public SablonAdeverinta SablonAdeverinta
    {
        get
        {
            return sablonAdeverinta;
        }
        set
        {
            sablonAdeverinta = value;
        }
    }

    private int id;
    public int Id
    {
        get { return id; }
        set { id = value; }
    }

    private int unitateId;
    public int UnitateId
    {
        get { return unitateId; }
        set { unitateId = value; }
    }

    private long gospodarieId;
    public long GospodarieId
    {
        get { return gospodarieId; }
        set { gospodarieId = value; }
    }

    private int utilizatorId;
    public int UtilizatorId
    {
        get { return utilizatorId; }
        set { utilizatorId = value; }
    }

    private string numeMembru = string.Empty;
    public string NumeMembru
    {
        get { return numeMembru; }
        set { numeMembru = value; }
    }

    private string numarChitanta = string.Empty;
    public string NumarChitanta
    {
        get { return numarChitanta; }
        set { numarChitanta = value; }
    }

    private string numarInregistrare = string.Empty;
    public string NumarInregistrare
    {
        get { return numarInregistrare; }
        set { numarInregistrare = value; }
    }

    private string numarIesire = string.Empty;
    public string NumarIesire
    {
        get { return numarIesire; }
        set { numarIesire = value; }
    }

    private DateTime data = DateTime.MinValue;
    public DateTime Data
    {
        get { return data; }
        set { data = value; }
    }

    private string antent = string.Empty;
    public string Antet
    {
        get { return antent; }
        set { antent = value; }
    }

    private string continut = string.Empty;
    public string Continut
    {
        get
        {
            return continut;
        }
        set
        {
            continut = value;
        }
    }

    private string textTiparit = string.Empty;
    public string TextTiparit
    {
        get { return textTiparit; }
        set { textTiparit = value; }
    }

    private string motiv = string.Empty;
    public string Motiv
    {
        get { return motiv; }
        set { motiv = value; }
    }

    private string subsol = string.Empty;
    public string Subsol
    {
        get { return subsol; }
        set { subsol = value; }
    }

    private int ora;
    public int Ora
    {
        get { return ora; }
        set { ora = value; }
    }

    private int minut;
    public int Minut
    {
        get { return minut; }
        set { minut = value; }
    }

    private int tipSablon;
    public int TipSablon
    {
        get { return tipSablon; }
        set { tipSablon = value; }
    }

	public Adeverinta(SablonAdeverinta sablonAdeverinta)
	{
        this.sablonAdeverinta = sablonAdeverinta;
	}

    public Adeverinta()
    {

    }
}