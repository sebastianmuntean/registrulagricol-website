﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Gospodarie;

public class GospodariiServicesPentruReNumerotare
{
    //    public static int GetIdsByIdForIdInitial(long id)
    //    {
    //        Gospodarii gospodarie = new Gospodarii();
    //        SqlCommand vCmd = new SqlCommand();

    //        vCmd.CommandText = @"SELECT  gospodarieIdInitial
    //                                    FROM gospodarii
    //                                    WHERE gospodarieId = @gospodarieId
    //                             ";

    //        vCmd.Parameters.AddWithValue("@gospodarieId", id);

    //        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
    //        vCmd.Connection = connection;

    //        SqlDataReader result = vCmd.ExecuteReader();

    //        if ( result.Read() )
    //        {
    //            gospodarie.IdInitial = Convert.ToInt32(result["gospodarieIdInitial"]);
    //        }

    //        result.Close();
    //        ManipuleazaBD.InchideConexiune(connection);

    //        return gospodarie.IdInitial;
    //    }

    //    public static List<Tuple<Int64, Int64>> GetIdsByUnitate(int idUnitate)
    //    {
    //        List<Tuple<Int64, Int64>> idsList = new List<Tuple<long, long>>();
    //        Gospodarii gospodarie = new Gospodarii();
    //        SqlCommand vCmd = new SqlCommand();

    //        vCmd.CommandText = @"SELECT  gospodarieId
    //                                        ,gospodarieIdInitial
    //                                    FROM gospodarii
    //                                    WHERE unitateId = @unitateId AND an = 2016
    //                             ";

    //        vCmd.Parameters.AddWithValue("@unitateId", idUnitate);

    //        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
    //        vCmd.Connection = connection;

    //        SqlDataReader result = vCmd.ExecuteReader();

    //        while ( result.Read() )
    //        {
    //            gospodarie.Id = Convert.ToInt64(result["gospodarieId"]);
    //            gospodarie.IdInitial = Convert.ToInt32(result["gospodarieIdInitial"]); 

    //            Tuple<Int64, Int64> ids = new Tuple<long, long>(gospodarie.Id,gospodarie.IdInitial);

    //            idsList.Add(ids);
    //        }

    //        result.Close();
    //        ManipuleazaBD.InchideConexiune(connection);

    //        return idsList;
    //    }

    public static GospodariiPentruReNumerotare GetGospodariiById(int id)
    {
        SqlCommand vCmd = new SqlCommand();

        vCmd.CommandText = @"SELECT 
                                       [gospodarieId]
                                       ,0 as Neregularitate
                                      ,[unitateId]
                                      ,[volum]
                                      ,[nrPozitie]
                                      ,[codSiruta]
                                      ,[tip]
                                      ,[strada]
                                      ,[nr]
                                      ,[nrInt]
                                      ,[bl]
                                      ,[sc]
                                      ,[et]
                                      ,[ap]
                                      ,[codExploatatie]
                                      ,[codUnic]
                                      ,[judet]
                                      ,[localitate]
                                      ,[persJuridica]
                                      ,[jUnitate]
                                      ,[jSubunitate]
                                      ,[jCodFiscal]
                                      ,[jNumeReprez]
                                      ,[strainas]
                                      ,[sStrada]
                                      ,[sNr]
                                      ,[sBl]
                                      ,[sSc]
                                      ,[sEtj]
                                      ,[sAp]
                                      ,[sJudet]
                                      ,[sLocalitate]
                                      ,[dataModificare]
                                      ,[oraModificare]
                                      ,[minutModificare]
                                      ,[volumInt]
                                      ,[nrPozitieInt]
                                      ,[an]
                                      ,[gospodarieIdInitial]
                                      ,[gospodarieCui]
                                      ,[gospodarieIdImport]
                                      ,[observatii]
                                      ,[gospodarieSat]
                                      ,[membruNume]
                                      ,[membruCnp]
                                      ,[taraStrainas]
                                      ,[nrRolNominal]
                                      ,[volumVechi]
                                      ,[pozitieVeche]
                                    FROM gospodarii
                                    where gospodarieId = " + id + "";

        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(2016));
        vCmd.Connection = connection;

        SqlDataReader result = vCmd.ExecuteReader();
        GospodariiPentruReNumerotare gospodarie = new GospodariiPentruReNumerotare();
        while (result.Read())
        {
            gospodarie.GospodarieId = Convert.ToInt64(result["gospodarieId"]);
            gospodarie.IdUnitate = Convert.ToInt32(result["unitateId"]);
            gospodarie.Volum = result["volum"].ToString();
            gospodarie.VolumAnterior = gospodarie.Volum;
            gospodarie.NrPozitie = result["nrPozitie"].ToString();
            gospodarie.CodSiruta = result["codSiruta"].ToString();
            gospodarie.Tip = result["tip"].ToString();
            gospodarie.CodExploatatie = result["codExploatatie"].ToString();
            gospodarie.Strada = result["strada"].ToString();
            gospodarie.CodUnic = result["codUnic"].ToString();
            gospodarie.IsPersoanaJuridica = Convert.ToBoolean(result["persJuridica"]);
            gospodarie.Unitate = result["jUnitate"].ToString();
            gospodarie.Subunitate = result["jSubunitate"].ToString();
            gospodarie.CodFiscal = result["jCodFiscal"].ToString();
            gospodarie.Reprezentant = result["jNumeReprez"].ToString();
            gospodarie.Judet = result["judet"].ToString();
            gospodarie.Localitate = result["localitate"].ToString();
            gospodarie.IsStrainas = Convert.ToBoolean(result["strainas"]);
            gospodarie.Nr = result["nr"].ToString();
            gospodarie.NrInt = result["nrint"].ToString();
            gospodarie.DataModificare = Convert.ToDateTime(result["dataModificare"]).Date;
            gospodarie.OraModificare = Convert.ToInt32(result["oraModificare"]);
            gospodarie.MinutModificare = Convert.ToInt32(result["minutModificare"]);
            gospodarie.VolumIntreg = Convert.ToInt32(result["volumInt"]);
            gospodarie.NrPozitieIntreg = Convert.ToInt32(result["nrPozitieInt"]);
            gospodarie.An = result["an"].ToString();
            gospodarie.IdInitial = Convert.ToInt32(result["gospodarieIdInitial"]);
            gospodarie.Cui = result["gospodarieCui"].ToString();
            gospodarie.Observatii = result["observatii"].ToString();
            gospodarie.VolumVechi = result["volumVechi"].ToString();
            gospodarie.MembruNume = result["membruNume"].ToString();
            gospodarie.CnpMembru = result["membruCnp"].ToString();
            gospodarie.NrRolNominal = Convert.ToString(result["nrRolNominal"]);
            gospodarie.PozitieVeche = Convert.ToString(result["pozitieVeche"]);
        }

        result.Close();
        ManipuleazaBD.InchideConexiune(connection);

        return gospodarie;
    }

    public static List<GospodariiPentruReNumerotare> GetGospodariiByUnitate(int idUnitate, int an)
    {
        List<GospodariiPentruReNumerotare> gospodarii = new List<GospodariiPentruReNumerotare>();

        SqlCommand vCmd = new SqlCommand();

        vCmd.CommandText = @"SELECT 
                                       [gospodarieId]
                                      ,[unitateId]
                                      ,[volum]
                                      ,[nrPozitie]
                                      ,[codSiruta]
                                      ,tipgospodarie.[tip] as tip
                                       ,gospodarii.[tip] as idTip
                                      ,[strada]
                                      ,[nr]
                                      ,[nrInt]
                                      ,[bl]
                                      ,[sc]
                                      ,[et]
                                      ,[ap]
                                      ,[codExploatatie]
                                      ,[codUnic]
                                      ,[judet]
                                      ,[localitate]
                                      ,[persJuridica]
                                      ,[jUnitate]
                                      ,[jSubunitate]
                                      ,[jCodFiscal]
                                      ,[jNumeReprez]
                                      ,[strainas]
                                      ,[sStrada]
                                      ,[sNr]
                                      ,[sBl]
                                      ,[sSc]
                                      ,[sEtj]
                                      ,[sAp]
                                      ,[sJudet]
                                      ,[sLocalitate]
                                      ,[dataModificare]
                                      ,[oraModificare]
                                      ,[minutModificare]
                                      ,[volumInt]
                                      ,[nrPozitieInt]
                                      ,[an]
                                      ,[gospodarieIdInitial]
                                      ,[gospodarieCui]
                                      ,[gospodarieIdImport]
                                      ,[observatii]
                                      ,[gospodarieSat]
                                      ,[membruNume]
                                      ,[membruCnp]
                                      ,[taraStrainas]
                                      ,[nrRolNominal]
                                      ,[volumVechi]
                                      ,[pozitieVeche]
                                    FROM gospodarii
                                    INNER JOIN tipgospodarie on tipgospodarie.id = gospodarii.tip
                                    WHERE unitateId = @unitateId
                                    AND an = @an
                             ";

        vCmd.Parameters.AddWithValue("@unitateId", idUnitate);
        vCmd.Parameters.AddWithValue("@an", an);

        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(an));
        vCmd.Connection = connection;

        SqlDataReader result = vCmd.ExecuteReader();

        while (result.Read())
        {
            GospodariiPentruReNumerotare gospodarie = GetGospodarie(result, idUnitate);

            gospodarii.Add(gospodarie);
        }

        result.Close();
        ManipuleazaBD.InchideConexiune(connection);

        return gospodarii;
    }

    public static List<GospodariiPentruReNumerotare> GetGospodariiConflictByUnitate(int idUnitate, int an, string volum, string pozitie)
    {
        List<GospodariiPentruReNumerotare> gospodarii = new List<GospodariiPentruReNumerotare>();

        SqlCommand vCmd = new SqlCommand();

        vCmd.CommandText = @"
                                       SELECT 
                                       [gospodarieId]
                                      ,[unitateId]
                                      ,[volum]
                                      ,[nrPozitie]
                                      ,[codSiruta]
                                      ,[tip]
                                      ,[strada]
                                      ,[nr]
                                      ,[nrInt]
                                      ,[bl]
                                      ,[sc]
                                      ,[et]
                                      ,[ap]
                                      ,[codExploatatie]
                                      ,[codUnic]
                                      ,[judet]
                                      ,[localitate]
                                      ,[persJuridica]
                                      ,[jUnitate]
                                      ,[jSubunitate]
                                      ,[jCodFiscal]
                                      ,[jNumeReprez]
                                      ,[strainas]
                                      ,[sStrada]
                                      ,[sNr]
                                      ,[sBl]
                                      ,[sSc]
                                      ,[sEtj]
                                      ,[sAp]
                                      ,[sJudet]
                                      ,[sLocalitate]
                                      ,[dataModificare]
                                      ,[oraModificare]
                                      ,[minutModificare]
                                      ,[volumInt]
                                      ,[nrPozitieInt]
                                      ,[an]
                                      ,[gospodarieIdInitial]
                                      ,[gospodarieCui]
                                      ,[gospodarieIdImport]
                                      ,[observatii]
                                      ,[gospodarieSat]
                                      ,[membruNume]
                                      ,[membruCnp]
                                      ,[taraStrainas]
                                      ,[nrRolNominal]
                                      ,[volumVechi]
                                      ,[pozitieVeche]
                                    FROM gospodarii
                                    INNER JOIN tipgospodarie on tipgospodarie.id = tip
                                    WHERE unitateId = @unitateId
                                    AND an = @an
                                    AND volum = '" + volum + "' AND nrPozitie = '" + pozitie + "'";

        vCmd.Parameters.AddWithValue("@unitateId", idUnitate);
        vCmd.Parameters.AddWithValue("@an", an);

        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(an));
        vCmd.Connection = connection;
        if (volum != null && pozitie != null)
        {
            SqlDataReader result = vCmd.ExecuteReader();

            while (result.Read())
            {
                GospodariiPentruReNumerotare gospodarie = GetGospodarie(result, idUnitate);

                gospodarii.Add(gospodarie);
            }

            result.Close();
            ManipuleazaBD.InchideConexiune(connection);
        }
        return gospodarii;
    }

    private static GospodariiPentruReNumerotare GetGospodarie(SqlDataReader result, int idUnitate)
    {
        GospodariiPentruReNumerotare gospodarie = new GospodariiPentruReNumerotare();

        gospodarie.GospodarieId = Convert.ToInt64(result["gospodarieId"]);
        gospodarie.IdUnitate = idUnitate;
        gospodarie.Volum = result["volum"].ToString();
        gospodarie.VolumLaIncarcareGrid = result["volum"].ToString();

        gospodarie.VolumAnterior = gospodarie.Volum;
        gospodarie.NrPozitie = result["nrPozitie"].ToString();
        gospodarie.NrPozitieLaIncarcareGrid = result["nrPozitie"].ToString();
        gospodarie.CodSiruta = result["codSiruta"].ToString();
        gospodarie.Tip = result["tip"].ToString();
        gospodarie.IdTip = Convert.ToInt32(result["idTip"]);
        gospodarie.CodExploatatie = result["codExploatatie"].ToString();
        gospodarie.Strada = result["strada"].ToString();
        gospodarie.CodUnic = result["codUnic"].ToString();
        gospodarie.IsPersoanaJuridica = Convert.ToBoolean(result["persJuridica"]);
        gospodarie.Unitate = result["jUnitate"].ToString();
        gospodarie.Subunitate = result["jSubunitate"].ToString();
        gospodarie.CodFiscal = result["jCodFiscal"].ToString();
        gospodarie.Reprezentant = result["jNumeReprez"].ToString();
        gospodarie.Judet = result["judet"].ToString();
        gospodarie.Localitate = result["localitate"].ToString();
        gospodarie.IsStrainas = Convert.ToBoolean(result["strainas"]);
        gospodarie.Nr = result["nr"].ToString();
        gospodarie.NrInt = result["nrint"].ToString();
        gospodarie.DataModificare = Convert.ToDateTime(result["dataModificare"]).Date;
        gospodarie.OraModificare = Convert.ToInt32(result["oraModificare"]);        //Convert.ToDateTime(result["oraModificare"]).Hour;
        gospodarie.MinutModificare = Convert.ToInt32(result["minutModificare"]);    //Convert.ToDateTime(result["minutModificare"]).Hour;
        gospodarie.VolumIntreg = Convert.ToInt32(result["volumInt"]);
        gospodarie.NrPozitieIntreg = Convert.ToInt32(result["nrPozitieInt"]);
        gospodarie.An = "2016";
        gospodarie.IdInitial = Convert.ToInt32(result["gospodarieIdInitial"]);
        gospodarie.Cui = result["gospodarieCui"].ToString();
        gospodarie.Observatii = result["observatii"].ToString();
        gospodarie.VolumVechi = result["volumVechi"].ToString();
        gospodarie.MembruNume = result["membruNume"].ToString();
        gospodarie.CnpMembru = result["membruCnp"].ToString();
        gospodarie.NrRolNominal = Convert.ToString(result["nrRolNominal"]);
        gospodarie.PozitieVeche = Convert.ToString(result["pozitieVeche"]);

        return gospodarie;
    }

    internal static long GetIdsByIdAndAnForIdFinal(long idInitial, int an)
    {
        GospodariiPentruReNumerotare gospodarie = new GospodariiPentruReNumerotare();
        SqlCommand vCmd = new SqlCommand();

        vCmd.CommandText = @"SELECT  gospodarieId
                                    FROM gospodarii
                                    WHERE gospodarieIdInitial = @gospodarieId AND an = @an
                             ";

        vCmd.Parameters.AddWithValue("@gospodarieId", idInitial);
        vCmd.Parameters.AddWithValue("@an", an);

        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(an));
        vCmd.Connection = connection;

        SqlDataReader result = vCmd.ExecuteReader();

        if (result.Read())
        {
            gospodarie.GospodarieId = Convert.ToInt64(result["gospodarieId"]);
        }

        result.Close();
        ManipuleazaBD.InchideConexiune(connection);

        return gospodarie.GospodarieId;
    }

    public static int GetIdInitialById(int idGospodarie, short an)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(an);
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select gospodarieIdInitial from gospodarii where gospodarieId ='" + idGospodarie + "'";

        return Convert.ToInt32(vCmd.ExecuteScalar());
    }

    public static void SetUpdateList(List<GospodariiPentruReNumerotare> gospodariilista)
    {
        int result = 0;
        int contor = 0;
        int numarGospodarii = 0;
        string query = string.Empty;
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(2016);

        foreach (GospodariiPentruReNumerotare gospodarie in gospodariilista)
        {
            query += @"UPDATE [gospodarii]
                               SET
                                  [volum] = '" + gospodarie.Volum + @"'
                                 ,[nrPozitie] = '" + gospodarie.NrPozitie + @"'
                             WHERE gospodarieId = '" + gospodarie.GospodarieId + "' AND unitateId = '" + gospodarie.IdUnitate + "' AND an = '" + gospodarie.An + "';";
            contor++;
            numarGospodarii++;
            if (contor >= 500 || numarGospodarii >= gospodariilista.Count)
            {
                using (vCon)
                {
                    var command = new SqlCommand(query, vCon);
                    result += command.ExecuteNonQuery();
                    query = string.Empty;
                    contor = 0;
                }
            }
        }
       
    }

}
