﻿using System;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ParceleServices
/// </summary>
public class ParceleServices
{
    public static short ConnectionYear = 0;

    public static Parcele GetParcelaById(long idParcela)
    {
        if (idParcela == 0)
        {
            return new Parcele();
        }

        Parcele parcela = new Parcele();

        SqlConnection vCon = ManipuleazaBD.CreareConexiune(ConnectionYear);
        SqlCommand command = new SqlCommand();
        command.Connection = vCon;

        command.CommandText = @"SELECT	 [parcelaDenumire]
                                        ,[parcelaCodRand]
                                        ,[parcelaSuprafataIntravilanHa]
                                        ,[parcelaSuprafataIntravilanAri]
                                        ,[parcelaSuprafataExtravilanHa]
                                        ,[parcelaSuprafataExtravilanAri]
                                        ,[parcelaNrTopo]
                                        ,[parcelaCF]
                                        ,[parcelaCategorie]
                                        ,[parcelaNrBloc]
                                        ,[parcelaMentiuni]
                                        ,[unitateaId]
                                        ,[gospodarieId]
                                        ,[an]
                                        ,[capitolId]
                                        ,[parcelaNrCadastral]
                                        ,[parcelaNrCadastralProvizoriu]
                                        ,[parcelaLocalitate]
                                        ,[parcelaAdresa]
                                        ,[volum]
                                        ,[nrPozitie]
                                        ,[importId]
                                        ,[parcelaTarla]
                                        ,[parcelaTitluProprietate]
                                        ,[parcelaClasaBonitate]
                                        ,[parcelaPunctaj]
                                        ,[parcelaIdInitial]
                                        ,[parcelaTimpAdaugare]
                                        ,[parcelaIdImport]
                                        ,[proprietarId]
                               FROM     parcele 
                               WHERE    parcelaId = @idParcela";

        command.Parameters.AddWithValue("@idParcela", idParcela);

        SqlDataReader parcelaDataReader = command.ExecuteReader();
        parcela.SuprafataExtravilan = new Suprafete();
        parcela.SuprafataIntravilan = new Suprafete();

        while (parcelaDataReader.Read())
        {
            parcela.Id = idParcela;
            parcela.UnitateId = Convert.ToInt16(parcelaDataReader["unitateaId"].ToString());
            parcela.IdGospodarie =  Convert.ToInt64(parcelaDataReader["gospodarieId"].ToString());
            parcela.An = parcelaDataReader["an"] != DBNull.Value ? Convert.ToInt16(parcelaDataReader["an"].ToString()) : 0;
            parcela.Adresa = parcelaDataReader["parcelaAdresa"] != DBNull.Value ? parcelaDataReader["parcelaAdresa"].ToString() : string.Empty;
            parcela.Denumire = parcelaDataReader["parcelaDenumire"] != DBNull.Value ? parcelaDataReader["parcelaDenumire"].ToString() : string.Empty;
            parcela.SuprafataIntravilan.Hectare = parcelaDataReader["parcelaSuprafataIntravilanHa"] != DBNull.Value ? Convert.ToDecimal(parcelaDataReader["parcelaSuprafataIntravilanHa"]) : 0;
            parcela.SuprafataIntravilan.Ari = parcelaDataReader["parcelaSuprafataIntravilanAri"] != DBNull.Value ? Convert.ToDecimal(parcelaDataReader["parcelaSuprafataIntravilanAri"]) : 0;
            parcela.SuprafataExtravilan.Hectare = parcelaDataReader["parcelaSuprafataExtravilanHa"] != DBNull.Value ? Convert.ToDecimal(parcelaDataReader["parcelaSuprafataExtravilanHa"]) : 0;
            parcela.SuprafataExtravilan.Ari = parcelaDataReader["parcelaSuprafataExtravilanAri"] != DBNull.Value ? Convert.ToDecimal(parcelaDataReader["parcelaSuprafataExtravilanAri"]) : 0;
            parcela.CodRand = parcelaDataReader["parcelaCodRand"] != DBNull.Value ? parcelaDataReader["parcelaCodRand"].ToString() : string.Empty;
            parcela.NrTopo = parcelaDataReader["parcelaNrTopo"] != DBNull.Value ? parcelaDataReader["parcelaNrTopo"].ToString() : string.Empty;
            parcela.CF = parcelaDataReader["parcelaCF"] != DBNull.Value ? parcelaDataReader["parcelaCF"].ToString() : string.Empty;
            parcela.Categorie = parcelaDataReader["parcelaCategorie"] != DBNull.Value ? parcelaDataReader["parcelaCategorie"].ToString() : string.Empty;
            try
            {
                parcela.ProprietarId = Convert.ToInt64(parcelaDataReader["proprietarId"].ToString());
            }
            catch
            {
                parcela.ProprietarId = 0;
            }
            parcela.NrBloc = parcelaDataReader["parcelaNrBloc"] != DBNull.Value ? parcelaDataReader["parcelaNrBloc"].ToString() : string.Empty; 
            parcela.Mentiuni = parcelaDataReader["parcelaMentiuni"] != DBNull.Value ? parcelaDataReader["parcelaMentiuni"].ToString() : string.Empty;
            parcela.NrCadastral = parcelaDataReader["parcelaNrCadastral"] != DBNull.Value ? parcelaDataReader["parcelaNrCadastral"].ToString() : string.Empty;
            parcela.NrCadastralProvizoriu = parcelaDataReader["parcelaNrCadastralProvizoriu"] != DBNull.Value ? parcelaDataReader["parcelaNrCadastralProvizoriu"].ToString() : string.Empty;
            parcela.Localitate = parcelaDataReader["parcelaLocalitate"] != DBNull.Value ? parcelaDataReader["parcelaLocalitate"].ToString() : string.Empty;
            parcela.Tarla = parcelaDataReader["parcelaTarla"] != DBNull.Value ? parcelaDataReader["parcelaTarla"].ToString() : string.Empty;
            parcela.TitluProprietate = parcelaDataReader["parcelaTitluProprietate"] != DBNull.Value ? parcelaDataReader["parcelaTitluProprietate"].ToString() : string.Empty;
            parcela.ClasaBonitate = parcelaDataReader["parcelaClasaBonitate"] != DBNull.Value ? parcelaDataReader["parcelaClasaBonitate"].ToString() : string.Empty;
            parcela.Punctaj = parcelaDataReader["parcelaPunctaj"] != DBNull.Value ? parcelaDataReader["parcelaPunctaj"].ToString() : string.Empty; 
        }

        parcelaDataReader.Close();
        ManipuleazaBD.InchideConexiune(vCon);

        return parcela;
    }

    internal static bool Adauga(Parcele parcela)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(HttpContext.Current.Session["SESan"]));
        SqlCommand command = new SqlCommand();
        command.Connection = vCon;

        command.CommandText = @"INSERT INTO parcele (
                                                    parcelaDenumire,
                                                    parcelaCodRand,
                                                    parcelaSuprafataIntravilanHa, 
                                                    parcelaSuprafataIntravilanAri,  
                                                    parcelaSuprafataExtravilanHa, 
                                                    parcelaSuprafataExtravilanAri,
                                                    parcelaNrTopo,
                                                    parcelaCF,  
                                                    parcelaCategorie, 
                                                    parcelaNrBloc,  
                                                    parcelaMentiuni, 
                                                    an, 
                                                    unitateaId, 
                                                    gospodarieId, 
                                                    parcelaNrCadastral, 
                                                    parcelaNrCadastralProvizoriu, 
                                                    parcelaLocalitate, 
                                                    parcelaAdresa, 
                                                    parcelaTarla, 
                                                    parcelaTitluProprietate, 
                                                    parcelaClasaBonitate,  
                                                    parcelaPunctaj, 
                                                    parcelaTimpAdaugare,
                                                    proprietarId
                                               )
                                        VALUES (     @parcelaDenumire
                                                    ,@parcelaCodRand
                                                    ,@parcelaSuprafataIntravilanHa
                                                    ,@parcelaSuprafataIntravilanAri
                                                    ,@parcelaSuprafataExtravilanHa
                                                    ,@parcelaSuprafataExtravilanAri
                                                    ,@parcelaNrTopo
                                                    ,@parcelaCF
                                                    ,@parcelaCategorie
                                                    ,@parcelaNrBloc
                                                    ,@parcelaMentiuni
                                                    ,@an
                                                    ,@unitateaId
                                                    ,@gospodarieId
                                                    ,@parcelaNrCadastral 
                                                    ,@parcelaNrCadastralProvizoriu 
                                                    ,@parcelaLocalitate 
                                                    ,@parcelaAdresa 
                                                    ,@parcelaTarla 
                                                    ,@parcelaTitluProprietate 
                                                    ,@parcelaClasaBonitate  
                                                    ,@parcelaPunctaj 
                                                    ,@parcelaTimpAdaugare
                                                    ,@proprietarId);
                                                     SELECT SCOPE_IDENTITY()
                                                     ";

        command.Parameters.AddWithValue("@parcelaDenumire", parcela.Denumire);
        command.Parameters.AddWithValue("@parcelaCodRand", parcela.CodRand);
        command.Parameters.AddWithValue("@parcelaSuprafataIntravilanHa", parcela.SuprafataIntravilan.Hectare);
        command.Parameters.AddWithValue("@parcelaSuprafataIntravilanAri", parcela.SuprafataIntravilan.Ari);
        command.Parameters.AddWithValue("@parcelaSuprafataExtravilanHa", parcela.SuprafataExtravilan.Hectare);
        command.Parameters.AddWithValue("@parcelaSuprafataExtravilanAri", parcela.SuprafataExtravilan.Ari);
        command.Parameters.AddWithValue("@parcelaNrTopo", parcela.NrTopo);
        command.Parameters.AddWithValue("@parcelaCF", parcela.CF);
        command.Parameters.AddWithValue("@parcelaCategorie", parcela.Categorie);
        command.Parameters.AddWithValue("@parcelaNrBloc", parcela.NrBloc);
        command.Parameters.AddWithValue("@parcelaMentiuni", parcela.Mentiuni);
        command.Parameters.AddWithValue("@an", parcela.An);
        command.Parameters.AddWithValue("@unitateaId", parcela.UnitateId);
        command.Parameters.AddWithValue("@gospodarieId", parcela.IdGospodarie);
        command.Parameters.AddWithValue("@parcelaNrCadastral",parcela.NrCadastral );
        command.Parameters.AddWithValue("@parcelaNrCadastralProvizoriu",parcela.NrCadastralProvizoriu ); 
        command.Parameters.AddWithValue("@parcelaLocalitate",parcela.Localitate ); 
        command.Parameters.AddWithValue("@parcelaAdresa",parcela.Adresa ); 
        command.Parameters.AddWithValue("@parcelaTarla",parcela.Tarla ); 
        command.Parameters.AddWithValue("@parcelaTitluProprietate",parcela.TitluProprietate ); 
        command.Parameters.AddWithValue("@parcelaClasaBonitate",parcela.ClasaBonitate ); 
        command.Parameters.AddWithValue("@parcelaPunctaj",parcela.Punctaj ); 
        command.Parameters.AddWithValue("@parcelaTimpAdaugare",parcela.TimpAdaugare );
        command.Parameters.AddWithValue("@proprietarId",parcela.ProprietarId );

        try
        {
            long idParcela = Convert.ToInt64(command.ExecuteScalar());

            ManipuleazaBD.fManipuleazaBD("UPDATE parcele SET parcelaIdInitial = " + idParcela + " WHERE parcelaId = " + idParcela, Convert.ToInt16(parcela.An));

            return true;
        }
        catch
        {
            return false;
        }
        finally
        {
            vCon.Close();
        }
    }

    public static bool Modifica(Parcele parcela)
    {
        if (parcela.Id == 0)
        {
            return false;
        }

        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(HttpContext.Current.Session["SESan"]));
        SqlCommand command = new SqlCommand();
       
        string sqlString = @"
                            UPDATE  [parcele]
                               SET [parcelaDenumire] = @parcelaDenumire
                                  ,[parcelaCodRand] = @parcelaCodRand
                                  ,[parcelaSuprafataIntravilanHa] = @parcelaSuprafataIntravilanHa
                                  ,[parcelaSuprafataIntravilanAri] = @parcelaSuprafataIntravilanAri
                                  ,[parcelaSuprafataExtravilanHa] = @parcelaSuprafataExtravilanHa
                                  ,[parcelaSuprafataExtravilanAri] = @parcelaSuprafataExtravilanAri
                                  ,[parcelaNrTopo] = @parcelaNrTopo
                                  ,[parcelaCF] = @parcelaCF
                                  ,[parcelaCategorie] = @parcelaCategorie
                                  ,[parcelaNrBloc] =  @parcelaNrBloc
                                  ,[parcelaMentiuni] = @parcelaMentiuni
                                  ,[parcelaNrCadastral] = @parcelaNrCadastral
                                  ,[parcelaNrCadastralProvizoriu] = @parcelaNrCadastralProvizoriu
                                  ,[parcelaLocalitate] = @parcelaLocalitate
                                  ,[parcelaAdresa] = @parcelaAdresa
                                  ,[parcelaTarla] = @parcelaTarla
                                  ,[parcelaTitluProprietate] = @parcelaTitluProprietate
                                  ,[parcelaClasaBonitate] = @parcelaClasaBonitate
                                  ,[parcelaPunctaj] = @parcelaPunctaj
                                  ,[proprietarId] = @proprietarId
                             WHERE parcelaId = @parcelaId";

        command.Parameters.AddWithValue("@parcelaDenumire", parcela.Denumire);
        command.Parameters.AddWithValue("@parcelaCodRand", parcela.CodRand);
        command.Parameters.AddWithValue("@parcelaSuprafataIntravilanHa", parcela.SuprafataIntravilan.Hectare);
        command.Parameters.AddWithValue("@parcelaSuprafataIntravilanAri", parcela.SuprafataIntravilan.Ari);
        command.Parameters.AddWithValue("@parcelaSuprafataExtravilanHa", parcela.SuprafataExtravilan.Hectare);
        command.Parameters.AddWithValue("@parcelaSuprafataExtravilanAri", parcela.SuprafataExtravilan.Ari);
        command.Parameters.AddWithValue("@parcelaNrTopo", parcela.NrTopo);
        command.Parameters.AddWithValue("@parcelaCF", parcela.CF);
        command.Parameters.AddWithValue("@parcelaCategorie", parcela.Categorie);
        command.Parameters.AddWithValue("@parcelaNrBloc", parcela.NrBloc);
        command.Parameters.AddWithValue("@parcelaMentiuni", parcela.Mentiuni);
        command.Parameters.AddWithValue("@parcelaNrCadastral", parcela.NrCadastral);
        command.Parameters.AddWithValue("@parcelaNrCadastralProvizoriu", parcela.NrCadastralProvizoriu);
        command.Parameters.AddWithValue("@parcelaLocalitate", parcela.Localitate);
        command.Parameters.AddWithValue("@parcelaAdresa", parcela.Adresa);
        command.Parameters.AddWithValue("@parcelaTarla", parcela.Tarla);
        command.Parameters.AddWithValue("@parcelaTitluProprietate", parcela.TitluProprietate);
        command.Parameters.AddWithValue("@parcelaClasaBonitate", parcela.ClasaBonitate);
        command.Parameters.AddWithValue("@parcelaPunctaj", parcela.Punctaj);
        command.Parameters.AddWithValue("@proprietarId", parcela.ProprietarId);
        command.Parameters.AddWithValue("@parcelaId", parcela.Id);

        command.Connection = connection;
        command.CommandText = sqlString;

        try
        {
            command.ExecuteNonQuery();

            return true;
        }
        catch
        {
            return false;
        }
        finally
        {
            connection.Close();
        }
    }

}
