﻿using System;
using System.Collections.Generic;
using System.Linq;

public class CulturiTotale:Culturi
{

    public CulturiTotale()
    {
        Capitol = new Capitole();
    }

    public override List<Culturi> GetCulturi()
    {
       return CulturiServices.GetCulturiTotale(this);
    }

    public override bool UpdateCultura()
    {
        return CulturiServices.UpdateCulturiTotale(this);
    }

    public override bool DeleteCultura()
    {
        throw new NotImplementedException();
    }

    public override bool AddCultura(Parcele parcela)
    {
        throw new NotImplementedException();
    }
}
