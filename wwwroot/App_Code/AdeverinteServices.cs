﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

public static class AdeverinteServices
{
    public static List<Adeverinta> GetAdeverinteByGospodarieId(long gospodarieId)
    {
        if ( gospodarieId == 0 )
        {
            return new List<Adeverinta>();
        }

        List<Adeverinta> adeverinte = new List<Adeverinta>();
        SqlCommand command = new SqlCommand();

        command.CommandText = @"SELECT [adeverintaId]
                                      ,[unitateId]
                                      ,[gospodarieId]
                                      ,[utilizatorId]
                                      ,[numeMembru]
                                      ,[numarChitanta]
                                      ,[numarInregistrare]
                                      ,[numarIesire]
                                      ,[data]
                                      ,[antet]
                                      ,[text]
                                      ,[textTiparit]
                                      ,[motiv]
                                      ,[subsol]
                                      ,[ora]
                                      ,[minut]
                                      ,[tipSablon]
                                  FROM [adeverinte]
                                  WHERE gospodarieId = @gospodarieId
                             ";

        command.Parameters.AddWithValue("@gospodarieId", gospodarieId);

        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(DateTime.Now.Year));
        command.Connection = connection;

        SqlDataReader result = command.ExecuteReader();

        while (result.Read())
        {
            Adeverinta adeverinta = GetAdeverinta(result, gospodarieId);

            adeverinte.Add(adeverinta);
        }

        result.Close();
        ManipuleazaBD.InchideConexiune(connection);

        return adeverinte;
    }

    private static Adeverinta GetAdeverinta(SqlDataReader result, long gospodarieId)
    {
        SablonAdeverinta sablonAdeverinta = new SablonAdeverinta();
        Adeverinta adeverinta = new Adeverinta(sablonAdeverinta);

        adeverinta.Id = Convert.ToInt32(result["adeverintaId"]);
        adeverinta.UnitateId =  Convert.ToInt32(result["unitateId"]);
        adeverinta.Data = Convert.ToDateTime(result["data"]).Date;   
        adeverinta.GospodarieId = gospodarieId;
        adeverinta.NumeMembru = result["numeMembru"].ToString();
        adeverinta.Motiv = result["motiv"].ToString();
        adeverinta.TextTiparit = result["textTiparit"].ToString();
        adeverinta.TipSablon = Convert.ToInt32(result["tipSablon"]);
        adeverinta.SablonAdeverinta = SablonAdeverintaServices.GetSablonByTip(adeverinta.TipSablon);
           
        return adeverinta;
    }

}