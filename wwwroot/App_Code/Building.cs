﻿using System;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Summary description for Building
/// </summary>
public class Building
{
    public Dictionary<long, Building> Buildings;

    int unitateId = -1;
    public int UnitateId
    {
        get
        {
            return this.unitateId;
        }
        set
        {
            this.unitateId = value;
        }
    }

    long idGospodarie = 0;
    public long IdGospodarie
    {
        get
        {
            return this.idGospodarie;
        }
        set
        {
            this.idGospodarie = value;
        }
    }

    long id = 0;
    public long Id
    {
        get
        {
            return this.id;
        }
        set
        {
            this.id = value;
        }
    }

    int an = 0;
    public int An
    {
        get
        {
            return this.an;
        }
        set
        {
            this.an = value;
        }
    }
   
    string adresa = string.Empty;
    public string Adresa
    {
        get
        {
            return this.adresa;
        }
        set
        {
            this.adresa = value;
        }
    }
 
    string zona = string.Empty;
    public string Zona
    {
        get
        {
            return this.zona;
        }
        set
        {
            this.zona = value;
        }
    }
   
    decimal suprafata = 0;
    public decimal Suprafata
    {
        get
        {
            return this.suprafata;
        }
        set
        {
            this.suprafata = value;
        }
    }
   
    string tip = string.Empty;
    public string Tip
    {
        get
        {
            return this.tip;
        }
        set
        {
            this.tip = value;
        }
    }
  
    string destinatie = string.Empty;
    public string Destinatie
    {
        get
        {
            return this.destinatie;
        }
        set
        {
            this.destinatie = value;
        }
    }

    int anTerminare = 0;
    public int AnTerminare
    {
        get { return anTerminare; }
        set { anTerminare = value; }
    }

    public bool BuildingAdded
    {
        get
        {
            return AddBuilding();
        }
    }

    public bool BuildingUpdated
    {
        get
        {
            return UpdateBuilding();
        }
    }

    public bool BuildingDeleted
    {
        get
        {
            return RemoveBuilding();
        }
    }

    public Building()
    {
        Buildings =  new Dictionary<long, Building>();
    }

    public void Clear()
    {
        adresa = string.Empty;
        zona = string.Empty;
        suprafata = 0;
        tip = string.Empty;
        destinatie = string.Empty;
        anTerminare = 0;
        id = 0;
        Buildings.Clear();
    }

    public Building Fill()
    {
        return BuildingServices.GetBuildingById(this.id);
    }

    public Dictionary<long, Building> FillBuildings()
    {
        Buildings = BuildingServices.GetBuildingsByIDGospodarie(this.idGospodarie);
        
        return Buildings;
    }

    public bool AddBuilding()
    {
        return BuildingServices.AddBuilding(this);
    }

    public bool RemoveBuilding()
    {
        return BuildingServices.RemoveBuilding(this.id);
    }

    public bool UpdateBuilding()
    {
        return BuildingServices.UpdateBuilding(this);
    }

    public List<Building> GetMissingBuildingsByUInitate()
    {
        return BuildingServices.GetMissingBuildingsByUInitate(unitateId);
    }


    public bool AddMissingBuildings(List<Building> buildings)
    {
        return BuildingServices.AddMissingBuildings(buildings);
    }
}