﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for InchidereAn
/// </summary>
public class InchidereAn
{

	public InchidereAn()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public static bool VerificaDacaAnulEsteDeschis(int unitateId)
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(HttpContext.Current.Session["SESan"]));
        string interogare = @"SELECT COUNT(*) 
                                  FROM inchideriAn 
                              WHERE unitateId='" + unitateId + "' AND an='" + HttpContext.Current.Session["SESan"] + "'";
        SqlCommand command = new SqlCommand(interogare, connection);
        int count = Convert.ToInt32(command.ExecuteScalar());
        ManipuleazaBD.InchideConexiune(connection);
        if (count > 0)
            return false;
        return true;
    }
    public static void DeleteAllFromUnitate(int idUnitate)
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(HttpContext.Current.Session["SESan"]));
        string delete = @"delete from gospodarii where unitateId='" + idUnitate.ToString() + "' and an='" + (Convert.ToInt32(HttpContext.Current.Session["SESan"]) + 1) + @"';
                          delete from membri where unitateId='" + idUnitate.ToString() + "'  and an='" + (Convert.ToInt32(HttpContext.Current.Session["SESan"]) + 1) + @"';
                          delete from parcele where unitateaId='" + idUnitate.ToString() + "' and an='" + (Convert.ToInt32(HttpContext.Current.Session["SESan"]) + 1) + @"';
                          delete from coordonate where unitateId='" + idUnitate.ToString() + "' and an='" + (Convert.ToInt32(HttpContext.Current.Session["SESan"]) + 1) + @"';
                          delete from paduri where unitateId='" + idUnitate.ToString() + "' and an='" + (Convert.ToInt32(HttpContext.Current.Session["SESan"]) + 1) + @"';
                          delete from capitole where unitateId='" + idUnitate.ToString() + "' and an='" + (Convert.ToInt32(HttpContext.Current.Session["SESan"]) + 1) + @"';
                          delete from capitoleCentralizate where unitateId='" + idUnitate.ToString() + "' and an='" + (Convert.ToInt32(HttpContext.Current.Session["SESan"]) + 1) + @"';
                          delete from parceleCatreCapitole where unitateId='" + idUnitate.ToString() + "' and anul='" + (Convert.ToInt32(HttpContext.Current.Session["SESan"]) + 1) + @"';
                          delete from mesajeInvalidari where unitateId='" + idUnitate.ToString() + "' and an='" + (Convert.ToInt32(HttpContext.Current.Session["SESan"]) + 1) + "'";
        SqlCommand command = new SqlCommand(delete, connection);
        command.CommandTimeout = 0;
        command.ExecuteNonQuery();
        ManipuleazaBD.InchideConexiune(connection);
    }
    
    public static DataTable GetGospodarii(SqlConnection connection, int unitateId)
    {
        string interogare = @"SELECT *
                                FROM gospodarii
                               where unitateId = " + unitateId + " and an = " + HttpContext.Current.Session["SESAn"] + "";
        SqlDataAdapter gospodariiAdapter = new SqlDataAdapter(interogare, connection);
        DataTable gospodariiTable = new DataTable();
        gospodariiAdapter.Fill(gospodariiTable);
        return gospodariiTable;
    }
  
    public static DataTable GetUnitati(CheckBoxList unitatiCheckBoxList, short an)
    {

        SqlConnection connection = ManipuleazaBD.CreareConexiune(an);
        string interogare = GetListaUnitati(unitatiCheckBoxList);
        SqlDataAdapter unitatiAdapter = new SqlDataAdapter(interogare, connection);
        DataTable unitatiTable = new DataTable();
        unitatiAdapter.Fill(unitatiTable);
        ManipuleazaBD.InchideConexiune(connection);
        return unitatiTable;
    }
    public static DataTable GetMembrii(int idUnitate, int idGospodarie, SqlConnection connection)
    {
        string interogareMembri = @"SELECT *
                                            FROM membri
                                        WHERE gospodarieId =" + idGospodarie + " and unitateId =" + idUnitate + " and an =" + HttpContext.Current.Session["SESan"] + "";
        SqlCommand membriCommand = new SqlCommand(interogareMembri, connection);
        SqlDataAdapter membriAdapter = new SqlDataAdapter(membriCommand);
        DataTable membriDataTable = new DataTable();
        membriAdapter.Fill(membriDataTable);
        return membriDataTable;
    }
    public static DataTable GetParcele(int idUnitate, int idGospodarie, SqlConnection connection)
    {
        string interogareParcele = @"SELECT *
                                            FROM parcele
                                         WHERE gospodarieId = " + idGospodarie + " and an = " + HttpContext.Current.Session["SESAn"] + " and unitateaId = '" + idUnitate + "'";

        SqlCommand parceleCommand = new SqlCommand(interogareParcele, connection);
        SqlDataAdapter parceleAdapter = new SqlDataAdapter(parceleCommand);
        DataTable parceleDataTable = new DataTable();
        parceleAdapter.Fill(parceleDataTable);
        return parceleDataTable;
    }
    public static DataTable GetCoordonate(int idUnitate, int idGospodarie, int idParcela, SqlConnection connection)
    {

        string interogareCoordonate = @"SELECT *
                                                    FROM coordonate
                                                WHERE gospodarieId = " + idGospodarie + " and unitateId=" + idUnitate + " and an=" + HttpContext.Current.Session["SESan"] + " and parcelaId = " + idParcela + " ";
        SqlCommand coordonateCommand = new SqlCommand(interogareCoordonate, connection);
        SqlDataAdapter coordonateAdapter = new SqlDataAdapter(coordonateCommand);
        DataTable coordonateDataTable = new DataTable();
        coordonateAdapter.Fill(coordonateDataTable);
        return coordonateDataTable;
    }
    public static DataTable GetParceleCatreCapitole(int idUnitate, int idGospodarie, int idParcela, SqlConnection connection)
    {
        string interogareParceleCatreCapitole = @"SELECT *
                                                             FROM parceleCatreCapitole
                                                           WHERE gospodarieId= " + idGospodarie + " and unitateId = " + idUnitate + " and anul = " + HttpContext.Current.Session["SESan"] + " and parcelaId = " + idParcela + "";
        SqlCommand parceleCatreCapitoleCommand = new SqlCommand(interogareParceleCatreCapitole, connection);
        SqlDataAdapter parceleCatreCapitoleAdapter = new SqlDataAdapter(parceleCatreCapitoleCommand);
        DataTable parceleCatreCapitoleDataTable = new DataTable();
        parceleCatreCapitoleAdapter.Fill(parceleCatreCapitoleDataTable);
        return parceleCatreCapitoleDataTable;
    }
    public static DataTable GetCapitole(int idUnitate, int idGospodarie, SqlConnection connection)
    {
        string interogareCapitole = @"SELECT * 
                                                FROM capitole
                                              WHERE gospodarieId=" + idGospodarie + " and unitateId= " + idUnitate + " and an=" + HttpContext.Current.Session["SESan"] + " and codCapitol <> '7' and codCapitol <> '8'";
        SqlCommand capitoleCommand = new SqlCommand(interogareCapitole, connection);
        SqlDataAdapter capitoleAdapter = new SqlDataAdapter(capitoleCommand);
        DataTable capitoleDataTable = new DataTable();
        capitoleAdapter.Fill(capitoleDataTable);
        return capitoleDataTable;
    }
    public static DataTable GetPaduri(int idUnitate, int idGospodarie, SqlConnection connection)
    {
        string interogarePaduri = @"SELECT *
                                         FROM paduri
                                        WHERE gospodarieId=" + idGospodarie + " and unitateId=" + idUnitate + " and an=" + HttpContext.Current.Session["SESan"] + "";
        SqlCommand paduriCommand = new SqlCommand(interogarePaduri, connection);
        SqlDataAdapter paduriAdapter = new SqlDataAdapter(paduriCommand);
        DataTable paduriDataTable = new DataTable();
        paduriAdapter.Fill(paduriDataTable);
        return paduriDataTable;
    }
    public static DataTable GetAdeverinte(int idUnitate, int idGospodarie, SqlConnection connection)
    {
        string interogareAdeverinte = @"SELECT gospodarieId
                                            FROM  adeverinte 
                                            WHERE  year(data)='" + HttpContext.Current.Session["sesAn"] + "' and unitateId='" + idUnitate + "' and gospodarieId = '" + idGospodarie + "'";
        SqlCommand adeverinteCommand = new SqlCommand(interogareAdeverinte, connection);
        SqlDataAdapter adeverinteAdapter = new SqlDataAdapter(adeverinteCommand);
        DataTable adeverinteDataTable = new DataTable();
        adeverinteAdapter.Fill(adeverinteDataTable);
        return adeverinteDataTable;
    }
    public static DataTable GetAnimale(int idUnitate, int idGospodarie, SqlConnection connection)
    {
        string interogareAnimale = @"SELECT col2,codRand 
                                                   FROM capitole 
                                               WHERE codCapitol = '8' AND gospodarieId = " + idGospodarie + " AND an = " + HttpContext.Current.Session["SESan"] + " AND unitateId = " + idUnitate + "";
        SqlCommand animaleCommand = new SqlCommand(interogareAnimale, connection);
        SqlDataAdapter animaleAdapter = new SqlDataAdapter(animaleCommand);
        DataTable animaleTable = new DataTable();
        animaleAdapter.Fill(animaleTable);
        return animaleTable;
    }
    public static void InsertSabloaneCapitole()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16( HttpContext.Current.Session["SESan"]));
        string interogareSabloaneCapitole = @"INSERT INTO sabloaneCapitole
                                                     ([an],[capitol],[codRand],[formula],[denumire1] ,[denumire2],[denumire3],[denumire4],[denumire5],[legatura])
                                             SELECT " + (Convert.ToInt32(HttpContext.Current.Session["SESan"]) + 1) + @",[capitol],[codRand],[formula],[denumire1],[denumire2]
                                                        ,[denumire3],[denumire4],[denumire5],[legatura] FROM sabloaneCapitole as sabloaneCapitole_1
                                                WHERE an = " + HttpContext.Current.Session["SESan"] + "";
        SqlCommand sabloaneCapitoleCommand = new SqlCommand(interogareSabloaneCapitole, connection);
        sabloaneCapitoleCommand.ExecuteNonQuery();
        ManipuleazaBD.InchideConexiune(connection);
    }

    public static void InsertCentralizatoare(int idUnitate,SqlConnection connection)
    {
        string insert = @"INSERT INTO capitoleCentralizate ([unitateId],[gospodarieId],[an],[codCapitol],[codRand],[col1],[col2],[col3],[col4],[col5],[col6],[col7],[col8],[gospodarieTip])
                                        SELECT "+idUnitate+",[gospodarieId],'"+ (Convert.ToInt32(HttpContext.Current.Session["SESan"]) + 1)+@"',[codCapitol],[codRand],
                                                            [col1],[col2],[col3],[col4],[col5],[col6],[col7],[col8],[gospodarieTip]
                                FROM capitoleCentralizate as capitoleCentralizate_1 
                                WHERE unitateId = " + idUnitate + " and an = " + HttpContext.Current.Session["SESAn"] + "";
        SqlCommand centralizatoareCommand = new SqlCommand(insert, connection);
        centralizatoareCommand.ExecuteNonQuery();
    }
    public static string InsertAnimaleDacaNuExistaMiscarePeSem2(int idUnitate, int idGospodarie,int idGospodarieNou,int codRand1,int codRand2)
    {

        string insert = @"INSERT into capitole ([unitateId]
      ,[gospodarieId]
      ,[an]
      ,[codCapitol]
      ,[codRand]
      ,[col1]
      ,[col2]
      ,[col3]
      ,[col4]
      ,[col5]
      ,[col6]
      ,[col7]
      ,[col8])
                                select " + idUnitate + "," + idGospodarieNou + ",'" + (Convert.ToInt32(HttpContext.Current.Session["SESan"]) + 1) + @"',
                                    [codCapitol],[codRand],[col2],0,[col3],[col4],[col5] ,[col6],[col7] ,[col8]
                                        FROM capitole as capitole_1
                    WHERE codRand >=" +codRand1+" AND codRand <="+codRand2+" and codCapitol = '7' and unitateId = '" + idUnitate + "' and gospodarieId = " + idGospodarie + @" 
                    and an = '" + HttpContext.Current.Session["SESan"] + "' ";
        return insert;
    }
    public static string InsertAnimaleDacaExistaMiscarePeSem2(int idUnitate, int idGospodarie, int idGospodarieNou,
       int codRand1, int codRand2, int suma)
    {
        string insert1 = @"INSERT into capitole ([unitateId]
      ,[gospodarieId]
      ,[an]
      ,[codCapitol]
      ,[codRand]
      ,[col1]
      ,[col2]
      ,[col3]
      ,[col4]
      ,[col5]
      ,[col6]
      ,[col7]
      ,[col8])
                                select " + idUnitate + "," + idGospodarieNou + ",'" + (Convert.ToInt32(HttpContext.Current.Session["SESan"]) + 1) + @"',
                                    [codCapitol],[codRand],[col2] + "+suma+@",0,[col3],[col4],[col5] ,[col6],[col7] ,[col8]
                                        FROM capitole as capitole_1
                    WHERE codRand = '" + codRand1 + "' and codCapitol = '7' and unitateId = '" + idUnitate + "' and gospodarieId = " + idGospodarie + @" 
                    and an = '" + HttpContext.Current.Session["SESan"] + "' ";
        string insert2 = @"INSERT into capitole ([unitateId]
      ,[gospodarieId]
      ,[an]
      ,[codCapitol]
      ,[codRand]
      ,[col1]
      ,[col2]
      ,[col3]
      ,[col4]
      ,[col5]
      ,[col6]
      ,[col7]
      ,[col8])
                                select " + idUnitate + "," + idGospodarieNou + ",'" + (Convert.ToInt32(HttpContext.Current.Session["SESan"]) + 1) + @"',
                                    [codCapitol],[codRand],[col2],0,[col3],[col4],[col5] ,[col6],[col7] ,[col8]
                                        FROM capitole as capitole_1
                    WHERE codRand >" + codRand1 + " AND codRand <=" + codRand2 + " and codCapitol = '7' and unitateId = '" + idUnitate + "' and gospodarieId = " + idGospodarie + @" 
                    and an = '" + HttpContext.Current.Session["SESan"] + "' ";
        return insert1 + insert2;
    }
    public static string InsertAnimaleDacaExistaMiscarePeSem2PentruAlbine(int idUnitate, int idGospodarie, int idGospodarieNou,
     int codRand1, int codRand2, int suma)
    {
        string insert = @"INSERT into capitole ([unitateId]
      ,[gospodarieId]
      ,[an]
      ,[codCapitol]
      ,[codRand]
      ,[col1]
      ,[col2]
      ,[col3]
      ,[col4]
      ,[col5]
      ,[col6]
      ,[col7]
      ,[col8])
                                select " + idUnitate + "," + idGospodarieNou + ",'" + (Convert.ToInt32(HttpContext.Current.Session["SESan"]) + 1) + @"',
                                    [codCapitol],[codRand],[col2] + " + suma + @",0,[col3],[col4],[col5] ,[col6],[col7] ,[col8]
                                        FROM capitole as capitole_1
                    WHERE codRand = '" + codRand1 + "' and codCapitol = '7' and unitateId = '" + idUnitate + "' and gospodarieId = " + idGospodarie + @" 
                    and an = '" + HttpContext.Current.Session["SESan"] + "' ";

        return insert;
    }
    public static string InsertAnimale(int idUnitate, int idGospodarie, int idGospodarieNou, SqlConnection connection)
    {

        string insert = @"INSERT into capitole ([unitateId]
      ,[gospodarieId]
      ,[an]
      ,[codCapitol]
      ,[codRand]
      ,[col1]
      ,[col2]
      ,[col3]
      ,[col4]
      ,[col5]
      ,[col6]
      ,[col7]
      ,[col8])
                                select " + idUnitate + "," + idGospodarieNou + ",'" + (Convert.ToInt32(HttpContext.Current.Session["SESan"]) + 1) + @"',
                                    [codCapitol],[codRand],[col2],0,[col3],[col4],[col5] ,[col6],[col7] ,[col8]
                                        FROM capitole as capitole_1
                    WHERE codCapitol = '7' and unitateId = '" + idUnitate + "' and gospodarieId = " + idGospodarie + @" 
                    and an = '" + HttpContext.Current.Session["SESan"] + "' ";
        return insert;
    }


    public static string InsertMentiuni(int idUnitate, int idGospodarie, int idGospodarieNou)
    {
        string insert = @"INSERT into [mentiuni]
           ([unitateId]
           ,[gospodarieId]
           ,[mentiuneText])
            SELECT " + idUnitate + "," + idGospodarieNou + @",[mentiuneText] 
                    FROM mentiuni as mentiuni_1
           where unitateId = " + idUnitate + " and gospodarieid = " + idGospodarie + "";
        return insert;

    }
    public static void InsertMentiuni2(int idUnitate, int idGospodarie, int idGospodarieNou)
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(HttpContext.Current.Session["SESan"]));
        string insert = @"INSERT into [mentiuni]
           ([unitateId]
           ,[gospodarieId]
           ,[mentiuneText])
            SELECT " + idUnitate + "," + idGospodarieNou + @",[mentiuneText] 
                    FROM mentiuni as mentiuni_1
           where unitateId = " + idUnitate + " and gospodarieid = " + idGospodarie + "";
        SqlCommand centralizatoareCommand = new SqlCommand(insert, connection);
        centralizatoareCommand.ExecuteNonQuery();
        ManipuleazaBD.InchideConexiune(connection);

    }
    public static DataTable GetListaUnitati()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(HttpContext.Current.Session["SESan"]));
        string interogare = @"SELECT unitateId, unitateDenumire
                              FROM unitati where unitateAnDeschis = '"+HttpContext.Current.Session["SESAn"]+"' order by unitateDenumire";

        SqlDataAdapter adapter = new SqlDataAdapter(interogare, connection);
        DataTable table = new DataTable();
        adapter.Fill(table);
        ManipuleazaBD.InchideConexiune(connection);
        return table;
    }
    public static string GetListaGospodarii(CheckBoxList unitatiCheckBoxList)
    {
        string interogare = @"SELECT *
                                FROM gospodarii
                                JOIN unitati ON unitati.unitateId = gospodarii.unitateId
                             WHERE  an = " + HttpContext.Current.Session["SESan"] + " AND unitati.unitateId IN (";
        for (int i = 0; i < unitatiCheckBoxList.Items.Count; i++)
        {
            if (unitatiCheckBoxList.Items[i].Selected)
            {
                interogare += unitatiCheckBoxList.Items[i].Value + ", ";
            }
        }
        return interogare = interogare.Remove(interogare.Length - 2) + ") ORDER BY unitateDenumire";
    }
    public static string GetListaUnitati(CheckBoxList unitatiCheckBoxList)
    {
        string interogare = @"SELECT *
                                FROM unitati
                             WHERE  unitateAnDeschis = " + HttpContext.Current.Session["SESan"] + " AND unitati.unitateId IN (";
        for (int i = 0; i < unitatiCheckBoxList.Items.Count; i++)
        {
            if (unitatiCheckBoxList.Items[i].Selected)
            {
                interogare += unitatiCheckBoxList.Items[i].Value + ", ";
            }
        }
        return interogare = interogare.Remove(interogare.Length - 2) + ") ORDER BY unitateDenumire";
    }
    public static void UpdateUnitateAni(int idUnitate, SqlConnection connectionInsert)
    {
        string updateUnitateAnDeschis = @"UPDATE [unitati] 
                                    SET unitateAnDeschis = '" + (Convert.ToInt32(HttpContext.Current.Session["SESan"]) + 1).ToString() + @"', 
                                    unitateAniInchisi =  unitateAniInchisi + '#" + (Convert.ToInt32(HttpContext.Current.Session["SESan"])).ToString() + "'  WHERE [unitateId] = '" + idUnitate + "'";
        SqlCommand updateUnitateAnDeschisCommand = new SqlCommand(updateUnitateAnDeschis, connectionInsert);
        updateUnitateAnDeschisCommand.ExecuteNonQuery();
    }

    public static void InsertAnInchis(int idUnitate, SqlConnection connectionInsert)
    {
        string insertInchidereAn = @"INSERT INTO [inchideriAn] ([unitateId], [utilizatorId], [an], [data]) 
                                                VALUES ('" + idUnitate + "', '" +HttpContext.Current.Session["SESutilizatorId"] + "', '" + HttpContext.Current.Session["SESan"] + "', convert(datetime,'" + DateTime.Now + "',104))";
        SqlCommand insertInchidereAnCommand = new SqlCommand(insertInchidereAn, connectionInsert);
        insertInchidereAnCommand.ExecuteNonQuery();
    }

    public static bool ExistaSabloaneCapitole()
    {
        int count = 0;
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(HttpContext.Current.Session["SESan"]));
        string interogareSabloaneCapitole = @"SELECT count(*)
                                                FROM sabloaneCapitole
                                                WHERE an = " + (Convert.ToInt32(HttpContext.Current.Session["SESan"]) + 1) + "";
        SqlCommand sabloaneCapitoleCommand = new SqlCommand(interogareSabloaneCapitole, connection);
        count =  Convert.ToInt32(sabloaneCapitoleCommand.ExecuteScalar());
        ManipuleazaBD.InchideConexiune(connection);
        return count > 0;
    }


    public static DataTable GetAdeverinteAn1(int idUnitate, int gospodarieIdNou, SqlConnection connectionSelect)
    {
        string interogareAdeverinte = @"SELECT gospodarieId,adeverintaID
                                            FROM  adeverinte 
                                            WHERE  year(data)='" + (Convert.ToInt32(HttpContext.Current.Session["SESan"]) -1) + "' and unitateId='" + idUnitate + "' and gospodarieId = '" + gospodarieIdNou + "'";
        SqlCommand adeverinteCommand = new SqlCommand(interogareAdeverinte, connectionSelect);
        SqlDataAdapter adeverinteAdapter = new SqlDataAdapter(adeverinteCommand);
        DataTable adeverinteDataTable = new DataTable();
        adeverinteAdapter.Fill(adeverinteDataTable);
        return adeverinteDataTable;
    }


    public static DataTable GetMesajeInvalidari(int idUnitate, int idGospodarie, SqlConnection connection)
    {
        string interogare = @"SELECT *
                                            FROM mesajeInvalidari
                                         WHERE gospodarieId = " + idGospodarie + " and an = " + HttpContext.Current.Session["SESAn"] + " and unitateId = '" + idUnitate + "'";

        SqlCommand mesajeCommand = new SqlCommand(interogare, connection);
        SqlDataAdapter mesajeAdapter = new SqlDataAdapter(mesajeCommand);
        DataTable mesajeDataTable = new DataTable();
        mesajeAdapter.Fill(mesajeDataTable);
        return mesajeDataTable;
    }
}