﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for TipuriAnimale
/// </summary>
[Serializable]
public class TipuriAnimale
{
	public TipuriAnimale()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public int Bovine { get; set; }
    public int Cabaline { get; set; }
    public int Porcine { get; set; }
    public int Ovine { get; set; }
    public int Albine { get; set; }
    public int Iepuri { get; set; }
    public int Pasari { get; set; }
}