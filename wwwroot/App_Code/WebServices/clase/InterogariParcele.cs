﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for InterogariParcele
/// </summary>
public class InterogariParcele
{
    public InterogariParcele()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public List<TipParcele> SelectListaParcele(string pCnp, string pCodFiscal, string pAn, bool pPJ)
    {
        List<TipParcele> vRezultat = new List<TipParcele>();
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(pAn));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        string vUnitateId = "0";
        vCmd.CommandText = "select top(1) unitateId from unitati where unitateCodFiscal='" + pCodFiscal + "'";
        try { vUnitateId = vCmd.ExecuteScalar().ToString(); }
        catch { }
        string vGospodarieId = "0";
        vCmd.CommandText = "select top(1) gospodarieId from membri where unitateId='" + vUnitateId + "' and cnp='" + pCnp + "' and an='" + pAn + "' ORDER BY gospodarieid DESC";
        // daca este pj iau id din gospodarii
        if (pPJ)
            vCmd.CommandText = "select top(1) gospodarieId from gospodarii where unitateId='" + vUnitateId + "' and an='" + pAn + "' and jCodFiscal='" + pCnp + "'";
        try { vGospodarieId = vCmd.ExecuteScalar().ToString(); }
        catch { }
        vCmd.CommandText = "SELECT parcelaCodRand, parcele.parcelaId, COALESCE(parcele.parcelaDenumire,'') as parcelaDenumire, COALESCE(parcele.parcelaCodRand,'') as parcelaCodRand, COALESCE(parcele.parcelaSuprafataIntravilanHa,'0') as parcelaSuprafataIntravilanHa, COALESCE(parcele.parcelaSuprafataIntravilanAri,'0') as parcelaSuprafataIntravilanAri, COALESCE(parcele.parcelaSuprafataExtravilanHa,'0') as parcelaSuprafataExtravilanHa, COALESCE(parcele.parcelaSuprafataExtravilanAri,'0') as parcelaSuprafataExtravilanAri, COALESCE(parcele.parcelaNrTopo,'') as parcelaNrTopo, COALESCE(parcele.parcelaCategorie,'') as parcelaCategorie, COALESCE(parcele.parcelaCF,'') as parcelaCF, COALESCE (sabloaneCapitole.denumire4, N' --- ') AS denumire4, COALESCE(parcele.parcelaNrBloc,'') as parcelaNrBloc, COALESCE(parcele.parcelaMentiuni,'') as parcelaMentiuni, COALESCE(parcele.an,'') as an, COALESCE(parcele.parcelaLocalitate,'') as parcelaLocalitate, COALESCE(parcele.parcelaAdresa,'') as parcelaAdresa, COALESCE(parcele.parcelaNrCadastralProvizoriu,'') as parcelaNrCadastralProvizoriu, COALESCE(parcele.parcelaNrCadastral,'') as parcelaNrCadastral, COALESCE(parcele.parcelaTarla,'') as parcelaTarla, COALESCE(parcele.parcelaTitluProprietate,'') as parcelaTitluProprietate, COALESCE(parcele.parcelaClasaBonitate,'0') as parcelaClasaBonitate, COALESCE(parcele.parcelaPunctaj,'0') as parcelaPunctaj FROM parcele INNER JOIN sabloaneCapitole ON parcele.parcelaCategorie = sabloaneCapitole.codRand WHERE (sabloaneCapitole.capitol = '2a') AND (parcele.gospodarieId = '" + vGospodarieId + "') AND (parcele.an = '" + pAn + "') AND (sabloaneCapitole.an = '" + pAn + "') ORDER BY parcele.parcelaCategorie, parcele.parcelaId DESC";
        SqlDataReader vTabel = vCmd.ExecuteReader();
        while (vTabel.Read())
        {
            TipParcele vTipParcele = new TipParcele()
            {
                parcelaId = Convert.ToInt64(vTabel["parcelaId"].ToString()),
                parcelaDenumire = vTabel["parcelaDenumire"].ToString(),
                an = Convert.ToInt32(vTabel["an"].ToString()),
                denumire4 = vTabel["denumire4"].ToString(),
                parcelaCF = vTabel["parcelaCF"].ToString(),
                parcelaClasaBonitate = vTabel["parcelaClasaBonitate"].ToString(),
                parcelaCodRand = vTabel["parcelaCodRand"].ToString(),
                parcelaLocalitate = vTabel["parcelaLocalitate"].ToString(),
                parcelaNrBloc = vTabel["parcelaNrBloc"].ToString(),
                parcelaNrCadastral = vTabel["parcelaNrCadastral"].ToString(),
                parcelaNrCadastralProvizoriu = vTabel["parcelaNrCadastralProvizoriu"].ToString(),
                parcelaNrTopo = vTabel["parcelaNrTopo"].ToString(),
                parcelaPunctaj = vTabel["parcelaPunctaj"].ToString(),
                parcelaSuprafataExtravilanAri = Convert.ToDecimal(vTabel["parcelaSuprafataExtravilanAri"].ToString()),
                parcelaSuprafataExtravilanHa = Convert.ToDecimal(vTabel["parcelaSuprafataExtravilanHa"].ToString()),
                parcelaSuprafataIntravilanAri = Convert.ToDecimal(vTabel["parcelaSuprafataIntravilanAri"].ToString()),
                parcelaSuprafataIntravilanHa = Convert.ToDecimal(vTabel["parcelaSuprafataIntravilanHa"].ToString()),
                parcelaTarla = vTabel["parcelaTarla"].ToString(),
                parcelaTitluProprietate = vTabel["parcelaTitluProprietate"].ToString(),
                parcelaAdresa = vTabel["parcelaAdresa"].ToString(),
                parcelaCategorie = vTabel["parcelaCategorie"].ToString()
            };
            vRezultat.Add(vTipParcele);
        }
        vTabel.Close();
        ManipuleazaBD.InchideConexiune(vCon);
        return vRezultat;
    }

    public  List<TipParcele> SelectListaParcele(List<string> pCnp, string pCodFiscal, string pAn)
    {

        List<int> gospodarieId = new List<int>();
        List<TipParcele> vRezultat = new List<TipParcele>();
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(pAn));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        string vUnitateId = "0";
        vCmd.CommandText = "select top(1) unitateId from unitati where unitateCodFiscal='" + pCodFiscal + "'";
        try { vUnitateId = vCmd.ExecuteScalar().ToString(); }
        catch { }
        vCmd.CommandText = "select gospodarieId from membri where unitateId='" + vUnitateId + "' and  CNP in ('" + pCnp + "') and an='" + pAn + "'";
        
        SqlDataReader reader = vCmd.ExecuteReader();
        while(reader.Read())
        {
            gospodarieId.Add(Convert.ToInt32(reader[0]));
        }
        reader.Close();


        vCmd.CommandText = "select  gospodarieId from gospodarii where unitateId='" + vUnitateId + "' and an='" + pAn + "' and jCodFiscal in ('" + pCnp + "')";
        SqlDataReader readerG = vCmd.ExecuteReader();
        while (reader.Read())
        {
            gospodarieId.Add(Convert.ToInt32(readerG[0]));
        }
        readerG.Close();

        vCmd.CommandText = @"SELECT membri.cnp as CNP, parcelaCodRand, parcele.parcelaId, COALESCE(parcele.parcelaDenumire,'') as parcelaDenumire, COALESCE(parcele.parcelaCodRand,'') as parcelaCodRand, COALESCE(parcele.parcelaSuprafataIntravilanHa,'0') as parcelaSuprafataIntravilanHa, COALESCE(parcele.parcelaSuprafataIntravilanAri,'0') as parcelaSuprafataIntravilanAri, COALESCE(parcele.parcelaSuprafataExtravilanHa,'0') as parcelaSuprafataExtravilanHa, COALESCE(parcele.parcelaSuprafataExtravilanAri,'0') as parcelaSuprafataExtravilanAri, COALESCE(parcele.parcelaNrTopo,'') as parcelaNrTopo, COALESCE(parcele.parcelaCategorie,'') as parcelaCategorie, COALESCE(parcele.parcelaCF,'') as parcelaCF, COALESCE (sabloaneCapitole.denumire4, N' --- ') AS denumire4, COALESCE(parcele.parcelaNrBloc,'') as parcelaNrBloc, COALESCE(parcele.parcelaMentiuni,'') as parcelaMentiuni, COALESCE(parcele.an,'') as an, COALESCE(parcele.parcelaLocalitate,'') as parcelaLocalitate, COALESCE(parcele.parcelaAdresa,'') as parcelaAdresa, COALESCE(parcele.parcelaNrCadastralProvizoriu,'') as parcelaNrCadastralProvizoriu, COALESCE(parcele.parcelaNrCadastral,'') as parcelaNrCadastral, COALESCE(parcele.parcelaTarla,'') as parcelaTarla, COALESCE(parcele.parcelaTitluProprietate,'') as parcelaTitluProprietate, COALESCE(parcele.parcelaClasaBonitate,'0') as parcelaClasaBonitate, COALESCE(parcele.parcelaPunctaj,'0') as parcelaPunctaj 
        FROM parcele
        INNER JOIN sabloaneCapitole ON parcele.parcelaCategorie = sabloaneCapitole.codRand 
        INNER JOIN membri on parcele.gospodarieId = membri.gospodarieId
        WHERE (sabloaneCapitole.capitol = '2a') AND (parcele.gospodarieId in ('" + gospodarieId+"')) AND (parcele.an = '" + pAn + "') AND (sabloaneCapitole.an = '" + pAn + @"') 
        ORDER BY parcele.parcelaCategorie, parcele.parcelaId DESC";
        SqlDataReader vTabel = vCmd.ExecuteReader();

        while (vTabel.Read())
        {
            TipParcele vTipParcele = new TipParcele()
            {
                parcelaId = Convert.ToInt64(vTabel["parcelaId"].ToString()),
                parcelaDenumire = vTabel["parcelaDenumire"].ToString(),
                an = Convert.ToInt32(vTabel["an"].ToString()),
                denumire4 = vTabel["denumire4"].ToString(),
                parcelaCF = vTabel["parcelaCF"].ToString(),
                parcelaClasaBonitate = vTabel["parcelaClasaBonitate"].ToString(),
                parcelaCodRand = vTabel["parcelaCodRand"].ToString(),
                parcelaLocalitate = vTabel["parcelaLocalitate"].ToString(),
                parcelaNrBloc = vTabel["parcelaNrBloc"].ToString(),
                parcelaNrCadastral = vTabel["parcelaNrCadastral"].ToString(),
                parcelaNrCadastralProvizoriu = vTabel["parcelaNrCadastralProvizoriu"].ToString(),
                parcelaNrTopo = vTabel["parcelaNrTopo"].ToString(),
                parcelaPunctaj = vTabel["parcelaPunctaj"].ToString(),
                parcelaSuprafataExtravilanAri = Convert.ToDecimal(vTabel["parcelaSuprafataExtravilanAri"].ToString()),
                parcelaSuprafataExtravilanHa = Convert.ToDecimal(vTabel["parcelaSuprafataExtravilanHa"].ToString()),
                parcelaSuprafataIntravilanAri = Convert.ToDecimal(vTabel["parcelaSuprafataIntravilanAri"].ToString()),
                parcelaSuprafataIntravilanHa = Convert.ToDecimal(vTabel["parcelaSuprafataIntravilanHa"].ToString()),
                parcelaTarla = vTabel["parcelaTarla"].ToString(),
                parcelaTitluProprietate = vTabel["parcelaTitluProprietate"].ToString(),
                parcelaAdresa = vTabel["parcelaAdresa"].ToString(),
                parcelaCategorie = vTabel["parcelaCategorie"].ToString(),
                CNP = vTabel["CNP"].ToString()
            };
            vRezultat.Add(vTipParcele);
        }
        vTabel.Close();
        ManipuleazaBD.InchideConexiune(vCon);
        return vRezultat;
    }
}