﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for InterogariUtilaje
/// </summary>
public class InterogariUtilaje
{
	public InterogariUtilaje()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public TipuriUtilaje SelectTipuriUtilaje(List<string> pGospodarieId, short an)
    {
        TipuriUtilaje vTipuriUtilaje = new TipuriUtilaje();

        SqlConnection vConn = ManipuleazaBD.CreareConexiune(an);
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vConn;

        foreach (string vGospodarieId in pGospodarieId)
        {
            vCmd.CommandText = "select coalesce(sum(col1),0) from capitole where gospodarieId='" + vGospodarieId + "' and codCapitol='9' and codRand in (1,2,3,4,5,6,7,32,33,34,36,39)";
            vTipuriUtilaje.Tractor +=Convert.ToInt32(vCmd.ExecuteScalar());

            vCmd.CommandText = "select coalesce(sum(col1),0) from capitole where gospodarieId='" + vGospodarieId + "' and codCapitol='9' and codRand in (59,60,61)";
            vTipuriUtilaje.Moara += Convert.ToInt32(vCmd.ExecuteScalar());

            vCmd.CommandText = "select coalesce(sum(col1),0) from capitole where gospodarieId='" + vGospodarieId + "' and codCapitol='9' and codRand in (45,46)";
            vTipuriUtilaje.Autovehicule += Convert.ToInt32(vCmd.ExecuteScalar());
        }

        ManipuleazaBD.InchideConexiune(vConn);
        return vTipuriUtilaje;

    }
}