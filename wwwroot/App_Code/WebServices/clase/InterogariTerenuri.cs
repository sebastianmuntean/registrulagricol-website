﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for InterogariTerenuri
/// </summary>
public class InterogariTerenuri
{
	public InterogariTerenuri()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public TipuriTerenuri SelectTipuriTerenuri(List<string> pGospodarieId, int pAn)
    {
        TipuriTerenuri vTipuriTerenuri = new TipuriTerenuri();

        SqlConnection vConn = ManipuleazaBD.CreareConexiune(Convert.ToInt16(pAn));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vConn;

        foreach (string vGospodarieId in pGospodarieId)
        {
            vCmd.CommandText = "SELECT parcelaCodRand, parcele.parcelaId, COALESCE(parcele.parcelaDenumire,'') as parcelaDenumire, COALESCE(parcele.parcelaCodRand,'') as parcelaCodRand, COALESCE(parcele.parcelaSuprafataIntravilanHa,'0') as parcelaSuprafataIntravilanHa, COALESCE(parcele.parcelaSuprafataIntravilanAri,'0') as parcelaSuprafataIntravilanAri, COALESCE(parcele.parcelaSuprafataExtravilanHa,'0') as parcelaSuprafataExtravilanHa, COALESCE(parcele.parcelaSuprafataExtravilanAri,'0') as parcelaSuprafataExtravilanAri, COALESCE(parcele.parcelaNrTopo,'') as parcelaNrTopo, COALESCE(parcele.parcelaCategorie,'') as parcelaCategorie, COALESCE(parcele.parcelaCF,'') as parcelaCF, COALESCE (sabloaneCapitole.denumire4, N' --- ') AS denumire4, COALESCE(parcele.parcelaNrBloc,'') as parcelaNrBloc, COALESCE(parcele.parcelaMentiuni,'') as parcelaMentiuni, COALESCE(parcele.an,'') as an, COALESCE(parcele.parcelaLocalitate,'') as parcelaLocalitate, COALESCE(parcele.parcelaAdresa,'') as parcelaAdresa, COALESCE(parcele.parcelaNrCadastralProvizoriu,'') as parcelaNrCadastralProvizoriu, COALESCE(parcele.parcelaNrCadastral,'') as parcelaNrCadastral, COALESCE(parcele.parcelaTarla,'') as parcelaTarla, COALESCE(parcele.parcelaTitluProprietate,'') as parcelaTitluProprietate, COALESCE(parcele.parcelaClasaBonitate,'0') as parcelaClasaBonitate, COALESCE(parcele.parcelaPunctaj,'0') as parcelaPunctaj FROM parcele INNER JOIN sabloaneCapitole ON parcele.parcelaCategorie = sabloaneCapitole.codRand WHERE (sabloaneCapitole.capitol = '2a') AND (parcele.gospodarieId ='" + vGospodarieId + "') AND (parcele.an = '" + pAn + "') AND (sabloaneCapitole.an ='" + pAn + "') ORDER BY parcele.parcelaId DESC";
            SqlDataReader vTable = vCmd.ExecuteReader();

            while (vTable.Read())
            {
                decimal vHectare = 0;
                //pt TerenConstructiiSiIntravilan adun suprafetele cu constructii din intravilan si extravilan si restul terenurilor din intravilan
                vTipuriTerenuri.TerenConstructiiSiIntravilan += Convert.ToDecimal(vTable["parcelaSuprafataIntravilanAri"]);
                if (vTable["parcelaDenumire"].ToString() == "CONSTRUCTII")
                {
                    try
                    {
                        vHectare = Convert.ToDecimal(vTable["parcelaSuprafataIntravilanHa"])*100;
                    }
                    catch
                    { }
                    vTipuriTerenuri.TerenConstructiiSiIntravilan += Convert.ToDecimal(vTable["parcelaSuprafataExtravilanAri"])+vHectare;
                }

                //pt TerenExtravilanProductiv adun suprafetele din extravilan, fara teren neproductiv si fara drum
                if (vTable["denumire4"].ToString() != "Drumuri si cai ferate" && vTable["denumire4"].ToString() != "Drumuri şi căi ferate" && vTable["denumire4"].ToString() != "Terenuri degradate si neproductive" && vTable["denumire4"].ToString() != "Terenuri degradate şi neproductive")
                {  try
                    {
                        vHectare = Convert.ToDecimal(vTable["parcelaSuprafataExtravilanHa"]) * 100;
                    }
                    catch
                    {}
                    vTipuriTerenuri.TerenExtravilanProductiv += Convert.ToDecimal(vTable["parcelaSuprafataExtravilanAri"])+vHectare;
                }
            }
            vTable.Close();

            vCmd.CommandText = "SELECT capitole.capitolId, capitole.unitateId, capitole.gospodarieId, capitole.an, capitole.codCapitol, capitole.codRand, convert(int,capitole.col1) as col1, capitole.col2, CONVERT (int, capitole.col3) AS col3, capitole.col4, CONVERT (int, capitole.col5) AS col5, capitole.col6, capitole.col7, capitole.col8, sabloaneCapitole.formula, sabloaneCapitole.denumire1, sabloaneCapitole.denumire2, sabloaneCapitole.denumire3, sabloaneCapitole.denumire4, sabloaneCapitole.denumire5 FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand WHERE  (capitole.an ='"+pAn+"') AND (capitole.gospodarieId ='"+vGospodarieId+"') AND (sabloaneCapitole.capitol = '11')  AND (sabloaneCapitole.an ='"+pAn+"') ORDER BY capitole.codRand";
            vTable = vCmd.ExecuteReader();
            while (vTable.Read())
            {
                if (vTable["codRand"].ToString() == "1" || vTable["codRand"].ToString() == "4" || vTable["codRand"].ToString() == "7" || vTable["codRand"].ToString() =="10" || vTable["codRand"].ToString() == "13" || vTable["codRand"].ToString() == "14" || vTable["codRand"].ToString() == "15" || vTable["codRand"].ToString() == "16" || vTable["codRand"].ToString() == "17" || vTable["codRand"].ToString() == "18" || vTable["codRand"].ToString() == "19")
                {
                    vTipuriTerenuri.TerenConstructiiInAfaraLocuinteiDeDomiciliu += Convert.ToDecimal(vTable["col1"]);
                }
            }
            vTable.Close();

            //transform vTipuriTerenuri.TerenConstructiiInAfaraLocuinteiDeDomiciliu din Mp in Ari
            vTipuriTerenuri.TerenConstructiiInAfaraLocuinteiDeDomiciliu = vTipuriTerenuri.TerenConstructiiInAfaraLocuinteiDeDomiciliu / 10;
        }
        ManipuleazaBD.InchideConexiune(vConn);
        return vTipuriTerenuri;
    }
}