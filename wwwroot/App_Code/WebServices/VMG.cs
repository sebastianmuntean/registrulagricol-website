﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;

/// <summary>
/// Summary description for VMG
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class VMG : System.Web.Services.WebService {

    public VMG () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    public HeaderAutentificare Autentificare { get; set; }

    [WebMethod]
    public string HelloWorld() 
    {
        return "Hello World";
    }

    [WebMethod(Description="suma parcelelor pt un cnp")]    
    [SoapHeader("Autentificare",Required=true)]
    public RaspunsSumaParceleVMG sumaParcele()
    {
        RaspunsSumaParceleVMG vRaspuns = new RaspunsSumaParceleVMG();      

        if (!autentificare(Autentificare.Parola))
        {
            vRaspuns.MesajEroare = "Parola Gresita!";
            vRaspuns.OperatieReusita = false;
            return vRaspuns;
        }

        return vRaspuns;        
    }

    private Boolean autentificare(string pParola)
    {
        if (pParola=="traxdata")
        {return true;}
        else
        return false;
    }

    [SoapHeader("Autentificare",Required=true)]
    [WebMethod (Description="Returneaza o lista cu numarul de animale, pe categorii")]
    public RaspunsAnimale Animale(List<string> pListaCNP, DateTime pDataCererii)
    {
        
        RaspunsAnimale vRaspuns = new RaspunsAnimale();
        List<string> vGospodarieId = new List<string>();
        vGospodarieId = formareListaGospodariiId(pListaCNP, pDataCererii.Year);

        if (!autentificare(Autentificare.Parola))
        {
            vRaspuns.MesajEroare = "Parola Gresita!";
            vRaspuns.OperatieReusita = false;
            return vRaspuns;
        } 

        InterogariAnimale vInterogariAnimale = new InterogariAnimale();

        try
        {
            vRaspuns.ListaAnimalePeTipuri = vInterogariAnimale.SelectTipuriAnimale(vGospodarieId, pDataCererii, Convert.ToInt16(HttpContext.Current.Session["SESan"]));
        }
        catch (Exception e)
        {
            vRaspuns.MesajEroare = e.Message;
            vRaspuns.OperatieReusita = false;
        }
        return vRaspuns;
    }

    private List<string> formareListaGospodariiId(List<string> pLista, int pAn)
    {
        List<string> vGospodarieID = new List<string>();
        SqlConnection vConn = ManipuleazaBD.CreareConexiune(Convert.ToInt16(pAn));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vConn;

        foreach (string vCnp in pLista)
        {
            vCmd.CommandText = "select distinct gospodarieId from membri where CNP='" + vCnp + "'and an='"+pAn+"'";
            SqlDataReader vTable=vCmd.ExecuteReader();
            while (vTable.Read())
            {
                vGospodarieID.Add(vTable["gospodarieId"].ToString());
            }
            vTable.Close();
        }
        ManipuleazaBD.InchideConexiune(vConn);
        return vGospodarieID;
    }

    [SoapHeader("Autentificare",Required=true)]
    [WebMethod(Description="Returneaza o lista cu numarul utilajelor, pe categorii")]
    public RaspunsUtilaje Utilaje(List<string> pListaCNP, DateTime pDataCererii)
    {
        RaspunsUtilaje vRaspuns =new RaspunsUtilaje();
        List<string> vGospodarieId = new List<string>();
        vGospodarieId = formareListaGospodariiId(pListaCNP, pDataCererii.Year);

        if (!autentificare(Autentificare.Parola))
        {
            vRaspuns.MesajEroare = "Parola Gresita!";
            vRaspuns.OperatieReusita = false;
            return vRaspuns;
        }

        InterogariUtilaje vInterogariUtilaje = new InterogariUtilaje();

        try
        {
            vRaspuns.ListaUtilajePeTipuri = vInterogariUtilaje.SelectTipuriUtilaje(vGospodarieId, Convert.ToInt16(HttpContext.Current.Session["SESan"]));
        }
        catch (Exception e)
        {
            vRaspuns.MesajEroare = e.Message;
            vRaspuns.OperatieReusita = false;
        }        

        return vRaspuns;
    }

    [SoapHeader("Autentificare", Required = true)]
    [WebMethod(Description = "Returneaza o lista cu suprafetele pe tipuri de teren")]
    public RaspunsTerenuri Terenuri(List<string> pListaCNP, DateTime pDataCererii)
    {
        RaspunsTerenuri vRaspuns = new RaspunsTerenuri();
        List<string> vGospodarieId = new List<string>();
        vGospodarieId = formareListaGospodariiId(pListaCNP, pDataCererii.Year);

        if (!autentificare(Autentificare.Parola))
        {
            vRaspuns.MesajEroare = "Parola Gresita!";
            vRaspuns.OperatieReusita = false;
            return vRaspuns;
        }

        InterogariTerenuri vInterogariTerenuri = new InterogariTerenuri();

        try
        {
            vRaspuns.ListaTerenuriPeTipuri = vInterogariTerenuri.SelectTipuriTerenuri(vGospodarieId, pDataCererii.Year);
        }
        catch (Exception e)
        {
            vRaspuns.MesajEroare = e.Message;
            vRaspuns.OperatieReusita = false;
        }

        return vRaspuns;
    }

}
