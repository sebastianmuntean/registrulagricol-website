﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;

/// <summary>
/// Functii ce opereaza cu sabloanele de drepturi
/// Creata la:                  26.01.2011
/// Autor:                      Sebastian Muntean
/// Ultima                      actualizare: 26.01.11
/// Autor:                      Sebastian Muntean
/// </summary>
public class clsSabloane
{
    public static int VerificaLinieSablon(string pInterogare)
    {
        int vExista = 0;
        string strConn = ConfigurationManager.ConnectionStrings["TNTRegistruAgricolConnectionString"].ConnectionString;
        SqlConnection con = new SqlConnection(strConn);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = pInterogare;
        con.Open();
        try
        {
            SqlDataReader vExistaSabloane = cmd.ExecuteReader();
            while (vExistaSabloane.Read())
            {
                if (Convert.ToInt32(vExistaSabloane["exista"]) > 0)
                {
                    vExista = 1;
                }
            }
        }
        catch { }
        return vExista;
    }

}
