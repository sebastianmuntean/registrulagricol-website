﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

public class SablonAdeverinta
{
	public SablonAdeverinta()
	{
	}

    private int id;
    public int Id
    {
        get { return id; }
        set { id = value; }
    }

    private int unitateId;
    public int UnitateId
    {
        get { return unitateId; }
        set { unitateId = value; }
    }

    private int tip;
    public int Tip
    {
        get { return tip; }
        set { tip = value; }
    }

    private int tipAdeverinta;
    public int TipAdeverinta
    {
        get { return tipAdeverinta; }
        set { tipAdeverinta = value; }
    }

    private string titlu = string.Empty;
    public string Titlu
    {
        get { return titlu; }
        set { titlu = value; }
    }

    private string text = string.Empty;
    public string Text
    {
        get { return text; }
        set { text = value; }
    }

    private int subsolId;
    public int SubsolId
    {
        get { return subsolId; }
        set { subsolId = value; }
    }

    private int antetId;
    public int AntetId
    {
        get { return antetId; }
        set { antetId = value; }
    }

    private string observatii = string.Empty;
    public string Observatii
    {
        get { return observatii; }
        set { observatii = value; }
    }

    private string denumire = string.Empty;
    public string Denumire
    {
        get { return denumire; }
        set { denumire = value; }
    }

}