﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Data.SqlClient;

/// <summary>
/// Functii de calcul capitole
/// Creata la:                  09.02.2011
/// Autor:                      Laza Tudor Mihai
/// Ultima                      actualizare: 09.02.2011
/// Autor:                      Laza Tudor Mihai
/// </summary>
public class CalculCapitole
{
    public CalculCapitole()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #region completeaza in capitoleCentralizate
    public static string[] vListaAni(string pSesAn)
    {
        // scoatem lista anilor din ciclul curent
        string vCiclu = ManipuleazaBD.fRezultaUnString("SELECT * FROM cicluri WHERE cicluAni LIKE '%" + pSesAn + "%'", "cicluAni", Convert.ToInt16(pSesAn));
        char[] vDelimitator = { '#' };
        string[] vAniDinCiclu = vCiclu.Split(vDelimitator, StringSplitOptions.RemoveEmptyEntries);
        return vAniDinCiclu;
    }
    public static List<List<string>> vListaValoriInitiale(string pGospodarieId, string pUnitateId, string pCodCapitol, short an)
    {
        
        // salvam valorile initiale in lista
        List<List<string>> vListaValoriInitiale = new List<List<string>> { };
        List<string> vCampuri = new List<string> { "capitolId", "codRand", "col1", "col2", "col3", "col4", "col5", "col6", "col7", "col8","col9","col10"};
        vListaValoriInitiale = ManipuleazaBD.fRezultaListaStringuri("SELECT * FROM capitole WHERE unitateId=" + pUnitateId + " AND gospodarieId=" + pGospodarieId + " AND codcapitol = '" + pCodCapitol + "' ", vCampuri, an);
        return vListaValoriInitiale;
    }
    public static string vInsertCapitoleCentralizate(string pGospodarieId, string pUnitateId, string pCodCapitol, string pAn, List<List<string>> pListaValoriInitiale)
    {
        string vMesaj = "S-a actualizat si in Centralizatoare!!!";
        // scriem si in capitoleCentralizate
        SqlConnection vConC = ManipuleazaBD.CreareConexiune(Convert.ToInt16(pAn));
        SqlTransaction vTranzC = ManipuleazaBD.DeschideTranzactie(vConC);
        SqlCommand vCmdC = new SqlCommand();
        vCmdC.Connection = vConC;
        vCmdC.Transaction = vTranzC;
        // tipul de gospodarie
            List<string> vCampuriTipuri = new List<string> { "persJuridica", "strainas" };
            List<List<string>> vGospodarieTipuri = ManipuleazaBD.fRezultaListaStringuri("SELECT  persJuridica, strainas FROM gospodarii WHERE (gospodarieId = " + pGospodarieId + ")", vCampuriTipuri, Convert.ToInt16(pAn));
            string vGospodarieTip = "0";
            switch (vGospodarieTipuri[0][0])
            {
                case "False":
                    if (vGospodarieTipuri[0][1] == "True") vGospodarieTip = "2";
                    else vGospodarieTip = "0";
                    break;
                case "True":
                    if (vGospodarieTipuri[0][1] == "True") vGospodarieTip = "3";
                    else vGospodarieTip = "1";
                    break;
            }
            // avem salvate datele initiale in listavaloriinitiale
            // citim din capitolecentralizate si scadem initialul si adaugam finalul
            List<string> vCampuri = new List<string> { "capitolId", "codRand", "col1", "col2", "col3", "col4", "col5", "col6", "col7", "col8" };
            List<List<string>> vListaCentralizata = ManipuleazaBD.fRezultaListaStringuri("SELECT * FROM capitoleCentralizate WHERE unitateId=" + pUnitateId + "  AND codcapitol = '" + pCodCapitol + "' AND gospodarieTip = '" + vGospodarieTip + "' AND an = " + pAn, vCampuri, Convert.ToInt16(pAn));
            // cititm din capitole ce s-a introdus mai devreme
            // salvam valorile initiale in lista
            List<List<string>> vListaValoriFinale = ManipuleazaBD.fRezultaListaStringuri("SELECT * FROM capitole WHERE unitateId=" + pUnitateId + " AND gospodarieId=" + pGospodarieId + " AND codcapitol = '" + pCodCapitol + "' ", vCampuri, Convert.ToInt16(pAn));
            // adunam cele 3 liste 
            List<List<string>> vListeDeUrcat = new List<List<string>>();
            int vPozitie = 0;
            if (vListaCentralizata.Count > 0)
            {
            try
            {
                foreach (List<string> vLista in vListaCentralizata)
                {
                    List<string> vListaDeUrcat = new List<string> { };
                    vListaDeUrcat.Add(vLista[0]);
                    vListaDeUrcat.Add(vLista[1]);
                    for (int i = 2; i <= 9; i++)
                        vListaDeUrcat.Add((Convert.ToDecimal(vLista[i]) - Convert.ToDecimal(pListaValoriInitiale[vPozitie][i]) + Convert.ToDecimal(vListaValoriFinale[vPozitie][i])).ToString());
                    vPozitie++;
                    vListeDeUrcat.Add(vListaDeUrcat);
                }
                string vInsert = "";
                // facem update in capitoleCentralizate 
                vPozitie = 0;
                foreach (List<string> vLista in vListeDeUrcat)
                {
                    vInsert += "UPDATE TOP(1) capitoleCentralizate SET ";
                    for (int i = 2; i <= 9; i++)
                        vInsert += " col" + (i - 1).ToString() + " = '" + vLista[i].Replace(',', '.') + "' ,";
                    vInsert = vInsert.Remove(vInsert.Length - 1) + " WHERE capitolId = " + vLista[0] + "; ";
                    vPozitie++;
                }
                vCmdC.CommandText = vInsert;
                vCmdC.ExecuteNonQuery();
                vTranzC.Commit();
            }
            catch
            {
                vMesaj = "Eroare la scriere in Centralizatoare!!!";
                vTranzC.Rollback();
            }
            finally
            {
                ManipuleazaBD.InchideConexiune(vConC);
            }
            }
            else
            {
                string vInsert = "";
                vCmdC.CommandText = "select count(*) from sabloaneCapitole WHERE (an = '" + pAn + "') AND (Capitol = '"+pCodCapitol+"')";
                int count = Convert.ToInt32(vCmdC.ExecuteScalar());
                for (int codRand = 1; codRand <= count; codRand++)
                {
                    vInsert += @"insert into capitoleCentralizate ([unitateId],[gospodarieId],[an] ,[codCapitol] ,[codRand] ,[col1] ,[col2],[col3]
                                                    ,[col4] ,[col5],[col6] ,[col7],[col8],[gospodarieTip])
                                        values(" + pUnitateId + "," + pGospodarieId + "," + pAn + ",'" + pCodCapitol + "','" + codRand + "','0','0','0','0','0','0','0','0'," + vGospodarieTip + ")";
                }
                vCmdC.CommandText = vInsert;
                vCmdC.ExecuteNonQuery();
                vTranzC.Commit();
            }
            ManipuleazaBD.InchideConexiune(vConC);
        
        return vMesaj;
    }
    #endregion

    public static Int16 PopulareGridView(List<Capitole> pCapitole, Page pPage, GridView pGridView, GridViewRowEventArgs pGridViewRow, Int16 pTabIndex, Int32 pCalculColoane)
    {
        Int16 vTabIndex = pTabIndex;
        Label vFormula = (Label)pGridViewRow.Row.FindControl("lblFormula");
        Label vCodRand = (Label)pGridViewRow.Row.FindControl("lblCodRand");
        TextBox vCol1 = (TextBox)pGridViewRow.Row.FindControl("tbCol1");
        TextBox vCol2 = (TextBox)pGridViewRow.Row.FindControl("tbCol2");
        TextBox vCol3 = (TextBox)pGridViewRow.Row.FindControl("tbCol3");
        TextBox vCol4 = (TextBox)pGridViewRow.Row.FindControl("tbCol4");
        TextBox vCol5 = (TextBox)pGridViewRow.Row.FindControl("tbCol5");
        TextBox vCol6 = (TextBox)pGridViewRow.Row.FindControl("tbCol6");
        TextBox vCol7 = (TextBox)pGridViewRow.Row.FindControl("tbCol7");
        TextBox vCol8 = (TextBox)pGridViewRow.Row.FindControl("tbCol8");
        TextBox vCol9 = (TextBox)pGridViewRow.Row.FindControl("tbCol9");
        TextBox vCol10 = (TextBox)pGridViewRow.Row.FindControl("tbCol10");
        // adaug la coloanele din gridview valorile din lista pCapitole
        if (pGridViewRow.Row.RowType == DataControlRowType.DataRow)
        {
            vTabIndex = PreluareDateLista(pCapitole, vTabIndex, vCodRand, vCol1, vCol2, vCol3, vCol4, vCol5, vCol6, vCol7, vCol8,vCol9,vCol10);
            // daca randul curent are formula pun textboxurile cu enabled=false
            vTabIndex = DisableTextBoxTotal(vTabIndex, vFormula, vCol1, vCol2, vCol3, vCol4, vCol5, vCol6, vCol7, vCol8, pCalculColoane);
        }
        //if (pGridViewRow.Row.RowIndex == pGridView.Rows.Count)
        //    ControlTabIndex.SetTabIndex(pPage, pGridView);
        return vTabIndex;
    }

    private static Int16 DisableTextBoxTotal(Int16 vTabIndex, Label vFormula, TextBox vCol1, TextBox vCol2, TextBox vCol3, TextBox vCol4, TextBox vCol5, TextBox vCol6, TextBox vCol7, TextBox vCol8, Int32 pCalculColoane)
    {
        if (vFormula.Text.IndexOf("+") != -1 || vFormula.Text.IndexOf("-") != -1)
        {
            if (vCol1 != null)
            {
                vCol1.Enabled = false;
                vCol1.TabIndex = 0;
                if (pCalculColoane != 1)
                    vTabIndex -= 1;
            }
            if (vCol2 != null)
            {
                vCol2.Enabled = false;
                vCol2.TabIndex = 0;
                if (pCalculColoane != 1)
                    vTabIndex -= 1;
            }
            if (vCol3 != null)
            {
                vCol3.Enabled = false;
                vCol3.TabIndex = 0;
                vTabIndex -= 1;
            }
            if (vCol4 != null)
            {
                vCol4.Enabled = false;
                vCol4.TabIndex = 0;
                vTabIndex -= 1;
            }
            if (vCol5 != null)
            {
                vCol5.Enabled = false;
                vCol5.TabIndex = 0;
                vTabIndex -= 1;
            }
            if (vCol6 != null)
            {
                vCol6.Enabled = false;
                vCol6.TabIndex = 0;
                vTabIndex -= 1;
            }
            if (vCol7 != null)
            {
                vCol7.Enabled = false;
                vCol7.TabIndex = 0;
                vTabIndex -= 1;
            }
            if (vCol8 != null)
            {
                vCol8.Enabled = false;
                vCol8.TabIndex = 0;
                vTabIndex -= 1;
            }
        }
        return vTabIndex;
    }

    private static Int16 PreluareDateLista(List<Capitole> pCapitole, Int16 vTabIndex, Label vCodRand, TextBox vCol1, TextBox vCol2, TextBox vCol3, TextBox vCol4, TextBox vCol5, TextBox vCol6, TextBox vCol7, TextBox vCol8, TextBox vCol9, TextBox vCol10)
    {
        if (vCol1 != null)
        {
          //  try
          //  {
                vCol1.Text = pCapitole[Convert.ToInt32(vCodRand.Text) - 1].Col1;
          //  }
          //  catch { vCol1.Text = "0"; }
            if (vCol1.Enabled == true)
            {
                vCol1.TabIndex = Convert.ToInt16(vTabIndex + 1);
                vTabIndex += 1;
            }
        }
        if (vCol2 != null)
        {
          //  try
          //  {
                vCol2.Text = pCapitole[Convert.ToInt32(vCodRand.Text) - 1].Col2;
         //   }
          //  catch { vCol2.Text = "0"; }
            if (vCol2.Enabled == true)
            {
                vCol2.TabIndex = Convert.ToInt16(vTabIndex + 1);
                vTabIndex += 1;
            }
        }
        if (vCol3 != null)
        {
            vCol3.Text = pCapitole[Convert.ToInt32(vCodRand.Text) - 1].Col3;
            if (vCol3.Enabled == true)
            {
                vCol3.TabIndex = Convert.ToInt16(vTabIndex + 1);
                vTabIndex += 1;
            }
        }
        if (vCol4 != null)
        {
            vCol4.Text = pCapitole[Convert.ToInt32(vCodRand.Text) - 1].Col4;
            if (vCol4.Enabled == true)
            {
                vCol4.TabIndex = Convert.ToInt16(vTabIndex + 1);
                vTabIndex += 1;
            }
        }
        if (vCol5 != null)
        {
            vCol5.Text = pCapitole[Convert.ToInt32(vCodRand.Text) - 1].Col5;
            if (vCol5.Enabled == true)
            {
                vCol5.TabIndex = Convert.ToInt16(vTabIndex + 1);
                vTabIndex += 1;
            }
        }
        if (vCol6 != null)
        {
            vCol6.Text = pCapitole[Convert.ToInt32(vCodRand.Text) - 1].Col6;
            if (vCol6.Enabled == true)
            {
                vCol6.TabIndex = Convert.ToInt16(vTabIndex + 1);
                vTabIndex += 1;
            }
        }
        if (vCol7 != null)
        {
            vCol7.Text = pCapitole[Convert.ToInt32(vCodRand.Text) - 1].Col7;
            if (vCol7.Enabled == true)
            {
                vCol7.TabIndex = Convert.ToInt16(vTabIndex + 1);
                vTabIndex += 1;
            }
        }
        if (vCol8 != null)
        {
            vCol8.Text = pCapitole[Convert.ToInt32(vCodRand.Text) - 1].Col8;
            if (vCol8.Enabled == true)
            {
                vCol8.TabIndex = Convert.ToInt16(vTabIndex + 1);
                vTabIndex += 1;
            }
        }
        if (vCol9 != null)
        {
            vCol9.Text = pCapitole[Convert.ToInt32(vCodRand.Text) - 1].Col9;
            if (vCol9.Enabled == true)
            {
                vCol9.TabIndex = Convert.ToInt16(vTabIndex + 1);
                vTabIndex += 1;
            }
        }
        if (vCol10 != null)
        {
            vCol10.Text = pCapitole[Convert.ToInt32(vCodRand.Text) - 1].Col10;
            if (vCol10.Enabled == true)
            {
                vCol10.TabIndex = Convert.ToInt16(vTabIndex + 1);
                vTabIndex += 1;
            }
        }
        return vTabIndex;
    }
    // pTipCol - daca 1 => coloana este de tip ari si se extrag hectarele
    public static List<Capitole> PopuleazaViewState(GridView pGridView, Int32 pTipCol2, Int32 pTipCol4, Int32 pTipCol6, Int32 pTipCol8, Int32 pCalculColoane)
    {
        List<Capitole> vCapitole = new List<Capitole>();
        vCapitole.Clear();
        // parcurg randurile din GridView si calculez formulele
        for (int i = 0; i < pGridView.Rows.Count; i++)
        {
            if (pGridView.Rows[i].RowType == DataControlRowType.DataRow)
            {
                Label vFormula = (Label)pGridView.Rows[i].FindControl("lblFormula");
                Label vCodRand = (Label)pGridView.Rows[i].FindControl("lblCodRand");
                Label vId = (Label)pGridView.Rows[i].FindControl("lblId");
                TextBox vCol1 = (TextBox)pGridView.Rows[i].FindControl("tbCol1");
                TextBox vCol2 = (TextBox)pGridView.Rows[i].FindControl("tbCol2");
                TextBox vCol3 = (TextBox)pGridView.Rows[i].FindControl("tbCol3");
                TextBox vCol4 = (TextBox)pGridView.Rows[i].FindControl("tbCol4");
                TextBox vCol5 = (TextBox)pGridView.Rows[i].FindControl("tbCol5");
                TextBox vCol6 = (TextBox)pGridView.Rows[i].FindControl("tbCol6");
                TextBox vCol7 = (TextBox)pGridView.Rows[i].FindControl("tbCol7");
                TextBox vCol8 = (TextBox)pGridView.Rows[i].FindControl("tbCol8");
                TextBox vCol9 = (TextBox)pGridView.Rows[i].FindControl("tbCol9");
                TextBox vCol10 = (TextBox)pGridView.Rows[i].FindControl("tbCol10");

                Capitole vCapitol = new Capitole();
                // verific daca valorile sunt numerice
                VerificareValoriNumerice(vCol1, vCol2, vCol3, vCol4, vCol5, vCol6, vCol7, vCol8);
                // variabile pentru totaluri
                decimal vTotCol1 = 0;
                decimal vTotCol2 = 0;
                decimal vTotCol3 = 0;
                decimal vTotCol4 = 0;
                decimal vTotCol5 = 0;
                decimal vTotCol6 = 0;
                decimal vTotCol7 = 0;
                decimal vTotCol8 = 0;
                string vTotCol9 = "";
                string vTotCol10 = "";
                // extrag valorile din coloane
                ExtrageValoriColoane(vCol1, vCol2, vCol3, vCol4, vCol5, vCol6, vCol7, vCol8,vCol9,vCol10, ref vTotCol1, ref vTotCol2, ref vTotCol3, ref vTotCol4, ref vTotCol5, ref vTotCol6, ref vTotCol7, ref vTotCol8, ref vTotCol9, ref vTotCol10);
                // calculez total rand
                if (pCalculColoane == 1)
                {
                    vTotCol1 = vTotCol3 + vTotCol5 + vTotCol7;
                    //if (vCol1 != null)
                    //    vCol1.Text = vTotCol1.ToString();
                    vTotCol2 = vTotCol4 + vTotCol6 + vTotCol8;
                    //if (vCol2 != null)
                    //    vCol2.Text = vTotCol2.ToString();
                }
                // daca randul curent este de tip formula incep calculul totalului
                if (vFormula.Text.IndexOf("+") != -1 || vFormula.Text.IndexOf("-") != -1)
                {
                    string vRand = "", vOperatie = "";
                    int vRandInt = 0;
                    vTotCol1 = 0; vTotCol2 = 0; vTotCol3 = 0; vTotCol4 = 0; vTotCol5 = 0; vTotCol6 = 0; vTotCol7 = 0; vTotCol8 = 0;
                    // parcurg formula
                    // daca gasesc + sau - calculez totalurile
                    for (int b = 0; b < vFormula.Text.Length; b++)
                    {
                        TextBox vColTemp1 = (TextBox)pGridView.Rows[i].FindControl("tbCol1");
                        TextBox vColTemp2 = (TextBox)pGridView.Rows[i].FindControl("tbCol2");
                        TextBox vColTemp3 = (TextBox)pGridView.Rows[i].FindControl("tbCol3");
                        TextBox vColTemp4 = (TextBox)pGridView.Rows[i].FindControl("tbCol4");
                        TextBox vColTemp5 = (TextBox)pGridView.Rows[i].FindControl("tbCol5");
                        TextBox vColTemp6 = (TextBox)pGridView.Rows[i].FindControl("tbCol6");
                        TextBox vColTemp7 = (TextBox)pGridView.Rows[i].FindControl("tbCol7");
                        TextBox vColTemp8 = (TextBox)pGridView.Rows[i].FindControl("tbCol8");
                        if (vColTemp1 != null)
                            vColTemp1.Text = "0";
                        if (vColTemp2 != null)
                            vColTemp2.Text = "0";
                        if (vColTemp3 != null)
                            vColTemp3.Text = "0";
                        if (vColTemp4 != null)
                            vColTemp4.Text = "0";
                        if (vColTemp5 != null)
                            vColTemp5.Text = "0";
                        if (vColTemp6 != null)
                            vColTemp6.Text = "0";
                        if (vColTemp7 != null)
                            vColTemp7.Text = "0";
                        if (vColTemp8 != null)
                            vColTemp8.Text = "0";
                        // extrag randul
                        if (vFormula.Text[b].ToString() != "+" && vFormula.Text[b].ToString() != "-")
                        {
                            vRand += vFormula.Text[b].ToString();
                        }
                        ExtrageValoriRand(pGridView, vFormula, ref vRand, ref vOperatie, ref vRandInt, b, ref vColTemp1, ref vColTemp2, ref vColTemp3, ref vColTemp4, ref vColTemp5, ref vColTemp6, ref vColTemp7, ref vColTemp8);
                        VerificaValoriDecimalTotale(vColTemp1, vColTemp2, vColTemp3, vColTemp4, vColTemp5, vColTemp6, vColTemp7, vColTemp8);
                        CalculTotalColoane(ref vTotCol1, ref vTotCol2, ref vTotCol3, ref vTotCol4, ref vTotCol5, ref vTotCol6, ref vTotCol7, ref vTotCol8, vOperatie, vColTemp1, vColTemp2, vColTemp3, vColTemp4, vColTemp5, vColTemp6, vColTemp7, vColTemp8);
                        if (vFormula.Text[b].ToString() == "+" || vFormula.Text[b].ToString() == "-")
                            vOperatie = vFormula.Text[b].ToString();
                    }
                }
                ExtrageHaDinAri(pTipCol2, pTipCol4, pTipCol6, pTipCol8, vCol2, vCol4, vCol6, vCol8, ref vTotCol1, ref vTotCol2, ref vTotCol3, ref vTotCol4, ref vTotCol5, ref vTotCol6, ref vTotCol7, ref vTotCol8);
                // salvez datele in lista vCapitole
                vCapitol.Col1 = vTotCol1.ToString();
                if (vCol1 != null)
                    vCol1.Text = vTotCol1.ToString();
                vCapitol.Col2 = vTotCol2.ToString();
                if (vCol2 != null)
                    vCol2.Text = vTotCol2.ToString();
                vCapitol.Col3 = vTotCol3.ToString();
                if (vCol3 != null)
                    vCol3.Text = vTotCol3.ToString();
                vCapitol.Col4 = vTotCol4.ToString();
                if (vCol4 != null)
                    vCol4.Text = vTotCol4.ToString();
                vCapitol.Col5 = vTotCol5.ToString();
                if (vCol5 != null)
                    vCol5.Text = vTotCol5.ToString();
                vCapitol.Col6 = vTotCol6.ToString();
                if (vCol6 != null)
                    vCol6.Text = vTotCol6.ToString();
                vCapitol.Col7 = vTotCol7.ToString();
                if (vCol7 != null)
                    vCol7.Text = vTotCol7.ToString();
                vCapitol.Col8 = vTotCol8.ToString();
                if (vCol8 != null)
                    vCol8.Text = vTotCol8.ToString();
                if (vCol9 != null )
                    vCol9.Text = vTotCol9.ToString();
                vCapitol.Col9 = vTotCol9.ToString();
                if (vCol10 != null)
                    vCol10.Text = vTotCol10.ToString();
                vCapitol.Col10 = vTotCol10.ToString();
                vCapitol.Id = Convert.ToInt64(vId.Text);
                vCapitole.Add(vCapitol);
            }
        }
        return vCapitole;
    }

    private static void VerificaValoriDecimalTotale(TextBox vColTemp1, TextBox vColTemp2, TextBox vColTemp3, TextBox vColTemp4, TextBox vColTemp5, TextBox vColTemp6, TextBox vColTemp7, TextBox vColTemp8)
    {
        try { if (vColTemp1 != null)Convert.ToDecimal(vColTemp1.Text); }
        catch
        {
            if (vColTemp1 != null)
            {
                vColTemp1.Text = "0,00";
                vColTemp1.ForeColor = Color.Red;
            }
        }
        try { if (vColTemp2 != null)Convert.ToDecimal(vColTemp2.Text); }
        catch
        {
            if (vColTemp2 != null)
            {
                vColTemp2.Text = "0,00";
                vColTemp2.ForeColor = Color.Red;
            }
        }
        try { if (vColTemp3 != null)Convert.ToDecimal(vColTemp3.Text); }
        catch
        {
            if (vColTemp3 != null)
            {
                vColTemp3.Text = "0,00";
                vColTemp3.ForeColor = Color.Red;
            }
        }
        try { if (vColTemp4 != null)Convert.ToDecimal(vColTemp4.Text); }
        catch
        {
            if (vColTemp4 != null)
            {
                vColTemp4.Text = "0,00";
                vColTemp4.ForeColor = Color.Red;
            }
        }
        try { if (vColTemp5 != null)Convert.ToDecimal(vColTemp5.Text); }
        catch
        {
            if (vColTemp5 != null)
            {
                vColTemp5.Text = "0,00";
                vColTemp5.ForeColor = Color.Red;
            }
        }
        try { if (vColTemp6 != null)Convert.ToDecimal(vColTemp6.Text); }
        catch
        {
            if (vColTemp6 != null)
            {
                vColTemp6.Text = "0,00";
                vColTemp6.ForeColor = Color.Red;
            }
        }
        try { if (vColTemp7 != null)Convert.ToDecimal(vColTemp7.Text); }
        catch
        {
            if (vColTemp7 != null)
            {
                vColTemp7.Text = "0,00";
                vColTemp7.ForeColor = Color.Red;
            }
        }
        try { if (vColTemp8 != null)Convert.ToDecimal(vColTemp8.Text); }
        catch
        {
            if (vColTemp8 != null)
            {
                vColTemp8.Text = "0,00";
                vColTemp8.ForeColor = Color.Red;
            }
        }
    }

    private static void VerificareValoriNumerice(TextBox vCol1, TextBox vCol2, TextBox vCol3, TextBox vCol4, TextBox vCol5, TextBox vCol6, TextBox vCol7, TextBox vCol8)
    {
        try { if (vCol1 != null)Convert.ToDecimal(vCol1.Text); }
        catch
        {
            if (vCol1 != null)
            {
                vCol1.Text = "0,00";
                vCol1.ForeColor = Color.Red;
            }
        }
        try { if (vCol2 != null)Convert.ToDecimal(vCol2.Text); }
        catch
        {
            if (vCol2 != null)
            {
                vCol2.Text = "0,00";
                vCol2.ForeColor = Color.Red;
            }
        }
        try { if (vCol3 != null)Convert.ToDecimal(vCol3.Text); }
        catch
        {
            if (vCol3 != null)
            {
                vCol3.Text = "0,00";
                vCol3.ForeColor = Color.Red;
            }
        }
        try { if (vCol4 != null)Convert.ToDecimal(vCol4.Text); }
        catch
        {
            if (vCol4 != null)
            {
                vCol4.Text = "0,00";
                vCol4.ForeColor = Color.Red;
            }
        }
        try { if (vCol5 != null)Convert.ToDecimal(vCol5.Text); }
        catch
        {
            if (vCol5 != null)
            {
                vCol5.Text = "0,00";
                vCol5.ForeColor = Color.Red;
            }
        }
        try { if (vCol6 != null)Convert.ToDecimal(vCol6.Text); }
        catch
        {
            if (vCol6 != null)
            {
                vCol6.Text = "0,00";
                vCol6.ForeColor = Color.Red;
            }
        }
        try { if (vCol7 != null)Convert.ToDecimal(vCol7.Text); }
        catch
        {
            if (vCol7 != null)
            {
                vCol7.Text = "0,00";
                vCol7.ForeColor = Color.Red;
            }
        }
        try { if (vCol8 != null)Convert.ToDecimal(vCol8.Text); }
        catch
        {
            if (vCol8 != null)
            {
                vCol8.Text = "0,00";
                vCol8.ForeColor = Color.Red;
            }
        }
    }

    private static void ExtrageValoriRand(GridView pGridView, Label vFormula, ref string vRand, ref string vOperatie, ref int vRandInt, int b, ref TextBox vColTemp1, ref TextBox vColTemp2, ref TextBox vColTemp3, ref TextBox vColTemp4, ref TextBox vColTemp5, ref TextBox vColTemp6, ref TextBox vColTemp7, ref TextBox vColTemp8)
    {
        // extrag valorile pentru rand
        //if (vRand != "")
        //{
        //    int rand = Convert.ToInt32(vRand);

        //    if (b + 1 == vFormula.Text.Length & rand > 0)
        //    {
        //        vRandInt = rand - 1;
           
        //        vColTemp1 = (TextBox)pGridView.Rows[vRandInt].FindControl("tbCol1");
        //        vColTemp2 = (TextBox)pGridView.Rows[vRandInt].FindControl("tbCol2");
        //        vColTemp3 = (TextBox)pGridView.Rows[vRandInt].FindControl("tbCol3");
        //        vColTemp4 = (TextBox)pGridView.Rows[vRandInt].FindControl("tbCol4");
        //        vColTemp5 = (TextBox)pGridView.Rows[vRandInt].FindControl("tbCol5");
        //        vColTemp6 = (TextBox)pGridView.Rows[vRandInt].FindControl("tbCol6");
        //        vColTemp7 = (TextBox)pGridView.Rows[vRandInt].FindControl("tbCol7");
        //        vColTemp8 = (TextBox)pGridView.Rows[vRandInt].FindControl("tbCol8");
        //    }
        //    else if ((vFormula.Text[b].ToString() == "+" || vFormula.Text[b].ToString() == "-"))
        //    {
        //        vRandInt = rand - 1;

        //        if (vOperatie != "" && vFormula.Text[b].ToString() != vOperatie)
        //        {
        //        }
        //        else
        //        {
        //            vOperatie = vFormula.Text[b].ToString();
        //        }

        //        vColTemp1 = (TextBox)pGridView.Rows[vRandInt].FindControl("tbCol1");
        //        vColTemp2 = (TextBox)pGridView.Rows[vRandInt].FindControl("tbCol2");
        //        vColTemp3 = (TextBox)pGridView.Rows[vRandInt].FindControl("tbCol3");
        //        vColTemp4 = (TextBox)pGridView.Rows[vRandInt].FindControl("tbCol4");
        //        vColTemp5 = (TextBox)pGridView.Rows[vRandInt].FindControl("tbCol5");
        //        vColTemp6 = (TextBox)pGridView.Rows[vRandInt].FindControl("tbCol6");
        //        vColTemp7 = (TextBox)pGridView.Rows[vRandInt].FindControl("tbCol7");
        //        vColTemp8 = (TextBox)pGridView.Rows[vRandInt].FindControl("tbCol8");
        //    }

        //    vRand = "";
        //}
        // extrag valorile pentru rand
        if (b + 1 == vFormula.Text.Length && vRand != "")
        {
            vRandInt = Convert.ToInt32(vRand) - 1;
            vRand = "";
            vColTemp1 = (TextBox)pGridView.Rows[vRandInt].FindControl("tbCol1");
            vColTemp2 = (TextBox)pGridView.Rows[vRandInt].FindControl("tbCol2");
            vColTemp3 = (TextBox)pGridView.Rows[vRandInt].FindControl("tbCol3");
            vColTemp4 = (TextBox)pGridView.Rows[vRandInt].FindControl("tbCol4");
            vColTemp5 = (TextBox)pGridView.Rows[vRandInt].FindControl("tbCol5");
            vColTemp6 = (TextBox)pGridView.Rows[vRandInt].FindControl("tbCol6");
            vColTemp7 = (TextBox)pGridView.Rows[vRandInt].FindControl("tbCol7");
            vColTemp8 = (TextBox)pGridView.Rows[vRandInt].FindControl("tbCol8");
        }
        else if (vRand != "" && (vFormula.Text[b].ToString() == "+" || vFormula.Text[b].ToString() == "-"))
        {
            vRandInt = Convert.ToInt32(vRand) - 1;
            vRand = "";
            if (vOperatie != "" && vFormula.Text[b].ToString() != vOperatie)
            { }
            else
                vOperatie = vFormula.Text[b].ToString();
            vColTemp1 = (TextBox)pGridView.Rows[vRandInt].FindControl("tbCol1");
            vColTemp2 = (TextBox)pGridView.Rows[vRandInt].FindControl("tbCol2");
            vColTemp3 = (TextBox)pGridView.Rows[vRandInt].FindControl("tbCol3");
            vColTemp4 = (TextBox)pGridView.Rows[vRandInt].FindControl("tbCol4");
            vColTemp5 = (TextBox)pGridView.Rows[vRandInt].FindControl("tbCol5");
            vColTemp6 = (TextBox)pGridView.Rows[vRandInt].FindControl("tbCol6");
            vColTemp7 = (TextBox)pGridView.Rows[vRandInt].FindControl("tbCol7");
            vColTemp8 = (TextBox)pGridView.Rows[vRandInt].FindControl("tbCol8");
        }
    }

    private static void ExtrageHaDinAri(Int32 pTipCol2, Int32 pTipCol4, Int32 pTipCol6, Int32 pTipCol8, TextBox vCol2, TextBox vCol4, TextBox vCol6, TextBox vCol8, ref decimal vTotCol1, ref decimal vTotCol2, ref decimal vTotCol3, ref decimal vTotCol4, ref decimal vTotCol5, ref decimal vTotCol6, ref decimal vTotCol7, ref decimal vTotCol8)
    {
        // daca pTipCol este 1 extrag hectarele din ari
        if (vCol2 != null && pTipCol2 == 1)
        {
            vTotCol1 += Math.Floor(vTotCol2 / 100);
            vTotCol2 -= Math.Floor(vTotCol2 / 100) * 100;
        }
        if (vCol4 != null && pTipCol4 == 1)
        {
            vTotCol3 += Math.Floor(vTotCol4 / 100);
            vTotCol4 -= Math.Floor(vTotCol4 / 100) * 100;
        }
        if (vCol6 != null && pTipCol6 == 1)
        {
            vTotCol5 += Math.Floor(vTotCol6 / 100);
            vTotCol6 -= Math.Floor(vTotCol6 / 100) * 100;
        }
        if (vCol8 != null && pTipCol8 == 1)
        {
            vTotCol7 += Math.Floor(vTotCol8 / 100);
            vTotCol8 -= Math.Floor(vTotCol8 / 100) * 100;
        }
    }

    private static void CalculTotalColoane(ref decimal vTotCol1, ref decimal vTotCol2, ref decimal vTotCol3, ref decimal vTotCol4, ref decimal vTotCol5, ref decimal vTotCol6, ref decimal vTotCol7, ref decimal vTotCol8, string vOperatie, TextBox vColTemp1, TextBox vColTemp2, TextBox vColTemp3, TextBox vColTemp4, TextBox vColTemp5, TextBox vColTemp6, TextBox vColTemp7, TextBox vColTemp8)
    {
        // daca operatia este adunare adaug valorile la totaluri
        if (vOperatie == "+")
        {
            if (vColTemp1 != null)
                vTotCol1 += Convert.ToDecimal(vColTemp1.Text);
            if (vColTemp2 != null)
                vTotCol2 += Convert.ToDecimal(vColTemp2.Text);
            if (vColTemp3 != null)
                vTotCol3 += Convert.ToDecimal(vColTemp3.Text);
            if (vColTemp4 != null)
                vTotCol4 += Convert.ToDecimal(vColTemp4.Text);
            if (vColTemp5 != null)
                vTotCol5 += Convert.ToDecimal(vColTemp5.Text);
            if (vColTemp6 != null)
                vTotCol6 += Convert.ToDecimal(vColTemp6.Text);
            if (vColTemp7 != null)
                vTotCol7 += Convert.ToDecimal(vColTemp7.Text);
            if (vColTemp8 != null)
                vTotCol8 += Convert.ToDecimal(vColTemp8.Text);
        }
        // daca operatia este scadere scad din valorile de la total
        else if (vOperatie == "-")
        {
            if (vColTemp1 != null)
                vTotCol1 -= Convert.ToDecimal(vColTemp1.Text);
            if (vColTemp2 != null)
                vTotCol2 -= Convert.ToDecimal(vColTemp2.Text);
            if (vColTemp3 != null)
                vTotCol3 -= Convert.ToDecimal(vColTemp3.Text);
            if (vColTemp4 != null)
                vTotCol4 -= Convert.ToDecimal(vColTemp4.Text);
            if (vColTemp5 != null)
                vTotCol5 -= Convert.ToDecimal(vColTemp5.Text);
            if (vColTemp6 != null)
                vTotCol6 -= Convert.ToDecimal(vColTemp6.Text);
            if (vColTemp7 != null)
                vTotCol7 -= Convert.ToDecimal(vColTemp7.Text);
            if (vColTemp8 != null)
                vTotCol8 -= Convert.ToDecimal(vColTemp8.Text);
        }
    }

    private static void ExtrageValoriColoane(TextBox vCol1, TextBox vCol2, TextBox vCol3, TextBox vCol4, TextBox vCol5, TextBox vCol6, TextBox vCol7, TextBox vCol8, TextBox vCol9, TextBox vCol10, ref decimal vTotCol1, ref decimal vTotCol2, ref decimal vTotCol3, ref decimal vTotCol4, ref decimal vTotCol5, ref decimal vTotCol6, ref decimal vTotCol7, ref decimal vTotCol8, ref string vTotCol9, ref string vTotCol10)
    {
        if (vCol1 != null)
            vTotCol1 = Convert.ToDecimal(vCol1.Text);
        if (vCol2 != null)
            vTotCol2 = Convert.ToDecimal(vCol2.Text);
        if (vCol3 != null)
            vTotCol3 = Convert.ToDecimal(vCol3.Text);
        if (vCol4 != null)
            vTotCol4 = Convert.ToDecimal(vCol4.Text);
        if (vCol5 != null)
            vTotCol5 = Convert.ToDecimal(vCol5.Text);
        if (vCol6 != null)
            vTotCol6 = Convert.ToDecimal(vCol6.Text);
        if (vCol7 != null)
            vTotCol7 = Convert.ToDecimal(vCol7.Text);
        if (vCol8 != null)
            vTotCol8 = Convert.ToDecimal(vCol8.Text);
        if (vCol9 != null)
            vTotCol9 = vCol9.Text;
        if (vCol10 != null)
            vTotCol10 = vCol10.Text;
    }
    public static void PopuleazaTabelaCapitole(Int32 pAn, Int64 pGospodarieId,Int64 pUnitateId, String pCodCapitol)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(pAn));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        SqlCommand vCmd1 = new SqlCommand();
        vCmd1.Connection = vCon;
        // verific daca sunt introduse deja inregistrari pentru gospodaria curenta la capitolul curent
        if (pGospodarieId != -1)
            vCmd.CommandText = "select count(*) from capitole where an='" + pAn.ToString() + "' and gospodarieId='" + pGospodarieId.ToString() + "' and codCapitol='" + pCodCapitol.ToString() + "'";
        else vCmd.CommandText = "select count(*) from capitole where an='" + pAn.ToString() + "' and gospodarieId='" + pGospodarieId.ToString() + "' and codCapitol='" + pCodCapitol.ToString() + "' and unitateId='" + pUnitateId.ToString() + "'";
        int vCountCapitol = Convert.ToInt32(vCmd.ExecuteScalar());
        if (vCountCapitol == 0)
        {
            vCmd1.CommandText = "select * from sabloaneCapitole where an='" + pAn.ToString() + "' and capitol='" + pCodCapitol + "'";
            SqlDataReader vTabelSabloane = vCmd1.ExecuteReader();

            vCmd.CommandText = string.Empty;
            
            while (vTabelSabloane.Read())
            {
                vCmd.CommandText += @"INSERT INTO [capitole] ([unitateId], [gospodarieId], [an], [codCapitol], [codRand], [col1], [col2], [col3], [col4], [col5], [col6], [col7], [col8]) VALUES ('" + pUnitateId.ToString() + "','" + pGospodarieId.ToString() + "','" + pAn.ToString() + "','" + vTabelSabloane["capitol"].ToString() + "', '" + vTabelSabloane["codRand"].ToString() + "', '0', '0', '0', '0', '0', '0', '0', '0')";
                
            }
            if ( vCmd.CommandText != string.Empty )
            {
                vCmd.ExecuteNonQuery();
            }
            vTabelSabloane.Close();
        }

        if (pGospodarieId != -1 && pCodCapitol.ToString() == "8")
        {
            vCmd.CommandText = "select count(sablonCapitoleId) from sabloaneCapitole where an='" + pAn.ToString() + "' and capitol='" + pCodCapitol.ToString() + "'";
          
            if( Convert.ToInt32(vCmd.ExecuteScalar()) > vCountCapitol && vCountCapitol > 0 )
            {
                vCmd1.CommandText = "select * from sabloaneCapitole where an='" + pAn.ToString() + "' and capitol='" + pCodCapitol + "' and codRand > 79";
                SqlDataReader vTabelSabloane = vCmd1.ExecuteReader();
             
                vCmd.CommandText = string.Empty;
                
                if ( pAn==2015 )
                {
                    while ( vTabelSabloane.Read() )
                    {
                        vCmd.CommandText += @"INSERT INTO [capitole] ([unitateId], [gospodarieId], [an], [codCapitol], [codRand], [col1], [col2], [col3], [col4], [col5], [col6], [col7], [col8]) VALUES ('" + pUnitateId.ToString() + "','" + pGospodarieId.ToString() + "','" + pAn.ToString() + "','" + 8 + "', '" + vTabelSabloane["codRand"].ToString() + "', '0', '0', '0', '0', '0', '0', '0', '0');";
                    }
                }
                else 
                {
                    if ( pAn > 2015 )
                    {
                        for ( int i = 2; i <= 85; i++ )
                        { 
                            if ( i == 10 || i == 18 || i == 27 || i == 36 || i == 45 || i == 54 || i == 63 || i == 72 || i == 80 )
                            {
                                continue;
                            }

                            vCmd.CommandText += @"INSERT INTO [capitole] ([unitateId], [gospodarieId], [an], [codCapitol], [codRand], [col1], [col2], [col3], [col4], [col5], [col6], [col7], [col8]) VALUES ('" + pUnitateId.ToString() + "','" + pGospodarieId.ToString() + "','" + pAn.ToString() + "','" + 8 + "', " + i + ", '0', '0', '0', '0', '0', '0', '0', '0');";
                        }
                    }
                }

                if ( vCmd.CommandText != string.Empty )
                {
                   try
                   {
                       vCmd.ExecuteNonQuery();
                   }
                    catch
                    {
                        
                    }
                }

              //  vTabelSabloane.Close();
            }
        }

        if (pGospodarieId != -1 && pCodCapitol.ToString() == "7" )
        {
            vCmd.CommandText = "select count(sablonCapitoleId) from sabloaneCapitole where an='" + pAn.ToString() + "' and capitol='" + pCodCapitol.ToString() + "'";

            if (Convert.ToInt32(vCmd.ExecuteScalar()) > vCountCapitol && vCountCapitol > 0)
            {
                vCmd1.CommandText = "select * from sabloaneCapitole where an='" + pAn.ToString() + "' and capitol='" + pCodCapitol + "' and codRand > 78";
                SqlDataReader vTabelSabloane = vCmd1.ExecuteReader();

                vCmd.CommandText = string.Empty;

                while (vTabelSabloane.Read())
                {
                    vCmd.CommandText += @"INSERT INTO [capitole] ([unitateId], [gospodarieId], [an], [codCapitol], [codRand], [col1], [col2], [col3], [col4], [col5], [col6], [col7], [col8]) VALUES ('" + pUnitateId.ToString() + "','" + pGospodarieId.ToString() + "','" + pAn.ToString() + "','" + vTabelSabloane["capitol"].ToString() + "', '" + vTabelSabloane["codRand"].ToString() + "', '0', '0', '0', '0', '0', '0', '0', '0');";
                   
                }

                if(vCmd.CommandText != string.Empty)
                {
                    vCmd.ExecuteNonQuery();
                }

                vTabelSabloane.Close();
            }
        }

        ManipuleazaBD.InchideConexiune(vCon);
    }

    public static void AdaugaCapitol2ADin2B(String pUnitateId, String pAn, String pGospodarieId, string vC3)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(pAn));
        SqlTransaction vTranz = ManipuleazaBD.DeschideTranzactie(vCon);
        SqlCommand vCmd1 = new SqlCommand();
        SqlCommand vCmd2 = new SqlCommand();

        vCmd1.Connection = vCon;
        vCmd1.Transaction = vTranz;
        vCmd2.Connection = vCon;
        vCmd2.Transaction = vTranz;
        
        try
        {
            // daca nu am inregistrari in C2A adaug inregistrari cu 0 daca nu le modific coloanele cu 0
            vCmd1.CommandText = "select count(*) from capitole WHERE (unitateId = '" + pUnitateId + "') AND (an = '" + pAn + "') AND (gospodarieId = '" + pGospodarieId + "') AND (codCapitol = '2a')";
      
            if (Convert.ToInt32(vCmd1.ExecuteScalar()) == 0)
            {
                // parcurg sabloane capitole pentru C2A
                vCmd2.CommandText = "SELECT capitol, codRand FROM sabloaneCapitole WHERE an = '" + pAn + "' and capitol='2a' ORDER BY codRand";
                SqlDataReader vSabloane = vCmd2.ExecuteReader();
              
                while (vSabloane.Read())
                {
                    vCmd1.CommandText = "INSERT INTO capitole (unitateId, gospodarieId, an, codCapitol, codRand, col1, col2, col3, col4, col5, col6, col7, col8) VALUES ('" + pUnitateId + "', '" + pGospodarieId + "', '" + pAn + "', '" + vSabloane["capitol"].ToString() + "', '" + vSabloane["codRand"].ToString() + "', '0', '0', '0', '0', '0', '0', '0', '0')";
                 
                    vCmd1.ExecuteNonQuery();
                }
                vSabloane.Close();
            }
            else
            {
                // modific datele din C2A si le pun sumele 0
                vCmd1.CommandText = "UPDATE capitole SET col1 = '0', col2 = '0', col3 = '0', col4 = '0', col5 = '0', col6 = '0', col7 = '0', col8 = '0' WHERE (unitateId = '" + pUnitateId + "') AND (an = '" + pAn + "') AND (gospodarieId = '" + pGospodarieId + "') AND (codCapitol = '2a')";
            
                vCmd1.ExecuteNonQuery();
            }

            // daca nu am inregistrari in C3 adaug inregistrari cu 0
            vCmd1.CommandText = "select count(*) from capitole WHERE (unitateId = '" + pUnitateId + "') AND (an = '" + pAn + "') AND (gospodarieId = '" + pGospodarieId + "') AND (codCapitol = '3')";
        
            if (Convert.ToInt32(vCmd1.ExecuteScalar()) == 0)
            {
                // parcurg sabloane capitole pentru C2A
                vCmd2.CommandText = "SELECT capitol, codRand FROM sabloaneCapitole WHERE an = '" + pAn + "' and capitol='3' ORDER BY codRand";
                SqlDataReader vSabloane = vCmd2.ExecuteReader();
                while (vSabloane.Read())
                {
                    vCmd1.CommandText = "INSERT INTO capitole (unitateId, gospodarieId, an, codCapitol, codRand, col1, col2, col3, col4, col5, col6, col7, col8) VALUES ('" + pUnitateId + "', '" + pGospodarieId + "', '" + pAn + "', '" + vSabloane["capitol"].ToString() + "', '" + vSabloane["codRand"].ToString() + "', '0', '0', '0', '0', '0', '0', '0', '0')";
                    vCmd1.ExecuteNonQuery();
                }
                vSabloane.Close();
            }

            // fac o lista care contine codurile de rand si formulele pentru C2A
            // la coloane pun 0
            vCmd2.CommandText = "SELECT sabloaneCapitole.formula, sabloaneCapitole.capitol, capitole.capitolId FROM capitole INNER JOIN sabloaneCapitole ON capitole.codCapitol = sabloaneCapitole.capitol AND capitole.codRand = sabloaneCapitole.codRand where sabloaneCapitole.an='" + pAn + "' and capitol='2a'  and gospodarieId = '"+pGospodarieId+"' order by capitole.codRand";
          
            SqlDataReader vTabel = vCmd2.ExecuteReader();
            List<Capitole> vCapitole = new List<Capitole>();

            while (vTabel.Read())
            {
                Capitole vRandCapitole = new Capitole();
                vRandCapitole.Col1 = "0";
                vRandCapitole.Col2 = "0";
                vRandCapitole.Col3 = "0";
                vRandCapitole.Col4 = "0";
                vRandCapitole.Col5 = "0";
                vRandCapitole.Col6 = "0";
                vRandCapitole.Col7 = "0";
                vRandCapitole.Col8 = "0";
                vRandCapitole.Formula = vTabel["formula"].ToString();
                vRandCapitole.Id = Convert.ToInt64(vTabel["capitolId"].ToString());
                vCapitole.Add(vRandCapitole);
            }

            vTabel.Close();
            // memorez o variabila cu localitatea din unitati
//            vCmd1.CommandText = "SELECT top(1) localitati.localitateDenumire FROM unitati INNER JOIN localitati ON unitati.localitateId = localitati.localitateId where unitateId='" + pUnitateId + "'";
         
            vCmd1.CommandText = "SELECT localitate FROM gospodarii WHERE (an = " + pAn + ") AND (gospodarieId = " + pGospodarieId + ")";
        
            String vLocalitateDomiciliu = vCmd1.ExecuteScalar().ToString();
          
            // parcurg parcelele si pun sumele in lista
            vCmd2.CommandText = "select parcelaCategorie, parcelaSuprafataIntravilanHa, parcelaSuprafataIntravilanAri, parcelaSuprafataExtravilanHa, parcelaSuprafataExtravilanAri, parcelaLocalitate from parcele where gospodarieId='" + pGospodarieId + "' and an='" + pAn + "'";
           
            vTabel = vCmd2.ExecuteReader();
          
            while (vTabel.Read())
            {
                // variabila pentru randul din lista
                int vRand = 0;
                try
                {
                    vRand = Convert.ToInt32(vTabel["parcelaCategorie"].ToString()) - 1;
                }
                catch { }
                
                // variabile pentru memorare date in lista
                decimal vCol1 = 0, vCol2 = 0, vCol3 = 0, vCol4 = 0, vCol5 = 0, vCol6 = 0;
                vCol1 = Convert.ToDecimal(vTabel["parcelaSuprafataIntravilanHa"].ToString()) + Convert.ToDecimal(vTabel["parcelaSuprafataExtravilanHa"].ToString());
                vCol2 = Convert.ToDecimal(vTabel["parcelaSuprafataIntravilanAri"].ToString()) + Convert.ToDecimal(vTabel["parcelaSuprafataExtravilanAri"].ToString());
               
                // verific daca localitatea este de domiciliu
                string localitateaParcelei = vTabel["parcelaLocalitate"].ToString().ToUpper().Replace("Ş", "S").Replace("Ţ", "T").Replace("Â", "A").Replace("Î", "I").Replace("Ș", "S").Replace("Ț", "T").Replace("Â", "A").Replace("Î", "I").Replace("Ă", "A");
                string localitateaDeDomiciliu = vLocalitateDomiciliu.ToUpper().Replace("Ş", "S").Replace("Ţ", "T").Replace("Â", "A").Replace("Î", "I").Replace("Ș", "S").Replace("Ț", "T").Replace("Â", "A").Replace("Î", "I").Replace("Ă", "A");

                if (localitateaParcelei == localitateaDeDomiciliu)
                {
                    vCol3 = Convert.ToDecimal(vTabel["parcelaSuprafataIntravilanHa"].ToString()) + Convert.ToDecimal(vTabel["parcelaSuprafataExtravilanHa"].ToString());
                    vCol4 = Convert.ToDecimal(vTabel["parcelaSuprafataIntravilanAri"].ToString()) + Convert.ToDecimal(vTabel["parcelaSuprafataExtravilanAri"].ToString());
                }
                else
                {
                    vCol5 = Convert.ToDecimal(vTabel["parcelaSuprafataIntravilanHa"].ToString()) + Convert.ToDecimal(vTabel["parcelaSuprafataExtravilanHa"].ToString());
                    vCol6 = Convert.ToDecimal(vTabel["parcelaSuprafataIntravilanAri"].ToString()) + Convert.ToDecimal(vTabel["parcelaSuprafataExtravilanAri"].ToString());
                }

                // memorez datele in lista
                vCapitole[vRand].Col1 = Convert.ToString(Convert.ToDecimal(vCapitole[vRand].Col1) + vCol1);
                vCapitole[vRand].Col2 = Convert.ToString(Convert.ToDecimal(vCapitole[vRand].Col2) + vCol2);
                vCapitole[vRand].Col3 = Convert.ToString(Convert.ToDecimal(vCapitole[vRand].Col3) + vCol3);
                vCapitole[vRand].Col4 = Convert.ToString(Convert.ToDecimal(vCapitole[vRand].Col4) + vCol4);
                vCapitole[vRand].Col5 = Convert.ToString(Convert.ToDecimal(vCapitole[vRand].Col5) + vCol5);
                vCapitole[vRand].Col6 = Convert.ToString(Convert.ToDecimal(vCapitole[vRand].Col6) + vCol6);

                // extrag ha din ari
                vCapitole[vRand].Col1 = Convert.ToString(Convert.ToDecimal(vCapitole[vRand].Col1) + Math.Floor(Convert.ToDecimal(vCapitole[vRand].Col2) / 100));
                vCapitole[vRand].Col2 = Convert.ToString(Convert.ToDecimal(vCapitole[vRand].Col2) - Math.Floor(Convert.ToDecimal(vCapitole[vRand].Col2) / 100) * 100);
                vCapitole[vRand].Col3 = Convert.ToString(Convert.ToDecimal(vCapitole[vRand].Col3) + Math.Floor(Convert.ToDecimal(vCapitole[vRand].Col4) / 100));
                vCapitole[vRand].Col4 = Convert.ToString(Convert.ToDecimal(vCapitole[vRand].Col4) - Math.Floor(Convert.ToDecimal(vCapitole[vRand].Col4) / 100) * 100);
                vCapitole[vRand].Col5 = Convert.ToString(Convert.ToDecimal(vCapitole[vRand].Col5) + Math.Floor(Convert.ToDecimal(vCapitole[vRand].Col6) / 100));
                vCapitole[vRand].Col6 = Convert.ToString(Convert.ToDecimal(vCapitole[vRand].Col6) - Math.Floor(Convert.ToDecimal(vCapitole[vRand].Col6) / 100) * 100);
            }

            vTabel.Close();
            // calculez totalurile

            foreach (Capitole a in vCapitole)
            {
                // variabile pt totaluri
                decimal vTotCol1 = Convert.ToDecimal(a.Col1), vTotCol2 = Convert.ToDecimal(a.Col2), vTotCol3 = Convert.ToDecimal(a.Col3), vTotCol4 = Convert.ToDecimal(a.Col4), vTotCol5 = Convert.ToDecimal(a.Col5), vTotCol6 = Convert.ToDecimal(a.Col6);
                // daca este de tip formula incep calculul totalurilor

                if (a.Formula.IndexOf("+") != -1 || a.Formula.IndexOf("-") != -1)
                {
                    string vRand = "", vOperatie = "";
                    int vRandInt = 0;
                    // variabile pentru calcul
                    decimal vCol1 = 0, vCol2 = 0, vCol3 = 0, vCol4 = 0, vCol5 = 0, vCol6 = 0;

                    for (int b = 0; b < a.Formula.Length; b++)
                    {
                        // extrag randul
                        if (a.Formula[b].ToString() != "+" && a.Formula[b].ToString() != "-")
                        {
                            vRand += a.Formula[b].ToString();
                        }

                        // extrag valorile pentru rand
                        if ( b + 1 == a.Formula.Length && vRand != "" )
                        {
                            vRandInt = Convert.ToInt32(vRand) - 1;
                            vRand = "";
                            vCol1 = Convert.ToDecimal(vCapitole[vRandInt].Col1);
                            vCol2 = Convert.ToDecimal(vCapitole[vRandInt].Col2);
                            vCol3 = Convert.ToDecimal(vCapitole[vRandInt].Col3);
                            vCol4 = Convert.ToDecimal(vCapitole[vRandInt].Col4);
                            vCol5 = Convert.ToDecimal(vCapitole[vRandInt].Col5);
                            vCol6 = Convert.ToDecimal(vCapitole[vRandInt].Col6);
                        }
                        else
                        {
                            if ( vRand != "" && (a.Formula[b].ToString() == "+" || a.Formula[b].ToString() == "-") )
                            {
                                vRandInt = Convert.ToInt32(vRand) - 1;
                                vRand = "";

                                if ( vOperatie != "" && a.Formula[b].ToString() != vOperatie )
                                {
                                }
                                else
                                {
                                    vOperatie = a.Formula[b].ToString();
                                }

                                vCol1 = Convert.ToDecimal(vCapitole[vRandInt].Col1);
                                vCol2 = Convert.ToDecimal(vCapitole[vRandInt].Col2);
                                vCol3 = Convert.ToDecimal(vCapitole[vRandInt].Col3);
                                vCol4 = Convert.ToDecimal(vCapitole[vRandInt].Col4);
                                vCol5 = Convert.ToDecimal(vCapitole[vRandInt].Col5);
                                vCol6 = Convert.ToDecimal(vCapitole[vRandInt].Col6);
                            }
                        }
                        // daca operatia este adunare adaug valorile la totaluri

                        if ( vOperatie == "+" && (a.Formula[b].ToString() == "+" || a.Formula[b].ToString() == "-" || (b + 1 == a.Formula.Length)) )
                        {
                            vTotCol1 += vCol1;
                            vTotCol2 += vCol2;
                            vTotCol3 += vCol3;
                            vTotCol4 += vCol4;
                            vTotCol5 += vCol5;
                            vTotCol6 += vCol6;
                        }
                        // daca operatia este scadere scad din valorile de la total
                        else
                        {
                            if ( vOperatie == "-" && (a.Formula[b].ToString() == "+" || a.Formula[b].ToString() == "-") || (b + 1 == a.Formula.Length) )
                            {
                                vTotCol1 -= vCol1;
                                vTotCol2 -= vCol2;
                                vTotCol3 -= vCol3;
                                vTotCol4 -= vCol4;
                                vTotCol5 -= vCol5;
                                vTotCol6 -= vCol6;
                            }
                        }

                        if (a.Formula[b].ToString() == "+" || a.Formula[b].ToString() == "-")
                            vOperatie = a.Formula[b].ToString();
                    }

                    // extrag ha din ari
                    vTotCol1 += Math.Floor(vTotCol2 / 100);
                    vTotCol2 -= Math.Floor(vTotCol2 / 100) * 100;
                    vTotCol3 += Math.Floor(vTotCol4 / 100);
                    vTotCol4 -= Math.Floor(vTotCol4 / 100) * 100;
                    vTotCol5 += Math.Floor(vTotCol6 / 100);
                    vTotCol6 -= Math.Floor(vTotCol6 / 100) * 100;

                    // salvez datele in lista
                    a.Col1 = vTotCol1.ToString();
                    a.Col2 = vTotCol2.ToString();
                    a.Col3 = vTotCol3.ToString();
                    a.Col4 = vTotCol4.ToString();
                    a.Col5 = vTotCol5.ToString();
                    a.Col6 = vTotCol6.ToString();
                }
            }

            // salvez datele in c3
            int vCap3 = 0;

            foreach (Capitole a in vCapitole)
            {
                vCmd1.CommandText = "update capitole set col1='" + a.Col1.Replace(',', '.') + "', col2='" + a.Col2.Replace(',', '.') + "', col3='" + a.Col3.Replace(',', '.') + "', col4='" + a.Col4.Replace(',', '.') + "', col5='" + a.Col5.Replace(',', '.') + "', col6='" + a.Col6.Replace(',', '.') + "' where capitolId='" + a.Id.ToString() + "'";
                vCmd1.ExecuteNonQuery();
                // daca e randul 10 (total teren agricol) scriem si in 3 primul rand 
                //if (vCap3 == 9 && vC3 == "1")
                    if (vCap3 == 9)
                    {
                    // scriem doar primul rand dupa care corectam tot capitolul

                    string vCapitolId3 = "0";
                    vCmd2.CommandText = "SELECT TOP(1) capitole.capitolId FROM capitole where capitole.an='" + pAn + "' and codCapitol='3' and  codRand='1' and gospodarieId = '" + pGospodarieId + "' order by capitole.codRand";
                    vTabel = vCmd2.ExecuteReader();
                    while (vTabel.Read())
                    {
                        vCapitolId3 = vTabel["capitolId"].ToString();
                    }
                    vTabel.Close();
                    // primul rand de suprafata in proprietate
                    vCmd1.CommandText = "update capitole set col1='" + a.Col1.Replace(',', '.') + "', col2='" + a.Col2.Replace(',', '.') + "', col3='0', col4='0', col5='0', col6='0' where capitolId='" + vCapitolId3 + "'";
                    vCmd1.ExecuteNonQuery();
                    
                }
                vCap3++;
            }
            vTranz.Commit();
        }
        catch 
        { 
            vTranz.Rollback(); 
        }
        finally 
        { 
            ManipuleazaBD.InchideConexiune(vCon);
            // refacem tot capitolul 
            Probleme(pAn, pUnitateId, pGospodarieId,"3");
        }
    }

    public static void StergeRanduriCapitolValoriZero(string pUnitateId, string pAn)
    {
        List<string> vCampuriCapitol = new List<string> {"codCapitol", "gospodarieId"};
        List<List<string>> vGospodariiDeSters = new List<List<string>> { };
        int vGata = 0;
        int vContor = 1;
        string vInterogare = "";
        while (vGata == 0)
        {
  ClassLog.fLog(1, 52, DateTime.Now, "STERGE 0", "unit:" + pUnitateId + " | an:" + pAn, " incepe", 0, 1);
            //vGospodariiDeSters = ManipuleazaBD.fRezultaListaStringuri(@"SELECT TOP (1000) codCapitol, gospodarieId FROM         capitole WHERE  (unitateId = " + pUnitateId + ") AND (an = " + pAn + ") GROUP BY codCapitol, gospodarieId, an HAVING (SUM(col1) + SUM(col2) + SUM(col3) + SUM(col4) + SUM(col5) + SUM(col6) + SUM(col7) + SUM(col8) = 0)", vCampuriCapitol);
  vGospodariiDeSters = ManipuleazaBD.fRezultaListaStringuri(@"SELECT codCapitol, gospodarieId FROM         capitole WHERE  (unitateId = " + pUnitateId + ") AND (an = " + pAn + ") GROUP BY codCapitol, gospodarieId, an HAVING (SUM(col1) + SUM(col2) + SUM(col3) + SUM(col4) + SUM(col5) + SUM(col6) + SUM(col7) + SUM(col8) = 0)", vCampuriCapitol, Convert.ToInt16(pAn));
ClassLog.fLog(1, 52, DateTime.Now, "STERGE 0", "unit:" + pUnitateId + " | an:" + pAn, " a luat lista gospodariixcapitole:" + vGospodariiDeSters.Count() + " | "  + @"SELECT TOP (1000) codCapitol, gospodarieId FROM         capitole WHERE  (unitateId = " + pUnitateId + ") AND (an = " + pAn + ") GROUP BY codCapitol, gospodarieId, an HAVING (SUM(col1) + SUM(col2) + SUM(col3) + SUM(col4) + SUM(col5) + SUM(col6) + SUM(col7) + SUM(col8) = 0)", 0, 1);
            foreach (List<string> vGospodarie in vGospodariiDeSters)
            {
                vInterogare += "DELETE FROM capitole WHERE an =" + pAn + " AND unitateId = " + pUnitateId + " AND gospodarieId = " + vGospodarie[1] + " AND codCapitol = '" + vGospodarie[0] + "' AND convert(decimal,col1) = 0  AND convert(decimal,col2) = 0  AND convert(decimal,col2) = 0  AND convert(decimal,col3) = 0  AND convert(decimal,col4) = 0  AND convert(decimal,col5) = 0  AND convert(decimal,col6) = 0  AND convert(decimal,col7) = 0  AND convert(decimal,col8) = 0;";
            }
  ClassLog.fLog(1, 52, DateTime.Now, "STERGE 0", "unit:" + pUnitateId + " | an:" + pAn, " a terminat stringul lung de:" + vInterogare.Length, 0, 1);
            if (vGospodariiDeSters.Count == 0) vGata = 1;
            else ManipuleazaBD.fManipuleazaBD(vInterogare, Convert.ToInt16(pAn));
  ClassLog.fLog(1, 52, DateTime.Now, "STERGE 0", "unit:" + pUnitateId + " | an:" + pAn, " a terminat de inserat", 0, 1);
            vContor++;
            return;
        }
            
    }
    
    public static void ActualizeazaCapitole(string pUnitateId, string pAn, string pGospodarieId, string pCapitolId) 
    {
        List<string> vGospodarii =  new List<string>{};
        List<string> vCapitole =  new List<string>{};
        switch (pGospodarieId)
        { 
            case "0":
                // luam lista de gospodarii
                vGospodarii = ManipuleazaBD.fRezultaListaStringuri("SELECT gospodarieId FROM gospodarii WHERE an =" + pAn + " AND unitateId=" + pUnitateId, "gospodarieId", Convert.ToInt16(pAn));
                foreach (string vGospodarie in vGospodarii)
                {
                    if (pCapitolId == "0")
                    {
                        // luam lista de capitole
                        vCapitole = ManipuleazaBD.fRezultaListaStringuri("SELECT DISTINCT capitol FROM sabloaneCapitole WHERE an ='" + pAn + "' ORDER BY capitol", "capitol", Convert.ToInt16(pAn));
                        foreach (string vCapitol in vCapitole)
                            Probleme(pAn, pUnitateId, vGospodarie, vCapitol);
                    }
                    else Probleme(pAn, pUnitateId, vGospodarie, pCapitolId);
                }
                break;
            default:
                if (pCapitolId == "0")
                {
                    // luam lista de capitole
                    vCapitole = ManipuleazaBD.fRezultaListaStringuri("SELECT DISTINCT capitol FROM sabloaneCapitole WHERE an ='" + pAn + "' ORDER BY capitol", "capitol", Convert.ToInt16(pAn));
                    foreach (string vCapitol in vCapitole)
                        Probleme(pAn, pUnitateId, pGospodarieId.ToString(), vCapitol);
                }
                else Probleme(pAn, pUnitateId, pGospodarieId.ToString(), pCapitolId);
                break;
        }
    }


    public static string CreeazaInsertRanduriCapitol(string pGospodarieId, string pAn, string pCapitol, string pUnitateId)
    {
        // daca are vreunul capitolid = 0 facem insert, daca nu, update
        string vInterogare = "";
        foreach (List<string> vRand in ScoateRanduriCapitol(pGospodarieId,pAn,pCapitol,pUnitateId))
        {
            if (vRand[10] == "0")
            {
                vInterogare += @"INSERT INTO [capitole] ([unitateId], [gospodarieId], [an], [codCapitol], [codRand], [col1], [col2], [col3], [col4], [col5], [col6], [col7], [col8]) VALUES ('" + pUnitateId.ToString() + "','" + pGospodarieId.ToString() + "','" + pAn.ToString() + "','" + pCapitol + "', '" + vRand[0] + "', '" + vRand[2].Replace(',', '.') + "', '" + vRand[3].Replace(',', '.') + "', '" + vRand[4].Replace(',', '.') + "', '" + vRand[5].Replace(',', '.') + "', '" + vRand[6].Replace(',', '.') + "', '" + vRand[7].Replace(',', '.') + "', '" + vRand[8].Replace(',', '.') + "', '" + vRand[9].Replace(',', '.') + "'); ";
            }
            else
            {
                vInterogare += "update capitole set col1='" + vRand[2].Replace(',', '.') + "',  col2='" + vRand[3].Replace(',', '.') + "',  col3='" + vRand[4].Replace(',', '.') + "',  col4='" + vRand[5].Replace(',', '.') + "',  col5='" + vRand[6].Replace(',', '.') + "',  col6='" + vRand[7].Replace(',', '.') + "',  col7='" + vRand[8].Replace(',', '.') + "',  col8='" + vRand[9].Replace(',', '.') + "' WHERE capitolId='" + vRand[10] + "'; ";
            }
            
        }
        return vInterogare;
    }
    public static List<List<List<string>>> Probleme(string pAn, string pUnitateId)
    { 
        // lista de gospodarii 
        List<string> vGospodarii = ManipuleazaBD.fRezultaListaStringuri("SELECT * FROM gospodarii WHERE unitateId = " + pUnitateId + " AND an=" + pAn, "gospodarieId", Convert.ToInt16(pAn));
        // lista capitolelor
        List<string> vCapitole = ManipuleazaBD.fRezultaListaStringuri("SELECT DISTINCT capitol FROM sabloaneCapitole WHERE an =" + pAn, "capitol", Convert.ToInt16(pAn));
        // lista problemlor de returnat
        List<List<List<string>>> vProbleme = new List<List<List<string>>> { };
        int i1 = 0;
        int i2 = 0;
        int i3 = vGospodarii.Count * vCapitole.Count;
        int i4 = 0;


        foreach (string vGospodarie in vGospodarii)
        {        
            string vInterogare = "";
            if (i1 > 100000) return vProbleme;

            foreach (string vCapitol in vCapitole)
            {            
                i1++;
             //   if (vCapitol == "7" )
                    if (1 == 1 || vCapitol.IndexOf("12") == -1 && vCapitol.IndexOf("13") == -1)
                    {
                    //string vCapitol = "7";
                    List<List<string>> vDateCapitol = ScoateRanduriCapitol(vGospodarie, pAn, vCapitol, pUnitateId);
                    List<List<string>> vRanduriRele = new List<List<string>> { };
                    List<List<string>> vRanduriBune = new List<List<string>> { };
                   
                    foreach (List<string> vRand in vDateCapitol)
                    {
                        // verific daca are capitolid = 0 si mai exista cel putin un alt rand la resp capitol
                        if (vRand[10] == "0")
                        {
                            vRanduriRele.Add(vRand);
                            vInterogare += @"INSERT INTO [capitole] ([unitateId], [gospodarieId], [an], [codCapitol], [codRand], [col1], [col2], [col3], [col4], [col5], [col6], [col7], [col8]) VALUES ('" + pUnitateId.ToString() + "','" + vRand[12] + "','" + pAn.ToString() + "','" + vCapitol + "', '" + vRand[0] + "', '" + vRand[2].Replace(',', '.') + "', '" + vRand[3].Replace(',', '.') + "', '" + vRand[4].Replace(',', '.') + "', '" + vRand[5].Replace(',', '.') + "', '" + vRand[6].Replace(',', '.') + "', '" + vRand[7].Replace(',', '.') + "', '" + vRand[8].Replace(',', '.') + "', '" + vRand[9].Replace(',', '.') + "'); ";
                            i1 = 10000000;
                        }
                        else
                        {
                            vRanduriBune.Add(vRand);
                            vInterogare += "update capitole set col1='" + vRand[2].Replace(',', '.') + "',  col2='" + vRand[3].Replace(',', '.') + "',  col3='" + vRand[4].Replace(',', '.') + "',  col4='" + vRand[5].Replace(',', '.') + "',  col5='" + vRand[6].Replace(',', '.') + "',  col6='" + vRand[7].Replace(',', '.') + "',  col7='" + vRand[8].Replace(',', '.') + "',  col8='" + vRand[9].Replace(',', '.') + "' WHERE capitolId='" + vRand[10] + "'; ";
                        }
                    }
                    if (vRanduriRele.Count != vDateCapitol.Count)
                    {
                        i2++;
                        ManipuleazaBD.fManipuleazaBD(vInterogare, Convert.ToInt16(pAn));
                        ClassLog.fLog(1, 52, DateTime.Now, "CORECTARE CAPITOLE", "unitatea:" + pUnitateId + " | an:" + pAn, " | cap:" + vCapitol + " | g:" + vGospodarie + " | corectata nr. " + i2 + " | trecute: " + i1 + " | total: " + i3, 0, 1);

                        /*  if (vRanduriRele.Count > 0)
                              vProbleme.Add(vRanduriRele);
                         if (vRanduriBune.Count > 0)
                              vProbleme.Add(vRanduriBune);*/
                        List<List<string>> vInterogari1 = new List<List<string>> { };
                        List<string> vInterogari2 = new List<string> { };
                        vInterogari2.Add(vInterogare);
                        vInterogari2.Add(vCapitol);
                        vInterogari2.Add(vGospodarie);
                        vInterogari1.Add(vInterogari2);
                      //  vProbleme.Add(vInterogari1);
                    }
                    else
                    {
                        i4++;
                        ClassLog.fLog(1, 52, DateTime.Now, "CORECTARE CAPITOLE", "unitatea:" + pUnitateId + " | an:" + pAn, " | cap:" + vCapitol + " | g:" + vGospodarie + " | NEcorectat nr. " + i4 + " | trecute: " + i1 + " | total: " + i3, 0, 1);
                    }
                }
            }
        }
        return vProbleme;
    }
    public static void Probleme(string pAn, string pUnitateId, string pGospodarie, string pCapitol)
    {
        // lista problemlor de returnat
        List<List<List<string>>> vProbleme = new List<List<List<string>>> { };
        int i1 = 0;
        int i2 = 0;
        int i4 = 0;
        i1++;
        //string vCapitol = "7";
        List<List<string>> vDateCapitol = ScoateRanduriCapitol(pGospodarie, pAn, pCapitol, pUnitateId);
        List<List<string>> vRanduriRele = new List<List<string>> { };
        List<List<string>> vRanduriBune = new List<List<string>> { };
        string vInterogare = "";
        foreach (List<string> vRand in vDateCapitol)
        {
            // verific daca are capitolid = 0 si mai exista cel putin un alt rand la resp capitol
            if (vRand[10] == "0")
            {
                vRanduriRele.Add(vRand);
                vInterogare += @"INSERT INTO [capitole] ([unitateId], [gospodarieId], [an], [codCapitol], [codRand], [col1], [col2], [col3], [col4], [col5], [col6], [col7], [col8]) VALUES ('" + pUnitateId.ToString() + "','" + vRand[12] + "','" + pAn.ToString() + "','" + pCapitol + "', '" + vRand[0] + "', '" + vRand[2].Replace(',', '.') + "', '" + vRand[3].Replace(',', '.') + "', '" + vRand[4].Replace(',', '.') + "', '" + vRand[5].Replace(',', '.') + "', '" + vRand[6].Replace(',', '.') + "', '" + vRand[7].Replace(',', '.') + "', '" + vRand[8].Replace(',', '.') + "', '" + vRand[9].Replace(',', '.') + "'); ";
            }
            else
            {
                vRanduriBune.Add(vRand);
                vInterogare += "update capitole set col1='" + vRand[2].Replace(',', '.') + "',  col2='" + vRand[3].Replace(',', '.') + "',  col3='" + vRand[4].Replace(',', '.') + "',  col4='" + vRand[5].Replace(',', '.') + "',  col5='" + vRand[6].Replace(',', '.') + "',  col6='" + vRand[7].Replace(',', '.') + "',  col7='" + vRand[8].Replace(',', '.') + "',  col8='" + vRand[9].Replace(',', '.') + "' WHERE capitolId='" + vRand[10] + "'; ";
            }
        }
        if (vRanduriRele.Count != vDateCapitol.Count)
        {
            i2++;
            ManipuleazaBD.fManipuleazaBD(vInterogare, Convert.ToInt16(pAn));
            ClassLog.fLog(1, 52, DateTime.Now, "CORECTARE CAPITOL", "unitatea:" + pUnitateId + " | an:" + pAn, " | cap:" + pCapitol + " | g:" + pGospodarie + " | corectata nr. " + i2 + " | trecute: " + i1, 0, 1);
        }
        else
        {
            i4++;
            ClassLog.fLog(1, 52, DateTime.Now, "CORECTARE CAPITOLE", "unitatea:" + pUnitateId + " | an:" + pAn, " | cap:" + pCapitol + " | g:" + pGospodarie + " | NEcorectat nr. " + i4 + " | trecute: " + i1, 0, 1);
        }
    }
    public static List<List<string>> ScoateRanduriCapitol(string pGospodarieId, string pAn, string pCapitol, string pUnitateId)
    {
        List<string> vCampuriCapitol = new List<string> { "codRand", "col1", "col2", "col3", "col4", "col5", "col6", "col7", "col8", "capitolId", "capitol", "gospodarie" };
        List<List<string>> vListaCapitol = ManipuleazaBD.fRezultaListaStringuri("SELECT '" + pCapitol + "' as capitol, '" + pGospodarieId + "' as gospodarie,  capitolId, coalesce(codRand,'0') as codRand,  coalesce(col1,'0') as col1,coalesce(col2,'0') as col2,coalesce(col3,'0') as col3,coalesce(col4,'0') as col4,coalesce(col5,'0') as col5,coalesce(col6,'0') as col6,coalesce(col7,'0') as col7,coalesce(col8,'0')   as col8 FROM capitole WHERE an = '" + pAn + "' AND codCapitol = '" + pCapitol + "' AND gospodarieId = '" + pGospodarieId + "'", vCampuriCapitol, Convert.ToInt16(pAn));
        
        List<List<string>> vListaCapitolCorect = new List<List<string>> { };
        // daca nu are niciun rand nu mai continuam
        if (vListaCapitol.Count == 0) return vListaCapitolCorect;

        // luam din SabloaneCapitole lista cu randurile corecte
        List<string> vCampuriSabloane = new List<string> { "codRand", "formula" };
        List<List<string>> vListaSabloane = ManipuleazaBD.fRezultaListaStringuri("SELECT coalesce(codRand,'0') as codRand,  coalesce(formula,'') as formula  FROM sabloaneCapitole WHERE an = '" + pAn + "' AND capitol = '" + pCapitol + "'", vCampuriSabloane, Convert.ToInt16(pAn));


        int vContor = 0;
        foreach (List<string> vRandSablon in vListaSabloane)
        {
            int vExista = 0;
            foreach (List<string> vRandCapitol in vListaCapitol)
            {
                if (vRandCapitol[0] == vRandSablon[0])
                {
                    // daca e acelasi rand scriem in vListaCapitolCorect
                    List<string> vCorect = new List<string> { };
                    vCorect.Add(vRandSablon[0]);
                    vCorect.Add(vRandSablon[1]);
                    int vContor1 = 0;
                    foreach (string vColoana in vRandCapitol)
                    {
                        if (vContor1 > 0)
                        {
                            try
                            {
                                if (vContor1 < 9) vCorect.Add(Convert.ToDecimal(vColoana).ToString());
                                else
                                    vCorect.Add(vColoana);
                            }
                            catch
                            {
                                vCorect.Add("0");
                            }
                        }
                        vContor1++;
                    }
                    vListaCapitolCorect.Add(vCorect);
                    vExista = 1;
                    break;
                }
            }
            // daca nu s-a gasit randul inseram unul nou gol
            if (vExista == 0)
            {
                List<string> vCorect = new List<string> { };
                vCorect.Add(vRandSablon[0]);
                vCorect.Add(vRandSablon[1]);
                for (int i = 1; i <= 8; i++)
                    vCorect.Add("0");
                vCorect.Add("0");
                vCorect.Add(pCapitol);
                vCorect.Add(pGospodarieId);
                vListaCapitolCorect.Add(vCorect);
            }
            vContor++;
        }
        // recalculam totalurile
        // corectam coloanele cu formule
        List<List<string>> vListaCapitolCorectFormule = new List<List<string>> { };
        List<List<string>> vListaDeCorectat = new List<List<string>> { };
        List<List<List<string>>> vListeDeCorectat = new List<List<List<string>>> { };
        int vPozitieInLista = 0;
        foreach (List<string> vRandCapitol in vListaCapitolCorect)
        {
            // scoatem liste cu ce se adauga si ce se scade
            if (vRandCapitol[1].IndexOf("+") != -1 || vRandCapitol[1].IndexOf("-") != -1)
            { 
                string vFormula = vRandCapitol[1].Trim();
                int vPozitiePlus = -1;
                int vPozitieMinus = -1;
                string vIntermediar = "";
                List<string> vAduna = new List<string> { }; 
                List<string> vScade = new List<string> { };
                // adaugam primul rand
                vPozitiePlus = vFormula.IndexOf('+');
                vPozitieMinus = vFormula.IndexOf('-');
                int vPozitie = -1;
                vIntermediar = "";
                int v=0;
                // scapam de primul rand fara semn
                                    // cautam primul semn
                    vPozitiePlus = vFormula.IndexOf('+');
                    vPozitieMinus = vFormula.IndexOf('-');
                    if (vPozitiePlus != 0 && vPozitieMinus != 0)
                    { 
                        // gasim pozitia primului semn
                        int vPoz = 0;
                        if (vPozitiePlus > vPozitieMinus && vPozitieMinus != -1) vPoz = vPozitieMinus;
                        else vPoz = vPozitiePlus;
                        vAduna.Add(vFormula.Substring(0, vPoz));
                        vFormula = vFormula.Remove(0, vPoz);
                    }
                while (vFormula.Length > 0)
                {
                    v++;
                    // cautam primul semn
                    vPozitiePlus = vFormula.Trim().IndexOf('+');
                    vPozitieMinus = vFormula.Trim().IndexOf('-');
                    
                    vPozitie = -1;
                    if (vPozitiePlus == 0)
                    {
                        // adaugam
                        vFormula = vFormula.Remove(0, 1);
                        vPozitie = vFormula.IndexOf('+', vPozitiePlus);
                        if (vPozitie == -1)
                        {
                            vPozitie = vFormula.IndexOf('-', vPozitiePlus);
                            if (vPozitie == -1)
                                vPozitie = vFormula.Length;
                        }
                        vIntermediar = vFormula.Substring(0, vPozitie).Replace("+", String.Empty).Replace("-", String.Empty).Trim();
                        if (vIntermediar.Length > 0)
                        {
                            vAduna.Add(vIntermediar);
                            // stergem din formula ce am salvat
                            vFormula = vFormula.Remove(0, vIntermediar.Length);
                        }
                    }
                    else if (vPozitieMinus == 0)
                    {
                        // scadem
                        vFormula = vFormula.Remove(0, 1);
                        vPozitie = vFormula.IndexOf('-', vPozitieMinus);
                        if (vPozitie == -1)
                        {
                            vPozitie = vFormula.IndexOf('-', vPozitieMinus);
                            if (vPozitie == -1)
                                vPozitie = vFormula.Length;
                        }
                        vIntermediar = vFormula.Substring(0, vPozitie).Replace("+", String.Empty).Replace("-", String.Empty).Trim();
                        if (vIntermediar.Length > 0)
                        {
                            vScade.Add(vIntermediar);
                            // stergem din formula ce am salvat
                            vFormula = vFormula.Remove(0, vIntermediar.Length);
                        }
                    }
                    else
                        vPozitie = vFormula.Length;
                    vIntermediar = "";
                }
                // completam 
                // adaugam randul
                if (vAduna.Count > 0 || vScade.Count > 0)
                {
                    vListaDeCorectat = new List<List<string>> { };
                    List<string> vListaDeAdaugat = new List<string> { };
                    vListaDeAdaugat.Add(vPozitieInLista.ToString());
                    vListaDeCorectat.Add(vListaDeAdaugat);
                    // adaugam "adauga"
                    vListaDeCorectat.Add(vAduna);
                    // adaugam "scade"
                    vListaDeCorectat.Add(vScade);
                    // adaugam listA in listE
                    vListeDeCorectat.Add(vListaDeCorectat);
                }
            }
            vPozitieInLista++;
        }
        // luam din liste de corectat si executam totalurile
        foreach (List<List<string>> vLista1 in vListeDeCorectat)
        {
            // Lista[0][0] --> vListaCapitolCorect
            decimal vTotal1 = 0;
            decimal vTotal2 = 0;
            decimal vTotal3 = 0;
            decimal vTotal4 = 0;
            decimal vTotal5 = 0;
            decimal vTotal6 = 0;
            decimal vTotal7 = 0;
            decimal vTotal8 = 0;
            foreach (string vAduna1 in vLista1[1])
            {
                vTotal1 += Convert.ToDecimal(vListaCapitolCorect[Convert.ToInt16(vAduna1)-1][2].Replace('.', ','));
                vTotal2 += Convert.ToDecimal(vListaCapitolCorect[Convert.ToInt16(vAduna1) - 1][3].Replace('.', ','));
                vTotal3 += Convert.ToDecimal(vListaCapitolCorect[Convert.ToInt16(vAduna1) - 1][4].Replace('.', ','));
                vTotal4 += Convert.ToDecimal(vListaCapitolCorect[Convert.ToInt16(vAduna1) - 1][5].Replace('.', ','));
                vTotal5 += Convert.ToDecimal(vListaCapitolCorect[Convert.ToInt16(vAduna1) - 1][6].Replace('.', ','));
                vTotal6 += Convert.ToDecimal(vListaCapitolCorect[Convert.ToInt16(vAduna1) - 1][7].Replace('.', ','));
                vTotal7 += Convert.ToDecimal(vListaCapitolCorect[Convert.ToInt16(vAduna1) - 1][8].Replace('.', ','));
                vTotal8 += Convert.ToDecimal(vListaCapitolCorect[Convert.ToInt16(vAduna1) - 1][9].Replace('.', ','));
            }
            foreach (string vScade1 in vLista1[2])
            {
                vTotal1 -= Convert.ToDecimal(vListaCapitolCorect[Convert.ToInt16(vScade1) - 1][2].Replace('.', ','));
                vTotal2 -= Convert.ToDecimal(vListaCapitolCorect[Convert.ToInt16(vScade1) - 1][3].Replace('.', ','));
                vTotal3 -= Convert.ToDecimal(vListaCapitolCorect[Convert.ToInt16(vScade1) - 1][4].Replace('.', ','));
                vTotal4 -= Convert.ToDecimal(vListaCapitolCorect[Convert.ToInt16(vScade1) - 1][5].Replace('.', ','));
                vTotal5 -= Convert.ToDecimal(vListaCapitolCorect[Convert.ToInt16(vScade1) - 1][6].Replace('.', ','));
                vTotal6 -= Convert.ToDecimal(vListaCapitolCorect[Convert.ToInt16(vScade1) - 1][7].Replace('.', ','));
                vTotal7 -= Convert.ToDecimal(vListaCapitolCorect[Convert.ToInt16(vScade1) - 1][8].Replace('.', ','));
                vTotal8 -= Convert.ToDecimal(vListaCapitolCorect[Convert.ToInt16(vScade1) - 1][9].Replace('.', ','));
            }

            // daca e cap 3 facem conversia ha - ari
            if (pCapitol == "2a" || pCapitol == "3" || pCapitol == "4a" || pCapitol == "5b" || pCapitol == "5c" || pCapitol == "5d" || pCapitol == "6")
            {
                string[] vAriHa = CalculeazaAriHa(vTotal1.ToString(), "", vTotal2.ToString(), "");
                vTotal1 = Convert.ToDecimal(vAriHa[0]);
                vTotal2 = Convert.ToDecimal(vAriHa[1]);
                if (pCapitol == "2a")
                {
                    vAriHa = CalculeazaAriHa(vTotal3.ToString(), "", vTotal4.ToString(), "");
                    vTotal3 = Convert.ToDecimal(vAriHa[0]);
                    vTotal4 = Convert.ToDecimal(vAriHa[1]);               
                    vAriHa = CalculeazaAriHa(vTotal5.ToString(), "", vTotal6.ToString(), "");
                    vTotal5 = Convert.ToDecimal(vAriHa[0]);
                    vTotal6 = Convert.ToDecimal(vAriHa[1]);
                }
            }
            vListaCapitolCorect[Convert.ToInt16(vLista1[0][0])][2] = vTotal1.ToString();
            vListaCapitolCorect[Convert.ToInt16(vLista1[0][0])][3] = vTotal2.ToString();
            vListaCapitolCorect[Convert.ToInt16(vLista1[0][0])][4] = vTotal3.ToString();
            vListaCapitolCorect[Convert.ToInt16(vLista1[0][0])][5] = vTotal4.ToString();
            vListaCapitolCorect[Convert.ToInt16(vLista1[0][0])][6] = vTotal5.ToString();
            vListaCapitolCorect[Convert.ToInt16(vLista1[0][0])][7] = vTotal6.ToString();
            vListaCapitolCorect[Convert.ToInt16(vLista1[0][0])][8] = vTotal7.ToString();
            vListaCapitolCorect[Convert.ToInt16(vLista1[0][0])][9] = vTotal8.ToString();
        }
        return vListaCapitolCorect;
    }
    public static string[] CalculeazaAriHa(string pHa1, string pHa2, string pAri1, string pAri2)
    {
        string[] vValori = { "0", "0" };
        if (pHa1 == "") pHa1 = "0";
        if (pHa2 == "") pHa2 = "0";
        if (pAri1 == "") pAri1 = "0";
        if (pAri2 == "") pAri2 = "0";
        decimal vValoare = Convert.ToDecimal(pHa1) * 100 + Convert.ToDecimal(pHa2) * 100 + Convert.ToDecimal(pAri1) + Convert.ToDecimal(pAri2);
        vValori[1] = (vValoare % 100).ToString();
        vValori[0] = Convert.ToInt64((vValoare - (vValoare % 100)) / 100).ToString();
        return vValori;
    }
    public static List<string> CompleteazaRand(List<string> pListaInitiala, List<string> pAduna, List<string> pScade)
    {
        // completam randul 
        return pListaInitiala;
    }
}
