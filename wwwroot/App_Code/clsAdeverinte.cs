﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsAdeverinte
/// ultima modificare           20.01.2012
///         autor               alex
///         modificari          adaugare taguri : bovine,pasari
///         sesizare            491
///     ultima actualizare          26.01.2012
///     utilizator                  SM
///     modificare                  adaugare taguri [email], [utilizator], [cod-postal]

/// </summary>
public class clsAdeverinte
{
    public clsAdeverinte()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static string GetGopodarieIdByAn(string an, string volum, string pozitie)
    {
        string gospodarieID = "";
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(an));
        SqlCommand command = new SqlCommand();
        command.Connection = connection;
        command.CommandText = "select gospodarieId from gospodarii where an = " + an + " and volum = '" + volum + "' and nrPozitie = '" + pozitie + "' and unitateId = " + HttpContext.Current.Session["SESunitateId"] + " ";
        SqlDataReader dataReader = command.ExecuteReader();
        if (dataReader.Read())
        {
            gospodarieID = dataReader["gospodarieId"].ToString();
        }
        ManipuleazaBD.InchideConexiune(connection);
        return gospodarieID;
    }

    public static string GetVolum(int an, int gospodarieId)
    {
        string volum = "";
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(an));
        SqlCommand command = new SqlCommand();
        command.Connection = connection;
        command.CommandText = "select volum from gospodarii where an = " + an + " and gospodarieId = '" + gospodarieId + "' and unitateId = " + HttpContext.Current.Session["SESunitateId"] + " ";
        SqlDataReader dataReader = command.ExecuteReader();
        if (dataReader.Read())
        {
            volum = dataReader["volum"].ToString();
        }
        ManipuleazaBD.InchideConexiune(connection);
        return volum;
    }

    public static string GetPozitie(int an, int gospodarieId)
    {
        string nrPozitie = "";
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(an));
        SqlCommand command = new SqlCommand();
        command.Connection = connection;
        command.CommandText = "select nrPozitie from gospodarii where an = " + an + " and gospodarieId = '" + gospodarieId + "' and unitateId = " + HttpContext.Current.Session["SESunitateId"] + " ";
        SqlDataReader dataReader = command.ExecuteReader();
        if (dataReader.Read())
        {
            nrPozitie = dataReader["nrPozitie"].ToString();
        }
        ManipuleazaBD.InchideConexiune(connection);
        return nrPozitie;
    }
    public static string TagAnimale(string pGospodarie, string pAnulCurent, string pCodRand)
    {
        string vTagInterpretat = "";
        int vAreBunuri = 0;
        string vSemestruText = "";
        string vColSemestru = "col1";
        if (Convert.ToInt32(DateTime.Now.Month) <= 6 && pAnulCurent == DateTime.Now.Year.ToString())
        {
            vColSemestru = "col1";
        }
        else
        {
            vColSemestru = "col2";
        }
        List<string> vColoane = new List<string> { "capitolId", vColSemestru };
        List<string> vUM = new List<string> { "bucati", "bucati" };
        string vTemporar = "";
        int vAreValoare = 0;
        vTemporar = StringCapitole(vColoane, vUM, " ", pGospodarie, pAnulCurent, "7", pCodRand, out vAreValoare);
        if (vAreValoare != 0) { vTagInterpretat = vTemporar + vSemestruText; vAreBunuri = 1; }
        if (vAreBunuri == 0) { vTagInterpretat = "0 "; }
        return vTagInterpretat;
    }

    public static int TagAnimaleFaraText(string pGospodarie, string pAnulCurent, string pCodRand)
    {
        int vTagInterpretat = 0;
        int vAreBunuri = 0;
        int vSemestruText = 0;
        string vColSemestru = "col1";
        if (Convert.ToInt32(DateTime.Now.Month) <= 6 && pAnulCurent == DateTime.Now.Year.ToString())
        {
            vColSemestru = "col1";
        }
        else
        {
            vColSemestru = "col2";
        }
        List<string> vColoane = new List<string> { "capitolId", vColSemestru };
        List<string> vUM = new List<string> { "bucati", "bucati" };
        int vTemporar = 0;
        int vAreValoare = 0;
        vTemporar = (int)StringCapitoleFaraText(vColoane, vUM, " ", pGospodarie, pAnulCurent, "7", pCodRand, out vAreValoare);
        if (vAreValoare != 0) { vTagInterpretat = vTemporar + vSemestruText; vAreBunuri = 1; }
        if (vAreBunuri == 0) { vTagInterpretat = 0; }
        return vTagInterpretat;
    }

    public static string TagTerenuri3(string pGospodarie, string pAnulCurent, string pParcelaCategorie, string pContractTip)
    {
        string vTagInterpretat = ManipuleazaBD.fRezultaUnString(@"SELECT parceleAtribuiriContracte.contractGospodarieIdPrimeste, 
(SUM(parceleModUtilizare.c3Ha) * 100 + SUM(parceleModUtilizare.c3Ari)) / 100 AS suprafata, 
parcele.parcelaCategorie, 'primite' AS z, parceleAtribuiriContracte.contractTip 
FROM  parcele 
    INNER JOIN parceleModUtilizare ON parcele.parcelaId = parceleModUtilizare.parcelaId 
    INNER JOIN parceleAtribuiriContracte ON parceleModUtilizare.contractId = parceleAtribuiriContracte.contractId AND parceleAtribuiriContracte.contractActiv = 1
WHERE 
    (parceleAtribuiriContracte.contractGospodarieIdPrimeste = '" + pGospodarie + "') " +
    "AND (parcele.an = '" + pAnulCurent + "') " +
    "AND (parceleAtribuiriContracte.contractDataInceput <= CONVERT(date, GETDATE(), 104)) AND (parceleAtribuiriContracte.contractDataFinal >= CONVERT(date, GETDATE(), 104)) AND " +
    "(parcele.parcelaCategorie = '" + pParcelaCategorie + "') AND (parceleAtribuiriContracte.contractTip = '" + pContractTip + "') GROUP BY parceleAtribuiriContracte.contractGospodarieIdPrimeste, parcele.parcelaCategorie, parceleAtribuiriContracte.contractTip", "suprafata", Convert.ToInt16(pAnulCurent));
        return vTagInterpretat;
    }
    public static string TagTerenuri3Proprietate(string pGospodarie, string pAnulCurent, string pParcelaCategorie)
    {
        // scoatem valori in proprietate - toate
        decimal vTagInterpretatProprietate = 0;
        try
        {
            vTagInterpretatProprietate = Convert.ToDecimal(ManipuleazaBD.fRezultaUnString(@"SELECT ((SUM(parcele.parcelaSuprafataIntravilanHa) * 100 + SUM(parcele.parcelaSuprafataIntravilanAri)) 
+ SUM(parcele.parcelaSuprafataExtravilanHa) * 100 + SUM(parcele.parcelaSuprafataExtravilanAri)) / 100 AS suprafata
FROM  parcele 
    INNER JOIN sabloaneCapitole ON parcele.parcelaCategorie = sabloaneCapitole.codRand
WHERE   (parcele.gospodarieId = '" + pGospodarie + "') AND (parcele.an = '" + pAnulCurent + "') AND (sabloaneCapitole.an = '" + pAnulCurent + "') AND (sabloaneCapitole.capitol = '2a') AND (sabloaneCapitole.denumire4 <> '') AND (CHARINDEX('+', sabloaneCapitole.formula) = 0) AND (CHARINDEX('-', sabloaneCapitole.formula) = 0) AND (sabloaneCapitole.codRand = '" + pParcelaCategorie + "')", "suprafata", Convert.ToInt16(pAnulCurent)));
        }
        catch
        { }
        // valori date
        decimal vTagInterpretatDate = 0;
        try
        {
            vTagInterpretatDate = Convert.ToDecimal(ManipuleazaBD.fRezultaUnString(@"SELECT     (SUM(parceleModUtilizare.c3Ha) * 100 + SUM(parceleModUtilizare.c3Ari)) / 100 AS suprafata
FROM         parcele INNER JOIN
                      parceleModUtilizare ON parcele.parcelaId = parceleModUtilizare.parcelaId INNER JOIN
                      parceleAtribuiriContracte ON parceleModUtilizare.contractId = parceleAtribuiriContracte.contractId AND parceleAtribuiriContracte.contractActiv = 1
WHERE     (parcele.an = '2012') AND (parceleAtribuiriContracte.contractDataInceput <= CONVERT(date, GETDATE(), 104)) AND 
                      (parceleAtribuiriContracte.contractDataFinal >= CONVERT(date, GETDATE(), 104)) AND (parcele.parcelaCategorie = '" + pParcelaCategorie + @"') AND 
                      (parceleAtribuiriContracte.contractTip IN ('3', '4', '5', '6', '7', '8')) AND (parceleAtribuiriContracte.contractGospodarieIdDa = '" + pGospodarie + "')", "suprafata", Convert.ToInt16(pAnulCurent)));
        }
        catch { }
        string vTagInterpretat = (-Convert.ToDecimal(vTagInterpretatDate) + Convert.ToDecimal(vTagInterpretatProprietate)).ToString("f2");
        return vTagInterpretat;
    }


    public static decimal TagCulturi(string pGospodarie, string pAnulCurent, string pCodRand)
    {
        List<string> vColoane = new List<string> { "col1", "col2" };
        decimal vValoare = 0;
        List<List<string>> vLista = ManipuleazaBD.fRezultaListaStringuri("SELECT col1, col2 FROM capitole WHERE gospodarieId ='" + pGospodarie + "' AND codCapitol ='4a' AND an='" + pAnulCurent + "' AND codRand='" + pCodRand + "'", vColoane, Convert.ToInt16(pAnulCurent));
        foreach (List<string> vValori in vLista)
            vValoare += Convert.ToDecimal(vValori[0]) * 100 + Convert.ToDecimal(vValori[1]);
        return vValoare / 100;
    }
    public static List<List<string>> Masura112CalculTabel1(string pGospodarieId, string pAn)
    {
        // luam toate datele din capitolele care ne intereseaza
        List<string> vCampuri = new List<string> { "codCapitol", "codRand", "col1", "col2" };
        List<List<string>> vDateDinCapitole = ManipuleazaBD.fRezultaListaStringuri("SELECT     codCapitol, codRand, col1, col2 FROM         capitole WHERE     (gospodarieId = " + pGospodarieId + ") AND (codCapitol IN ('2a', '4a', '4b', '4c', '5b', '5c', '5d')) AND (an = " + pAn + ")", vCampuri, Convert.ToInt16(pAn));
        // luam datele din sabloaneMasuri pt masura 112
        List<string> vCampuriMasuri = new List<string> { "sablonDescriere", "sablonCorespondenti", "sablonUnitateMasura", "sablonCoeficient", "sablonCodRand", "sablonTabel" };
        List<List<string>> vDateDinMasuri = ManipuleazaBD.fRezultaListaStringuri("SELECT  * FROM sabloaneMasuri WHERE (sablonMasura ='112') AND sablonTabel = '1'", vCampuriMasuri, Convert.ToInt16(pAn));
        // parcurgem lista din sabloane si cautam datele in corespondenti
        List<List<string>> vRanduriCompletate = new List<List<string>> { };
        string vFormat = "0.000";
        decimal vTotalUDE = 0;
        foreach (List<string> vRandMasura in vDateDinMasuri)
        {
            decimal vValoare = 0;
            decimal vValoareHa = 0;
            List<string> vRandCompletat = vRandMasura;
            if (vRandMasura[1] != "" && vRandMasura[1] != null)
            {
                List<string[]> vCorespondenti = MasuriCorespondenti(vRandMasura[1]);
                vValoare = 0;
                vValoareHa = 0;
                foreach (string[] vCorespondent in vCorespondenti)
                {
                    foreach (List<string> vDate in vDateDinCapitole)
                    {
                        if (vDate[0] == vCorespondent[1] && vDate[1] == vCorespondent[2])
                        {
                            // semnul
                            if (vCorespondent[0] == "+")
                            {
                                // la 4b si 4c sunt mp, operam / 10000
                                if (vCorespondent[1] == "4b" || vCorespondent[1] == "4c")
                                {
                                    vValoareHa += Convert.ToDecimal(vDate[2]) * Convert.ToDecimal(vRandMasura[2]) / 10000;
                                    vValoare += Convert.ToDecimal(vDate[2]) * Convert.ToDecimal(vRandMasura[2]) / 10000 * Convert.ToDecimal(vRandMasura[3]);

                                }
                                else
                                {
                                    vValoareHa += (Convert.ToDecimal(vDate[2]) + Convert.ToDecimal(vDate[3]) / 100) * Convert.ToDecimal(vRandMasura[2]);
                                    vValoare += (Convert.ToDecimal(vDate[2]) + Convert.ToDecimal(vDate[3]) / 100) * Convert.ToDecimal(vRandMasura[2]) * Convert.ToDecimal(vRandMasura[3]);
                                }
                            }
                            else
                            {
                                // la 4b si 4c sunt mp, operam / 10000
                                if (vCorespondent[1] == "4b" || vCorespondent[1] == "4c")
                                {
                                    vValoareHa -= Convert.ToDecimal(vDate[2]) * Convert.ToDecimal(vRandMasura[2]) / 10000;
                                    vValoare -= Convert.ToDecimal(vDate[2]) * Convert.ToDecimal(vRandMasura[2]) / 10000 * Convert.ToDecimal(vRandMasura[3]);
                                }
                                else
                                {
                                    vValoareHa -= (Convert.ToDecimal(vDate[2]) + Convert.ToDecimal(vDate[3]) / 100) * Convert.ToDecimal(vRandMasura[2]);
                                    vValoare -= (Convert.ToDecimal(vDate[2]) + Convert.ToDecimal(vDate[3]) / 100) * Convert.ToDecimal(vRandMasura[2]) * Convert.ToDecimal(vRandMasura[3]);
                                }
                            }
                        }
                    }
                }
                vTotalUDE += vValoare;
                vRandCompletat.Add(vValoareHa.ToString(vFormat));
                vRandCompletat.Add(vValoare.ToString(vFormat));
            }
            else
            {
                vRandCompletat.Add("0,000");
                vRandCompletat.Add("0,000");
            }
            vRanduriCompletate.Add(vRandCompletat);
        }
        List<string> vUltimulRand = new List<string> { "total", "", "", "", "", "", "", vTotalUDE.ToString(vFormat) };
        vRanduriCompletate.Add(vUltimulRand);
        return vRanduriCompletate;
    }
    public static List<List<string>> Masura112CalculTabel2(string pGospodarieId, string pAn)
    {
        // luam toate datele din capitolele care ne intereseaza
        string vSemestru = "";
        if (Convert.ToInt16(DateTime.Now.Month) > 6) vSemestru = "col2";
        else vSemestru = "col1";
        List<string> vCampuri = new List<string> { "codCapitol", "codRand", vSemestru };
        List<List<string>> vDateDinCapitole = ManipuleazaBD.fRezultaListaStringuri("SELECT     codCapitol, codRand, col1, col2 FROM         capitole WHERE     (gospodarieId = " + pGospodarieId + ") AND (codCapitol = '7') AND (an = " + pAn + ")", vCampuri, Convert.ToInt16(pAn));
        // luam datele din sabloaneMasuri pt masura 112
        List<string> vCampuriMasuri = new List<string> { "sablonDescriere", "sablonCorespondenti", "sablonUnitateMasura", "sablonCoeficient", "sablonCodRand", "sablonTabel" };
        List<List<string>> vDateDinMasuri = ManipuleazaBD.fRezultaListaStringuri("SELECT  * FROM sabloaneMasuri WHERE (sablonMasura ='112') AND sablonTabel = '2'", vCampuriMasuri, Convert.ToInt16(pAn));
        // parcurgem lista din sabloane si cautam datele in corespondenti
        List<List<string>> vRanduriCompletate = new List<List<string>> { };
        string vFormat = "0.000";
        decimal vTotalUDE = 0;
        foreach (List<string> vRandMasura in vDateDinMasuri)
        {
            decimal vValoare = 0;
            decimal vValoareCapete = 0;
            List<string> vRandCompletat = vRandMasura;
            if (vRandMasura[1] != "" && vRandMasura[1] != null)
            {
                List<string[]> vCorespondenti = MasuriCorespondenti(vRandMasura[1]);
                vValoare = 0;
                vValoareCapete = 0;
                foreach (string[] vCorespondent in vCorespondenti)
                {
                    foreach (List<string> vDate in vDateDinCapitole)
                    {
                        if (vDate[0] == vCorespondent[1] && vDate[1] == vCorespondent[2])
                        {
                            // semnul
                            if (vCorespondent[0] == "+")
                            {
                                vValoareCapete += Convert.ToDecimal(vDate[2]) * Convert.ToDecimal(vRandMasura[2]);
                                vValoare += Convert.ToDecimal(vDate[2]) * Convert.ToDecimal(vRandMasura[2]) * Convert.ToDecimal(vRandMasura[3]);
                            }
                            else
                            {
                                vValoareCapete -= Convert.ToDecimal(vDate[2]) * Convert.ToDecimal(vRandMasura[2]);
                                vValoare -= Convert.ToDecimal(vDate[2]) * Convert.ToDecimal(vRandMasura[2]) * Convert.ToDecimal(vRandMasura[3]);
                            }
                        }
                    }
                }
                vTotalUDE += vValoare;
                vRandCompletat.Add(vValoareCapete.ToString(vFormat));
                vRandCompletat.Add(vValoare.ToString(vFormat));
            }
            else
            {
                vRandCompletat.Add("0,000");
                vRandCompletat.Add("0,000");
            }
            vRanduriCompletate.Add(vRandCompletat);
        }
        List<string> vUltimulRand = new List<string> { "total", "", "", "", "", "", "", vTotalUDE.ToString(vFormat) };
        vRanduriCompletate.Add(vUltimulRand);
        return vRanduriCompletate;
    }
    public static List<List<string>> Masura141CalculTabel1(string pGospodarieId, string pAn)
    {
        // luam toate datele din capitolele care ne intereseaza
        List<string> vCampuri = new List<string> { "codCapitol", "codRand", "col1", "col2" };
        List<List<string>> vDateDinCapitole = ManipuleazaBD.fRezultaListaStringuri("SELECT     codCapitol, codRand, col1, col2 FROM         capitole WHERE     (gospodarieId = " + pGospodarieId + ") AND (codCapitol IN ('2a', '4a', '4b', '4c', '5b', '5c', '5d')) AND (an = " + pAn + ")", vCampuri, Convert.ToInt16(pAn));
        // luam datele din sabloaneMasuri pt masura 141
        List<string> vCampuriMasuri = new List<string> { "sablonDescriere", "sablonCorespondenti", "sablonUnitateMasura", "sablonCoeficient", "sablonCodRand", "sablonTabel" };
        List<List<string>> vDateDinMasuri = ManipuleazaBD.fRezultaListaStringuri("SELECT  * FROM sabloaneMasuri WHERE (sablonMasura ='141') AND sablonTabel = '1' ORDER BY sablonCodRand", vCampuriMasuri, Convert.ToInt16(pAn));
        // parcurgem lista din sabloane si cautam datele in corespondenti
        List<List<string>> vRanduriCompletate = new List<List<string>> { };
        string vFormat = "0.000";
        decimal vTotalUDE = 0;
        foreach (List<string> vRandMasura in vDateDinMasuri)
        {
            decimal vValoare = 0;
            decimal vValoareHa = 0;
            List<string> vRandCompletat = vRandMasura;
            if (vRandMasura[1] != "" && vRandMasura[1] != null)
            {
                List<string[]> vCorespondenti = MasuriCorespondenti(vRandMasura[1]);
                vValoare = 0;
                vValoareHa = 0;
                foreach (string[] vCorespondent in vCorespondenti)
                {
                    foreach (List<string> vDate in vDateDinCapitole)
                    {
                        if (vDate[0] == vCorespondent[1] && vDate[1] == vCorespondent[2])
                        {
                            // semnul
                            if (vCorespondent[0] == "+")
                            {
                                // la 4b si 4c sunt mp, operam / 10000
                                if (vCorespondent[1] == "4b" || vCorespondent[1] == "4c")
                                {
                                    vValoareHa += Convert.ToDecimal(vDate[2]) * Convert.ToDecimal(vRandMasura[2]) / 10000;
                                    vValoare += Convert.ToDecimal(vDate[2]) * Convert.ToDecimal(vRandMasura[2]) / 10000 * Convert.ToDecimal(vRandMasura[3]);

                                }
                                else
                                {
                                    vValoareHa += (Convert.ToDecimal(vDate[2]) + Convert.ToDecimal(vDate[3]) / 100) * Convert.ToDecimal(vRandMasura[2]);
                                    vValoare += (Convert.ToDecimal(vDate[2]) + Convert.ToDecimal(vDate[3]) / 100) * Convert.ToDecimal(vRandMasura[2]) * Convert.ToDecimal(vRandMasura[3]);
                                }
                            }
                            else
                            {
                                // la 4b si 4c sunt mp, operam / 10000
                                if (vCorespondent[1] == "4b" || vCorespondent[1] == "4c")
                                {
                                    vValoareHa -= Convert.ToDecimal(vDate[2]) * Convert.ToDecimal(vRandMasura[2]) / 10000;
                                    vValoare -= Convert.ToDecimal(vDate[2]) * Convert.ToDecimal(vRandMasura[2]) / 10000 * Convert.ToDecimal(vRandMasura[3]);
                                }
                                else
                                {
                                    vValoareHa -= (Convert.ToDecimal(vDate[2]) + Convert.ToDecimal(vDate[3]) / 100) * Convert.ToDecimal(vRandMasura[2]);
                                    vValoare -= (Convert.ToDecimal(vDate[2]) + Convert.ToDecimal(vDate[3]) / 100) * Convert.ToDecimal(vRandMasura[2]) * Convert.ToDecimal(vRandMasura[3]);
                                }
                            }
                        }
                    }
                }
                vTotalUDE += vValoare;
                vRandCompletat.Add(vValoareHa.ToString(vFormat));
                vRandCompletat.Add(vValoare.ToString(vFormat));
            }
            else
            {
                vRandCompletat.Add("0,000");
                vRandCompletat.Add("0,000");
            }
            vRanduriCompletate.Add(vRandCompletat);
        }
        List<string> vUltimulRand = new List<string> { "total", "", "", "", "", "", "", vTotalUDE.ToString(vFormat) };
        vRanduriCompletate.Add(vUltimulRand);
        return vRanduriCompletate;
    }
    public static List<List<string>> Masura141CalculTabel2(string pGospodarieId, string pAn)
    {
        // luam toate datele din capitolele care ne intereseaza
        string vSemestru = "";
        if (Convert.ToInt16(DateTime.Now.Month) > 6) vSemestru = "col2";
        else vSemestru = "col1";
        List<string> vCampuri = new List<string> { "codCapitol", "codRand", vSemestru };
        List<List<string>> vDateDinCapitole = ManipuleazaBD.fRezultaListaStringuri("SELECT     codCapitol, codRand, col1, col2 FROM         capitole WHERE     (gospodarieId = " + pGospodarieId + ") AND (codCapitol = '7') AND (an = " + pAn + ")", vCampuri, Convert.ToInt16(pAn));
        // luam datele din sabloaneMasuri pt masura 141
        List<string> vCampuriMasuri = new List<string> { "sablonDescriere", "sablonCorespondenti", "sablonUnitateMasura", "sablonCoeficient", "sablonCodRand", "sablonTabel" };
        List<List<string>> vDateDinMasuri = ManipuleazaBD.fRezultaListaStringuri("SELECT  * FROM sabloaneMasuri WHERE (sablonMasura ='141') AND sablonTabel = '2' ORDER BY sablonCodRand", vCampuriMasuri, Convert.ToInt16(pAn));
        // parcurgem lista din sabloane si cautam datele in corespondenti
        List<List<string>> vRanduriCompletate = new List<List<string>> { };
        string vFormat = "0.000";
        decimal vTotalUDE = 0;
        foreach (List<string> vRandMasura in vDateDinMasuri)
        {
            decimal vValoare = 0;
            decimal vValoareCapete = 0;
            List<string> vRandCompletat = vRandMasura;
            if (vRandMasura[1] != "" && vRandMasura[1] != null)
            {
                List<string[]> vCorespondenti = MasuriCorespondenti(vRandMasura[1]);
                vValoare = 0;
                vValoareCapete = 0;
                foreach (string[] vCorespondent in vCorespondenti)
                {
                    foreach (List<string> vDate in vDateDinCapitole)
                    {
                        if (vDate[0] == vCorespondent[1] && vDate[1] == vCorespondent[2])
                        {
                            // semnul
                            if (vCorespondent[0] == "+")
                            {
                                vValoareCapete += Convert.ToDecimal(vDate[2]) * Convert.ToDecimal(vRandMasura[2]);
                                vValoare += Convert.ToDecimal(vDate[2]) * Convert.ToDecimal(vRandMasura[2]) * Convert.ToDecimal(vRandMasura[3]);
                            }
                            else
                            {
                                vValoareCapete -= Convert.ToDecimal(vDate[2]) * Convert.ToDecimal(vRandMasura[2]);
                                vValoare -= Convert.ToDecimal(vDate[2]) * Convert.ToDecimal(vRandMasura[2]) * Convert.ToDecimal(vRandMasura[3]);
                            }
                        }
                    }
                }
                vTotalUDE += vValoare;
                vRandCompletat.Add(vValoareCapete.ToString(vFormat));
                vRandCompletat.Add(vValoare.ToString(vFormat));
            }
            else
            {
                vRandCompletat.Add("0,000");
                vRandCompletat.Add("0,000");
            }
            vRanduriCompletate.Add(vRandCompletat);
        }
        List<string> vUltimulRand = new List<string> { "total", "", "", "", "", "", "", vTotalUDE.ToString(vFormat) };
        vRanduriCompletate.Add(vUltimulRand);
        return vRanduriCompletate;
    }
    public static List<string[]> MasuriCorespondenti(string pCorespondenti)
    {
        List<string[]> vCorespondenti = new List<string[]> { };
        string[] vGrupuri = pCorespondenti.Split('#');
        foreach (string vGrup in vGrupuri)
        {
            string vSemn = vGrup[0].ToString();
            string vCapitol = vGrup.Remove(0, 1).Substring(0, vGrup.IndexOf('^') - 1);
            string vCodRand = vGrup.Remove(0, 2 + vCapitol.Length).Substring(0);
            string[] vRand = { vSemn, vCapitol, vCodRand };
            vCorespondenti.Add(vRand);
        }
        return vCorespondenti;
    }
    public static string CautaTag(string pText)
    {
        string vTagCautat = "";
        int vPozitieInitiala = pText.IndexOf('[');
        int vPozitieFinala = -1;
        if (vPozitieInitiala > -1)
        {
            vPozitieFinala = pText.IndexOf(']', vPozitieInitiala);
            if (vPozitieFinala > vPozitieInitiala)
            {
                vTagCautat = pText.Substring(vPozitieInitiala + 1, vPozitieFinala - vPozitieInitiala - 1);
            }
        }
        return vTagCautat;
    }
    public static void FormeazaSubsol(string pText)
    {

        return;
    }
    public static string FormeazaAntet(string pUnitate, string pSablonAdeverinta, List<string> pCampuri, List<string> pValori, string pAnulCurent, string pUtilizatorId)
    {
        // extragem antetId
        string vAntetId = ManipuleazaBD.fRezultaUnString("SELECT adevAntetId FROM adeverinteSabloane WHERE adevSablonId='" + pSablonAdeverinta + "' ", "adevAntetId", Convert.ToInt16(pAnulCurent));
        string vAntet = "";
        vAntet += ManipuleazaBD.fRezultaUnString("SELECT adevSubsolText FROM adeverinteSubsoluri WHERE adevSubsolId='" + vAntetId + "'", "adevSubsolText", Convert.ToInt16(pAnulCurent)) + "<br />";
        // inlocuim tagurile din form
        int vContor = 0;
        foreach (string vCamp in pCampuri)
        {
            vAntet = vAntet.Replace("[" + vCamp + "]", pValori[vContor]);
            vContor++;
        }
        // inlocuim tagurile din baza de date
        while (CautaTag(vAntet) != "")
        {
            vAntet = vAntet.Replace("[" + CautaTag(vAntet) + "]", InterpreteazaTaguri(CautaTag(vAntet), pUnitate, "", "", pAnulCurent, pUtilizatorId));
        }
        return vAntet.Replace("<link ", "<linkX ").Replace("html>", "htmlX>").Replace("title>", "titleX>").Replace("body>", "bodyX>");
    }
    public static string FormeazaSubsol(string pUnitate, string pSablonAdeverinta, string pAnulCurent, string pUtilizatorId)
    {
        // extragem subsolId
        string vSubsolId = ManipuleazaBD.fRezultaUnString("SELECT adevSubsolId FROM adeverinteSabloane WHERE adevSablonId='" + pSablonAdeverinta + "' ", "adevSubsolId", Convert.ToInt16(pAnulCurent));
        // Col1-3 partea stanga, 4-6 mijloc, 7-9 dreapta; le concatenam
        string vSubsol = "";
        vSubsol += ManipuleazaBD.fRezultaUnString("SELECT adevSubsolText FROM adeverinteSubsoluri WHERE adevSubsolId='" + vSubsolId + "'", "adevSubsolText", Convert.ToInt16(pAnulCurent)) + "<br />";
        // inlocuim tagurile din baza de date
        while (CautaTag(vSubsol) != "")
        {
            vSubsol = vSubsol.Replace("[" + CautaTag(vSubsol) + "]", InterpreteazaTaguri(CautaTag(vSubsol), pUnitate, "", "", pAnulCurent, pUtilizatorId));
        }

        return vSubsol.Replace("<link ", "<linkX ").Replace("html>", "htmlX>").Replace("title>", "titleX>").Replace("body>", "bodyX>");
    }

    public static void PopuleazaGridTagNecunoscut(List<ListaSelect> pTaguriNecunoscute)
    {
    }
    public static string RezultaTextAdeverintaTagNecunoscut(string pText, List<ListaSelect> pTaguriNecunoscute)
    {
        int vContor = 0;
        int vPrimaPozitie = 0;
        int vUltimaPozitie = 0;
        foreach (ListaSelect vTagNecunoscut in pTaguriNecunoscute)
        {
            string vTextDeCautat = "<span class=\"camp_necunoscut\" title=\"" + vTagNecunoscut.Col1 + "\">";
            int test = pText.IndexOf("<span class=\"camp_necunoscut\" title=\"2 necunoscut\">");
            vPrimaPozitie = pText.IndexOf(vTextDeCautat) + 2;
            vUltimaPozitie = pText.IndexOf("</span>", vPrimaPozitie);
            pText = pText.Replace(pText.Substring(vPrimaPozitie, vUltimaPozitie - vPrimaPozitie + 7), "<span class=\"camp_necunoscut\" title=\"" + vTagNecunoscut.Col1 + "\">" + vTagNecunoscut.Col2 + "</span>");
            vContor++;
        }
        return pText;
    }
    public static string StringCapitole(List<string> pListaColoane, List<string> pUM, string pText, string pGospodarie, string pAn, string pCapitol, string pCodRand, out int pAreValoare)
    {
        pAreValoare = 0;
        string vTagInterpretat = "";
        string vSelect = "";
        foreach (string vColoana in pListaColoane)
        {
            if (vColoana != "capitolId") { vSelect += "," + vColoana; }
            else { vSelect += vColoana; }
        }
        List<string> vListaString = ManipuleazaBD.fRezultaUnString("SELECT  " + vSelect + " FROM capitole WHERE gospodarieId ='" + pGospodarie + "' AND codCapitol ='" + pCapitol + "' AND an='" + pAn + "' AND codRand='" + pCodRand + "'", pListaColoane, Convert.ToInt16(pAn));
        Decimal vTerenHa = 0;
        Decimal vTerenAri = 0;
        Int32 vBucati = 0;

        int vContor = 0;
        // calculam vTeren 
        Decimal vColoanaZero = 0;


        foreach (string vColoana in vListaString)
        {
            try
            {
                vColoanaZero = Convert.ToDecimal(vColoana);
            }
            catch
            {
                vColoanaZero = 0;
            }
            if (vContor > 0 && vColoanaZero > 0)
            {
                switch (pUM[vContor])
                {
                    case "ha":
                        vTerenAri += 100 * (Convert.ToDecimal(vColoana));
                        break;
                    case "ari":
                        vTerenAri += Convert.ToDecimal(vColoana);
                        break;
                    case "persoane":
                        break;
                    case "bucati":
                        vBucati += Convert.ToInt32(Convert.ToDecimal(vColoana));
                        break;
                }
                pAreValoare = 1;

            }
            vContor++;
        }
        // primul element din pTexte este cel introductiv gen " terenuri în suprafaţă totală de "
        vTagInterpretat += pText;
        // primul element in pUm da tipul final al UM
        switch (pUM[0])
        {
            case "ha":
                vTerenHa += Math.Truncate(vTerenAri / 100);
                vTagInterpretat += " " + vTerenHa.ToString() + "ha";
                break;
            case "ari":
                vTerenAri += vTerenAri;
                vTagInterpretat += " " + vTerenAri.ToString("F") + "ari";
                break;
            case "ha+ari":
                vTerenHa += Math.Truncate(vTerenAri / 100);
                vTerenAri -= Math.Truncate(vTerenAri / 100) * 100;
                vTagInterpretat += " " + vTerenHa.ToString() + "ha " + vTerenAri.ToString("F") + "ari";
                break;
            case "persoane":
                break;
            case "bucati":
                vTagInterpretat += " " + vBucati.ToString();
                break;
        }
        return vTagInterpretat;

    }

    /// <summary>
    /// Doar Valoarea in decimal fara texte 
    /// </summary>
    /// <param name="pListaColoane"></param>
    /// <param name="pUM"></param>
    /// <param name="pText"></param>
    /// <param name="pGospodarie"></param>
    /// <param name="pAn"></param>
    /// <param name="pCapitol"></param>
    /// <param name="pCodRand"></param>
    /// <param name="pAreValoare"></param>
    /// <returns></returns>
    public static Decimal StringCapitoleFaraText(List<string> pListaColoane, List<string> pUM, string pText, string pGospodarie, string pAn, string pCapitol, string pCodRand, out int pAreValoare)
    {
        pAreValoare = 0;
        Decimal vTagInterpretat = 0;
        string vSelect = "";
        foreach (string vColoana in pListaColoane)
        {
            if (vColoana != "capitolId") { vSelect += "," + vColoana; }
            else { vSelect += vColoana; }
        }
        List<string> vListaString = ManipuleazaBD.fRezultaUnString("SELECT  " + vSelect + " FROM capitole WHERE gospodarieId ='" + pGospodarie + "' AND codCapitol ='" + pCapitol + "' AND an='" + pAn + "' AND codRand='" + pCodRand + "'", pListaColoane, Convert.ToInt16(pAn));
        Decimal vTerenHa = 0;
        Decimal vTerenAri = 0;
        Int32 vBucati = 0;

        int vContor = 0;
        // calculam vTeren 
        Decimal vColoanaZero = 0;


        foreach (string vColoana in vListaString)
        {
            try
            {
                vColoanaZero = Convert.ToDecimal(vColoana);
            }
            catch
            {
                vColoanaZero = 0;
            }
            if (vContor > 0 && vColoanaZero > 0)
            {
                switch (pUM[vContor])
                {
                    case "ha":
                        vTerenAri += 100 * (Convert.ToDecimal(vColoana));
                        break;
                    case "ari":
                        vTerenAri += Convert.ToDecimal(vColoana);
                        break;
                    case "persoane":
                        break;
                    case "bucati":
                        vBucati += Convert.ToInt32(Convert.ToDecimal(vColoana));
                        break;
                }
                pAreValoare = 1;

            }
            vContor++;
        }
        // primul element din pTexte este cel introductiv gen " terenuri în suprafaţă totală de "

        // primul element in pUm da tipul final al UM
        switch (pUM[0])
        {
            case "ha":
                vTerenHa += Math.Truncate(vTerenAri / 100);
                vTagInterpretat = vTerenHa;
                break;
            case "ari":
                vTerenAri += vTerenAri;
                vTagInterpretat = vTerenAri;
                break;
            case "ha+ari":
                vTerenHa += Math.Truncate(vTerenAri / 100);
                vTerenAri -= Math.Truncate(vTerenAri / 100) * 100;
                vTagInterpretat = vTerenHa + vTerenAri / 100;
                break;
            case "persoane":
                break;
            case "bucati":
                vTagInterpretat = vBucati;
                break;
        }
        return vTagInterpretat;

    }

    public static string StringCapitole(List<string> pListaColoane, List<string> pUM, string pText, string pGospodarie, string pAn, string pCapitol, string pCodRand, out int pAreValoare, decimal pAriMinim)
    {
        pAreValoare = 0;
        string vTagInterpretat = "";
        string vSelect = "";
        foreach (string vColoana in pListaColoane)
        {
            if (vColoana != "capitolId") { vSelect += "," + vColoana; }
            else { vSelect += vColoana; }
        }
        List<string> vListaString = ManipuleazaBD.fRezultaUnString("SELECT  " + vSelect + " FROM capitole WHERE gospodarieId ='" + pGospodarie + "' AND codCapitol ='" + pCapitol + "' AND an='" + pAn + "' AND codRand='" + pCodRand + "'", pListaColoane, Convert.ToInt16(pAn));
        Decimal vTerenHa = 0;
        Decimal vTerenAri = 0;
        Int32 vBucati = 0;

        int vContor = 0;
        // calculam vTeren 
        Decimal vColoanaZero = 0;


        foreach (string vColoana in vListaString)
        {
            try
            {
                vColoanaZero = Convert.ToDecimal(vColoana);
            }
            catch
            {
                vColoanaZero = 0;
            }
            if (vContor > 0 && vColoanaZero > 0)
            {
                switch (pUM[vContor])
                {
                    case "ha":
                        vTerenAri += 100 * (Convert.ToDecimal(vColoana));
                        break;
                    case "ari":
                        vTerenAri += Convert.ToDecimal(vColoana);
                        break;
                    case "persoane":
                        break;
                    case "bucati":
                        vBucati += Convert.ToInt32(Convert.ToDecimal(vColoana));
                        break;
                }
                pAreValoare = 1;

            }
            vContor++;
        }
        // primul element din pTexte este cel introductiv gen " terenuri în suprafaţă totală de "
        vTagInterpretat += pText;
        // daca am suprafata < ariMinim nu afisez
        if (vTerenHa * 100 + vTerenAri < pAriMinim)
        {
            vTerenHa = 0;
            vTerenAri = 0;
        }
        // primul element in pUm da tipul final al UM
        switch (pUM[0])
        {
            case "ha":
                vTerenHa += Math.Truncate(vTerenAri / 100);
                vTagInterpretat += " " + vTerenHa.ToString() + "ha";
                break;
            case "ari":
                vTerenAri += vTerenAri;
                vTagInterpretat += " " + vTerenAri.ToString("F") + "ari";
                break;
            case "ha+ari":
                vTerenHa += Math.Truncate(vTerenAri / 100);
                vTerenAri -= Math.Truncate(vTerenAri / 100) * 100;
                vTagInterpretat += " " + vTerenHa.ToString() + "ha " + vTerenAri.ToString("F") + "ari";
                break;
            case "persoane":
                break;
            case "bucati":
                vTagInterpretat += " " + vBucati.ToString();
                break;
        }
        return vTagInterpretat;

    }
    public static string[] CalculeazaAriHa(string pHa1, string pHa2, string pAri1, string pAri2)
    {
        string[] vValori = { "0", "0" };
        if (pHa1 == "") pHa1 = "0";
        if (pHa2 == "") pHa2 = "0";
        if (pAri1 == "") pAri1 = "0";
        if (pAri2 == "") pAri2 = "0";
        decimal vValoare = Convert.ToDecimal(pHa1) * 100 + Convert.ToDecimal(pHa2) * 100 + Convert.ToDecimal(pAri1) + Convert.ToDecimal(pAri2);
        vValori[1] = (vValoare % 100).ToString();
        vValori[0] = Convert.ToInt64((vValoare - (vValoare % 100)) / 100).ToString();
        return vValori;
    }


    public static void RezultaTextAdeverinta(string pUnitate, string pGospodarie, string pMembru, string pSablonAdeverinta, out List<ListaSelect> pListaCampuri, out String pText, string pAnulCurent, string pUtilizatorId)
    {
        string vText = "";
        string vTextTemporar = "";
        string vTextTag = "";
        List<ListaSelect> vListaTaguriNecunoscute = new List<ListaSelect> { };

        vText = "";
        // TITLUL
        //      vText += "<div class=\"adeverinta_titlu\">";
        //     vText += ManipuleazaBD.fRezultaUnString("SELECT adevSablonTitlu FROM adeverinteSabloane WHERE adevSablonId ='" + pSablonAdeverinta + "'", "adevSablonTitlu");
        //     vText += "</div>";
        // TEXTUL PRINCIPAL
        //vText += "<div class=\"adeverinta_text\"><p>";

        vText += "<table class=\"adeverinta_antet\"><tr><td>";
        vTextTemporar += ManipuleazaBD.fRezultaUnString("SELECT adevSablonText FROM adeverinteSabloane WHERE adevSablonId ='" + pSablonAdeverinta + "'", "adevSablonText", Convert.ToInt16(pAnulCurent));
        int vContor = 0;
        while (CautaTag(vTextTemporar) != "")
        {

            string vTagGasit = CautaTag(vTextTemporar);
            if (vTagGasit == "contracte-arenda")
            {
            }

            vTextTag = InterpreteazaTaguri(vTagGasit, pUnitate, pGospodarie, pMembru, pAnulCurent, pUtilizatorId);
            if (vTextTag.IndexOf("<span class=\"camp_necunoscut\" title=\"") != 0)
            {
                vTextTemporar = vTextTemporar.Replace("[" + vTagGasit + "]", vTextTag);
            }
            else
            {
                ListaSelect vTagSelect = new ListaSelect { };
                vTextTemporar = vTextTemporar.Replace("[" + vTagGasit + "]", vTextTag);
                vTagSelect.Col1 = vTagGasit; vTagSelect.Col2 = vTagGasit;
                vListaTaguriNecunoscute.Add(vTagSelect);
                vContor++;
            }
        }
        vText += vTextTemporar + "</tr></td></table>";
        PopuleazaGridTagNecunoscut(vListaTaguriNecunoscute);
        pText = vText.Replace("<link ", "<linkX ").Replace("html>", "htmlX>").Replace("title>", "titleX>").Replace("body>", "bodyX>");
        pListaCampuri = vListaTaguriNecunoscute;
    }

    public static string InterpreteazaTaguri(string pTag, string pUnitate, string pGospodarie, string pValoareSpecifica, string pAnulCurent, string pUtilizatorId)
    {
        string vTagInterpretat = "<span class=\"camp_necunoscut\" title=\"" + pTag + "\">" + pTag + "</span>"; int vContor = 0;
        List<string> vListaStringScrise = new List<string> { };
        List<string> vListaString = new List<string> { };
        List<string> vListaTagInterpretat = new List<string> { };
        List<ListaSelect> vListaMultipla = new List<ListaSelect> { };
        string[] vCampuri = { }; string[] vValori = { }; string[] vWhereCampuri = { }; string[] vWhereValori = { };
        string[] vConditii = { }; string[] vLogici = { }; string[] vRezultate = { };
        int vAreBunuri = 0;

        int vSemestru = 1;
        string vSemestruText = "";

        switch (pTag)
        {

            case "livada":
                SqlConnection vConL = ManipuleazaBD.CreareConexiune(Convert.ToInt16(pAnulCurent));
                SqlCommand vCmdL = new SqlCommand();
                vCmdL.Connection = vConL;

                vCmdL.CommandText = "SELECT       COALESCE (SUM(col1), 0)   AS suma FROM            capitole WHERE        (unitateId = " + pUnitate + ") AND (gospodarieId = " + pGospodarie + ") AND (an = " + pAnulCurent + ") AND (codCapitol = '2a') AND (codRand = 7)";
                decimal vHa = Convert.ToDecimal(vCmdL.ExecuteScalar().ToString());
                vCmdL.CommandText = "SELECT       COALESCE (SUM(col2), 0)   AS suma FROM            capitole WHERE        (unitateId = " + pUnitate + ") AND (gospodarieId = " + pGospodarie + ") AND (an = " + pAnulCurent + ") AND (codCapitol = '2a') AND (codRand = 7)";
                decimal vAri = Convert.ToDecimal(vCmdL.ExecuteScalar().ToString());
                ManipuleazaBD.InchideConexiune(vConL);
                vTagInterpretat = Convert.ToInt64(vHa).ToString() + "ha " + vAri.ToString("F") + "ari";
                break;
            case "cap-gospodarie":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("Select membruNume from gospodarii where  (unitateId = " + pUnitate + ") AND (gospodarieId = " + pGospodarie + ") AND (an = " + pAnulCurent + ")", "membruNume", Convert.ToInt16(pAnulCurent));
                break;
            case "anul":
                vTagInterpretat = pAnulCurent;
                break;
            case "judet":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT judetDenumire FROM unitati INNER JOIN judete ON unitati.judetId= judete.judetId  WHERE unitateId='" + pUnitate + "'", "judetDenumire", Convert.ToInt16(pAnulCurent));
                break;
            case "unitate":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT unitateDenumire FROM unitati WHERE unitateId='" + pUnitate + "'", "unitateDenumire", Convert.ToInt16(pAnulCurent));
                break;
            case "primar":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT unitatePrimar FROM unitati WHERE unitateId='" + pUnitate + "'", "unitatePrimar", Convert.ToInt16(pAnulCurent));
                break;
            case "viceprimar":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT unitateVicePrimar FROM unitati WHERE unitateId='" + pUnitate + "'", "unitateVicePrimar", Convert.ToInt16(pAnulCurent));
                break;
            case "secretar":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT unitateSecretar FROM unitati WHERE unitateId='" + pUnitate + "'", "unitateSecretar", Convert.ToInt16(pAnulCurent));
                break;
            case "contabil":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT unitateContabil FROM unitati WHERE unitateId='" + pUnitate + "'", "unitateContabil", Convert.ToInt16(pAnulCurent));
                break;
            case "sefserviciuagricol":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT unitateSefServiciuAgricol FROM unitati WHERE unitateId='" + pUnitate + "'", "unitateSefServiciuAgricol", Convert.ToInt16(pAnulCurent));
                break;
            case "inspector":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT unitateInspector1 FROM unitati WHERE unitateId='" + pUnitate + "'", "unitateInspector1", Convert.ToInt16(pAnulCurent));
                break;
            case "inspector1":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT unitateInspector1 FROM unitati WHERE unitateId='" + pUnitate + "'", "unitateInspector1", Convert.ToInt16(pAnulCurent));
                break;
            case "inspector2":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT unitateInspector2 FROM unitati WHERE unitateId='" + pUnitate + "'", "unitateInspector2", Convert.ToInt16(pAnulCurent));
                break;
            case "inspector3":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT unitateInspector3 FROM unitati WHERE unitateId='" + pUnitate + "'", "unitateInspector3", Convert.ToInt16(pAnulCurent));
                break;
            case "utilizator":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT utilizatorNume + ' '+ utilizatorPrenume as utilizator  FROM utilizatori WHERE unitateId='" + pUnitate + "' AND utilizatorId = '" + pUtilizatorId + "'", "utilizator", Convert.ToInt16(pAnulCurent));
                break;
            case "adresa-unitate":
                vListaStringScrise = new List<string> { "strada ", "nr.", "ap.", "cod postal " };
                vListaString = new List<string> { "unitateStrada", "unitateNr", "unitateAp", "unitateCodPostal" };
                vListaTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT coalesce(unitateStrada,'') as unitateStrada, coalesce(unitateNr,'') as unitateNr,  coalesce(unitateAp,'') as unitateAp, coalesce(unitateCodPostal,'') as unitateCodPostal FROM unitati WHERE unitateId ='" + pUnitate + "'", vListaString, Convert.ToInt16(pAnulCurent));
                vContor = 0;
                foreach (string vElement in vListaTagInterpretat)
                {
                    if (vElement != "" && vElement != null)
                    {
                        if (vContor == 0) { vTagInterpretat = vListaStringScrise[vContor] + vElement; }
                        else { vTagInterpretat += ",  " + vListaStringScrise[vContor] + vElement; }

                    }
                    vContor++;
                }
                break;
            case "cod-postal-unitate":
                vListaStringScrise = new List<string> { "cod postal " };
                vListaString = new List<string> { "unitateCodPostal" };
                vListaTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT unitateCodPostal FROM unitati WHERE unitateId ='" + pUnitate + "'", vListaString, Convert.ToInt16(pAnulCurent));
                vContor = 0;
                foreach (string vElement in vListaTagInterpretat)
                {
                    if (vElement != "" && vElement != null)
                    {
                        if (vContor == 0) { vTagInterpretat = vListaStringScrise[vContor] + vElement; }
                        else { vTagInterpretat += ",  " + vListaStringScrise[vContor] + vElement; }

                    }
                    vContor++;
                }
                break;
            case "data": vTagInterpretat = DateTime.Now.ToString("dd.MM.yyyy"); break;
            case "telefon-unitate":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT unitateTelefon FROM unitati WHERE unitateId='" + pUnitate + "'", "unitateTelefon", Convert.ToInt16(pAnulCurent));
                if (vTagInterpretat != "" && vTagInterpretat != null)
                {
                    vTagInterpretat = "tel." + vTagInterpretat;
                }
                break;
            case "fax-unitate":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT unitateFax FROM unitati WHERE unitateId='" + pUnitate + "'", "unitateFax", Convert.ToInt16(pAnulCurent));
                if (vTagInterpretat != "" && vTagInterpretat != null)
                {
                    vTagInterpretat = "fax " + vTagInterpretat;
                }
                break;
            case "email-unitate":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT unitateEmail FROM unitati WHERE unitateId='" + pUnitate + "'", "unitateEmail", Convert.ToInt16(pAnulCurent));
                if (vTagInterpretat != "" && vTagInterpretat != null)
                {
                    vTagInterpretat = "email: " + vTagInterpretat;
                }
                break;
            case "email":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT unitateEmail FROM unitati WHERE unitateId='" + pUnitate + "'", "unitateEmail", Convert.ToInt16(pAnulCurent));
                if (vTagInterpretat != "" && vTagInterpretat != null)
                {
                    vTagInterpretat = "email: " + vTagInterpretat;
                }
                break;
            case "web-unitate":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT unitateWeb FROM unitati WHERE unitateId='" + pUnitate + "'", "unitateWeb", Convert.ToInt16(pAnulCurent));
                if (vTagInterpretat != "" && vTagInterpretat != null)
                {
                    vTagInterpretat = " " + vTagInterpretat;
                }
                break;
            case "lista-membri":
                // scoatem lista membrilor
                List<string> vCampuriMembri = new List<string> { "nume" };
                List<List<string>> vValoriMembri = ManipuleazaBD.fRezultaListaStringuri("SELECT nume FROM membri WHERE  gospodarieId='" + pGospodarie + "' AND an = '" + pAnulCurent + "' ORDER BY codRudenie, nume", vCampuriMembri, Convert.ToInt16(pAnulCurent));
                vTagInterpretat = "<table cellspacing='5' border='0' align='center' style='padding:10pt; margin:1pt; clear:both;'>";
                foreach (List<string> vMembru in vValoriMembri)
                {
                    vTagInterpretat += "<tr><td>" + vMembru[0] + "</td></tr>";
                }
                vTagInterpretat += "</table>";
                break;
            case "lista-membri-enumerati":
                // scoatem lista membrilor
                vCampuriMembri = new List<string> { "nume" };
                vValoriMembri = ManipuleazaBD.fRezultaListaStringuri("SELECT nume FROM membri WHERE  gospodarieId='" + pGospodarie + "' AND an = '" + pAnulCurent + "' ORDER BY codRudenie, nume", vCampuriMembri, Convert.ToInt16(pAnulCurent));

                foreach (List<string> vMembru in vValoriMembri)
                {
                    vTagInterpretat += vMembru[0] + ",";
                }
                break;
            case "lista-membri-rudenie":
                // scoatem lista membrilor
                vCampuriMembri = new List<string> { "nume", "denumireRudenie" };
                vValoriMembri = ManipuleazaBD.fRezultaListaStringuri("SELECT nume, denumireRudenie FROM membri WHERE  gospodarieId='" + pGospodarie + "' AND an = '" + pAnulCurent + "' ORDER BY codRudenie, nume", vCampuriMembri, Convert.ToInt16(pAnulCurent));
                vTagInterpretat = "<table cellspacing='5' border='0' align='center' style='padding:10pt; margin:1pt; clear:both;'>";
                foreach (List<string> vMembru in vValoriMembri)
                {
                    vTagInterpretat += "<tr><td>" + vMembru[0] + "</td><td>" + vMembru[1] + "</td></tr>";
                }
                vTagInterpretat += "</table>";
                break;
            case "lista-membri-rudenie-data-nasterii":
                // scoatem lista membrilor
                vCampuriMembri = new List<string> { "nume", "dataNasterii", "denumireRudenie" };
                vValoriMembri = ManipuleazaBD.fRezultaListaStringuri("SELECT nume, dataNasterii, denumireRudenie FROM membri WHERE  gospodarieId='" + pGospodarie + "' AND an = '" + pAnulCurent + "' ORDER BY codRudenie, nume", vCampuriMembri, Convert.ToInt16(pAnulCurent));
                vTagInterpretat = "<table cellspacing='5' border='0' align='center' style='padding:10pt; margin:1pt; clear:both;'>";
                foreach (List<string> vMembru in vValoriMembri)
                {
                    vTagInterpretat += "<tr><td>" + vMembru[0] + "</td><td>" + vMembru[2] + "</td><td> data naşterii: " + vMembru[1].Remove(10) + "</td></tr>";
                }
                vTagInterpretat += "</table>";
                break;
            case "lista-membri-rudenie-data-nasterii-cnp":
                // scoatem lista membrilor
                vCampuriMembri = new List<string> { "nume", "dataNasterii", "denumireRudenie", "cnp" };
                vValoriMembri = ManipuleazaBD.fRezultaListaStringuri("SELECT nume, dataNasterii, cnp, denumireRudenie FROM membri WHERE  gospodarieId='" + pGospodarie + "' AND an = '" + pAnulCurent + "' ORDER BY codRudenie, nume", vCampuriMembri, Convert.ToInt16(pAnulCurent));
                vTagInterpretat = "<table cellspacing='5' border='0' align='center' style='padding:10pt; margin:1pt; clear:both;'>";
                foreach (List<string> vMembru in vValoriMembri)
                {
                    vTagInterpretat += "<tr><td>" + vMembru[0] + "</td><td>" + vMembru[2] + "</td><td> data naşterii: " + vMembru[1].Remove(10) + "</td><td> CNP: " + vMembru[3] + "</td></tr>";
                }
                vTagInterpretat += "</table>";
                break;
            case "nume":
                // verificam daca e PJ
                string vPJ = ManipuleazaBD.fRezultaUnString("SELECT * FROM gospodarii WHERE gospodarieId =" + pGospodarie, "jUnitate", Convert.ToInt16(pAnulCurent));
                if (vPJ != "" && vPJ != null)
                    vTagInterpretat = vPJ;
                else
                    vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT nume FROM membri WHERE  capitolId='" + pValoareSpecifica + "'", "nume", Convert.ToInt16(pAnulCurent));
                break;

            case "membru":
                // verificam daca e PJ
                vPJ = ManipuleazaBD.fRezultaUnString("SELECT * FROM gospodarii WHERE gospodarieId =" + pGospodarie, "jUnitate", Convert.ToInt16(pAnulCurent));
                if (vPJ != "" && vPJ != null)
                {
                    vTagInterpretat = vPJ;
                    vTagInterpretat += ",reprezentat prin ";
                    vTagInterpretat += ManipuleazaBD.fRezultaUnString("SELECT nume FROM membri WHERE  gospodarieId='" + pGospodarie + "' and codRudenie ='1'", "nume", Convert.ToInt16(pAnulCurent));
                }
                else
                    vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT nume FROM membri WHERE  capitolId='" + pValoareSpecifica + "'", "nume", Convert.ToInt16(pAnulCurent));
                break;
            case "cui_cnp":
                List<string> vCampuriGospodarii = new List<string> { "persJuridica", "gospodarieCui", "jCodFiscal" };
                List<List<string>> vDateGospodarie = ManipuleazaBD.fRezultaListaStringuri("SELECT coalesce(persJuridica, '') as persJuridica,coalesce(gospodarieCui, '') as gospodarieCui, coalesce(jCodFiscal, '') as jCodFiscal  FROM gospodarii WHERE gospodarieId = '" + pGospodarie + "' and an = '" + pAnulCurent + "'", vCampuriGospodarii, Convert.ToInt16(pAnulCurent));
                if (vDateGospodarie[0][0] == "False" && vDateGospodarie[0][1] != null && vDateGospodarie[0][1] != "" && vDateGospodarie[0][1] != "-")
                {
                    vTagInterpretat = vDateGospodarie[0][1];
                }
                else if (vDateGospodarie[0][0] == "True")
                {
                    vTagInterpretat = vDateGospodarie[0][2];
                }
                else
                {
                    vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT * FROM membri WHERE gospodarieId = '" + pGospodarie + "' AND  capitolid= '" + pValoareSpecifica + "'", "cnp", Convert.ToInt16(pAnulCurent));
                }

                break;
            case "data-nasterii":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT dataNasterii FROM membri WHERE unitateId='" + pUnitate + "' AND gospodarieId ='" + pGospodarie + "' AND capitolId='" + pValoareSpecifica + "'", "dataNasterii", Convert.ToInt16(pAnulCurent));
                try { vTagInterpretat = Convert.ToDateTime(vTagInterpretat).ToString("dd.MM.yyyy"); }
                catch { }
                break;
            case "localitate":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT CASE strainas WHEN 1 THEN slocalitate ELSE localitate END AS localitate FROM gospodarii WHERE unitateId='" + pUnitate + "' AND gospodarieId ='" + pGospodarie + "'", "localitate", Convert.ToInt16(pAnulCurent));
                break;
            case "localitate-proprietate":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT  localitate FROM gospodarii WHERE unitateId='" + pUnitate + "' AND gospodarieId ='" + pGospodarie + "'", "localitate", Convert.ToInt16(pAnulCurent));
                break;
            case "localitate-unitate":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT [localitati].[localitateDenumire] AS localitatea  FROM gospodarii INNER JOIN unitati ON [gospodarii].[unitateId] = [unitati].[unitateId] INNER JOIN localitati ON [unitati].[localitateId] = [localitati].[localitateId] WHERE [gospodarii].[unitateId]='" + pUnitate + "' AND gospodarieId ='" + pGospodarie + "'", "localitatea", Convert.ToInt16(pAnulCurent));
                break;
            case "localitate-rang":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT [unitateLocalitateRang] FROM [unitati] WHERE [unitateId]='" + pUnitate + "'", "unitateLocalitateRang", Convert.ToInt16(pAnulCurent));
                switch (vTagInterpretat)
                {
                    case "oras":
                        vTagInterpretat = "oraşului ";
                        break;
                    case "comuna":
                        vTagInterpretat = "comunei ";
                        break;
                    case "municipiu":
                        vTagInterpretat = "municipiului ";
                        break;
                    case "sat":
                        vTagInterpretat = "satului ";
                        break;
                }
                break;
            case "localitate-unitate-sat":
                List<string> vStringLocalitati = new List<string> { "localitatea", "satul" };
                List<string> vTaguriLocalitati = new List<string> { };
                vTaguriLocalitati = ManipuleazaBD.fRezultaUnString("SELECT [localitati].[localitateDenumire] AS localitatea, [gospodarii].[localitate] AS satul  FROM gospodarii INNER JOIN unitati ON [gospodarii].[unitateId] = [unitati].[unitateId] INNER JOIN localitati ON [unitati].[localitateId] = [localitati].[localitateId] WHERE [gospodarii].[unitateId]='" + pUnitate + "' AND gospodarieId ='" + pGospodarie + "'", vStringLocalitati, Convert.ToInt16(pAnulCurent));
                vTagInterpretat = "localitatea " + vTaguriLocalitati[0];
                if (vTaguriLocalitati[1] != "" && vTaguriLocalitati[1] != null) { vTagInterpretat += ", satul " + vTaguriLocalitati[1]; }
                break;
            case "volum":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT volum FROM gospodarii WHERE unitateId='" + pUnitate + "' AND gospodarieId ='" + pGospodarie + "'", "volum", Convert.ToInt16(pAnulCurent));
                break;
            case "pozitia":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT nrPozitie FROM gospodarii WHERE unitateId='" + pUnitate + "' AND gospodarieId ='" + pGospodarie + "'", "nrPozitie", Convert.ToInt16(pAnulCurent));
                break;
            case "lista_parcele_proprietate":
                List<string> vColoaneParcele = new List<string> { "ari", "categoria", "parcelaNrBloc" };
                List<List<string>> vParcele = ManipuleazaBD.fRezultaListaStringuri("SELECT     CONVERT(DECIMAL(10, 0), parcele.parcelaSuprafataIntravilanHa) * 100 + CONVERT(DECIMAL(10, 0), parcele.parcelaSuprafataExtravilanHa)*100 + CONVERT(DECIMAL(10, 2), REPLACE(parcele.parcelaSuprafataIntravilanAri, ',', '.')) + CONVERT(DECIMAL(10, 2), REPLACE(parcele.parcelaSuprafataExtravilanAri, ',', '.')) AS ari,COALESCE(parcele.parcelaNrBloc,'') as parcelaNrBloc, sabloaneCapitole.denumire4 as categoria FROM parcele INNER JOIN sabloaneCapitole ON parcele.parcelaCategorie = sabloaneCapitole.codRand WHERE     (parcele.unitateaId = '" + pUnitate + "') AND (parcele.gospodarieId = '" + pGospodarie + "') AND (parcele.an = '" + pAnulCurent + "') AND (sabloaneCapitole.an = '" + pAnulCurent + "') AND (sabloaneCapitole.capitol = '2a') AND (sabloaneCapitole.denumire4 <> '') AND (CHARINDEX('+', sabloaneCapitole.formula) = 0) AND (CHARINDEX('-', sabloaneCapitole.formula) = 0) ORDER BY parcelaNrBloc", vColoaneParcele, Convert.ToInt16(pAnulCurent));
                vTagInterpretat = "<table cellspacing='0' cellpadding='0' style='width:100%; margin:0; padding:0; text-align:right;'>";
                if (vParcele.Count > 0)
                {
                    foreach (List<string> vParcela in vParcele)
                    {
                        vTagInterpretat += "<tr><td style='width:21pt; border:1px solid Black;margin:0; padding:0;'>" + CalculeazaAriHa("0", "0", vParcela[0], "0")[0] + "</td><td style='margin:0; padding:0;width:30pt; border:1pt solid Black;'>" + CalculeazaAriHa("0", "0", vParcela[0], "0")[1] + "</td><td style='margin:0; padding:0;width:150pt; border:1pt solid Black;'>" + vParcela[1] + "</td><td style='margin:0; padding:0;width:80pt; border:1pt solid Black;'>" + vParcela[2] + "</td></tr>";
                    }
                }
                else
                {
                    for (int celule = 0; celule < 4; celule++)
                    {
                        vTagInterpretat += "<tr><td style='width:21pt; border:1px solid Black;margin:0; padding:0;'>&nbsp;</td><td style='margin:0; padding:0;width:30pt; border:1pt solid Black;'>&nbsp;</td><td style='margin:0; padding:0;width:150pt; border:1pt solid Black;'>&nbsp;</td><td style='margin:0; padding:0;width:80pt; border:1pt solid Black;'>&nbsp;</td></tr>";
                    }
                }
                vTagInterpretat += "</table>";
                break;

            case "contracte-arenda":
                vTagInterpretat = "";
                List<List<string>> contracteArenda = new List<List<string>>();
                List<string> coloaneContracte = new List<string> { "nrCrt", "arendasArendator", "contractNr", "contractDataSemnarii", "periaodaArendare", "suprafataHa", "parcelaCategorie", "parcelaNrBloc" };
                contracteArenda = ManipuleazaBD.fRezultaListaStringuri(@"SELECT row_number() over (order by parceleAtribuiriContracte.contractid) as nrCrt,
(CASE gospodarii.persJuridica WHEN 0 THEN gospodarii.membruNume ELSE gospodarii.jUnitate END  + ' / ' + CASE  gospodarii_1.persJuridica WHEN 0 THEN  gospodarii_1.membruNume ELSE gospodarii_1.jUnitate END) as arendasArendator ,
 parceleAtribuiriContracte.contractNr,
 Convert(varchar,parceleAtribuiriContracte.contractDataSemnarii,104) as contractDataSemnarii,
 (Convert(varchar,parceleAtribuiriContracte.contractDataInceput,104) + ' - ' + Convert(varchar,parceleAtribuiriContracte.contractDataFinal,104)) as periaodaArendare, 
                        Convert(decimal(18,4),SUM(parceleModUtilizare.c3Ha) + ROUND(SUM(parceleModUtilizare.c3Ari) / 100, 0, 1) +( SUM(parceleModUtilizare.c3Ari) - ROUND(SUM(parceleModUtilizare.c3Ari) / 100, 0, 1) * 100 )/ 100 )AS suprafataHa
                     , CASE parcelaCategorie
   WHEN 1 THEN 'Teren arabil (inclusiv sere si solarii)'
   WHEN 2 THEN 'Pasuni naturale' 
   WHEN 3 THEN 'Fânete naturale'
   WHEN 4 THEN 'Vii, pepiniere viticole si hameisti'
   WHEN 7 THEN 'Livezi de pomi, pepiniere, arbusti fructiferi'
   WHEN 9 THEN 'Gradini familiale'
   WHEN 11 THEN 'Paduri si alte terenuri cu vegetatie forestiera'
   WHEN 13 THEN 'Drumuri si cai ferate'
   WHEN 14 THEN 'Constructii'
   WHEN 15 THEN 'Terenuri degradate si neproductive'
   WHEN 16 THEN 'Ape si balti'
	end AS parcelaCategorie
					 ,parcele.parcelaNrBloc
  FROM parceleAtribuiriContracte
 INNER JOIN gospodarii ON gospodarii.gospodarieId = parceleAtribuiriContracte.contractGospodarieIdPrimeste 
 INNER JOIN gospodarii AS gospodarii_1 ON parceleAtribuiriContracte.contractGospodarieIdDa = gospodarii_1.gospodarieId 
 INNER JOIN parceleModUtilizare  ON parceleModUtilizare.contractId = parceleAtribuiriContracte.contractId AND parceleAtribuiriContracte.contractActiv = 1
 INNER JOIN parcele on parcele.parcelaId = parceleModUtilizare.parcelaId
 INNER JOIN sabloaneCapitole ON sabloaneCapitole.codRand = parceleModUtilizare.c3Rand  

  WHERE 
        (sabloaneCapitole.capitol = '3') 
      --  AND parceleAtribuiriContracte.contractTip in (3,10) 
        AND (sabloaneCapitole.an = " + pAnulCurent + @")  
        AND (sabloaneCapitole.an = " + pAnulCurent + @")
        AND (gospodarii.gospodarieId =  " + pGospodarie + " or gospodarii_1.gospodarieId = " + pGospodarie + @") 
        AND YEAR(parceleAtribuiriContracte.contractDataFinal) >= " + pAnulCurent + @"
  GROUP BY gospodarii_1.gospodarieId, gospodarii.gospodarieId, parceleAtribuiriContracte.contractNr, parcelaCategorie,parceleAtribuiriContracte.contractDataSemnarii,parceleAtribuiriContracte.contractid,parceleAtribuiriContracte.contractDataInceput, parceleAtribuiriContracte.contractDataFinal,redeventa,gospodarii.membruNume,gospodarii_1.membruNume,parcele.parcelaNrBloc,  gospodarii.jUnitate,gospodarii.persJuridica,  gospodarii_1.jUnitate,gospodarii_1.persJuridica", coloaneContracte, Convert.ToInt16(pAnulCurent));
                //  and parceleAtribuiriContracte.contractTip in (3,10)
                if (contracteArenda.Count > 0)
                {

                    foreach (List<string> item in contracteArenda)
                    {
                        vTagInterpretat += "<tr><td style='width:21pt;font size=4; border:1px solid Black;margin:0; padding:0;'>" + item[0] + "</td><td style='margin:0; font size=4;padding:0;width:150pt; border:1pt solid Black;'>" + item[1] + "</td><td style='margin:0; font size=4;padding:0;width:80pt; border:1pt solid Black;'>" + item[2] + "</td><td style='margin:0;font size=4; padding:0;width:80pt; border:1pt solid Black;'>" + item[3] + "</td><td style='margin:0; font size=4;padding:0;width:80pt; border:1pt solid Black;'>" + item[4] + "</td><td style='margin:0;font size=4; padding:0;width:80pt; border:1pt solid Black;'>" + item[5] + "</td><td style='margin:0;font size=4; padding:0;width:80pt; border:1pt solid Black;'>" + item[6] + "</td><td style='margin:0; font size=4;padding:0;width:80pt; border:1pt solid Black;'>" + item[7] + "</td></tr>";
                    }
                }
                else {
                    vTagInterpretat += "<tr><td style='width:21pt;font size=4; border:1px solid Black;margin:0; padding:0;' colspan='8'>- Nu exista inregistrari. -</td></tr>";
                }
                break;
            case "lista_parcele_proprietate-apia":
                List<string> vColoaneParceleApia = new List<string> { "ari", "categoria", "parcelaNrBloc", "parcelaNrTopo", "parcelaTitluProprietate" };
                List<List<string>> vParceleApia = ManipuleazaBD.fRezultaListaStringuri("SELECT     CONVERT(DECIMAL(10, 0), parcele.parcelaSuprafataIntravilanHa) * 100 + CONVERT(DECIMAL(10, 0), parcele.parcelaSuprafataExtravilanHa)*100 + CONVERT(DECIMAL(10, 2), REPLACE(parcele.parcelaSuprafataIntravilanAri, ',', '.')) + CONVERT(DECIMAL(10, 2), REPLACE(parcele.parcelaSuprafataExtravilanAri, ',', '.')) AS ari,COALESCE(parcele.parcelaNrBloc,'') as parcelaNrBloc, sabloaneCapitole.denumire4 as categoria,COALESCE(parcele.parcelaNrTopo,'') as parcelaNrTopo,COALESCE(parcele.parcelaTitluProprietate,'') as parcelaTitluProprietate FROM parcele INNER JOIN sabloaneCapitole ON parcele.parcelaCategorie = sabloaneCapitole.codRand WHERE     (parcele.unitateaId = '" + pUnitate + "') AND (parcele.gospodarieId = '" + pGospodarie + "') AND (parcele.an = '" + pAnulCurent + "') AND (sabloaneCapitole.an = '" + pAnulCurent + "') AND (sabloaneCapitole.capitol = '2a') AND (sabloaneCapitole.denumire4 <> '') AND (CHARINDEX('+', sabloaneCapitole.formula) = 0) AND (CHARINDEX('-', sabloaneCapitole.formula) = 0) ORDER BY parcelaNrBloc", vColoaneParceleApia, Convert.ToInt16(pAnulCurent));
                vTagInterpretat = "<table cellspacing='0' cellpadding='0' style='width:100%; margin:0; padding:0; text-align:right;'>";
                if (vParceleApia.Count > 0)
                {
                    foreach (List<string> vParcela in vParceleApia)
                    {
                        vTagInterpretat += "<tr><td style='width:30pt; border:1px solid Black;margin:0; padding:0;'>" + CalculeazaAriHa("0", "0", vParcela[0], "0")[0] + "</td><td style='margin:0; padding:0;width:30pt; border:1pt solid Black;'>" + CalculeazaAriHa("0", "0", vParcela[0], "0")[1] + "</td><td style='margin:0; padding:0;width:280pt; border:1pt solid Black;'>" + vParcela[1] + "</td><td style='margin:0; padding:0;width:150pt; border:1pt solid Black;'>" + vParcela[2] + "</td><td style='margin:0; padding:0;width:150pt; border:1pt solid Black;'>" + vParcela[3] + "</td><td style='margin:0; padding:0;width:150pt; border:1pt solid Black;'>" + vParcela[4] + "</td></tr>";
                    }
                }
                else
                {
                    for (int celule = 0; celule < 6; celule++)
                    {
                        vTagInterpretat += "<tr><td style='width:21pt; border:1px solid Black;margin:0; padding:0;'>&nbsp;</td><td style='margin:0; padding:0;width:30pt; border:1pt solid Black;'>&nbsp;</td><td style='margin:0; padding:0;width:150pt; border:1pt solid Black;'>&nbsp;</td><td style='margin:0; padding:0;width:80pt; border:1pt solid Black;'>&nbsp;</td></tr>";
                    }
                }

                vTagInterpretat += "</table>";
                break;
            case "numar-rol":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("select nrRolNominal from gospodarii where gospodarieId = " + pGospodarie + " and an=" + pAnulCurent + " and unitateId = " + pUnitate + "", "nrRolNominal", Convert.ToInt16(pAnulCurent));
                break;
            case "lista_parcele_proprietate_doarcublocfizic":
                vColoaneParcele = new List<string> { "ari", "categoria", "parcelaNrBloc" };
                vParcele = ManipuleazaBD.fRezultaListaStringuri("SELECT     CONVERT(DECIMAL(10, 0), parcele.parcelaSuprafataIntravilanHa) * 100 + CONVERT(DECIMAL(10, 0), parcele.parcelaSuprafataExtravilanHa)*100 + CONVERT(DECIMAL(10, 2), REPLACE(parcele.parcelaSuprafataIntravilanAri, ',', '.')) + CONVERT(DECIMAL(10, 2), REPLACE(parcele.parcelaSuprafataExtravilanAri, ',', '.')) AS ari,COALESCE(parcele.parcelaNrBloc,'') as parcelaNrBloc, sabloaneCapitole.denumire4 as categoria FROM parcele INNER JOIN sabloaneCapitole ON parcele.parcelaCategorie = sabloaneCapitole.codRand WHERE     (parcele.unitateaId = '" + pUnitate + "') AND (parcele.gospodarieId = '" + pGospodarie + "') AND (parcele.an = '" + pAnulCurent + "') AND (sabloaneCapitole.an = '" + pAnulCurent + "') AND (sabloaneCapitole.capitol = '2a') AND (sabloaneCapitole.denumire4 <> '') AND (CHARINDEX('+', sabloaneCapitole.formula) = 0) AND (CHARINDEX('-', sabloaneCapitole.formula) = 0) AND (parcele.parcelaNrBloc IS NOT NULL)  AND (parcele.parcelaNrBloc <> '')  ORDER BY parcelaNrBloc", vColoaneParcele, Convert.ToInt16(pAnulCurent));
                vTagInterpretat = "<table cellspacing='0' cellpadding='0' style='width:100%; margin:0; padding:0; text-align:right;'>";
                if (vParcele.Count > 0)
                {
                    foreach (List<string> vParcela in vParcele)
                    {
                        vTagInterpretat += "<tr><td style='width:21pt; border:1px solid Black;margin:0; padding:0;'>" + CalculeazaAriHa("0", "0", vParcela[0], "0")[0] + "</td><td style='margin:0; padding:0;width:30pt; border:1pt solid Black;'>" + CalculeazaAriHa("0", "0", vParcela[0], "0")[1] + "</td><td style='margin:0; padding:0;width:150pt; border:1pt solid Black;'>" + vParcela[1] + "</td><td style='margin:0; padding:0;width:80pt; border:1pt solid Black;'>" + vParcela[2] + "</td></tr>";
                    }
                }
                else
                {
                    for (int celule = 0; celule < 4; celule++)
                    {
                        vTagInterpretat += "<tr><td style='width:21pt; border:1px solid Black;margin:0; padding:0;'>&nbsp;</td><td style='margin:0; padding:0;width:30pt; border:1pt solid Black;'>&nbsp;</td><td style='margin:0; padding:0;width:150pt; border:1pt solid Black;'>&nbsp;</td><td style='margin:0; padding:0;width:80pt; border:1pt solid Black;'>&nbsp;</td></tr>";
                    }
                }

                vTagInterpretat += "</table>";
                break;
            case "lista_parcele_arenda":
                vTagInterpretat = "<table cellspacing='0' cellpadding='0' style='width:100%; margin:0; padding:0; text-align:left;'>";

                vColoaneParcele = new List<string> { "suprafata", "parcelaNrBloc", "denumire1" };
                vParcele = ManipuleazaBD.fRezultaListaStringuri(@"SELECT parceleAtribuiriContracte.contractGospodarieIdPrimeste,  parcele.parcelaNrBloc, (SUM(parceleModUtilizare.c3Ha)*100 + SUM(parceleModUtilizare.c3Ari))/100 AS suprafata, sabloaneCapitole.denumire1, parcele.parcelaCategorie FROM         parcele INNER JOIN   parceleModUtilizare ON parcele.parcelaId = parceleModUtilizare.parcelaId INNER JOIN parceleAtribuiriContracte ON parceleModUtilizare.contractId = parceleAtribuiriContracte.contractId AND parceleAtribuiriContracte.contractActiv = 1 INNER JOIN sabloaneCapitole ON parcele.parcelaCategorie = sabloaneCapitole.codRand WHERE     (parceleAtribuiriContracte.contractGospodarieIdPrimeste = '" + pGospodarie + "') AND (parcele.an = '" + pAnulCurent + "') AND (sabloaneCapitole.an = '" + pAnulCurent + "') AND (sabloaneCapitole.capitol = '2a') AND (parceleAtribuiriContracte.contractDataInceput <= CONVERT(date, GETDATE(), 104)) AND (parceleAtribuiriContracte.contractDataFinal >= CONVERT(date, GETDATE(), 104))GROUP BY sabloaneCapitole.denumire1, parceleAtribuiriContracte.contractGospodarieIdPrimeste,  parcele.parcelaNrBloc, parcele.parcelaCategorie ORDER BY parcele.parcelaNrBloc, sabloaneCapitole.denumire1", vColoaneParcele, Convert.ToInt16(pAnulCurent));

                List<List<string>> vParceleDate = ManipuleazaBD.fRezultaListaStringuri(@"SELECT parceleAtribuiriContracte.contractGospodarieIdDa,  parcele.parcelaNrBloc, (SUM(parceleModUtilizare.c3Ha)*100 + SUM(parceleModUtilizare.c3Ari))/100 AS suprafata, sabloaneCapitole.denumire1, parcele.parcelaCategorie FROM         parcele INNER JOIN   parceleModUtilizare ON parcele.parcelaId = parceleModUtilizare.parcelaId INNER JOIN parceleAtribuiriContracte ON parceleModUtilizare.contractId = parceleAtribuiriContracte.contractId AND parceleAtribuiriContracte.contractActiv = 1 INNER JOIN sabloaneCapitole ON parcele.parcelaCategorie = sabloaneCapitole.codRand WHERE     (parceleAtribuiriContracte.contractGospodarieIdDa = '" + pGospodarie + "') AND (parcele.an = '" + pAnulCurent + "') AND (sabloaneCapitole.an = '" + pAnulCurent + "') AND (sabloaneCapitole.capitol = '2a') AND (parceleAtribuiriContracte.contractDataInceput <= CONVERT(date, GETDATE(), 104)) AND (parceleAtribuiriContracte.contractDataFinal >= CONVERT(date, GETDATE(), 104)) AND (sabloaneCapitole.codRand in (1,2,3,4,7,9,15,16)) GROUP BY sabloaneCapitole.denumire1, parceleAtribuiriContracte.contractGospodarieIdDa,  parcele.parcelaNrBloc, parcele.parcelaCategorie ORDER BY parcele.parcelaNrBloc, sabloaneCapitole.denumire1", vColoaneParcele, Convert.ToInt16(pAnulCurent));

                List<string> vColoaneParceleProprietate = new List<string> { "suprafata", "parcelaNrBloc", "categoria" };
                List<List<string>> vParceleProprietate = ManipuleazaBD.fRezultaListaStringuri(@"SELECT COALESCE (parcele.parcelaNrBloc, N'') AS parcelaNrBloc, sabloaneCapitole.denumire4 AS categoria,( (SUM(parcele.parcelaSuprafataIntravilanHa)*100 + SUM(parcele.parcelaSuprafataIntravilanAri)) + SUM(parcele.parcelaSuprafataExtravilanHa) * 100 + SUM(parcele.parcelaSuprafataExtravilanAri) )/100 AS suprafata FROM parcele INNER JOIN  sabloaneCapitole ON parcele.parcelaCategorie = sabloaneCapitole.codRand WHERE (parcele.unitateaId = '" + pUnitate + "') AND (parcele.gospodarieId = '" + pGospodarie + "') AND (parcele.an = '" + pAnulCurent + "') AND (sabloaneCapitole.an = '" + pAnulCurent + "') AND (sabloaneCapitole.capitol = '2a') AND (sabloaneCapitole.denumire4 <> '') AND (CHARINDEX('+', sabloaneCapitole.formula) = 0) AND (CHARINDEX('-', sabloaneCapitole.formula) = 0) AND (sabloaneCapitole.codRand in (1,2,3,4,7,9,15,16)) GROUP BY parcele.parcelaNrBloc, sabloaneCapitole.denumire4", vColoaneParceleProprietate, Convert.ToInt16(pAnulCurent));
                List<List<string>> vParceleDeScris = new List<List<string>> { };

                int vNrParcele = 0;
                // scadem din proprietate cele date
                foreach (List<string> vParcelaProprietate in vParceleProprietate)
                {
                    vNrParcele = 0;
                    foreach (List<string> vParcelaData in vParceleDate)
                    {
                        List<string> vParcelaDeScris = new List<string> { };
                        if (vParcelaData[1] == vParcelaProprietate[1] && vParcelaData[2] == vParcelaProprietate[2])
                        {
                            vNrParcele++;
                            decimal vSuprafata = Convert.ToDecimal(vParcelaProprietate[0]) - Convert.ToDecimal(vParcelaData[0]);
                            if (vSuprafata > 0)
                            {
                                vParcelaDeScris.Add(vSuprafata.ToString());
                                vParcelaDeScris.Add(vParcelaProprietate[1]);
                                vParcelaDeScris.Add(vParcelaProprietate[2]);
                                vParceleDeScris.Add(vParcelaDeScris);
                            }
                            break;
                        }
                        else
                        {
                            //
                        }

                    }
                    if (vNrParcele == 0)
                        vParceleDeScris.Add(vParcelaProprietate);
                }
                List<List<string>> vParceleDeScris1 = new List<List<string>> { };
                List<List<string>> vParceleDeScrisDeScos = new List<List<string>> { };
                List<List<string>> vParceleDeScos = new List<List<string>> { };

                vNrParcele = 0;

                foreach (List<string> vParcela in vParceleDeScris)
                {
                    vNrParcele = 0;
                    foreach (List<string> vParcelaPrimita in vParcele)
                    {
                        List<string> vParcelaDeScris = new List<string> { };
                        if (vParcela[1] == vParcelaPrimita[1] && vParcela[2] == vParcelaPrimita[2])
                        {
                            vNrParcele++;
                            vParcelaDeScris.Add((Convert.ToDecimal(vParcelaPrimita[0]) + Convert.ToDecimal(vParcela[0])).ToString());
                            vParcelaDeScris.Add(vParcela[1]);
                            vParcelaDeScris.Add(vParcela[2]);
                            vParceleDeScris1.Add(vParcelaDeScris);
                            vParceleDeScos.Add(vParcelaPrimita);
                            vParceleDeScrisDeScos.Add(vParcela);
                            break;
                        }
                    }
                    if (vNrParcele == 0)
                        vParceleDeScris1.Add(vParcela);
                }
                foreach (List<string> vParcela in vParceleDeScos)
                {
                    vParcele.Remove(vParcela);
                }
                foreach (List<string> vParcelaScrisa in vParceleDeScris1)
                {
                    vTagInterpretat += "<tr><td style='margin:0; padding:0 5pt; border:0pt solid Black;'>BF " + vParcelaScrisa[1] + "</td><td style=' border:0px solid Black;margin:0; padding:0 5pt;'>" + vParcelaScrisa[0].Remove(vParcelaScrisa[0].Length - 2) + " ha;&nbsp;&nbsp;&nbsp;</td><td style='margin:0; padding:0 5pt; border:0pt solid Black;'>categoria de folosinţă: " + vParcelaScrisa[2] + "</td></tr>";
                }
                foreach (List<string> vParcela in vParcele)
                {
                    vTagInterpretat += "<tr><td style='margin:0; padding:0 5pt; border:0pt solid Black;'>BF " + vParcela[1] + "</td><td style=' border:0px solid Black;margin:0; padding:0 5pt;'>" + vParcela[0].Remove(vParcela[0].Length - 2) + " ha;&nbsp;&nbsp;&nbsp;</td><td style='margin:0; padding:0 5pt; border:0pt solid Black;'>categoria de folosinţă: " + vParcela[2] + "</td></tr>";
                }
                if (vParcele.Count == 0 && vParceleDeScris.Count == 0)
                {
                    for (int celule = 0; celule < 4; celule++)
                    {
                        vTagInterpretat += "<tr><td style='margin:0; padding:0 5pt; border:0pt solid Black;'>BF _____________&nbsp;&nbsp;&nbsp;</td><td style=' border:0px solid Black;margin:0; padding:0 5pt;'>____ha ____ari;&nbsp;&nbsp;&nbsp;</td><td style='margin:0; padding:0 5pt; border:0pt solid Black;'>categoria de folosinţă: ________________________________________</td></tr>";
                    }
                }

                vTagInterpretat += "</table>";
                break;
            case "tip-gospodarie":
                string vTip = ManipuleazaBD.fRezultaUnString("SELECT tip FROM gospodarii WHERE gospodarieId = '" + pGospodarie + "'", "tip", Convert.ToInt16(pAnulCurent));
                switch (vTip)
                {
                    case "1":
                        vTagInterpretat = " 1 (localnic)";
                        break;
                    case "2":
                        vTagInterpretat = " 2 (străinaş)";
                        break;
                    case "3":
                        vTagInterpretat = " 3 (firmă pe raza localităţii)";
                        break;
                    case "4":
                        vTagInterpretat = " 4 (firmă străinaşă)";
                        break;
                    default:
                        vTagInterpretat = "";
                        break;
                }
                break;
                List<string> listaArendasi = new List<string>();
            case "arendator":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString(@"select  membruNume as arendator from gospodarii
join parceleAtribuiriContracte
on gospodarii.gospodarieId = parceleAtribuiriContracte.contractGospodarieIdDa
where (parceleAtribuiriContracte.contractGospodarieIdPrimeste =" + pGospodarie + " or parceleAtribuiriContracte.contractGospodarieIdDa = " + pGospodarie + ")and  gospodarii.unitateId = " + pUnitate + " and year(contractDataFinal) >= " + pAnulCurent + "", "arendator", Convert.ToInt16(pAnulCurent)); break;
            case "arendas":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString(@"select  membruNume as arendas from  gospodarii
join  parceleAtribuiriContracte
on gospodarii.gospodarieId = parceleAtribuiriContracte.contractGospodarieIdPrimeste
where (parceleAtribuiriContracte.contractGospodarieIdDa =" + pGospodarie + " or parceleAtribuiriContracte.contractGospodarieIdPrimeste = " + pGospodarie + ")and  gospodarii.unitateId = " + pUnitate + " and year(contractDataFinal) >= " + pAnulCurent + "", "arendas", Convert.ToInt16(pAnulCurent)); break;
            case "parcele-bloc-fizic":
                vTagInterpretat = "";
                vColoaneParcele = new List<string> { "parcelaDenumire", "parcelaNrBloc" };
                vParcele = ManipuleazaBD.fRezultaListaStringuri("SELECT parcelaDenumire, parcelaNrBloc FROM parcele WHERE unitateaId='" + pUnitate + "' AND gospodarieId ='" + pGospodarie + "'", vColoaneParcele, Convert.ToInt16(pAnulCurent));
                foreach (List<string> vParcela in vParcele)
                {
                    vTagInterpretat += "<br />" + vParcela[0] + " (bloc fizic: " + vParcela[1] + ") ";
                }
                break;
            case "adresa-domiciliu":
                vTagInterpretat = "";
                // daca e localnic luam adrea gospodariei
                if (ManipuleazaBD.fRezultaUnString("SELECT strainas FROM gospodarii WHERE gospodarieId ='" + pGospodarie + "'", "strainas", Convert.ToInt16(pAnulCurent)) == "False")
                {
                    vListaStringScrise = new List<string> { "strada ", "nr.", "bl.", "sc.", "et.", "ap." };
                    vListaString = new List<string> { "strada", "nr", "bl", "sc", "et", "ap" };
                    vListaTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT coalesce(strada, '') as strada, coalesce(nr, '') as nr, coalesce(bl, '') as bl, coalesce(sc,'') as sc, coalesce(et,'') as et, coalesce(ap,'') as ap  FROM gospodarii WHERE gospodarieId ='" + pGospodarie + "'", vListaString, Convert.ToInt16(pAnulCurent));
                    vContor = 0;
                    foreach (string vElement in vListaTagInterpretat)
                    {
                        if (vElement != "" && vElement != null)
                        {
                            if (vContor == 0) { vTagInterpretat = vListaStringScrise[vContor] + vElement; }
                            else { vTagInterpretat += ",  " + vListaStringScrise[vContor] + vElement; }

                            // vTagInterpretat += " ";
                        }
                        vContor++;
                    }
                }
                else
                {
                    vListaStringScrise = new List<string> { "judeţul ", "localitatea ", "strada ", "nr.", "bl.", "sc.", "et.", "ap." };
                    vListaString = new List<string> { "sJudet", "sLocalitate", "sStrada", "sNr", "sBl", "sSc", "sEtj", "sAp" };
                    vListaTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT   coalesce(sJudet, '') as sJudet,   coalesce(sLocalitate, '') as sLocalitate,   coalesce(sStrada, '') as sStrada, coalesce(sNr, '') as sNr, coalesce(sBl, '') as sBl, coalesce(sSc,'') as sSc, coalesce(sEtj,'') as sEtj, coalesce(sAp,'') as sAp   FROM gospodarii WHERE gospodarieId ='" + pGospodarie + "'", vListaString, Convert.ToInt16(pAnulCurent));
                    vContor = 0;
                    foreach (string vElement in vListaTagInterpretat)
                    {
                        if (vElement != "" && vElement != null)
                        {
                            if (vContor == 0) { vTagInterpretat = vListaStringScrise[vContor] + vElement; }
                            else { vTagInterpretat += ",  " + vListaStringScrise[vContor] + vElement; }

                            // vTagInterpretat += " ";
                        }
                        vContor++;
                    }
                }
                break;
            case "adresa":
                vListaStringScrise = new List<string> { "strada ", "nr.", "bl.", "sc.", "et.", "ap." };
                vListaString = new List<string> { "strada", "nr", "bl", "sc", "et", "ap" };
                vListaTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT strada, nr,  bl, sc, et, ap  FROM gospodarii WHERE gospodarieId ='" + pGospodarie + "'", vListaString, Convert.ToInt16(pAnulCurent));
                vContor = 0;
                foreach (string vElement in vListaTagInterpretat)
                {
                    if (vElement != "" && vElement != null)
                    {
                        if (vContor == 0) { vTagInterpretat = vListaStringScrise[vContor] + vElement; }
                        else { vTagInterpretat += ",  " + vListaStringScrise[vContor] + vElement; }

                        // vTagInterpretat += " ";
                    }
                    vContor++;
                }
                break;
            case "cnp":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT cnp FROM membri WHERE gospodarieId ='" + pGospodarie + "' AND capitolId='" + pValoareSpecifica + "'", "cnp", Convert.ToInt16(pAnulCurent));
                break;
            case "terenuri":
                List<string> vColoane = new List<string> { "capitolId", "col1", "col2" };
                List<string> vUM = new List<string> { "ha+ari", "ha", "ari" };
                string vTemporar = "";
                int vAreValoare = 0;
                vTagInterpretat = StringCapitole(vColoane, vUM, "terenuri în suprafaţă totală de ", pGospodarie, pAnulCurent, "2a", "18", out vAreValoare);
                vTemporar = StringCapitole(vColoane, vUM, ", din care teren arabil ", pGospodarie, pAnulCurent, "2a", "1", out vAreValoare);
                if (vAreValoare != 0) { vTagInterpretat += vTemporar; }
                vTemporar = StringCapitole(vColoane, vUM, ", paşuni naturale ", pGospodarie, pAnulCurent, "2a", "2", out vAreValoare);
                if (vAreValoare != 0) { vTagInterpretat += vTemporar; }
                vTemporar = StringCapitole(vColoane, vUM, ", fâneţe naturale ", pGospodarie, pAnulCurent, "2a", "3", out vAreValoare);
                if (vAreValoare != 0) { vTagInterpretat += vTemporar; }
                vTemporar = StringCapitole(vColoane, vUM, ", vii, pepiniere viticole şi hameişti ", pGospodarie, pAnulCurent, "2a", "4", out vAreValoare);
                if (vAreValoare != 0) { vTagInterpretat += vTemporar; }
                vTemporar = StringCapitole(vColoane, vUM, ", livezi de pomi, pepiniere, arbuşti fructiferi ", pGospodarie, pAnulCurent, "2a", "7", out vAreValoare);
                if (vAreValoare != 0) { vTagInterpretat += vTemporar; }
                vTemporar = StringCapitole(vColoane, vUM, ", grădini familiale ", pGospodarie, pAnulCurent, "2a", "9", out vAreValoare);
                if (vAreValoare != 0) { vTagInterpretat += vTemporar; }
                vTemporar = StringCapitole(vColoane, vUM, ", păduri şi alte terenuri cu vegetaţie forestieră	 ", pGospodarie, pAnulCurent, "2a", "11", out vAreValoare);
                if (vAreValoare != 0) { vTagInterpretat += vTemporar; }
                vTemporar = StringCapitole(vColoane, vUM, ", drumuri şi căi ferate ", pGospodarie, pAnulCurent, "2a", "13", out vAreValoare);
                if (vAreValoare != 0) { vTagInterpretat += vTemporar; }
                vTemporar = StringCapitole(vColoane, vUM, ", construcţii ", pGospodarie, pAnulCurent, "2a", "14", out vAreValoare);
                if (vAreValoare != 0) { vTagInterpretat += vTemporar; }
                vTemporar = StringCapitole(vColoane, vUM, ", terenuri degradate şi neproductive ", pGospodarie, pAnulCurent, "2a", "15", out vAreValoare);
                if (vAreValoare != 0) { vTagInterpretat += vTemporar; }
                vTemporar = StringCapitole(vColoane, vUM, ", ape şi bălţi ", pGospodarie, pAnulCurent, "2a", "16", out vAreValoare);
                if (vAreValoare != 0) { vTagInterpretat += vTemporar; }
                break;
            //[teren_proprietate] = cap 2a rand 18 col1+col2 din care cap2a rand 1 col1+col2
            case "terenuri-minim30ari":
                List<string> vColoane30 = new List<string> { "capitolId", "col1", "col2" };
                List<string> vUM30 = new List<string> { "ha+ari", "ha", "ari" };
                string vTemporar30 = "";
                int vAreValoare30 = 0;
                vTagInterpretat = StringCapitole(vColoane30, vUM30, "terenuri în suprafaţă totală de ", pGospodarie, pAnulCurent, "2a", "18", out vAreValoare30, 30);
                vTemporar30 = StringCapitole(vColoane30, vUM30, ", din care teren arabil ", pGospodarie, pAnulCurent, "2a", "1", out vAreValoare30, 30);
                if (vAreValoare30 != 0) { vTagInterpretat += vTemporar30; }
                vTemporar30 = StringCapitole(vColoane30, vUM30, ", paşuni naturale ", pGospodarie, pAnulCurent, "2a", "2", out vAreValoare30, 30);
                if (vAreValoare30 != 0) { vTagInterpretat += vTemporar30; }
                vTemporar30 = StringCapitole(vColoane30, vUM30, ", fâneţe naturale ", pGospodarie, pAnulCurent, "2a", "3", out vAreValoare30, 30);
                if (vAreValoare30 != 0) { vTagInterpretat += vTemporar30; }
                vTemporar30 = StringCapitole(vColoane30, vUM30, ", vii, pepiniere viticole şi hameişti ", pGospodarie, pAnulCurent, "2a", "4", out vAreValoare30, 30);
                if (vAreValoare30 != 0) { vTagInterpretat += vTemporar30; }
                vTemporar30 = StringCapitole(vColoane30, vUM30, ", livezi de pomi, pepiniere, arbuşti fructiferi ", pGospodarie, pAnulCurent, "2a", "7", out vAreValoare30, 30);
                if (vAreValoare30 != 0) { vTagInterpretat += vTemporar30; }
                vTemporar30 = StringCapitole(vColoane30, vUM30, ", grădini familiale ", pGospodarie, pAnulCurent, "2a", "9", out vAreValoare30, 30);
                if (vAreValoare30 != 0) { vTagInterpretat += vTemporar30; }
                vTemporar30 = StringCapitole(vColoane30, vUM30, ", păduri şi alte terenuri cu vegetaţie forestieră	 ", pGospodarie, pAnulCurent, "2a", "11", out vAreValoare30, 30);
                if (vAreValoare30 != 0) { vTagInterpretat += vTemporar30; }
                vTemporar30 = StringCapitole(vColoane30, vUM30, ", drumuri şi căi ferate ", pGospodarie, pAnulCurent, "2a", "13", out vAreValoare30, 30);
                if (vAreValoare30 != 0) { vTagInterpretat += vTemporar30; }
                vTemporar30 = StringCapitole(vColoane30, vUM30, ", construcţii ", pGospodarie, pAnulCurent, "2a", "14", out vAreValoare30, 30);
                if (vAreValoare30 != 0) { vTagInterpretat += vTemporar30; }
                vTemporar30 = StringCapitole(vColoane30, vUM30, ", terenuri degradate şi neproductive ", pGospodarie, pAnulCurent, "2a", "15", out vAreValoare30, 30);
                if (vAreValoare30 != 0) { vTagInterpretat += vTemporar30; }
                vTemporar30 = StringCapitole(vColoane30, vUM30, ", ape şi bălţi ", pGospodarie, pAnulCurent, "2a", "16", out vAreValoare30, 30);
                if (vAreValoare30 != 0) { vTagInterpretat += vTemporar30; }
                break;
            //
            case "suprafata-terenuri":
                {
                    decimal intravilan = Convert.ToDecimal(ManipuleazaBD.fRezultaUnString("SELECT coalesce(SUM(parcelaSuprafataIntravilanHa) + SUM(parcelaSuprafataIntravilanAri) / 100,0) AS total FROM parcele WHERE (gospodarieId = '" + pGospodarie + "')", "total", Convert.ToInt16(pAnulCurent)));

                    decimal extravilan = Convert.ToDecimal(ManipuleazaBD.fRezultaUnString("SELECT coalesce(SUM(parcelaSuprafataExtravilanHa) + SUM(parcelaSuprafataExtravilanAri) / 100,0) AS total FROM parcele WHERE (gospodarieId = '" + pGospodarie + "')", "total", Convert.ToInt16(pAnulCurent)));

                    if (intravilan > 0 && extravilan > 0)
                    {
                        vTagInterpretat = Math.Round(intravilan, 2) + " ha intravilan si" + Math.Round(extravilan, 2) + " ha extravilan";
                    }
                    else
                    {
                        if (intravilan > 0)
                        {
                            vTagInterpretat = Math.Round(intravilan, 2) + " ha intravilan";
                        }
                        else
                            if (extravilan > 0)
                        {
                            vTagInterpretat = Math.Round(extravilan, 2) + " ha extravilan";
                        }
                        else
                        {
                            vTagInterpretat = "";
                        }
                    }
                    break;
                }
            case "teren-intravilan-ha":
                {
                    vTagInterpretat = Convert.ToDecimal(ManipuleazaBD.fRezultaUnString("SELECT coalesce(SUM(parcelaSuprafataIntravilanHa) + SUM(parcelaSuprafataIntravilanAri) / 100,0) AS total FROM parcele WHERE (gospodarieId = '" + pGospodarie + "')", "total", Convert.ToInt16(pAnulCurent)).Replace(".", ",")).ToString("F4") + " ha";
                }
                break;
            case "teren-intravilan-mp":
                {
                    vTagInterpretat = (Convert.ToDecimal(ManipuleazaBD.fRezultaUnString("SELECT coalesce(SUM(parcelaSuprafataIntravilanHa) + SUM(parcelaSuprafataIntravilanAri) / 100,0) AS total FROM parcele WHERE (gospodarieId = '" + pGospodarie + "')", "total", Convert.ToInt16(pAnulCurent)).Replace(".", ",")) * 10000).ToString("F2") + " mp";
                }
                break;
            case "teren-extravilan-ha":
                {
                    vTagInterpretat = Convert.ToDecimal(ManipuleazaBD.fRezultaUnString("SELECT coalesce(SUM(parcelaSuprafataExtravilanHa) + SUM(parcelaSuprafataExtravilanAri) / 100,0) AS total FROM parcele WHERE (gospodarieId = '" + pGospodarie + "')", "total", Convert.ToInt16(pAnulCurent)).Replace(".", ",")).ToString("F4") + " ha";
                }
                break;
            case "teren-extravilan-mp":
                {
                    vTagInterpretat = (Convert.ToDecimal(ManipuleazaBD.fRezultaUnString("SELECT coalesce(SUM(parcelaSuprafataExtravilanHa) + SUM(parcelaSuprafataExtravilanAri) / 100,0) AS total FROM parcele WHERE (gospodarieId = '" + pGospodarie + "')", "total", Convert.ToInt16(pAnulCurent)).Replace(".", ",")) * 10000).ToString("F2") + " mp";
                }
                break;
            case "teren-arabil-intravilan-ha":
                {
                    vTagInterpretat = Convert.ToDecimal(ManipuleazaBD.fRezultaUnString("SELECT coalesce(SUM(parcelaSuprafataIntravilanHa) + SUM(parcelaSuprafataIntravilanAri) / 100,0) AS total FROM parcele WHERE (gospodarieId = '" + pGospodarie + "') AND (parcelaCategorie = 1)", "total", Convert.ToInt16(pAnulCurent)).Replace(".", ",")).ToString("F4") + " ha";
                }
                break;
            case "teren-arabil-intravilan-mp":
                {
                    vTagInterpretat = (Convert.ToDecimal(ManipuleazaBD.fRezultaUnString("SELECT coalesce(SUM(parcelaSuprafataIntravilanHa) + SUM(parcelaSuprafataIntravilanAri) / 100,0) AS total FROM parcele WHERE (gospodarieId = '" + pGospodarie + "') AND (parcelaCategorie = 1)", "total", Convert.ToInt16(pAnulCurent)).Replace(".", ",")) * 10000).ToString("F2") + " mp";
                }
                break;
            case "teren-arabil-extravilan-ha":
                {
                    vTagInterpretat = Convert.ToDecimal(ManipuleazaBD.fRezultaUnString("SELECT coalesce(SUM(parcelaSuprafataExtravilanHa) + SUM(parcelaSuprafataExtravilanAri) / 100,0) AS total FROM parcele WHERE (gospodarieId = '" + pGospodarie + "') AND (parcelaCategorie = 1)", "total", Convert.ToInt16(pAnulCurent)).Replace(".", ",")).ToString("F4") + " ha";
                }
                break;
            case "teren-arabil-extravilan-mp":
                {
                    vTagInterpretat = (Convert.ToDecimal(ManipuleazaBD.fRezultaUnString("SELECT coalesce(SUM(parcelaSuprafataExtravilanHa) + SUM(parcelaSuprafataExtravilanAri) / 100,0) AS total FROM parcele WHERE (gospodarieId = '" + pGospodarie + "') AND (parcelaCategorie = 1)", "total", Convert.ToInt16(pAnulCurent)).Replace(".", ",")) * 10000).ToString("F2") + " mp";
                }
                break;

            case "teren-necultivat-ha":
                {

                    decimal vTerenArabilIntravilanHa = Convert.ToDecimal(
                        ManipuleazaBD.fRezultaUnString("SELECT coalesce(SUM(parcelaSuprafataIntravilanHa) + SUM(parcelaSuprafataIntravilanAri) / 100,0) AS total FROM parcele WHERE (gospodarieId = '" + pGospodarie + "') AND (parcelaCategorie = 1)", "total", Convert.ToInt16(pAnulCurent)).Replace(".", ",")
                        );
                    decimal vTerenArabilExtravilanHa = Convert.ToDecimal(
                        ManipuleazaBD.fRezultaUnString("SELECT coalesce(SUM(parcelaSuprafataExtravilanHa) + SUM(parcelaSuprafataExtravilanAri) / 100,0) AS total FROM parcele WHERE (gospodarieId = '" + pGospodarie + "') AND (parcelaCategorie = 1)", "total", Convert.ToInt16(pAnulCurent)).Replace(".", ",")
                        );

                    vAreValoare = 0;
                    vColoane = new List<string> { "capitolId", "col1", "col2" };
                    vUM = new List<string> { "ha+ari", "ha", "ari" };
                    decimal vTerenArendaPrimit = StringCapitoleFaraText(vColoane, vUM, "terenuri primite în arendă în suprafaţă de ", pGospodarie, pAnulCurent, "3", "3", out vAreValoare);

                    vTagInterpretat = (vTerenArabilExtravilanHa + vTerenArabilExtravilanHa + vTerenArendaPrimit).ToString("F") + " ha";

                }
                break;
            case "teren-proprietate":
                vColoane = new List<string> { "capitolId", "col1", "col2" };
                vUM = new List<string> { "ha+ari", "ha", "ari" };
                vTemporar = "";
                vAreValoare = 0;
                vTagInterpretat = StringCapitole(vColoane, vUM, "terenuri proprietate în suprafaţă totală de ", pGospodarie, pAnulCurent, "2a", "18", out vAreValoare);
                vTemporar = StringCapitole(vColoane, vUM, ", din care teren arabil (inclusiv sere şi solarii) ", pGospodarie, pAnulCurent, "2a", "1", out vAreValoare);
                if (vAreValoare != 0) { vTagInterpretat += vTemporar; }
                break;
            case "terenuri-detaliat":
                vColoane = new List<string> { "capitolId", "col1", "col2" };
                vUM = new List<string> { "ha+ari", "ha", "ari" };
                vTemporar = "";
                vAreValoare = 0;
                vTagInterpretat = StringCapitole(vColoane, vUM, "terenuri proprietate în suprafaţă totală de ", pGospodarie, pAnulCurent, "2a", "18", out vAreValoare);
                vTemporar = StringCapitole(vColoane, vUM, ", din care teren arabil (inclusiv sere şi solarii) ", pGospodarie, pAnulCurent, "2a", "1", out vAreValoare);
                if (vAreValoare != 0) { vTagInterpretat += vTemporar; }
                break;
            //[teren_arenda_dat] = cap 3 rand 10 col1+col2
            case "teren-arenda-dat":
                vColoane = new List<string> { "capitolId", "col1", "col2" };
                vUM = new List<string> { "ha+ari", "ha", "ari" };
                vTemporar = "";
                vAreValoare = 0;
                vTagInterpretat = StringCapitole(vColoane, vUM, "terenuri date în arendă în suprafaţă de ", pGospodarie, pAnulCurent, "3", "10", out vAreValoare);
                if (vAreValoare != 0) { vTagInterpretat += vTemporar; }
                break;
            case "teren-arenda-primit":
                vColoane = new List<string> { "capitolId", "col1", "col2" };
                vUM = new List<string> { "ha+ari", "ha", "ari" };
                vTemporar = "";
                vAreValoare = 0;
                vTagInterpretat = StringCapitole(vColoane, vUM, "terenuri primite în arendă în suprafaţă de ", pGospodarie, pAnulCurent, "3", "3", out vAreValoare);
                if (vAreValoare != 0) { vTagInterpretat += vTemporar; }
                break;
            case "teren-parte-dat":
                vColoane = new List<string> { "capitolId", "col1", "col2" };
                vUM = new List<string> { "ha+ari", "ha", "ari" };
                vTemporar = "";
                vAreValoare = 0;
                vTagInterpretat = StringCapitole(vColoane, vUM, "terenuri date în parte în suprafaţă de ", pGospodarie, pAnulCurent, "3", "11", out vAreValoare);
                if (vAreValoare != 0) { vTagInterpretat += vTemporar; }
                break;
            case "teren-parte-primit":
                vColoane = new List<string> { "capitolId", "col1", "col2" };
                vUM = new List<string> { "ha+ari", "ha", "ari" };
                vTemporar = "";
                vAreValoare = 0;
                vTagInterpretat = StringCapitole(vColoane, vUM, "terenuri primite în parte în suprafaţă de ", pGospodarie, pAnulCurent, "3", "4", out vAreValoare);
                if (vAreValoare != 0) { vTagInterpretat += vTemporar; }
                break;
            case "teren-gratuit-dat":
                vColoane = new List<string> { "capitolId", "col1", "col2" };
                vUM = new List<string> { "ha+ari", "ha", "ari" };
                vTemporar = "";
                vAreValoare = 0;
                vTagInterpretat = StringCapitole(vColoane, vUM, "terenuri date  cu titlu gratuit în suprafaţă de ", pGospodarie, pAnulCurent, "3", "12", out vAreValoare);
                if (vAreValoare != 0) { vTagInterpretat += vTemporar; }
                break;
            case "teren-gratuit-primit":
                vColoane = new List<string> { "capitolId", "col1", "col2" };
                vUM = new List<string> { "ha+ari", "ha", "ari" };
                vTemporar = "";
                vAreValoare = 0;
                vTagInterpretat = StringCapitole(vColoane, vUM, "terenuri primite cu titlu gratuit în suprafaţă de ", pGospodarie, pAnulCurent, "3", "5", out vAreValoare);
                if (vAreValoare != 0) { vTagInterpretat += vTemporar; }
                break;
            case "teren-concesiune-dat":
                vColoane = new List<string> { "capitolId", "col1", "col2" };
                vUM = new List<string> { "ha+ari", "ha", "ari" };
                vTemporar = "";
                vAreValoare = 0;
                vTagInterpretat = StringCapitole(vColoane, vUM, "terenuri date în concesiune în suprafaţă de ", pGospodarie, pAnulCurent, "3", "13", out vAreValoare);
                if (vAreValoare != 0) { vTagInterpretat += vTemporar; }
                break;
            case "teren-concesiune-primit":
                vColoane = new List<string> { "capitolId", "col1", "col2" };
                vUM = new List<string> { "ha+ari", "ha", "ari" };
                vTemporar = "";
                vAreValoare = 0;
                vTagInterpretat = StringCapitole(vColoane, vUM, "terenuri primite în concesiune în suprafaţă de ", pGospodarie, pAnulCurent, "3", "6", out vAreValoare);
                if (vAreValoare != 0) { vTagInterpretat += vTemporar; }
                break;
            case "teren-asociere-dat":
                vColoane = new List<string> { "capitolId", "col1", "col2" };
                vUM = new List<string> { "ha+ari", "ha", "ari" };
                vTemporar = "";
                vAreValoare = 0;
                vTagInterpretat = StringCapitole(vColoane, vUM, "terenuri date în asociere în suprafaţă de ", pGospodarie, pAnulCurent, "3", "14", out vAreValoare);
                if (vAreValoare != 0) { vTagInterpretat += vTemporar; }
                break;
            case "teren-asociere-primit":
                vColoane = new List<string> { "capitolId", "col1", "col2" };
                vUM = new List<string> { "ha+ari", "ha", "ari" };
                vTemporar = "";
                vAreValoare = 0;
                vTagInterpretat = StringCapitole(vColoane, vUM, "terenuri primite în asociere în suprafaţă de ", pGospodarie, pAnulCurent, "3", "7", out vAreValoare);
                if (vAreValoare != 0) { vTagInterpretat += vTemporar; }
                break;
            case "teren-alte-forme-dat":
                vColoane = new List<string> { "capitolId", "col1", "col2" };
                vUM = new List<string> { "ha+ari", "ha", "ari" };
                vTemporar = "";
                vAreValoare = 0;
                vTagInterpretat = StringCapitole(vColoane, vUM, "terenuri date sub alte forme în suprafaţă de ", pGospodarie, pAnulCurent, "3", "15", out vAreValoare);
                if (vAreValoare != 0) { vTagInterpretat += vTemporar; }
                break;
            case "teren-alte-forme-primit":
                vColoane = new List<string> { "capitolId", "col1", "col2" };
                vUM = new List<string> { "ha+ari", "ha", "ari" };
                vTemporar = "";
                vAreValoare = 0;
                vTagInterpretat = StringCapitole(vColoane, vUM, "terenuri primite sub alte forme în suprafaţă de ", pGospodarie, pAnulCurent, "3", "8", out vAreValoare);
                if (vAreValoare != 0) { vTagInterpretat += vTemporar; }
                break;
            case "teren-unitati-pj":
                vColoane = new List<string> { "capitolId", "col1", "col2" };
                vUM = new List<string> { "ha+ari", "ha", "ari" };
                vTemporar = "";
                vAreValoare = 0;
                vTagInterpretat = StringCapitole(vColoane, vUM, "Din rândul 09 - la unitati cu personalitate juridică ", pGospodarie, pAnulCurent, "3", "16", out vAreValoare);
                if (vAreValoare != 0) { vTagInterpretat += vTemporar; }
                break;
            //*************************
            // terenuri utilizate - contracte - in ha
            /************************ */
            /*** arabil ***/
            case "teren-arabil-proprietate":
                try
                {
                    vTagInterpretat = Convert.ToDecimal(TagTerenuri3Proprietate(pGospodarie, pAnulCurent, "1")).ToString("f2");
                }
                catch { vTagInterpretat = "0"; }
                break;
            case "teren-arabil-total":
                decimal arabilTemp = 0;
                for (int i = 3; i <= 8; i++)
                {
                    try
                    {
                        arabilTemp += Convert.ToDecimal(TagTerenuri3(pGospodarie, pAnulCurent, "1", i.ToString()));
                    }
                    catch { }
                }
                try
                {
                    arabilTemp += Convert.ToDecimal(TagTerenuri3Proprietate(pGospodarie, pAnulCurent, "1"));
                }
                catch { }
                vTagInterpretat = arabilTemp.ToString("f2");
                break;
            case "teren-arabil-arenda":
                try
                {
                    vTagInterpretat = Convert.ToDecimal(TagTerenuri3(pGospodarie, pAnulCurent, "1", "3")).ToString("f2");
                }
                catch { vTagInterpretat = "0"; }
                break;
            case "teren-arabil-parte":
                try
                {
                    vTagInterpretat = Convert.ToDecimal(TagTerenuri3(pGospodarie, pAnulCurent, "1", "4")).ToString("f2");
                }
                catch { vTagInterpretat = "0"; }
                break;
            case "teren-arabil-gratuit":
                try
                {
                    vTagInterpretat = Convert.ToDecimal(TagTerenuri3(pGospodarie, pAnulCurent, "1", "5")).ToString("f2");
                }
                catch { vTagInterpretat = "0"; }
                break;
            case "teren-arabil-concesiune":
                try
                {
                    vTagInterpretat = Convert.ToDecimal(TagTerenuri3(pGospodarie, pAnulCurent, "1", "6")).ToString("f2");
                }
                catch { vTagInterpretat = "0"; }
                break;
            case "teren-arabil-asociere":
                try
                {
                    vTagInterpretat = Convert.ToDecimal(TagTerenuri3(pGospodarie, pAnulCurent, "1", "7")).ToString("f2");
                }
                catch { vTagInterpretat = "0"; }
                break;
            case "teren-arabil-alte-forme":
                try
                {
                    vTagInterpretat = Convert.ToDecimal(TagTerenuri3(pGospodarie, pAnulCurent, "1", "8")).ToString("f2");
                }
                catch { vTagInterpretat = "0"; }
                break;

            /*** pasuni ***/
            case "fanete-pasuni-permanente":
                {
                    decimal pasuniTempA = 0;
                    for (int i = 3; i <= 8; i++)
                    {
                        try
                        {
                            pasuniTempA += Convert.ToDecimal(TagTerenuri3(pGospodarie, pAnulCurent, "2", i.ToString()));
                        }
                        catch { }
                    }

                    decimal faneteTempA = 0;
                    for (int i = 3; i <= 8; i++)
                    {
                        try
                        {
                            faneteTempA += Convert.ToDecimal(TagTerenuri3(pGospodarie, pAnulCurent, "3", i.ToString()));
                        }
                        catch { }
                    }

                    vTagInterpretat = (pasuniTempA + faneteTempA).ToString("F");
                }
                break;

            case "teren-pasuni-proprietate":
                try
                {
                    vTagInterpretat = Convert.ToDecimal(TagTerenuri3Proprietate(pGospodarie, pAnulCurent, "2")).ToString("f2");
                }
                catch { vTagInterpretat = "0"; }
                break;
            case "teren-pasuni-total":
                decimal pasuniTemp = 0;
                for (int i = 3; i <= 8; i++)
                {
                    try
                    {
                        pasuniTemp += Convert.ToDecimal(TagTerenuri3(pGospodarie, pAnulCurent, "2", i.ToString()));
                    }
                    catch { }
                }
                try
                {
                    pasuniTemp += Convert.ToDecimal(TagTerenuri3Proprietate(pGospodarie, pAnulCurent, "2"));
                }
                catch { }
                vTagInterpretat = pasuniTemp.ToString("f2");
                break;
            case "teren-pasuni-arenda":
                try
                {
                    vTagInterpretat = Convert.ToDecimal(TagTerenuri3(pGospodarie, pAnulCurent, "2", "3")).ToString("f2");
                }
                catch { vTagInterpretat = "0"; }
                break;
            case "teren-pasuni-parte":
                try
                {
                    vTagInterpretat = Convert.ToDecimal(TagTerenuri3(pGospodarie, pAnulCurent, "2", "4")).ToString("f2");
                }
                catch { vTagInterpretat = "0"; }
                break;
            case "teren-pasuni-gratuit":
                try
                {
                    vTagInterpretat = Convert.ToDecimal(TagTerenuri3(pGospodarie, pAnulCurent, "2", "5")).ToString("f2");
                }
                catch { vTagInterpretat = "0"; }
                break;
            case "teren-pasuni-concesiune":
                try
                {
                    vTagInterpretat = Convert.ToDecimal(TagTerenuri3(pGospodarie, pAnulCurent, "2", "6")).ToString("f2");
                }
                catch { vTagInterpretat = "0"; }
                break;
            case "teren-pasuni-asociere":
                try
                {
                    vTagInterpretat = Convert.ToDecimal(TagTerenuri3(pGospodarie, pAnulCurent, "2", "7")).ToString("f2");
                }
                catch { vTagInterpretat = "0"; }
                break;
            case "teren-pasuni-alte-forme":
                try
                {
                    vTagInterpretat = Convert.ToDecimal(TagTerenuri3(pGospodarie, pAnulCurent, "2", "8")).ToString("f2");
                }
                catch { vTagInterpretat = "0"; }
                break;

            /*** fanete ***/
            case "teren-fanete-proprietate":
                try
                {
                    vTagInterpretat = Convert.ToDecimal(TagTerenuri3Proprietate(pGospodarie, pAnulCurent, "3")).ToString("f2");
                }
                catch { vTagInterpretat = "0"; }
                break;
            case "teren-fanete-total":
                decimal faneteTemp = 0;
                for (int i = 3; i <= 8; i++)
                {
                    try
                    {
                        faneteTemp += Convert.ToDecimal(TagTerenuri3(pGospodarie, pAnulCurent, "3", i.ToString()));
                    }
                    catch { }
                }
                try
                {
                    faneteTemp += Convert.ToDecimal(TagTerenuri3Proprietate(pGospodarie, pAnulCurent, "3"));
                }
                catch { }
                vTagInterpretat = faneteTemp.ToString("f2");
                break;
            case "teren-fanete-arenda":
                try
                {
                    vTagInterpretat = Convert.ToDecimal(TagTerenuri3(pGospodarie, pAnulCurent, "3", "3")).ToString("f2");
                }
                catch { vTagInterpretat = "0"; }
                break;
            case "teren-fanete-parte":
                try
                {
                    vTagInterpretat = Convert.ToDecimal(TagTerenuri3(pGospodarie, pAnulCurent, "3", "4")).ToString("f2");
                }
                catch { vTagInterpretat = "0"; }
                break;
            case "teren-fanete-gratuit":
                try
                {
                    vTagInterpretat = Convert.ToDecimal(TagTerenuri3(pGospodarie, pAnulCurent, "3", "5")).ToString("f2");
                }
                catch { vTagInterpretat = "0"; }
                break;
            case "teren-fanete-concesiune":
                try
                {
                    vTagInterpretat = Convert.ToDecimal(TagTerenuri3(pGospodarie, pAnulCurent, "3", "6")).ToString("f2");
                }
                catch { vTagInterpretat = "0"; }
                break;
            case "teren-fanete-asociere":
                try
                {
                    vTagInterpretat = Convert.ToDecimal(TagTerenuri3(pGospodarie, pAnulCurent, "3", "7")).ToString("f2");
                }
                catch { vTagInterpretat = "0"; }
                break;
            case "teren-fanete-alte-forme":
                try
                {
                    vTagInterpretat = Convert.ToDecimal(TagTerenuri3(pGospodarie, pAnulCurent, "3", "8")).ToString("f2");
                }
                catch { vTagInterpretat = "0"; }
                break;
            /*** vii ***/
            case "teren-vii-proprietate":
                try
                {
                    vTagInterpretat = Convert.ToDecimal(TagTerenuri3Proprietate(pGospodarie, pAnulCurent, "4")).ToString("f2");
                }
                catch { vTagInterpretat = "0"; }
                break;
            case "teren-vii-total":
                decimal viiTemp = 0;
                for (int i = 3; i <= 8; i++)
                {
                    try
                    {
                        viiTemp += Convert.ToDecimal(TagTerenuri3(pGospodarie, pAnulCurent, "4", i.ToString()));
                    }
                    catch { }
                }
                try
                {
                    viiTemp += Convert.ToDecimal(TagTerenuri3Proprietate(pGospodarie, pAnulCurent, "4"));
                }
                catch { }
                vTagInterpretat = viiTemp.ToString("f2");
                break;
            /*** paduri ***/
            case "teren-paduri-proprietate":
                try
                {
                    vTagInterpretat = Convert.ToDecimal(TagTerenuri3Proprietate(pGospodarie, pAnulCurent, "11")).ToString("f2");
                }
                catch { vTagInterpretat = "0"; }
                break;
            case "teren-paduri-total":
                decimal paduriTemp = 0;
                for (int i = 3; i <= 8; i++)
                {
                    try
                    {
                        paduriTemp += Convert.ToDecimal(TagTerenuri3(pGospodarie, pAnulCurent, "11", i.ToString()));
                    }
                    catch { }
                }
                try
                {
                    paduriTemp += Convert.ToDecimal(TagTerenuri3Proprietate(pGospodarie, pAnulCurent, "11"));
                }
                catch { }
                vTagInterpretat = paduriTemp.ToString("f2");
                break;

            //*************************
            // culturi 4a
            /************************ */
            case "culturi":
                vTagInterpretat = "";
                List<string> culturi = new List<string>();
                culturi = ManipuleazaBD.fRezultaListaStringuri(@"SELECT '- ' + Convert(nvarchar, Convert(decimal(16,2),col1 + col2 / 100)) + ' - ' + denumire1 as culturi
                                              FROM [capitole]
                                              join sabloaneCapitole 
                                              on capitole.codRand = sabloaneCapitole.codRand and capitole.codCapitol = sabloaneCapitole.capitol
                                              where capitole.codCapitol = '4a' and sabloaneCapitole.capitol = '4a' and capitole.an = '" + pAnulCurent + "' and sabloaneCapitole.an = '" + pAnulCurent + @"' and LEN(formula) < 4
                                              and capitole.gospodarieId = '" + pGospodarie + "' and (capitole.col1 > 0 or capitole.col2 > 0) and capitole.unitateId = '" + pUnitate + "'", "culturi", Convert.ToInt16(pAnulCurent));
                if (culturi.Count > 0)
                {
                    foreach (string item in culturi)
                    {
                        vTagInterpretat += item + "<br>";
                    }
                }
                break;

            case "grau/secara":
                vTagInterpretat = (
                    TagCulturi(pGospodarie, pAnulCurent, "2") +
                    TagCulturi(pGospodarie, pAnulCurent, "3") +
                    TagCulturi(pGospodarie, pAnulCurent, "4") +
                    TagCulturi(pGospodarie, pAnulCurent, "5") +
                    TagCulturi(pGospodarie, pAnulCurent, "6") +
                    TagCulturi(pGospodarie, pAnulCurent, "7") +
                    TagCulturi(pGospodarie, pAnulCurent, "84") +
                    TagCulturi(pGospodarie, pAnulCurent, "85") +
                    TagCulturi(pGospodarie, pAnulCurent, "86") +
                    TagCulturi(pGospodarie, pAnulCurent, "87") +
                    TagCulturi(pGospodarie, pAnulCurent, "88") +
                    TagCulturi(pGospodarie, pAnulCurent, "89")
                    ).ToString("f2");
                break;
            case "orz":
                vTagInterpretat = (
                    TagCulturi(pGospodarie, pAnulCurent, "8") +
                    TagCulturi(pGospodarie, pAnulCurent, "90")
                    ).ToString("f2");
                break;
            case "orzoaica":
                vTagInterpretat = (
                    TagCulturi(pGospodarie, pAnulCurent, "9") +
                    TagCulturi(pGospodarie, pAnulCurent, "10") +
                    TagCulturi(pGospodarie, pAnulCurent, "91") +
                    TagCulturi(pGospodarie, pAnulCurent, "92")
                    ).ToString("f2");
                break;

            case "cartofi":
                vTagInterpretat = (
                    TagCulturi(pGospodarie, pAnulCurent, "26")
                    ).ToString("f2");
                break;

            case "porumb":
                vTagInterpretat = (
                    TagCulturi(pGospodarie, pAnulCurent, "12") + TagCulturi(pGospodarie, pAnulCurent, "72") +
                    TagCulturi(pGospodarie, pAnulCurent, "94")
                    ).ToString("f2");
                break;

            case "porumb-verde-furajer":
                vTagInterpretat = (
                    TagCulturi(pGospodarie, pAnulCurent, "77")
                    ).ToString("f2");
                break;

            case "porumb-boabe":
                vTagInterpretat = (
                    TagCulturi(pGospodarie, pAnulCurent, "12")
                    ).ToString("f2");
                break;

            case "radacinoase":
                vTagInterpretat = (
                    TagCulturi(pGospodarie, pAnulCurent, "23")

                    ).ToString("f2");
                break;

            case "sfecla-de-zahar":
                vTagInterpretat = (
                    TagCulturi(pGospodarie, pAnulCurent, "23") +
                    TagCulturi(pGospodarie, pAnulCurent, "105")
                    ).ToString("f2");
                break;
            case "floarea-soarelui":
                vTagInterpretat = (
                    TagCulturi(pGospodarie, pAnulCurent, "31") +
                    TagCulturi(pGospodarie, pAnulCurent, "112")
                    ).ToString("f2");
                break;
            case "plante-de-nutret":
                vTagInterpretat = (
                    TagCulturi(pGospodarie, pAnulCurent, "24") +
                    TagCulturi(pGospodarie, pAnulCurent, "25") +
                    TagCulturi(pGospodarie, pAnulCurent, "76") +
                    TagCulturi(pGospodarie, pAnulCurent, "141")
                    ).ToString("f2");
                break;

            case "ovaz":
                vTagInterpretat = (
                    TagCulturi(pGospodarie, pAnulCurent, "11") +
                    TagCulturi(pGospodarie, pAnulCurent, "93")
                    ).ToString("f2");
                break;
            case "legume":
                vTagInterpretat = (
                    TagCulturi(pGospodarie, pAnulCurent, "45") +
                    TagCulturi(pGospodarie, pAnulCurent, "126")
                    ).ToString("f2");
                break;


            //*************************
            // cladiri - capitol 11
            /************************ */

            case "suprafata-cladire1":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT convert(decimal(5,2), col1,0) as col1 FROM capitole WHERE gospodarieId =" + pGospodarie + " AND codCapitol='11' AND codRand = 1 AND an = " + pAnulCurent, "col1", Convert.ToInt16(pAnulCurent));
                break;
            case "suprafata-cladiri":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT sum(convert(decimal(5,2), col1,0)) as col1 FROM capitole WHERE gospodarieId =" + pGospodarie + " AND codCapitol='11' AND codRand in (1, 4, 7, 10) AND an = " + pAnulCurent, "col1", Convert.ToInt16(pAnulCurent));
                break;


            //*************************
            // terenuri fara text
            /************************ */

            case "cap3-r1-c1":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT convert(decimal(5,0), col1,0) as col1 FROM capitole WHERE gospodarieId =" + pGospodarie + " AND codCapitol='3' AND codRand = 1 AND an = " + pAnulCurent, "col1", Convert.ToInt16(pAnulCurent));
                break;
            case "cap3-r1-c2":
                // refacem calculele in capitol aplicam corectia 
                CalculCapitole.ActualizeazaCapitole(pUnitate, pAnulCurent, pGospodarie, "3");
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT  convert(decimal(5,2), col2,0) as col2 FROM capitole WHERE gospodarieId =" + pGospodarie + " AND codCapitol='3' AND codRand = 1 AND an = " + pAnulCurent, "col2", Convert.ToInt16(pAnulCurent));
                break;
            case "cap3-r2-c1":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT  convert(decimal(5,0), col1,0) as col1 FROM capitole WHERE gospodarieId =" + pGospodarie + " AND codCapitol='3' AND codRand = 2 AND an = " + pAnulCurent, "col1", Convert.ToInt16(pAnulCurent));
                break;
            case "cap3-r2-c2":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT  convert(decimal(5,2), col2,0) as col2 FROM capitole WHERE gospodarieId =" + pGospodarie + " AND codCapitol='3' AND codRand = 2 AND an = " + pAnulCurent, "col2", Convert.ToInt16(pAnulCurent));
                break;
            case "cap3-r3-c1":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT  convert(decimal(5,0), col1,0) as col1 FROM capitole WHERE gospodarieId =" + pGospodarie + " AND codCapitol='3' AND codRand = 3 AND an = " + pAnulCurent, "col1", Convert.ToInt16(pAnulCurent));
                break;
            case "cap3-r3-c2":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT   convert(decimal(5,2), col2,0) as col2 FROM capitole WHERE gospodarieId =" + pGospodarie + " AND codCapitol='3' AND codRand = 3 AND an = " + pAnulCurent, "col2", Convert.ToInt16(pAnulCurent));
                break;
            case "cap3-r4-c1":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT  convert(decimal(5,0), col1,0) as col1 FROM capitole WHERE gospodarieId =" + pGospodarie + " AND codCapitol='3' AND codRand = 4 AND an = " + pAnulCurent, "col1", Convert.ToInt16(pAnulCurent));
                break;
            case "cap3-r4-c2":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT  convert(decimal(5,2), col2,0) as col2 FROM capitole WHERE gospodarieId =" + pGospodarie + " AND codCapitol='3' AND codRand = 4 AND an = " + pAnulCurent, "col2", Convert.ToInt16(pAnulCurent));
                break;
            case "cap3-r5-c1":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT  convert(decimal(5,0), col1,0) as  col1 FROM capitole WHERE gospodarieId =" + pGospodarie + " AND codCapitol='3' AND codRand = 5 AND an = " + pAnulCurent, "col1", Convert.ToInt16(pAnulCurent));
                break;
            case "cap3-r5-c2":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT  convert(decimal(5,2), col2,0) as col2 FROM capitole WHERE gospodarieId =" + pGospodarie + " AND codCapitol='3' AND codRand = 5 AND an = " + pAnulCurent, "col2", Convert.ToInt16(pAnulCurent));
                break;
            case "cap3-r6-c1":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT  convert(decimal(5,0), col1,0) as col1 FROM capitole WHERE gospodarieId =" + pGospodarie + " AND codCapitol='3' AND codRand = 6 AND an = " + pAnulCurent, "col1", Convert.ToInt16(pAnulCurent));
                break;
            case "cap3-r6-c2":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT  convert(decimal(5,2), col2,0) as col2 FROM capitole WHERE gospodarieId =" + pGospodarie + " AND codCapitol='3' AND codRand = 6 AND an = " + pAnulCurent, "col2", Convert.ToInt16(pAnulCurent));
                break;
            case "cap3-r7-c1":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT  convert(decimal(5,0), col1,0) as col1 FROM capitole WHERE gospodarieId =" + pGospodarie + " AND codCapitol='3' AND codRand = 7 AND an = " + pAnulCurent, "col1", Convert.ToInt16(pAnulCurent));
                break;
            case "cap3-r7-c2":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT  convert(decimal(5,2), col2,0) as col2 FROM capitole WHERE gospodarieId =" + pGospodarie + " AND codCapitol='3' AND codRand = 7 AND an = " + pAnulCurent, "col2", Convert.ToInt16(pAnulCurent));
                break;
            case "cap3-r8-c1":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT  convert(decimal(5,0), col1,0) as col1 FROM capitole WHERE gospodarieId =" + pGospodarie + " AND codCapitol='3' AND codRand = 8 AND an = " + pAnulCurent, "col1", Convert.ToInt16(pAnulCurent));
                break;
            case "cap3-r8-c2":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT  convert(decimal(5,2), col2,0) as col2 FROM capitole WHERE gospodarieId =" + pGospodarie + " AND codCapitol='3' AND codRand = 8 AND an = " + pAnulCurent, "col2", Convert.ToInt16(pAnulCurent));
                break;
            case "cap3-r9-c1":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT  convert(decimal(5,0), col1,0) as col1 FROM capitole WHERE gospodarieId =" + pGospodarie + " AND codCapitol='3' AND codRand = 9 AND an = " + pAnulCurent, "col1", Convert.ToInt16(pAnulCurent));
                break;
            case "cap3-r9-c2":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT  convert(decimal(5,2), col2,0) as col2 FROM capitole WHERE gospodarieId =" + pGospodarie + " AND codCapitol='3' AND codRand = 9 AND an = " + pAnulCurent, "col2", Convert.ToInt16(pAnulCurent));
                break;
            case "cap3-r10-c1":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT  convert(decimal(5,0), col1,0) as col1 FROM capitole WHERE gospodarieId =" + pGospodarie + " AND codCapitol='3' AND codRand = 10 AND an = " + pAnulCurent, "col1", Convert.ToInt16(pAnulCurent));
                break;
            case "cap3-r10-c2":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT  convert(decimal(5,2), col2,0) as col2 FROM capitole WHERE gospodarieId =" + pGospodarie + " AND codCapitol='3' AND codRand = 10 AND an = " + pAnulCurent, "col2", Convert.ToInt16(pAnulCurent));
                break;
            case "cap3-r11-c1":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT  convert(decimal(5,0), col1,0) as col1 FROM capitole WHERE gospodarieId =" + pGospodarie + " AND codCapitol='3' AND codRand = 11 AND an = " + pAnulCurent, "col1", Convert.ToInt16(pAnulCurent));
                break;
            case "cap3-r11-c2":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT  convert(decimal(5,2), col2,0) as col2 FROM capitole WHERE gospodarieId =" + pGospodarie + " AND codCapitol='3' AND codRand = 11 AND an = " + pAnulCurent, "col2", Convert.ToInt16(pAnulCurent));
                break;
            case "cap3-r12-c1":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT  convert(decimal(5,0), col1,0) as col1 FROM capitole WHERE gospodarieId =" + pGospodarie + " AND codCapitol='3' AND codRand = 12 AND an = " + pAnulCurent, "col1", Convert.ToInt16(pAnulCurent));
                break;
            case "cap3-r12-c2":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT  convert(decimal(5,2), col2,0) as col2 FROM capitole WHERE gospodarieId =" + pGospodarie + " AND codCapitol='3' AND codRand = 12 AND an = " + pAnulCurent, "col2", Convert.ToInt16(pAnulCurent));
                break;
            case "cap3-r13-c1":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT  convert(decimal(5,0), col1,0) as col1 FROM capitole WHERE gospodarieId =" + pGospodarie + " AND codCapitol='3' AND codRand = 13 AND an = " + pAnulCurent, "col1", Convert.ToInt16(pAnulCurent));
                break;
            case "cap3-r13-c2":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT  convert(decimal(5,2), col2,0) as col2 FROM capitole WHERE gospodarieId =" + pGospodarie + " AND codCapitol='3' AND codRand = 13 AND an = " + pAnulCurent, "col2", Convert.ToInt16(pAnulCurent));
                break;
            case "cap3-r14-c1":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT  convert(decimal(5,0), col1,0) as col1 FROM capitole WHERE gospodarieId =" + pGospodarie + " AND codCapitol='3' AND codRand = 14 AND an = " + pAnulCurent, "col1", Convert.ToInt16(pAnulCurent));
                break;
            case "cap3-r14-c2":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT  convert(decimal(5,2), col2,0) as col2 FROM capitole WHERE gospodarieId =" + pGospodarie + " AND codCapitol='3' AND codRand = 14 AND an = " + pAnulCurent, "col2", Convert.ToInt16(pAnulCurent));
                break;
            case "cap3-r15-c1":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT  convert(decimal(5,0), col1,0) as col1 FROM capitole WHERE gospodarieId =" + pGospodarie + " AND codCapitol='3' AND codRand = 15 AND an = " + pAnulCurent, "col1", Convert.ToInt16(pAnulCurent));
                break;
            case "cap3-r15-c2":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT  convert(decimal(5,2), col2,0) as col2 FROM capitole WHERE gospodarieId =" + pGospodarie + " AND codCapitol='3' AND codRand = 15 AND an = " + pAnulCurent, "col2", Convert.ToInt16(pAnulCurent));
                break;
            case "cap3-r16-c1":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT  convert(decimal(5,0), col1,0) as col1 FROM capitole WHERE gospodarieId =" + pGospodarie + " AND codCapitol='3' AND codRand = 16 AND an = " + pAnulCurent, "col1", Convert.ToInt16(pAnulCurent));
                break;
            case "cap3-r16-c2":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT  convert(decimal(5,2), col2,0) as col2 FROM capitole WHERE gospodarieId =" + pGospodarie + " AND codCapitol='3' AND codRand = 16 AND an = " + pAnulCurent, "col2", Convert.ToInt16(pAnulCurent));
                break;
            case "cap3-r17-c1":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT  convert(decimal(5,0), col1,0) as col1 FROM capitole WHERE gospodarieId =" + pGospodarie + " AND codCapitol='3' AND codRand = 17 AND an = " + pAnulCurent, "col1", Convert.ToInt16(pAnulCurent));
                break;
            case "cap3-r17-c2":
                vTagInterpretat = ManipuleazaBD.fRezultaUnString("SELECT  convert(decimal(5,2), col2,0) as col2 FROM capitole WHERE gospodarieId =" + pGospodarie + " AND codCapitol='3' AND codRand = 17 AND an = " + pAnulCurent, "col2", Convert.ToInt16(pAnulCurent));
                break;

            /****************************/


            case "bf_apia":

                break;
            case "animale":
                vAreBunuri = 0;
                string vColSemestru = "col1";
                vTagInterpretat = "situaţia efectivelor de animale la începutul semestrului ";
                if (Convert.ToInt32(DateTime.Now.Month) <= 6 && pAnulCurent == DateTime.Now.Year.ToString())
                {
                    vColSemestru = "col1";
                    vTagInterpretat += "I era de ";
                }
                else
                {
                    vColSemestru = "col2";
                    vTagInterpretat += "II era de ";
                }
                vColoane = new List<string> { "capitolId", vColSemestru };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                if (Convert.ToInt32(pAnulCurent) < 2015)
                {
                    vTemporar = StringCapitole(vColoane, vUM, " bovine ", pGospodarie, pAnulCurent, "7", "1", out vAreValoare);
                    if (vAreValoare != 0) { vTagInterpretat += vTemporar; vAreBunuri = 1; }
                    vTemporar = StringCapitole(vColoane, vUM, ", ovine ", pGospodarie, pAnulCurent, "7", "26", out vAreValoare);
                    if (vAreValoare != 0) { vTagInterpretat += vTemporar; vAreBunuri = 1; }
                    vTemporar = StringCapitole(vColoane, vUM, ", caprine ", pGospodarie, pAnulCurent, "7", "32", out vAreValoare);
                    if (vAreValoare != 0) { vTagInterpretat += vTemporar; vAreBunuri = 1; }
                    vTemporar = StringCapitole(vColoane, vUM, ", porcine ", pGospodarie, pAnulCurent, "7", "37", out vAreValoare);
                    if (vAreValoare != 0) { vTagInterpretat += vTemporar; vAreBunuri = 1; }
                    vTemporar = StringCapitole(vColoane, vUM, ", cabaline ", pGospodarie, pAnulCurent, "7", "50", out vAreValoare);
                    if (vAreValoare != 0) { vTagInterpretat += vTemporar; vAreBunuri = 1; }
                    vTemporar = StringCapitole(vColoane, vUM, ", măgari ", pGospodarie, pAnulCurent, "7", "53", out vAreValoare);
                    if (vAreValoare != 0) { vTagInterpretat += vTemporar; vAreBunuri = 1; }
                    vTemporar = StringCapitole(vColoane, vUM, ", catâri ", pGospodarie, pAnulCurent, "7", "54", out vAreValoare);
                    if (vAreValoare != 0) { vTagInterpretat += vTemporar; vAreBunuri = 1; }
                    vTemporar = StringCapitole(vColoane, vUM, ", iepuri ", pGospodarie, pAnulCurent, "7", "55", out vAreValoare);
                    if (vAreValoare != 0) { vTagInterpretat += vTemporar; vAreBunuri = 1; }
                    vTemporar = StringCapitole(vColoane, vUM, ", animale de blană ", pGospodarie, pAnulCurent, "7", "57", out vAreValoare);
                    if (vAreValoare != 0) { vTagInterpretat += vTemporar; vAreBunuri = 1; }
                    vTemporar = StringCapitole(vColoane, vUM, ", alte animale de blană ", pGospodarie, pAnulCurent, "7", "61", out vAreValoare);
                    if (vAreValoare != 0) { vTagInterpretat += vTemporar; vAreBunuri = 1; }
                    vTemporar = StringCapitole(vColoane, vUM, ", păsări ", pGospodarie, pAnulCurent, "7", "62", out vAreValoare);
                    if (vAreValoare != 0) { vTagInterpretat += vTemporar; vAreBunuri = 1; }
                    vTemporar = StringCapitole(vColoane, vUM, ", alte animale domestice ", pGospodarie, pAnulCurent, "7", "71", out vAreValoare);
                    if (vAreValoare != 0) { vTagInterpretat += vTemporar; vAreBunuri = 1; }
                    vTemporar = StringCapitole(vColoane, vUM, ", albine ", pGospodarie, pAnulCurent, "7", "72", out vAreValoare);
                    if (vAreValoare != 0) { vTagInterpretat += vTemporar; vAreBunuri = 1; }
                    if (vAreBunuri == 0) { vTagInterpretat += "0 animale"; }
                }
                else
                {
                    vTemporar = StringCapitole(vColoane, vUM, " bovine ", pGospodarie, pAnulCurent, "7", "1", out vAreValoare);
                    if (vAreValoare != 0) { vTagInterpretat += vTemporar; vAreBunuri = 1; }
                    vTemporar = StringCapitole(vColoane, vUM, ", ovine ", pGospodarie, pAnulCurent, "7", "30", out vAreValoare);
                    if (vAreValoare != 0) { vTagInterpretat += vTemporar; vAreBunuri = 1; }
                    vTemporar = StringCapitole(vColoane, vUM, ", caprine ", pGospodarie, pAnulCurent, "7", "36", out vAreValoare);
                    if (vAreValoare != 0) { vTagInterpretat += vTemporar; vAreBunuri = 1; }
                    vTemporar = StringCapitole(vColoane, vUM, ", porcine ", pGospodarie, pAnulCurent, "7", "41", out vAreValoare);
                    if (vAreValoare != 0) { vTagInterpretat += vTemporar; vAreBunuri = 1; }
                    vTemporar = StringCapitole(vColoane, vUM, ", cabaline ", pGospodarie, pAnulCurent, "7", "56", out vAreValoare);
                    if (vAreValoare != 0) { vTagInterpretat += vTemporar; vAreBunuri = 1; }
                    vTemporar = StringCapitole(vColoane, vUM, ", măgari ", pGospodarie, pAnulCurent, "7", "59", out vAreValoare);
                    if (vAreValoare != 0) { vTagInterpretat += vTemporar; vAreBunuri = 1; }
                    vTemporar = StringCapitole(vColoane, vUM, ", catâri ", pGospodarie, pAnulCurent, "7", "60", out vAreValoare);
                    if (vAreValoare != 0) { vTagInterpretat += vTemporar; vAreBunuri = 1; }
                    vTemporar = StringCapitole(vColoane, vUM, ", iepuri ", pGospodarie, pAnulCurent, "7", "61", out vAreValoare);
                    if (vAreValoare != 0) { vTagInterpretat += vTemporar; vAreBunuri = 1; }
                    vTemporar = StringCapitole(vColoane, vUM, ", animale de blană ", pGospodarie, pAnulCurent, "7", "63", out vAreValoare);
                    if (vAreValoare != 0) { vTagInterpretat += vTemporar; vAreBunuri = 1; }
                    vTemporar = StringCapitole(vColoane, vUM, ", alte animale de blană ", pGospodarie, pAnulCurent, "7", "67", out vAreValoare);
                    if (vAreValoare != 0) { vTagInterpretat += vTemporar; vAreBunuri = 1; }
                    vTemporar = StringCapitole(vColoane, vUM, ", păsări ", pGospodarie, pAnulCurent, "7", "68", out vAreValoare);
                    if (vAreValoare != 0) { vTagInterpretat += vTemporar; vAreBunuri = 1; }
                    vTemporar = StringCapitole(vColoane, vUM, ", alte animale domestice ", pGospodarie, pAnulCurent, "7", "77", out vAreValoare);
                    if (vAreValoare != 0) { vTagInterpretat += vTemporar; vAreBunuri = 1; }
                    vTemporar = StringCapitole(vColoane, vUM, ", albine ", pGospodarie, pAnulCurent, "7", "78", out vAreValoare);
                    if (vAreValoare != 0) { vTagInterpretat += vTemporar; vAreBunuri = 1; }
                    if (vAreBunuri == 0) { vTagInterpretat += "0 animale"; }
                }
                break;

            ///
            /// BOVINE
            /// 
            case "bovine":
                SqlConnection vConB = ManipuleazaBD.CreareConexiune(Convert.ToInt16(pAnulCurent));
                SqlCommand vCmdB = new SqlCommand();
                vCmdB.Connection = vConB;
                if (Convert.ToInt32(DateTime.Now.Month) <= 6 && pAnulCurent == DateTime.Now.Year.ToString())
                    vCmdB.CommandText = "SELECT      CONVERT(integer, COALESCE (SUM(col1), 0))  AS suma FROM            capitole WHERE        (unitateId = " + pUnitate + ") AND (gospodarieId = " + pGospodarie + ") AND (an = " + pAnulCurent + ") AND (codCapitol = '7') AND (codRand = 1)";
                else vCmdB.CommandText = "SELECT      CONVERT(integer, COALESCE (SUM(col2), 0))  AS suma FROM            capitole WHERE        (unitateId = " + pUnitate + ") AND (gospodarieId = " + pGospodarie + ") AND (an = " + pAnulCurent + ") AND (codCapitol = '7') AND (codRand = 1)";
                vTagInterpretat = Convert.ToInt32(vCmdB.ExecuteScalar().ToString()).ToString();
                ManipuleazaBD.InchideConexiune(vConB);
                break;
            case "vaci-lapte":
                // daca e in ciclul 2010-2014
                if (Convert.ToInt32(pAnulCurent) < 2015)
                    vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "20");
                // daca e in ciclul 2015-2019
                else vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "24");
                break;
            case "bovine-tineret-ingrasat":
                // daca e in ciclul 2010-2014
                if (Convert.ToInt32(pAnulCurent) < 2015)
                    vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "7");
                // daca e in ciclul 2015-2019
                else vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "7");
                break;
            case "bovine-tineret-prasala":
                // daca e in ciclul 2010-2014
                if (Convert.ToInt32(pAnulCurent) < 2015)
                    vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "7");
                // daca e in ciclul 2015-2019
                else vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "7");
                vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "13");
                break;
            case "juninci-reproductie":
                {
                    int vTmp = TagAnimaleFaraText(pGospodarie, pAnulCurent, "18");
                    vTmp += TagAnimaleFaraText(pGospodarie, pAnulCurent, "19");
                    vTagInterpretat = vTmp.ToString();
                }
                break;

            case "juninci":
                // daca e in ciclul 2010-2014
                if (Convert.ToInt32(pAnulCurent) < 2015)
                    vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "18");
                // daca e in ciclul 2015-2019
                else vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "20");
                break;

            case "":
               
                break;

            case "vitei-0-1":
                vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "2");
                break;

            case "bovine-femele-1-2":
                vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "9");
                break;

            case "bovine-masculi-1-2":
                vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "10");
                break;

            case "bovine-masculi-2-peste":
                vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "12");
                break;

            case "vitei-0-6":
                vTagInterpretat = Convert.ToString(Convert.ToInt16(TagAnimale(pGospodarie, pAnulCurent, "4")) + Convert.ToInt16(TagAnimale(pGospodarie, pAnulCurent, "6")));
                break;

            ///
            /// OVINE
            /// 
            case "ovine":
                // daca e in ciclul 2010-2014
                if (Convert.ToInt32(pAnulCurent) < 2015)
                { vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "26"); }
                // daca e in ciclul 2015-2019
                else vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "30");
                break;
            case "berbeci":
                // daca e in ciclul 2010-2014
                if (Convert.ToInt32(pAnulCurent) < 2015)
                { vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "31"); }
                // daca e in ciclul 2015-2019
                else vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "35");
                break;
            case "ovine-tineret":
                // daca e in ciclul 2010-2014
                if (Convert.ToInt32(pAnulCurent) < 2015)
                { vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "29"); }
                // daca e in ciclul 2015-2019
                else vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "33");
                break;
            case "ovine-femele-reproductie":
                // daca e in ciclul 2010-2014
                if (Convert.ToInt32(pAnulCurent) < 2015)
                { vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "27"); }
                // daca e in ciclul 2015-2019
                else vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "31");
                break;
            case "ovine-mioare-montate":
                // daca e in ciclul 2010-2014
                if (Convert.ToInt32(pAnulCurent) < 2015)
                { vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "28"); }
                // daca e in ciclul 2015-2019
                else vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "32");
                break;

            case "alte-ovine":
                // daca e in ciclul 2010-2014
                if (Convert.ToInt32(pAnulCurent) < 2015)
                { vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "30"); }
                // daca e in ciclul 2015-2019
                else vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "34");
                break;

            ///
            /// CAPRINE
            /// 
            case "caprine":
                // daca e in ciclul 2010-2014
                if (Convert.ToInt32(pAnulCurent) < 2015)
                { vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "32"); }
                // daca e in ciclul 2015-2019
                else vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "36");
                break;
            case "capre":
                // daca e in ciclul 2010-2014
                if (Convert.ToInt32(pAnulCurent) < 2015)
                { vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "33"); }
                // daca e in ciclul 2015-2019
                else vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "37");
                break;
            case "tapi":
                // daca e in ciclul 2010-2014
                if (Convert.ToInt32(pAnulCurent) < 2015)
                { vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "34"); }
                // daca e in ciclul 2015-2019
                else vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "38");
                break;
            case "caprine-tineret":
                // daca e in ciclul 2010-2014
                if (Convert.ToInt32(pAnulCurent) < 2015)
                { vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "35"); }
                // daca e in ciclul 2015-2019
                else vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "39");
                break;
            case "alte-caprine":
                // daca e in ciclul 2010-2014
                if (Convert.ToInt32(pAnulCurent) < 2015)
                { vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "36"); }
                // daca e in ciclul 2015-2019
                else vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "40");
                break;

            case "ovine/caprine":
                // daca e in ciclul 2010-2014
                if (Convert.ToInt32(pAnulCurent) < 2015)
                { vTagInterpretat = Convert.ToString(Convert.ToInt16(TagAnimale(pGospodarie, pAnulCurent, "26")) + Convert.ToInt16(TagAnimale(pGospodarie, pAnulCurent, "32"))); }
                // daca e in ciclul 2015-2019
                else vTagInterpretat = Convert.ToString(Convert.ToInt16(TagAnimale(pGospodarie, pAnulCurent, "30")) + Convert.ToInt16(TagAnimale(pGospodarie, pAnulCurent, "36")));
                break;

            ///
            /// PORCINE
            /// 
            case "porcine":
                // daca e in ciclul 2010-2014
                if (Convert.ToInt32(pAnulCurent) < 2015)
                { vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "37"); }
                // daca e in ciclul 2015-2019
                else vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "41");
                break;
            case "purcei":
                // daca e in ciclul 2010-2014
                if (Convert.ToInt32(pAnulCurent) < 2015)
                { vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "38"); }
                // daca e in ciclul 2015-2019
                else vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "42");
                break;
            case "porcine-ingrasat":
                // daca e in ciclul 2010-2014
                if (Convert.ToInt32(pAnulCurent) < 2015)
                { vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "41"); }
                // daca e in ciclul 2015-2019
                else vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "45");
                break;
            case "scroafe":
                // daca e in ciclul 2010-2014
                if (Convert.ToInt32(pAnulCurent) < 2015)
                { vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "47"); }
                // daca e in ciclul 2015-2019
                else vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "51");
                break;
            case "vieri":
                // daca e in ciclul 2010-2014
                if (Convert.ToInt32(pAnulCurent) < 2015)
                    vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "46");
                // daca e in ciclul 2015-2019
                else vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "50");
                break;
            case "scroafe-montate":
                // daca e in ciclul 2010-2014
                if (Convert.ToInt32(pAnulCurent) < 2015)
                    vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "47");
                // daca e in ciclul 2015-2019
                else vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "51");
                break;
            case "scrofite":
                // daca e in ciclul 2010-2014
                if (Convert.ToInt32(pAnulCurent) < 2015)
                    vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "48");
                // daca e in ciclul 2015-2019
                else vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "54");
                break;
            case "scroafe-nemontate":
                // daca e in ciclul 2010-2014
                if (Convert.ToInt32(pAnulCurent) < 2015)
                    vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "48");
                // daca e in ciclul 2015-2019
                else vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "53");
                break;
            case "scrofite-tineret-femel-prasila":
                // daca e in ciclul 2010-2014
                if (Convert.ToInt32(pAnulCurent) < 2015)
                    vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "49");
                // daca e in ciclul 2015-2019
                else vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "55");
                break;
            case "porcine-etc":
                vTagInterpretat = (
                    Convert.ToInt16(TagAnimale(pGospodarie, pAnulCurent, "37")) -
                    Convert.ToInt16(TagAnimale(pGospodarie, pAnulCurent, "38")) -
                    Convert.ToInt16(TagAnimale(pGospodarie, pAnulCurent, "41")) -
                    Convert.ToInt16(TagAnimale(pGospodarie, pAnulCurent, "47"))
                    ).ToString();
                break;



        
            case "pasari":
                // daca e in ciclul 2010-2014
                if (Convert.ToInt32(pAnulCurent) < 2015)
                    vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "62");
                // daca e in ciclul 2015-2019
                else vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "68");
                break;


                //SqlConnection vConP = ManipuleazaBD.CreareConexiune(Convert.ToInt16(pAnulCurent));
                //SqlCommand vCmdP = new SqlCommand();
                //vCmdP.Connection = vConP;

                //// daca e in ciclul 2010-2014
                //if (Convert.ToInt32(pAnulCurent) < 2015)
                //{
                //    if (Convert.ToInt32(DateTime.Now.Month) <= 6 && pAnulCurent == DateTime.Now.Year.ToString())
                //        vCmdP.CommandText = "SELECT      CONVERT(integer, COALESCE (SUM(col1), 0) )  AS suma FROM            capitole WHERE        (unitateId = " + pUnitate + ") AND (gospodarieId = " + pGospodarie + ") AND (an = " + pAnulCurent + ") AND (codCapitol = '7') AND (codRand = 62)";
                //    else
                //        vCmdP.CommandText = "SELECT      CONVERT(integer,  COALESCE (SUM(col2), 0))  AS suma FROM            capitole WHERE        (unitateId = " + pUnitate + ") AND (gospodarieId = " + pGospodarie + ") AND (an = " + pAnulCurent + ") AND (codCapitol = '7') AND (codRand = 62)";
                //}
                //// daca e in ciclul 2015-2019
                //else
                //{
                //    if (Convert.ToInt32(DateTime.Now.Month) <= 6 && pAnulCurent == DateTime.Now.Year.ToString())
                //        vCmdP.CommandText = "SELECT      CONVERT(integer, COALESCE (SUM(col1), 0) )  AS suma FROM            capitole WHERE        (unitateId = " + pUnitate + ") AND (gospodarieId = " + pGospodarie + ") AND (an = " + pAnulCurent + ") AND (codCapitol = '7') AND (codRand = 62)";
                //    else
                //        vCmdP.CommandText = "SELECT      CONVERT(integer,  COALESCE (SUM(col2), 0))  AS suma FROM            capitole WHERE        (unitateId = " + pUnitate + ") AND (gospodarieId = " + pGospodarie + ") AND (an = " + pAnulCurent + ") AND (codCapitol = '7') AND (codRand = 62)";
                //}


                //vTagInterpretat = Convert.ToInt32(vCmdP.ExecuteScalar().ToString()).ToString();
                //ManipuleazaBD.InchideConexiune(vConP);
                //break;

            case "alte-pasari":
                vTagInterpretat = (Convert.ToInt16(TagAnimale(pGospodarie, pAnulCurent, "64"))
                                + Convert.ToInt16(TagAnimale(pGospodarie, pAnulCurent, "65"))
                                + Convert.ToInt16(TagAnimale(pGospodarie, pAnulCurent, "66"))
                                + Convert.ToInt16(TagAnimale(pGospodarie, pAnulCurent, "67"))
                                ).ToString();
                break;

            case "pui-carne":
                // daca e in ciclul 2010-2014
                if (Convert.ToInt32(pAnulCurent) < 2015)
                    vTagInterpretat = (Convert.ToInt16(TagAnimale(pGospodarie, pAnulCurent, "62"))
                              - Convert.ToInt16(TagAnimale(pGospodarie, pAnulCurent, "69"))
                              ).ToString();
                // daca e in ciclul 2015-2019
                else vTagInterpretat = (Convert.ToInt16(TagAnimale(pGospodarie, pAnulCurent, "68"))
                              - Convert.ToInt16(TagAnimale(pGospodarie, pAnulCurent, "75"))
                              ).ToString();


                break;

            case "gaini-ouatoare":
                // daca e in ciclul 2010-2014
                if (Convert.ToInt32(pAnulCurent) < 2015)
                    vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "70");
                // daca e in ciclul 2015-2019
                else vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "75");
                break;

            case "albine":
                // daca e in ciclul 2010-2014
                if (Convert.ToInt32(pAnulCurent) < 2015)
                    vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "72");
                // daca e in ciclul 2015-2019
                else vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "78");
                break;
            case "cabaline":
                // daca e in ciclul 2010-2014
                if (Convert.ToInt32(pAnulCurent) < 2015)
                    vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "50");
                // daca e in ciclul 2015-2019
                else vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "56");
                break;
            case "magari":
                // daca e in ciclul 2010-2014
                if (Convert.ToInt32(pAnulCurent) < 2015)
                    vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "53");
                // daca e in ciclul 2015-2019
                else vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "59");
                break;
            case "catari":
                // daca e in ciclul 2010-2014
                if (Convert.ToInt32(pAnulCurent) < 2015)
                    vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "54");
                // daca e in ciclul 2015-2019
                else vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "60");
                break;

            case "iepuri":
                // daca e in ciclul 2010-2014
                if (Convert.ToInt32(pAnulCurent) < 2015)
                    vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "55");
                // daca e in ciclul 2015-2019
                else vTagInterpretat = TagAnimale(pGospodarie, pAnulCurent, "61");
                break;


            /*****************************************/
            /**               UTILAJE               **/
            case "utilaje":
                // cap.9
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vListaString = new List<string> { };
                vTagInterpretat = "utilaje, instalaţii pentru agricultură şi silvicultură, mijloace de transport cu tracţiune animală şi mecanică existente la începutul anului";
                string strConn = ConfigurationManager.ConnectionStrings["TNTRegistruAgricolConnectionString"].ConnectionString;
                SqlConnection con = new SqlConnection(strConn);
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "SELECT denumire1 FROM sabloaneCapitole WHERE capitol='9'";
                con.Open();
                try
                {
                    SqlDataReader vRezultat = cmd.ExecuteReader();
                    while (vRezultat.Read())
                    {
                        vListaString.Add(vRezultat["denumire1"].ToString());
                    }
                    vRezultat.Close();
                }
                catch { }
                con.Close();
                vContor = 1;
                foreach (string vDenumire in vListaString)
                {
                    vTemporar = StringCapitole(vColoane, vUM, ", " + vDenumire.ToString(), pGospodarie, pAnulCurent, "9", vContor.ToString(), out vAreValoare);
                    if (vAreValoare != 0) { vTagInterpretat += vTemporar; vAreBunuri = 1; }
                    vContor++;
                }
                if (vAreBunuri == 0) { vTagInterpretat += " 0 utilaje; "; }
                break;
            case "tractoare":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                int vNrTractoare = 0;
                for (int i = 1; i <= 7; i++)
                {
                    vNrTractoare += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNrTractoare.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "motocultoare":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                int vNr = 0;
                vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", "8", out vAreValoare));
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "motocositoare":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", "9", out vAreValoare));
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "pluguri":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 10; i <= 13; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "pluguri-mecanica":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 10; i <= 12; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "pluguri-animala":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 13; i <= 13; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "cultivatoare":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 14; i <= 14; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "grape":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 15; i <= 16; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "grape-mecanica":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 15; i <= 15; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "grape-animala":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 16; i <= 16; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "combinatoare":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 17; i <= 17; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "semanatori":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 18; i <= 24; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "semanatori-mecanica":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 18; i <= 23; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "semanatori-animala":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 24; i <= 24; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "masini-plantat":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 25; i <= 26; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "masini-plantat-cartofi":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 25; i <= 25; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "masini-plantat-rasaduri":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 26; i <= 26; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "masini-imprastiat":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 27; i <= 28; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "masini-imprastiat-chimice":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 27; i <= 27; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "masini-imprastiat-organice":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 28; i <= 28; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "masini-stropit":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 29; i <= 30; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "masini-stropit-purtate":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 29; i <= 29; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "masini-stropit-tractate":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 30; i <= 30; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "masini-erbicidat":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 31; i <= 31; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "combine-paioase":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 32; i <= 32; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "combine-porumb":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 33; i <= 33; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "combine-furaje":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 34; i <= 34; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "batoze":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 35; i <= 35; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "combine-sfecla":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 36; i <= 36; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "dislocatoare-sfecla":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 37; i <= 37; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "decoletat-sfecla":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 38; i <= 38; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "recoltat-cartofi":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 39; i <= 39; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "cositori":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 40; i <= 40; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "vindrovere":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 41; i <= 41; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "prese-balotat":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 42; i <= 43; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "prese-balotat-paralelipipedici":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 42; i <= 42; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "prese-balotat-cilindrici":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 43; i <= 43; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "remorci":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 44; i <= 44; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "auto-transport-marfa":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 45; i <= 46; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "auto-transport-marfa-1,5tone":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 45; i <= 45; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "auto-transport-marfa+1,5tone":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 46; i <= 46; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "care-carute":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 47; i <= 47; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "incarcatoare-hidraulice":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 48; i <= 48; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "motopompe-irigat":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 49; i <= 49; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "aripi-ploaie-deplasare-manuala":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 50; i <= 50; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "instalatii-irigat":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 51; i <= 53; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "instalatii-irigat-tambur":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 51; i <= 51; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "instalatii-irigat-deplasare-liniara":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 52; i <= 52; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "instalatii-irigat-pivot":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 53; i <= 53; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "instalatii-muls":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 54; i <= 54; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "instalatii-preparare-furaje":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 55; i <= 55; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "instalatii-dejectii":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 56; i <= 56; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "instalatii-tuica":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 57; i <= 57; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "alte-utilaje":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 58; i <= 58; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "mori":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 59; i <= 61; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "mori-ciocanele":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 59; i <= 59; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "mori-valturi":
                vColoane = new List<string> { "capitolId", "col1" };
                vUM = new List<string> { "bucati", "bucati" };
                vTemporar = "";
                vAreValoare = 0;
                vNr = 0;
                for (int i = 60; i <= 61; i++)
                {
                    vNr += Convert.ToInt32(StringCapitole(vColoane, vUM, "", pGospodarie, pAnulCurent, "9", i.ToString(), out vAreValoare));
                }
                if (vAreValoare != 0) { vTagInterpretat = vNr.ToString(); }
                else { vTagInterpretat = ""; }
                break;
            case "nr-pers":
                vTagInterpretat = ManipuleazaBD.fRezultaUnStringScalar("SELECT Count(*) as NumarMembri FROM membri WHERE gospodarieId ='" + pGospodarie + "' AND an ='" + pAnulCurent + "' ", "NumarMembri", Convert.ToInt16(pAnulCurent));
                break;
            case "copii-in-intretinere":
                DateTime v18ani = DateTime.Now;
                v18ani = v18ani.AddYears(-18);
                strConn = ConfigurationManager.ConnectionStrings["TNTRegistruAgricolConnectionString"].ConnectionString;
                con = new SqlConnection(strConn);
                cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "SELECT nume FROM membri WHERE gospodarieId ='" + pGospodarie + "' AND an ='" + pAnulCurent + "' AND codRudenie = '3' AND dataNasterii < '" + v18ani + "'";
                vTemporar = "";
                con.Open();
                try
                {
                    vContor = 0;
                    SqlDataReader vRezultat = cmd.ExecuteReader();
                    while (vRezultat.Read())
                    {
                        vContor++;
                        if (vContor == 1)
                        {
                            vTemporar = vRezultat["nume"].ToString();
                        }
                        else
                        {
                            vTemporar += ", " + vRezultat["nume"].ToString();
                        }

                    }
                    vRezultat.Close();
                }
                catch { }
                con.Close();
                if (vContor > 0)
                {
                    vTagInterpretat = "un număr de " + vContor.ToString() + " copii aflaţi în întreţinere (cu vârsta mai mică de 18 ani) şi anume " + vTemporar;
                }
                else
                {
                    vTagInterpretat = "nu are copii aflaţi în întreţinere (cu vârsta mai mică de 18 ani)";
                }

                break;
            case "inceput":
                // vTagInterpretat = "<p>&nbsp;&nbsp;&nbsp;&nbsp;";
                vTagInterpretat = " ";
                break;
            case "paragraf":
                //vTagInterpretat = "</p><p>&nbsp;&nbsp;&nbsp;&nbsp;";

                vTagInterpretat = "<div style='width:50px; height:10px; float:left; display:inline-block;'></div>";
                break;
            case "ingrosat-incepe":
                vTagInterpretat = "<strong>";
                break;
            case "ingrosat-termina":
                vTagInterpretat = "</strong>";
                break;
            case "10-incepe":
                vTagInterpretat = "<span style=\"font-size:10px;\">";
                break;
            case "10-termina":
                vTagInterpretat = "</span>";
                break;
            case "11-incepe":
                vTagInterpretat = "<span style=\"font-size:11px;\">";
                break;
            case "11-termina":
                vTagInterpretat = "</span>";
                break;
            case "12-incepe":
                vTagInterpretat = "<span style=\"font-size:12px;\">";
                break;
            case "12-termina":
                vTagInterpretat = "</span>";
                break;
            case "13-incepe":
                vTagInterpretat = "<span style=\"font-size:13px;\">";
                break;
            case "13-termina":
                vTagInterpretat = "</span>";
                break;
            case "14-incepe":
                vTagInterpretat = "<span style=\"font-size:14px;\">";
                break;
            case "14-termina":
                vTagInterpretat = "</span>";
                break;
            case "15-incepe":
                vTagInterpretat = "<span style=\"font-size:15px;\">";
                break;
            case "15-termina":
                vTagInterpretat = "</span>";
                break;
            case "16-incepe":
                vTagInterpretat = "<span style=\"font-size:16px;\">";
                break;
            case "16-termina":
                vTagInterpretat = "</span>";
                break;
            case "17-incepe":
                vTagInterpretat = "<span style=\"font-size:17px;\">";
                break;
            case "17-termina":
                vTagInterpretat = "</span>";
                break;
            case "18-incepe":
                vTagInterpretat = "<span style=\"font-size:18px;\">";
                break;
            case "18-termina":
                vTagInterpretat = "</span>";
                break;
            case "19-incepe":
                vTagInterpretat = "<span style=\"font-size:19px;\">";
                break;
            case "19-termina":
                vTagInterpretat = "</span>";
                break;
            case "20-incepe":
                vTagInterpretat = "<span style=\"font-size:20px;\">";
                break;
            case "20-termina":
                vTagInterpretat = "</span>";
                break;
            case "21-incepe":
                vTagInterpretat = "<span style=\"font-size:21px;\">";
                break;
            case "21-termina":
                vTagInterpretat = "</span>";
                break;
            case "22-incepe":
                vTagInterpretat = "<span style=\"font-size:22px;\">";
                break;
            case "22-termina":
                vTagInterpretat = "</span>";
                break;
            case "23-incepe":
                vTagInterpretat = "<span style=\"font-size:23px;\">";
                break;
            case "23-termina":
                vTagInterpretat = "</span>";
                break;
            case "24-incepe":
                vTagInterpretat = "<span style=\"font-size:24px;\">";
                break;
            case "24-termina":
                vTagInterpretat = "</span>";
                break;
            case "scris-mic-incepe":
                vTagInterpretat = "<span style=\"font-size: 0.8em\">";
                break;
            case "scris-mic-termina":
                vTagInterpretat = "</span>";
                break;
            case "scris-mare-incepe":
                vTagInterpretat = "<span style=\"font-size: 1.8em\">";
                break;
            case "scris-mare-termina":
                vTagInterpretat = "</span>";
                break;
            case "final":
                //  vTagInterpretat = "</p>";
                vTagInterpretat = " ";
                break;

        }
        if (vTagInterpretat.IndexOf("<span class=\"camp_necunoscut\" title=\"") != 0) { }
        return vTagInterpretat;
    }
    protected static Adeverinta CreeazaAdeverintaOnline(Membri membru, string sablonAdeverinta)
    {
        Adeverinta adeverinta = new Adeverinta();
        List<string> campuri = new List<string> { "data", "chitanta", "nr-inregistrare", "nr-iesire" };
        List<string> valori = new List<string> { DateTime.Now.ToString("dd.MM.yyyy"), "-", "-", "-" };
        // antet
        adeverinta.Antet = FormeazaAntet(membru.IdUnitate.ToString(), sablonAdeverinta, campuri, valori, DateTime.Now.Year.ToString(), "52");
        // subsol
        adeverinta.Subsol = FormeazaSubsol(membru.IdUnitate.ToString(), sablonAdeverinta, DateTime.Now.Year.ToString(), "52");
        // continut
        if (sablonAdeverinta != "0")
        {
            List<ListaSelect> listaCampuri = new List<ListaSelect>();
            string continut = string.Empty;
            RezultaTextAdeverinta(membru.IdUnitate.ToString(), membru.IdGospodarie.ToString(), membru.IdMembru.ToString(), sablonAdeverinta, out listaCampuri, out continut, DateTime.Now.Year.ToString(), "52");
            adeverinta.Continut = continut;
        }
        else
        { }
        return adeverinta;
    }

    public static bool SalveazaAdeverintaOnline(Membri membru, string sablonAdeverinta)
    {
        if (membru.IdMembru == 0 || sablonAdeverinta == string.Empty)
        {
            return false;
        }
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(DateTime.Now.Year));
        SqlTransaction vTranz = ManipuleazaBD.DeschideTranzactie(vCon);
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.Transaction = vTranz;
        int vAdeverintaId = 0;
        Adeverinta adeverinta = CreeazaAdeverintaOnline(membru, sablonAdeverinta);
        adeverinta.Continut = adeverinta.Continut.Replace("<span class=\"camp_necunoscut\"", "<span class=\"camp_tiparit\"");
        try
        {
            //string vMembruId = "0";
            //try { vMembruId = ddlMembru.SelectedItem.ToString(); }
            //catch { }
            vCmd.CommandText = @"INSERT INTO [adeverinte] 
                ([unitateId]
                , [gospodarieId]
                , [utilizatorId]
                , [numeMembru]
                , [numarChitanta]
                , [numarInregistrare]
                , [numarIesire]
                , [data]
                , [antet]
                , [text]
                , [textTiparit]
                , [subsol]
                , [ora]
                , [minut]
                , [tipSablon]) VALUES (
                '" + membru.IdUnitate +
                "', '" + membru.IdGospodarie +
                @"', '52'
                , N'" + membru.Nume +
                @"', N'-'
                   , N'-'
                   , N'-'
                   , Convert(datetime,'" + DateTime.Now.ToString("dd.MM.yyyy") + @"',104)
                   , N'" + adeverinta.Antet.Replace('\'', '"') +
                @"', N'" + adeverinta.Continut.Replace('\'', '"') +
                "', N'<table class=\"adeverinta_antet\"><tr><td>" +
                adeverinta.Antet.Replace('\'', '"') + "</td></tr><tr><td>" + adeverinta.Continut.Replace('\'', '"') + "</td></tr><tr><td>" + adeverinta.Subsol.Replace('\'', '"') + @"</td></tr></table>', N'" +
                adeverinta.Subsol.Replace('\'', '"') +
                "', '" + DateTime.Now.Hour.ToString() +
                "', '" + DateTime.Now.Minute.ToString() +
                "', '" + sablonAdeverinta + "');    SELECT CAST(scope_identity() AS int)";

            vCmd.CommandText = vCmd.CommandText.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
            vAdeverintaId = Convert.ToInt32(vCmd.ExecuteScalar());
            ClassLog.fLog(membru.IdUnitate, 52, DateTime.Now, "adeverinte - online", "adaugare adeverinta, id:" + vAdeverintaId.ToString(), "", vCmd, membru.IdGospodarie, 5);
            vTranz.Commit();

            return true;
        }
        catch { vTranz.Rollback(); return false; }
        finally { ManipuleazaBD.InchideConexiune(vCon); }
        if (vAdeverintaId != 0)
        {
            // redirect spre pagina care interpreteaza adeverinta
            //Response.Redirect("~/printAdeverinta.aspx?adeverintaId=" + vAdeverintaId.ToString());
        }
    }
    /*  public class Adeverinta
     {
         private string antet = string.Empty;
         public string Antet
         {
             get
             {
                 return antet;
             }
             set
             {
                 antet = value;
             }
         }
         private string subsol = string.Empty;
         public string Subsol
         {
             get
             {
                 return subsol;
             }
             set
             {
                 subsol = value;
             }
         }
         private string continut = string.Empty;
         public string Continut
         {
             get
             {
                 return continut;
             }
             set
             {
                 continut = value;
             }
         }

         public Adeverinta()
         {

         }
     } */

    //public static void SalveazaAdeverintaOnline(Membri membru, string sablonAdeverinta)
    //{

    //    SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(DateTime.Now.Year));
    //    SqlTransaction vTranz = ManipuleazaBD.DeschideTranzactie(vCon);
    //    SqlCommand vCmd = new SqlCommand();
    //    vCmd.Connection = vCon;
    //    vCmd.Transaction = vTranz;
    //    int vAdeverintaId = 0;
    //    Adeverinta adeverinta = CreeazaAdeverintaOnline(membru, sablonAdeverinta);
    //    adeverinta.Continut = adeverinta.Continut.Replace("<span class=\"camp_necunoscut\"", "<span class=\"camp_tiparit\"");
    //    try
    //    {
    //        //string vMembruId = "0";
    //        //try { vMembruId = ddlMembru.SelectedItem.ToString(); }
    //        //catch { }
    //        vCmd.CommandText = @"INSERT INTO [adeverinte] 
    //            ([unitateId]
    //            , [gospodarieId]
    //            , [utilizatorId]
    //            , [numeMembru]
    //            , [numarChitanta]
    //            , [numarInregistrare]
    //            , [numarIesire]
    //            , [data]
    //            , [antet]
    //            , [text]
    //            , [textTiparit]
    //            , [subsol]
    //            , [ora]
    //            , [minut]
    //            , [tipSablon]) VALUES (
    //            '" + membru.IdUnitate +
    //            "', '" + membru.IdGospodarie +
    //            @"', '52'
    //            , N'" + membru.Nume +
    //            @"', N'-'
    //               , N'-'
    //               , N'-'
    //               , Convert(datetime,'" + DateTime.Now.ToString("dd.MM.yyyy") + @"',104)
    //               , N'" + adeverinta.Antet.Replace('\'', '"') +
    //            @"', N'" + adeverinta.Continut.Replace('\'', '"') +
    //            "', N'<table class=\"adeverinta_antet\"><tr><td>" +
    //            adeverinta.Antet.Replace('\'', '"') + "</td></tr><tr><td>" + adeverinta.Continut.Replace('\'', '"') + "</td></tr><tr><td>" + adeverinta.Subsol.Replace('\'', '"') + @"</td></tr></table>', N'" +
    //            adeverinta.Subsol.Replace('\'', '"') +
    //            "', '" + DateTime.Now.Hour.ToString() +
    //            "', '" + DateTime.Now.Minute.ToString() +
    //            "', '" + sablonAdeverinta + "');    SELECT CAST(scope_identity() AS int)";

    //        vCmd.CommandText = vCmd.CommandText.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
    //        vAdeverintaId = Convert.ToInt32(vCmd.ExecuteScalar());
    //        ClassLog.fLog(membru.IdUnitate, 52, DateTime.Now, "adeverinte - online", "adaugare adeverinta, id:" + vAdeverintaId.ToString(), "", vCmd, membru.IdGospodarie, 5);
    //        vTranz.Commit();
    //    }
    //    catch { vTranz.Rollback(); }
    //    finally { ManipuleazaBD.InchideConexiune(vCon); }
    //    if (vAdeverintaId != 0)
    //    {
    //        // redirect spre pagina care interpreteaza adeverinta
    //        //Response.Redirect("~/printAdeverinta.aspx?adeverintaId=" + vAdeverintaId.ToString());
    //    }
    //}
    //public class Adeverinta
    //{
    //    private string antet = string.Empty;
    //    public string Antet
    //    {
    //        get
    //        {
    //            return antet;
    //        }
    //        set
    //        {
    //            antet = value;
    //        }
    //    }
    //    private string subsol = string.Empty;
    //    public string Subsol
    //    {
    //        get
    //        {
    //            return subsol;
    //        }
    //        set
    //        {
    //            subsol = value;
    //        }
    //    }
    //    private string continut = string.Empty;
    //    public string Continut
    //    {
    //        get
    //        {
    //            return continut;
    //        }
    //        set
    //        {
    //            continut = value;
    //        }
    //    }

    //    public Adeverinta()
    //    {

    //    }
    //}
}
