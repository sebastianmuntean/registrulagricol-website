﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CulturiServices
/// </summary>
public class CulturiServices
{
    internal static bool AdaugaInParceleCatreCapitole(Parcele parcela)
    {
        if (parcela.Id == 0)
        {
            return false;
        }

        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(HttpContext.Current.Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;

        vCmd.CommandText = @"INSERT INTO [parceleCatreCapitole]
                                        ([parcelaId]
                                        ,[c4aRand]
                                        ,[c4aHa]
                                        ,[c4aAri]
                                        ,[c4a1Rand]
                                        ,[c4a1Ha]
                                        ,[c4a1Ari]
                                        ,[c4bRand]
                                        ,[c4bAri]
                                        ,[c4b2Rand]
                                        ,[c4b2Arie]
                                        ,[c4cRand]
                                        ,[c4cAri]
                                        ,[anul]
                                        ,[unitateId]
                                        ,[gospodarieId])
                                  VALUES(
                                         @parcelaId
                                        ,@c4aRand
                                        ,@c4aHa
                                        ,@c4aAri
                                        ,@c4a1Rand
                                        ,@c4a1Ha
                                        ,@c4a1Ari
                                        ,@c4bRand
                                        ,@c4bArie
                                        ,@c4b2Rand
                                        ,@c4b2Arie
                                        ,@c4cRand
                                        ,@c4cArie
                                        ,@an
                                        ,@unitateId
                                        ,@gospodarieId
                                        )";

        vCmd.Parameters.AddWithValue("@parcelaId", parcela.Id);
        vCmd.Parameters.AddWithValue("@c4aRand", parcela.CulturaIn4a[parcela.CulturaIn4a.Count-1].CodRand);
        vCmd.Parameters.AddWithValue("@c4aHa", ((Suprafete)parcela.CulturaIn4a[parcela.CulturaIn4a.Count-1].SuprafataCultivata).Hectare);
        vCmd.Parameters.AddWithValue("@c4aAri", ((Suprafete)parcela.CulturaIn4a[parcela.CulturaIn4a.Count-1].SuprafataCultivata).Ari);
        vCmd.Parameters.AddWithValue("@c4a1Rand", parcela.CulturaIn4a1[parcela.CulturaIn4a1.Count-1].CodRand);
        vCmd.Parameters.AddWithValue("@c4a1Ha", ((Suprafete)parcela.CulturaIn4a1[parcela.CulturaIn4a1.Count-1].SuprafataCultivata).Hectare);
        vCmd.Parameters.AddWithValue("@c4a1Ari", ((Suprafete)parcela.CulturaIn4a1[parcela.CulturaIn4a1.Count-1].SuprafataCultivata).Ari);
        vCmd.Parameters.AddWithValue("@c4bRand", parcela.CulturaIn4b1[parcela.CulturaIn4b1.Count-1].CodRand);
        vCmd.Parameters.AddWithValue("@c4bArie", ((Arii)parcela.CulturaIn4b1[parcela.CulturaIn4b1.Count - 1].SuprafataCultivata).Arie);
        vCmd.Parameters.AddWithValue("@c4b2Rand", parcela.CulturaIn4b2[parcela.CulturaIn4b2.Count-1].CodRand);
        vCmd.Parameters.AddWithValue("@c4b2Arie", ((Arii)parcela.CulturaIn4b2[parcela.CulturaIn4b2.Count - 1].SuprafataCultivata).Arie);
        vCmd.Parameters.AddWithValue("@c4cRand", parcela.CulturaIn4c[parcela.CulturaIn4c.Count-1].CodRand);
        vCmd.Parameters.AddWithValue("@c4cArie", ((Arii)parcela.CulturaIn4c[parcela.CulturaIn4c.Count-1].SuprafataCultivata).Arie);
        vCmd.Parameters.AddWithValue("@an", parcela.An);
        vCmd.Parameters.AddWithValue("@unitateId", parcela.UnitateId);
        vCmd.Parameters.AddWithValue("@gospodarieId", parcela.IdGospodarie);

        try
        {
            vCmd.ExecuteNonQuery();
            
            return true;
        }
        catch
        {
            return false;
        }
        finally
        {
            vCon.Close();
        }
    }
 
    public static List<Culturi> GetCulturiTotale(Culturi culturi)
    {
        List<Culturi> listaCulturi = new List<Culturi>();

        if (culturi.IdGospodarie == 0 || culturi.Capitol.Cod == null || culturi.An == 0)
        {
            return listaCulturi;
        }

        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(HttpContext.Current.Session["SESan"]));
        SqlCommand command = new SqlCommand();
        command.Connection = vCon;

        command.CommandText = @"SELECT [capitolId]
                                      ,[col1]
                                      ,[col2]
                                      ,[codRand]
                                 FROM  [capitole] 
                                 WHERE an = @an AND codCapitol = @codCapitol AND gospodarieId = @gospodarieId 
                                ";
        string codCapitol = culturi.Capitol.Cod.ToString().ToLower();
        if ( codCapitol == "b1" )
            codCapitol = codCapitol.Replace("1", "");

        command.Parameters.AddWithValue("@codCapitol", "4" + codCapitol);
        command.Parameters.AddWithValue("@gospodarieId", culturi.IdGospodarie);
        command.Parameters.AddWithValue("@an", culturi.An);

        SqlDataReader culturiDataReader = command.ExecuteReader();

        if (culturiDataReader.HasRows)
        {
            while (culturiDataReader.Read())
            {
                Culturi cultura = new CulturiTotale();
                cultura.Capitol = new Capitole();

                cultura.Id = Convert.ToInt64(culturiDataReader["capitolId"].ToString());
                cultura.CodRand = culturiDataReader["codRand"] != DBNull.Value ? culturiDataReader["codRand"].ToString() : string.Empty;
                cultura.An = culturi.An;
                cultura.Capitol.Cod = culturi.Capitol.Cod;
                cultura.UnitateId = culturi.UnitateId;
                cultura.IdGospodarie = culturi.IdGospodarie;

                cultura.SuprafataCultivata = new Suprafete
                {
                    Hectare = culturiDataReader["col1"] != DBNull.Value ? Convert.ToDecimal(culturiDataReader["col1"]) : 0,
                    Ari = culturiDataReader["col2"] != DBNull.Value ? Convert.ToDecimal(culturiDataReader["col2"]) : 0
                };

                listaCulturi.Add(cultura);
            }
        }

        culturiDataReader.Close();
      
        ManipuleazaBD.InchideConexiune(vCon);

        return listaCulturi;
    }

    public static bool UpdateCulturi(Culturi cultura)
    {
        if (cultura.Id == 0)
        {
            return false;
        }

        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(HttpContext.Current.Session["SESan"]));
        SqlCommand command = new SqlCommand();
        command.Connection = vCon;

        if (cultura is CulturiIn4a)
        {
           return UpdateCulturiIn4a(command, cultura);
        }
        else
        {
            if (cultura is CulturiIn4a1)
            {
               return UpdateCulturiIn4a1(command, cultura);
            }
            else
            {
                if (cultura is CulturiIn4b1)
                {
                   return UpdateCulturiIn4b1(command, cultura);
                }
                else
                {
                    if (cultura is CulturiIn4b2)
                    {
                       return UpdateCulturiIn4b2(command, cultura);
                    }
                    else
                    {
                        if (cultura is CulturiIn4c)
                        {
                           return UpdateCulturiIn4c(command, cultura);
                        }
                    }
                }
            }
        }

        ManipuleazaBD.InchideConexiune(vCon);

        return false;
    }
   
    private static bool UpdateCulturiIn4a(SqlCommand command, Culturi cultura)
    {
        command.CommandText = @"UPDATE   parceleCatreCapitole SET
                                         c4aHa = @hectare
                                        ,c4aAri = @ari
                                 WHERE   id = @id
                                ";

        command.Parameters.AddWithValue("@id", cultura.Id);
        command.Parameters.AddWithValue("@hectare", ((Suprafete)cultura.SuprafataCultivata).Hectare);
        command.Parameters.AddWithValue("@ari", ((Suprafete)cultura.SuprafataCultivata).Ari);
       
        try
        {
            command.ExecuteNonQuery();

            return true;
        }
        catch
        {
            return false;
        }
    }

    private static bool UpdateCulturiIn4a1(SqlCommand command, Culturi cultura)
    {
        command.CommandText = @"UPDATE   parceleCatreCapitole SET
                                         c4a1Ha = @hectare
                                        ,c4a1Ari = @ari
                                 WHERE   id = @id
                                ";

        command.Parameters.AddWithValue("@id", cultura.Id);
        command.Parameters.AddWithValue("@hectare", ((Suprafete)cultura.SuprafataCultivata).Hectare);
        command.Parameters.AddWithValue("@ari", ((Suprafete)cultura.SuprafataCultivata).Ari);

        try
        {
            command.ExecuteNonQuery();

            return true;
        }
        catch
        {
            return false;
        }
    }

    private static bool UpdateCulturiIn4b1(SqlCommand command, Culturi cultura)
    {
        command.CommandText = @"UPDATE   parceleCatreCapitole SET
                                         c4bAri = @arie
                                 WHERE   id = @id
                                ";

        command.Parameters.AddWithValue("@id", cultura.Id);
        command.Parameters.AddWithValue("@arie", ((Arii)cultura.SuprafataCultivata).Arie);

        try
        {
            command.ExecuteNonQuery();

            return true;
        }
        catch
        {
            return false;
        }
    }

    private static bool UpdateCulturiIn4b2(SqlCommand command, Culturi cultura)
    {
        command.CommandText = @"UPDATE   parceleCatreCapitole SET
                                         c4b2Arie = @arie
                                 WHERE   id = @id
                                ";

        command.Parameters.AddWithValue("@id", cultura.Id);
        command.Parameters.AddWithValue("@arie", ((Arii)cultura.SuprafataCultivata).Arie);

        try
        {
            command.ExecuteNonQuery();

            return true;
        }
        catch
        {
            return false;
        }
    }

    private static bool UpdateCulturiIn4c(SqlCommand command, Culturi cultura)
    {
        command.CommandText = @"UPDATE   parceleCatreCapitole SET
                                         c4cAri = @arie
                                 WHERE   id = @id
                                ";

        command.Parameters.AddWithValue("@id", cultura.Id);
        command.Parameters.AddWithValue("@arie", ((Arii)cultura.SuprafataCultivata).Arie);

        try
        {
            command.ExecuteNonQuery();

            return true;
        }
        catch
        {
            return false;
        }
    }

    public static bool UpdateCulturiTotale(CulturiTotale cultura)
    {
        if(cultura.Id == 0)
        {
            return false;
        }

        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(HttpContext.Current.Session["SESan"]));
        SqlCommand command = new SqlCommand();
        command.Connection = vCon;

        command.CommandText = @"UPDATE   capitole SET
                                         col1 = @col1
                                        ,col2 = @col2
                                 FROM    capitole 
                                 WHERE   capitolId = @idCapitol
                                ";

        command.Parameters.AddWithValue("@idCapitol", cultura.Id);
        command.Parameters.AddWithValue("@col1", ((Suprafete)cultura.SuprafataCultivata).Hectare);

        if(cultura.Capitol.Cod == Utils.CapitolCulturi.A || cultura.Capitol.Cod == Utils.CapitolCulturi.A1)
        {
            command.Parameters.AddWithValue("@col2", ((Suprafete)cultura.SuprafataCultivata).Ari);
        }
        else
        {
            command.Parameters.AddWithValue("@col2", ((Arii)((Suprafete)cultura.SuprafataCultivata).ToMetriPatrati()).Arie);
        }

        try
        {
            command.ExecuteNonQuery();
            
            return true;
        }
        catch
        {
            return false;
        }
        finally
        {
            ManipuleazaBD.InchideConexiune(vCon);
        }
    }

    internal static List<Culturi> GetCulturiInParcela(Culturi culturi)
    {
        List<Culturi> listaCulturi = new List<Culturi>();

        if (culturi.ParcelaId == 0)
        {
            return listaCulturi;
        }

        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(HttpContext.Current.Session["SESan"]));
        SqlCommand command = new SqlCommand();
        command.Connection = vCon;

        if (culturi is CulturiIn4a)
        {
            GetCulturiIn4aInParcela(command, culturi, listaCulturi);
        }
        else
        {
            if (culturi is CulturiIn4a1)
            {
                GetCulturiIn4a1InParcela(command, culturi, listaCulturi);
            }
            else
            {
                if (culturi is CulturiIn4b1)
                {
                    GetCulturiIn4b1InParcela(command, culturi, listaCulturi);
                }
                else
                {
                    if (culturi is CulturiIn4b2)
                    {
                        GetCulturiIn4b2InParcela(command, culturi, listaCulturi);
                    }
                    else
                    {
                        if (culturi is CulturiIn4c)
                        {
                            GetCulturiIn4cInParcela(command, culturi, listaCulturi);
                        }
                    }
                }
            }
        }

        ManipuleazaBD.InchideConexiune(vCon);

        return listaCulturi;
    }

    private static void GetCulturiIn4aInParcela(SqlCommand command, Culturi culturi, List<Culturi> listaCulturi)
    {
        command.CommandText = @"SELECT [id]
                                      ,[parcelaId]
                                      ,[c4aRand]
                                      ,[c4aHa]
                                      ,[c4aAri]
	                                  ,denumire1
                                FROM  [parceleCatreCapitole] 
                                INNER JOIN sabloaneCapitole ON codRand = c4aRand
                                WHERE anul = @an AND parcelaId = @idParcela AND an = @an AND  capitol = '4a' and gospodarieId = @gospodarieId
                                ";

        command.Parameters.AddWithValue("@idParcela", culturi.ParcelaId);
        command.Parameters.AddWithValue("@an", culturi.An);
        command.Parameters.AddWithValue("@gospodarieId", culturi.IdGospodarie);

        SqlDataReader culturiDataReader = command.ExecuteReader();
    
        if ( culturiDataReader.HasRows )
        {
            while ( culturiDataReader.Read() )
            {
                Culturi cultura = new CulturiIn4a();
                
                cultura.ParcelaId = culturi.ParcelaId;
                cultura.Id = Convert.ToInt64(culturiDataReader["id"].ToString());
                cultura.An = culturi.An;
                cultura.CodRand = culturiDataReader["c4aRand"] != DBNull.Value ? culturiDataReader["c4aRand"].ToString().Trim() : string.Empty;
                cultura.Denumire = culturiDataReader["denumire1"] != DBNull.Value ? culturiDataReader["denumire1"].ToString() : string.Empty;
                cultura.IdGospodarie = culturi.IdGospodarie;
                cultura.UnitateId = culturi.UnitateId;
                
                cultura.SuprafataCultivata = new Suprafete
                {
                    Hectare = culturiDataReader["c4aHa"] != DBNull.Value ? Convert.ToDecimal(culturiDataReader["c4aHa"]) : 0,
                    Ari = culturiDataReader["c4aAri"] != DBNull.Value ? Convert.ToDecimal(culturiDataReader["c4aAri"]) : 0
                };

                listaCulturi.Add(cultura);
            }
        }

        culturiDataReader.Close();
    }

    private static void GetCulturiIn4a1InParcela(SqlCommand command, Culturi culturi, List<Culturi> listaCulturi)
    {
        command.CommandText = @"SELECT [id]
                                      ,[parcelaId]
                                      ,[c4a1Rand]
                                      ,[c4a1Ha]
                                      ,[c4a1Ari]
	                                  ,denumire1
                                FROM  [parceleCatreCapitole] 
                                INNER JOIN sabloaneCapitole ON codRand = c4a1Rand
                                WHERE anul = @an AND parcelaId = @idParcela AND an = @an AND  capitol = '4a1' and gospodarieId = @gospodarieId
                                ";

        command.Parameters.AddWithValue("@idParcela", culturi.ParcelaId);
        command.Parameters.AddWithValue("@an", culturi.An);
        command.Parameters.AddWithValue("@gospodarieId", culturi.IdGospodarie);

        SqlDataReader culturiDataReader = command.ExecuteReader();

        if (culturiDataReader.HasRows)
        {
            while (culturiDataReader.Read())
            {
                Culturi cultura = new CulturiIn4a1();

                cultura.ParcelaId = culturi.ParcelaId;
                cultura.Id = Convert.ToInt64(culturiDataReader["id"].ToString());
                cultura.An = culturi.An;
                cultura.CodRand = culturiDataReader["c4a1Rand"] != DBNull.Value ? culturiDataReader["c4a1Rand"].ToString().Trim() : string.Empty;
                cultura.Denumire = culturiDataReader["denumire1"] != DBNull.Value ? culturiDataReader["denumire1"].ToString() : string.Empty;
                cultura.IdGospodarie = culturi.IdGospodarie;
                cultura.UnitateId = culturi.UnitateId;

                cultura.SuprafataCultivata = new Suprafete
                {
                    Hectare = culturiDataReader["c4a1Ha"] != DBNull.Value ? Convert.ToDecimal(culturiDataReader["c4a1Ha"]) : 0,
                    Ari = culturiDataReader["c4a1Ari"] != DBNull.Value ? Convert.ToDecimal(culturiDataReader["c4a1Ari"]) : 0
                };

                listaCulturi.Add(cultura);
            }
        }

        culturiDataReader.Close();
    }

    private static void GetCulturiIn4b1InParcela(SqlCommand command, Culturi culturi, List<Culturi> listaCulturi)
    {
        command.CommandText = @"SELECT [id]
                                      ,[parcelaId]
                                      ,[c4bRand]
                                      ,[c4bAri]
	                                  ,denumire1
                                FROM  [parceleCatreCapitole] 
                                INNER JOIN sabloaneCapitole ON codRand = c4bRand
                                WHERE anul = @an AND parcelaId = @idParcela AND an = @an AND  capitol = '4b' and gospodarieId = @gospodarieId
                                ";

        command.Parameters.AddWithValue("@idParcela", culturi.ParcelaId);
        command.Parameters.AddWithValue("@an", culturi.An);
        command.Parameters.AddWithValue("@gospodarieId", culturi.IdGospodarie);

        SqlDataReader culturiDataReader = command.ExecuteReader();

        if (culturiDataReader.HasRows)
        {
            while (culturiDataReader.Read())
            {
                Culturi cultura = new CulturiIn4b1();

                cultura.ParcelaId = culturi.ParcelaId;
                cultura.Id = Convert.ToInt64(culturiDataReader["id"].ToString());
                cultura.An = culturi.An;
                cultura.CodRand = culturiDataReader["c4bRand"] != DBNull.Value ? culturiDataReader["c4bRand"].ToString().Trim() : string.Empty;
                cultura.Denumire = culturiDataReader["denumire1"] != DBNull.Value ? culturiDataReader["denumire1"].ToString() : string.Empty;
                cultura.IdGospodarie = culturi.IdGospodarie;
                cultura.UnitateId = culturi.UnitateId;

                cultura.SuprafataCultivata = new Arii
                {
                    Arie = culturiDataReader["c4bAri"] != DBNull.Value ? Convert.ToDecimal(culturiDataReader["c4bAri"]) : 0
                };

                listaCulturi.Add(cultura);
            }
        }

        culturiDataReader.Close();
    }

    private static void GetCulturiIn4b2InParcela(SqlCommand command, Culturi culturi, List<Culturi> listaCulturi)
    {
        command.CommandText = @"SELECT [id]
                                      ,[parcelaId]
                                      ,[c4b2Rand]
                                      ,[c4b2Arie]
	                                  ,denumire1
                                FROM  [parceleCatreCapitole] 
                                INNER JOIN sabloaneCapitole ON codRand = c4b2Rand
                                WHERE anul = @an AND parcelaId = @idParcela AND an = @an AND  capitol = '4b2' and gospodarieId = @gospodarieId
                                ";

        command.Parameters.AddWithValue("@idParcela", culturi.ParcelaId);
        command.Parameters.AddWithValue("@an", culturi.An);
        command.Parameters.AddWithValue("@gospodarieId", culturi.IdGospodarie);

        SqlDataReader culturiDataReader = command.ExecuteReader();

        if (culturiDataReader.HasRows)
        {
            while (culturiDataReader.Read())
            {
                Culturi cultura = new CulturiIn4b2();

                cultura.ParcelaId = culturi.ParcelaId;
                cultura.Id = Convert.ToInt64(culturiDataReader["id"].ToString());
                cultura.An = culturi.An;
                cultura.CodRand = culturiDataReader["c4b2Rand"] != DBNull.Value ? culturiDataReader["c4b2Rand"].ToString().Trim() : string.Empty;
                cultura.Denumire = culturiDataReader["denumire1"] != DBNull.Value ? culturiDataReader["denumire1"].ToString() : string.Empty;
                cultura.IdGospodarie = culturi.IdGospodarie;
                cultura.UnitateId = culturi.UnitateId;

                cultura.SuprafataCultivata = new Arii
                {
                    Arie = culturiDataReader["c4b2Arie"] != DBNull.Value ? Convert.ToDecimal(culturiDataReader["c4b2Arie"]) : 0
                };

                listaCulturi.Add(cultura);
            }
        }

        culturiDataReader.Close();
    }

    private static void GetCulturiIn4cInParcela(SqlCommand command, Culturi culturi, List<Culturi> listaCulturi)
    {
        command.CommandText = @"SELECT [id]
                                      ,[parcelaId]
                                      ,[c4cRand]
                                      ,[c4cAri]
	                                  ,denumire1
                                FROM  [parceleCatreCapitole] 
                                INNER JOIN sabloaneCapitole ON codRand = c4cRand
                                WHERE anul = @an AND parcelaId = @idParcela AND an = @an AND  capitol = '4c' and gospodarieId = @gospodarieId
                                ";

        command.Parameters.AddWithValue("@idParcela", culturi.ParcelaId);
        command.Parameters.AddWithValue("@an", culturi.An);
        command.Parameters.AddWithValue("@gospodarieId", culturi.IdGospodarie);

        SqlDataReader culturiDataReader = command.ExecuteReader();

        if (culturiDataReader.HasRows)
        {
            while (culturiDataReader.Read())
            {
                Culturi cultura = new CulturiIn4c();

                cultura.ParcelaId = culturi.ParcelaId;
                cultura.Id = Convert.ToInt64(culturiDataReader["id"].ToString());
                cultura.An = culturi.An;
                cultura.CodRand = culturiDataReader["c4cRand"] != DBNull.Value ? culturiDataReader["c4cRand"].ToString().Trim() : string.Empty;
                cultura.Denumire = culturiDataReader["denumire1"] != DBNull.Value ? culturiDataReader["denumire1"].ToString() : string.Empty;
                cultura.IdGospodarie = culturi.IdGospodarie;
                cultura.UnitateId = culturi.UnitateId;

                cultura.SuprafataCultivata = new Arii
                {
                    Arie = culturiDataReader["c4cAri"] != DBNull.Value ? Convert.ToDecimal(culturiDataReader["c4cAri"]) : 0
                };

                listaCulturi.Add(cultura);
            }
        }

        culturiDataReader.Close();
    }

    internal static bool StergeCultura(Culturi cultura)
    {
        if(cultura.Id ==0)
        {
            return false;
        }

        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(HttpContext.Current.Session["SESan"]));
        SqlCommand command = new SqlCommand();
      
        command.Connection = connection;
        command.Transaction = connection.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);
        
        StergeDinParceleCatreCapitole(command, cultura);
        
        try
        {
            command.Transaction.Commit();

            return true;
        }
        catch
        {
            command.Transaction.Rollback();

            return false;
        }
        finally
        {
            connection.Close();
        }
    }

    private static void StergeDinParceleCatreCapitole(SqlCommand command, Culturi cultura)
    {
        command.CommandText = @"DELETE FROM parceleCatreCapitole WHERE id = @id";
        command.Parameters.AddWithValue("@id", cultura.Id);
      
        try
        {
            command.ExecuteNonQuery();
        }
        catch
        {
            throw;
        }
    }
}