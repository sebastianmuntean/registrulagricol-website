﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
/// <summary>
/// Summary description for Services
/// </summary>
public class VanzareTerenExtravilanServices
{
    static SqlCommand command = new SqlCommand();

	public VanzareTerenExtravilanServices()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static DataTable GetListaParceleExtravilanByUser()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(HttpContext.Current.Session["SESan"]));
        DataTable table = CreateListaParceleTableStructure();
        command.Connection = connection;
        command.CommandText = @"SELECT parcelaCodRand, parcele.parcelaId, COALESCE(parcele.parcelaDenumire,'') as parcelaDenumire, COALESCE(parcele.parcelaCodRand,'') as parcelaCodRand, COALESCE(parcele.parcelaSuprafataExtravilanHa,'0') as parcelaSuprafataExtravilanHa, COALESCE(parcele.parcelaSuprafataExtravilanAri,'0') as parcelaSuprafataExtravilanAri, COALESCE(parcele.parcelaNrTopo,'') as parcelaNrTopo, COALESCE(parcele.parcelaCategorie,'') as parcelaCategorie, COALESCE(parcele.parcelaCF,'') as parcelaCF, COALESCE (sabloaneCapitole.denumire4, N' --- ') AS denumire4, COALESCE(parcele.parcelaNrBloc,'') as parcelaNrBloc, COALESCE(parcele.parcelaMentiuni,'') as parcelaMentiuni, COALESCE(parcele.an,'') as an, COALESCE(parcele.parcelaLocalitate,'') as parcelaLocalitate, COALESCE(parcele.parcelaAdresa,'') as parcelaAdresa, COALESCE(parcele.parcelaNrCadastralProvizoriu,'') as parcelaNrCadastralProvizoriu, COALESCE(parcele.parcelaNrCadastral,'') as parcelaNrCadastral, COALESCE(parcele.parcelaTarla,'') as parcelaTarla, COALESCE(parcele.parcelaTitluProprietate,'') as parcelaTitluProprietate, COALESCE(parcele.parcelaClasaBonitate,'0') as parcelaClasaBonitate, COALESCE(parcele.parcelaPunctaj,'0') as parcelaPunctaj 
                    FROM parcele INNER JOIN sabloaneCapitole ON parcele.parcelaCategorie = sabloaneCapitole.codRand 
                    WHERE (sabloaneCapitole.capitol = '2a') 
                    AND (parcele.gospodarieId = " + HttpContext.Current.Session["SESgospodarieId"] + @") 
                    AND (parcele.an = " + HttpContext.Current.Session["SESan"] + ") AND (sabloaneCapitole.an = " + HttpContext.Current.Session["SESan"] + @")  
                    AND (parcele.parcelaCategorie LIKE  '" + MasterModel.parcelaCategorie + @"' ) 
                    AND (parcele.parcelaCF LIKE '%" + MasterModel.parcelaCF + @"%') 
                    AND (parcele.parcelaDenumire LIKE '%" + MasterModel.parcelaDenumire + @"%') 
                    AND (parcele.parcelaNrBloc LIKE '%" + MasterModel.parcelaBloc + @"%') 
                    AND (parcele.parcelaNrTopo LIKE '%" + MasterModel.parcelaTopo + @"%') 
                    AND (parcele.parcelaNrCadastral LIKE '%" + MasterModel.parcelaNrCadastral + @"%') 
                    AND (parcele.parcelaNrCadastralProvizoriu LIKE '%" + MasterModel.parcelaNrCadastralProvizoriu + @"%') 
                    AND ((parcele.parcelaSuprafataExtravilanAri > 0) or (parcele.parcelaSuprafataExtravilanHa > 0))";
        SqlDataReader dataReader = command.ExecuteReader();
        while (dataReader.Read())
        {
            PopulateListaParceleTable(table, dataReader);
        }
        ManipuleazaBD.InchideConexiune(connection);
        return table;
    }

    private static void PopulateListaParceleTable(DataTable table, SqlDataReader dataReader)
    {
        DataRow row = table.NewRow();
        row["parcelaId"] = dataReader["parcelaId"];
        row["Denumire Parcela"] = dataReader["parcelaDenumire"];
        row["Extravilan Ha"] = dataReader["parcelaSuprafataExtravilanHa"];
        row["Extravilan Ari"] = dataReader["parcelaSuprafataExtravilanAri"];
        row["Nr TOPO"] = dataReader["parcelaNrTopo"];
        row["CF"] = dataReader["parcelaCF"];
        row["Nr. Cadastral"] = dataReader["parcelaNrCadastral"];
        row["Nr. Cadastral provizioriu"] = dataReader["parcelaNrCadastralProvizoriu"];
        row["Bloc fizic"] = dataReader["parcelaNrBloc"];
        row["Adresa"] = dataReader["parcelaAdresa"];
        row["Categorie"] = dataReader["denumire4"];
        table.Rows.Add(row);
    }

    private static DataTable CreateListaParceleTableStructure()
    {
        DataTable table = new DataTable();
        table.Columns.Add("parcelaId");
        table.Columns.Add("Denumire parcela");
        table.Columns.Add("Extravilan Ha");
        table.Columns.Add("Extravilan Ari");
        table.Columns.Add("Categorie");
        table.Columns.Add("Nr TOPO");
        table.Columns.Add("CF");
        table.Columns.Add("Nr. Cadastral");
        table.Columns.Add("Nr. Cadastral provizioriu");
        table.Columns.Add("Bloc Fizic");
        table.Columns.Add("Adresa");
        return table;
    }

    public static DataTable GetListaOferte()
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(HttpContext.Current.Session["SESan"]));
        DataTable table = CreateListaOferteTableStructure();
        command.Connection = connection;
        command.CommandText = "select * FROM OferteVanzare JOIN parcele on OferteVanzare.idParcela = parcele.parcelaId  INNER JOIN sabloaneCapitole ON parcele.parcelaCategorie = sabloaneCapitole.codRand WHERE (sabloaneCapitole.capitol = '2a') AND (parcele.gospodarieId = " + HttpContext.Current.Session["SESgospodarieId"] + ") AND (parcele.an = " + HttpContext.Current.Session["SESan"] + ") AND (sabloaneCapitole.an = " + HttpContext.Current.Session["SESan"] + ") AND OferteVanzare.nrDosar LIKE '%" + MasterModel.nrDosar + "%' AND OferteVanzare.nrRegistruGeneral LIKE '%" + MasterModel.nrRegistruGeneral + "%'  AND (parcele.parcelaCF LIKE '%" + MasterModel.parcelaCF + "%') AND (parcele.parcelaDenumire LIKE '%" + MasterModel.parcelaDenumire + "%') AND (parcele.parcelaNrBloc LIKE '%" + MasterModel.parcelaBloc + "%') AND (parcele.parcelaNrTopo LIKE '%" + MasterModel.parcelaTopo + "%') AND (parcele.parcelaNrCadastral LIKE '%" + MasterModel.parcelaNrCadastral+ "%') AND (parcele.parcelaNrCadastralProvizoriu LIKE '%" + MasterModel.parcelaNrCadastralProvizoriu + "%')  AND (parcele.parcelaCategorie LIKE  '" + MasterModel.parcelaCategorie + "' )";
        SqlDataReader dataReader = command.ExecuteReader();

        while (dataReader.Read())
        {
            PopulateListaOferteTable(table, dataReader);
        }
        ManipuleazaBD.InchideConexiune(connection);
        return table;
    }

    private static void PopulateListaOferteTable(DataTable table, SqlDataReader dataReader)
    {
        DataRow row = table.NewRow();
        row["idOferta"] = dataReader["idOferta"];
        row["Nr. Cerere"] = dataReader["nrDosar"];
        row["Nr. Registru general"] = dataReader["nrRegistruGeneral"];
        row["Data inregistrarii"] = Convert.ToDateTime(dataReader["dataInregistrarii"]).ToShortDateString();
        row["Denumire parcela"] = dataReader["parcelaDenumire"];
        row["Nr TOPO"] = dataReader["parcelaNrTopo"];
        row["CF"] = dataReader["parcelaCF"];
        row["Nr. Cadastral"] = dataReader["parcelaNrCadastral"];
        row["Nr. Cadastral provizioriu"] = dataReader["parcelaNrCadastralProvizoriu"];
        row["Bloc fizic"] = dataReader["parcelaNrBloc"];
        row["Adresa"] = dataReader["parcelaAdresa"];
        row["Suprafata oferita Ha"] = dataReader["suprafataOferitaHa"];
        row["Suprafata oferita Ari"] = dataReader["suprafataOferitaAri"];
        row["Valoare parcela (lei)"] = dataReader["valoareParcela"];
        row["Categorie"] = dataReader["denumire4"];
        table.Rows.Add(row);
    }

    private static DataTable CreateListaOferteTableStructure()
    {
        DataTable table = new DataTable();
        table.Columns.Add("idOferta");
        table.Columns.Add("Nr. Cerere");
        table.Columns.Add("Nr. Registru general");
        table.Columns.Add("Data inregistrarii");
        table.Columns.Add("Denumire parcela");
        table.Columns.Add("Categorie");
        table.Columns.Add("Nr TOPO");
        table.Columns.Add("CF");
        table.Columns.Add("Nr. Cadastral");
        table.Columns.Add("Nr. Cadastral provizioriu");
        table.Columns.Add("Bloc Fizic");
        table.Columns.Add("Adresa");
        table.Columns.Add("Suprafata oferita Ha");
        table.Columns.Add("Suprafata oferita Ari");
        table.Columns.Add("Valoare parcela (lei)");
        return table;
    }

    public static DataTable GetListaCereri(short an)
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(an);
        DataTable table = CreateListaCereriTableStructure();
        command.Connection = connection;
        command.CommandText = "Select idCerere,numeOfertant,CereriVanzare.dataInregistrarii,sumaOferita from CereriVanzare join OferteVanzare on CereriVanzare.idOferta = OferteVanzare.idOferta where(CereriVanzare.idOferta = '" + HttpContext.Current.Session["SESidOferta"] + "') and numeOfertant LIKE '%" + MasterModel.numeOfertant + "%'";
        SqlDataReader dataReader = command.ExecuteReader();

        while (dataReader.Read())
        {
           PopulateListaCereriTable(table, dataReader);
        }
        ManipuleazaBD.InchideConexiune(connection);
        return table;

    }

    private static void PopulateListaCereriTable(DataTable table, SqlDataReader dataReader)
    {
        DataRow row = table.NewRow();
        row["idCerere"] = dataReader["idCerere"];
        row["Nume ofertant"] = dataReader["numeOfertant"];
        row["Data inregistrarii"] = Convert.ToDateTime(dataReader["dataInregistrarii"]).ToShortDateString();
        row["Suma oferita (lei)"] = dataReader["sumaOferita"];
        table.Rows.Add(row);
    }

    private static DataTable CreateListaCereriTableStructure()
    {
        DataTable table = new DataTable();
        table.Columns.Add("idCerere");
        table.Columns.Add("Nume ofertant");
        table.Columns.Add("Data inregistrarii");
        table.Columns.Add("Suma oferita (lei)");
        return table;
    }

    public static bool IsPersoanaFizica(short an)
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(an);
        command.Connection = connection;
        command.CommandText = "select persJuridica from  gospodarii  where gospodarii.gospodarieId = " + HttpContext.Current.Session["SESgospodarieId"].ToString() + " and unitateId = " + HttpContext.Current.Session["SESunitateId"].ToString() + " and an = " + HttpContext.Current.Session["SESan"] + " and persJuridica = 0";
        SqlDataReader dataReader = command.ExecuteReader();
        if (dataReader.Read())
        {
            ManipuleazaBD.InchideConexiune(connection);
            return true;
        }
        ManipuleazaBD.InchideConexiune(connection);
        return false;
    }

    public static string GetNumeMembruSelectat(short an)
    {
        string numeMembruSelectat = string.Empty;
        SqlConnection connection = ManipuleazaBD.CreareConexiune(an);
        command.Connection = connection;
        command.CommandText = "Select membruNume from gospodarii where gospodarieId = " + HttpContext.Current.Session["SESgospodarieId"].ToString() + " and unitateId = " + HttpContext.Current.Session["SESunitateId"].ToString() + "";
        SqlDataReader dataReader = command.ExecuteReader();
        if (dataReader.Read())
        {
            numeMembruSelectat = dataReader["membruNume"].ToString();
        }
        ManipuleazaBD.InchideConexiune(connection);
        return numeMembruSelectat;
    }

    public static bool HasCereri(short an)
    {
        int nrCereri = 0;
        SqlConnection connection = ManipuleazaBD.CreareConexiune(an);
        command.Connection = connection;
        command.CommandText = "select count(*) from CereriVanzare join OferteVanzare on CereriVanzare.idOferta = OferteVanzare.idOferta where CereriVanzare.idOferta = '" + HttpContext.Current.Session["SESidOferta"] + "'";
        nrCereri = Convert.ToInt32(command.ExecuteScalar());
        if (nrCereri == 0)
        {
            return false;
        }
        ManipuleazaBD.InchideConexiune(connection);
        return true;
    }

    public static int GetLastNumarCerere(short an)
    {
        int lastNrCerere = 0;
        SqlConnection connection = ManipuleazaBD.CreareConexiune(an);
        command.Connection = connection;
        command.CommandText = "select coalesce(max(nrDosar) + 1,1) from OferteVanzare where unitateId = " + HttpContext.Current.Session["SESunitateId"] + " and an = "+HttpContext.Current.Session["SESAn"]+" ";
        lastNrCerere = Convert.ToInt32(command.ExecuteScalar());
        ManipuleazaBD.InchideConexiune(connection);
        return lastNrCerere;
    }

    public static bool VerifyIfNumarCerereExist(string nrCerere, short an)
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(an);
        command.Connection = connection;
        command.CommandText = "Select coalesce(nrDosar,0) as nrDosar from OferteVanzare where unitateId = " + HttpContext.Current.Session["SESunitateId"] + " and an = "+HttpContext.Current.Session["SESAn"]+" ";
        SqlDataReader dataReader = command.ExecuteReader();
        while (dataReader.Read())
        {
            string x = dataReader["nrDosar"].ToString();
            if (nrCerere == dataReader["nrDosar"].ToString())
            {
                ManipuleazaBD.InchideConexiune(connection);
                return false;
            }
        }
        ManipuleazaBD.InchideConexiune(connection);
        return true;
    }

    public static string GetNumarCarteFunciara(short an)
    {
        string nrCarteFunciara = string.Empty;
        SqlConnection connection = ManipuleazaBD.CreareConexiune(an);
        command.Connection = connection;
        command.CommandText = "select parcele.parcelaCF FROM parcele WHERE parcelaId = " + HttpContext.Current.Session["SESparcelaId"] + " and gospodarieId = " + HttpContext.Current.Session["SESgospodarieId"] + " and unitateaId = " + HttpContext.Current.Session["SESunitateId"] + "";
        SqlDataReader dataReader = command.ExecuteReader();
        if (dataReader.Read())
        {
            nrCarteFunciara = dataReader["parcelaCF"].ToString();
        }
        ManipuleazaBD.InchideConexiune(connection);
        return nrCarteFunciara;
    }

    public static DataTable GetCap14(int unitateid, int gospodarieid, int an)
    {
        SqlConnection connection = ManipuleazaBD.CreareConexiune(Convert.ToInt16(an));
        string interogare = @"select OferteVanzare.idOferta,
convert(varchar(20),nrDosar) + '/' + convert(varchar(20), ofertevanzare.dataInregistrarii, 104) as nrDataOferta,
 Convert(decimal(16,2),suprafataOferitaHa + suprafataOferitaAri/100) as suprafata,
nrCarteFunciara ,Coalesce(numeOfertant,'') as cumparator,Coalesce(sumaOferita,0) as pret
from OferteVanzare
join OferteVanzareDetalii
on OferteVanzare.idOferta = OferteVanzareDetalii.idOferta
left join CereriVanzare
on CereriVanzare.idOferta = OferteVanzare.idOferta
where OferteVanzare.unitateId = " + unitateid + " and OferteVanzare.idGospodarie = " + gospodarieid + " and OferteVanzare.an = " + an + "";
        SqlDataAdapter adapter = new SqlDataAdapter(interogare, connection);
        DataTable table = new DataTable();
        adapter.Fill(table);
        ManipuleazaBD.InchideConexiune(connection);
        return table;
    }

}