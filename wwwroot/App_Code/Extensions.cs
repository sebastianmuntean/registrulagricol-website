﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Security.Cryptography;
using System.Text;

/// <summary>
/// Summary description for Extensions
/// </summary>
public static class Extensions
{
    /// <summary>
    /// Orice suprafata in metri patrati este transformata in ari. 
    /// </summary>
    /// <param name="arie">Suprafata in metri patrati</param>
    /// <returns></returns>
    public static ISuprafete ToAri(this ISuprafete arie )
    { 
        return new Suprafete
        {
            Ari = ((Arii)arie).Arie / 100
        };
    }

    public static ISuprafete ToMetriPatrati(this ISuprafete ari)
    {
        return new Arii
        {
            Arie = (((Suprafete)ari).Hectare*100+((Suprafete)ari).Ari) * 100
        };
    }

    public static string ResolveDiacritics(this string word)
    {
        return word.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
    }
}

public static class DataEncryption
{
    public static string MD5Encrypt(this string input)
    {
        MD5CryptoServiceProvider hasher = new MD5CryptoServiceProvider();
        byte[] tBytes = Encoding.ASCII.GetBytes(input);
        byte[] hBytes = hasher.ComputeHash(tBytes);

        StringBuilder sb = new StringBuilder();
        for (int c = 0; c < hBytes.Length; c++)
            sb.AppendFormat("{0:x2}", hBytes[c]);

        return (sb.ToString());
    }

    private const int Keysize = 256;

    private const int DerivationIterations = 500;

    public static string Encrypt(this string plainText)
    {
        if (plainText == string.Empty)
        {
            return string.Empty;
        }

        string passPhrase = "PoRtAl-2015(TNTComputers):Aplicatie de facilitare a accesului la informatie";
        var saltStringBytes = Generate256BitsOfRandomEntropy();
        var ivStringBytes = Generate256BitsOfRandomEntropy();
        var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
        var password = new Rfc2898DeriveBytes(passPhrase, saltStringBytes, DerivationIterations);

        var keyBytes = password.GetBytes(Keysize / 8);
        using (var symmetricKey = new RijndaelManaged())
        {
            symmetricKey.BlockSize = 256;
            symmetricKey.Mode = CipherMode.CBC;
            symmetricKey.Padding = PaddingMode.PKCS7;
            using (var encryptor = symmetricKey.CreateEncryptor(keyBytes, ivStringBytes))
            {
                using (var memoryStream = new MemoryStream())
                {
                    using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                        cryptoStream.FlushFinalBlock();
                        var cipherTextBytes = saltStringBytes;
                        cipherTextBytes = cipherTextBytes.Concat(ivStringBytes).ToArray();
                        cipherTextBytes = cipherTextBytes.Concat(memoryStream.ToArray()).ToArray();
                        memoryStream.Close();
                        cryptoStream.Close();
                        return Convert.ToBase64String(cipherTextBytes);
                    }
                }
            }
        }
    }

    public static string Decrypt(this string cipherText)
    {
        if (cipherText == string.Empty)
        {
            return string.Empty;
        }

        string passPhrase = "PoRtAl-2015(TNTComputers):Aplicatie de facilitare a accesului la informatie";
        var cipherTextBytesWithSaltAndIv = Convert.FromBase64String(cipherText);
        var saltStringBytes = cipherTextBytesWithSaltAndIv.Take(Keysize / 8).ToArray();
        var ivStringBytes = cipherTextBytesWithSaltAndIv.Skip(Keysize / 8).Take(Keysize / 8).ToArray();
        var cipherTextBytes = cipherTextBytesWithSaltAndIv.Skip((Keysize / 8) * 2).Take(cipherTextBytesWithSaltAndIv.Length - ((Keysize / 8) * 2)).ToArray();

        var password = new Rfc2898DeriveBytes(passPhrase, saltStringBytes, DerivationIterations);
        {
            var keyBytes = password.GetBytes(Keysize / 8);
            using (var symmetricKey = new RijndaelManaged())
            {
                symmetricKey.BlockSize = 256;
                symmetricKey.Mode = CipherMode.CBC;
                symmetricKey.Padding = PaddingMode.PKCS7;
                using (var decryptor = symmetricKey.CreateDecryptor(keyBytes, ivStringBytes))
                {
                    using (var memoryStream = new MemoryStream(cipherTextBytes))
                    {
                        using (var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                        {
                            var plainTextBytes = new byte[cipherTextBytes.Length];
                            var decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                            memoryStream.Close();
                            cryptoStream.Close();
                            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
                        }
                    }
                }
            }
        }
    }

    private static byte[] Generate256BitsOfRandomEntropy()
    {
        var randomBytes = new byte[32];
        randomBytes[0] = 1;
        randomBytes[1] = 128;
        randomBytes[2] = 12;
        randomBytes[3] = 36;
        randomBytes[4] = 41;
        randomBytes[5] = 93;
        randomBytes[6] = 111;
        randomBytes[7] = 85;
        randomBytes[8] = 20;
        randomBytes[9] = 31;
        randomBytes[10] = 54;
        randomBytes[11] = 67;
        randomBytes[12] = 89;
        randomBytes[13] = 10;
        randomBytes[14] = 5;
        randomBytes[15] = 8;
        randomBytes[16] = 17;
        randomBytes[17] = 26;
        randomBytes[18] = 124;
        randomBytes[19] = 105;
        randomBytes[20] = 107;
        randomBytes[21] = 23;
        randomBytes[22] = 68;
        randomBytes[23] = 74;
        randomBytes[24] = 45;
        randomBytes[25] = 82;
        randomBytes[26] = 77;
        randomBytes[27] = 14;
        randomBytes[28] = 39;
        randomBytes[29] = 127;
        randomBytes[30] = 3;
        randomBytes[31] = 4;
        //using (var rngCsp = new RNGCryptoServiceProvider())
        //{
        //    rngCsp.GetBytes(randomBytes);
        //}
        return randomBytes;
    }
}