﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;

/// <summary>
/// Creeaza Log oeratiilor
/// Creata la:                  03.02.2011
/// Autor:                      Oprician daniel
/// Modificata la:              01.04.2011 -> Datorita modificari bazei de date
/// Autor:                      SM - adaugare doua overload pentru clase, in cazul in care nu folosesc tranzactii
/// 
/// </summary>
public class ClassLog
{
    public ClassLog()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static void fLog(Int64 pUnitate, Int64 pUtilizator, DateTime pData, string pTabela, string pOperatie, string pValoareVeche, SqlCommand pCmd, Int64 pGospodarieId, Int32 pOperatieId)
    {
        Int32 vOra = pData.Hour;
        Int32 vMin = pData.Minute;
        string vUtilizatorIp = HttpContext.Current.Request.UserHostAddress;
        pCmd.CommandText = "select * from gospodarii where gospodarieId='" + pGospodarieId.ToString() + "'";
        SqlDataReader vTabel = pCmd.ExecuteReader();
        // daca e deblocare 
        string vDateGospodarie = "";
        if (pOperatie.IndexOf("|DEBLOCARE|") == -1)
            vDateGospodarie = vUtilizatorIp + " | ";
        else
            vDateGospodarie = "";
        if (vTabel.Read())
            vDateGospodarie += "Unit." + vTabel["unitateId"].ToString() + "|vol." + vTabel["volum"].ToString() + "|poz." + vTabel["nrPozitie"].ToString() + ": ";
        vTabel.Close();

        pCmd.CommandText = "INSERT INTO LogOperatii(unitateId,utilizatorId, data, ora, min, tabela, operatia, valoareVeche, operatieId) VALUES ('" + pUnitate + "','" + pUtilizator + "',convert(datetime,'" + pData.Date + "',104),'" + vOra + "','" + vMin + "',N'" + pTabela + "',N'" + vDateGospodarie + pOperatie + "',N'" + pValoareVeche + "', '" + pOperatieId.ToString() + "')";
        pCmd.CommandText = pCmd.CommandText.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
        pCmd.ExecuteNonQuery();
    }

    public static void fLog(Int64 pUnitate, Int64 pUtilizator, DateTime pData, string pTabela, string pOperatie, string pValoareVeche, Int64 pGospodarieId, Int32 pOperatieId)
    {
        Int32 vOra = pData.Hour;
        Int32 vMin = pData.Minute;
        string vUtilizatorIp = HttpContext.Current.Request.UserHostAddress;
        DBConnections connection = new DBConnections(Convert.ToInt16(DateTime.Now.Year));

        SqlConnection vCon = new SqlConnection(connection.Create());
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandType = System.Data.CommandType.Text;
        vCmd.CommandText = "select * from gospodarii where gospodarieId='" + pGospodarieId.ToString() + "'";
        using (vCon)
        {
            vCon.Open();
            SqlDataReader vTabel = vCmd.ExecuteReader();
            // daca e deblocare 
            string vDateGospodarie = "";
            if (pOperatie.IndexOf("|DEBLOCARE|") == -1)
                vDateGospodarie = vUtilizatorIp + " | ";
            else
                vDateGospodarie = "";
            if (vTabel.Read())
                vDateGospodarie += "Unit." + vTabel["unitateId"].ToString() + "|vol." + vTabel["volum"].ToString() + "|poz." + vTabel["nrPozitie"].ToString() + ": ";
            vTabel.Close();
            vCmd.CommandText = "INSERT INTO LogOperatii(unitateId,utilizatorId, data, ora, min, tabela, operatia, valoareVeche, operatieId) VALUES ('" + pUnitate + "','" + pUtilizator + "',convert(datetime,'" + pData.Date + "',104),'" + vOra + "','" + vMin + "',N'" + pTabela + "',N'" + vDateGospodarie + pOperatie + "',N'" + pValoareVeche + "', '" + pOperatieId.ToString() + "')";
            vCmd.CommandText = vCmd.CommandText.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
            vCmd.ExecuteNonQuery();
            vCon.Close();
        }
    }
    public static void fLog(Int64 pUnitate, Int64 pUtilizator, DateTime pData, string pTabela, string pOperatie, string pValoareVeche, Int64 pGospodarieId, Int32 pOperatieId, string pUtilizatorIp)
    {
        Int32 vOra = pData.Hour;
        Int32 vMin = pData.Minute;
        string vUtilizatorIp = HttpContext.Current.Request.UserHostAddress;
        DBConnections connection = new DBConnections(Convert.ToInt16(DateTime.Now.Year));

        SqlConnection vCon = new SqlConnection(connection.Create());
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandType = System.Data.CommandType.Text;
        vCmd.CommandText = "select * from gospodarii where gospodarieId='" + pGospodarieId.ToString() + "'";
        vCon.Open();
        SqlDataReader vTabel = vCmd.ExecuteReader();
        // daca e deblocare 
        string vDateGospodarie = "";
        if (pOperatie.IndexOf("|DEBLOCARE|") == -1)
            vDateGospodarie = vUtilizatorIp + " | ";
        else
            vDateGospodarie = "";
        if (vTabel.Read())
            vDateGospodarie += "Unit." + vTabel["unitateId"].ToString() + "|vol." + vTabel["volum"].ToString() + "|poz." + vTabel["nrPozitie"].ToString() + ": ";
        vTabel.Close();
        vCmd.CommandText = "INSERT INTO LogOperatii(unitateId,utilizatorId, data, ora, min, tabela, operatia, valoareVeche, operatieId, utilizatorIP) VALUES ('" + pUnitate + "','" + pUtilizator + "',convert(datetime,'" + pData.Date + "',104),'" + vOra + "','" + vMin + "',N'" + pTabela + "',N'" + vDateGospodarie + pOperatie + "',N'" + pValoareVeche + "', '" + pOperatieId.ToString() + "', '" + pUtilizatorIp + "')";
        vCmd.CommandText = vCmd.CommandText.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
        vCmd.ExecuteNonQuery();
        vCon.Close();
    }
    public static void UpdateDataUltimeiModificari(Int64 pUnitate, Int64 pGospodarie, SqlCommand pCmd)
    {
        pCmd.CommandText = "update gospodarii set dataModificare=convert(datetime,'" + DateTime.Now.Date + "',104), oraModificare='" + DateTime.Now.Hour + "', minutModificare='" + DateTime.Now.Minute + "' WHERE gospodarieId = '" + pGospodarie.ToString() + "'";
        pCmd.CommandText = pCmd.CommandText.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
        pCmd.ExecuteNonQuery();
    }


    public static void UpdateDataUltimeiModificari(Int64 pUnitate, Int64 pGospodarie)
    {
        DBConnections connection = new DBConnections(Convert.ToInt16(DateTime.Now.Year));

        SqlConnection vCon = new SqlConnection(connection.Create());
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandType = System.Data.CommandType.Text;
        vCmd.CommandText = "update gospodarii set dataModificare=convert(datetime,'" + DateTime.Now.Date + "',104), oraModificare='" + DateTime.Now.Hour + "', minutModificare='" + DateTime.Now.Minute + "' WHERE gospodarieId = '" + pGospodarie.ToString() + "'";
        vCmd.CommandText = vCmd.CommandText.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
        vCon.Open();
        vCmd.ExecuteNonQuery();
        vCon.Close();
    }
}
