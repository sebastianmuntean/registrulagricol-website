﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="raportStatisticUtilizatori.aspx.cs" Inherits="raportStatisticUtilizatori" Culture="ro-RO" UICulture="ro-RO" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .paddingLeft
        {
            padding-left: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Rapoarte / Raport statistic utilizatori" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="upUpdatePanel" runat="server">
        <ContentTemplate>
        <asp:Panel ID="pnAfiseazaMesaje" runat="server">
                <asp:ValidationSummary ID="valCustomSummary" runat="server" DisplayMode="SingleParagraph"
                    Visible="true" CssClass="validator" ForeColor="" />
            </asp:Panel>
            <asp:Panel ID="Panel5" CssClass="ascunsa" runat="server">
                <!-- //////////// VALIDATOARE  \\\\\\\\\\\\\\\\\\ -->
                <asp:CustomValidator ID="valCustom" runat="server" ErrorMessage=""></asp:CustomValidator>
            </asp:Panel>
            <asp:Panel ID="pnlGeneral" runat="server" CssClass="panel_general">
                <asp:Panel ID="pnTitlu" CssClass="adauga" runat="server">
                    <asp:Panel ID="pnlCentralizatoare" runat="server" CssClass="centralizatoare">
                        <asp:Label ID="lblFJudet" runat="server" Text="Judeţ"></asp:Label>
                        <asp:DropDownList ID="ddlFJudet" AutoPostBack="true" runat="server" OnPreRender="ddlFJudet_PreRender"
                            OnSelectedIndexChanged="ddlFJudet_SelectedIndexChanged">
                        </asp:DropDownList>
                        <asp:Label ID="lblUnitate" CssClass="paddingLeft" runat="server" Text="Alege unitatea"></asp:Label>
                        <asp:DropDownList ID="ddlUnitate" runat="server" OnInit="ddlUnitate_Init" AutoPostBack="true"
                            OnSelectedIndexChanged="ddlUnitate_SelectedIndexChanged">
                        </asp:DropDownList>
                        <asp:Label ID="lblDataDin" CssClass="paddingLeft" runat="server" Text="Din data"></asp:Label>
                        <asp:TextBox ID="tbDataDin" Width="100px" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="tbDataDin_CalendarExtender" runat="server" 
                            Enabled="True" Format="dd.MM.yyyy" TargetControlID="tbDataDin">
                        </asp:CalendarExtender>
                        <asp:Label ID="lblDataLa" CssClass="paddingLeft" runat="server" Text="La data"></asp:Label>
                        <asp:TextBox ID="tbDataLa" Width="100px" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="tbDataLa_CalendarExtender" runat="server" 
                            Enabled="True" Format="dd.MM.yyyy" TargetControlID="tbDataLa">
                        </asp:CalendarExtender>
                    </asp:Panel>
                    <asp:Panel ID="pnUtilizatori" CssClass="lista_categorii" ScrollBars="Vertical" Height="200px"
                        Width="900px" runat="server">
                        <h1>
                            <asp:Label ID="lblUtilizatori" runat="server" Text="Alegeţi utilizatorii"></asp:Label>
                        </h1>
                        <p style="width: 100%; border-bottom: solid 1px #5A540B; padding-bottom: 2px;">
                            <asp:LinkButton ID="lbSelectatiTot" runat="server" OnClick="lbSelectatiTot_Click">Selectați tot</asp:LinkButton>
                            <asp:Label ID="lblCautaDupaUtilizatori" runat="server" Text="Caută după:"></asp:Label>
                            <asp:Label ID="lblFUUtilizatori" runat="server" Text="Nume utilizator"></asp:Label>
                            <asp:TextBox ID="tbFUUtilizator" Width="300px" AutoPostBack="true" runat="server"></asp:TextBox>
                        </p>
                        <asp:CheckBoxList ID="cblUtilizatori" runat="server" DataSourceID="SqlUtilizatori"
                            DataTextField="denumire" DataValueField="utilizatorId" RepeatColumns="3" RepeatDirection="Horizontal">
                        </asp:CheckBoxList>
                        <asp:SqlDataSource ID="SqlUtilizatori" runat="server" 
                            
                            SelectCommand="SELECT unitati.unitateDenumire + ' - ' + utilizatori.utilizatorNume + ' ' + utilizatori.utilizatorPrenume AS denumire, utilizatori.utilizatorId FROM unitati INNER JOIN utilizatori ON unitati.unitateId = utilizatori.unitateId WHERE (unitati.unitateDenumire + ' - ' + utilizatori.utilizatorNume + ' ' + utilizatori.utilizatorPrenume LIKE '%' + @nume + '%') AND (CONVERT (nvarchar, unitati.judetId) LIKE @judetId) AND (CONVERT (nvarchar, unitati.unitateId) LIKE @unitateId) " OnSelecting="SqlUtilizatori_Selecting">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="tbFUUtilizator" DefaultValue="%" Name="nume" PropertyName="Text" />
                                <asp:ControlParameter ControlID="ddlFJudet" DefaultValue="" Name="judetId" PropertyName="SelectedValue" />
                                <asp:ControlParameter ControlID="ddlUnitate" Name="unitateId" PropertyName="SelectedValue" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </asp:Panel>
                    <br />
                    <asp:Panel ID="pnButoane" runat="server" CssClass="butoane">
                        <asp:Button ID="btTiparire" CssClass="buton" runat="server" Text="Tipărire" 
                            onclick="btTiparire_Click" />
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
