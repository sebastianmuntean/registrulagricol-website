﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="transferParcela.aspx.cs" Inherits="transferParcela" EnableEventValidation="false" Culture="ro-RO" uiCulture="ro-RO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Transfer parcelă la gospodăria" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="upPostBack" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnListaGospodarii" runat="server" CssClass="panel_general" Visible="true">
                <asp:Panel ID="Panel4" runat="server">
                    <asp:Label ID="lblEroare1" runat="server" CssClass="validator" Text="Eroare" Visible="false" />
                </asp:Panel>
                <asp:Panel ID="pnCautare" runat="server" CssClass="cauta">
                    <asp:Label ID="lbCautaDupa" runat="server" Text="Caută după:"></asp:Label>
                    <asp:Label ID="lbfVolum" runat="server" Text="Volum"></asp:Label>
                    <asp:TextBox ID="tbfVolum" Width="20px" runat="server" AutoPostBack="True"></asp:TextBox>
                    <asp:Label ID="lbfNrPoz" runat="server" Text="Nr. poziţie"></asp:Label>
                    <asp:TextBox ID="tbfNrPoz" Width="20px" runat="server" AutoPostBack="True"></asp:TextBox>
                    <asp:Label ID="ldfNume" runat="server" Text="Nume"></asp:Label>
                    <asp:TextBox ID="tbfNume" Width="80px" runat="server" AutoPostBack="True"></asp:TextBox>
                    <asp:Label ID="lbfLoc" runat="server" Text="Localitate"></asp:Label>
                    <asp:TextBox ID="tbfLoc" Width="80px" runat="server" AutoPostBack="True"></asp:TextBox>
                    <asp:Label ID="lbfStrada" runat="server" Text="Stradă"></asp:Label>
                    <asp:TextBox ID="tbfStrada" Width="80px" runat="server" AutoPostBack="True"></asp:TextBox>
                    <asp:Label ID="lbfNr" runat="server" Text="Nr."></asp:Label>
                    <asp:TextBox ID="tbfNr" Width="20px" runat="server" AutoPostBack="True"></asp:TextBox>
                </asp:Panel>
                <asp:Panel ID="pnGrid" runat="server" Visible="true">
                    <asp:GridView ID="gvGospodarii" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        BorderStyle="None" CellPadding="3" GridLines="Vertical" ShowFooter="True" CssClass="tabela"
                        DataKeyNames="gospodarieId" DataSourceID="SqlSabloaneCapitole" EmptyDataText="Nu sunt adaugate inregistrari"
                        AllowPaging="True" OnRowDataBound="gvGospodarii_RowDataBound">
                        <Columns>
                            <asp:BoundField DataField="volum" HeaderText="Volum" SortExpression="volum" />
                            <asp:BoundField DataField="nrPozitie" HeaderText="Număr poziţie" SortExpression="nrPozitie" />
                            <asp:BoundField DataField="nume" HeaderText="Nume" SortExpression="nume" />
                            <asp:BoundField DataField="codSiruta" HeaderText="Cod şiruta" SortExpression="codSiruta" />
                            <asp:BoundField DataField="tip" HeaderText="Tip" SortExpression="tip" />
                            <asp:BoundField DataField="strada" HeaderText="Strada" SortExpression="strada" />
                            <asp:BoundField DataField="nr" HeaderText="Număr" SortExpression="nr" />
                            <asp:BoundField DataField="nrInt" HeaderText="Număr întreg" SortExpression="nrInt" />
                            <asp:BoundField DataField="codExploatatie" HeaderText="Cod exploataţie" SortExpression="codExploatatie" />
                            <asp:BoundField DataField="codUnic" HeaderText="Cod unic" SortExpression="codUnic" />
                            <asp:BoundField DataField="judet" HeaderText="Judeţ" SortExpression="judet" />
                            <asp:BoundField DataField="localitate" HeaderText="Localitate" SortExpression="localitate" />
                        </Columns>
                        <SelectedRowStyle BackColor="#5a540b" ForeColor="#fffdde" Font-Bold="true" />
                        <HeaderStyle Font-Bold="True" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlSabloaneCapitole" runat="server" 
                        SelectCommand="SELECT gospodarii.gospodarieId, gospodarii.unitateId, gospodarii.volum, gospodarii.nrPozitie, gospodarii.codSiruta, CASE gospodarii.tip WHEN '1' THEN 'Localnic' WHEN '2' THEN 'Străinaş' WHEN '3' THEN 'Firmă pe raza localităţii' ELSE 'Firmă străinaşă' END AS tip, gospodarii.strada, gospodarii.nr, gospodarii.nrInt, gospodarii.bl, gospodarii.sc, gospodarii.et, gospodarii.ap, gospodarii.codExploatatie, gospodarii.codUnic, gospodarii.judet, gospodarii.localitate, gospodarii.persJuridica, gospodarii.jUnitate, gospodarii.jSubunitate, gospodarii.jCodFiscal, gospodarii.jNumeReprez, gospodarii.strainas, gospodarii.sStrada, gospodarii.sNr, gospodarii.sBl, gospodarii.sSc, gospodarii.sEtj, gospodarii.sAp, gospodarii.sJudet, gospodarii.sLocalitate, CASE persjuridica WHEN 0 THEN nume ELSE junitate END AS nume FROM gospodarii LEFT OUTER JOIN membri ON gospodarii.gospodarieId = membri.gospodarieId AND membri.codRudenie = 1 WHERE (COALESCE (gospodarii.localitate, N'') LIKE '%' + @localitate + '%') AND (COALESCE (gospodarii.strada, N'') LIKE '%' + @strada + '%') AND (COALESCE (gospodarii.nr, N'') LIKE @nr) AND (COALESCE (gospodarii.volum, N'') LIKE @volum) AND (COALESCE (gospodarii.nrPozitie, N'') LIKE @nrPozitie) AND (CONVERT (nvarchar, gospodarii.unitateId) LIKE @unitateId) AND (COALESCE (CASE persjuridica WHEN 0 THEN nume ELSE junitate END, N'') LIKE '%' + @nume + '%') AND (gospodarii.an = @an)">
                        <SelectParameters>
                            <asp:SessionParameter Name="unitateId" SessionField="SESunitateId" />
                            <asp:ControlParameter ControlID="tbfVolum" DefaultValue="%" Name="volum" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfNrPoz" DefaultValue="%" Name="nrPozitie" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfLoc" DefaultValue="%" Name="localitate" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfStrada" DefaultValue="%" Name="strada" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfNr" DefaultValue="%" Name="nr" PropertyName="Text" />
                            <asp:ControlParameter ControlID="tbfNume" DefaultValue="%" Name="nume" PropertyName="Text" />
                            <asp:SessionParameter Name="an" SessionField="SESan" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="pnAdaugaSabloneCapitole" CssClass="panel_general" runat="server" Visible="true">
                <asp:Panel ID="Panel1" CssClass="adauga" runat="server">
                    <asp:Panel ID="pnEroare" runat="server">
                        <h2>
                            DETALII TRANSFER PARCELĂ</h2>
                    </asp:Panel>
                    <asp:Panel ID="Panel3" runat="server">
                        <p>
                            <asp:Label ID="lblTipTransfer" runat="server" Text="Tipul transferului"></asp:Label>
                            <asp:DropDownList ID="ddlTipTransfer" runat="server" 
                                DataSourceID="SqlTipTransfer" DataTextField="denumire" 
                                DataValueField="tipTransferId">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="SqlTipTransfer" runat="server" 
                                
                                SelectCommand="SELECT [tipTransferId], [denumire] FROM [tipTransferParcela]">
                            </asp:SqlDataSource>
                        </p>
                        <p>
                            <asp:Label ID="lblData" runat="server" Text="Data transfer"></asp:Label>
                            <asp:TextBox ID="tbData" runat="server" oninit="tbData_Init"></asp:TextBox>
                        </p>
                        <p>
                            <asp:Label ID="lblDocument" runat="server" Text="Document care să ateste transferul"></asp:Label>
                            <asp:TextBox ID="tbDocument" Width="500px" runat="server"></asp:TextBox>
                        </p>
                        <p>
                            <asp:Label ID="lblObservatii" runat="server" Text="Observaţii"></asp:Label>
                            <asp:TextBox ID="tbObservatii" TextMode="MultiLine" Rows="5" Width="500px" runat="server"></asp:TextBox>
                        </p>
                    </asp:Panel>
                    <asp:Panel ID="pnButoaneAdaugare" runat="server" CssClass="butoane">
                        <asp:Button ID="btSalveazaSablon" runat="server" CssClass="buton" 
                            Text="transferă parcelă" 
                            
                            onclientclick="return confirm (&quot;Atenţie !!! Vor fi transferate doar parcelele! Celelalte capitole se modifică manual ! Sigur transferaţi parcela la gospodăria aleasă ?&quot;)" 
                            onclick="btSalveazaSablon_Click" />
                        <asp:Button ID="btAnuleazaSalvarea" runat="server" CssClass="buton" 
                            Text="listă parcele" onclick="btAnuleazaSalvarea_Click" />
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
