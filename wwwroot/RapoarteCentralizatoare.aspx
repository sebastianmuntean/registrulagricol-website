﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="RapoarteCentralizatoare.aspx.cs" Inherits="RapoarteCentralizatoare" Culture="ro-RO" UICulture="ro-RO"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="url" runat="Server">
    <asp:Label ID="url" runat="server" Text="Rapoarte / Rapoarte centralizatoare" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="upUpdatePanel" runat="server">
    <ContentTemplate>
    <asp:Panel ID="pnlGeneral" runat="server" CssClass="panel_general">
        <asp:Panel ID="pnTitlu" CssClass="adauga" runat="server">
            <asp:Panel ID="pnlCentralizatoare" runat="server" CssClass="centralizatoare">
                <asp:Label ID="lblFJudet" runat="server" Text="Judeţ"></asp:Label>
                    <asp:DropDownList ID="ddlFJudet" AutoPostBack="true" runat="server" OnPreRender="ddlFJudet_PreRender"
                        OnSelectedIndexChanged="ddlFJudet_SelectedIndexChanged">
                    </asp:DropDownList>
                <asp:Label ID="lblUnitate" runat="server" Text="Alege unitatea"></asp:Label>
                <asp:DropDownList ID="ddlUnitate" runat="server"
                    oninit="ddlUnitate_Init" AutoPostBack="true" 
                    onselectedindexchanged="ddlUnitate_SelectedIndexChanged">
                </asp:DropDownList>
                <p style="height:20px;"></p>
                <asp:LinkButton ID="lbCapitol1" runat="server" OnClick="lbCapitol1_Click"><strong>CAPITOLUL I:</strong> Numărul poziţiilor înscrise în registrul agricol</asp:LinkButton>
                <asp:LinkButton ID="lbCapitol1Volum" runat="server" OnClick="lbCapitol1Volum_Click"><strong>CAPITOLUL I:</strong> Numărul poziţiilor înscrise în registrul agricol - Grupat pe volum</asp:LinkButton>
                <asp:LinkButton ID="lbCap1Persoane" runat="server" OnClick="lbCap1Persoane_Click"><strong>CAPITOLUL I:</strong> Numărul persoanelor înscrise în registrul agricol</asp:LinkButton>
                <asp:LinkButton ID="lbCapitol2a" runat="server" OnClick="lbCapitol2a_Click"><strong>CAPITOLUL II: a)</strong> Terenuri aflate în proprietate situate pe raza localităţii</asp:LinkButton>
                <asp:LinkButton ID="lbCapitol2b" runat="server" OnClick="lbCapitol2b_Click"><strong>CAPITOLUL II: b)</strong> Identificarea pe parcele a terenurilor aflate în proprietatea gospodăriei / exploataţiei agricole fără personalitate juridică şi a unităţilor cu personalitate</asp:LinkButton>
                <asp:LinkButton ID="lbCapitol2c" runat="server" OnClick="lbCapitol2c_Click"><strong>CAPITOLUL II: c)</strong> Identificarea pădurilor proprietate privată în raport cu grupa funcţională şi vârsta</asp:LinkButton>
                <asp:LinkButton ID="lbCapitol3" runat="server" OnClick="lbCapitol3_Click"><strong>CAPITOLUL III:</strong> Modul de utilizare a suprafeţelor agricole situate pe raza localităţii</asp:LinkButton>
                <asp:LinkButton ID="lbCapitol4" runat="server" OnClick="lbCapitol4_Click"><strong>CAPITOLUL IV: a)</strong> Suprafaţa arabilă situată pe raza localităţii</asp:LinkButton>
                <asp:LinkButton ID="lbCapitol4a1" runat="server" OnClick="lbCapitol4a1_Click"><strong>CAPITOLUL IV: a1)</strong> Culturi succesive în câmp, culturi intercalate, culturi modificate genetic pe raza localității</asp:LinkButton>
                <asp:LinkButton ID="lbCapitol4b1" runat="server" OnClick="lbCapitol4b1_Click"><strong>CAPITOLUL IV: b1)</strong> Suprafața cultivată în sere şi alte spații protejate pe raza localității</asp:LinkButton>
                 <asp:LinkButton ID="lbCapitol4b2" runat="server" OnClick="lbCapitol4b2_Click"><strong>CAPITOLUL IV: b2)</strong> Suprafața cultivată solarii şi alte spații protejate pe raza localității</asp:LinkButton>
                <asp:LinkButton ID="lbCapitol4c" runat="server" OnClick="lbCapitol4c_Click"><strong>CAPITOLUL IV: c)</strong> Suprafaţa cultivată cu legume si cartofi în grădinile familiale pe raza localităţii</asp:LinkButton>
                <asp:LinkButton ID="lbCapitol5a" runat="server" OnClick="lbCapitol5a_Click"><strong>CAPITOLUL V:</strong> a) Pomi fructiferi răzleţi pe raza localităţii</asp:LinkButton>
                <asp:LinkButton ID="lbCapitol5b" runat="server" OnClick="lbCapitol5b_Click"><strong>CAPITOLUL V: b)</strong> Suprafaţa plantaţiilor pomicole şi numărul pomilor pe raza localităţii</asp:LinkButton>
                <asp:LinkButton ID="lbCapitol5c" runat="server" OnClick="lbCapitol5c_Click"><strong>CAPITOLUL V: c)</strong> Alte plantaţii de pomi în teren agricol, situate pe raza localităţii</asp:LinkButton>
                <asp:LinkButton ID="lbCapitol5d" runat="server" OnClick="lbCapitol5d_Click"><strong>CAPITOLUL V: d)</strong> Vii, pepiniere şi hameişti situate pe raza localităţii</asp:LinkButton>
                <asp:LinkButton ID="lbCapitol6" runat="server" OnClick="lbCapitol6_Click"><strong>CAPITOLUL VI:</strong> Suprafeţele efective irigate în câmp, situate pe raza localităţii</asp:LinkButton>
                <asp:LinkButton ID="lbCapitol7" runat="server" OnClick="lbCapitol7_Click"><strong>CAPITOLUL VII:</strong> Animale domestice şi/sau  animale sălbatice crescute în captivitate, în condiţiile legii - Situaţia la începutul semestrului</asp:LinkButton>
                <asp:LinkButton ID="lbCapitol7Grupat" runat="server" OnClick="lbCapitol7Grupat_Click"><strong>CAPITOLUL VII:</strong> Animale domestice şi/sau  animale sălbatice crescute în captivitate, în condiţiile legii - Grupat dupa localitate,tip persoana si tip gospodarie</asp:LinkButton>
                <asp:LinkButton ID="lbCapitol8" runat="server" OnClick="lbCapitol8_Click"><strong>CAPITOLUL  VIII:</strong> Evoluţia efectivelor de animale în cursul anului, aflate în proprietatea gospodăriilor/exploataţiilor agricole fără personalitate juridică, cu domiciliul în localitate şi/sau în proprietatea unităţilor cu personalitate juridică, care au activitate pe raza localităţii</asp:LinkButton>
                <asp:LinkButton ID="lbCapitol9" runat="server" OnClick="lbCapitol9_Click"><strong>CAPITOLUL  IX:</strong> Utilaje, instalaţii pentru agricultură şi silvicultură, mijloace de transport cu tracţiune animală şi mecanică existente la începutul anului pe raza localităţii</asp:LinkButton>
                <asp:LinkButton ID="lbCapitol10a" runat="server" OnClick="lbCapitol10a_Click"><strong>CAPITOLUL  X:</strong> a) Aplicarea îngrăşămintelor, amendamentelor şi pesticidelor pe suprafeţe situate pe raza localităţii</asp:LinkButton>
                <asp:LinkButton ID="lbCapitol10b1" runat="server" OnClick="lbCapitol10b1_Click"><strong>CAPITOLUL  X: b)</strong> utilizarea îngrăşămintelor chimice (în echivalent substanţă activă) la principalele culturi (total îngrăşăminte)</asp:LinkButton>
                <asp:LinkButton ID="lbCapitol10b2" runat="server" OnClick="lbCapitol10b2_Click"><strong>CAPITOLUL  X: b)</strong> utilizarea îngrăşămintelor chimice (în echivalent substanţă activă) la principalele culturi (îngrăşăminte azotoase)</asp:LinkButton>
                <asp:LinkButton ID="lbCapitol10b3" runat="server" OnClick="lbCapitol10b3_Click"><strong>CAPITOLUL  X: b)</strong> utilizarea îngrăşămintelor chimice (în echivalent substanţă activă) la principalele culturi (îngrăşăminte potasice)</asp:LinkButton>
                <asp:LinkButton ID="lbCapitol10b4" runat="server" OnClick="lbCapitol10b4_Click"><strong>CAPITOLUL  X: b)</strong> utilizarea îngrăşămintelor chimice (în echivalent substanţă activă) la principalele culturi (îngrăşăminte fosfatice)</asp:LinkButton>
                <asp:LinkButton ID="lbCapitol11" runat="server" OnClick="lbCapitol11_Click"><strong>CAPITOLUL  XI:</strong> Construcţii existente la începutul anului pe raza localităţii</asp:LinkButton>
                <asp:LinkButton ID="lbCapitol12a" runat="server" OnClick="lbCapitol12a_Click"><strong>CAPITOLUL  XII: a)</strong> Producţia vegetală obţinută de gospodăriile/exploataţiile agricole fără personalitate juridică cu domiciliul/sediul în localitate şi/sau unităţile cu personalitate juridică care au activitate pe raza localităţii</asp:LinkButton>
                 <asp:LinkButton ID="lbCapitol12a1" runat="server" OnClick="lbCapitol12a1_Click"><strong>CAPITOLUL  XII: a1)</strong> Producţia vegetală obţinută -Culturi succesive în câmp, culturi intercalate, culturi modificate genetic pe raza localităţii</asp:LinkButton>
                <asp:LinkButton ID="lbCapitol12b1" runat="server" OnClick="lbCapitol12b1_Click"><strong>CAPITOLUL  XII: b1)</strong> Productia vegetala obtinuta în sere pe raza localitatii</asp:LinkButton>
                <asp:LinkButton ID="lbCapitol12b2" runat="server" OnClick="lbCapitol12b2_Click"><strong>CAPITOLUL  XII: b2)</strong> Productia vegetala obtinuta în solarii si alte spatii
protejate pe raza localitatii</asp:LinkButton>
                <asp:LinkButton ID="lbCapitolc12c" runat="server" OnClick="lbCapitolc12c_Click"><strong>CAPITOLUL  XII: c)</strong> Productia vegetala obtinuta pe suprafetele cultivate cu
legume si cartofi în gradinile familiale pe raza localitatii</asp:LinkButton>
                <asp:LinkButton ID="lbCapitol12b" runat="server" OnClick="lbCapitol12b_Click"><strong>CAPITOLUL  XII: d)</strong> Producţia de fructe obţinută de gospodăriile/exploataţiile agricole fără personalitate juridică cu domiciliul/sediul în localitate şi/sau unităţile cu personalitate juridică care au activitate pe raza localităţii</asp:LinkButton>
                <asp:LinkButton ID="lbCapitolc12e" runat="server" OnClick="lbCapitolc12e_Click"><strong>CAPITOLUL  XII: e)</strong> Productia obtinuta din alte plantatii pomicole aflate în
teren agricol, pe raza localitatii</asp:LinkButton>
                 <asp:LinkButton ID="lbCapitol12c" runat="server" OnClick="lbCapitol12c_Click"><strong>CAPITOLUL  XII: f)</strong> Producţia de struguri obţinută de gospodăriile/exploataţiile agricole fără personalitate juridică cu domiciliul/sediul în localitate şi/sau unităţile cu personalitate juridică care au activitate pe raza localităţii</asp:LinkButton>
                <asp:LinkButton ID="lbCapitol13" runat="server" OnClick="lbCapitol13_Click"><strong>CAPITOLUL  XIII:</strong> Producţia animală obţinută de gospodăriile/exploataţiile agricole fără personalitate juridică cu domiciliul/sediul în localitate şi/sau unităţile cu personalitate juridică care au activitate pe raza localităţii</asp:LinkButton>
                <asp:LinkButton ID="lbCapitol14" runat="server" OnClick="lbCapitol14_Click"><strong>CAPITOLUL  XIV:</strong> Înregistrări privind exercitarea dreptului de preemțiune</asp:LinkButton>
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
    </ContentTemplate></asp:UpdatePanel>
</asp:Content>
