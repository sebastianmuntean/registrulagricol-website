﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

/// <summary>
/// Adaugare adeverinte
/// Creata la:                  23.02.2011
/// Autor:                      Laza Tudor Mihai
/// Ultima                      actualizare: 01.04.2011
/// Autor:                      SM, modificare log
/// </summary> 
public partial class adevAdaugaAdeverinte : System.Web.UI.Page
{
    string volum = "";
    string pozitie = "";
    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["SESgospodarieId"] == null)
        {
            HttpCookie vCookie = Request.Cookies["COOKTNT"];
            vCookie = CriptareCookie.DecodeCookie(vCookie);
            if (vCookie != null)
            {
                if (vCookie["COOKgospodarieId"] != null)
                {
                    Session["SESgospodarieId"] = vCookie["COOKgospodarieId"].ToString();
                }
                else if (Request.Url.AbsolutePath.IndexOf("Gospodarii.aspx") == -1 && Session["SESgospodarieId"] == null)
                    Response.Redirect("~/Gospodarii.aspx");
            }
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
         DBConnections connection = new DBConnections(Convert.ToInt16(Session["SESan"]));

        sdsAdeverinte.ConnectionString = connection.Create();
        sdsMembri.ConnectionString = connection.Create();

        //sdsMembriDataNasteriiCampuri.ConnectionString = connection.Create();
        //sdsMembriCampuri.ConnectionString = connection.Create();
        //sdsCampuriNecompletate.ConnectionString = connection.Create();
        
        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "adaugaAdeverinte", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
            string vTip = "1", vValoare = "";
            List<string> vCampuri = new List<string> { "tipUtilizatorId", "unitateId" };
            List<string> vRezultate = new List<string> { };
            vRezultate.Clear();
            vRezultate = ManipuleazaBD.fRezultaUnString("SELECT TOP(1) tipUtilizatorId, unitateId FROM utilizatori WHERE utilizatorId ='" + Session["SESutilizatorId"].ToString() + "'", vCampuri, Convert.ToInt16(Session["SESan"]));
            if (vRezultate[0] != "1")
            {
                vTip = vRezultate[0];
                vValoare = vRezultate[1];
            }
            AutoCompleteExtenderTipSablon.ContextKey = "adeverinteTip#" + vTip + "#" + vValoare;
            adeverintaAnTextBox.Text = Session["SESan"].ToString();
        }      
         volum = clsAdeverinte.GetVolum(Convert.ToInt32(Session["SESan"]), Convert.ToInt32(Session["SESgospodarieID"]));
         pozitie = clsAdeverinte.GetPozitie(Convert.ToInt32(Session["SESan"]), Convert.ToInt32(Session["SESgospodarieID"]));
        
    }

    protected void gvAdeverinte_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (gvAdeverinte.SelectedValue != null)
        {
            btModifica.Visible = true;
            btSterge.Visible = true;
            btTiparire.Visible = true;

        }
        else
        {
            btModifica.Visible = false;
            btSterge.Visible = false;
            btTiparire.Visible = false;
        }
        
    }
    protected void btAdauga_Click(object sender, EventArgs e)
    {
        ViewState["tip"] = "a";
        pnGrid.Visible = false;
        pnButoanePrincipale.Visible = false;
        pnAdaugaAdeverinta.Visible = true;

        GolireCampuriAdaugare();
    }

    private void GolireCampuriAdaugare()
    {
        btSalveaza.Text = "adaugă o adeverinţă";
        //  tbContinut.Text = "Se emite prezenta pentru a servi la ";
        tbData.Text = DateTime.Now.Date.ToString("dd.MM.yyyy");
        tbNumarChitanta.Text = "";
        tbNumarIesire.Text = "";
        tbNumarInreg.Text = "";
        lblAntet.Text = "";
        lblContinut.Text = "";
        lblSubsol.Text = "";
        tbTipSablon.Text = "";
        tbTipSablonId.Text = "0";
        pnlRest.Visible = false;
        btSalveaza.Visible = false;
        btEditareAvansata.Visible = false;
        //Antet_OnLoad();
    }
    protected void btModifica_Click(object sender, EventArgs e)
    {
        if (gvAdeverinte.SelectedRow.Cells[4].Text == DateTime.Now.Date.ToString("dd.MM.yyyy"))
        {
            ViewState["tip"] = "m";
            pnGrid.Visible = false;
            pnButoanePrincipale.Visible = false;
            pnAdaugaAdeverinta.Visible = true;
            PopuleazaDateModificare();
            ddlTipSablon_SelectedIndexChanged(sender, e);

        }
        else
        {
            valCustom.IsValid = false;
            valCustom.ErrorMessage = "Puteți șterge sau modifica doar o adeverință completată în ziua curentă!";
        }
    }

    private void PopuleazaDateModificare()
    {
        btSalveaza.Text = "modifică adeverinţă";
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "SELECT adeverinte.adeverintaId, adeverinte.unitateId, adeverinte.gospodarieId, adeverinte.utilizatorId, adeverinte.numeMembru, adeverinte.numarChitanta, adeverinte.numarInregistrare, adeverinte.numarIesire, adeverinte.data, adeverinte.antet, adeverinte.text, adeverinte.textTiparit, adeverinte.motiv, adeverinte.subsol, adeverinte.ora, adeverinte.minut, adeverinte.tipSablon, membri.capitolId, membri.unitateId AS Expr1, membri.gospodarieId AS Expr2, membri.an, membri.nume, membri.codRand, membri.cnp, membri.codSex, membri.codRudenie, membri.denumireRudenie, membri.dataNasterii, membri.mentiuni, adeverinteSabloane.adevSablonDenumire FROM adeverinte INNER JOIN membri ON adeverinte.numeMembru = membri.nume INNER JOIN adeverinteSabloane ON adeverinte.tipSablon = adeverinteSabloane.adevSablonId WHERE adeverintaId='" + gvAdeverinte.SelectedValue.ToString() + "'";
        SqlDataReader vTabel = vCmd.ExecuteReader();
        if (vTabel.Read())
        {
            try
            {
                ddlMembru.DataBind();
            }
            catch
            {
                ListItem vMembru = new ListItem { };
                vMembru.Value = "0";
                vMembru.Text = "-";
                ddlMembru.Items.Add(vMembru);
                ddlMembru.SelectedValue = "0";
            }
            Antet_OnLoad();
            tbTipSablonId.Text = vTabel["tipSablon"].ToString();
            tbTipSablon.Text = vTabel["adevSablonDenumire"].ToString();
            try
            {
                ddlMembru.SelectedValue = vTabel["capitolId"].ToString();
            }
            catch { }
            tbNumarChitanta.Text = vTabel["numarChitanta"].ToString();
            tbNumarInreg.Text = vTabel["numarInregistrare"].ToString();
            tbNumarIesire.Text = vTabel["numarIesire"].ToString();
            tbData.Text = Convert.ToDateTime(vTabel["data"].ToString()).ToString("dd.MM.yyyy");
            lblAntet.Text = vTabel["antet"].ToString();
            lblContinut.Text = vTabel["text"].ToString();
            // tbContinut.Text = vTabel["motiv"].ToString();
            lblSubsol.Text = vTabel["subsol"].ToString();
        }
        vTabel.Close();
        ManipuleazaBD.InchideConexiune(vCon);
    }
    protected void btSterge_Click(object sender, EventArgs e)
    {
        if (gvAdeverinte.SelectedRow.Cells[4].Text == DateTime.Now.Date.ToString("dd.MM.yyyy"))
        {
            SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
            SqlTransaction vTranz = ManipuleazaBD.DeschideTranzactie(vCon);
            SqlCommand vCmd = new SqlCommand();
            vCmd.Connection = vCon;
            vCmd.Transaction = vTranz;
            try
            {
                vCmd.CommandText = "delete from adeverinte where adeverintaId='" + gvAdeverinte.SelectedValue.ToString() + "'";
                vCmd.ExecuteNonQuery();
                ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "adeverinte", "stergere adeverinta, id: " + gvAdeverinte.SelectedValue.ToString(), "", vCmd, Convert.ToInt64(Session["SESgospodarieId"]), 4);
                vTranz.Commit();
                gvAdeverinte.DataBind();
            }
            catch { vTranz.Rollback(); }
            finally { ManipuleazaBD.InchideConexiune(vCon); }
        }
        else
        {
            valCustom.IsValid = false;
            valCustom.ErrorMessage = "Puteți șterge sau modifica doar o adeverință completată în ziua curentă!";
        }
    }
    protected void btTiparire_Click(object sender, EventArgs e)
    {
        // redirect spre pagina care interpreteaza adeverinta
        //Response.Redirect("~/adevTiparesteAdeverinta.aspx?id=" + gvAdeverinte.SelectedValue.ToString());
        ResponseHelper.Redirect("~/adevTiparesteAdeverinta.aspx?id=" + gvAdeverinte.SelectedValue.ToString(), "_blank", "height=600,width=900,status=no,toolbar=no,menubar=no,location=no, scrollbars=1");
        ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "tipareste", "tiparire adeverinta creata anterior, id:" + gvAdeverinte.SelectedValue.ToString(), "", Convert.ToInt64(Session["SESgospodarieId"]), 5);
    }
    protected void btAnuleazaSalvarea_Click(object sender, EventArgs e)
    {
        pnGrid.Visible = true;
        pnButoanePrincipale.Visible = true;
        pnAdaugaAdeverinta.Visible = false;
    }
    protected void btSalveaza_Click(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlTransaction vTranz = ManipuleazaBD.DeschideTranzactie(vCon);
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.Transaction = vTranz;
        int vAdeverintaId = 0;
        lblContinut.Text = lblContinut.Text.Replace("<span class=\"camp_necunoscut\"", "<span class=\"camp_tiparit\"");
        try
        {
            string vMembruId = "0";
            try { vMembruId = ddlMembru.SelectedItem.ToString(); }
            catch { }
            if (ViewState["tip"].ToString() == "a")
            {
                vCmd.CommandText = @"INSERT INTO [adeverinte] ([unitateId], [gospodarieId], [utilizatorId], [numeMembru], [numarChitanta], [numarInregistrare], [numarIesire], [data], [antet], [text], [textTiparit], [subsol], [ora], [minut], [tipSablon]) VALUES ('" + Session["SESunitateId"].ToString() + "', '" + Session["SESgospodarieId"].ToString() + "', '" + Session["SESutilizatorId"].ToString() + "', N'" + vMembruId + "', N'" + tbNumarChitanta.Text.Replace('\'', '"') + "', N'" + tbNumarInreg.Text.Replace('\'', '"') + "', N'" + tbNumarIesire.Text.Replace('\'', '"') + "', Convert(datetime,'" + tbData.Text + "',104), N'" + lblAntet.Text.Replace('\'', '"') + "', N'" + lblContinut.Text.Replace('\'', '"') + "', N'<table class=\"adeverinta_antet\"><tr><td>" + lblAntet.Text.Replace('\'', '"') + "</td></tr><tr><td>" + lblContinut.Text.Replace('\'', '"') + "</td></tr><tr><td>" + lblSubsol.Text.Replace('\'', '"') + "</td></tr></table>', N'" + lblSubsol.Text.Replace('\'', '"') + "', '" + DateTime.Now.Hour.ToString() + "', '" + DateTime.Now.Minute.ToString() + "', '" + tbTipSablonId.Text + "');    SELECT CAST(scope_identity() AS int)";
                vCmd.CommandText = vCmd.CommandText.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
                //SELECT SCOPE_IDENTITY() AS [SCOPE_IDENTITY]";
                vAdeverintaId = Convert.ToInt32(vCmd.ExecuteScalar());
                ResponseHelper.Redirect("~/adevTiparesteAdeverinta.aspx?id=" + vAdeverintaId.ToString() + "&p=" + rbPePagina.SelectedValue, "_blank", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no, scrollbars=1");
                ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "adeverinte", "adaugare adeverinta, id:" + vAdeverintaId.ToString(), "", vCmd, Convert.ToInt64(Session["SESgospodarieId"]), 5);
                vTranz.Commit();
            }
            else if (ViewState["tip"].ToString() == "m")
            {
                vCmd.CommandText = @"UPDATE [adeverinte] SET [unitateId] = '" + Session["SESunitateId"].ToString() + "', [gospodarieId] = '" + Session["SESgospodarieId"].ToString() + "', [utilizatorId] = '" + Session["SESutilizatorId"].ToString() + "', [numeMembru] = N'" + vMembruId + "', [numarChitanta] = N'" + tbNumarChitanta.Text.Replace('\'', '"') + "', [numarInregistrare] = N'" + tbNumarInreg.Text.Replace('\'', '"') + "', [numarIesire] = N'" + tbNumarIesire.Text.Replace('\'', '"') + "', [data] = convert(datetime,'" + tbData.Text + "',104), [antet] = N'" + lblAntet.Text.Replace('\'', '"') + "', [text] = N'" + lblContinut.Text.Replace('\'', '"') + "', [textTiparit] = N'<table class=\"adeverinta_antet\"><tr><td>" + lblAntet.Text.Replace('\'', '"') + "</td></tr><tr><td>" + lblContinut.Text.Replace('\'', '"') + "</td></tr><tr><td>" + lblSubsol.Text.Replace('\'', '"') + "</td></tr></table>', [subsol] = N'" + lblSubsol.Text.Replace('\'', '"') + "', [ora] = '" + DateTime.Now.Hour.ToString() + "', [minut] = '" + DateTime.Now.Minute.ToString() + "', [tipSablon] = '" + tbTipSablonId.Text + "' WHERE (([adeverintaId] = '" + gvAdeverinte.SelectedValue.ToString() + "'))";
                vCmd.CommandText = vCmd.CommandText.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
                vCmd.ExecuteNonQuery();
                ResponseHelper.Redirect("~/adevTiparesteAdeverinta.aspx?id=" + gvAdeverinte.SelectedValue.ToString() + "&p=" + rbPePagina.SelectedValue, "_blank", "height=600,width=900,status=no,toolbar=no,menubar=no,location=no, scrollbars=1");
                ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "adeverinte", "modificare adeverinta id:" + gvAdeverinte.SelectedValue.ToString(), "", vCmd, Convert.ToInt64(Session["SESgospodarieId"]), 5);
                vTranz.Commit();
                vAdeverintaId = Convert.ToInt32(gvAdeverinte.SelectedValue);
            }
            pnGrid.Visible = true;
            pnButoanePrincipale.Visible = true;
            pnAdaugaAdeverinta.Visible = false;
            gvAdeverinte.DataBind();
        }
        catch { vTranz.Rollback(); }
        finally { ManipuleazaBD.InchideConexiune(vCon); }
        if (vAdeverintaId != 0)
        {
            // redirect spre pagina care interpreteaza adeverinta
            //Response.Redirect("~/printAdeverinta.aspx?adeverintaId=" + vAdeverintaId.ToString());
        }
    }
    protected void gvAdeverinte_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(gvAdeverinte, e, this);
    }
    protected void Antet_OnLoad()
    {
        if (ViewState["tip"].ToString() == "a")
        {
            try
            {
                ddlMembru.DataBind();
            }
            catch
            {
                ListItem vMembru = new ListItem { };
                vMembru.Value = "0";
                vMembru.Text = "-";
                ddlMembru.Items.Add(vMembru);
                ddlMembru.SelectedValue = "0";
            }
        }
        else
        {
            try { ddlMembru.SelectedValue = ddlMembru.Items[0].Value; }
            catch { }
        }
        //ddlTipSablon.SelectedValue = ddlTipSablon.Items[0].Value;
        tbTipSablonId.Text = "0";
        tbTipSablon.Text = "";
        List<string> vCampuri = new List<string> { "data", "chitanta", "nr-inregistrare", "nr-iesire" };
        List<string> vValori = new List<string> { tbData.Text, tbNumarChitanta.Text, tbNumarInreg.Text, tbNumarIesire.Text };
        lblAntet.Text = clsAdeverinte.FormeazaAntet(Session["SESunitateId"].ToString(), tbTipSablonId.Text, vCampuri, vValori, adeverintaAnTextBox.Text, Session["SESutilizatorId"].ToString());
        lblSubsol.Text = clsAdeverinte.FormeazaSubsol(Session["SESunitateId"].ToString(), tbTipSablonId.Text, adeverintaAnTextBox.Text, Session["SESutilizatorId"].ToString());
        List<ListaSelect> vListaCampuri = new List<ListaSelect>();
        string vContinut = lblContinut.Text;
        clsAdeverinte.RezultaTextAdeverinta(Session["SESunitateId"].ToString(), Session["SESgospodarieId"].ToString(), ddlMembru.SelectedValue, tbTipSablonId.Text, out vListaCampuri, out vContinut, adeverintaAnTextBox.Text, Session["SESutilizatorId"].ToString());
        lblContinut.Text = vContinut;
        sdsCampuriNecompletate.SelectCommand = "select top(" + vListaCampuri.Count + ") [id] from grid";
        gvCampuriNecompletate.DataBind();
        for (int i = 0; i < gvCampuriNecompletate.Rows.Count; i++)
        {
            if (gvCampuriNecompletate.Rows[i].RowType == DataControlRowType.DataRow)
            {
                Label vLblCamp = (Label)gvCampuriNecompletate.Rows[i].FindControl("lblCamp");
                TextBox vTbCamp = (TextBox)gvCampuriNecompletate.Rows[i].FindControl("tbCamp");
                DropDownList vDdlCampMembru = (DropDownList)gvCampuriNecompletate.Rows[i].FindControl("ddlCampMembru");
                DropDownList vDdlCampMembruDataNasterii = (DropDownList)gvCampuriNecompletate.Rows[i].FindControl("ddlCampMembruDataNasterii");
                vLblCamp.Text = vListaCampuri[i].Col1;
                if (vLblCamp.Text == "membru")
                {
                    vTbCamp.Visible = false;
                    vDdlCampMembruDataNasterii.Visible = false;
                    vDdlCampMembru.Visible = true;
                    vListaCampuri[i].Col2 = vDdlCampMembru.SelectedValue;
                    try { vDdlCampMembru.SelectedValue = vListaCampuri[i].Col2; }
                    catch { }
                }
                else if (vLblCamp.Text == "membru-data-nasterii")
                {
                    vTbCamp.Visible = false;
                    vDdlCampMembru.Visible = false;
                    vDdlCampMembruDataNasterii.Visible = true;
                    vListaCampuri[i].Col2 = vDdlCampMembruDataNasterii.SelectedValue;
                    try { vDdlCampMembruDataNasterii.SelectedValue = vListaCampuri[i].Col2; }
                    catch { }
                }
                else
                {
                    vTbCamp.Visible = true;
                    vTbCamp.Text = vListaCampuri[i].Col2;
                }
            }
        }
        lblContinut.Text = clsAdeverinte.RezultaTextAdeverintaTagNecunoscut(lblContinut.Text, vListaCampuri);
    }
    protected void upAdauga_Load(object sender, EventArgs e)
    {
        List<string> vCampuri = new List<string> { "data", "chitanta", "nr-inregistrare", "nr-iesire" };
        List<string> vValori = new List<string> { tbData.Text, tbNumarChitanta.Text, tbNumarInreg.Text, tbNumarIesire.Text };
        lblAntet.Text = clsAdeverinte.FormeazaAntet(Session["SESunitateId"].ToString(), tbTipSablonId.Text, vCampuri, vValori, adeverintaAnTextBox.Text, Session["SESutilizatorId"].ToString());
        lblSubsol.Text = clsAdeverinte.FormeazaSubsol(Session["SESunitateId"].ToString(), tbTipSablonId.Text, adeverintaAnTextBox.Text, Session["SESutilizatorId"].ToString());
        // ddlMembru.DataBind();
        //lblContinut.Text = clsAdeverinte.RezultaTextAdeverinta(Session["SESunitateId"].ToString(), Session["SESgospodarieId"].ToString(), ddlMembru.SelectedValue.ToString(), ddlTipSablon.SelectedValue);
    }
    protected void ddlMembru_SelectedIndexChanged(object sender, EventArgs e)
    {

        List<ListaSelect> vListaCampuri = new List<ListaSelect>();
        string vContinut = lblContinut.Text;
        clsAdeverinte.RezultaTextAdeverinta(Session["SESunitateId"].ToString(), Session["SESgospodarieId"].ToString(), ddlMembru.SelectedValue.ToString(), tbTipSablonId.Text, out vListaCampuri, out vContinut, adeverintaAnTextBox.Text, Session["SESutilizatorId"].ToString());
        lblContinut.Text = vContinut;
        sdsCampuriNecompletate.SelectCommand = "select top(" + vListaCampuri.Count + ") [id] from grid";
        gvCampuriNecompletate.DataBind();
        for (int i = 0; i < gvCampuriNecompletate.Rows.Count; i++)
        {
            if (gvCampuriNecompletate.Rows[i].RowType == DataControlRowType.DataRow)
            {
                Label vLblCamp = (Label)gvCampuriNecompletate.Rows[i].FindControl("lblCamp");
                TextBox vTbCamp = (TextBox)gvCampuriNecompletate.Rows[i].FindControl("tbCamp");
                DropDownList vDdlCampMembru = (DropDownList)gvCampuriNecompletate.Rows[i].FindControl("ddlCampMembru");
                DropDownList vDdlCampMembruDataNasterii = (DropDownList)gvCampuriNecompletate.Rows[i].FindControl("ddlCampMembruDataNasterii");
                vLblCamp.Text = vListaCampuri[i].Col1;
                if (vLblCamp.Text == "membru")
                {
                    vTbCamp.Visible = false;
                    vDdlCampMembruDataNasterii.Visible = false;
                    vDdlCampMembru.Visible = true;
                    vListaCampuri[i].Col2 = vDdlCampMembru.SelectedValue;
                    try { vDdlCampMembru.SelectedValue = vListaCampuri[i].Col2; }
                    catch { }
                }
                else if (vLblCamp.Text == "membru-data-nasterii")
                {
                    vTbCamp.Visible = false;
                    vDdlCampMembru.Visible = false;
                    vDdlCampMembruDataNasterii.Visible = true;
                    vListaCampuri[i].Col2 = vDdlCampMembruDataNasterii.SelectedValue;
                    try { vDdlCampMembruDataNasterii.SelectedValue = vListaCampuri[i].Col2; }
                    catch { }
                }
                else
                {
                    vTbCamp.Visible = true;
                    vTbCamp.Text = vListaCampuri[i].Col2;
                }
            }
        }
        lblContinut.Text = clsAdeverinte.RezultaTextAdeverintaTagNecunoscut(lblContinut.Text, vListaCampuri);
    }
    protected void ddlTipSablon_SelectedIndexChanged(object sender, EventArgs e)
    {
        
    }
    protected void sdsCampuriNecompletate_Init(object sender, EventArgs e)
    {

    }
    protected void tbCamp_TextChanged(object sender, EventArgs e)
    {
        List<ListaSelect> vListaCampuri = new List<ListaSelect>();
        for (int i = 0; i < gvCampuriNecompletate.Rows.Count; i++)
        {
            if (gvCampuriNecompletate.Rows[i].RowType == DataControlRowType.DataRow)
            {
                ListaSelect vListaSelect = new ListaSelect() { };
                Label vLblCamp = (Label)gvCampuriNecompletate.Rows[i].FindControl("lblCamp");
                TextBox vTbCamp = (TextBox)gvCampuriNecompletate.Rows[i].FindControl("tbCamp");
                DropDownList vDdlCampMembru = (DropDownList)gvCampuriNecompletate.Rows[i].FindControl("ddlCampMembru");
                DropDownList vDdlCampMembruDataNasterii = (DropDownList)gvCampuriNecompletate.Rows[i].FindControl("ddlCampMembruDataNasterii");
                vListaSelect.Col1 = vLblCamp.Text;
                if (vTbCamp.Visible == true)
                    vListaSelect.Col2 = vTbCamp.Text;
                else if (vDdlCampMembru.Visible == true)
                    vListaSelect.Col2 = vDdlCampMembru.SelectedValue;
                else if (vDdlCampMembruDataNasterii.Visible == true)
                    vListaSelect.Col2 = vDdlCampMembruDataNasterii.SelectedValue;
                vListaCampuri.Add(vListaSelect);

                // ListaSelect vListaSelect = new ListaSelect() { };
                //  Label vLblCamp = (Label)gvCampuriNecompletate.Rows[i].FindControl("lblCamp");
                //   TextBox vTbCamp = (TextBox)gvCampuriNecompletate.Rows[i].FindControl("tbCamp");
                // DropDownList vDdlCamp = (DropDownList)gvCampuriNecompletate.Rows[i].FindControl("ddlCamp");
                /*    vListaSelect.Col1 = vLblCamp.Text;
                    if (vTbCamp.Visible == true)
                        vListaSelect.Col2 = vTbCamp.Text;
                    else { }
                        //vListaSelect.Col2 = vDdlCamp.SelectedValue;
                    vListaCampuri.Add(vListaSelect);*/
            }
        }
        lblContinut.Text = clsAdeverinte.RezultaTextAdeverintaTagNecunoscut(lblContinut.Text, vListaCampuri);
    }
    protected void gvCampuriNecompletate_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label vLblCamp = (Label)e.Row.FindControl("lblCamp");
            TextBox vTbCamp = (TextBox)e.Row.FindControl("tbCamp");
            DropDownList vDdlCampMembru = (DropDownList)e.Row.FindControl("ddlCampMembru");
            DropDownList vDdlCampMembruDataNasterii = (DropDownList)e.Row.FindControl("ddlCampMembruDataNasterii");
            if (vLblCamp.Text == "membru")
            { vTbCamp.Visible = false; vDdlCampMembru.Visible = true; vDdlCampMembruDataNasterii.Visible = false; }
            else if (vLblCamp.Text == "membru-data-nasterii")
            { vTbCamp.Visible = false; vDdlCampMembru.Visible = false; vDdlCampMembruDataNasterii.Visible = true; }
            else
            { vTbCamp.Visible = true; vDdlCampMembru.Visible = false; vDdlCampMembruDataNasterii.Visible = false; }
        }
    }
    protected void ddlCamp_SelectedIndexChanged(object sender, EventArgs e)
    {
        List<ListaSelect> vListaCampuri = new List<ListaSelect>();
        for (int i = 0; i < gvCampuriNecompletate.Rows.Count; i++)
        {
            if (gvCampuriNecompletate.Rows[i].RowType == DataControlRowType.DataRow)
            {
                ListaSelect vListaSelect = new ListaSelect() { };
                Label vLblCamp = (Label)gvCampuriNecompletate.Rows[i].FindControl("lblCamp");
                TextBox vTbCamp = (TextBox)gvCampuriNecompletate.Rows[i].FindControl("tbCamp");
                DropDownList vDdlCampMembru = (DropDownList)gvCampuriNecompletate.Rows[i].FindControl("ddlCampMembru");
                DropDownList vDdlCampMembruDataNasterii = (DropDownList)gvCampuriNecompletate.Rows[i].FindControl("ddlCampMembruDataNasterii");
                vListaSelect.Col1 = vLblCamp.Text;
                if (vTbCamp.Visible == true)
                    vListaSelect.Col2 = vTbCamp.Text;
                else if (vDdlCampMembru.Visible == true)
                    vListaSelect.Col2 = vDdlCampMembru.SelectedValue;
                else if (vDdlCampMembruDataNasterii.Visible == true)
                    vListaSelect.Col2 = vDdlCampMembruDataNasterii.SelectedValue;
                vListaCampuri.Add(vListaSelect);
            }
        }
        lblContinut.Text = clsAdeverinte.RezultaTextAdeverintaTagNecunoscut(lblContinut.Text, vListaCampuri);
    }
    protected void btEditareAvansata_Click(object sender, EventArgs e)
    {
        // salvam ca si la salvare normala fara sa tiparim
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlTransaction vTranz = ManipuleazaBD.DeschideTranzactie(vCon);
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.Transaction = vTranz;
        int vAdeverintaId = 0;
        lblContinut.Text = lblContinut.Text.Replace("<span class=\"camp_necunoscut\"", "<span class=\"camp_tiparit\"");
        try
        {
            string vMembruId = "0";
            try { vMembruId = ddlMembru.SelectedItem.ToString(); }
            catch { }
            if (ViewState["tip"].ToString() == "a")
            {
                vCmd.CommandText = @"INSERT INTO [adeverinte] ([unitateId], [gospodarieId], [utilizatorId], [numeMembru], [numarChitanta], [numarInregistrare], [numarIesire], [data], [antet], [text], [textTiparit], [subsol], [ora], [minut], [tipSablon]) VALUES ('" + Session["SESunitateId"].ToString() + "', '" + Session["SESgospodarieId"].ToString() + "', '" + Session["SESutilizatorId"].ToString() + "', N'" + vMembruId + "', N'" + tbNumarChitanta.Text.Replace('\'', '"') + "', N'" + tbNumarInreg.Text.Replace('\'', '"') + "', N'" + tbNumarIesire.Text.Replace('\'', '"') + "', Convert(datetime,'" + tbData.Text + "',104), N'" + lblAntet.Text.Replace('\'', '"') + "', N'" + lblContinut.Text.Replace('\'', '"') + "', N'<table class=\"adeverinta_antet\"><tr><td>" + lblAntet.Text.Replace('\'', '"') + "</td></tr><tr><td>" + lblContinut.Text.Replace('\'', '"') + "</td></tr><tr><td>" + lblSubsol.Text.Replace('\'', '"') + "</td></tr></table>', N'" + lblSubsol.Text.Replace('\'', '"') + "', '" + DateTime.Now.Hour.ToString() + "', '" + DateTime.Now.Minute.ToString() + "', '" + tbTipSablonId.Text + "');    SELECT CAST(scope_identity() AS int)";
                vCmd.CommandText = vCmd.CommandText.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
                //SELECT SCOPE_IDENTITY() AS [SCOPE_IDENTITY]";
                vAdeverintaId = Convert.ToInt32(vCmd.ExecuteScalar());
                ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "adeverinte", "adaugare adeverinta, id:" + vAdeverintaId.ToString(), "", vCmd, Convert.ToInt64(Session["SESgospodarieId"]), 2);
                vTranz.Commit();
            }
            else if (ViewState["tip"].ToString() == "m")
            {
                vCmd.CommandText = @"UPDATE [adeverinte] SET [unitateId] = '" + Session["SESunitateId"].ToString() + "', [gospodarieId] = '" + Session["SESgospodarieId"].ToString() + "', [utilizatorId] = '" + Session["SESutilizatorId"].ToString() + "', [numeMembru] = N'" + vMembruId + "', [numarChitanta] = N'" + tbNumarChitanta.Text.Replace('\'', '"') + "', [numarInregistrare] = N'" + tbNumarInreg.Text.Replace('\'', '"') + "', [numarIesire] = N'" + tbNumarIesire.Text.Replace('\'', '"') + "', [data] = convert(datetime,'" + tbData.Text + "',104), [antet] = N'" + lblAntet.Text.Replace('\'', '"') + "', [text] = N'" + lblContinut.Text.Replace('\'', '"') + "', [textTiparit] = N'<table class=\"adeverinta_antet\"><tr><td>" + lblAntet.Text.Replace('\'', '"') + "</td></tr><tr><td>" + lblContinut.Text.Replace('\'', '"') + "</td></tr><tr><td>" + lblSubsol.Text.Replace('\'', '"') + "</td></tr></table>', [subsol] = N'" + lblSubsol.Text.Replace('\'', '"') + "', [ora] = '" + DateTime.Now.Hour.ToString() + "', [minut] = '" + DateTime.Now.Minute.ToString() + "', [tipSablon] = '" + tbTipSablonId.Text + "' WHERE (([adeverintaId] = '" + gvAdeverinte.SelectedValue.ToString() + "'))";
                vCmd.CommandText = vCmd.CommandText.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
                vCmd.ExecuteNonQuery();
                ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "adeverinte", "modificare adeverinta id:" + gvAdeverinte.SelectedValue.ToString(), "", vCmd, Convert.ToInt64(Session["SESgospodarieId"]), 3);
                vTranz.Commit();
                vAdeverintaId = Convert.ToInt32(gvAdeverinte.SelectedValue);
            }
            gvAdeverinte.DataBind();
        }
        catch { vTranz.Rollback(); }
        finally { ManipuleazaBD.InchideConexiune(vCon); }
        //  pagina de editare avansata
        Response.Redirect("~/adevEditareAvansata.aspx?id=" + vAdeverintaId.ToString());

    }
    protected void tbTipSablon_TextChanged(object sender, EventArgs e)
    {
        Session["SESgospodarieId"] = clsAdeverinte.GetGopodarieIdByAn(adeverintaAnTextBox.Text, volum, pozitie);
        if (tbTipSablonId.Text != "0")
        {
            pnlRest.Visible = true;
            btSalveaza.Visible = true;
            btEditareAvansata.Visible = true;
            List<ListaSelect> vListaCampuri = new List<ListaSelect>();
            string vContinut = lblContinut.Text;
            try
            {
                ddlMembru.DataBind();
            }
            catch
            {
                ListItem vMembru = new ListItem { };
                vMembru.Value = "0";
                vMembru.Text = "-";
                ddlMembru.Items.Add(vMembru);
                ddlMembru.SelectedValue = "0";
            }
            clsAdeverinte.RezultaTextAdeverinta(Session["SESunitateId"].ToString(), Session["SESgospodarieId"].ToString(), ddlMembru.SelectedValue.ToString(), tbTipSablonId.Text, out vListaCampuri, out vContinut, adeverintaAnTextBox.Text, Session["SESutilizatorId"].ToString());
            lblContinut.Text = vContinut;
            sdsCampuriNecompletate.SelectCommand = "select top(" + vListaCampuri.Count + ") [id] from grid";
            gvCampuriNecompletate.DataBind();
            for (int i = 0; i < gvCampuriNecompletate.Rows.Count; i++)
            {
                if (gvCampuriNecompletate.Rows[i].RowType == DataControlRowType.DataRow)
                {
                    Label vLblCamp = (Label)gvCampuriNecompletate.Rows[i].FindControl("lblCamp");
                    TextBox vTbCamp = (TextBox)gvCampuriNecompletate.Rows[i].FindControl("tbCamp");
                    DropDownList vDdlCampMembru = (DropDownList)gvCampuriNecompletate.Rows[i].FindControl("ddlCampMembru");
                    DropDownList vDdlCampMembruDataNasterii = (DropDownList)gvCampuriNecompletate.Rows[i].FindControl("ddlCampMembruDataNasterii");
                    vLblCamp.Text = vListaCampuri[i].Col1;
                    if (vLblCamp.Text == "membru")
                    {
                        vTbCamp.Visible = false;
                        vDdlCampMembruDataNasterii.Visible = false;
                        vDdlCampMembru.Visible = true;
                        vListaCampuri[i].Col2 = vDdlCampMembru.SelectedValue;
                        try { vDdlCampMembru.SelectedValue = vListaCampuri[i].Col2; }
                        catch { }
                    }
                    else if (vLblCamp.Text == "membru-data-nasterii")
                    {
                        vTbCamp.Visible = false;
                        vDdlCampMembru.Visible = false;
                        vDdlCampMembruDataNasterii.Visible = true;
                        vListaCampuri[i].Col2 = vDdlCampMembruDataNasterii.SelectedValue;
                        try { vDdlCampMembruDataNasterii.SelectedValue = vListaCampuri[i].Col2; }
                        catch { }
                    }
                    else
                    {
                        vTbCamp.Visible = true;
                        vTbCamp.Text = vListaCampuri[i].Col2;
                    }
                }
            }
            lblContinut.Text = clsAdeverinte.RezultaTextAdeverintaTagNecunoscut(lblContinut.Text, vListaCampuri);
        }
        else
        { GolireCampuriAdaugare(); }
    }

}
