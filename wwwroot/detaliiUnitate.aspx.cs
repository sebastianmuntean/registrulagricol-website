﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Drawing.Imaging;
using System.Drawing;
using System.IO;

/// <summary>
/// Contul meu
/// Creata la:                  07.03.2011
/// Autor:                      Sebastian Muntean
/// Ultima                      actualizare: 10.03.2011
/// Autor:                      Sebastian Muntean
/// </summary> 
public partial class detaliiUnitate : System.Web.UI.Page
{
    #region initializare-limbi
    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if (vCookie1 != null)
        {
            if (vCookie1["COOKlimba"] != null)
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if (Session["SESlimba"] == null)
        {
            Session["SESlimba"] = "ro-RO";
            if (vCookie1 != null)
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if (Session["SESlimba"] != null)
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();

    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        DBConnections connection = new DBConnections(Convert.ToInt16(Session["SESan"]));

        SqlEmbleme.ConnectionString = connection.Create();

        if (!IsPostBack)
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "Detalii unitate", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1);
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.CommandText = "select * from unitati where unitateId='" + Session["SESunitateId"].ToString() + "'";
        SqlDataReader vTabel = vCmd.ExecuteReader();
        if (vTabel.Read())
        {
            tbPrimar.Text = vTabel["unitatePrimar"].ToString();
            tbVicePrimar.Text = vTabel["unitateVicePrimar"].ToString();
            tbSecretar.Text = vTabel["unitateSecretar"].ToString();
            tbContabil.Text = vTabel["unitateContabil"].ToString();
            tbSefServiciuAgricol.Text = vTabel["unitateSefServiciuAgricol"].ToString();
            tbInspector1.Text = vTabel["unitateInspector1"].ToString();
            tbInspector2.Text = vTabel["unitateInspector2"].ToString();
            tbInspector3.Text = vTabel["unitateInspector3"].ToString();

            tbAp.Text = vTabel["unitateAp"].ToString();
            tbCodPostal.Text = vTabel["unitateCodPostal"].ToString();
            tbEmail.Text = vTabel["unitateEmail"].ToString();
            tbFax.Text = vTabel["unitateFax"].ToString();
            tbNumar.Text = vTabel["unitateNr"].ToString();
            tbStrada.Text = vTabel["unitateStrada"].ToString();
            tbTelefon.Text = vTabel["unitateTelefon"].ToString();
            tbWeb.Text = vTabel["unitateWeb"].ToString();
            ddlTipAfisareStrainas.SelectedValue = vTabel["unitateTipAfisareStrainas"].ToString();
        }
        vTabel.Close();
        ManipuleazaBD.InchideConexiune(vCon);
    }
    protected void btModificaCont_Click(object sender, EventArgs e)
    {
        SqlConnection vCon = ManipuleazaBD.CreareConexiune(Convert.ToInt16(Session["SESan"]));
        SqlTransaction vTranz = ManipuleazaBD.DeschideTranzactie(vCon);
        SqlCommand vCmd = new SqlCommand();
        vCmd.Connection = vCon;
        vCmd.Transaction = vTranz;
        try
        {
            vCmd.CommandText = "UPDATE [unitati] SET [unitatePrimar] = N'" + tbPrimar.Text + "', [unitateVicePrimar] = N'" + tbVicePrimar.Text + "', [unitateSecretar] = N'" + tbSecretar.Text + "', [unitateContabil] = N'" + tbContabil.Text + "', [unitateSefServiciuAgricol] = N'" + tbSefServiciuAgricol.Text + "', [unitateInspector1] = N'" + tbInspector1.Text + "', [unitateInspector2] = N'" + tbInspector2.Text + "', [unitateInspector3] = N'" + tbInspector3.Text + "', unitateAp=N'" + tbAp.Text + "', unitateCodPostal=N'" + tbCodPostal.Text + "', unitateEmail='" + tbEmail.Text + "', unitateFax = '" + tbFax.Text + "', unitateNr=N'" + tbNumar.Text + "', unitateStrada=N'" + tbStrada.Text + "', unitateTelefon='" + tbTelefon.Text + "', unitateWeb='" + tbWeb.Text + "', unitateTipAfisareStrainas='" + ddlTipAfisareStrainas.SelectedValue + "' WHERE (([unitateId] = '" + Session["SESunitateId"].ToString() + "'))";
            vCmd.CommandText = vCmd.CommandText.Replace("ț", "ţ").Replace("ș", "ş").Replace("Ț", "Ţ").Replace("Ș", "Ş");
            vCmd.ExecuteNonQuery();
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "utilizatori", "modificare", "", vCmd, Convert.ToInt64(Session["SESgospodarieId"]), 3);
            vTranz.Commit();
            valCustom.IsValid = false;
            valCustom.ErrorMessage = "Datele unității au fost modificate!";
            btEmblema_Click(sender, e);
        }
        catch { vTranz.Rollback(); }
        finally { ManipuleazaBD.InchideConexiune(vCon); }
    }
    protected string AlegeFisier(object sender, EventArgs e)
    {
        // alegem mai multe fisiere imagini pentru unitate
        // le redenumim ca imagine + id-unitate + n + numar unic
        string filepath = "";
        string vExtensie = "";
        string vUnitateCodFiscal = ManipuleazaBD.fRezultaUnString("SELECT unitateCodFiscal FROM unitati WHERE unitateId ='" + Session["SESunitateId"].ToString() + "'", "unitateCodFiscal", Convert.ToInt16(Session["SESan"]));
        if (fuEmblema.HasFile)
        {
            // alegem fisierul de importat
            filepath = fuEmblema.PostedFile.FileName;
            string vFisier = filepath;
            if (filepath.IndexOf(".jpg") == (filepath.Length - 4) || filepath.IndexOf(".jpeg") == (filepath.Length - 5) || filepath.IndexOf(".gif") == (filepath.Length - 4) || filepath.IndexOf(".png") == (filepath.Length - 4) || filepath.IndexOf(".bmp") == (filepath.Length - 4))
            {
                // scoatem extensia
                vExtensie = filepath.Substring(filepath.LastIndexOf('.'));
            }
            else
            {
                valCustom.IsValid = false;
                valCustom.ErrorMessage = "Fișierul sa fie de tip imagine!";
                filepath = "fisier invalid"; return filepath;
            }
            // cautam ultimul fisier si anume cifra de dupa n
            string vUltimulNumarDeFisier = ManipuleazaBD.fRezultaUnString("SELECT fisierNumar FROM fisiere WHERE unitateCodFiscal = '" + vUnitateCodFiscal + "' AND fisierUltimul = 'True'", "fisierNumar", Convert.ToInt16(Session["SESan"]));
            if (vUnitateCodFiscal != "14146589")
            {
                if (vUltimulNumarDeFisier == "")
                {
                    // daca nu mai exista fisiere
                    vUltimulNumarDeFisier = "101";
                }
                else
                {
                    // daca exista incrementam
                    vUltimulNumarDeFisier = (Convert.ToInt32(vUltimulNumarDeFisier) + 1).ToString();
                }
            }
            else
            {
                if (vUltimulNumarDeFisier == "")
                {
                    // daca nu mai exista fisiere
                    vUltimulNumarDeFisier = "1";
                }
                else
                {
                    // daca exista incrementam
                    vUltimulNumarDeFisier = (Convert.ToInt32(vUltimulNumarDeFisier) + 1).ToString();
                }
            }

            // construim numele fisierului
            string vFisierNume = "imagine-" + vUnitateCodFiscal + "-n" + vUltimulNumarDeFisier;
            // numeFisier
            if (tbEmblemaDescriere.Text == "")
                tbEmblemaDescriere.Text = fuEmblema.FileName;
            // este stema sau nu
            try
            {
                fuEmblema.PostedFile.SaveAs(Server.MapPath(".\\Imagini\\Embleme\\") + vFisierNume + "temp");
                Stream inFile = new FileStream(Server.MapPath(".\\Imagini\\Embleme\\") + vFisierNume + "temp", FileMode.Open, FileAccess.Read, FileShare.Read);
                ResizeFromStream(Server.MapPath(".\\Imagini\\Embleme\\") + vFisierNume + vExtensie, 500, inFile);
                // scriem in fisiere pt fiserele vechi
                ManipuleazaBD.fManipuleazaBD("UPDATE fisiere SET fisierUltimul = 'False' WHERE unitateCodFiscal ='" + vUnitateCodFiscal + "'", Convert.ToInt16(Session["SESan"]));
                // scriem in fisiere pt fisierul nou
                ManipuleazaBD.fManipuleazaBD("INSERT INTO fisiere (fisierDescriere, fisierNume, fisierDataIncarcare, fisierStatus,  fisierUltimul, fisierNumar, unitateCodFiscal) VALUES ('" + tbEmblemaDescriere.Text + "', '" + vFisierNume + vExtensie + "', CONVERT(datetime,'" + DateTime.Now.ToString() + "', 104), 'activ', 'True', '" + vUltimulNumarDeFisier + "', '" + vUnitateCodFiscal + "' )", Convert.ToInt16(Session["SESan"]));
                // stergem fisierul temporar
                File.Delete(Server.MapPath(".\\Imagini\\Embleme\\") + vFisierNume + "temp");
                tbEmblemaDescriere.Text = "";
            }
            catch
            {
            }
        }
        gvEmbleme.DataBind();
        return filepath;
    }
    protected void btEmblema_Click(object sender, EventArgs e)
    {
        // deschidem fiserul recent copiat si dezarhivat cu XMLAlegefisier
        string vCaleEmblema = AlegeFisier(sender, e);
        if (vCaleEmblema == "fisier invalid")
        {
            valCustom.IsValid = false;
            valCustom.ErrorMessage = "Alegeți un fișier imagine!";
            return;
        }
    }
    public void ResizeFromStream(string ImageSavePath, int MaxSideSize, Stream Buffer)
    {
        int intNewWidth; int intNewHeight;
        System.Drawing.Image imgInput = System.Drawing.Image.FromStream(Buffer);
        //Determine image format 
        ImageFormat fmtImageFormat = imgInput.RawFormat;
        //get image original width and height 
        int intOldWidth = imgInput.Width;
        int intOldHeight = imgInput.Height;
        //determine if landscape or portrait 
        int intMaxSide;
        if (intOldWidth >= intOldHeight)
        { intMaxSide = intOldWidth; }
        else { intMaxSide = intOldHeight; }
        if (intMaxSide > MaxSideSize)
        {
            //set new width and height 
            double dblCoef = MaxSideSize / (double)intMaxSide;
            intNewWidth = Convert.ToInt32(dblCoef * intOldWidth);
            intNewHeight = Convert.ToInt32(dblCoef * intOldHeight);
        }
        else
        { intNewWidth = intOldWidth; intNewHeight = intOldHeight; }
        //create new bitmap             
        Bitmap bmpResized = new Bitmap(imgInput, intNewWidth, intNewHeight);
        try
        {
            //save bitmap to disk 
            bmpResized.Save(ImageSavePath, fmtImageFormat);
        }
        catch
        {
            valCustom.IsValid = false;
            valCustom.ErrorMessage = "Eroare la redimensionarea imaginii!";
        }
        //release used resources 
        imgInput.Dispose();
        bmpResized.Dispose();
        Buffer.Close();
    }
    protected void fuEmblema_Load(object sender, EventArgs e)
    {

    }
    protected void imgEmblema_OnLoad(object sender, EventArgs e)
    {
        /*/   if (File.Exists(Server.MapPath(".\\Imagini\\Embleme\\tnt-registrul-agricol-") + Session["SESunitateId"].ToString() + ".jpg"))
           {
               imgEmblema.Visible = true;
               imgEmblema.ImageUrl = "~\\Imagini\\Embleme\\tnt-registrul-agricol-" + Session["SESunitateId"].ToString() + ".jpg";
           }
           else
           {
               imgEmblema.Visible = false;
           }*/
    }
    protected void gvEmbleme_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        /*  if (File.Exists(Server.MapPath(".\\Imagini\\Embleme\\tnt-registrul-agricol-") + Session["SESunitateId"].ToString() + ".jpg"))
          {
              imgEmblema.Visible = true;
              imgEmblema.ImageUrl = "~\\Imagini\\Embleme\\tnt-registrul-agricol-" + Session["SESunitateId"].ToString() + ".jpg";
          }
          else
          {
              imgEmblema.Visible = false;
          }*/
    }

    protected void gvEmbleme_PreRender(object sender, EventArgs e)
    {
        for (int i = 0; i < gvEmbleme.Rows.Count; i++)
        {
            if (gvEmbleme.Rows[i].RowType == DataControlRowType.DataRow)
            {
                if (File.Exists(Server.MapPath(".\\Imagini\\Embleme\\" + ((Label)gvEmbleme.Rows[i].FindControl("lblGvNumeFisier")).Text)))
                {
                    ((System.Web.UI.WebControls.Image)gvEmbleme.Rows[i].FindControl("imgGvEmblema")).Visible = true;
                    ((System.Web.UI.WebControls.Image)gvEmbleme.Rows[i].FindControl("imgGvEmblema")).ImageUrl = "~\\Imagini\\Embleme\\" + ((Label)gvEmbleme.Rows[i].FindControl("lblGvNumeFisier")).Text;
                }
                else
                {
                    ((System.Web.UI.WebControls.Image)gvEmbleme.Rows[i].FindControl("imgGvEmblema")).Visible = false;
                }

            }
        }
    }
    protected void SqlEmbleme_Init(object sender, EventArgs e)
    {
        SqlEmbleme.SelectParameters["unitateCodFiscal"].DefaultValue = ManipuleazaBD.fRezultaUnString("SELECT unitateCodFiscal  FROM unitati WHERE unitateId ='" + Session["SESunitateId"].ToString() + "'", "unitateCodFiscal", Convert.ToInt16(Session["SESan"]));
    }
}
