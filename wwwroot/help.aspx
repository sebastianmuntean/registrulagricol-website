﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="help.aspx.cs" Inherits="help" Culture="ro-RO" UICulture="ro-RO" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/ajax.css" rel="stylesheet" type="text/css" />
    <link href="css/master.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Panel ID="pnAdaugaMembru" CssClass="panel_general" runat="server" Visible="true">
            <asp:Panel ID="pnAd" CssClass="adauga" runat="server">
                <asp:Panel ID="pnAdTitlu" runat="server">
                    <h2>
                        <asp:Label ID="lblPaginaDenumire" runat="server" OnInit="lblPaginaDenumire_Init"></asp:Label></h2>
                </asp:Panel>
                <asp:Panel ID="pnAd1" runat="server">
                    <p>
                        <asp:Label ID="lblHelp" runat="server" OnInit="lblHelp_Init" Font-Size="Large"></asp:Label>
                    </p>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="pnButoaneAdaugare" runat="server" CssClass="butoane">
                <asp:Button ID="btInapoi" runat="server" CssClass="buton" Text="Înapoi" 
                    oninit="btInapoi_Init"/>
                <asp:Button ID="btListaCapitole" runat="server" CssClass="buton" Text="listă gospodării"
                        Visible="true" PostBackUrl="~/Gospodarii.aspx" />
            </asp:Panel>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
