﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

public partial class capitol11_nou : System.Web.UI.Page
{
    Building building;
    bool fieldsAreNotValidated = false;

    #region Events

    #region initializare-limbi

    protected override void InitializeCulture()
    {
        try
        {
            HttpCookie vCookie = Request.Cookies["COOKlimbi"];
        }
        catch
        {
            HttpCookie vCookie = new HttpCookie("COOKlimbi");
            vCookie["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie);
        }
        HttpCookie vCookie1 = Request.Cookies["COOKlimbi"];
        if ( vCookie1 != null )
        {
            if ( vCookie1["COOKlimba"] != null )
            {
                Session["SESlimba"] = vCookie1["COOKlimba"].ToString();
            }
        }
        else
        {
            HttpCookie vCookie2 = new HttpCookie("COOKLimbi");
            vCookie2["COOKlimba"] = "ro-RO";
            Response.Cookies.Add(vCookie2);
        }
        vCookie1 = Request.Cookies["COOKlimbi"];
        if ( Session["SESlimba"] == null )
        {
            Session["SESlimba"] = "ro-RO";
            if ( vCookie1 != null )
            {
                vCookie1["COOKlimba"] = "ro-RO";
                Response.Cookies.Add(vCookie1);
            }
            else
            {
                Response.Cookies.Add(vCookie1);
            }
        }
        if ( Session["SESlimba"] != null )
        {
            UICulture = Session["SESlimba"].ToString();
        }
        base.InitializeCulture();
    }

    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        if ( Session["SESgospodarieId"] == null )
        {
            HttpCookie vCookie = Request.Cookies["COOKTNT"];
            vCookie = CriptareCookie.DecodeCookie(vCookie);
            if ( vCookie != null )
            {
                if ( vCookie["COOKgospodarieId"] != null )
                {
                    Session["SESgospodarieId"] = vCookie["COOKgospodarieId"].ToString();
                }
                else if ( Request.Url.AbsolutePath.IndexOf("Gospodarii.aspx") == -1 && Session["SESgospodarieId"] == null )
                    Response.Redirect("~/Gospodarii.aspx");
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ViewState["TabIndex"] = 30;

        if ( Session["cladiri"] == null || ((Building)Session["cladiri"]).IdGospodarie!=Convert.ToInt64(Session["SESgospodarieId"]))
        {
            building = new Building();
            building.IdGospodarie = Convert.ToInt64(Session["SESgospodarieId"]);
            
            BuildingServices.ConnectionYear = Convert.ToInt16(Session["SESAn"]);
            
            building.FillBuildings();
            Session["cladiri"] = building;
        }
        else
        {
            building = (Building)Session["cladiri"];
        }

        if ( !IsPostBack )
        {
            ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "Cap.11", "vizualizare pagina", "", Convert.ToInt64(Session["SESgospodarieId"]), 1); 
            FillCladiriGridView();
        }

        if ( Convert.ToInt32(Session["SESAn"]) < 2015 )
        {
            Response.Redirect("~/Capitol11_2014.aspx");
        }
    }

    protected void ToUpdatePanelButtonClick(object sender, EventArgs e)
    {
        ShowHideControlsForUpdate();

        LoadControlsForUpdate();
    }

    protected void ModificaArataLista(object sender, EventArgs e)
    {
        addBuildingGeneralPanel.Visible = false;
        cladiriPanel.Visible = true;
        deleteButton.Visible = false;
        updateButton.Visible = false;
        toUpdatePanelButton.Visible = false;
        cladiriGridView.SelectedIndex = 1;
    }

    protected void CladiriGridViewSelectedIndexChanged(object sender, EventArgs e)
    {
        toUpdatePanelButton.Visible = true;
        deleteButton.Visible = true;
    }

    protected void CladiriGridViewRowDataBound(object sender, GridViewRowEventArgs e)
    {
        ClasaGridView.SelectGrid(cladiriGridView, e, this);
    }

    protected void PrintChapterButtonClick(object sender, EventArgs e)
    {
        DataTable capitolDataTable =  clsTiparireCapitole.UmpleRapCapitol11_CuSablon(Session["SESutilizatorId"].ToString(), Session["SESan"].ToString(), Session["SESgospodarieId"].ToString(),"11");
        Session["printCapitol11"] = capitolDataTable;
        
        ResponseHelper.Redirect("~/printCapitolul11.aspx?codCapitol=11", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
        ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "tiparire", "tiparire capitol 11", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);

    }
    protected void PrintChapterButtonClickExcel(object sender, EventArgs e)
    {
        DataTable capitolDataTable = clsTiparireCapitole.UmpleRapCapitol11_CuSablon(Session["SESutilizatorId"].ToString(), Session["SESan"].ToString(), Session["SESgospodarieId"].ToString(), "11");
        Session["printCapitol11"] = capitolDataTable;

        ResponseHelper.Redirect("~/printCapitolul11xls.aspx?codCapitol=11", "_new", "height=500,width=700,status=no,toolbar=no,menubar=no,location=no");
        ClassLog.fLog(Convert.ToInt64(Session["SESunitateId"]), Convert.ToInt64(Session["SESutilizatorId"]), DateTime.Now, "tiparire", "tiparire capitol 11", "", Convert.ToInt64(Session["SESgospodarieId"]), 5);

    }

    protected void ToAddPanelButtonClick(object sender, EventArgs e)
    {
        addBuildingGeneralPanel.Visible = true;
        cladiriPanel.Visible = false;
        addButton.Visible = true;
        updateButton.Visible = false;
        btAdaugaArataListaCladiri.Visible = true;
        addBuildingGeneralPanel.DefaultButton = addButton.ID;
        Initializare();
    }

    public void AddButtonClick(object sender, EventArgs e)
    {
        ValidateFields();

        if ( fieldsAreNotValidated )
        {
            return;
        }

        AddBuilding();
        AdaugaArataLista(); 
        FillCladiriGridView();
    }

    public void UpdateButtonClick(object sender, EventArgs e)
    {
        ValidateFields();

        if ( fieldsAreNotValidated )
        {
            return;
        }

        UpdateBuilding();
        AdaugaArataLista();
        FillCladiriGridView();
    }

    protected void DeleteButtonClick(object sender, EventArgs e)
    {
        DeleteBuilding();
        FillCladiriGridView();
   
        deleteButton.Visible = false;
        toUpdatePanelButton.Visible = false;
        cladiriGridView.SelectedIndex = -1;
    }

    protected void BtAdaugaArataListaCladiriClick(object sender, EventArgs e)
    {
        AdaugaArataLista();
    }
 
    #endregion Events

    #region Methods

    protected void Initializare()
    {
        addAdresaTextBox.Text = string.Empty;
        addSuprafataTextBox.Text = string.Empty;
        addZonaTextBox.Text = string.Empty;
        addTypeTextBox.Text = string.Empty;
        addFinishYearTextBox.Text = string.Empty;
        addDestinationTextBox.Text = string.Empty;
    }

    private void AddBuilding()
    {
        building = new Building();

        building.IdGospodarie = Convert.ToInt64(Session["SESgospodarieId"]);
        building.Adresa = addAdresaTextBox.Text;
        building.Zona = addZonaTextBox.Text;
        building.Suprafata = addSuprafataTextBox.Text != string.Empty ? Convert.ToDecimal(addSuprafataTextBox.Text) : 0;
        building.Destinatie = addDestinationTextBox.Text;
        building.Tip = addTypeTextBox.Text;
        building.AnTerminare = addFinishYearTextBox.Text != string.Empty ? Convert.ToInt16(addFinishYearTextBox.Text) : 0;
        building.UnitateId = Convert.ToInt16(Session["SESunitateId"]);
        building.An = Convert.ToInt16(Session["SESAn"]);

        BuildingServices.ConnectionYear = Convert.ToInt16(building.An);

        if ( building.BuildingAdded )
        {
            building.FillBuildings();

            Session["cladiri"] = building;
        }
    }

    private void UpdateBuilding()
    {
        building.Clear();

        building.Id = Convert.ToInt64(cladiriGridView.SelectedValue);
        building.Adresa = addAdresaTextBox.Text;
        building.Zona = addZonaTextBox.Text;
        building.Suprafata = addSuprafataTextBox.Text != string.Empty ? Convert.ToDecimal(addSuprafataTextBox.Text) : 0;
        building.Tip = addTypeTextBox.Text;
        building.AnTerminare = addFinishYearTextBox.Text != string.Empty ? Convert.ToInt16(addFinishYearTextBox.Text) : 0;
        building.Destinatie = addDestinationTextBox.Text;

        BuildingServices.ConnectionYear = Convert.ToInt16(Session["SESAn"]);

        if ( building.BuildingUpdated )
        {
            building.FillBuildings();

            Session["cladiri"] = building;
        }
    }

    private void DeleteBuilding()
    {
        building.Clear();
        building.Id = Convert.ToInt64(cladiriGridView.SelectedValue);

        BuildingServices.ConnectionYear = Convert.ToInt16(building.An);

        if ( building.BuildingDeleted )
        {
            building.FillBuildings();
            Session["cladiri"] = building;
        }
    }

    protected void AdaugaArataLista()
    {
        addBuildingGeneralPanel.Visible = false;
        cladiriPanel.Visible = true;
        deleteButton.Visible = false;
        updateButton.Visible = false;
        toUpdatePanelButton.Visible = false;
        cladiriGridView.SelectedIndex = -1; 
        buildingsPanel.Visible = true;
    }

    private void ShowHideControlsForUpdate()
    {
        addBuildingGeneralPanel.Visible = true;
        cladiriPanel.Visible = false;
        addButton.Visible = false;
        updateButton.Visible = true;
        btModificaArataLista.Visible = true;
        btAdaugaArataListaCladiri.Visible = false;
        addBuildingGeneralPanel.DefaultButton = updateButton.ID;
    }

    private void LoadControlsForUpdate()
    {
        long idCladire = Convert.ToInt64(cladiriGridView.SelectedValue);

        building = building.Buildings[idCladire];

        addAdresaTextBox.Text = building.Adresa;
        addSuprafataTextBox.Text = building.Suprafata.ToString();
        addZonaTextBox.Text = building.Zona;
        addTypeTextBox.Text = building.Tip;
        addFinishYearTextBox.Text = building.AnTerminare.ToString();
        addDestinationTextBox.Text = building.Destinatie;
    }

    private void FillCladiriGridView()
    {
        cladiriGridView.DataSource = building.Buildings.Values;
        cladiriGridView.DataBind();
    }

    private void ValidateFields()
    {
        try
        {
            if ( addSuprafataTextBox.Text != string.Empty )
            {
                Convert.ToDecimal(addSuprafataTextBox.Text.Replace('.', ','));
                fieldsAreNotValidated = false; 
            }
        }
        catch
        {
            addAdresaValidator.IsValid = false;
            addAdresaValidator.ErrorMessage = "Suprafaţa trebuie să fie numerică!";
            fieldsAreNotValidated = true;
            return;
        }

        if ( addFinishYearTextBox.Text.Length != 4 && addFinishYearTextBox.Text != string.Empty )
        {
            addAdresaValidator.IsValid = false;
            addAdresaValidator.ErrorMessage = "Anul terminării nu e valid!";
            fieldsAreNotValidated = true;
            return;
        }

        try
        {
            if ( addFinishYearTextBox.Text != string.Empty )
            {
                Convert.ToInt16(addFinishYearTextBox.Text);
                fieldsAreNotValidated = false;
            }
        }
        catch
        {
            addAdresaValidator.IsValid = false;
            addAdresaValidator.ErrorMessage = "Anul terminării trebuie să fie numeric!";
            fieldsAreNotValidated = true;
            return;
        }
    }

    #endregion Methods
 
//    protected void RelocateButtonClick(object sender, EventArgs e)
//    {
//        SqlConnection connection = ManipuleazaBD.CreareConexiune();
//        SqlCommand cmd = new SqlCommand();
//        cmd.Connection = connection;

//        cmd.CommandText = @"SELECT [id]
//                          ,[capitolId]
//                      FROM [TNTRegistruAgricol3]. [capitol11] ";

//        SqlDataReader capitolidReader = cmd.ExecuteReader();

//        StringBuilder insert = new StringBuilder();
//        string suprafata = string.Empty;
//        string tip = string.Empty;
//        string anTerminare = string.Empty;

//        SqlConnection connection1 = ManipuleazaBD.CreareConexiune();
//        SqlCommand cmd1 = new SqlCommand();
//        cmd1.Connection = connection1;

//        int i = 0;

//        while ( capitolidReader.Read() )
//        {
//            long capitolId = Convert.ToInt64(capitolidReader[1]);

//            cmd1.CommandText = "SELECT col1 from capitole where Capitolid = " + capitolId + ";";

//            try
//            {
//                suprafata = cmd1.ExecuteScalar().ToString();
//            }
//            catch
//            {
//                suprafata = "0";
//            }

//            cmd1.CommandText = "SELECT col1 from capitole where Capitolid = " + (++capitolId) + ";";

//            try
//            {
//                tip = cmd1.ExecuteScalar().ToString();
//            }
//            catch
//            {
//                tip = string.Empty;
//            }

//            cmd1.CommandText = "SELECT col1 from capitole where Capitolid = " + (++capitolId) + ";";

//            try
//            {
//                anTerminare = cmd1.ExecuteScalar().ToString();
//            }
//            catch
//            {
//                anTerminare = string.Empty;
//            }
            
//            anTerminare = anTerminare.Replace(",0000", "");
            
//            if ( anTerminare.Length != 4 )
//            {
//                anTerminare = "0";
//            }

//            capitolId -= 2;

//            insert.Append("UPDATE capitol11 SET suprafata = " + suprafata.Replace(",", ".") + ", tip = '" + tip + "', anTerminare = " + Convert.ToDecimal(anTerminare) + " WHERE capitolId = " + capitolId + ";");

//            i++;
//            //if (i%10000==0)
//            //{
//            //    cmd.CommandTimeout = 190;
//            //    cmd.CommandText = insert.ToString();
//            //    cmd.ExecuteNonQuery();
//            //    insert.Remove(0, insert.Length);
//            //}
//            //else
//            //{
//            //    if(i>110000 && i<=112414)
//            //    {
//            //        cmd.CommandTimeout = 5;
//            //        cmd.CommandText = insert.ToString();
//            //        cmd.ExecuteNonQuery();
//            //        insert.Remove(0, insert.Length);
//            //    }
//            //}
//        }

//        cmd1.Connection.Close();

//        capitolidReader.Close();
//        cmd.CommandTimeout = 190;
//        cmd.CommandText = insert.ToString();
//     //   cmd.ExecuteNonQuery();

//        cmd.Connection.Close();
//    }

    protected void ReparaButtonClick(object sender, EventArgs e)
    {
        Building building = new Building();

        BuildingServices.ConnectionYear = Convert.ToInt16(Session["SESAn"]);

        building.UnitateId = Convert.ToInt32(Session["SESunitateId"]);

        List<Building> buildings = building.GetMissingBuildingsByUInitate();

        if(buildings.Count > 0)
        {
            building.AddMissingBuildings(buildings);
        }
    }
}
